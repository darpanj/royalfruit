-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 06:47 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zestbrai_royal_fruit`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_content`
--

CREATE TABLE `mst_content` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `sys_flag` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_content`
--

INSERT INTO `mst_content` (`id`, `sys_flag`, `title`, `content`, `created`, `updated`) VALUES
(2, 'terms', 'Nice', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:12:29', '2020-01-20 07:12:29'),
(3, 'help', 'Help', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><figure class=\"table\"><table><tbody><tr><td>1</td><td>1</td></tr><tr><td>1</td><td>1</td></tr></tbody></table></figure><p><strong>nice to see you Again</strong></p><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:20:19', '2020-01-20 07:20:19');

-- --------------------------------------------------------

--
-- Table structure for table `mst_email_templates`
--

CREATE TABLE `mst_email_templates` (
  `iEmailId` int(1) UNSIGNED NOT NULL,
  `vSysFlag` varchar(50) CHARACTER SET utf8 NOT NULL,
  `vTitle` varchar(100) CHARACTER SET utf8 NOT NULL,
  `vDescription` text CHARACTER SET utf8 NOT NULL,
  `vFromEmail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `vFromName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `eEmailType` enum('user','admin') CHARACTER SET utf8 NOT NULL DEFAULT 'user',
  `vSubject` varchar(200) CHARACTER SET utf8 NOT NULL,
  `vTemplate` text CHARACTER SET utf8 NOT NULL,
  `dUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `eIsactive` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_email_templates`
--

INSERT INTO `mst_email_templates` (`iEmailId`, `vSysFlag`, `vTitle`, `vDescription`, `vFromEmail`, `vFromName`, `eEmailType`, `vSubject`, `vTemplate`, `dUpdatedDate`, `eIsactive`) VALUES
(1, 'user_register', 'Confirmation email', 'Registration confirmation email', 'mail.zestbrains4u.site', 'AJAR', 'user', '{{SITENAME}} Team', '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\r\n<title></title>\r\n<style type=\"text/css\">body {\r\n    margin: 0;\r\n    padding: 0;\r\n    background: #ccc;\r\n    font-size: 15px;\r\n    line-height: normal;\r\n    color: #000;\r\n    font-family: arial;\r\n  }\r\n</style>\r\n<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td align=\"center\">\r\n			<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"background: #fff;\" width=\"640\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:15px 45px; background-color:#e85039;\" width=\"540\"><a href=\"{{SITE_URL}}\" style=\"display: inline-block;\" target=\"_blank\"><img alt=\"{{SITENAME}}\" src=\"{{SITE_LOGO}}\" style=\"width: 150px;\" /> </a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 50px 45px 25px 45px; font-family: arial; font-size: 15px; color: #333; line-height: normal;\" width=\"550\">Dear <strong>{{GREETINGS}},</strong></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">Thank You for register account on {{SITENAME}}.</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px;\" width=\"550\"><a href=\"{{ACTIVATION_LINK}}\" style=\"font-size: 14px; font-family: arial; color: #e85039; font-weight: bold; text-decoration: none; line-height: normal;\">Click here to active your account</a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 25px 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">Thank you,kind regards,</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 50px; font-family: arial; font-size: 14px; font-weight: bold; color: #000; line-height: normal;\" width=\"550\">The {{SITENAME}} Team</td>\r\n					</tr>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:20px 45px; font-family: arial; background:#e85039;color: #fff; font-size:15px; margin:0;\" width=\"550\">{{FOOTER_TEXT}}</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '2020-01-21 11:51:27', 'y'),
(2, 'forgot_password', 'Forgot password', 'Email with reset password link', 'mail.zestbrains4u.site', 'AJAR', 'user', 'You requested a new password to your {{SITENAME}} account', '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\r\n<title></title>\r\n<style type=\"text/css\">body {\r\n    margin: 0;\r\n    padding: 0;\r\n    background: #ccc;\r\n    font-size: 15px;\r\n    line-height: normal;\r\n    color: #000;\r\n    font-family: arial;\r\n  }\r\n</style>\r\n<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td align=\"center\">\r\n			<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"background: #fff;\" width=\"640\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:15px 45px; background-color:#5454D9;\" width=\"540\"><a href=\"{{SITE_URL}}\" style=\"display: inline-block;\" target=\"_blank\"><img alt=\"{{SITENAME}}\" src=\"{{SITE_LOGO}}\" style=\"width: 150px;\" /> </a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 50px 45px 25px 45px; font-family: arial; font-size: 15px; color: #333; line-height: normal;\" width=\"550\">Dear <strong>{{GREETINGS}},</strong></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">You Recently Request to Reset Your Password at {{SITENAME}} Account.</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px;\" width=\"550\"><a href=\"{{RESET_PASS_LINK}}\" style=\"font-size: 14px; font-family: arial; color: #e85039; font-weight: bold; text-decoration: none; line-height: normal;\">Click Hear to Reset Your Password</a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 25px 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">Thank you,kind regards,</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 50px; font-family: arial; font-size: 14px; font-weight: bold; color: #000; line-height: normal;\" width=\"550\">The {{SITENAME}} Team</td>\r\n					</tr>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:20px 45px; font-family: arial; background:#5454D9;color: #fff; font-size:15px; margin:0;\" width=\"550\">{{FOOTER_TEXT}}</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '2020-01-21 11:51:31', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `mst_imagethumb`
--

CREATE TABLE `mst_imagethumb` (
  `iThumbId` int(1) UNSIGNED NOT NULL,
  `vFolder` varchar(50) CHARACTER SET utf8 NOT NULL,
  `iHeight` int(1) UNSIGNED NOT NULL,
  `iWidth` int(1) UNSIGNED NOT NULL,
  `vDefaultImage` varchar(100) CHARACTER SET utf8 NOT NULL,
  `vIsactive` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_imagethumb`
--

INSERT INTO `mst_imagethumb` (`iThumbId`, `vFolder`, `iHeight`, `iWidth`, `vDefaultImage`, `vIsactive`, `comments`) VALUES
(1, 'users', 150, 150, 'no_image.png', 'y', ''),
(2, 'models', 40, 40, 'no_image.png', 'y', 'for_profile_pic_of_models'),
(3, 'models', 175, 336, 'no_image.png', 'y', 'for_cover_pic_of_models'),
(4, 'models', 175, 336, 'no_image.png', 'y', 'for_streamer_pic_of_models');

-- --------------------------------------------------------

--
-- Table structure for table `mst_lang`
--

CREATE TABLE `mst_lang` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_code` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_lang`
--

INSERT INTO `mst_lang` (`id`, `name`, `short_code`, `currency`, `status`, `insert_date`, `update_date`) VALUES
(1, 'English', 'en', 'USD', 'active', '2019-03-16 00:00:00', '2019-03-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mst_module`
--

CREATE TABLE `mst_module` (
  `id` int(11) NOT NULL,
  `mod_modulegroupcode` varchar(25) NOT NULL,
  `mod_modulegroupname` varchar(50) NOT NULL,
  `mod_modulegroupname_lang` varchar(255) NOT NULL,
  `mod_modulecode` varchar(25) NOT NULL,
  `mod_modulename` varchar(50) NOT NULL,
  `mod_modulename_lang` varchar(255) NOT NULL,
  `mod_modulegrouporder` int(3) NOT NULL,
  `mod_moduleorder` int(3) NOT NULL,
  `mod_modulepagename` varchar(255) NOT NULL,
  `vImage` varchar(255) NOT NULL,
  `display` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_module`
--

INSERT INTO `mst_module` (`id`, `mod_modulegroupcode`, `mod_modulegroupname`, `mod_modulegroupname_lang`, `mod_modulecode`, `mod_modulename`, `mod_modulename_lang`, `mod_modulegrouporder`, `mod_moduleorder`, `mod_modulepagename`, `vImage`, `display`) VALUES
(1, 'DASHBOARD', 'Dashboard', '', 'DASHBOARD', 'Dashboard', '', 1, 1, 'dashboard', 'kt-menu__link-icon flaticon2-architecture-and-city', 1),
(2, 'GEN_SET', 'General Settings', '', 'GEN_SET', 'General Settings', '', 100, 1, 'setting', 'kt-menu__link-icon fas fa-cogs', 1),
(3, 'GEN_SET', 'General Settings', '', 'SITE_SETTINGS', 'Site Settings', '', 100, 2, 'setting', 'kt-menu__link-icon', 1),
(4, 'GEN_SET', 'General Settings', '', 'CHANGE_PASS', 'Change Password', '', 100, 3, 'cpass', 'kt-menu__link-icon', 1),
(5, 'USERS', 'Users', '', 'USERS', 'Users', '', 2, 1, 'Users', 'fa fa-users', 1),
(6, 'MODELS', 'Models', '', 'MODELS', 'Models', '', 2, 1, 'ActiveModels', 'fa fa-users', 1),
(9, 'MODELS', 'Models', '', 'ACTIVE_MODELS', 'Active Models', '', 2, 1, 'ActiveModels', 'fa fa-users', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_role_rights`
--

CREATE TABLE `mst_role_rights` (
  `id` int(11) NOT NULL,
  `rr_rolecode` varchar(255) NOT NULL,
  `rr_modulecode` varchar(255) NOT NULL,
  `rr_create` enum('yes','no') NOT NULL,
  `rr_edit` enum('yes','no') NOT NULL,
  `rr_delete` enum('yes','no') NOT NULL,
  `rr_view` enum('yes','no') NOT NULL,
  `rr_status` enum('yes','no') NOT NULL,
  `extra` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_role_rights`
--

INSERT INTO `mst_role_rights` (`id`, `rr_rolecode`, `rr_modulecode`, `rr_create`, `rr_edit`, `rr_delete`, `rr_view`, `rr_status`, `extra`) VALUES
(1, 'admin', 'DASHBOARD', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(2, 'admin', 'GEN_SET', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(3, 'admin', 'SITE_SETTINGS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(4, 'admin', 'CHANGE_PASS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(5, 'admin', 'USERS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(6, 'admin', 'MODELS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(7, 'admin', 'ACTIVE_MODELS', 'no', 'yes', 'yes', 'yes', 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_sitesettings`
--

CREATE TABLE `mst_sitesettings` (
  `iFieldId` int(1) UNSIGNED NOT NULL,
  `vLabel` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `vLabelLang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `vType` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `vConstant` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `vOptions` text CHARACTER SET utf8 NOT NULL,
  `vClass` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `eRequired` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `vValue` text CHARACTER SET utf8 NOT NULL,
  `vHint` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `eEditable` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `eStatus` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `dUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_sitesettings`
--

INSERT INTO `mst_sitesettings` (`iFieldId`, `vLabel`, `vLabelLang`, `vType`, `vConstant`, `vOptions`, `vClass`, `eRequired`, `vValue`, `vHint`, `eEditable`, `eStatus`, `dUpdatedDate`) VALUES
(1, 'Site Name', '', 'textbox', 'SITENAME', '', NULL, 'y', 'ROYAL FRUIT', 'Site name which you want to display', 'y', 'y', '2020-02-18 05:43:22'),
(2, 'Site Logo', '', 'file', 'SITE_LOGO', '', 'img_class', '', 'logo.png', 'Site logo which you want to display', 'n', 'y', '2018-08-31 11:18:46'),
(3, 'Admin email', '', 'textbox', 'ADMIN_EMAIL', '', 'email_class', 'y', 'darpanj.zestbrains@gmail.com', NULL, 'y', 'y', '2020-02-18 05:43:33'),
(4, 'Footer Text', '', 'textbox', 'FOOTER_TEXT', '', NULL, 'y', 'Copyright  © {{YEAR}}  RoyalFruit. All Rights Reserved', NULL, 'y', 'y', '2020-02-18 05:44:21'),
(5, 'Author', '', 'textbox', 'SITE_AUTHOR', '', NULL, 'y', 'ROYALFRUIT', NULL, 'y', 'y', '2020-02-18 05:44:36');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_extra`
--

CREATE TABLE `tbl_model_extra` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `rules` text NOT NULL,
  `follow_price` int(10) NOT NULL COMMENT 'usd',
  `snapchat_price` int(10) NOT NULL COMMENT '''usd'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_extra`
--

INSERT INTO `tbl_model_extra` (`id`, `model_id`, `rules`, `follow_price`, `snapchat_price`) VALUES
(1, 12, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 25, 10),
(2, 3, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 12, 50),
(3, 4, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 20, 55),
(4, 5, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 11, 22),
(5, 6, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 99, 3),
(6, 7, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 12, 30),
(7, 8, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 9, 50),
(8, 9, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 45, 22),
(9, 10, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 10, 30),
(10, 11, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 78, 60);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_followers`
--

CREATE TABLE `tbl_model_followers` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_followers`
--

INSERT INTO `tbl_model_followers` (`id`, `model_id`, `user_id`, `payment_id`, `created`) VALUES
(2, 12, 2, 'Payment_20339', '2020-05-11 14:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_posts`
--

CREATE TABLE `tbl_model_posts` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `category` int(2) NOT NULL DEFAULT '0' COMMENT '0=followers,1=teaser,2=private,3=premium',
  `post_type` int(3) NOT NULL DEFAULT '0' COMMENT '0=''images'',1=''video''',
  `image` text NOT NULL,
  `video_thumb_image` text NOT NULL,
  `video_hint_text` text NOT NULL,
  `post_price` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_posts`
--

INSERT INTO `tbl_model_posts` (`id`, `model_id`, `category`, `post_type`, `image`, `video_thumb_image`, `video_hint_text`, `post_price`, `created`, `updated`) VALUES
(2, 3, 0, 0, '3/followers/images/follow_1.jpg', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(3, 4, 0, 0, '4/followers/images/profile.jpg', '', '', '', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(4, 5, 0, 0, '5/followers/images/profile.jpg', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(5, 6, 1, 0, '6/teaser/images/profile.jpg', '', '', '', '2020-01-26 07:00:00', '2020-02-26 15:54:51'),
(6, 7, 0, 0, '7/followers/images/profile.jpg', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(7, 8, 0, 0, '8/followers/images/profile.jpg', '', '', '', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(8, 9, 1, 0, '9/teaser/images/profile.jpg', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(9, 10, 0, 0, '10/followers/images/profile.jpg', '', '', '', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(10, 11, 0, 0, '11/followers/images/profile.jpg', '', '', '', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(12, 8, 1, 1, '8/teaser/videos/sample.mp4', '', '', '', '2020-01-30 07:00:00', '2020-02-26 15:54:51'),
(15, 3, 2, 0, '3/followers/images/follow_2.jpeg', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(16, 12, 1, 0, '9/teaser/images/profile.jpg', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(17, 12, 1, 1, '9/teaser/videos/sample.mp4', '', '', '', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(18, 12, 3, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Getting Nude', '70', '2020-02-25 00:00:00', '2020-02-26 15:54:51'),
(19, 12, 3, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Showing something hot', '80', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(20, 12, 2, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Showing something hot', '80', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(21, 11, 2, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Custom Video is a private', '80', '2020-02-26 00:00:00', '2020-02-26 15:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_posts_fav`
--

CREATE TABLE `tbl_model_posts_fav` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_posts_fav`
--

INSERT INTO `tbl_model_posts_fav` (`id`, `post_id`, `user_id`, `created`, `updated`) VALUES
(1, 15, 2, '2020-04-24 15:59:13', '2020-04-24 15:59:13'),
(4, 12, 2, '2020-04-29 12:51:20', '2020-04-29 12:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL,
  `read_flag` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_message` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `read_flag`, `user_id`, `notification_message`, `created`, `updated`) VALUES
(1, 0, 2, 'notification 1', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(2, 0, 2, 'notification 2\r\n', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(3, 0, 2, 'notification 3', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(4, 0, 2, 'notification 4\r\n', '2020-04-25 20:12:04', '2020-04-25 20:12:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `status` text NOT NULL,
  `payment_for` enum('0','1','2','3','4') NOT NULL DEFAULT '0' COMMENT '0=token puchase,1=model follow,2=snapchat purchase,3=private video purchase,4=custom video purchase',
  `price` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`id`, `payment_id`, `status`, `payment_for`, `price`, `user_id`, `created`) VALUES
(1, 'Payment_03085', 'success', '0', '', 2, '2020-04-20 10:34:53'),
(2, 'Payment_31987', 'success', '0', '', 2, '2020-04-20 10:35:06'),
(3, 'Payment_39641', 'success', '0', '', 2, '2020-04-20 10:36:02'),
(4, 'Payment_93771', 'success', '0', '', 2, '2020-04-20 10:36:10'),
(5, 'Payment_25759', 'success', '0', '', 2, '2020-04-20 10:37:09'),
(6, 'Payment_25254', 'success', '1', '12', 2, '2020-04-22 06:30:04'),
(7, 'Payment_65334', 'success', '1', '20', 2, '2020-04-22 06:30:57'),
(8, 'Payment_68178', 'success', '1', '99', 2, '2020-04-22 06:42:28'),
(9, 'Payment_13172', 'success', '1', '99', 2, '2020-04-22 06:50:07'),
(10, 'Payment_45653', 'success', '1', '45', 2, '2020-04-24 08:21:06'),
(11, 'Payment_77118', 'success', '1', '12', 2, '2020-04-24 08:35:06'),
(12, 'Payment_41075', 'success', '1', '25', 2, '2020-04-24 08:57:28'),
(13, 'Payment_08629', 'success', '1', '9', 2, '2020-04-24 09:14:04'),
(14, 'Payment_16373', 'success', '1', '12', 2, '2020-04-24 09:14:59'),
(15, 'Payment_01513', 'success', '1', '12', 2, '2020-04-24 09:15:05'),
(16, 'Payment_09092', 'success', '1', '12', 2, '2020-04-24 09:15:06'),
(17, 'Payment_38823', 'success', '1', '12', 2, '2020-04-24 09:15:06'),
(18, 'Payment_75218', 'success', '1', '12', 2, '2020-04-24 09:15:06'),
(19, 'Payment_07426', 'success', '1', '12', 2, '2020-04-24 09:15:07'),
(20, 'Payment_76805', 'success', '1', '20', 2, '2020-04-24 09:16:15'),
(21, 'Payment_78965', 'success', '1', '11', 2, '2020-04-24 09:17:43'),
(22, 'Payment_41880', 'success', '1', '11', 2, '2020-04-24 09:17:45'),
(23, 'Payment_25812', 'success', '1', '99', 2, '2020-04-24 09:18:45'),
(24, 'Payment_15281', 'success', '1', '12', 2, '2020-04-24 10:16:53'),
(25, 'Payment_54013', 'success', '1', '12', 2, '2020-04-24 10:31:51'),
(26, 'Payment_74135', 'success', '1', '45', 2, '2020-04-24 11:12:00'),
(27, 'Payment_52110', 'success', '1', '25', 2, '2020-04-24 12:13:32'),
(28, 'Payment_17756', 'success', '1', '12', 2, '2020-04-24 13:31:03'),
(29, 'Payment_49921', 'success', '1', '20', 2, '2020-04-25 05:37:30'),
(30, 'Payment_25851', 'success', '1', '99', 2, '2020-04-25 06:55:07'),
(31, 'Payment_16340', 'success', '1', '25', 2, '2020-04-29 12:45:16'),
(32, 'Payment_04331', 'success', '2', '70', 2, '2020-05-01 09:32:23'),
(33, 'Payment_43832', 'success', '2', '70', 2, '2020-05-01 09:35:06'),
(34, 'Payment_50881', 'success', '2', '70', 2, '2020-05-01 09:36:18'),
(35, 'Payment_83558', 'success', '2', '70', 2, '2020-05-01 09:37:10'),
(36, 'Payment_36924', 'success', '3', '70', 2, '2020-05-01 09:39:18'),
(37, 'Payment_17459', 'success', '3', '70', 2, '2020-05-01 10:54:15'),
(38, 'Payment_73553', 'success', '3', '70', 2, '2020-05-01 11:02:47'),
(39, 'Payment_24742', 'success', '3', '70', 2, '2020-05-01 11:05:48'),
(40, 'Payment_19108', 'success', '3', '70', 2, '2020-05-01 11:06:22'),
(41, 'Payment_73967', 'success', '1', '25', 2, '2020-05-01 11:07:13'),
(42, 'Payment_71104', 'success', '3', '70', 2, '2020-05-01 11:21:36'),
(43, 'Payment_10589', 'success', '3', '70', 2, '2020-05-01 11:42:44'),
(44, 'Payment_76064', 'success', '3', '70', 2, '2020-05-02 10:16:20'),
(45, 'Payment_68864', 'success', '3', '70', 2, '2020-05-02 10:45:53'),
(46, 'Payment_28059', 'success', '3', '70', 2, '2020-05-02 10:45:57'),
(47, 'Payment_72941', 'success', '3', '70', 2, '2020-05-02 13:06:24'),
(48, 'Payment_65132', 'success', '1', '25', 2, '2020-05-03 11:23:26'),
(49, 'Payment_14259', 'success', '1', '25', 2, '2020-05-11 13:24:41'),
(50, 'Payment_20339', 'success', '1', '25', 2, '2020-05-11 14:21:49'),
(51, 'Payment_23859', 'success', '3', '80', 2, '2020-05-11 14:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_token`
--

CREATE TABLE `tbl_token` (
  `id` int(11) NOT NULL,
  `token_qty` int(11) NOT NULL,
  `token_price` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_token`
--

INSERT INTO `tbl_token` (`id`, `token_qty`, `token_price`, `status`, `created`) VALUES
(1, 10, 100, 'active', '2020-04-20 00:00:00'),
(2, 20, 180, 'active', '2020-04-19 00:00:00'),
(3, 30, 250, 'active', '2020-04-19 00:00:00'),
(4, 40, 350, 'active', '2020-04-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_token_users`
--

CREATE TABLE `tbl_token_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tokens` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_token_users`
--

INSERT INTO `tbl_token_users` (`id`, `user_id`, `tokens`, `created`, `updated`) VALUES
(1, 2, 90, '2020-04-20 16:04:53', '2020-04-20 16:04:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(255) NOT NULL,
  `user_slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `type` enum('user','admin','model') CHARACTER SET latin1 NOT NULL DEFAULT 'user',
  `country_code` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT '+91',
  `mobile` varchar(15) CHARACTER SET latin1 NOT NULL,
  `profile_image` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cover_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `streamer_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diamonds` int(20) NOT NULL DEFAULT '0',
  `location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` int(11) NOT NULL DEFAULT '1',
  `language` enum('english','arabic') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'english',
  `currency` enum('usd','afghani','inr','riyal','dirham','euro') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'usd',
  `token` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` enum('active','inactive','deleted') CHARACTER SET latin1 NOT NULL DEFAULT 'active',
  `live_status` int(2) NOT NULL DEFAULT '0' COMMENT '0=offline,1=online',
  `is_featured` int(2) NOT NULL DEFAULT '0' COMMENT '0=not featured, 1=featured',
  `customer_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_slug`, `name`, `bio`, `facebook_id`, `twitter_id`, `google_id`, `email`, `username`, `password`, `type`, `country_code`, `mobile`, `profile_image`, `cover_image`, `streamer_image`, `diamonds`, `location`, `latitude`, `longitude`, `notification`, `language`, `currency`, `token`, `status`, `live_status`, `is_featured`, `customer_id`, `created`, `updated`) VALUES
(1, '', 'admin', '', '', '', '', 'admin.zestbrains@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '+91', '123456', '142/4bfb6b7519c6542d2e83200efa6e7de3.jpg', '', '', 0, '', '', '', 0, 'english', 'usd', '', 'active', 0, 0, '', '2020-01-02 11:03:35', '2020-01-02 11:03:35'),
(2, '', 'Lee Mark', 'this is mark henry\'s  bio', '', '', '', 'leemark@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'user', '+undefined', '', '2/profile_pic/2aa1b0c98dbdac6ce39f0ac95b580058.jpg', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-02-22 09:35:12', '2020-04-24 06:19:01'),
(3, '83794', 'Sakira', '', '', '', '', 'sakira@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '3/profile_pic/profile.jpg', '3/cover_pic/cover.png', '', 5, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-21 09:36:04', '2020-02-22 09:36:04'),
(4, '', 'Lady Gaga', '', '', '', '', 'ladygaga@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '4/profile_pic/profile.jpg', '4/cover_pic/cover.png', '', 8, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-22 09:36:04', '2020-02-22 09:36:04'),
(5, '', 'Celion Dion', '', '', '', '', 'celiondioan@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '5/profile_pic/profile.jpg', '5/cover_pic/cover.png', '', 1, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-23 09:36:04', '2020-02-22 09:36:04'),
(6, '', 'Jessa Coras', '', '', '', '', 'jessacoras@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '6/profile_pic/profile.jpg', '6/cover_pic/cover.png', '', 3, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-24 09:36:04', '2020-02-27 09:36:04'),
(7, '', 'Samy Lendropz', '', '', '', '', 'samy520@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '7/profile_pic/profile.jpg', '7/cover_pic/cover.png', '7/stremer_pic/mystream.jpg', 7, '', '', '', 1, 'english', 'usd', '', 'active', 1, 1, '', '2020-02-25 09:36:04', '2020-02-22 09:36:04'),
(8, '', 'Yuka Henry', '', '', '', '', 'yhenry@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '8/profile_pic/profile.jpg', '8/cover_pic/cover.png', '', 9, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-26 09:36:04', '2020-02-22 09:36:04'),
(9, '', 'Suzain Parmalika', '', '', '', '', 'suzainp@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '9/profile_pic/profile.jpg', '9/cover_pic/cover.png', '', 23, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-27 09:36:04', '2020-02-22 09:36:04'),
(10, '', 'Vislas Minrama', '', '', '', '', 'vislasm@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '10/profile_pic/profile.jpg', '10/cover_pic/cover.png', '', 2, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-28 09:36:04', '2020-02-22 09:36:04'),
(11, '', 'Rose Kindraa', '', '', '', '', 'rosekindra@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '+undefined', '', '11/profile_pic/profile.jpg', '11/cover_pic/cover.png', '', 120, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-29 09:36:04', '2020-03-02 08:40:00'),
(12, '83793', 'Vela Mista', 'Down to Earth and always smiling :) | Fitness enthusiast living in LA | IG: @gucciblue | Follow for                     access to daily private content, messaging, and more! LIVE EVERY MONDAY @ 7:30PM', '', '', '', 'vela@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '11/profile_pic/profile.jpg', '11/cover_pic/cover.png', '', 12, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-03-07 11:01:17', '2020-03-07 11:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_service`
--

CREATE TABLE `tbl_user_service` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_type` int(3) NOT NULL DEFAULT '0' COMMENT '''0=email_notification''',
  `service` int(5) NOT NULL DEFAULT '0' COMMENT '''0=private_message'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_videos`
--

CREATE TABLE `tbl_videos` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=private_videos, 1=custom videos',
  `payment_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_videos`
--

INSERT INTO `tbl_videos` (`id`, `post_id`, `user_id`, `category`, `payment_id`) VALUES
(1, 19, 2, '0', 'Payment_23859');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_content`
--
ALTER TABLE `mst_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_email_templates`
--
ALTER TABLE `mst_email_templates`
  ADD PRIMARY KEY (`iEmailId`);

--
-- Indexes for table `mst_imagethumb`
--
ALTER TABLE `mst_imagethumb`
  ADD PRIMARY KEY (`iThumbId`);

--
-- Indexes for table `mst_lang`
--
ALTER TABLE `mst_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_module`
--
ALTER TABLE `mst_module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mod_modulecode` (`mod_modulecode`);

--
-- Indexes for table `mst_role_rights`
--
ALTER TABLE `mst_role_rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sitesettings`
--
ALTER TABLE `mst_sitesettings`
  ADD PRIMARY KEY (`iFieldId`);

--
-- Indexes for table `tbl_model_extra`
--
ALTER TABLE `tbl_model_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_extra_key` (`model_id`);

--
-- Indexes for table `tbl_model_followers`
--
ALTER TABLE `tbl_model_followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_followers` (`model_id`),
  ADD KEY `model_user_followers` (`user_id`);

--
-- Indexes for table `tbl_model_posts`
--
ALTER TABLE `tbl_model_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_id` (`model_id`);

--
-- Indexes for table `tbl_model_posts_fav`
--
ALTER TABLE `tbl_model_posts_fav`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fav_post` (`post_id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_token`
--
ALTER TABLE `tbl_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_token_users`
--
ALTER TABLE `tbl_token_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_service`
--
ALTER TABLE `tbl_user_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_user_id` (`user_id`);

--
-- Indexes for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_content`
--
ALTER TABLE `mst_content`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mst_email_templates`
--
ALTER TABLE `mst_email_templates`
  MODIFY `iEmailId` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mst_imagethumb`
--
ALTER TABLE `mst_imagethumb`
  MODIFY `iThumbId` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mst_lang`
--
ALTER TABLE `mst_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_module`
--
ALTER TABLE `mst_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mst_role_rights`
--
ALTER TABLE `mst_role_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mst_sitesettings`
--
ALTER TABLE `mst_sitesettings`
  MODIFY `iFieldId` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_model_extra`
--
ALTER TABLE `tbl_model_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_model_followers`
--
ALTER TABLE `tbl_model_followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_model_posts`
--
ALTER TABLE `tbl_model_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_model_posts_fav`
--
ALTER TABLE `tbl_model_posts_fav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `tbl_token`
--
ALTER TABLE `tbl_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_token_users`
--
ALTER TABLE `tbl_token_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_user_service`
--
ALTER TABLE `tbl_user_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_model_extra`
--
ALTER TABLE `tbl_model_extra`
  ADD CONSTRAINT `model_extra_key` FOREIGN KEY (`model_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_model_followers`
--
ALTER TABLE `tbl_model_followers`
  ADD CONSTRAINT `model_followers` FOREIGN KEY (`model_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `model_user_followers` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_model_posts`
--
ALTER TABLE `tbl_model_posts`
  ADD CONSTRAINT `model_id` FOREIGN KEY (`model_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_model_posts_fav`
--
ALTER TABLE `tbl_model_posts_fav`
  ADD CONSTRAINT `fav_post` FOREIGN KEY (`post_id`) REFERENCES `tbl_model_posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_user_service`
--
ALTER TABLE `tbl_user_service`
  ADD CONSTRAINT `service_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
