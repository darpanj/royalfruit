var Custom = function () {

    // private functions & variables

    var dispMessage = function (sType, sText) {
        toastr[sType.toLowerCase()](sText, sType);
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.
        },

        //some helper function
        myNotification: function (sType, sText) {
            dispMessage(sType, sText);
        }

    };

}();
$(function () {
    var url = window.location;
    $('.page-sidebar-menu  a[href="' + url + '"]').parent('li').addClass('active');
    $('.page-sidebar-menu a').filter(function () {
        return this.href == url;
    }).parent('li').addClass('active').parent('ul').parent('li').addClass('active open');
});

function setTitle(aoData, a) {
    aoTitles = []; // this array will hold title-based sort info
    oSettings = a.fnSettings();  // the oSettings will give us access to the aoColumns info
    i = 0;
    for (ao in aoData) {
        name = aoData[ao].name;
        value = aoData[ao].value;

        if (name.substr(0, "iSortCol_".length) == "iSortCol_") {
            // get the column number from "ao"
            iCol = parseInt(name.replace("iSortCol_", ""));
            sName = "";
            if (oSettings.aoColumns[value])
                sName = oSettings.aoColumns[value].sName;
            // create an entry in aoTitles (which will later be appended to aoData) for this column
            aoTitles.push({ name: "iSortTitle_" + iCol, value: sName });
            i++;
        }

    }

    // for each entry in aoTitles, push it onto aoData
    for (ao in aoTitles)
        aoData.push(aoTitles[ao]);
}

//Change record status start
$(document).on('switch-change', '.status-switch', function (event, state) {
    $this = $(this);
    bootbox.confirm("Are you sure you want to change status?", function (result) {
        if (result) {
            $this.prop('checked', state.value);
            var customAct = typeof $this.data('getaction') != 'undefined' ? $this.data('getaction') : '';
            var val = state.value ? 'y' : 'n';
            var url = $this.data('url');
            var id = $this.data('id');
            var table = $this.data('table');
            var action = customAct != '' ? customAct : 'change_status';

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: { action: action, table: table, value: val, id: id },
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        } else {
            if (state.value) {
                $this.closest("div").parent("div").removeClass("switch-on");
                $this.closest("div").parent("div").addClass("switch-off");
            } else {
                $this.closest("div").parent("div").removeClass("switch-off");
                $this.closest("div").parent("div").addClass("switch-on");
            }
        }
    });
});


$(document).on('change', '.dropdown', function (event, state) {
    $this = $(this);
    bootbox.confirm("Are you sure you want to change order status?", function (result) {
        if (result) {
            var customAct = typeof $this.data('getaction') != 'undefined' ? $this.data('getaction') : '';
            var val = $this.val();
            var url = $this.data('url');
            var id = $this.data('id');
            var table = $this.data('table');
            var action = customAct != '' ? customAct : 'change_order_status';

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: { action: action, table: table, value: val, id: id },
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        }
    });
});
//Change record status end

//Delete single record start
$(document).on('click', '.btnDelete', function (event, state) {
    $this = $(this);
    $this.attr("disabled", "disabled");
    bootbox.confirm("Are you sure you want to delete this?", function (result) {
        if (result) {

            var url = $this.data('url');
            var id = $this.data('id');

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: { action: 'delete', id: id },
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        }
        $this.removeAttr("disabled");
    });

});

//Delete single record end

function getStatusText(code) {
    sText = "";
    if (code !== undefined) {
        switch (code) {
            case 200: {
                sText = 'Success';
                break;
            }
            case 404: {
                sText = 'Error';
                break;
            }
            case 403: {
                sText = 'Error';
                break;
            }
            case 500: {
                sText = 'Error';
                break;
            }
            default: {
                sText = 'Error';
            }

        }
    }
    return sText;
}

function scrollToElement(e) {
    $('html, body').animate({ scrollTop: $(e).offset().top - 100 }, 'slow');
}

$(document).on('click', '.btnEdit,.btnView', function (e) {
    e.preventDefault();
    var $this = $(this);
    var id = $this.data('id');
    var type = $this.data('type');
    var editLink = $(this).data('url');

    $.ajax({
        url: editLink,
        type: 'POST',
        beforeSend: addOverlay,
        dataType: 'json',
        data: { id: id, type: type },
        success: function (r) {
            if (r.status == 200) {
                $(".pageform").html(r.html);
                $('.portlet-toggler').toggle();
                scrollToElement(".page-content");
            } else {
                sText = r.msg;
                Custom.myNotification('Error', sText);
            }
        },
        complete: removeOverlay
    });
});

$(document).on('click', '.btn-toggler', function (e) {
    e.preventDefault();
    $('.portlet-toggler').toggle();
});

//start select all and delete records
$(document).on('click', '.all_select', function () {
    if ($(this).hasClass('allChecked')) {
        $('#listResults tbody input[class="small-chk"]').prop('checked', false);
    } else {
        $('#listResults tbody input[class="small-chk"]').prop('checked', true);
    }
    $(this).toggleClass('allChecked');
});

$(document).on('click', '#listResults tbody input[class=small-chk]', function () {
    var numberOfChecked = $('#listResults tbody input[class="small-chk"]:checked').length;
    var totalCheckboxes = $('#listResults tbody input[class="small-chk"]').length;

    if (numberOfChecked > 0) {
        if (numberOfChecked == totalCheckboxes) {
            $('.all_select').prop('indeterminate', false);
            $('.all_select').prop('checked', true);
            $('.all_select').addClass('allChecked');
        } else {
            if ($('.all_select').hasClass('allChecked')) {
                $('.all_select').removeClass('allChecked');
            }
            $('.all_select').prop('indeterminate', true);
        }
    } else {
        $('.all_select').prop('indeterminate', false);
        $('.all_select').prop('checked', false);
    }
});

$(document).on("click", ".delete_all_link", function (e) {
    $(".delete_all_link").attr("disabled", "disabled");
    e.preventDefault();
    var url = $(this).data('url');
    var searchIDs = [];
    $("#listResults tbody input[class='small-chk']:checked").each(function () {
        searchIDs.push($(this).val());
    });
    if (searchIDs.length > 0) {
        var ids = searchIDs.join();
        bootbox.confirm("Are you sure you want to delete selected records?", function (result) {
            if (result) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    beforeSend: addOverlay,
                    dataType: 'json',
                    data: { action: 'delete_all', ids: ids },
                    success: function (r) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        Custom.myNotification(sType, sText);
                        oTable.draw();
                        setTimeout(function () {
                            $('.all_select').prop('indeterminate', false);
                            $('.all_select').prop('checked', false);
                            if ($('.all_select').hasClass('allChecked')) {
                                $('.all_select').removeClass('allChecked');
                            }
                        }, 2000);

                    },
                    complete: removeOverlay
                });
            }
            $(".delete_all_link").removeAttr("disabled");
        });
    } else {
        bootbox.alert('please select at-least one record', function () {
            $('.all_select').prop('indeterminate', false);
            $(".delete_all_link").removeAttr("disabled");
        });
    }

});

//end select all and delete records

function unbind_back_portlet(this1) {
    console.log(this1);
    $("#" + this1).unbind('submit');
    back_portlet();
}

function back_portlet() {
    $('.portlet-toggler').toggle();
}

function inner_back_portlet() {
    $('.portlet2-toggler').toggle();
}

function add_btn_overlay() {
    $('button:submit').attr("disabled", true);
}

function remove_btn_overlay() {
    $('button:submit').attr("disabled", false);
}

function addOverlay() {
    $('<div id="overlayDocument"><img src="' + SITE_IMG + 'loading.gif" /></div>').appendTo(document.body);
}

function removeOverlay() {
    $('#overlayDocument').remove();
}


/* follow button click starts*/
$(document).ready(function () {
    //genrate dynamic modal when follow button click
    $(document).on('click', '.follow', function () {
        let modelId = $(this).attr('data-model-id');
        let userId = $("#usedId").val();
        let url;
        if (userId > 0) {
            url = SITE_URL + 'account/get_model_by_id';
        } else {
            url = SITE_URL + 'home/get_model_by_id';
        }
        $.ajax({
            method: 'post',
            dataType: "JSON",
            data: { 'userId': modelId },
            url: url,
            success: function (r) {
                //console.log(r.models.follow_price);
                $('#model_name_modal').html(r.models.name);
                $('#model_price').html(r.models.follow_price);

                $('#modelId').val(r.models.id);
                $('#type').val(1);
                $('#price').val(r.models.follow_price);

                $('#image_model_modal').attr('src', SITE_IMG + 'uploads/models/' + r.models.profile_image);
                $("#smallModal").modal();
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });
<<<<<<< HEAD
    
    $(document).on('click', '.subscribe', function () {
                let modelId = $(this).attr('data-model-id');
                $('#modelId5').val(modelId);
                $("#subscribeModel").modal();           
});
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

    //on purchase button click
    $(document).on('click', '#purchase_now', function () {
        let modelId = $(this).parent().find("#modelId").val();

<<<<<<< HEAD
$(document).on('click', '#subscribe', function () {
        let modelId = $(this).parent().find("#modelId5").val();
        let email= $(this).parent().find("#email").val();
        let url;
        
            url = SITE_URL + 'home/subscribe_now';
       
        $.ajax({
            method: 'post',
            dataType: "JSON",
            data: { 'email': email, 'modelId': modelId},
            url: url,
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );



            },
            error: function () {
                console.log('ajax error');
            }
        });
});

$('#subscribeModel').on('show.bs.modal', function(e) {
    //get data-id attribute of the clicked element
    var modelId = $(e.relatedTarget).data('data-model-id');
    //populate the textbox
    $(e.currentTarget).find('input[name="modelId"]').val(modelId);
});


    //on purchase button click
    $(document).on('click', '#purchase_now', function () {
        let modelId = $(this).parent().find("#modelId").val();

        let type = $(this).parent().find("#type").val(); // 0 for token purchase, 1 for follow purchase, 2 for custom video purchase,3 for premium video purchase
        let price = $(this).parent().find("#price").val();
        let postId = 0;
        postId = $(this).parent().find("#post_id").val();
        let userId = $("#usedId").val();

        let url;
        if (userId > 0) {
            url = SITE_URL + 'account/follow_payment';
        } else {
            window.location.href = SITE_URL + "home/login";
        }
        $.ajax({
            method: 'post',
            dataType: "JSON",
            data: { 'type': type, 'modelId': modelId, 'price': price, 'userId': userId, 'post_id': postId },
            url: url,
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );



            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    //on add to favourites button cllick
    $(document).on('click', '#add_fav', function () {
        let postId = $(this).attr('data-post-id');
        // postData = { 'postId': postId };
        $.ajax({
            url: SITE_URL + 'account/post_add_to_fav',
            method: 'post',
            dataType: 'Json',
            data: { 'postId': postId },
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    //remove favourites post on click
    $(document).on('click', '#remove_fav', function () {
        let postId = $(this).attr('data-post-id');
        // postData = { 'postId': postId };
        $.ajax({
            url: SITE_URL + 'account/post_remove_fav',
            method: 'post',
            dataType: 'Json',
            data: { 'postId': postId },
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    //purchase premium videos on click
    $(document).on('click', '.buy_premium_videos', function () {
        let postId = $(this).attr('data-post_id');
        let userId = $("#usedId").val();
        let url;
        if (userId > 0) {
            url = SITE_URL + 'account/get_post_by_id';
        } else {
            url = SITE_URL + 'home/get_post_by_id';
        }

        $.ajax({
            method: 'post',
            dataType: "JSON",
            data: { 'postId': postId },
            url: url,
            success: function (r) {
                //console.log(r.models.follow_price);
                $("#premiumVideoModal").find('#model_name_modal').html(r.models.name);
                $("#premiumVideoModal").find('#model_price').html(r.models.post_price);
                $("#premiumVideoModal").find('#post_id').val(r.models.id);

                //alert(r.models.model_id);
                $("#premiumVideoModal").find('#modelId').val(r.models.model_id);
                $("#premiumVideoModal").find('#type').val(3);
                $("#premiumVideoModal").find('#price').val(r.models.post_price);

                $("#premiumVideoModal").find('#image_model_modal').attr('src', SITE_IMG + 'uploads/models/' + r.models.profile_image);
                $("#premiumVideoModal").modal();
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

=======
        let type = $(this).parent().find("#type").val(); // 0 for token purchase, 1 for follow purchase, 2 for custom video purchase,3 for premium video purchase
        let price = $(this).parent().find("#price").val();
        let postId = 0;
        postId = $(this).parent().find("#post_id").val();
        let userId = $("#usedId").val();

        let url;
        if (userId > 0) {
            url = SITE_URL + 'account/follow_payment';
        } else {
            window.location.href = SITE_URL + "home/login";
        }
        $.ajax({
            method: 'post',
            dataType: "JSON",
            data: { 'type': type, 'modelId': modelId, 'price': price, 'userId': userId, 'post_id': postId },
            url: url,
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );



            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    //on add to favourites button cllick
    $(document).on('click', '#add_fav', function () {
        let postId = $(this).attr('data-post-id');
        // postData = { 'postId': postId };
        $.ajax({
            url: SITE_URL + 'account/post_add_to_fav',
            method: 'post',
            dataType: 'Json',
            data: { 'postId': postId },
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    //remove favourites post on click
    $(document).on('click', '#remove_fav', function () {
        let postId = $(this).attr('data-post-id');
        // postData = { 'postId': postId };
        $.ajax({
            url: SITE_URL + 'account/post_remove_fav',
            method: 'post',
            dataType: 'Json',
            data: { 'postId': postId },
            success: function (r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function () {
                    location.reload();
                }
                );
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    //purchase premium videos on click
    $(document).on('click', '.buy_premium_videos', function () {
        let postId = $(this).attr('data-post_id');
        let userId = $("#usedId").val();
        let url;
        if (userId > 0) {
            url = SITE_URL + 'account/get_post_by_id';
        } else {
            url = SITE_URL + 'home/get_post_by_id';
        }

        $.ajax({
            method: 'post',
            dataType: "JSON",
            data: { 'postId': postId },
            url: url,
            success: function (r) {
                //console.log(r.models.follow_price);
                $("#premiumVideoModal").find('#model_name_modal').html(r.models.name);
                $("#premiumVideoModal").find('#model_price').html(r.models.post_price);
                $("#premiumVideoModal").find('#post_id').val(r.models.id);

                //alert(r.models.model_id);
                $("#premiumVideoModal").find('#modelId').val(r.models.model_id);
                $("#premiumVideoModal").find('#type').val(3);
                $("#premiumVideoModal").find('#price').val(r.models.post_price);

                $("#premiumVideoModal").find('#image_model_modal').attr('src', SITE_IMG + 'uploads/models/' + r.models.profile_image);
                $("#premiumVideoModal").modal();
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
});

/* follow button click ends*/
