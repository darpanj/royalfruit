"use strict";

// Class Definition
var KTLoginGeneral = function () {

    var login = $('#kt_login');

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">' + msg + '</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    // Private Functions

    var displaySignInForm = function () {
        $('#kt_login').removeClass('kt-login--forgot');
        $('#kt_login').removeClass('kt-login--signup');

        $('#kt_login').addClass('kt-login--signin');
        KTUtil.animateClass($('.kt-login__signin')[0], 'flipInX animated');
        //login.find('.kt-login__signin').animateClass('flipInX animated');
    }

    var displayForgotForm = function () {
        $('#kt_login').removeClass('kt-login--signin');
        $('#kt_login').removeClass('kt-login--signup');

        $('#kt_login').addClass('kt-login--forgot');
        //login.find('.kt-login--forgot').animateClass('flipInX animated');
        KTUtil.animateClass($(".kt-login__forgot")[0], 'flipInX animated');

    }

    var handleFormSwitch = function () {
        $('#kt_login_forgot').click(function (e) {
            e.preventDefault();
            displayForgotForm();
        });

        $('#kt_login_forgot_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });
    }


    var handleForgotFormSubmit = function () {
        $('#kt_login_forgot_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    vEmail: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    vEmail: {
                        required: "Email is required."
                    }
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: ADMIN_URL + 'login',
                success: function (response, status, xhr, $form) {
                   var r = JSON.parse(response);
                   console.log(r);
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = $('.kt-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();
                        if (r.status == 200)
                            showErrorMsg(signInForm, 'success', r.message);
                        else
                            showErrorMsg(signInForm, 'danger', r.message);
                    }, 2000);
                }
            });
        });
    }

    // Public Functions
    return {
        // public functions
        init: function () {
            handleFormSwitch();
            handleForgotFormSubmit();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function () {
    KTLoginGeneral.init();
});
