var Custom = function () {

    // private functions & variables

    var dispMessage = function (sType, sText) {
        toastr[sType.toLowerCase()](sText, sType);
    }

    // public functions
    return {
        //main function
        init: function () {
            //initialize here something.            
        },
        //some helper function
        myNotification: function (sType, sText) {
            dispMessage(sType, sText);
        }

    };

}();
$(function () {
    var url = window.location;
    $('.kt-menu__item  a[href="' + url + '"]').parent('li').parent('ul').parent('div').parent('li').addClass('kt-menu__item--active').addClass('kt-menu__item--open');
    $('.kt-menu__item  a[href="' + url + '"]').parent('li').addClass('kt-menu__item--active').addClass('kt-menu__item--open');

    $(document).on('click', '.eye_pass', function () {
        var type = $('.pass_toggle').attr('type');
        if (type == "password") {
            $('.pass_toggle').attr('type', 'text');
        } else {
            $('.pass_toggle').attr('type', 'password');
        }
    });

    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, nospace_msg);

});


//Change record status start
$(document).on('switch-change', '.status-switch', function (event, state) {
    console.log('working');
    $this = $(this);
    bootbox.confirm("Are you sure you want to change status?", function (result) {
        if (result) {
            $this.prop('checked', state.value);
            var customAct = typeof $this.data('getaction') != 'undefined' ? $this.data('getaction') : '';
            var val = state.value ? 'y' : 'n';
            var url = $this.data('url');
            var id = $this.data('id');
            var table = $this.data('table');
            var action = customAct != '' ? customAct : 'change_status';

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: {action: action, table: table, value: val, id: id},
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        } else {
            if (state.value) {
                $this.closest("div").parent("div").removeClass("switch-on");
                $this.closest("div").parent("div").addClass("switch-off");
            } else {
                $this.closest("div").parent("div").removeClass("switch-off");
                $this.closest("div").parent("div").addClass("switch-on");
            }
        }
    });
});
//Change record status end

$(document).on('click', '.btnInnerDelete', function (event, state) {
    var customAct = typeof $(this).data('getaction') != 'undefined' ? $(this).data('getaction') : '';
    var msg = "Are you sure you want to delete this?";
    $this = $(this);
    $this.attr("disabled", "disabled");
    if (customAct == 'delete_campaign_store') {
        msg += " If press ok than cases/sku will be deleted also.";
    }
    bootbox.confirm(msg, function (result) {
        if (result) {
            var url = $this.data('url');
            var id = $this.data('id');
            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: {action: customAct, id: id},
                success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        Custom.myNotification(sType, sText);
                        if (customAct == 'delete_campaign_user')
                            oTableinner.draw();
                        else if (customAct == 'delete_campaign_sku') {
                            oTableinnerSku.draw();
                        } else if (customAct == 'delete_campaign_case') {
                            oTableinnerCases.draw();
                        } else {
                            oTableinnerStore.draw();
                            oTableinnerCases.draw();
                            oTableinnerSku.draw();
                        }
                    } else {
                        sText = r.message;
                        Custom.myNotification('Error', sText);
                    }
                },
                complete: removeOverlay
            });
        }
        $this.removeAttr("disabled");
    });

});

$(document).on('click', '.btnInnerView,.btnInnerEdit', function (e) {
    e.preventDefault();
    var customAct = typeof $(this).data('getaction') != 'undefined' ? $(this).data('getaction') : '';
    var $this = $(this);
    var id = $this.data('id');
    var type = $this.data('type');
    var editLink = $(this).data('url');

    $.ajax({
        url: editLink,
        type: 'POST',
        beforeSend: addOverlay,
        dataType: 'json',
        data: {id: id, type: type, action: customAct},
        success: function (r) {
            if (r.status == 200) {
                $(".pageform2").html(r.html);
                $('.portlet2-toggler').toggle();
                // scrollToElement(".page-content");
                scrollToElement(".kt-container");
            } else {
                sText = r.msg;
                Custom.myNotification('Error', sText);
            }
        },
        complete: removeOverlay
    });
});

//Delete single record start
$(document).on('click', '.btnDelete', function (event, state) {
    $this = $(this);
    $this.attr("disabled", "disabled");
    bootbox.confirm("Are you sure you want to delete this?", function (result) {
        if (result) {

            var url = $this.data('url');
            var id = $this.data('id');

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: {action: 'delete', id: id},
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        }
        $this.removeAttr("disabled");
    });

});

//Approve button
$(document).on('click', '.btnApprove', function (event, state) {
    $this = $(this);
    $this.attr("disabled", "disabled");
    bootbox.confirm("Are you sure you want to Approve this?", function (result) {
        if (result) {

            var url = $this.data('url');
            var id = $this.data('id');

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: {action: 'approve', id: id},
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        }
        $this.removeAttr("disabled");
    });

});

//Complete button
$(document).on('click', '.btnComplete', function (event, state) {
    $this = $(this);
    $this.attr("disabled", "disabled");
    bootbox.confirm("Are you sure you want to Complete this?", function (result) {
        if (result) {

            var url = $this.data('url');
            var id = $this.data('id');

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: {action: 'complete', id: id},
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        }
        $this.removeAttr("disabled");
    });

});

//Cancel button
$(document).on('click', '.btnCancel', function (event, state) {
    $this = $(this);
    $this.attr("disabled", "disabled");
    bootbox.confirm("Are you sure you want to Cancel this?", function (result) {
        if (result) {

            var url = $this.data('url');
            var id = $this.data('id');

            $.ajax({
                url: url,
                type: 'POST',
                beforeSend: addOverlay,
                dataType: 'json',
                data: {action: 'cancel', id: id},
                success: function (r) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    Custom.myNotification(sType, sText);
                    oTable.draw();
                },
                complete: removeOverlay
            });
        }
        $this.removeAttr("disabled");
    });

});

//Delete single record end


function getStatusText(code) {
    sText = "";
    if (code !== undefined) {
        switch (code) {
            case 200: {
                sText = 'Success';
                break;
            }
            case 404: {
                sText = 'Error';
                break;
            }
            case 403: {
                sText = 'Error';
                break;
            }
            case 500: {
                sText = 'Error';
                break;
            }
            default: {
                sText = 'Error';
            }

        }
    }
    return sText;
}

function scrollToElement(e) {
    $('html, body').animate({scrollTop: $(e).offset().top - 100}, 'slow');
}

$(document).on('click', '.btnEdit,.btnView,.btnClone', function (e) {
    e.preventDefault();
    var $this = $(this);
    var id = $this.data('id');
    var type = $this.data('type');
    var editLink = $(this).data('url');

    $.ajax({
        url: editLink,
        type: 'POST',
        beforeSend: addOverlay,
        dataType: 'json',
        data: {id: id, type: type},
        success: function (r) {
            if (r.status == 200) {
                $(".pageform").html(r.html);
                $('.portlet-toggler').toggle();
                // scrollToElement(".page-content");
                scrollToElement(".kt-container");
            } else {
                sText = r.msg;
                Custom.myNotification('Error', sText);
            }
        },
        complete: removeOverlay
    });
});

//Campaign archive/current filteration
$(document).on("change", "#toggle-trigger", function (e) {
    var $this = $(this);
    var url = $(this).data('url');
    typeof $(this).data('getaction') != 'undefined' ? $(this).data('getaction') : '';
    var type = typeof $this.data('type') != 'undefined' ? $this.data('type') : 'normal';
    if (type == 'archive') {
        if ($this.parent().hasClass("off")) {
            bootbox.confirm("Are you sure you want switch ?", function (result) {
                if (result) {
                    window.location.replace(url);
                } else {
                    $this.bootstrapToggle('on');
                }
            });
        }
    } else if (type == 'normal') {
        if (!$this.parent().hasClass("off")) {
            bootbox.confirm("Are you sure you want switch ?", function (result) {
                if (result) {
                    window.location.replace(url);
                } else {
                    $this.bootstrapToggle('off');
                }
            });
        }
    } else if (type == 'country') {
        oTable.destroy();
        if ($this.parent().hasClass("off")) {
            load_country('InActive');
        } else {
            load_country('Active');
        }
    }
});
//Campaign archive/current filteration ends


$(document).on('click', '.btn-toggler', function (e) {
    e.preventDefault();
    $('.portlet-toggler').toggle();
});

//start select all and delete records
$(document).on('click', '.all_select', function () {
    if ($(this).hasClass('allChecked')) {
        $('#listResults tbody input[class="small-chk"]').prop('checked', false);
        $('.all_select').prop('checked', false);
    } else {
        $('#listResults tbody input[class="small-chk"]').prop('checked', true);
        $('.all_select').prop('checked', true);
    }
    $(this).toggleClass('allChecked');
});

$(document).on('click', '#listResults tbody input[class=small-chk]', function () {
    var numberOfChecked = $('#listResults tbody input[class="small-chk"]:checked').length;
    var totalCheckboxes = $('#listResults tbody input[class="small-chk"]').length;

    if (numberOfChecked > 0) {
        if (numberOfChecked == totalCheckboxes) {
            $('.all_select').prop('indeterminate', false);
            $('.all_select').prop('checked', true);
            $('.all_select').addClass('allChecked');
        } else {
            if ($('.all_select').hasClass('allChecked')) {
                $('.all_select').removeClass('allChecked');
            }
            $('.all_select').prop('indeterminate', true);
        }
    } else {
        $('.all_select').prop('indeterminate', false);
        $('.all_select').prop('checked', false);
    }
});

$(document).on("click", ".delete_all_link", function (e) {
    $(".delete_all_link").attr("disabled", "disabled");
    e.preventDefault();
    var url = $(this).data('url');
    var searchIDs = [];
    $("#listResults tbody input[class='small-chk']:checked").each(function () {
        searchIDs.push($(this).val());
    });
    if (searchIDs.length > 0) {
        var ids = searchIDs.join();
        bootbox.confirm("Are you sure you want to delete selected records?", function (result) {
            if (result) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    beforeSend: addOverlay,
                    dataType: 'json',
                    data: {action: 'delete_all', ids: ids},
                    success: function (r) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        Custom.myNotification(sType, sText);
                        oTable.draw();
                        setTimeout(function () {
                            $('.all_select').prop('indeterminate', false);
                            $('.all_select').prop('checked', false);
                            if ($('.all_select').hasClass('allChecked')) {
                                $('.all_select').removeClass('allChecked');
                            }
                        }, 2000);

                    },
                    complete: removeOverlay
                });
            }
            $(".delete_all_link").removeAttr("disabled");
        });
    } else {
        bootbox.alert('please select at-least one record', function () {
            $('.all_select').prop('indeterminate', false);
            $(".delete_all_link").removeAttr("disabled");
        });
    }

});

//end select all and delete records

function image_crop(width, ratio, minX, minY, maxX, maxY) {
    var w1 = width;
    var h1 = (ratio) * w1;
    jQuery(function ($) {
        var jcrop_api, boundx, boundy;
        $('#crop_image').Jcrop({
            boxWidth: width,
            onChange: updateCoords,
            onSelect: updateCoords,
            setSelect: [0, 0, w1, h1],
            minSize: [minX, minY],
            maxSize: [maxX, maxY],
            aspectRatio: ratio
        }, function () {
            // Use the API to get the real image size
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];
            // Store the API in the jcrop_api variable
            jcrop_api = this;
            // Move the preview into the jcrop container for css positioning      
        });

        function updateCoords(c) {
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);
        }
        ;

    });
}

function web_email_status(module, email_status, id) {
    $.post(SITE_URL + 'user/web_email_status', {module: module, email_status: email_status, id: id}, function (r) {
        sType = getStatusText(r.status);
        sText = r.message;
        Custom.myNotification(sType, sText);
    }, 'json');
}

function web_delete(module, id) {
    $.post(SITE_URL + 'user/web_delete', {module: module, id: id}, function (r) {

        if (r.status == 200) {
            $('#row_' + id).remove();
        }
        sType = getStatusText(r.status);
        sText = r.message;
        Custom.myNotification(sType, sText);
    }, 'json');
}

function web_finding(id, action, url) {
    $.post(SITE_URL + url, {action: action, id: id}, function (r) {
        $('.portlet-toggler.pageform').html(r.html);
        $('.portlet-toggler').toggle();
        return false;
    }, 'json');

}

function unbind_back_portlet(this1) {
    console.log(this1);
    $("#" + this1).unbind('submit');
    back_portlet();
}

function back_portlet() {
    $('.portlet-toggler').toggle();
}

function inner_back_portlet() {
    $('.portlet2-toggler').toggle();
}

function add_btn_overlay() {
    $('button:submit').attr("disabled", true);
}

function remove_btn_overlay() {
    $('button:submit').attr("disabled", false);
}

function addOverlay() {
    $('<div id="overlayDocument"><img src="' + SITE_IMG + 'loading.gif" /></div>').appendTo(document.body);
}

function removeOverlay() {
    $('#overlayDocument').remove();
}

var showErrorMsg = function (form, type, msg) {
    var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">' + msg + '</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

    form.find('.alert').remove();
    alert.prependTo(form);
    //alert.animateClass('fadeIn animated');
    KTUtil.animateClass(alert[0], 'fadeIn animated');
    alert.find('span').html(msg);
}