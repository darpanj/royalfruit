var Login = function () {

    var handleLogin = function () {
        $('#kt_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('#form_login');

            form.validate({
                rules: {
                    vEmail: {
                        required: true,
                    },
                    vPassword: {
                        required: true
                    }
                },
                messages: {
                    vEmail: {
                        required: username_req
                    },
                    vPassword: {
                        required: pass_req
                    }
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: ADMIN_URL + 'login',
                success: function (r, status, xhr, $form) {
                    r = JSON.parse(r);
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        if (r.status == 200) {
                            window.location.href = ADMIN_URL + 'dashboard';
                        } else {
                            showErrorMsg(form, 'danger', r.message);
                        }
                    }, 2000);
                }
            });
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
//                var btn = $(this);
//                var form = $(this).closest('form');
//                form.validate({
//                    rules: {
//                        vEmail: {
//                            required: true,
//                        },
//                        vPassword: {
//                            required: true
//                        }
//                    },
//                    messages: {
//                        vEmail: {
//                            required: username_req
//                        },
//                        vPassword: {
//                            required: pass_req
//                        }
//                    },
//                });
//
//                if (!form.valid()) {
//                    return;
//                }
//                btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
//
//                form.ajaxSubmit({
//                    url: ADMIN_URL + 'login',
//                    success: function (r, status, xhr, $form) {
//                        r = JSON.parse(r);
//                        // similate 2s delay
//                        setTimeout(function () {
//                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
//                            if (r.status == 200) {
//                                window.location.href = ADMIN_URL + 'dashboard';
//                            } else {
//                                showErrorMsg(form, 'danger', r.message);
//                            }
//                        }, 2000);
//                    }
//                });
                $('#kt_login_signin_submit').click();
            }
        });
    }





    return {
        //main function to initiate the module
        init: function () {

            handleLogin();

        }

    };

}();