-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2020 at 10:21 PM
-- Server version: 5.6.48-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zestbrai_royal_fruit`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_content`
--

CREATE TABLE `mst_content` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `sys_flag` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_content`
--

INSERT INTO `mst_content` (`id`, `sys_flag`, `title`, `content`, `created`, `updated`) VALUES
(2, 'usc', '18 U.S.C 2257', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:12:29', '2020-05-21 06:15:39'),
(3, 'copyright', 'Copyright', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><figure class=\"table\"><table><tbody><tr><td>1</td><td>1</td></tr><tr><td>1</td><td>1</td></tr></tbody></table></figure><p><strong>nice to see you Again</strong></p><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:20:19', '2020-05-21 06:15:44'),
(4, 'privacypolicy', 'Privacypolicy', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><figure class=\"table\"><table><tbody><tr><td>1</td><td>1</td></tr><tr><td>1</td><td>1</td></tr></tbody></table></figure><p><strong>nice to see you Again</strong></p><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:20:19', '2020-05-21 06:15:53'),
(5, 'californiaprivacypolicy', 'California Privacypolicy', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><figure class=\"table\"><table><tbody><tr><td>1</td><td>1</td></tr><tr><td>1</td><td>1</td></tr></tbody></table></figure><p><strong>nice to see you Again</strong></p><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:20:19', '2020-05-21 06:16:07'),
(6, 'termsandconditions', 'Terms and Conditions', '<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2><figure class=\"table\"><table><tbody><tr><td>1</td><td>1</td></tr><tr><td>1</td><td>1</td></tr></tbody></table></figure><p><strong>nice to see you Again</strong></p><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipiscin] velit, sed quia non numquam do eius modi tempora incididunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p><h2>Vero eos et accusamus et iusto odio dignissimos ducimus</h2><p>Qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><p>Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Qua temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae pondere ad lineam. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p><p>Quibus ego assentior, dum modo de iisdem rebus ne Graecos quidem legendos putent. Res vero bonaa verbis electis graviter omateque dictas quis i legat? Nisi qui se plane Graeciun dici velit, ut a 9 Scaeiola est praetore salutatus Athenis Albucius. Quem quidem locum cum multa venustate et omm sale idem Lucilius, apud quem praeclare Scaevola.</p><p>Qui autem alia matunt scribi a nobis, aequi esse debent, quod et seripta multa sunt, sic ut plura nemini e nostris, et scribentur fortasse plura si vita suppetet; et tamen qui diligenter haec quae de philosophia Htteris mandamus legere assueverit, iudicabit nulla ad legendum his esse potiora.</p><h3>Tempore intellegi convenire</h3><p>Epicurus autem, in quibus sequitur Democritum, noil fere labitur, Quam- quam utriusque cum mutta non prolx). turn illiid in priniis, quoJ, cum in rerum nalura duo quaerenda sint, ununi quae materia sit ex qua quaeque res cfficiatur, alterum quae vis sit quae quidque efficiat, de materia disserucrunt, vim et causam efficiendi reliquerunt. Sed lioc commune vitiuni; illae Epicur propriae ruinae: censet enim eadem ilia indlvidua e solida corpora ferri deorsum suo pondere ad lineam i hunc naturalem esse omnium corporum motuni.</p><p>Deinde ibidem homo acutus, cam illud occorreret, j omnia deorsum e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere, itaque attulit rem commenticiam.</p><p>Declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita eifici complexiones et copulationes et adhaesiones atomorum inter se, ex quo eificeretur mundus omnesque partes mundi quaeque in eo essent. Quae cum res tota fieta sit piieriliter, turn ne efficit quidem^ quod vult. Nam et ipsa declinatio ad libidinem fiiigitur - ait enim deelinare atomum sine causa, quo nibil turpius physico quam fieri.</p>', '2020-01-20 07:20:19', '2020-05-21 06:16:21');

-- --------------------------------------------------------

--
-- Table structure for table `mst_email_templates`
--

CREATE TABLE `mst_email_templates` (
  `iEmailId` int(1) UNSIGNED NOT NULL,
  `vSysFlag` varchar(50) CHARACTER SET utf8 NOT NULL,
  `vTitle` varchar(100) CHARACTER SET utf8 NOT NULL,
  `vDescription` text CHARACTER SET utf8 NOT NULL,
  `vFromEmail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `vFromName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `eEmailType` enum('user','admin') CHARACTER SET utf8 NOT NULL DEFAULT 'user',
  `vSubject` varchar(200) CHARACTER SET utf8 NOT NULL,
  `vTemplate` text CHARACTER SET utf8 NOT NULL,
  `dUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `eIsactive` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_email_templates`
--

INSERT INTO `mst_email_templates` (`iEmailId`, `vSysFlag`, `vTitle`, `vDescription`, `vFromEmail`, `vFromName`, `eEmailType`, `vSubject`, `vTemplate`, `dUpdatedDate`, `eIsactive`) VALUES
(1, 'user_register', 'Confirmation email', 'Registration confirmation email', 'mail.zestbrains4u.site', 'AJAR', 'user', '{{SITENAME}} Team', '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\r\n<title></title>\r\n<style type=\"text/css\">body {\r\n    margin: 0;\r\n    padding: 0;\r\n    background: #ccc;\r\n    font-size: 15px;\r\n    line-height: normal;\r\n    color: #000;\r\n    font-family: arial;\r\n  }\r\n</style>\r\n<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td align=\"center\">\r\n			<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"background: #fff;\" width=\"640\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:15px 45px; background-color:#e85039;\" width=\"540\"><a href=\"{{SITE_URL}}\" style=\"display: inline-block;\" target=\"_blank\"><img alt=\"{{SITENAME}}\" src=\"{{SITE_LOGO}}\" style=\"width: 150px;\" /> </a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 50px 45px 25px 45px; font-family: arial; font-size: 15px; color: #333; line-height: normal;\" width=\"550\">Dear <strong>{{GREETINGS}},</strong></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">Thank You for register account on {{SITENAME}}.</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px;\" width=\"550\"><a href=\"{{ACTIVATION_LINK}}\" style=\"font-size: 14px; font-family: arial; color: #e85039; font-weight: bold; text-decoration: none; line-height: normal;\">Click here to active your account</a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 25px 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">Thank you,kind regards,</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 50px; font-family: arial; font-size: 14px; font-weight: bold; color: #000; line-height: normal;\" width=\"550\">The {{SITENAME}} Team</td>\r\n					</tr>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:20px 45px; font-family: arial; background:#e85039;color: #fff; font-size:15px; margin:0;\" width=\"550\">{{FOOTER_TEXT}}</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '2020-01-21 11:51:27', 'y'),
(2, 'forgot_password', 'Forgot password', 'Email with reset password link', 'mail.zestbrains4u.site', 'AJAR', 'user', 'You requested a new password to your {{SITENAME}} account', '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\r\n<title></title>\r\n<style type=\"text/css\">body {\r\n    margin: 0;\r\n    padding: 0;\r\n    background: #ccc;\r\n    font-size: 15px;\r\n    line-height: normal;\r\n    color: #000;\r\n    font-family: arial;\r\n  }\r\n</style>\r\n<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td align=\"center\">\r\n			<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"background: #fff;\" width=\"640\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:15px 45px; background-color:#5454D9;\" width=\"540\"><a href=\"{{SITE_URL}}\" style=\"display: inline-block;\" target=\"_blank\"><img alt=\"{{SITENAME}}\" src=\"{{SITE_LOGO}}\" style=\"width: 150px;\" /> </a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 50px 45px 25px 45px; font-family: arial; font-size: 15px; color: #333; line-height: normal;\" width=\"550\">Dear <strong>{{GREETINGS}},</strong></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">You Recently Request to Reset Your Password at {{SITENAME}} Account.</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px;\" width=\"550\"><a href=\"{{RESET_PASS_LINK}}\" style=\"font-size: 14px; font-family: arial; color: #e85039; font-weight: bold; text-decoration: none; line-height: normal;\">Click Hear to Reset Your Password</a></td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 25px 45px 5px; font-family: arial; font-size: 14px; color: #333; line-height: normal;\" width=\"550\">Thank you,kind regards,</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding: 0 45px 50px; font-family: arial; font-size: 14px; font-weight: bold; color: #000; line-height: normal;\" width=\"550\">The {{SITENAME}} Team</td>\r\n					</tr>\r\n					<tr>\r\n						<td align=\"center\" style=\"padding:20px 45px; font-family: arial; background:#5454D9;color: #fff; font-size:15px; margin:0;\" width=\"550\">{{FOOTER_TEXT}}</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '2020-01-21 11:51:31', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `mst_imagethumb`
--

CREATE TABLE `mst_imagethumb` (
  `iThumbId` int(1) UNSIGNED NOT NULL,
  `vFolder` varchar(50) CHARACTER SET utf8 NOT NULL,
  `iHeight` int(1) UNSIGNED NOT NULL,
  `iWidth` int(1) UNSIGNED NOT NULL,
  `vDefaultImage` varchar(100) CHARACTER SET utf8 NOT NULL,
  `vIsactive` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_imagethumb`
--

INSERT INTO `mst_imagethumb` (`iThumbId`, `vFolder`, `iHeight`, `iWidth`, `vDefaultImage`, `vIsactive`, `comments`) VALUES
(1, 'users', 150, 150, 'no_image.png', 'y', ''),
(2, 'models', 40, 40, 'no_image.png', 'y', 'for_profile_pic_of_models'),
(3, 'models', 175, 336, 'no_image.png', 'y', 'for_cover_pic_of_models'),
(4, 'models', 175, 336, 'no_image.png', 'y', 'for_streamer_pic_of_models'),
(5, 'models', 100, 100, 'no_image.png', 'y', 'avatar_pic_of_models_in_edit_profile'),
(6, 'models', 399, 993, 'no_image.png', 'y', 'header_pic_of_models_in_edit_profile'),
(7, 'models', 399, 993, 'no_image.png', 'y', 'stream_pic_of_models_in_edit_profile'),
(8, 'shop', 399, 993, 'no_image.png', 'y', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_lang`
--

CREATE TABLE `mst_lang` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_code` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_lang`
--

INSERT INTO `mst_lang` (`id`, `name`, `short_code`, `currency`, `status`, `insert_date`, `update_date`) VALUES
(1, 'English', 'en', 'USD', 'active', '2019-03-16 00:00:00', '2019-03-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mst_module`
--

CREATE TABLE `mst_module` (
  `id` int(11) NOT NULL,
  `mod_modulegroupcode` varchar(25) NOT NULL,
  `mod_modulegroupname` varchar(50) NOT NULL,
  `mod_modulegroupname_lang` varchar(255) NOT NULL,
  `mod_modulecode` varchar(25) NOT NULL,
  `mod_modulename` varchar(50) NOT NULL,
  `mod_modulename_lang` varchar(255) NOT NULL,
  `mod_modulegrouporder` int(3) NOT NULL,
  `mod_moduleorder` int(3) NOT NULL,
  `mod_modulepagename` varchar(255) NOT NULL,
  `vImage` varchar(255) NOT NULL,
  `display` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_module`
--

INSERT INTO `mst_module` (`id`, `mod_modulegroupcode`, `mod_modulegroupname`, `mod_modulegroupname_lang`, `mod_modulecode`, `mod_modulename`, `mod_modulename_lang`, `mod_modulegrouporder`, `mod_moduleorder`, `mod_modulepagename`, `vImage`, `display`) VALUES
(1, 'DASHBOARD', 'Dashboard', '', 'DASHBOARD', 'Dashboard', '', 1, 1, 'dashboard', 'kt-menu__link-icon flaticon2-architecture-and-city', 1),
(2, 'GEN_SET', 'General Settings', '', 'GEN_SET', 'General Settings', '', 100, 1, 'setting', 'kt-menu__link-icon fas fa-cogs', 1),
(3, 'GEN_SET', 'General Settings', '', 'SITE_SETTINGS', 'Site Settings', '', 100, 2, 'setting', 'kt-menu__link-icon', 1),
(4, 'GEN_SET', 'General Settings', '', 'CHANGE_PASS', 'Change Password', '', 100, 3, 'cpass', 'kt-menu__link-icon', 1),
(5, 'USERS', 'Users', '', 'USERS', 'Users', '', 2, 1, 'Users', 'fa fa-users', 1),
(6, 'MODELS', 'Models', '', 'MODELS', 'Models', '', 2, 1, 'ActiveModels', 'fa fa-users', 1),
(9, 'MODELS', 'Models', '', 'ACTIVE_MODELS', 'Active Models', '', 2, 1, 'ActiveModels', 'fa fa-users', 1),
(10, 'GEN_SET', 'General Settings', '', 'Content', 'Content', '', 100, 4, 'Content', 'fa fa-cogs', 1),
(11, 'PAYMENT', 'Payment', '', 'PAYMENT', 'Payment', '', 101, 1, 'payment', 'kt-menu__link-icon fas fa-cogs', 1),
(12, 'PAYMENT', 'Follower Purchase', '', 'FOLLOWER_PURCHASE', 'Follower Purchase', '', 101, 2, 'follower_purchase', 'kt-menu__link-icon fas fa-cogs', 1),
(13, 'PAYMENT', 'Custom Video Purchase', '', 'CUSTOM_VIDEO_PURCHASE', 'Custom Video Purchase', '', 101, 3, 'custom_video_purchase', 'kt-menu__link-icon fas fa-cogs', 1),
(14, 'PAYMENT', 'Premium Video Purchase', '', 'PREMIUM_VIDEO_PURCHASE', 'Premium Video Purchase', '', 101, 4, 'premium_video_purchase', 'kt-menu__link-icon fas fa-cogs', 1),
(15, 'PAYMENT', 'Token Purchase', '', 'TOKEN_PURCHASE', 'Token Purchase', '', 101, 5, 'token_purchase', 'kt-menu__link-icon fas fa-cogs', 1),
(16, 'PAYMENT', 'Snapchat Video Purchase', '', 'SNAPCHAT_VIDEO_PURCHASE', 'Snapchat Video Purchase', '', 101, 6, 'snapchat_video_purchase', 'kt-menu__link-icon fas fa-cogs', 1),
(17, 'GEN_SET', 'Getting Started', '', 'GETTING_STARTED', 'Getting Started', '', 100, 5, 'getting_started', 'fa fa-cogs', 1),
(18, 'GEN_SET', 'How to Make Money', '', 'MAKE_MONEY', 'How to Make Money', '', 100, 6, 'make_money', 'fa fa-cogs', 1),
(19, 'GEO_BLOCKING', 'Geo Blocking', '', 'GEO_BLOCKING', 'Geo Blocking', '', 3, 1, 'geo_blocking', 'fa fa-map-marker', 1),
(20, 'MODEL_POST', 'Model Post', '', 'MODEL_POST', 'Model Post', '', 4, 1, 'model_post', 'kt-menu__link-icon fas fa-cogs', 1),
(21, 'MODEL_POST', 'Images', '', 'IMAGES', 'Images', '', 4, 2, 'post_images', 'kt-menu__link-icon fas fa-cogs', 1),
(22, 'MODEL_POST', 'Videos', '', 'VIDEOS', 'Videos', '', 4, 2, 'post_videos', 'kt-menu__link-icon fas fa-cogs', 1),
(23, 'GEN_SET', 'Documents', '', 'DOCUMENTS', 'Documents', '', 100, 7, 'documents', 'fa fa-cogs', 1),
(24, 'MODELS', 'Models', '', 'PENDING_APPROVAL', 'Pending Approval', '', 2, 2, 'pending_approval', 'fa fa-users', 1),
(25, 'SHOP', 'Shop', '', 'SHOP', 'Shop', '', 3, 1, 'shop', 'fa fa-cart-plus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_role_rights`
--

CREATE TABLE `mst_role_rights` (
  `id` int(11) NOT NULL,
  `rr_rolecode` varchar(255) NOT NULL,
  `rr_modulecode` varchar(255) NOT NULL,
  `rr_create` enum('yes','no') NOT NULL,
  `rr_edit` enum('yes','no') NOT NULL,
  `rr_delete` enum('yes','no') NOT NULL,
  `rr_view` enum('yes','no') NOT NULL,
  `rr_status` enum('yes','no') NOT NULL,
  `extra` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_role_rights`
--

INSERT INTO `mst_role_rights` (`id`, `rr_rolecode`, `rr_modulecode`, `rr_create`, `rr_edit`, `rr_delete`, `rr_view`, `rr_status`, `extra`) VALUES
(1, 'admin', 'DASHBOARD', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(2, 'admin', 'GEN_SET', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(3, 'admin', 'SITE_SETTINGS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(4, 'admin', 'CHANGE_PASS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(5, 'admin', 'USERS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(6, 'admin', 'MODELS', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(7, 'admin', 'ACTIVE_MODELS', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(8, 'admin', 'Content', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(9, 'admin', 'PAYMENT', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(10, 'admin', 'FOLLOWER_PURCHASE', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(11, 'admin', 'CUSTOM_VIDEO_PURCHASE', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(12, 'admin', 'PREMIUM_VIDEO_PURCHASE', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(13, 'admin', 'TOKEN_PURCHASE', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(14, 'admin', 'SNAPCHAT_VIDEO_PURCHASE', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(15, 'admin', 'GETTING_STARTED', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(16, 'admin', 'MAKE_MONEY', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(17, 'admin', 'GEO_BLOCKING', 'no', 'no', 'yes', 'no', 'no', ''),
(18, 'admin', 'MODEL_POST', 'yes', 'yes', 'yes', 'yes', 'yes', ''),
(19, 'admin', 'IMAGES', 'no', 'no', 'yes', 'no', 'yes', 'yes'),
(20, 'admin', 'VIDEOS', 'no', 'no', 'yes', 'no', 'yes', 'yes'),
(21, 'admin', 'DOCUMENTS', 'no', 'yes', 'yes', 'yes', 'yes', ''),
(22, 'admin', 'PENDING_APPROVAL', 'no', 'no', 'yes', 'yes', 'yes', 'yes'),
(23, 'admin', 'SHOP', 'yes', 'yes', 'yes', 'yes', 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_sitesettings`
--

CREATE TABLE `mst_sitesettings` (
  `iFieldId` int(1) UNSIGNED NOT NULL,
  `vLabel` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `vLabelLang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `vType` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `vConstant` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `vOptions` text CHARACTER SET utf8 NOT NULL,
  `vClass` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `eRequired` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `vValue` text CHARACTER SET utf8 NOT NULL,
  `vHint` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `eEditable` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `eStatus` enum('y','n') CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `dUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_sitesettings`
--

INSERT INTO `mst_sitesettings` (`iFieldId`, `vLabel`, `vLabelLang`, `vType`, `vConstant`, `vOptions`, `vClass`, `eRequired`, `vValue`, `vHint`, `eEditable`, `eStatus`, `dUpdatedDate`) VALUES
(1, 'Site Name', '', 'textbox', 'SITENAME', '', NULL, 'y', 'ROYAL FRUIT', 'Site name which you want to display', 'y', 'y', '2020-02-18 05:43:22'),
(2, 'Site Logo', '', 'file', 'SITE_LOGO', '', 'img_class', '', 'logo.png', 'Site logo which you want to display', 'n', 'y', '2018-08-31 11:18:46'),
(3, 'Admin email', '', 'textbox', 'ADMIN_EMAIL', '', 'email_class', 'y', 'darpanj.zestbrains@gmail.com', NULL, 'y', 'y', '2020-02-18 05:43:33'),
(4, 'Footer Text', '', 'textbox', 'FOOTER_TEXT', '', NULL, 'y', 'Copyright  © {{YEAR}}  RoyalFruit. All Rights Reserved', NULL, 'y', 'y', '2020-02-18 05:44:21'),
(5, 'Author', '', 'textbox', 'SITE_AUTHOR', '', NULL, 'y', 'ROYALFRUIT', NULL, 'y', 'y', '2020-02-18 05:44:36'),
(6, 'Instagram', '', 'textbox', 'INSTAGRAM', '', NULL, 'y', 'https://www.instagram.com', NULL, 'y', 'y', '2020-06-03 15:26:39'),
(7, 'Twitter', '', 'textbox', 'TWITTER', '', NULL, 'y', 'https://www.twitter.com', NULL, 'y', 'y', '2020-06-03 15:26:39'),
(8, 'Facebook', '', 'textbox', 'FACEBOOK', '', NULL, 'y', 'https://www.facebook.com', NULL, 'y', 'y', '2020-06-03 15:26:39'),
(9, 'DMCA EMAIL', '', 'textbox', 'DMCA_EMAIL', '', 'email_class', 'y', 'dmca@royalfruit.com', NULL, 'y', 'y', '2020-02-18 18:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agreements`
--

CREATE TABLE `tbl_agreements` (
  `id` int(11) NOT NULL,
  `agreement_model` text NOT NULL,
  `agreement_2257` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_agreements`
--

INSERT INTO `tbl_agreements` (`id`, `agreement_model`, `agreement_2257`, `status`, `created_at`, `updated_at`) VALUES
(1, 'e8acd5504d801010ac5a5ab46f719462.png', 'ceb1f773ef2ce22c6eaf193bb908fecc.png', 'active', '2020-06-04 00:00:00', '2020-06-18 11:32:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blocked_fans`
--

CREATE TABLE `tbl_blocked_fans` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `blocked_fan_id` int(11) NOT NULL,
  `blocked_fan_type` enum('user','model') NOT NULL DEFAULT 'user',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blocked_fans`
--

INSERT INTO `tbl_blocked_fans` (`id`, `model_id`, `blocked_fan_id`, `blocked_fan_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 2, 'user', 'active', '2020-06-18 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_custom_video_requests`
--

CREATE TABLE `tbl_custom_video_requests` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `request_sender_id` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `instruction` text NOT NULL,
  `payment_id` text NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_model_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not deleted, 1: deleted',
  `is_user_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not deleted, 1: deleted',
  `request_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: requested, 1: accepted, 2: declined',
  `declined_reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_custom_video_requests`
--

INSERT INTO `tbl_custom_video_requests` (`id`, `model_id`, `request_sender_id`, `price`, `instruction`, `payment_id`, `status`, `created_at`, `updated_at`, `is_model_deleted`, `is_user_deleted`, `request_status`, `declined_reason`) VALUES
(1, 12, 2, 122.00, 'Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text. Lorem ipsum is simply dummy text.', 'Payment_66072', 'active', '2020-05-19 12:01:15', '0000-00-00 00:00:00', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_friend_requests`
--

CREATE TABLE `tbl_friend_requests` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_status` int(11) NOT NULL DEFAULT '0' COMMENT '0: requested, 1: accepted, 2: declined, 3: canceled',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_friend_requests`
--

INSERT INTO `tbl_friend_requests` (`id`, `model_id`, `user_id`, `request_status`, `status`, `created_at`, `updated_at`) VALUES
(1, 11, 12, 1, 'active', '2020-05-30 15:49:43', '2020-05-30 18:59:00'),
(2, 10, 12, 0, 'active', '2020-05-30 15:51:10', '0000-00-00 00:00:00'),
(3, 9, 12, 0, 'active', '2020-06-15 05:29:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_geo_blocking`
--

CREATE TABLE `tbl_geo_blocking` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `country` text CHARACTER SET utf8 NOT NULL,
  `state` text CHARACTER SET utf8 NOT NULL,
  `city` text CHARACTER SET utf8 NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_getting_started`
--

CREATE TABLE `tbl_getting_started` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `pdf` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_getting_started`
--

INSERT INTO `tbl_getting_started` (`id`, `image`, `pdf`, `status`, `created_at`, `updated_at`) VALUES
(1, '8e35f715ac40f19cefcfd29e07c74c1b.png', '8613d35b54a05e365f8249f48ab345c6.pdf', 'active', '2020-06-04 00:00:00', '2020-06-04 13:06:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_make_money`
--

CREATE TABLE `tbl_make_money` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `pdf` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_make_money`
--

INSERT INTO `tbl_make_money` (`id`, `image`, `pdf`, `status`, `created_at`, `updated_at`) VALUES
(1, '8d9f806d2dd25c59c4f029ba6da30026.png', '5f9a965b0e2e2a07e24d08fc85f6ae12.pdf', 'active', '2020-06-04 00:00:00', '2020-06-04 13:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `id` int(11) NOT NULL,
  `message_thread_id` int(11) NOT NULL,
  `model_follower_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_text` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`id`, `message_thread_id`, `model_follower_id`, `sender_id`, `receiver_id`, `message_text`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 12, 2, 'Hello', 'active', '2020-04-16 02:00:00', '2020-04-16 00:00:00'),
(2, 1, 2, 2, 12, 'Hello Wolansys.', 'active', '2020-04-16 04:00:00', '2020-04-16 00:00:00'),
(3, 1, 2, 12, 2, 'How are you Matthew?', 'active', '2020-04-16 04:02:00', '2020-04-16 00:00:00'),
(4, 1, 2, 2, 12, 'I am fine as always.', 'active', '2020-04-16 04:03:00', '2020-04-16 00:00:00'),
(5, 1, 2, 2, 12, 'What about you?', 'active', '2020-04-16 04:03:00', '2020-04-16 00:00:00'),
(6, 1, 2, 12, 2, 'I am well.', 'active', '2020-04-16 04:06:00', '2020-04-16 00:00:00'),
(7, 1, 2, 12, 2, '635b76fc4d703373cfa2ee5e843fe378.jpeg', 'active', '2020-04-16 04:07:00', '2020-04-16 00:00:00'),
(8, 1, 2, 2, 12, 'Hahaha', 'active', '2020-04-16 04:08:00', '2020-04-16 00:00:00'),
(9, 1, 2, 2, 12, '9c55f411ef51b97c684194bee46b2c72.jpeg', 'active', '2020-04-16 04:08:00', '2020-04-16 00:00:00'),
(10, 1, 2, 12, 2, '0e6c777522c6b2d093e0ad768fa52efe.jpeg', 'active', '2020-04-16 04:08:00', '2020-04-16 00:00:00'),
(11, 1, 2, 2, 12, 'eeb4b05deb1b4abc36e8f34bf09dfe23.jpeg', 'active', '2020-04-16 13:47:16', '0000-00-00 00:00:00'),
(12, 1, 2, 2, 12, 'I am uploading the sample pictures. HAHAHA', 'active', '2020-04-16 13:47:16', '0000-00-00 00:00:00'),
(13, 1, 2, 12, 2, 'It\'s funny. HAHAHA', 'active', '2020-04-16 13:48:40', '0000-00-00 00:00:00'),
(14, 1, 2, 2, 12, 'Yes...', 'active', '2020-04-16 13:51:27', '0000-00-00 00:00:00'),
(15, 1, 2, 2, 12, 'We will gonna rock today in this game.', 'active', '2020-04-16 14:17:23', '0000-00-00 00:00:00'),
(16, 1, 2, 12, 2, 'Why not?', 'active', '2020-04-16 14:19:45', '0000-00-00 00:00:00'),
(17, 1, 2, 12, 2, 'For sure.', 'active', '2020-04-16 14:22:13', '0000-00-00 00:00:00'),
(18, 1, 2, 2, 12, '0f212a28c88160dbb0a53286ab187bce.jpeg', 'active', '2020-04-16 14:26:36', '0000-00-00 00:00:00'),
(19, 1, 2, 12, 2, '24b65bda6c38086f56f8ef031ef87de2.jpeg', 'active', '2020-04-16 14:30:06', '0000-00-00 00:00:00'),
(20, 1, 2, 2, 12, 'Hello, Good Morning buddy...', 'active', '2020-04-17 07:04:42', '0000-00-00 00:00:00'),
(21, 1, 2, 12, 2, 'Very Good Morning', 'active', '2020-04-17 07:05:38', '0000-00-00 00:00:00'),
(22, 1, 2, 2, 12, 'Good Morning Buddy', 'active', '2020-04-18 06:44:38', '0000-00-00 00:00:00'),
(23, 1, 2, 12, 2, 'Very Good Morning', 'active', '2020-04-18 06:44:51', '0000-00-00 00:00:00'),
(24, 2, 34, 7, 5, 'Hello', 'active', '2020-04-20 11:08:25', '0000-00-00 00:00:00'),
(25, 2, 34, 5, 7, 'Hello...!!', 'active', '2020-04-20 11:08:37', '0000-00-00 00:00:00'),
(26, 3, 3, 6, 12, 'Hello', 'active', '2020-05-20 12:19:16', '0000-00-00 00:00:00'),
(27, 3, 3, 12, 6, 'Hello', 'active', '2020-05-20 12:19:50', '0000-00-00 00:00:00'),
(28, 1, 0, 2, 12, 'How are you?', 'active', '2020-06-12 14:02:54', '0000-00-00 00:00:00'),
(29, 1, 0, 2, 12, '...', 'active', '2020-06-12 14:03:13', '0000-00-00 00:00:00'),
(30, 1, 0, 2, 12, 'hello', 'active', '2020-06-24 07:34:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message_threads`
--

CREATE TABLE `tbl_message_threads` (
  `id` int(11) NOT NULL,
  `chat_user_one` int(11) NOT NULL,
  `chat_user_two` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1: subscribers, 2: models',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message_threads`
--

INSERT INTO `tbl_message_threads` (`id`, `chat_user_one`, `chat_user_two`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 2, 1, 'active', '2020-06-11 00:00:00', '0000-00-00 00:00:00'),
(2, 7, 5, 1, 'active', '2020-06-11 00:00:00', '0000-00-00 00:00:00'),
(3, 6, 12, 1, 'active', '2020-06-11 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_extra`
--

CREATE TABLE `tbl_model_extra` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `rules` text NOT NULL,
  `follow_price` int(10) NOT NULL COMMENT 'usd',
  `snapchat_price` int(10) NOT NULL COMMENT '''usd''',
  `model_payment_details_id` int(11) NOT NULL DEFAULT '0',
  `is_allow_coustom_video_request` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes',
  `custom_video_price` double(10,2) NOT NULL,
  `custom_video_delivery_time` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `geo_block_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_extra`
--

INSERT INTO `tbl_model_extra` (`id`, `model_id`, `rules`, `follow_price`, `snapchat_price`, `model_payment_details_id`, `is_allow_coustom_video_request`, `custom_video_price`, `custom_video_delivery_time`, `status`, `created_at`, `updated_at`, `geo_block_id`) VALUES
(1, 12, '<div>Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate.&nbsp;</div>', 25, 10, 1, 1, 300.00, 'A day', 'active', '0000-00-00 00:00:00', '2020-06-22 14:41:23', ''),
(2, 3, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 12, 50, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(3, 4, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 20, 55, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(4, 5, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 11, 22, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(5, 6, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 99, 3, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(6, 7, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 12, 30, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(7, 8, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 9, 50, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(8, 9, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 45, 22, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(9, 10, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 10, 30, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(10, 11, 'Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque, convallis montes sollicitudin tincidunt ultricies curabitur litora inceptos congue, netus viverra cursus orci nullam etiam tellus. Nullam magnis vivamus potenti mauris odio risus scelerisque consequat quam suspendisse, ridiculus leo nunc duis per litora metus orci ac ultrices, quis pellentesque cum parturient nec auctor fermentum eget tristique. Et quam eleifend etiam conubia tempor pellentesque sollicitudin ante fermentum euismod vel, suspendisse diam class ullamcorper facilisi cursus ligula sociosqu vulputate. ', 78, 60, 0, 0, 0.00, '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_followers`
--

CREATE TABLE `tbl_model_followers` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `follow_start_date` date NOT NULL,
  `follow_end_date` date NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_followers`
--

INSERT INTO `tbl_model_followers` (`id`, `model_id`, `user_id`, `payment_id`, `status`, `follow_start_date`, `follow_end_date`, `created`) VALUES
(2, 12, 2, 'Payment_59634', 'active', '2020-05-19', '2020-06-19', '2020-05-19 14:56:53'),
(3, 3, 2, 'Payment_67242', 'active', '2020-05-19', '2020-06-19', '2020-05-19 14:57:13'),
(4, 4, 2, 'Payment_47304', 'active', '2020-05-19', '2020-06-19', '2020-05-19 14:57:25'),
(5, 5, 2, 'Payment_75745', 'active', '2020-05-19', '2020-06-19', '2020-05-19 14:57:33'),
(6, 8, 2, 'Payment_91695', 'active', '0000-00-00', '0000-00-00', '2020-05-22 18:32:45'),
(7, 6, 2, 'Payment_20187', 'active', '0000-00-00', '0000-00-00', '2020-05-22 18:32:57'),
(8, 9, 15, 'Payment_79160', 'active', '0000-00-00', '0000-00-00', '2020-08-07 05:37:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_payment_details`
--

CREATE TABLE `tbl_model_payment_details` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `payment_via` tinyint(4) NOT NULL,
  `account_holder` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_type` tinyint(4) NOT NULL,
  `routing_number` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `pay_to` varchar(255) NOT NULL,
  `mailing_address` text NOT NULL,
  `paxum_email_address` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_payment_details`
--

INSERT INTO `tbl_model_payment_details` (`id`, `model_id`, `payment_via`, `account_holder`, `email_address`, `bank_name`, `account_type`, `routing_number`, `account_number`, `pay_to`, `mailing_address`, `paxum_email_address`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 3, '', '', '', 0, '', '', '', '', 'vela@gmail.com', 'active', '2020-05-30 06:37:54', '2020-05-30 06:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_posts`
--

CREATE TABLE `tbl_model_posts` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `category` int(2) NOT NULL DEFAULT '0' COMMENT '0=followers,1=teaser,2=private,3=premium',
  `post_type` int(3) NOT NULL DEFAULT '0' COMMENT '0=''images'',1=''video''',
  `image` text NOT NULL,
  `video_thumb_image` text NOT NULL,
  `video_hint_text` text NOT NULL,
  `post_price` varchar(30) NOT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_posts`
--

INSERT INTO `tbl_model_posts` (`id`, `model_id`, `category`, `post_type`, `image`, `video_thumb_image`, `video_hint_text`, `post_price`, `is_approved`, `status`, `created`, `updated`) VALUES
(2, 3, 0, 0, '3/followers/images/follow_1.jpg', '', '', '', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(3, 4, 0, 0, '4/followers/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(4, 5, 0, 0, '5/followers/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(5, 6, 1, 0, '6/teaser/images/profile.jpg', '', '', '', 0, 'active', '2020-01-26 07:00:00', '2020-02-26 15:54:51'),
(6, 7, 0, 0, '7/followers/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(7, 8, 0, 0, '8/followers/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(8, 9, 1, 0, '9/teaser/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(9, 10, 0, 0, '10/followers/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(10, 11, 0, 0, '11/followers/images/profile.jpg', '', '', '', 0, 'active', '2020-02-26 07:00:00', '2020-02-26 15:54:51'),
(12, 8, 1, 1, '8/teaser/videos/sample.mp4', '', '', '', 0, 'active', '2020-01-30 07:00:00', '2020-02-26 15:54:51'),
(15, 3, 2, 0, '3/followers/images/follow_2.jpeg', '', '', '', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(16, 12, 1, 0, '9/teaser/images/profile.jpg', '', '', '', 1, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(17, 12, 1, 1, '9/teaser/videos/sample.mp4', '', '', '', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(18, 12, 3, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Getting Nude', '70', 0, 'active', '2020-02-25 00:00:00', '2020-02-26 15:54:51'),
(19, 12, 3, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Showing something hot', '80', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(20, 12, 2, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Showing something hot', '80', 0, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(21, 11, 2, 1, '9/teaser/videos/sample.mp4', '11/followers/images/profile.jpg', 'Custom Video is a private', '80', 1, 'active', '2020-02-26 00:00:00', '2020-02-26 15:54:51'),
(22, 12, 1, 0, '12/teaser/images/c10e911e0ce55ca855fd8f42ec24c0ed.png', '', '', '', 0, 'active', '2020-06-22 09:08:37', '2020-06-22 02:08:37'),
(23, 12, 1, 1, '12/teaser/videos/bdf96df6949511210bb46576120ab54e.mp4', '', '', '', 0, 'active', '2020-06-22 09:45:29', '2020-06-22 02:45:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_posts_fav`
--

CREATE TABLE `tbl_model_posts_fav` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_posts_fav`
--

INSERT INTO `tbl_model_posts_fav` (`id`, `post_id`, `user_id`, `created`, `updated`) VALUES
(3, 2, 2, '2020-04-24 16:31:23', '2020-04-24 16:31:23'),
(4, 12, 2, '2020-04-27 10:47:51', '2020-04-27 10:47:51'),
(5, 12, 2, '2020-04-27 10:47:52', '2020-04-27 10:47:52'),
(6, 3, 2, '2020-05-01 05:27:36', '2020-05-01 05:27:36'),
(7, 3, 2, '2020-05-01 05:27:37', '2020-05-01 05:27:37'),
(8, 16, 2, '2020-05-09 02:02:28', '2020-05-09 02:02:28'),
(9, 17, 12, '2020-06-22 09:10:26', '2020-06-22 09:10:26'),
(10, 8, 2, '2020-08-11 03:12:46', '2020-08-11 03:12:46'),
(11, 7, 2, '2020-08-17 22:32:32', '2020-08-17 22:32:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_post_comments`
--

CREATE TABLE `tbl_model_post_comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `commenter_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_post_comments`
--

INSERT INTO `tbl_model_post_comments` (`id`, `post_id`, `commenter_id`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(1, 19, 2, 'Nice video', 'active', '2020-06-09 14:57:05', '0000-00-00 00:00:00'),
(2, 19, 2, 'fsafsf', 'active', '2020-06-16 15:09:11', '0000-00-00 00:00:00'),
(3, 19, 2, 'Hey', 'active', '2020-06-17 12:16:59', '0000-00-00 00:00:00'),
(4, 19, 2, 'Good', 'active', '2020-06-17 14:21:38', '0000-00-00 00:00:00'),
(5, 19, 2, 'hello', 'active', '2020-06-24 07:33:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_subscribers`
--

CREATE TABLE `tbl_model_subscribers` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `email` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_subscribers`
--

INSERT INTO `tbl_model_subscribers` (`id`, `model_id`, `email`, `created`) VALUES
(1, 3, 'ram@gmail.com', '2020-08-17 14:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_videos`
--

CREATE TABLE `tbl_model_videos` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `category` int(2) NOT NULL DEFAULT '0' COMMENT '0=public,1=private',
  `video` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_videos`
--

INSERT INTO `tbl_model_videos` (`id`, `model_id`, `category`, `video`, `created`, `updated`) VALUES
(1, 3, 0, '3/public/videos/prfile.jpg', '2020-02-13 07:00:00', '2020-02-26 17:05:57'),
(2, 4, 0, '4/public/videos/prfile.jpg', '2020-02-26 06:09:00', '2020-02-26 17:05:57'),
(3, 4, 0, '4/public/videos/prfile.jpg', '2020-02-26 06:09:00', '2020-02-26 17:05:57'),
(4, 4, 0, '4/public/videos/prfile.jpg', '2020-02-26 06:09:00', '2020-02-26 17:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_w9form`
--

CREATE TABLE `tbl_model_w9form` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `file_name` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_w9form`
--

INSERT INTO `tbl_model_w9form` (`id`, `model_id`, `file_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 'f089ba5294dd174055d4d2065f33110c.pdf', 'active', '2020-05-30 14:55:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL,
  `read_flag` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_message` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `read_flag`, `user_id`, `notification_message`, `created`, `updated`) VALUES
(1, 0, 2, 'notification 1', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(2, 0, 2, 'notification 2\r\n', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(3, 0, 2, 'notification 3', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(4, 0, 2, 'notification 4\r\n', '2020-04-25 20:12:04', '2020-04-25 20:12:04'),
(5, 0, 9, 'Noelle Mcdonald Followed You.', '2020-08-06 22:37:13', '2020-08-06 22:37:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `email` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `user_id`, `shop_id`, `amount`, `email`, `transaction_id`, `payment_status`, `insert_date`, `update_date`) VALUES
(1, 16, 6, 25, '', 'ch_1HEY8oAOO7ZMIjyQtefSp7I8', 'success', '2020-08-10 10:22:07', '2020-08-10 10:22:07'),
(2, 16, 6, 25, '', 'ch_1HEYKKAOO7ZMIjyQ9T3PwVqp', 'success', '2020-08-10 10:34:01', '2020-08-10 10:34:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `status` text NOT NULL,
  `payment_for` enum('0','1','2','3','4') NOT NULL DEFAULT '0' COMMENT '0=token puchase,1=model follow,2=snapchat purchase,3=private video purchase,4=custom video purchase ',
  `price` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`id`, `payment_id`, `status`, `payment_for`, `price`, `user_id`, `created`) VALUES
(1, 'Payment_93883', 'success', '0', '20', 2, '2020-04-20 11:43:12'),
(2, 'Payment_51670', 'success', '1', '12', 2, '2020-04-24 16:20:22'),
(3, 'Payment_70888', 'success', '1', '20', 2, '2020-04-24 16:27:11'),
(4, 'Payment_14112', 'success', '1', '25', 2, '2020-05-01 15:23:02'),
(5, 'Payment_86391', 'success', '1', '25', 2, '2020-05-01 15:23:11'),
(6, 'Payment_72460', 'success', '1', '25', 2, '2020-05-01 15:23:36'),
(7, 'Payment_49939', 'success', '1', '25', 2, '2020-05-01 15:32:17'),
(8, 'Payment_28797', 'success', '3', '70', 2, '2020-05-01 15:33:55'),
(9, 'Payment_66072', 'success', '3', '122', 2, '2020-05-19 12:01:15'),
(10, 'Payment_31910', 'success', '1', '25', 2, '2020-05-19 14:52:40'),
(11, 'Payment_64499', 'success', '1', '12', 2, '2020-05-19 14:54:08'),
(12, 'Payment_18951', 'success', '1', '25', 2, '2020-05-19 14:55:51'),
(13, 'Payment_54433', 'success', '1', '99', 2, '2020-05-19 14:56:30'),
(14, 'Payment_59634', 'success', '1', '25', 2, '2020-05-19 14:56:53'),
(15, 'Payment_67242', 'success', '1', '12', 2, '2020-05-19 14:57:13'),
(16, 'Payment_47304', 'success', '1', '20', 2, '2020-05-19 14:57:25'),
(17, 'Payment_75745', 'success', '1', '11', 2, '2020-05-19 14:57:33'),
(18, 'Payment_77385', 'success', '', '350', 2, '2020-05-22 13:23:23'),
(19, 'Payment_52225', 'success', '', '250', 2, '2020-05-22 14:59:47'),
(20, 'Payment_95419', 'success', '', '100', 2, '2020-05-22 18:32:07'),
(21, 'Payment_91884', 'success', '', '250', 2, '2020-05-22 18:32:22'),
(22, 'Payment_91695', 'success', '1', '9', 2, '2020-05-22 18:32:45'),
(23, 'Payment_20187', 'success', '1', '99', 2, '2020-05-22 18:32:57'),
(24, 'Payment_10750', 'success', '3', '80', 2, '2020-07-06 09:25:09'),
(25, 'Payment_67010', 'success', '', '180', 2, '2020-06-24 07:33:50'),
(26, 'Payment_79160', 'success', '1', '45', 15, '2020-08-07 05:37:13'),
(27, 'Payment_43277', 'success', '', '100', 2, '2020-08-17 22:58:19'),
(28, 'Payment_60519', 'success', '', '180', 2, '2020-08-17 23:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop`
--

CREATE TABLE `tbl_shop` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_shop`
--

INSERT INTO `tbl_shop` (`id`, `name`, `description`, `image`, `price`, `status`, `insert_date`, `update_date`) VALUES
(1, 'test shop product 2', '', '1b18b169f1f9abf01fe9e89a57d8ac95.jpeg', 30, 'inactive', '2020-08-07 11:54:42', '2020-08-07 12:03:26'),
(6, 'test shop product', '', '14bcfe51860a601d7622dd4b99584319.jpeg', 25, 'active', '2020-08-07 11:56:04', '2020-08-07 11:56:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_token`
--

CREATE TABLE `tbl_token` (
  `id` int(11) NOT NULL,
  `token_qty` int(11) NOT NULL,
  `token_price` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_token`
--

INSERT INTO `tbl_token` (`id`, `token_qty`, `token_price`, `status`, `created`) VALUES
(1, 10, 100, 'active', '2020-04-20 00:00:00'),
(2, 20, 180, 'active', '2020-04-19 00:00:00'),
(3, 30, 250, 'active', '2020-04-19 00:00:00'),
(4, 40, 350, 'active', '2020-04-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_token_users`
--

CREATE TABLE `tbl_token_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tokens` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `token_id` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_token_users`
--

INSERT INTO `tbl_token_users` (`id`, `user_id`, `tokens`, `payment_id`, `token_id`, `price`, `status`, `created`, `updated`) VALUES
(1, 2, 10, 'Payment_03085', 1, 100.00, 'active', '2020-04-20 04:43:12', '2020-04-20 04:43:12'),
(2, 2, 30, 'Payment_52225', 3, 250.00, 'active', '2020-05-22 07:59:47', '2020-05-22 07:59:47'),
(3, 2, 10, 'Payment_95419', 1, 100.00, 'active', '2020-05-22 11:32:07', '2020-05-22 11:32:07'),
(4, 2, 30, 'Payment_91884', 3, 250.00, 'active', '2020-05-22 11:32:22', '2020-05-22 11:32:22'),
(5, 2, 20, 'Payment_67010', 2, 180.00, 'active', '2020-06-24 00:33:50', '2020-06-24 00:33:50'),
(6, 2, 10, 'Payment_43277', 1, 100.00, 'active', '2020-08-17 15:58:19', '2020-08-17 15:58:19'),
(7, 2, 20, 'Payment_60519', 2, 180.00, 'active', '2020-08-17 16:04:45', '2020-08-17 16:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(255) NOT NULL,
  `user_slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `refreal_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `type` enum('user','admin','model') CHARACTER SET latin1 NOT NULL DEFAULT 'user',
  `country_code` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT '+91',
  `mobile` varchar(15) CHARACTER SET latin1 NOT NULL,
  `profile_image` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cover_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `streamer_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diamonds` int(20) NOT NULL DEFAULT '0',
  `location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` int(11) NOT NULL DEFAULT '1',
  `language` enum('english','arabic') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'english',
  `currency` enum('usd','afghani','inr','riyal','dirham','euro') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'usd',
  `token` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` enum('active','inactive','deleted') CHARACTER SET latin1 NOT NULL DEFAULT 'active',
  `live_status` int(2) NOT NULL DEFAULT '0' COMMENT '0=offline,1=online',
  `is_featured` int(2) NOT NULL DEFAULT '0' COMMENT '0=not featured, 1=featured',
  `customer_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `residency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_location` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiktok_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscription_price` int(11) NOT NULL,
  `premium_price` int(11) NOT NULL,
  `livestreaming_rate` int(11) NOT NULL,
  `livestreaming_rate_1_on_1` int(11) NOT NULL,
  `allow_fancam_request` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `allow_stream_request` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `new_subscriber` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `new_content_purchase` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `new_private_message` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `receive_latest_news_updates` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `new_model_friend_request` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_in_premium_video_store` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `allow_my_friends` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_approved` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Not approved, 1: Approved',
  `document_photo_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_id_verification_selfie` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `agreement_model` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes',
  `agreement_2257` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes',
  `current_step` tinyint(4) NOT NULL DEFAULT '1',
  `government_issued_id_type` int(11) NOT NULL,
  `government_id_issued_by_state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `government_id_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `government_id_expiration_month` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `government_id_expiration_day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `government_id_expiration_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_expiration` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes',
  `legal_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob_month` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob_day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias_one` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias_two` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_slug`, `refreal_code`, `name`, `bio`, `facebook_id`, `twitter_id`, `google_id`, `email`, `username`, `password`, `type`, `country_code`, `mobile`, `profile_image`, `cover_image`, `streamer_image`, `diamonds`, `location`, `latitude`, `longitude`, `notification`, `language`, `currency`, `token`, `status`, `live_status`, `is_featured`, `customer_id`, `created`, `updated`, `residency`, `address`, `display_location`, `country`, `state`, `city`, `instagram_url`, `twitter_url`, `facebook_url`, `tiktok_url`, `subscription_price`, `premium_price`, `livestreaming_rate`, `livestreaming_rate_1_on_1`, `allow_fancam_request`, `allow_stream_request`, `new_subscriber`, `new_content_purchase`, `new_private_message`, `receive_latest_news_updates`, `new_model_friend_request`, `available_in_premium_video_store`, `allow_my_friends`, `is_approved`, `document_photo_id`, `document_id_verification_selfie`, `agreement_model`, `agreement_2257`, `current_step`, `government_issued_id_type`, `government_id_issued_by_state`, `government_id_number`, `government_id_expiration_month`, `government_id_expiration_day`, `government_id_expiration_year`, `no_expiration`, `legal_name`, `dob_month`, `dob_day`, `dob_year`, `alias_one`, `alias_two`) VALUES
(1, '', '', 'admin', '', '', '', '', 'admin.zestbrains@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '+91', '123456', '142/4bfb6b7519c6542d2e83200efa6e7de3.jpg', '', '', 0, '', '', '', 0, 'english', 'usd', '', 'active', 0, 0, '', '2020-01-02 11:03:35', '2020-01-02 11:03:35', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(2, '', '', 'Lee Markk', 'lee mark\'s bio', '', '', '', 'leemark@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'user', '+undefined', '', '2/profile_pic/205ce815ab77a5c74e6fc77b55e9c4c5.jpg', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-02-22 09:35:12', '2020-05-18 06:29:56', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(3, '6324', '', 'Sakira', '', '', '', '', 'sakira@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '3/profile_pic/profile.jpg', '3/cover_pic/cover.png', '', 5, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-25 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(4, '9521', '', 'Lady Gaga', '', '', '', '', 'ladygaga@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '4/profile_pic/profile.jpg', '4/cover_pic/cover.png', '', 8, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-26 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(5, '5321', '', 'Celion Dion', '', '', '', '', 'celiondioan@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '5/profile_pic/profile.jpg', '5/cover_pic/cover.png', '', 1, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-27 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(6, '7724', '', 'Jessa Coras', '', '', '', '', 'jessacoras@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '6/profile_pic/profile.jpg', '6/cover_pic/cover.png', '', 3, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-02-28 09:36:04', '2020-02-27 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(7, '3356', '', 'Samy Lendropz', '', '', '', '', 'samy520@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '7/profile_pic/profile.jpg', '7/cover_pic/cover.png', '7/stremer_pic/mystream.jpg', 7, '', '', '', 1, 'english', 'usd', '', 'active', 1, 1, '', '2020-02-29 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(8, '2045', '', 'Yuka Henry', '', '', '', '', 'yhenry@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '8/profile_pic/profile.jpg', '8/cover_pic/cover.png', '', 9, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-03-03 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(9, '9523', '', 'Suzain Parmalika', '', '', '', '', 'suzainp@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '9/profile_pic/profile.jpg', '9/cover_pic/cover.png', '', 23, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-03-04 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(10, 'a45ds', '', 'Vislas Minrama', '', '', '', '', 'vislasm@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '10/profile_pic/profile.jpg', '10/cover_pic/cover.png', '', 2, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-03-05 09:36:04', '2020-02-22 09:36:04', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(11, '5642', '', 'Rose Kindraa', '', '', '', '', 'rosekindra@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '+undefined', '', '11/profile_pic/profile.jpg', '11/cover_pic/cover.png', '', 120, '', '', '', 1, 'english', 'usd', '', 'active', 0, 1, '', '2020-03-06 09:36:04', '2020-03-02 08:40:00', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(12, '83793', '83793', 'Vela Mista', 'Down to Earth and always smiling :) | Fitness enthusiast living in LA | IG: @gucciblue | Follow for                     access to daily private content, messaging, and more! LIVE EVERY MONDAY @ 7:30PM A', '', '', '', 'vela@gmail.com', 'Joya', 'e10adc3949ba59abbe56e057f20f883e', 'model', '+91', '9988776655', '12/profile_pic/88fc4414cf54b34c3b5061b31e274f97.png', '12/cover_pic/0875d53bb8bf3569fba360e8925c2506.jpeg', '12/stremer_pic/b583b0da30719775d08920a8ac0e7571.jpeg', 12, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-03-07 08:25:17', '2020-06-19 15:06:45', 'Non-US Resident', 'Lorem ipsum', '1', 'United States', 'Alaska', 'Homer', 'https://www.instagram.com/vela', 'https://www.twitter.com/vela', 'https://www.facebook.com/vela', 'https://www.tiktok.com/vela', 1, 2, 3, 4, '1', '1', '1', '1', '1', '1', '1', '1', '1', 0, '12/photo_id/1dd67245169bf21fca54dbdd6419f630.jpg', '12/id_verification_selfie/97913b19223249c1b14c966176ee790e.jpg', 1, 1, 4, 1, 'New Jersey', '1234567890', '', '', '', 1, 'Vela Mista', '07', '5', '1993', '', ''),
(13, '94092', 'REFERRAL-61577519597', 'Carolyn J. Carroll', '', '', '', '', 'carolyn@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'inactive', 0, 0, '', '2020-06-10 08:16:47', '2020-06-10 08:16:47', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(14, '43897', 'REFERRAL-06402366291', 'Isis R. Kovac', '', '', '', '', 'isis@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'model', '', '', '', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'inactive', 0, 0, '', '2020-06-10 08:17:55', '2020-06-10 08:17:55', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(15, '', '', 'Noelle Mcdonald', '', '', '', '', 'lovis@mailinator.com', '', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'user', '', '', '', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-08-07 05:36:17', '2020-08-07 05:36:17', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(16, '', '', 'Kenneth Owens', '', '', '', '', 'lazaliqa@mailinator.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'user', '', '', '', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-08-10 09:51:53', '2020-08-10 09:51:53', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', ''),
(17, '', '', 'Cynthia Ramsey', '', '', '', '', 'qojela@mailinator.com', '', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'user', '', '', '', '', '', 0, '', '', '', 1, 'english', 'usd', '', 'active', 0, 0, '', '2020-08-10 14:37:53', '2020-08-10 14:37:53', '', '', '0', '', '', '', '', '', '', '', 0, 0, 0, 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '', '', 0, 0, 1, 0, '', '', '', '', '', 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_service`
--

CREATE TABLE `tbl_user_service` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_type` int(3) NOT NULL DEFAULT '0' COMMENT '''0=email_notification''',
  `service` int(5) NOT NULL DEFAULT '0' COMMENT '''0=private_message'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_videos`
--

CREATE TABLE `tbl_videos` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=private_videos, 1=custom videos',
  `payment_id` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_videos`
--

INSERT INTO `tbl_videos` (`id`, `post_id`, `user_id`, `category`, `payment_id`, `status`) VALUES
(1, 19, 2, '0', 'Payment_10750', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_content`
--
ALTER TABLE `mst_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_email_templates`
--
ALTER TABLE `mst_email_templates`
  ADD PRIMARY KEY (`iEmailId`);

--
-- Indexes for table `mst_imagethumb`
--
ALTER TABLE `mst_imagethumb`
  ADD PRIMARY KEY (`iThumbId`);

--
-- Indexes for table `mst_lang`
--
ALTER TABLE `mst_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_module`
--
ALTER TABLE `mst_module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mod_modulecode` (`mod_modulecode`);

--
-- Indexes for table `mst_role_rights`
--
ALTER TABLE `mst_role_rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sitesettings`
--
ALTER TABLE `mst_sitesettings`
  ADD PRIMARY KEY (`iFieldId`);

--
-- Indexes for table `tbl_agreements`
--
ALTER TABLE `tbl_agreements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blocked_fans`
--
ALTER TABLE `tbl_blocked_fans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_custom_video_requests`
--
ALTER TABLE `tbl_custom_video_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_friend_requests`
--
ALTER TABLE `tbl_friend_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_geo_blocking`
--
ALTER TABLE `tbl_geo_blocking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_getting_started`
--
ALTER TABLE `tbl_getting_started`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_make_money`
--
ALTER TABLE `tbl_make_money`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_message_threads`
--
ALTER TABLE `tbl_message_threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_model_extra`
--
ALTER TABLE `tbl_model_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_extra_key` (`model_id`);

--
-- Indexes for table `tbl_model_followers`
--
ALTER TABLE `tbl_model_followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_followers` (`model_id`),
  ADD KEY `model_user_followers` (`user_id`);

--
-- Indexes for table `tbl_model_payment_details`
--
ALTER TABLE `tbl_model_payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_model_posts`
--
ALTER TABLE `tbl_model_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_id` (`model_id`);

--
-- Indexes for table `tbl_model_posts_fav`
--
ALTER TABLE `tbl_model_posts_fav`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fav_post` (`post_id`);

--
-- Indexes for table `tbl_model_post_comments`
--
ALTER TABLE `tbl_model_post_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_model_subscribers`
--
ALTER TABLE `tbl_model_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_model_videos`
--
ALTER TABLE `tbl_model_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_video_id` (`model_id`);

--
-- Indexes for table `tbl_model_w9form`
--
ALTER TABLE `tbl_model_w9form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_shop`
--
ALTER TABLE `tbl_shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_token`
--
ALTER TABLE `tbl_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_token_users`
--
ALTER TABLE `tbl_token_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_service`
--
ALTER TABLE `tbl_user_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_user_id` (`user_id`);

--
-- Indexes for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_content`
--
ALTER TABLE `mst_content`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mst_email_templates`
--
ALTER TABLE `mst_email_templates`
  MODIFY `iEmailId` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mst_imagethumb`
--
ALTER TABLE `mst_imagethumb`
  MODIFY `iThumbId` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mst_lang`
--
ALTER TABLE `mst_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_module`
--
ALTER TABLE `mst_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `mst_role_rights`
--
ALTER TABLE `mst_role_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `mst_sitesettings`
--
ALTER TABLE `mst_sitesettings`
  MODIFY `iFieldId` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_agreements`
--
ALTER TABLE `tbl_agreements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_blocked_fans`
--
ALTER TABLE `tbl_blocked_fans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_custom_video_requests`
--
ALTER TABLE `tbl_custom_video_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_friend_requests`
--
ALTER TABLE `tbl_friend_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_geo_blocking`
--
ALTER TABLE `tbl_geo_blocking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_getting_started`
--
ALTER TABLE `tbl_getting_started`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_make_money`
--
ALTER TABLE `tbl_make_money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_message_threads`
--
ALTER TABLE `tbl_message_threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_model_extra`
--
ALTER TABLE `tbl_model_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_model_followers`
--
ALTER TABLE `tbl_model_followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_model_payment_details`
--
ALTER TABLE `tbl_model_payment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_model_posts`
--
ALTER TABLE `tbl_model_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_model_posts_fav`
--
ALTER TABLE `tbl_model_posts_fav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_model_post_comments`
--
ALTER TABLE `tbl_model_post_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_model_subscribers`
--
ALTER TABLE `tbl_model_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_model_videos`
--
ALTER TABLE `tbl_model_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_model_w9form`
--
ALTER TABLE `tbl_model_w9form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_shop`
--
ALTER TABLE `tbl_shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_token`
--
ALTER TABLE `tbl_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_token_users`
--
ALTER TABLE `tbl_token_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_user_service`
--
ALTER TABLE `tbl_user_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_model_followers`
--
ALTER TABLE `tbl_model_followers`
  ADD CONSTRAINT `model_followers` FOREIGN KEY (`model_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `model_user_followers` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_model_posts`
--
ALTER TABLE `tbl_model_posts`
  ADD CONSTRAINT `model_id` FOREIGN KEY (`model_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_model_videos`
--
ALTER TABLE `tbl_model_videos`
  ADD CONSTRAINT `model_video_id` FOREIGN KEY (`model_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_user_service`
--
ALTER TABLE `tbl_user_service`
  ADD CONSTRAINT `service_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
