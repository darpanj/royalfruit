<?php

require_once('vendor/autoload.php');

Class Payment {

    public $publishKey = 'pk_test_MPrQLpqAEy4tVXDLUnKz9euT00HbgJZyb9';
    public $secretKey = 'sk_test_SrsKgeULaTV74DqsCZVncV6I00S3kfzQIP';

    public function init() {
        \Stripe\Stripe::setApiKey($this->secretKey);
    }

    public function chargeCustomer($amt = 0, $token = '', $curr = 'gbp') {
        $_response = array(
            'status' => '412',
            'message' => 'somthing went wrong'
        );
        try {
            $charge = \Stripe\Charge::create([
                        "amount" => $amt * 100,
                        "currency" => "gbp",
                        "source" => $token, // obtained with Stripe.js
            ]);
            $_response = array(
                'status' => '200',
                'message' => 'success',
                'id' => $charge->id
            );
            return $_response;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (strpos($e->getMessage(), 'currency') !== false) {
                $msg = 'Currency invalid stripe not supported';
            }
            $_response = array(
                'status' => '412',
                'message' => $e->getMessage(),
                'id' => 0
            );
            return $_response;
        }
    }

}
