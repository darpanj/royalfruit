<?php

require_once('stripe/init.php');

Class Stripe {

    public $publishKey = Stripe_test;
    public $secretKey = Stripe_secret;

//
    function __construct() {
        \Stripe\Stripe::setApiKey($this->secretKey);
    }

    public function test() {
        pre('test');
    }

    public function create_user($email = "", $token = "") {
        $data = array('status' => 412, 'message' => 'Something Went Wrong', 'id' => 0);
        try {
            $customer = \Stripe\Customer::create([
                        'source' => $token,
                        'email' => $email,
            ]);
            $data['id'] = $customer->id;
            $data['status'] = 200;
        } catch (Exception $e) {
            $data['message'] = $e->getMessage();
        }
        return $data;
    }

    public function update_user($user_id = "", $card_token = "") {
        $data = array('status' => 412, 'message' => 'Something Went Wrong');
        try {
            $data = \Stripe\Customer::update($user_id, [
                        'source' => $card_token,
            ]);
            $data['status'] = 200;
        } catch (Exception $e) {
            $data['message'] = $e->getMessage();
        }
        return $data;
    }

    public function ajar_charge($amount = 0, $customer_id = "", $currency = "usd", $type = "booking") {
        $data = array('status' => 412, 'message' => 'Something Went Wrong', 'id' => 0);
        try {
            if ($type == "booking") {
                $type = 'Ajar Car Rent Payment';
            } elseif ($type == "extend") {
                $type = 'Car Booking day extend';
            } else {
                $type = 'Ajar Car Deposit Pay';
            }


            $currency_code = get_price_code($currency);
            $charge = \Stripe\Charge::create([
                        'amount' => $amount * 100,
                        'currency' => $currency_code,
                        'customer' => $customer_id,
                        'description' => $type,
                        'statement_descriptor' => $type,
            ]);
            $data['id'] = $charge->id;
            $data['status'] = 200;
        } catch
        (Exception $e) {
            $data['message'] = $e->getMessage();
        }
        return $data;
    }

    public function refund_payment($charge_id = "", $amount = 0) {

        $data_r = array('status' => 412, 'message' => 'Something Went Wrong');
        try {
            $data = array('charge' => $charge_id);
            if (!empty($amount)) {
                $amount = calculation_percentage($amount);
                $data['amount'] = $amount * 100;
            }
            \Stripe\Refund::create($data);
            $data_r['status'] = 200;
        } catch (Exception $e) {
            $data_r['message'] = $e->getMessage();
        }
        return $data_r;
    }

    public function create_card_token() {
        $card = \Stripe\Token::create([
                    'card' => [
                        'number' => '4242424242424242',
                        'exp_month' => 1,
                        'exp_year' => 2021,
                        'cvc' => '314',
                    ],
        ]);
        return $card;
    }

    public
            function createCustomer($token = '', $email = '') {
        try {
            $customer = \Stripe\Customer::create([
                        'source' => $token,
                        'email' => $email,
            ]);

            return $customer->id;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public
            function create_connect_user($email = "", $token = "") {
        try {
            $acct = \Stripe\Account::create([
                        "email" => $email,
                        "country" => country_stripe,
                        "type" => "custom",
                        "account_token" => $token,
                        "settings" => array(
                            "payouts" => array(
                                "schedule" => array(
                                    "interval" => "manual"
                                )
                            )
                        )
            ]);
            return ['status' => 200, 'msg' => 'Something Went Wrong', 'id' => $acct->id];
        } catch (Exception $e) {
            return ['status' => 412, 'msg' => $e->getMessage()];
        }
    }

    public
            function createConnectUser($email = '', $post = array()) {
        try {
            $acct = \Stripe\Account::create([
                        "business_type" => "individual",
                        "individual" => array(
                            "first_name" => $post['first_name'],
                            "last_name" => $post['last_name'],
                            "dob" => array(
                                "day" => (int) $post['dday'],
                                "month" => (int) $post['dmonth'],
                                "year" => (int) $post['dyear'],
                            ),
                            "address" => array(
                                "postal_code" => $post['postal_code'],
                                "city" => $post['city'],
                                "line1" => $post['address_line'],
                            )
                        ),
                        "tos_acceptance" => array(
                            'date' => time(),
                            'ip' => $_SERVER['REMOTE_ADDR']
                        ),
                        "email" => $email,
                        "country" => country_stripe,
                        "type" => "custom",
                        "settings" => array(
                            "payouts" => array(
                                "schedule" => array(
                                    "interval" => "manual"
                                )
                            )
                        )
            ]);
//            return $acct->id;
            return ['status' => 200, 'msg' => 'Something Went Wrong', 'id' => $acct->id];
        } catch (Exception $e) {
            return ['status' => 412, 'msg' => $e->getMessage()];
        }
    }

    public
            function createBank($accId = '', $post = array()) {
        try {
            $_bankToken = \Stripe\Token::create([
                        'bank_account' => [
                            'country' => country_stripe,
                            'currency' => curency_stripe,
                            'account_holder_name' => $post['f_name'] . ' ' . $post['l_name'],
                            'account_holder_type' => 'individual',
                            'routing_number' => space_remove($post['transit_number']),
                            'account_number' => space_remove($post['account_number']),
                        ],
            ]);
            $_bank_account = \Stripe\Account::createExternalAccount(
                            $accId, [
                        'external_account' => $_bankToken->id,
                            ]
            );
            return ['status' => 200, 'msg' => 'Something Went Wrong'];
        } catch (Exception $e) {
            return ['status' => 412, 'msg' => $e->getMessage()];
        }
    }

    public
            function buy_credit($credit = 1, $customer_id = '', $email = "") {
        $_response = array(
            'status' => '412',
            'message' => 'somthing went wrong'
        );

        try {
            $charge = \Stripe\Charge::create([
                        'amount' => $credit * 100,
                        'currency' => curency_stripe,
                        'customer' => $customer_id,
                        'receipt_email' => $email,
            ]);
            $_response = array(
                'status' => '200',
                'message' => 'success',
                'id' => $charge->id,
                'receipt_url' => $charge->receipt_url
            );
            return $_response;
        } catch (Exception $e) {
            $_response = array(
                'status' => '412',
                'message' => $e->getMessage(),
//                'message' => 'Your card was declined',
                'id' => 0
            );
            return $_response;
        }
    }

    public
            function chargeCustomer($amt = 0, $custId = '', $accId = '', $curr = 'gbp') {
        $_response = array(
            'status' => '412',
            'message' => 'somthing went wrong'
        );
        try {
            $charge = \Stripe\Charge::create([
                        'amount' => $amt * 100,
                        'currency' => $curr,
                        'customer' => $custId,
                        "transfer_data" => [
                            "destination" => $accId,
                        ],
            ]);
            $_response = array(
                'status' => '200',
                'message' => 'success',
                'id' => $charge->transfer
            );
            return $_response;
        } catch (Exception $e) {
            $_response = array(
                'status' => '412',
                'message' => $e->getMessage(),
                'id' => 0
            );
            return $_response;
        }
    }

    public
            function getTransfer($transferId = '') {
        try {
            $amt = \Stripe\Transfer::retrieve($transferId);
            return $amt->amount;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public
            function getTransferChargeId($transferId = '') {
        try {
            $chargeId = \Stripe\Transfer::retrieve($transferId);
            return $chargeId->source_transaction;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public
            function refundByTransactionId($chargeId = '') {
        try {
            $re = \Stripe\Refund::create([
                        "charge" => $chargeId
            ]);
            return $re;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public
            function payout($amt = 0, $actId = '') {
        try {
//            $payout = \Stripe\Payout::create([
//                'amount' => $amt,
//                'currency' => curency_stripe,
//            ], ['stripe_account' => $actId]);
            $payout = \Stripe\Transfer::create([
                        "amount" => $amt * 100,
                        "currency" => curency_stripe,
                        "destination" => $actId,
//                "transfer_group" => "ORDER_95"
            ]);
            return ['status' => 200, 'msg' => 'Something went Wrong', 'id' => $payout->id];
//            return $payout->id;
        } catch (Exception $e) {
            return ['status' => 412, 'msg' => $e->getMessage()];
//            return $e->getMessage();
        }
    }

}
