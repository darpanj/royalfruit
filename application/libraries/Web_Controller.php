<<<<<<< HEAD
<?php

class Web_Controller extends CI_Controller
{

    public $data = array();
    public $access = array();
    public $admin_lang = array();
    public $user_lang = "english";
    public $user_id = 0;
    public $user_type = 0;
    //s public $ADM_URL = 'admin';
    public $user_city = '';
    public $currency = 'usd';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin');
        $this->Admin->siteSettings();
        $this->data['header_panel'] = true;
        $this->data['footer_panel'] = true;
        $this->data['error_type'] = "";
        $this->data['error_message'] = "";
        $this->data['action'] = $this->input->post('action');
        $this->data['method'] = $this->router->fetch_method();
        $this->data['class'] = $this->router->fetch_class();
        $this->data['msgType'] = $this->session->userdata('msgType');

        $user_lang = ($this->session->userdata('user_lang')) ? $this->session->userdata('user_lang') : $this->user_lang;
        $this->lang->load('message_lang', $user_lang);
        $this->data['language'] = $user_lang;
        $this->admin_lang = $this->common->get_data_by_id('mst_lang', 'status', 'active');
        $this->data['fb_login_url'] = get_facebook_url();
        $this->data['google_login_url'] = get_google_url();
        $this->data['user_id'] = $this->user_id;
        $this->data['extra_class'] = "";
        $this->chk_admin_session();

        $ip = $_SERVER['REMOTE_ADDR'];
        // echo $ip;
        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        if ($query && $query['status'] == 'success') {
            $this->user_city = $query['city'];
        }
    }

    function chk_admin_session()
    {
        $sessionDetails = $this->session->userdata;
        //pre($sessionDetails);
        $this->ADM_URL = $this->uri->segment(1) . '/';
        if ($this->uri->segment(1) == 'admin') {
            $session_login = isset($sessionDetails['ADMINID']) ? (int) $sessionDetails['ADMINID'] : 0;
            $this->user_id = $this->session->userdata('ADMINID');
            $this->user_type = $this->session->userdata('ADMINUSERTYPE');
        } else {
            $session_login = isset($sessionDetails['USERID']) ? (int) $sessionDetails['USERID'] : 0;
            $this->user_id = $this->session->userdata('USERID');
            $this->user_type = $this->session->userdata('USERTYPE');
        }
        //pre($session_login);

        if ($session_login <= 0) {
            $this->data['user_detail'] = array();
            // Allow some methods?


            $allowed = array(
                'login', 'login_check', 'faq', 'register', 'reset', 'become_model', 'signup_user', 'load_models', 'model_details', 'explore', 'load_posts', 'get_model_by_id', 'live', '',
            );
            $allowed = array('home', 'model', 'shop');
            $extra = array('model');
            // pre($this->uri->segment(1));
            if (!in_array($this->uri->segment(1), $allowed)) {
                redirect(base_url('home/login'));
            }
        } else if ($session_login && $session_login == TRUE) {
            $this->data['user_detail'] = $this->db->get_where('tbl_users', array('id' => $this->user_id))->row_array();
            $this->data['user_id'] = $this->data['user_detail']['id'];
            $this->data['counts'] = $this->Admin->siteCount_web($this->data['user_id']);
            if ($this->data['user_detail']['type'] == 'model') {
                $this->data['user_image'] = checkImage(2, $this->data['user_detail']['profile_image']);
                $this->data['cover_image'] = checkImage(2, $this->data['user_detail']['cover_image']);
            } else if ($this->data['user_detail']['type'] == 'user') {
                $this->data['user_image'] = checkImage(1, $this->data['user_detail']['profile_image']);
            }

            $notallowed = array('login');
            // pre($this->uri->segment(2));
            if (in_array($this->uri->segment(2), $notallowed)) {
                //pre($this->data['user_detail']['type']);
                if ($this->data['user_detail']['type'] == 'model') {
                    redirect(base_url('account/application'));
                } else {
                    redirect(base_url('account/dashboard'));
                }
            }
            try {
                $commonModules = $this->Admin->getCommonModule();
                // all modules
                $allModules = $this->Admin->getAllModule();
                // modules based on user role
                $userRights = $this->Admin->getUserRights($this->user_type);
                $this->access = $this->set_rights($allModules, $userRights, $commonModules);
            } catch (Exception $ex) {
                $this->session->set_flashdata('error', $ex->getMessage());
            }
        }
    }

    public function textWatermark($source_image)
    {
        $config['source_image'] = FCPATH . SITE_UPD . '/models' . '/' . $source_image;
        //The image path,which you would like to watermarking
        $config['wm_text'] = 'RoyalFruit-darpnnnnn';
        $config['wm_type'] = 'text';
        // $config['wm_font_path'] = './fonts/atlassol.ttf';
        $config['wm_font_size'] = 16;
        $config['wm_font_color'] = '#ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_hor_alignment'] = 'right';
        $config['wm_padding'] = '20';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            return $this->image_lib->display_errors();
        }
    }
    public function overlayWatermark($source_image)
    {
        //$source_image = $this->image_create_gd($source_image);
        $config['image_library'] = 'gd2';
        $config['source_image'] = FCPATH . SITE_UPD . '/models' . '/' . $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = FCPATH . ADM_MEDIA . '/' . WATERMARK_IMG;     //the overlay image
        $config['wm_opacity'] = 50;
        //$config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'right';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
    }


    function format_interval(DateInterval $interval)
    {
        $result = "";
        if ($interval->y) {
            $result = $interval->format("%y years ");
        }
        if (empty($result) && $interval->m) {
            $result = $interval->format("%m months ");
        }
        if (empty($result) && $interval->d) {
            $result = $interval->format("%d days ");
        }
        if (empty($result) && $interval->h) {
            $result = $interval->format("%h hours ");
        }
        if (empty($result) && $interval->i) {
            $result = $interval->format("%i minutes ");
        }
        if (empty($result) && $interval->s) {
            $result = $interval->format("%s seconds ");
        }
        if (empty($result))
            $result = 'now';

        return $result;
    }

    //Listing Filteration
    function getDTFilters($post = array())
    {
        $filters = array(
            'offset' => isset($post['start']) ? intval($post['start']) : 0,
            'limit' => isset($post['length']) ? intval($post['length']) : 25,
            'sort' => isset($post['columns'][$post["order"][0]['column']]['data']) ? $post['columns'][$post["order"][0]['column']]['data'] : 'dCreatedDate',
            'order' => isset($post["order"][0]['dir']) ? $post["order"][0]['dir'] : 'DESC',
            'search' => isset($post["search"]['value']) ? $post["search"]['value'] : '',
            'sEcho' => isset($post['sEcho']) ? $post['sEcho'] : 1,
        );
        return $filters;
    }

    public function change_lang($lang = "english")
    {
        $this->session->set_userdata("user_lang", $lang);
        redirect(base_url(), "refresh");
    }

    public function upload_ck_file()
    {
        if (isset($_FILES['upload'])) {
            // ------ Process your file upload code -------
            $filen = $_FILES['upload']['tmp_name'];
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];
            $filename = md5(time() . rand()) . '.' . $extension;
            $con_images = SITE_UPD . 'ckeditor/' . $filename;
            move_uploaded_file($filen, FCPATH . $con_images);
            $url = base_url($con_images);
            /* $url = $con_images; */

            $funcNum = $_GET['CKEditorFuncNum'];
            // Optional: instance name (might be used to load a specific configuration file or anything else).
            $CKEditor = $_GET['CKEditor'];
            // Optional: might be used to provide localized messages.
            $langCode = $_GET['langCode'];

            // Usually you will only assign something here if the file could not be uploaded.
            $message = '';
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
        }
    }

    //Unique Email validation
    public function unique_email()
    {
        $id = $this->input->post('id');

        if (empty($id))
            $check = $this->db->get_where('tbl_users', array('email' => $this->input->post('email')))->num_rows();
        else
            $check = $this->db->get_where('tbl_users', array('email' => $this->input->post('email'), 'id !=' => $id))->num_rows();


        //pre($check);
        if ($check > 0) {
            $this->form_validation->set_message("unique_email", "%s already exists");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //Unique Email validation
    public function unique_mobile()
    {
        $id = $this->input->post('id');
        $country_code = '+257';

        if (empty($id))
            $check = $this->db->get_where('tbl_users', array('mobile' => $this->input->post('mobile')))->num_rows();
        else
            $check = $this->db->get_where('tbl_users', array('mobile' => $this->input->post('mobile'), 'id !=' => $id))->num_rows();
        if ($check > 0) {
            $this->form_validation->set_message("unique_mobile", $this->lang->line('err_number_taken'));
            return FALSE;
        } else {
            return TRUE;
        }
    }
    //Unique Email validation
    public function oldPassword()
    {
        $id = $this->input->post('id');
        $check = $this->db->get_where('tbl_users', array('password' => md5($this->input->post('old_password')), 'id' => $id))->num_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message("old_password", $this->lang->line('err_old_password'));
            return FALSE;
        }
    }

    private function set_rights($menus, $menuRights, $topmenu)
    {
        //  pre($menus);
        $data = array();
        for ($i = 0, $c = count($menus); $i < $c; $i++) {
            $row = array();
            for ($j = 0, $c2 = count($menuRights); $j < $c2; $j++) {
                if ($menuRights[$j]["rr_modulecode"] == $menus[$i]["mod_modulecode"]) {
                    if (
                        $this->authorize($menuRights[$j]["rr_create"]) || $this->authorize($menuRights[$j]["rr_edit"]) ||
                        $this->authorize($menuRights[$j]["rr_delete"]) || $this->authorize($menuRights[$j]["rr_view"])
                    ) {

                        $row["menu"] = $menus[$i]["mod_modulegroupcode"];
                        $row["menu_name"] = ($this->user_lang == 'en') ? $menus[$i]["mod_modulename"] : $menus[$i]["mod_modulename_lang"];
                        $row["page_name"] = $menus[$i]["mod_modulepagename"];
                        $row["image"] = $menus[$i]["vImage"];
                        $row["create"] = $menuRights[$j]["rr_create"];
                        $row["edit"] = $menuRights[$j]["rr_edit"];
                        $row["delete"] = $menuRights[$j]["rr_delete"];
                        $row["view"] = $menuRights[$j]["rr_view"];
                        $row["status"] = $menuRights[$j]["rr_status"];
                        $row["extra"] = $menuRights[$j]["extra"];
                        $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]] = $row;
                        $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_menu_name"] = ($this->user_lang == 'en') ? $menus[$i]["mod_modulegroupname"] : $menus[$i]["mod_modulegroupname_lang"];
                        if ($menus[$i]['mod_moduleorder'] == 1 || empty($data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_page_name"])) {
                            $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_page_name"] = $menus[$i]["mod_modulepagename"];
                            $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_image"] = $menus[$i]["vImage"];
                        }
                    }
                }
            }
        }
        return $data;
    }

    private function authorize($module)
    {
        return $module == "yes" ? TRUE : FALSE;
    }

    private function valid_url($user_type = '')
    {
        $allowed = array('dashboard', 'cpass', 'profile', 'logout', 'account');
        if (!empty($user_type)) {
            $controller = $this->uri->segment(2);
            $method = $this->uri->segment(3);
            if (in_array($controller, $allowed))
                return TRUE;
            $module_data = $this->common->get_data_by_id('mst_module', 'mod_modulepagename', $controller, '*', array(), '', '', 1, 'row');
            if (!empty($module_data)) {
                $rights = $this->common->get_data_by_id('mst_role_rights', 'rr_rolecode', $user_type, '*', array('rr_modulecode' => $module_data['mod_modulecode']));
                if (!empty($rights)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function load_version_model($path, $model)
    {
        if (file_exists(APPPATH . "models/" . $path . EXT)) {
            $this->load->model($path, $model);
        } else {
            $this->data['error'] = $model . ' Model could not load';
        }
    }

    public function upload_image($folder = 'users')
    {
        $data = $_POST['image'];
        if (!empty($data)) {
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $imageName = md5(time() . rand()) . '.jpeg';
            file_put_contents(FCPATH . SITE_UPD . $folder . '/' . $imageName, $data);
            return $imageName;
        } else {
            return $data;
        }
    }
}
=======
<?php

class Web_Controller extends CI_Controller
{

    public $data = array();
    public $access = array();
    public $admin_lang = array();
    public $user_lang = "english";
    public $user_id = 0;
    public $user_type = 0;
    public $user_city = '';
    //s public $ADM_URL = 'admin';
    public $currency = 'usd';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin');
        $this->Admin->siteSettings();
        $this->data['header_panel'] = true;
        $this->data['footer_panel'] = true;
        $this->data['error_type'] = "";
        $this->data['error_message'] = "";
        $this->data['action'] = $this->input->post('action');
        $this->data['method'] = $this->router->fetch_method();
        $this->data['class'] = $this->router->fetch_class();
        $this->data['msgType'] = $this->session->userdata('msgType');

        $user_lang = ($this->session->userdata('user_lang')) ? $this->session->userdata('user_lang') : $this->user_lang;
        $this->lang->load('message_lang', $user_lang);
        $this->data['language'] = $user_lang;
        $this->admin_lang = $this->common->get_data_by_id('mst_lang', 'status', 'active');
        $this->data['fb_login_url'] = get_facebook_url();
        $this->data['google_login_url'] = get_google_url();
        $this->data['user_id'] = $this->user_id;
        $this->data['extra_class'] = "";
        $this->chk_admin_session();

        $ip = $_SERVER['REMOTE_ADDR'];
        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        if ($query && $query['status'] == 'success') {
            $this->user_city = $query['city'];
        }
    }

    function chk_admin_session()
    {
        $sessionDetails = $this->session->userdata;
        //pre($sessionDetails);
        $this->ADM_URL = $this->uri->segment(1) . '/';
        if ($this->uri->segment(1) == 'admin') {
            $session_login = isset($sessionDetails['ADMINID']) ? (int) $sessionDetails['ADMINID'] : 0;
            $this->user_id = $this->session->userdata('ADMINID');
            $this->user_type = $this->session->userdata('ADMINUSERTYPE');
        } else {
            $session_login = isset($sessionDetails['USERID']) ? (int) $sessionDetails['USERID'] : 0;
            $this->user_id = $this->session->userdata('USERID');
            $this->user_type = $this->session->userdata('USERTYPE');
        }
        //pre($session_login);

        if ($session_login <= 0) {
            $this->data['user_detail'] = array();
            // Allow some methods?


            $allowed = array(
                'login', 'login_check', 'faq', 'register', 'reset', 'become_model', 'signup_user', 'load_models', 'model_details', 'explore', 'load_posts', 'get_model_by_id', 'live', '',
            );
            $allowed = array('home', 'model');
            $extra = array('model');
            // pre($this->uri->segment(1));
            if (!in_array($this->uri->segment(1), $allowed)) {
                redirect(base_url('home/login'));
            }
        } else if ($session_login && $session_login == TRUE) {
            $this->data['user_detail'] = $this->db->get_where('tbl_users', array('id' => $this->user_id))->row_array();
            $this->data['user_id'] = $this->data['user_detail']['id'];
            $this->data['counts'] = $this->Admin->siteCount_web($this->data['user_id']);
            if ($this->data['user_detail']['type'] == 'model') {
                $this->data['user_image'] = checkImage(2, $this->data['user_detail']['profile_image']);
                $this->data['cover_image'] = checkImage(2, $this->data['user_detail']['cover_image']);
            } else if ($this->data['user_detail']['type'] == 'user') {
                $this->data['user_image'] = checkImage(1, $this->data['user_detail']['profile_image']);
            }

            $notallowed = array('login');
            // pre($this->uri->segment(2));
            if (in_array($this->uri->segment(2), $notallowed)) {
                //pre($this->data['user_detail']['type']);
                if ($this->data['user_detail']['type'] == 'model') {
                    redirect(base_url('account/application'));
                } else {
                    redirect(base_url('account/dashboard'));
                }
            }
            try {
                $commonModules = $this->Admin->getCommonModule();
                // all modules
                $allModules = $this->Admin->getAllModule();
                // modules based on user role
                $userRights = $this->Admin->getUserRights($this->user_type);
                $this->access = $this->set_rights($allModules, $userRights, $commonModules);
            } catch (Exception $ex) {
                $this->session->set_flashdata('error', $ex->getMessage());
            }
        }
    }

    public function textWatermark($source_image)
    {
        $config['source_image'] = FCPATH . SITE_UPD . '/models' . '/' . $source_image;
        //The image path,which you would like to watermarking
        $config['wm_text'] = 'RoyalFruit-darpnnnnn';
        $config['wm_type'] = 'text';
        // $config['wm_font_path'] = './fonts/atlassol.ttf';
        $config['wm_font_size'] = 16;
        $config['wm_font_color'] = '#ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_hor_alignment'] = 'right';
        $config['wm_padding'] = '20';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            return $this->image_lib->display_errors();
        }
    }
    public function overlayWatermark($source_image)
    {
        //$source_image = $this->image_create_gd($source_image);
        $config['image_library'] = 'gd2';
        $config['source_image'] = FCPATH . SITE_UPD . '/models' . '/' . $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = FCPATH . ADM_MEDIA . '/' . WATERMARK_IMG;     //the overlay image
        $config['wm_opacity'] = 50;
        //$config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'right';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
    }


    function format_interval(DateInterval $interval)
    {
        $result = "";
        if ($interval->y) {
            $result = $interval->format("%y years ");
        }
        if (empty($result) && $interval->m) {
            $result = $interval->format("%m months ");
        }
        if (empty($result) && $interval->d) {
            $result = $interval->format("%d days ");
        }
        if (empty($result) && $interval->h) {
            $result = $interval->format("%h hours ");
        }
        if (empty($result) && $interval->i) {
            $result = $interval->format("%i minutes ");
        }
        if (empty($result) && $interval->s) {
            $result = $interval->format("%s seconds ");
        }
        if (empty($result))
            $result = 'now';

        return $result;
    }

    //Listing Filteration
    function getDTFilters($post = array())
    {
        $filters = array(
            'offset' => isset($post['start']) ? intval($post['start']) : 0,
            'limit' => isset($post['length']) ? intval($post['length']) : 25,
            'sort' => isset($post['columns'][$post["order"][0]['column']]['data']) ? $post['columns'][$post["order"][0]['column']]['data'] : 'dCreatedDate',
            'order' => isset($post["order"][0]['dir']) ? $post["order"][0]['dir'] : 'DESC',
            'search' => isset($post["search"]['value']) ? $post["search"]['value'] : '',
            'sEcho' => isset($post['sEcho']) ? $post['sEcho'] : 1,
        );
        return $filters;
    }

    public function change_lang($lang = "english")
    {
        $this->session->set_userdata("user_lang", $lang);
        redirect(base_url(), "refresh");
    }

    public function upload_ck_file()
    {
        if (isset($_FILES['upload'])) {
            // ------ Process your file upload code -------
            $filen = $_FILES['upload']['tmp_name'];
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];
            $filename = md5(time() . rand()) . '.' . $extension;
            $con_images = SITE_UPD . 'ckeditor/' . $filename;
            move_uploaded_file($filen, FCPATH . $con_images);
            $url = base_url($con_images);
            /* $url = $con_images; */

            $funcNum = $_GET['CKEditorFuncNum'];
            // Optional: instance name (might be used to load a specific configuration file or anything else).
            $CKEditor = $_GET['CKEditor'];
            // Optional: might be used to provide localized messages.
            $langCode = $_GET['langCode'];

            // Usually you will only assign something here if the file could not be uploaded.
            $message = '';
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
        }
    }

    //Unique Email validation
    public function unique_email()
    {
        $id = $this->input->post('id');

        if (empty($id))
            $check = $this->db->get_where('tbl_users', array('email' => $this->input->post('email')))->num_rows();
        else
            $check = $this->db->get_where('tbl_users', array('email' => $this->input->post('email'), 'id !=' => $id))->num_rows();


        //pre($check);
        if ($check > 0) {
            $this->form_validation->set_message("unique_email", "%s already exists");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //Unique Email validation
    public function unique_mobile()
    {
        $id = $this->input->post('id');
        $country_code = '+257';

        if (empty($id))
            $check = $this->db->get_where('tbl_users', array('mobile' => $this->input->post('mobile')))->num_rows();
        else
            $check = $this->db->get_where('tbl_users', array('mobile' => $this->input->post('mobile'), 'id !=' => $id))->num_rows();
        if ($check > 0) {
            $this->form_validation->set_message("unique_mobile", $this->lang->line('err_number_taken'));
            return FALSE;
        } else {
            return TRUE;
        }
    }
    //Unique Email validation
    public function oldPassword()
    {
        $id = $this->input->post('id');
        $check = $this->db->get_where('tbl_users', array('password' => md5($this->input->post('old_password')), 'id' => $id))->num_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message("old_password", $this->lang->line('err_old_password'));
            return FALSE;
        }
    }

    private function set_rights($menus, $menuRights, $topmenu)
    {
        //  pre($menus);
        $data = array();
        for ($i = 0, $c = count($menus); $i < $c; $i++) {
            $row = array();
            for ($j = 0, $c2 = count($menuRights); $j < $c2; $j++) {
                if ($menuRights[$j]["rr_modulecode"] == $menus[$i]["mod_modulecode"]) {
                    if (
                        $this->authorize($menuRights[$j]["rr_create"]) || $this->authorize($menuRights[$j]["rr_edit"]) ||
                        $this->authorize($menuRights[$j]["rr_delete"]) || $this->authorize($menuRights[$j]["rr_view"])
                    ) {

                        $row["menu"] = $menus[$i]["mod_modulegroupcode"];
                        $row["menu_name"] = ($this->user_lang == 'en') ? $menus[$i]["mod_modulename"] : $menus[$i]["mod_modulename_lang"];
                        $row["page_name"] = $menus[$i]["mod_modulepagename"];
                        $row["image"] = $menus[$i]["vImage"];
                        $row["create"] = $menuRights[$j]["rr_create"];
                        $row["edit"] = $menuRights[$j]["rr_edit"];
                        $row["delete"] = $menuRights[$j]["rr_delete"];
                        $row["view"] = $menuRights[$j]["rr_view"];
                        $row["status"] = $menuRights[$j]["rr_status"];
                        $row["extra"] = $menuRights[$j]["extra"];
                        $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]] = $row;
                        $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_menu_name"] = ($this->user_lang == 'en') ? $menus[$i]["mod_modulegroupname"] : $menus[$i]["mod_modulegroupname_lang"];
                        if ($menus[$i]['mod_moduleorder'] == 1 || empty($data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_page_name"])) {
                            $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_page_name"] = $menus[$i]["mod_modulepagename"];
                            $data[$menus[$i]["mod_modulegroupcode"]][$menuRights[$j]["rr_modulecode"]]["top_image"] = $menus[$i]["vImage"];
                        }
                    }
                }
            }
        }
        return $data;
    }

    private function authorize($module)
    {
        return $module == "yes" ? TRUE : FALSE;
    }

    private function valid_url($user_type = '')
    {
        $allowed = array('dashboard', 'cpass', 'profile', 'logout', 'account');
        if (!empty($user_type)) {
            $controller = $this->uri->segment(2);
            $method = $this->uri->segment(3);
            if (in_array($controller, $allowed))
                return TRUE;
            $module_data = $this->common->get_data_by_id('mst_module', 'mod_modulepagename', $controller, '*', array(), '', '', 1, 'row');
            if (!empty($module_data)) {
                $rights = $this->common->get_data_by_id('mst_role_rights', 'rr_rolecode', $user_type, '*', array('rr_modulecode' => $module_data['mod_modulecode']));
                if (!empty($rights)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function load_version_model($path, $model)
    {
        if (file_exists(APPPATH . "models/" . $path . EXT)) {
            $this->load->model($path, $model);
        } else {
            $this->data['error'] = $model . ' Model could not load';
        }
    }

    public function upload_image($folder = 'users')
    {
        $data = $_POST['image'];
        if (!empty($data)) {
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $imageName = md5(time() . rand()) . '.jpeg';
            file_put_contents(FCPATH . SITE_UPD . $folder . '/' . $imageName, $data);
            return $imageName;
        } else {
            return $data;
        }
    }
}
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
