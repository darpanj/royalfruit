<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
//user defined constant
define('FCM_KEY', 'AAAAMJDjTJY:APA91bF1BCWj9UDG5OsWE6ZP5BL6HdejlhFzZfbY9VZ2qnlS_kNBPGEJf5zQoZtn8hbJDDyyKrtosXPCmY_Ow5tIwv4MX2c8UzuE6f_Oonox1UnjrLGIjm2e9zCxCUwOEpr5jinK3qZm');
define('SITE_UPD', 'themes/uploads/');
define('MODEL_UPD', 'themes/uploads/models/');
define('ADM_CSS', 'themes/admin/css/');
define('ADM_THEME_JS', 'themes/admin/js/');
define('ADM_JS', 'themes/admin/scripts/');
define('ADM_PLUGINS', 'themes/admin/plugins/');
define('ADM_IMG', 'themes/admin/img/');
define('ADM_URL', 'admin/');
define('ADM_MEDIA', 'themes/admin/media/');
define('ADM_THEME_VENDOR', 'themes/admin/vendors/');
define('SITE_IMG', '');
define('MEND_SIGN', '<span class="text-danger">*</span>');
define('PAGING_LIMIT', 10);
define('CUSTOM_SEPARATOR', '{{##}}');

define('STATUS_400', "400");
define('STATUS_200', "200");
define('SUCCESSFULLY_LOGIN', "Login successfully");
define('USER_NOT_ACTIVE', "User not active");
define('USER_NOT_EXIST', "User not exist");
define('REGISTER_SUCCESS', "Register successfully.");
define('PROFILE_UPDATED', "Profile successfully updated.");

define('FRONT_CSS', 'themes/front/css/');
define('FRONT_JS', 'themes/front/js/');
define('FRONT_IMG', 'themes/front/images/');
define('FRONT_FONTS', 'themes/front/fonts/');
define('FRONT_VIDEOS', 'themes/front/video/');
define('FRONT_THEME_ASSETS', 'themes/front/theme-assets/');
define('FRONT_THEME_ASSETS_IMG', 'themes/front/theme-assets/images/');
define('FRONT_THEME_ASSETS_CSS', 'themes/front/theme-assets/css/');
define('FRONT_THEME_ASSETS_VENDOR', 'themes/front/theme-assets/vendors/');
define('CORPORATE_FILE', 'themes/front/corporate/');
define('FRONT_URL', 'front/');
define('Stripe_test', 'pk_test_ugK8fpPkCWcB486RlNdRHTBH000UohU6qZ');
define('Stripe_secret', 'sk_test_ztSrE6QIkfdzYFLL7Giia6Lr00P1Uhfzf3');
define('Default_currency', 'usd');
define('WATERMARK_IMG', 'logo.png');
<<<<<<< HEAD
=======

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
define('MESSEGE_FOR_MODEL_USER_FOLLOWED', 'Followed You.');
