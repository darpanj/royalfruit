<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head lang="en">
    <noscript>
        <meta http-equiv="refresh" content="0; url=<?php echo base_url() . 'noscript'; ?>"/>
    </noscript>
    <base href="<?php echo base_url(); ?>"><!--end::Base Path -->
    <meta charset="utf-8"/>

    <title><?php echo 'Reset Password'; ?> | <?php echo SITENAME; ?></title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->
    <link href="<?php echo base_url() . ADM_CSS; ?>/pages/login/login-2.css" rel="stylesheet" type="text/css"/>
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/fullcalendar/fullcalendar.bundle.css"
          rel="stylesheet" type="text/css"/>
    <!--end::Page Vendors Styles -->


    <!--begin:: Global Mandatory Vendors -->
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/perfect-scrollbar/css/perfect-scrollbar.css"
          rel="stylesheet" type="text/css"/>
    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/tether/dist/css/tether.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-timepicker/css/bootstrap-timepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-daterangepicker/daterangepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-select/dist/css/bootstrap-select.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/select2/dist/css/select2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/ion-rangeslider/css/ion.rangeSlider.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/nouislider/distribute/nouislider.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/owl.carousel/dist/assets/owl.carousel.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/owl.carousel/dist/assets/owl.theme.default.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/dropzone/dist/dropzone.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/quill/dist/quill.snow.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/@yaireo/tagify/dist/tagify.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/summernote/dist/summernote.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-markdown/css/bootstrap-markdown.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/animate.css/animate.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/toastr/build/toastr.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/dual-listbox/dist/dual-listbox.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/morris.js/morris.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/socicon/css/socicon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/vendors/line-awesome/css/line-awesome.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/vendors/flaticon/flaticon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/vendors/flaticon2/flaticon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/@fortawesome/fontawesome-free/css/all.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/datatables/datatables.bundle.css" rel="stylesheet"
          type="text/css"/>
    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Styles(used by all pages) -->

    <link href="<?php echo base_url() . ADM_CSS; ?>/style.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href="<?php echo base_url() . ADM_CSS; ?>/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_CSS; ?>/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_CSS; ?>/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() . ADM_CSS; ?>/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="<?php echo base_url() . ADM_MEDIA; ?>/logos/favicon-16x16.png"/>
    <link href="<?php echo base_url(ADM_CSS . 'style.css'); ?>" rel="stylesheet" type="text/css"/>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery/dist/jquery.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/popper.js/dist/umd/popper.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap/dist/js/bootstrap.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/js-cookie/src/js.cookie.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/moment/min/moment.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/tooltip.js/dist/umd/tooltip.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/perfect-scrollbar/dist/perfect-scrollbar.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/sticky-js/dist/sticky.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/wnumb/wNumb.js" type="text/javascript"></script>
    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery-form/dist/jquery.form.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/block-ui/jquery.blockUI.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/bootstrap-datepicker.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/bootstrap-timepicker.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-daterangepicker/daterangepicker.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-maxlength/src/bootstrap-maxlength.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-select/dist/js/bootstrap-select.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-switch/dist/js/bootstrap-switch.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/bootstrap-switch.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/select2/dist/js/select2.full.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/ion-rangeslider/js/ion.rangeSlider.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/typeahead.js/dist/typeahead.bundle.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/handlebars/dist/handlebars.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/inputmask/dist/jquery.inputmask.bundle.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/inputmask/dist/inputmask/inputmask.date.extensions.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/nouislider/distribute/nouislider.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/owl.carousel/dist/owl.carousel.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/autosize/dist/autosize.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/clipboard/dist/clipboard.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/dropzone/dist/dropzone.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/dropzone.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/quill/dist/quill.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/@yaireo/tagify/dist/tagify.polyfills.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/@yaireo/tagify/dist/tagify.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/summernote/dist/summernote.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/markdown/lib/markdown.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-markdown/js/bootstrap-markdown.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/bootstrap-markdown.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/bootstrap-notify/bootstrap-notify.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/bootstrap-notify.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery-validation/dist/jquery.validate.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery-validation/dist/additional-methods.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/jquery-validation.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/toastr/build/toastr.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/dual-listbox/dist/dual-listbox.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/raphael/raphael.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/morris.js/morris.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/chart.js/dist/Chart.bundle.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/vendors/jquery-idletimer/idle-timer.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/waypoints/lib/jquery.waypoints.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/counterup/jquery.counterup.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/es6-promise-polyfill/promise.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/sweetalert2/dist/sweetalert2.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/js/vendors/sweetalert2.init.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery.repeater/src/lib.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery.repeater/src/jquery.input.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/jquery.repeater/src/repeater.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/general/dompurify/dist/purify.js"
            type="text/javascript"></script>
    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Bundle(used by all pages) -->

    <script src="<?php echo base_url() . ADM_THEME_JS; ?>scripts.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>/custom/datatables/datatables.bundle.js"
            type="text/javascript"></script>
    <!--end::Global Theme Bundle -->

    <!--begin::Page Scripts(used by this page) -->
    <!--<script src="<?php // echo base_url() . ADM_THEME_JS;                                    ?>/pages/dashboard.js" type="text/javascript"></script>-->
    <script src="<?php echo base_url() . ADM_THEME_JS; ?>/pages/login/login-general.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_JS; ?>/pages/crud/forms/validation/form-widgets.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js"></script>
    <style>
        .alert {
            margin: 0 !important;
        }
    </style>
    <script>
        var ADMIN_URL = '<?php echo base_url() ?>';
        var SITE_URL = '<?php echo base_url(); ?>';
        var SITE_IMG = '<?php echo base_url('themes') . '/'; ?>';
        var status_msg = "<?php echo $this->lang->line("lbl_warning_change_status"); ?>";
        var delete_msg = "<?php echo $this->lang->line("lbl_warning_delete"); ?>";
        var delete_at_one_msg = "<?php echo $this->lang->line("lbl_warning_plz_sel_at_one"); ?>";
        var delete_all_msg = "<?php echo $this->lang->line("lbl_warning_delete_all"); ?>";
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
             style="background-image: url(<?php echo base_url() . ADM_MEDIA; ?>/bg/bg-1.jpg);">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="<?php echo base_url() . ADM_MEDIA; ?>/logo.png" width="50%">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title"><?php echo $this->lang->line('lbl_header_reset_pass'); ?></h3>
                        </div>
                        <div class="custom-messages"></div>
                        <form class="kt-form login-form"
                              action="<?php echo base_url('home/reset_password/' . $token); ?>"
                              name="reset_pass" id="reset_pass" method="post">
                            <div class="input-group">
                                <input class="form-control" type="password"
                                       placeholder="<?php echo $this->lang->line('lbl_reset_pass_place_new_pass'); ?>"
                                       name="vPassword" id="vPassword" autocomplete="off">
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password"
                                       placeholder="<?php echo $this->lang->line('lbl_reset_pass_place_confirm_pass'); ?>"
                                       name="cPassword" id="cPassword">
                            </div>

                            <div class="kt-login__actions">
                                <button id="kt_login_signin_submit"
                                        class="btn btn-pill kt-login__btn-primary"><?php echo $this->lang->line('lbl_submit'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("submit", "#reset_pass", function () {
        $("#reset_pass").validate({
            ignore: [],
            rules: {
                vPassword: {required: true},
                cPassword: {required: true, equalTo: "#vPassword"}

            },
            messages: {
                vPassword: {
                    required: "&nbsp;Password is required"

                },
                cPassword: {
                    required: "&nbsp;Confirm password is required",
                    equalTo: "&nbsp;Confirm password must match"

                }
            },
        });
        if ($("#reset_pass").valid()) {
            return true;
        } else {
            return false;
        }
    });
    <?php if ($this->session->flashdata('success')): ?>
    showErrorMsg($(".custom-messages"), 'success', '<?php echo $this->session->flashdata('success'); ?>');
    <?php elseif ($this->session->flashdata('error')): ?>
    showErrorMsg($(".custom-messages"), 'danger', '<?php echo $this->session->flashdata('error'); ?>');
    <?php endif; ?>
</script>

</body>
</body>
<!-- END BODY -->
</html>