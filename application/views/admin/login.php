<script>
    var username_req = '<?php echo $this->lang->line("user_err_username_req"); ?>';
    var pass_req = '<?php echo $this->lang->line("err_pswd_req"); ?>';
</script>
<?php if ($method == 'reset') { ?>
    <div class="single-page-block login-div">
        <div class="single-page-block-inner effect-3d-element">
            <div class="blur-placeholder"><!-- --></div>
            <div class="single-page-block-form">
                <h3 class="text-center">
                    <i class="icmn-enter margin-right-10"></i>
                    <?php echo $headTitle; ?>
                </h3>
                <br />
                <form name="form_reset" id="form_reset" action="<?php echo base_url() . $this->ADM_URL . 'login/reset/' . $token; ?>" method="post">
                    <input type="hidden" name="action" value="submit_reset">
                    <input type="hidden" name="vToken" value="<?php echo $token; ?>">
                    <div class="form-group">
                        <div class="input-icon">
                            <input class="form-control placeholder-no-fix" type="password" placeholder="<?php echo $this->lang->line('lbl_reset_pass_place_new_pass'); ?>" name="npassword" id="npassword"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-icon">
                            <input class="form-control placeholder-no-fix" type="password" placeholder="<?php echo $this->lang->line('lbl_reset_pass_place_confirm_pass'); ?>" name="cpassword" id="cpassword"/>
                        </div>
                    </div>
                    <div class="form-actions">
                        <a  href="<?php echo base_url() . $this->ADM_URL; ?>login" class="btn">
                            <i class="m-icon-swapleft"></i> <?php echo $this->lang->line('lbl_login'); ?> </a>
                        <button type="submit" class="btn btn-primary green-haze pull-right">
                            <?php echo $this->lang->line('lbl_submit'); ?> <i class="m-icon-swapright m-icon-white"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on("submit", "#form_reset", function () {
            $("#form_reset").validate({
                ignore: [],
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                rules: {
                    npassword: {required: true},
                    cpassword: {required: true, equalTo: "#npassword"}

                },
                messages: {
                    npassword: {
                        required: "&nbsp;<?php echo $this->lang->line('err_reset_pass_new_pass_req'); ?>"
                    },
                    cpassword: {
                        required: "&nbsp;<?php echo $this->lang->line('err_reset_pass_confirm_pass_req'); ?>",
                        equalTo: "&nbsp;<?php echo $this->lang->line('err_reset_pass_confirm_not_match'); ?>"
                    }
                },
                errorPlacement: function (error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            if ($("#form_reset").valid()) {
                return true;
            } else {
                return false;
            }
        });
    </script>
<?php } else { ?>
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(<?php echo base_url() . ADM_MEDIA; ?>/bg/bg-1.jpg);">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="<?php echo base_url() . ADM_MEDIA; ?>/logo.png">
                            </a>
                        </div>
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title"><?php echo $this->lang->line('lbl_header_login_form'); ?></h3>
                            </div>
                            <form class="kt-form login-form" action="javascript:;" name="form_login" id="form_login" method="post">
                                <input type="hidden" name="action" value="submit_login">
                                <div class="input-group">
                                    <input class="form-control" type="text"  placeholder="<?php echo $this->lang->line('lbl_login_place_username'); ?>" name="vEmail"  value="<?php echo $email; ?>" autocomplete="off">
                                </div>

                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="<?php echo $this->lang->line('lbl_login_place_Password'); ?>" name="vPassword" value="<?php echo $password; ?>">
                                </div>
                                <div class="row kt-login__extra">
                                    <div class="col">
                                        <label class="kt-checkbox">
                                            <input type="checkbox" name="isremember" value="1" <?php echo $isremember; ?>> <?php echo $this->lang->line('lbl_remember_me'); ?>
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col kt-align-right">
                                        <a href="javascript:;" id="kt_login_forgot" class="kt-link kt-login__link"><?php echo $this->lang->line('lbl_forgot_pass_link'); ?></a>
                                    </div>
                                </div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_signin_submit" type="submit"  class="btn btn-pill kt-login__btn-primary"><?php echo $this->lang->line('lbl_login'); ?></button>
                                </div>
                            </form>
                        </div>
                        <div class="kt-login__forgot">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title"><?php echo $this->lang->line('lbl_par_forgot_pass'); ?></h3>
                            </div>
                            <form class="kt-form" name="form_forgot" id="form_forgot" action="javascript:" method="post">
                                <input type="hidden" name="action" value="submit_forgot">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="<?php echo $this->lang->line('lbl_forgot_place_email'); ?>" name="vEmail" id="femail" autocomplete="off">
                                </div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_forgot_submit" class="btn btn-pill kt-login__btn-primary"><?php echo $this->lang->line('lbl_send'); ?></button>&nbsp;&nbsp;
                                    <button id="kt_login_forgot_cancel" class="btn btn-pill kt-login__btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url() . ADM_JS; ?>login.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            Login.init();
        });
    </script>
<?php } ?>
