<style>
    .timeline_new {
        border-top: 1px solid #ebedf2;
    }

    .timeline_new .kt-widget__item {
        display: flex;
    }

    .timeline_new p {
        margin: 0;
    }

    .custome_icon_size {
        font-size: 35px;
    }

    .timeline_new .kt-widget__title,
    .timeline_new .kt-widget__value {
        display: block;
        color: #595d6e;
        font-weight: 600;
        font-size: .95rem;
    }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Applications/User/Profile3-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<?php echo $user['profile_image']; ?>"
                                     alt="image">
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                JM
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        <?php echo ucfirst($user['name']); ?>
                                    </a>
                                </div>

                                <div class="kt-widget__subhead">

                                    <a href="javascript:void(0);"><i
                                            class="flaticon2-calendar-3"></i> <?php echo ucfirst($user['type']); ?>
                                    </a>
                                    <a href="javascript:void(0);"><i
                                            class="flaticon-multimedia-2"></i><?php echo $user['email']; ?>
                                    </a>
                                </div>

                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        <br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Sent Friend Requests</h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget12">
                        <div class="kt-widget12__content pb-0">
                            <table class="mb-0 table table-striped- table-bordered table-hover table-checkable"
                                   id="listSentFriendRequest">
                                <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('lbl_sr_no') ?></th>
                                    <th>Model Name</th>
                                    <th>Model Image</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Friends</h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget12">
                        <div class="kt-widget12__content pb-0">
                            <table class="mb-0 table table-striped- table-bordered table-hover table-checkable"
                                   id="listFriends">
                                <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('lbl_sr_no') ?></th>
                                    <th>Model Name</th>
                                    <th>Model Image</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-md-3 col-ls-3">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Payment Details</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                        <?php if(!empty($payment_details)) { ?>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Payment Via</span>
                                                    <?php 
                                                        if($payment_details['payment_via'] == 1) {
                                                            $pVia = 'ACH(Direct Bank Deposit)-US Only';
                                                        } elseif ($payment_details['payment_via'] == 2) {
                                                            $pVia = 'Check (via mail)';
                                                        } elseif ($payment_details['payment_via'] == 3) {
                                                            $pVia = 'Paxum (Not Recommended)';
                                                        } else {
                                                            $pVia = 'N/A';
                                                        }

                                                    ?>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data"><?= $pVia ?></a>
                                                </div>
                                            </div>
                                            <?php if($payment_details['payment_via'] == 1) { ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Account Holder</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['account_holder'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Email Address</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['email_address'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Bank Name</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['bank_name'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Account Type</span>
                                                        <?php 
                                                            if($payment_details['account_type'] == 1) {
                                                                $pAT = 'Checking';
                                                            } elseif ($payment_details['account_type'] == 2) {
                                                                $pAT = 'Saving';
                                                            } else {
                                                                $pAT = 'N/A';
                                                            }

                                                        ?>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $pAT ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Routing Number</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['routing_number'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Account Number</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['account_number'] ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if($payment_details['payment_via'] == 2) { ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Pay to</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['pay_to'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Pay to</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['mailing_address'] ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if($payment_details['payment_via'] == 3) { ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Pay to</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['paxum_email_address'] ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } else { ?>
                                        <div class="kt-widget__item pb-0">
                                            <div class="kt-widget__contact">
                                                <h3>N/A</h3>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-3 col-ls-3">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Payments</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Upcoming Payment</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data">TBD</a>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Last Payment</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data">$<?= $last_payment ?></a>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Lifetime Paid</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data">$<?= $get_life_time_payment ?></a>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-3 col-ls-3">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">W9</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">W9 Form</span>
                                                <?php
                                                if(!empty($w9Form) && $w9Form['file_name'] != '') {
                                                    echo '<a href="javascript:void(0);"
                                                       class="kt-widget__data">Submitted</a>';
                                                } else {
                                                    echo 'N/A';
                                                }
                                                ?>
                                                    
                                                </div>
                                                <?php
                                                if(!empty($w9Form) && $w9Form['file_name'] != '') {
                                                    echo '<a href="'.site_url().'themes/uploads/w9form/'.$w9Form['file_name'].'" target="_blank">Uploaded Form</a>';
                                                }
                                                ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- echo '<a href="'.site_url().'themes/uploads/report/'.$data['report_document'].'" target="_blank">Report Document</a>'; -->


<script>
    $(function () {
        oTable = $('#listSentFriendRequest').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url($this->ADM_URL . $class . '/friend_request_lists/' . $user['id']); ?>",
            "columns": [
                {"data": "id", searchable: false, sortable: false},
                {"data": "model_name", sortable: false},
                {"data": "model_image", searchable: false, sortable: false},
                {"data": "date", searchable: false, sortable: false},
            ]
        });
    });
    $(function () {
        oTable = $('#listFriends').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url($this->ADM_URL . $class . '/friends_lists/' . $user['id']); ?>",
            "columns": [
                {"data": "id", searchable: false, sortable: false},
                {"data": "model_name", sortable: false},
                {"data": "model_image", searchable: false, sortable: false},
                {"data": "date", searchable: false, sortable: false},
            ]
        });
    });
</script>