<<<<<<< HEAD
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?php echo $headTitle; ?>
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right frm_profile" name="frm_profile" id="frm_profile" method="post"
              enctype="multipart/form-data" action="javascript:;">
            <input type="hidden" name="id" id="id" value="<?php echo $data['id']; ?>"/>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <div class="col-lg-12 mb-md-12 mb-3">
                        <label for="title"><?php echo MEND_SIGN; ?> <?php echo $this->lang->line('lbl_title'); ?>
                            :</label>
                        <input class="form-control" type="text"
                               placeholder="<?php echo $this->lang->line('placeholder_title'); ?>" id="title"
                               name="title" value="<?php echo $data['title']; ?>">
                    </div>
                    <div class="col-lg-12 mb-md-12">
                        <label for="content"><?php echo MEND_SIGN; ?> <?php echo $this->lang->line('lbl_Content'); ?>
                            :</label>
                        <div id="content"><?php echo $data['content']; ?></div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-success"
                                    id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                            <a href="javascript:back_portlet();"
                               class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('user_lbl_img'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>


            </div>
            <div class="modal-body">
                <label><b>Note : </b>
                    <small style="text-align: center;">Zoom the image in or out using the scrollbar at the bottom. If
                        the mouse has a wheel, the wheel will also zoom. You can also adjust the image to the left, to
                        the right, up or down.</small>
                </label>
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel
                </button>
                <button type="button" id="cropImageBtn"
                        class="btn btn-primary">Crop
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        let theEditor;
        ClassicEditor
            .create(document.querySelector('#content'))
            .then(editor => {
                theEditor = editor;
            })
            .catch(error => {
                console.error(error);
            });

        validator = $("#frm_profile").validate({
            rules: {
                title: {required: true},
            },
            messages: {
                title: {
                    required: "<?php echo $this->lang->line('placeholder_title'); ?>",
                },
            },
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTo('m_form_1_msg', -200);
            },
            submitHandler: function (form) {
                var data = new FormData($('#frm_profile')[0]);
                data.append('conetnt', theEditor.getData());
                $.ajax({
                    type: 'post',
                    data: data,
                    dataType: "json",
                    url: "<?php echo base_url($this->ADM_URL . strtolower($class) . '/add_edit_data'); ?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (r) {
                        if (r.status == 200) {
                            sType = getStatusText(r.status);
                            sText = r.message;
                            Custom.myNotification(sType, sText);
                            $('.portlet-toggler').toggle();
                            oTable.draw();
                            return false;
                        } else {
                            console.log(r);
                            $(".kt-alert__text").html(r.message);
                            $('#kt_form_1_msg').removeClass('kt-hidden').show();
                            sType = getStatusText(r.status);
                            sText = r.message;
                            Custom.myNotification(sType, sText);
                            KTUtil.scrollTo('m_form_1_msg', -200);
                        }
                    },
                    complete: removeOverlay
                });
            }
        });

    })

</script>
=======
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?php echo $headTitle; ?>
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right frm_profile" name="frm_profile" id="frm_profile" method="post"
              enctype="multipart/form-data" action="javascript:;">
            <input type="hidden" name="id" id="id" value="<?php echo $data['id']; ?>"/>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <div class="col-lg-12 mb-md-12 mb-3">
                        <label for="title"><?php echo MEND_SIGN; ?> <?php echo $this->lang->line('lbl_title'); ?>
                            :</label>
                        <input class="form-control" type="text"
                               placeholder="<?php echo $this->lang->line('placeholder_title'); ?>" id="title"
                               name="title" value="<?php echo $data['title']; ?>">
                    </div>
                    <div class="col-lg-12 mb-md-12">
                        <label for="content"><?php echo MEND_SIGN; ?> <?php echo $this->lang->line('lbl_Content'); ?>
                            :</label>
                        <div id="content"><?php echo $data['content']; ?></div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-success"
                                    id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                            <a href="javascript:back_portlet();"
                               class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('user_lbl_img'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>


            </div>
            <div class="modal-body">
                <label><b>Note : </b>
                    <small style="text-align: center;">Zoom the image in or out using the scrollbar at the bottom. If
                        the mouse has a wheel, the wheel will also zoom. You can also adjust the image to the left, to
                        the right, up or down.</small>
                </label>
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel
                </button>
                <button type="button" id="cropImageBtn"
                        class="btn btn-primary">Crop
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        let theEditor;
        ClassicEditor
            .create(document.querySelector('#content'))
            .then(editor => {
                theEditor = editor;
            })
            .catch(error => {
                console.error(error);
            });

        validator = $("#frm_profile").validate({
            rules: {
                title: {required: true},
            },
            messages: {
                title: {
                    required: "<?php echo $this->lang->line('placeholder_title'); ?>",
                },
            },
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTo('m_form_1_msg', -200);
            },
            submitHandler: function (form) {
                var data = new FormData($('#frm_profile')[0]);
                data.append('conetnt', theEditor.getData());
                $.ajax({
                    type: 'post',
                    data: data,
                    dataType: "json",
                    url: "<?php echo base_url($this->ADM_URL . strtolower($class) . '/add_edit_data'); ?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (r) {
                        if (r.status == 200) {
                            sType = getStatusText(r.status);
                            sText = r.message;
                            Custom.myNotification(sType, sText);
                            $('.portlet-toggler').toggle();
                            oTable.draw();
                            return false;
                        } else {
                            console.log(r);
                            $(".kt-alert__text").html(r.message);
                            $('#kt_form_1_msg').removeClass('kt-hidden').show();
                            sType = getStatusText(r.status);
                            sText = r.message;
                            Custom.myNotification(sType, sText);
                            KTUtil.scrollTo('m_form_1_msg', -200);
                        }
                    },
                    complete: removeOverlay
                });
            }
        });

    })

</script>
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
