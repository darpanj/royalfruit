<style>
    .timeline_new {
        border-top: 1px solid #ebedf2;
    }

    .timeline_new .kt-widget__item {
        display: flex;
    }

    .timeline_new p {
        margin: 0;
    }

    .custome_icon_size {
        font-size: 35px;
    }

    .timeline_new .kt-widget__title,
    .timeline_new .kt-widget__value {
        display: block;
        color: #595d6e;
        font-weight: 600;
        font-size: .95rem;
    }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Applications/User/Profile3-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <!-- <div class="kt-widget__media kt-hidden-">
                            </div> -->
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                JM
                            </div>
                            <div class="kt-widget__content">
                                <!-- <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        <?php echo ucfirst($user['model_name']); ?>
                                    </a>
                                    <a href="#" class="kt-widget__username">
                                        <?php echo ucfirst($user['user_name']); ?>
                                    </a>
                                </div>

                                <div class="kt-widget__subhead">

                                    <a href="javascript:void(0);"><i
                                            class="flaticon2-calendar-3"></i> <?php echo $user['payment_id']; ?>
                                    </a>
                                    <a href="javascript:void(0);"><i
                                            class="flaticon-multimedia-2"></i><?php echo $user['follow_start_date']; ?>
                                    </a>
                                    <a href="javascript:void(0);"><i
                                            class="flaticon-multimedia-2"></i><?php echo $user['follow_end_date']; ?>
                                    </a>
                                </div> -->

                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        <div class="kt-widget__item mt-2">
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">Model Name:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $user['model_name']; ?></a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">User Name:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $user['user_name']; ?></a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">Payment ID:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $user['payment_id']; ?></a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label"> Instruction:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $user['instruction']; ?></a>
                                            </div>
                                            <?php
                                                if($user['is_model_deleted'] == 1) {
                                                    $is_model_deleted = 'Yes';
                                                } else {
                                                    $is_model_deleted = 'No';
                                                }
                                            ?>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label"> Is model deleted?:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $is_model_deleted; ?></a>
                                            </div>
                                            <?php
                                                if($user['is_user_deleted'] == 1) {
                                                    $is_user_deleted = 'Yes';
                                                } else {
                                                    $is_user_deleted = 'No';
                                                }
                                            ?>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label"> Is user deleted?:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $is_user_deleted; ?></a>
                                            </div>
                                            <?php
                                                if($user['request_status'] == 2) {
                                                    $request_status = 'Accepted';
                                                } elseif($user['request_status'] == 3) {
                                                    $request_status = 'Declined';
                                                } else {
                                                    $request_status = 'Requested';
                                                }
                                            ?>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label"> Request status:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $request_status; ?></a>
                                            </div>
                                            <?php if($user['declined_reason'] != '') { ?>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">Declined reason:</span>
                                                <a href="javascript:void(0);"
                                                   class="kt-widget__data"><?php echo $user['declined_reason']; ?></a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



