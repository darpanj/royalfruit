<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <?php echo $headTitle; ?>
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" name="form_documents" id="form_documents" method="post"
          action="<?php echo base_url() . $this->ADM_URL . 'documents' ?>" enctype="multipart/form-data">
        <input type="hidden" name="action" value="submit_data"/>
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                    </div>
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"
                       for="image">Model Agreement<?php echo MEND_SIGN; ?></label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" class="item-img file center-block form-control" name="agreement_model" id="agreement_model" accept="image/*"/>
                    <?php 
                        if(!empty($documents)) {
                            if($documents['agreement_model'] != '') {
                                echo '<a href="'.site_url().'themes/uploads/agreements/'.$documents['agreement_model'].'" target="_blank">Uploaded Image</a>';
                            }
                        }
                    ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"
                       for="image">2257 Agreement<?php echo MEND_SIGN; ?></label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" class="item-img file center-block form-control" name="agreement_2257" id="agreement_2257" accept="image/*"/>
                    <?php 
                        if(!empty($documents)) {
                            if($documents['agreement_2257'] != '') {
                                echo '<a href="'.site_url().'themes/uploads/agreements/'.$documents['agreement_2257'].'" target="_blank">Uploaded Image</a>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class=" ">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success"
                                id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                        <a href="<?php echo base_url($this->ADM_URL); ?>"
                           class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>
<script type="text/javascript">
    $(document).on('submit', '#form_documents', function (e) {
        $("#form_documents").validate({
            rules: {
                agreement_model: {  
                  required: true,
                  accept: "image/*",
                },
                agreement_2257: {  
                  required: true,
                  accept: "image/*",
                },
            },
            messages: {
                agreement_model: {
                    required: "Image Must be required!",
                },
                agreement_2257: {
                    required: "Image Must be required!",
                },
            },
            ignore: [],
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTo('m_form_1_msg', -200);
            },
        });
        if ($("#form_documents").valid()) {
            return true;
        } else {
            return false;
        }
    });
</script>

