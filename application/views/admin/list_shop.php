<div class="portlet-toggler">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title"><?php echo $headTitle; ?></h3>

                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <?php echo $bradcrumb; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-list"></i>
                </span>
                    <h3 class="kt-portlet__head-title">
                        <?php echo $headTitle; ?>
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <?php if (array_key_exists('SHOP', $this->access) && $this->access['SHOP']['SHOP']['create'] == 'yes') { ?>
                                <a href="javascript:void(0);" data-id="0"
                                   title="<?php echo $this->lang->line('lbl_add') . ' Shop'; ?>"
                                   data-type="form"
                                   data-url="<?php echo base_url($this->ADM_URL . strtolower($class) . '/add_edit_view'); ?>"
                                   class="btn btn-brand btn-elevate btn-icon-sm btnEdit">
                                    <i class="la la-plus"></i>
                                    <?php echo $this->lang->line('lbl_add') . ' Shop'; ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="listResults">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="portlet-toggler pageform" style="display: none"></div>
<script type="text/javascript">
    $(document).ready(function () {
        oTable = $('#listResults').DataTable({
            responsive: true,
            "processing": true,
            "serverSide": true,
            "order": [[0, "DESC"]],
            "ajax": "<?php echo base_url($this->ADM_URL . $class . '/lists/'); ?>",
            "columns": [
                {"data": "id", searchable: false, sortable: false},
                {"data": "image", searchable: false, sortable: false},
                {"data": "name"},
                {"data": "price"},
                {"data": "status", searchable: false, sortable: false},
                {"data": "action", searchble: false, sortable: false}
            ],
            drawCallback: function (oSettings) {
                $('.status-switch').bootstrapSwitch();
            }
        });
    });
</script>