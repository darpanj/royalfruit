<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <?php echo $headTitle; ?>
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" name="form_getting_started" id="form_getting_started" method="post"
          action="<?php echo base_url() . $this->ADM_URL . 'make_money' ?>" enctype="multipart/form-data">
        <input type="hidden" name="action" value="submit_data"/>
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                    </div>
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"
                       for="image">Image File<?php echo MEND_SIGN; ?></label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" class="item-img file center-block form-control" name="image" id="image" accept="image/*"/>
                    <?php 
                        if(!empty($make_money)) {
                            if($make_money['image'] != '') {
                                echo '<a href="'.site_url().'themes/uploads/make_money/'.$make_money['image'].'" target="_blank">Uploaded Image</a>';
                            }
                        }
                    ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"
                       for="image">PDF File<?php echo MEND_SIGN; ?></label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" class="item-img file center-block form-control" name="pdf" id="pdf" accept="application/pdf"/>
                    <?php 
                        if(!empty($make_money)) {
                            if($make_money['pdf'] != '') {
                                echo '<a href="'.site_url().'themes/uploads/make_money/'.$make_money['pdf'].'" target="_blank">Uploaded PDF</a>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class=" ">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success"
                                id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                        <a href="<?php echo base_url($this->ADM_URL); ?>"
                           class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>
<script type="text/javascript">
    $(document).on('submit', '#form_getting_started', function (e) {
        $("#form_getting_started").validate({
            rules: {
                image: {  
                  required: true,
                  accept: "image/*",
                },
                pdf: {  
                  required: true,
                  accept: "application/pdf",
                },
            },
            messages: {
                image: {
                    required: "Image Must be required!",
                },
                pdf: {
                    required: "PDF Must be required!",
                },
            },
            ignore: [],
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTo('m_form_1_msg', -200);
            },
        });
        if ($("#form_getting_started").valid()) {
            return true;
        } else {
            return false;
        }
    });
</script>

