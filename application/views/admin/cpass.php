<script type="text/javascript" src="<?php echo base_url() . ADM_JS; ?>jquery.crypt.js"></script>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>                            
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <?php echo $headTitle; ?>
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" name="form_cpass" id="form_cpass" method="post" action="<?php echo base_url() . $this->ADM_URL . 'cpass' ?>">
        <input type="hidden" name="action" value="submit_cpass" />
        <input type="hidden" name="passvalue" id="passvalue" value="<?php echo $passvalue; ?>">
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                    </div>	
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>	
                    </div>			  	
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('old_password') ?><?php echo MEND_SIGN; ?>:</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="password" name="opassword" id="opassword" class="form-control" placeholder="<?php echo $this->lang->line('old_password') ?>" data-error-container="#error_opassword">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('new_password') ?><?php echo MEND_SIGN; ?></label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="password" name="npassword" id="npassword" class="form-control" placeholder="<?php echo $this->lang->line('new_password') ?>" data-error-container="#error_npassword">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('comfirm_password') ?><?php echo MEND_SIGN; ?></label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="<?php echo $this->lang->line('comfirm_password') ?>" data-error-container="#error_cpassword">
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class=" ">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success" id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                        <a href="<?php echo base_url($this->ADM_URL); ?>" class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>
<script>
    $.validator.addMethod("OldPswCheck", function (value, element) {
        var oldpswtxt = $().crypt({method: "md5", source: value});
        var oldpswval = $("#passvalue").val();
        return (oldpswval == oldpswtxt);
    }, "");
    $.validator.addMethod("NewPswCheck", function (value, element) {
        var newpswtxt = $("#npassword").crypt({method: "md5", source: value});
        var oldpswval = $("#passvalue").val();
        return (oldpswval != newpswtxt);
    }, "");
    $("#form_cpass").validate({
        rules: {
            opassword: {
                required: true,
                OldPswCheck: true
            },
            npassword: {
                required: true,
                minlength: 6,
                NewPswCheck: true,
//                passcheck: true

            },
            cpassword: {
                required: true,
                equalTo: "#npassword",
//                passcheck: true
            }
        },
        messages: {
            opassword: {
                required: "&nbsp;<?php echo $this->lang->line('err_change_pass_old_pass_req') ?>",
                OldPswCheck: "&nbsp;<?php echo $this->lang->line('err_change_pass_old_pass_not_correct') ?>"
            },
            npassword: {
                required: "&nbsp;<?php echo $this->lang->line('err_change_pass_new_pass_req') ?>",
                minlength: "&nbsp;<?php echo $this->lang->line('err_change_pass_at_least_6_req') ?>",
                NewPswCheck: "&nbsp;<?php echo $this->lang->line('err_change_pass_used_cur_plz_change') ?>",
//                passcheck: "<?php echo $this->lang->line('err_change_pass_not_valid') ?>"

            },
            cpassword: {
                required: "&nbsp;<?php echo $this->lang->line('err_change_pass_conf_req') ?>",
                equalTo: "&nbsp;<?php echo $this->lang->line('err_change_pass_should_same') ?>",
//                passcheck: "<?php echo $this->lang->line('err_change_pass_not_valid') ?>"

            }
        },
    });

</script>