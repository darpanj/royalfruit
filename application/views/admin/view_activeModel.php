<style>
    .timeline_new {
        border-top: 1px solid #ebedf2;
    }

    .timeline_new .kt-widget__item {
        display: flex;
    }

    .timeline_new p {
        margin: 0;
    }

    .custome_icon_size {
        font-size: 35px;
    }

    .timeline_new .kt-widget__title,
    .timeline_new .kt-widget__value {
        display: block;
        color: #595d6e;
        font-weight: 600;
        font-size: .95rem;
    }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Applications/User/Profile3-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<?php echo $user['profile_image']; ?>"
                                     alt="image">
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                JM
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        <?php echo ucfirst($user['name']); ?>
                                    </a>
                                </div>

                                <div class="kt-widget__subhead">

                                    <a href="javascript:void(0);"><i
                                            class="flaticon2-calendar-3"></i> <?php echo ucfirst($user['type']); ?>
                                    </a>
                                    <a href="javascript:void(0);"><i
                                            class="flaticon-multimedia-2"></i><?php echo $user['email']; ?>
                                    </a>
                                    <a href="javascript:void(0);"><i
                                            class="flaticon-user"></i><?php echo $user['username']; ?>
                                    </a>
                                </div>

                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        <br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Sent Friend Requests</h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget12">
                        <div class="kt-widget12__content pb-0">
                            <table class="mb-0 table table-striped- table-bordered table-hover table-checkable"
                                   id="listSentFriendRequest">
                                <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('lbl_sr_no') ?></th>
                                    <th>Model Name</th>
                                    <th>Model Image</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Friends</h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget12">
                        <div class="kt-widget12__content pb-0">
                            <table class="mb-0 table table-striped- table-bordered table-hover table-checkable"
                                   id="listFriends">
                                <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('lbl_sr_no') ?></th>
                                    <th>Model Name</th>
                                    <th>Model Image</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-md-6 col-ls-6">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Payment Details</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                        <?php if(!empty($payment_details)) { ?>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Payment Via</span>
                                                    <?php 
                                                        if($payment_details['payment_via'] == 1) {
                                                            $pVia = 'ACH(Direct Bank Deposit)-US Only';
                                                        } elseif ($payment_details['payment_via'] == 2) {
                                                            $pVia = 'Check (via mail)';
                                                        } elseif ($payment_details['payment_via'] == 3) {
                                                            $pVia = 'Paxum (Not Recommended)';
                                                        } else {
                                                            $pVia = 'N/A';
                                                        }

                                                    ?>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data"><?= $pVia ?></a>
                                                </div>
                                            </div>
                                            <?php if($payment_details['payment_via'] == 1) { ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Account Holder</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['account_holder'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Email Address</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['email_address'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Bank Name</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['bank_name'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Account Type</span>
                                                        <?php 
                                                            if($payment_details['account_type'] == 1) {
                                                                $pAT = 'Checking';
                                                            } elseif ($payment_details['account_type'] == 2) {
                                                                $pAT = 'Saving';
                                                            } else {
                                                                $pAT = 'N/A';
                                                            }

                                                        ?>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $pAT ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Routing Number</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['routing_number'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Account Number</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['account_number'] ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if($payment_details['payment_via'] == 2) { ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Pay to</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['pay_to'] ?></a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Pay to</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['mailing_address'] ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if($payment_details['payment_via'] == 3) { ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Pay to</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $payment_details['paxum_email_address'] ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } else { ?>
                                        <div class="kt-widget__item pb-0">
                                            <div class="kt-widget__contact">
                                                <h3>N/A</h3>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-3 col-ls-3">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Payments</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Upcoming Payment</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data">TBD</a>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Last Payment</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data">$<?= $last_payment ?></a>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Lifetime Paid</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data">$<?= $get_life_time_payment ?></a>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-3 col-ls-3">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">W9</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">W9 Form</span>
                                                <?php
                                                if(!empty($w9Form) && $w9Form['file_name'] != '') {
                                                    echo '<a href="javascript:void(0);"
                                                       class="kt-widget__data">Submitted</a>';
                                                } else {
                                                    echo 'N/A';
                                                }
                                                ?>
                                                    
                                                </div>
                                                <?php
                                                if(!empty($w9Form) && $w9Form['file_name'] != '') {
                                                    echo '<a href="'.site_url().'themes/uploads/w9form/'.$w9Form['file_name'].'" target="_blank">Uploaded Form</a>';
                                                }
                                                ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-md-6 col-ls-6">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Settings</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                        <?php if(!empty($user)) { ?>
                                            <?php 
                                                $residency = ($user['residency'] != '') ? $user['residency'] : 'N/A';
                                            ?>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Residency</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data"><?= $residency ?></a>
                                                </div>
                                            </div>
                                            <?php 
                                                $mobile = ($user['mobile'] != '') ? $user['mobile'] : 'N/A';
                                            ?>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Phone</span>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data"><?= $mobile ?></a>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Display Location</span>
                                                    <?php 
                                                        if($user['display_location'] == 1) {
                                                            $displayLocation = 'Yes';
                                                        } else {
                                                            $displayLocation = 'No';
                                                        }

                                                    ?>
                                                    <a href="javascript:void(0);"
                                                       class="kt-widget__data"><?= $displayLocation ?></a>
                                                </div>
                                            </div>
                                            <?php 
                                                $country = ($user['country'] != '') ? $user['country'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Country</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $country ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $state = ($user['state'] != '') ? $user['state'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">State</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $state ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $city = ($user['city'] != '') ? $user['city'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">City</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $city ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $instagram_url = ($user['instagram_url'] != '') ? $user['instagram_url'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Instagram</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $instagram_url ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $twitter_url = ($user['twitter_url'] != '') ? $user['twitter_url'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Twitter</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $twitter_url ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $facebook_url = ($user['facebook_url'] != '') ? $user['facebook_url'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Facebook</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $facebook_url ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $tiktok_url = ($user['tiktok_url'] != '') ? $user['tiktok_url'] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Tiktok</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $tiktok_url ?></a>
                                                    </div>
                                                </div>
                                            <?php
                                                $subscriptionPriceArray = array(
                                                                    '1' => '$10/month',
                                                                    '2' => '$15/month',
                                                                    '3' => '$20/month',
                                                                    '4' => '$25/month',
                                                                    );
                                                $subscriptionPrice = ($user['subscription_price'] != 0) ? $subscriptionPriceArray[$user['subscription_price']] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Subscription Price</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $subscriptionPrice ?></a>
                                                    </div>
                                                </div>
                                            <?php
                                                $premiumPriceArray = array(
                                                                    '1' => '$10/month',
                                                                    '2' => '$15/month',
                                                                    '3' => '$20/month',
                                                                    '4' => '$25/month',
                                                                    );
                                                $premiumPrice = ($user['premium_price'] != 0) ? $premiumPriceArray[$user['premium_price']] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Premium Price</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $premiumPrice ?></a>
                                                    </div>
                                                </div>
                                            <?php
                                                $lsRateArray = array(
                                                                    '1' => '1 tokens/min',
                                                                    '2' => '2 tokens/min',
                                                                    '3' => '3 tokens/min',
                                                                    '4' => '4 tokens/min',
                                                                    );
                                                $liveStreamingRate = ($user['livestreaming_rate'] != 0) ? $lsRateArray[$user['livestreaming_rate']] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Livestream Viewing Rate</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $liveStreamingRate ?></a>
                                                    </div>
                                                </div>
                                            <?php
                                                $ls1ON1Array = array(
                                                                    '1' => '1 tokens/min',
                                                                    '2' => '2 tokens/min',
                                                                    '3' => '3 tokens/min',
                                                                    '4' => '4 tokens/min',
                                                                    );
                                                $liveStreaming1ON1Rate = ($user['livestreaming_rate_1_on_1'] != 0) ? $ls1ON1Array[$user['livestreaming_rate_1_on_1']] : 'N/A';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">1-on-1 Livestream With FanCam Rate</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $liveStreaming1ON1Rate ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $allow_fancam_request = ($user['allow_fancam_request'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Allow FanCam Requests</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $allow_fancam_request ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $allow_stream_request = ($user['allow_stream_request'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Allow Stream Requests</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $allow_stream_request ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $new_subscriber = ($user['new_subscriber'] -= 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">New Subscriber</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $new_subscriber ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $new_content_purchase = ($user['new_content_purchase'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">New Content Purchase</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $new_content_purchase ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $new_private_message = ($user['new_private_message'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">New Private Message</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $new_private_message ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $receive_latest_news_updates = ($user['receive_latest_news_updates'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Receive latest news and updates</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $receive_latest_news_updates ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $new_model_friend_request = ($user['new_model_friend_request'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">New Model Friend Request</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $new_model_friend_request ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $available_in_premium_video_store = ($user['available_in_premium_video_store'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Premium Content</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $available_in_premium_video_store ?></a>
                                                    </div>
                                                </div>
                                            <?php 
                                                $allow_my_friends = ($user['allow_my_friends'] == 1) ? 'Yes' : 'No';
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Model Friend Feature</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $allow_my_friends ?></a>
                                                    </div>
                                                </div>
                                        <?php } else { ?>
                                        <div class="kt-widget__item pb-0">
                                            <div class="kt-widget__contact">
                                                <h3>N/A</h3>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="col-xl-6 col-md-6 col-ls-6">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">ID Information</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                        <?php if(!empty($user)) { ?>
                                            <?php 

                                                $govtIssuedIDType = array(
                                                                    '1' => "Driver's License",
                                                                    '2' => 'State ID',
                                                                    '3' => 'Military ID',
                                                                    '4' => 'Non-US Passport',
                                                                    '5' => 'US Passport',
                                                                    );

                                                $government_issued_id_type = ($user['government_issued_id_type'] != 0) ? $govtIssuedIDType[$user['government_issued_id_type']] : "N/A"; 
                                            ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Government Issued ID Type</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $government_issued_id_type ?></a>
                                                    </div>
                                                </div>
                                                <?php
                                                $government_id_issued_by_state = ($user['government_id_issued_by_state'] != "") ? $user['government_id_issued_by_state'] : "N/A"; 
                                                ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Issued By (State/Province)</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $government_id_issued_by_state ?></a>
                                                    </div>
                                                </div>
                                                <?php
                                                  $months = array(
                                                      '01' => 'January',
                                                      '02' => 'February',
                                                      '03' => 'March',
                                                      '04' => 'April',
                                                      '05' => 'May',
                                                      '06' => 'June',
                                                      '07' => 'July ',
                                                      '08' => 'August',
                                                      '09' => 'September',
                                                      '10' => 'October',
                                                      '11' => 'November',
                                                      '12' => 'December',
                                                  );
                                                  $expiration = "N/A";
                                                    if($user['no_expiration'] == 1) {
                                                      $expiration = "No Expiration";
                                                    } else {
                                                        if($user['government_id_expiration_month'] != "" && $user['government_id_expiration_day'] != "" && $user['government_id_expiration_year'] != "") {
                                                                $expiration = $months[$user['government_id_expiration_month']].' '.$user['government_id_expiration_day'].', '.$user['government_id_expiration_year'];
                                                        }
                                                    }
                                                ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">ID Expiration</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $expiration ?></a>
                                                    </div>
                                                </div>
                                                <?php
                                                  if(!empty($user) && $user['legal_name'] != "") {
                                                    $legal_name = $user['legal_name'];
                                                  } else {
                                                    $legal_name = 'N/A';
                                                  }
                                                ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Full Legal Name</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $legal_name ?></a>
                                                    </div>
                                                </div>
                                                <?php
                                                  if(!empty($user)) {
                                                    if($user['dob_month'] != "" && $user['dob_day'] != "" && $user['dob_year'] != "") {
                                                      $dob = $months[$user['dob_month']].' '.$user['dob_day'].', '.$user['dob_year'];
                                                    } else {
                                                      $dob = 'N/A';
                                                    }
                                                  } else {
                                                    $dob = 'N/A';
                                                  }
                                                ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Date Of Birth</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $dob ?></a>
                                                    </div>
                                                </div>
                                                <?php
                                                  $alias_one = (!empty($user) && $user['alias_one'] != '') ? $user['alias_one'] : '';
                                                  $alias_two = (!empty($user) && $user['alias_two'] != '') ? $user['alias_two'] : '';
                                                  if($alias_one != '' || $alias_two != '') {
                                                ?>
                                                <div class="kt-widget__item pb-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Aliases/Stage Names</span>
                                                        <a href="javascript:void(0);"
                                                           class="kt-widget__data"><?= $alias_one ?><?= ', '.$alias_two ?></a>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                        <?php } else { ?>
                                        <div class="kt-widget__item pb-0">
                                            <div class="kt-widget__contact">
                                                <h3>N/A</h3>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Photo ID</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                        <?php if(!empty($user)) { ?>
                                            <?php 
                                                $photoIDImage = ($user['document_photo_id'] != '') ? base_url().MODEL_UPD . $user['document_photo_id'] : base_url() . FRONT_IMG . 'photo_id.svg';
                                            ?>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <img src="<?php echo $photoIDImage; ?>"
                                                             alt="image" height="auto" width="100%">
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                        <div class="kt-widget__item pb-0">
                                            <div class="kt-widget__contact">
                                                <h3>N/A</h3>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="col-md-6">
            <!--begin:: Widgets/Finance Summary-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Selfie with ID</h3>
                    </div>
                </div>

                <div class="kt-portlet__body p-0">
                    <div class="kt-widget08">
                        <div class="kt-widget12__content pb-0">
                            <div class="kt-portlet__body pt-1">
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__body">
                                        <?php if(!empty($user)) { ?>
                                            <?php 
                                                $verificationImage = ($user['document_id_verification_selfie'] != '') ? base_url().MODEL_UPD . $user['document_id_verification_selfie'] : base_url() . FRONT_IMG . 'selfie_id.svg';
                                            ?>
                                            <div class="kt-widget__item pb-0">
                                                <div class="kt-widget__contact">
                                                    <img src="<?php echo $verificationImage; ?>"
                                                             alt="image" height="auto" width="100%">
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                        <div class="kt-widget__item pb-0">
                                            <div class="kt-widget__contact">
                                                <h3>N/A</h3>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>

</div>

<!-- echo '<a href="'.site_url().'themes/uploads/report/'.$data['report_document'].'" target="_blank">Report Document</a>'; -->


<script>
    $(function () {
        oTable = $('#listSentFriendRequest').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url($this->ADM_URL . $class . '/friend_request_lists/' . $user['id']); ?>",
            "columns": [
                {"data": "id", searchable: false, sortable: false},
                {"data": "model_name", sortable: false},
                {"data": "model_image", searchable: false, sortable: false},
                {"data": "date", searchable: false, sortable: false},
            ]
        });
    });
    $(function () {
        oTable = $('#listFriends').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url($this->ADM_URL . $class . '/friends_lists/' . $user['id']); ?>",
            "columns": [
                {"data": "id", searchable: false, sortable: false},
                {"data": "model_name", sortable: false},
                {"data": "model_image", searchable: false, sortable: false},
                {"data": "date", searchable: false, sortable: false},
            ]
        });
    });
</script>