<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?php echo $headTitle; ?>
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" name="frm_profile" id="frm_profile" class="frm_profile" method="post"
              enctype="multipart/form-data" action="javascript:;">
            <input type="hidden" name="action" value="submit_profile"/>
            <input type="hidden" name="id" id="id" value="<?php echo $user['id']; ?>"/>
            <input type="hidden" name="image" id="image_val"/>
            <div class="kt-portlet__body">
                <div class="kt-form__content">
                    <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                        <div class="kt-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="kt-alert__text">
                        </div>
                        <div class="kt-alert__close">
                            <button type="button" class="close" data-close="alert" aria-label="Close">
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('lbl_image'); ?><?php echo MEND_SIGN; ?></label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <label class="cabinet center-block">
                            <figure class="business-box-logo">
                                <?php if ($this->data['user_detail']['type'] == 'vendor') { ?>
                                    <img src="<?php echo checkImage(1, $user['logo']); ?>"
                                         class="gambar img-responsive img-thumbnail" id="item-img-output"/>
                                     <?php } else { ?>
                                    <img src="<?php echo checkImage(1, $user['profile_image']); ?>"
                                         class="gambar img-responsive img-thumbnail" id="item-img-output"/>
                                     <?php } ?>
                                <div class="overlay">
                                    <i class="fa fa-camera"></i>
                                    <span>Upload</span>
                                </div>

                            </figure>
                            <input type="file" class="item-img file center-block" name="image_input" id="image_input"/>
                        </label>
                        <small><?php echo $this->lang->line('lbl_help_block_mlogo') ?></small>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('lbl_name'); ?><?php echo MEND_SIGN; ?></label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input class="form-control" type="text"
                               placeholder="<?php echo $this->lang->line('placeholder_ent_name'); ?>" id="name"
                               name="name" value="<?php echo $user['name']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('email'); ?><?php echo MEND_SIGN; ?></label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input class="form-control" type="text"
                               placeholder="<?php echo $this->lang->line('placeholder_ent_email'); ?>" id="email"
                               name="email" value="<?php echo $user['email']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('lbl_username'); ?><?php echo MEND_SIGN; ?></label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input class="form-control" type="text"
                               placeholder="<?php echo $this->lang->line('placeholder_ent_username'); ?>" id="username"
                               name="username" value="<?php echo $user['username']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('mobile_no'); ?><?php echo MEND_SIGN; ?></label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input class="form-control" type="text"
                               placeholder="<?php echo $this->lang->line('placeholder_ent_mobile'); ?>" id="mobile"
                               name="mobile" value="<?php echo $user['mobile']; ?>">
                    </div>
                </div>

                <?php if ($this->data['user_detail']['type'] == 'vendor') { ?>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3 col-sm-12"><?php echo $this->lang->line('lbl_location'); ?><?php echo MEND_SIGN; ?></label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <input class="form-control" type="text"
                                   placeholder="<?php echo $this->lang->line('plc_location_form'); ?>" id="location"
                                   name="location" value="<?php echo $user['location']; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div id="address_div">

                            <input type="hidden" data-geo="lat" value="<?php echo $user['longitude']; ?>" id="latitude"

                                   name="latitude">

                            <input type="hidden" data-geo="lng" value="<?php echo $user['latitude']; ?>" id="longitude"

                                   name="longitude">

                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="kt-portlet__foot">
                <div class=" ">
                    <div class="row">
                        <div class="col-lg-9 ml-lg-auto">
                            <button type="submit" class="btn btn-success"
                                    id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                            <a href="<?php echo base_url($this->ADM_URL . 'dashboard'); ?>"
                               class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('user_lbl_img'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>


            </div>
            <div class="modal-body">
                <label><b>Note : </b>
                    <small style="text-align: center;">Zoom the image in or out using the scrollbar at the bottom. If
                        the mouse has a wheel, the wheel will also zoom. You can also adjust the image to the left, to
                        the right, up or down.</small>
                </label>
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel
                </button>
                <button type="button" id="cropImageBtn"
                        class="btn btn-primary">Crop
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {

        $("#location").geocomplete({

            details: "#address_div",

            detailsAttribute: "data-geo"

        });

        var $pImageCrop, tempFilename, rawImg, imageId, file_check, number_check, link;
        file_check = false;
        number_check = false;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $('#cropImagePop').modal('show');
                    rawImg = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        $pImageCrop = $('#upload-demo').croppie({
            viewport: {
                width: 164,
                height: 200,
                type: 'square'
            },
            enforceBoundary: true,
            enableExif: true,
            showZoomer: true,
        });
        $('#cropImagePop').on('shown.bs.modal', function () {
            $pImageCrop.croppie('bind', {
                url: rawImg,
            }).then(function () {
                console.log('jQuery bind complete');
            });
        });

        $('#cropImageBtn').on('click', function (ev) {
            $pImageCrop.croppie('result', {
                type: 'base64',
                format: 'jpeg',
                size: {width: 164, height: 200}
            }).then(function (resp) {
                $('#image_val').val(resp);
                $('#item-img-output').attr('src', resp);
                $('#cropImagePop').modal('hide');
            });
        });

        $('#image_input').change(function () {
            var file_this = document.getElementById('image_input').files[0];
            if (file_this !== null) {
                if (file_this.size < 1000000) {
                    file_check = false;
                    var value = this.value;
                    var allow = ['.png', '.jpeg', '.jpg', '.jfif'];
                    allow.forEach(function (a) {
                        var value_check = value.toLocaleLowerCase();
                        if (value_check.indexOf(a) > 0) {
                            file_check = true;
                        }
                    });
                    if (file_check) {
                        readFile(this, 0);
                    } else {
                        $('#image_input').val('');
                        Custom.myNotification('error', "Please Enter Valid Image Format");
                    }
                } else {
                    $('#image_input').val('');
                    Custom.myNotification('error', "Image size should be less than 1 MB !");
                }
            }
        });

    });
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "<?php echo $this->lang->line('err_profile_nospace_username'); ?>");
    validator = $("#frm_profile").validate({
        rules: {
            username: {required: true, noSpace: true},
            name: {required: true},
            mobile: {required: true},
            email: {required: true, email: true},
        },
        messages: {
            username: {
                required: "<?php echo $this->lang->line('err_profile_enter_username'); ?>",
            },
            name: {
                required: "<?php echo $this->lang->line('err_profile_enter_name'); ?>",
            },
            mobile: {
                required: "<?php echo $this->lang->line('err_profile_enter_mobile'); ?>",
            },
            email: {
                required: "<?php echo $this->lang->line('err_profile_enter_email'); ?>",
                email: "<?php echo $this->lang->line('err_profile_enter_valid_email'); ?>",
            },
        },
        invalidHandler: function (event, validator) {
            var alert = $('#kt_form_1_msg');
            alert.removeClass('kt--hide').show();
            KTUtil.scrollTo('m_form_1_msg', -200);
        },
        submitHandler: function (form) {
            var data = new FormData($('#frm_profile')[0]);
            $.ajax({
                type: 'post',
                data: data,
                dataType: "json",
                url: "<?php echo base_url($this->ADM_URL . 'profile'); ?>",
                cache: false,
                contentType: false,
                processData: false,
                success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        Custom.myNotification(sType, sText);
                        window.location.href = "<?php echo base_url($this->ADM_URL . 'dashboard'); ?>";
                        return false;
                    } else {
                        $(".kt-alert__text").html(r.message);
                        $('#kt_form_1_msg').removeClass('kt-hidden').show();
                        KTUtil.scrollTo('m_form_1_msg', -200);
                    }
                },
                complete: removeOverlay
            });
        }
    });
</script>
