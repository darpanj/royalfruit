<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                <?php echo $headTitle; ?>
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="javascript:;" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                <?php echo $bradcrumb; ?>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <?php echo $headTitle; ?>
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" name="form_settings" id="form_settings" method="post"
          action="<?php echo base_url() . $this->ADM_URL . 'setting' ?>" enctype="multipart/form-data">
        <input type="hidden" name="action" value="submit_settings"/>
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                    </div>
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <?php
            foreach ($getFields as $k => $setrow) {
                $required = '';
                $mend_sign = '';
                $setrow = (array)$setrow;

                if ($setrow["vType"] == "file" && $setrow["vValue"] == "") {
                    $required = "required ";
                    $mend_sign = MEND_SIGN;
                }
                if ($setrow["eRequired"] == 'y') {
                    $required = "required ";
                    $mend_sign = MEND_SIGN;
                }
                $_label = $setrow["vLabelLang"];
                if ($this->user_lang == 'en')
                    $_label = $setrow["vLabel"];
                switch ($setrow["vType"]) {
                    case 'textbox':
                    case 'number':
                    case 'password':
                    {
                        ?>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12"
                                   for="<?php echo $setrow["iFieldId"]; ?>"><?php echo $_label; ?><?php echo $mend_sign; ?></label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <input class="form-control <?php
                                echo $required;
                                echo $setrow["vClass"];
                                ?>" type="<?php echo $setrow["vType"]; ?>" id="<?php echo $setrow["iFieldId"]; ?>"
                                       name="<?php echo $setrow["iFieldId"]; ?>"
                                       value="<?php echo $setrow["vValue"]; ?>">
                            </div>
                        </div>

                        <?php
                        break;
                    }
                    case 'file':
                    {
                        ?>

                        <!-- need to code -->

                        <?php
                        break;
                    }
                    case 'textarea':
                    {
                        ?>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12"
                                   for="<?php echo $setrow["iFieldId"]; ?>"><?php echo $_label; ?><?php echo $mend_sign; ?></label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                    <textarea class="form-control <?php
                                    echo $required;
                                    echo $setrow["vClass"];
                                    ?>" id="<?php echo $setrow["iFieldId"]; ?>"
                                              name="<?php echo $setrow["iFieldId"]; ?>"><?php echo $setrow["vValue"]; ?></textarea>
                            </div>
                        </div>
                        <?php
                        break;
                    }
                    case 'radio':
                    {
                        $options = explode(',', $setrow["vOptions"]);
                        ?>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12"
                                   for="<?php echo $setrow["iFieldId"]; ?>"><?php echo $_label; ?><?php echo $mend_sign; ?></label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <?php foreach ($options as $key => $value) { ?>
                                    <label class="radio-inline">
                                        <input type="radio" <?php echo $value == $setrow["vValue"] ? 'checked' : '' ?>
                                               data-error-container="#privacy_error_<?php echo $setrow["iFieldId"]; ?>"
                                               value="<?php echo $value; ?>"
                                               name="<?php echo $setrow["iFieldId"]; ?>"><?php echo $value; ?>
                                    </label>
                                <?php } ?>
                                <span id="privacy_error_<?php echo $setrow["iFieldId"]; ?>"></span>
                            </div>
                        </div>
                        <?php
                        break;
                    }
                }
            }
            ?>
        </div>
        <div class="kt-portlet__foot">
            <div class=" ">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success"
                                id="btn-submit-dev"><?php echo $this->lang->line('lbl_submit'); ?></button>
                        <a href="<?php echo base_url($this->ADM_URL); ?>"
                           class="btn btn-secondary"><?php echo $this->lang->line('lbl_cancel'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("extension", function (value, element, param) {
            param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
            return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
        }, $.validator.format("Only jpg|jpeg|png allowed."));

        $.validator.addClassRules('email_class', {
            email: true
        });
        $.validator.addClassRules('img_class', {
            extension: true
        });
    });
    $(document).on('submit', '#form_settings', function (e) {
        $("#form_settings").validate({
            ignore: [],
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTo('m_form_1_msg', -200);
            },
        });
        if ($("#form_settings").valid()) {
            return true;
        } else {
            return false;
        }
    });
</script>

