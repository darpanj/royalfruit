<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div 
        id="kt_aside_menu"
        class="kt-aside-menu "
        data-ktmenu-vertical="1"
        data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500"  
        >		
        <ul class="kt-menu__nav ">
            <?php
            if ($this->access > 0) {
                foreach ($this->access as $menu) {
                    $counter = 0;
                    foreach ($menu as $_mkey => $_mval) {
                        if (count($menu) > 1) {
                            if ($counter == 0) {
                                ?>
                                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"  data-ktmenu-submenu-toggle="hover">
                                    <a  href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                        <i class="kt-menu__link-icon <?php echo $_mval['top_image']; ?>"></i>
                                        <span class="kt-menu__link-text"><?php echo $_mval['top_menu_name']; ?></span>
                                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                    </a>
                                    <div class="kt-menu__submenu ">
                                        <span class="kt-menu__arrow"></span>
                                        <ul class="kt-menu__subnav">
                                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true" >
                                                <span class="kt-menu__link">
                                                    <span class="kt-menu__link-text"><?php echo $_mval['top_menu_name']; ?></span>
                                                </span>
                                            </li>
                                        <?php } else { ?>
                                            <li class="kt-menu__item " aria-haspopup="true" >
                                                <a  href="<?php echo ($_mval['page_name'] != '') ? base_url() . $this->ADM_URL . $_mval['page_name'] : 'javascript:;'; ?>" class="kt-menu__link ">
                                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                        <span></span>
                                                    </i>
                                                    <span class="kt-menu__link-text"><?php echo $_mval['menu_name']; ?></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        if ($counter == count($menu) - 1) {
                                            ?>
                                        </ul>
                                    </div>
                                </li>
                                <?php
                            }
                            $counter++;
                        } else {
                            ?>
                            <li class="kt-menu__item" aria-haspopup="true" >
                                <a  href="<?php echo ($_mval['page_name'] != '') ? base_url() . $this->ADM_URL . $_mval['top_page_name'] : 'javascript:;'; ?>" class="kt-menu__link ">
                                    <i class="kt-menu__link-icon <?php echo $_mval['top_image']; ?>"></i>
                                    <span class="kt-menu__link-text"><?php echo $_mval['top_menu_name']; ?></span>
                                </a>
                            </li>
                            <?php
                        }
                    }
                }
            }
            ?>
            <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo base_url('admin/logout'); ?>"
                   class="kt-menu__link ">
                    <i class="kt-menu__link-icon fas fa-sign-out-alt"></i>
                    <span class="kt-menu__link-text"><?php echo $this->lang->line('lbl_logout'); ?></span>
                </a>
            </li>
        </ul>
    </div>
</div>
