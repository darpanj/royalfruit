<div class="site-reg site-login">
    <section class="site-blocks-cover overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 align-self-center">

                    <div class="row">

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="login-form d-flex">
        <div class="container">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 m-auto reg-form">
                    <form data-parsley-validate="" method="post" action="<?php echo base_url('home/signup_user'); ?>">

                        <div class="col-12 mt-4">
                            <input type="text" class="form-control1" id="name" placeholder="<?php echo $this->lang->line('lbl_user_name'); ?>" name="name" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_name_required'); ?>">
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control1" id="email" placeholder="<?php echo $this->lang->line('lbl_email'); ?>" name="email" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_email_required'); ?>" data-parsley-type="email" data-parsley-type-message="<?php echo $this->lang->line('err_email_valid_type'); ?>">
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control1" id="confirm_email" placeholder="<?php echo $this->lang->line('lbl_confirm_email'); ?>" name="confirm_email" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_confirm_email_required'); ?>" data-parsley-type="email" data-parsley-type-message="<?php echo $this->lang->line('err_email_valid_type'); ?>" data-parsley-equalto="#email" data-parsley-equalto-message="<?php echo $this->lang->line('err_email_confirmEmail_same'); ?>">
                        </div>
                        <div class="col-12">
                            <input type="password" class="form-control1" placeholder="<?php echo $this->lang->line('lbl_password'); ?>" name="password" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_pwd_required'); ?>" data-parsley-maxlength="16" data-parsley-maxlength-message="<?php echo $this->lang->line('err_pwd_maxlength'); ?>" data-parsley-minlength="6" data-parsley-minlength-message="<?php echo $this->lang->line('err_pwd_minlength'); ?>">
                        </div>
                        <!--                        <div class="col-12">
                                                    <div class="radio-item">
                                                        <p>Join As:</p>
                                                    </div>
                                                    <div class="radio-item">
                                                        <input type="radio" id="ritema" name="ritem" value="ropt1">
                                                        <label for="ritema">A Fan</label>
                                                    </div>

                                                    <div class="radio-item">
                                                        <input type="radio" id="ritemb" name="ritem" value="ropt2">
                                                        <label for="ritemb">A Model</label>
                                                    </div>
                                                </div>-->
                        <div class="col-12 mt-4">
                            <button type="submit">Join</button>
                        </div>


                        <div class="col-12 user-log mt-4">
                            <p>Already a fan? <a href="<?php echo base_url('home/login') ?>"><span>Login</span></a></p>
                        </div>
                        <div class="col-12 user-log mb-4">
                            <p>Already you a model? <a href="<?php echo base_url('home/become_model') ?>"><span>Apply as Model</span></a></p>
                        </div>

                    </form>
                </div>
                <div class="col-md-6 sec-reg">
                    <h2>Your Ultimate Premium Social Experience</h2>

                    <ul class="mt-4">
                        <li>Unlock Premium Content from your favorite models Direct messagi</li>
                        <li>Direct messaging</li>
                        <li>Access Premium Vidoes</li>
                        <li>1 on 1 Video chat</li>
                        <li>Cancel your subscription at any time</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


</div>