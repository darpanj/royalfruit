<div class="site-login">
    <section class="site-blocks-cover overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 align-self-center">

                    <div class="row">

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="login-form d-flex">
        <div class="container">
            <div class="row">
                <div class="col-md-6 m-auto">
                    <form data-parsley-validate="" method="post" action="<?php echo base_url('home/login_check'); ?>">
                        <div class="row">
                            <div class="col-12">
                                <p>Login to your <span>Fan</span> or <span>Model</span> account below</p>
                            </div>
                            <div class="col-12">
                                <input type="text" class="form-control1" id="email" placeholder="Email" name="email" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_email_required'); ?>" data-parsley-type="email" data-parsley-type-message="<?php echo $this->lang->line('err_email_valid_type'); ?>">
                            </div>
                            <div class="col-12">
                                <input type="password" class="form-control1" placeholder="Password" name="password" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_pwd_required'); ?>" data-parsley-maxlength="16" data-parsley-maxlength-message="<?php echo $this->lang->line('err_pwd_maxlength'); ?>" data-parsley-minlength="6" data-parsley-minlength-message="<?php echo $this->lang->line('err_pwd_minlength'); ?>">
                            </div>
                            <div class="col-12 mt-4">
                                <button type="submit"><?php echo $this->lang->line('lbl_login'); ?></button>
                            </div>
                            <div class="col-12">
                                <a href="" class="pull-right" style="font-size: 13px;"><?php echo $this->lang->line('lbl_forgot_password'); ?>?</a>
                            </div>
                            <div class="col-12 user-log mt-4">
                                <p><?php echo $this->lang->line('lbl_new_user'); ?>? <a href="<?php echo base_url('home/register'); ?>"><span><?php echo $this->lang->line('lbl_join_now'); ?></span></a> or <a href="<?php echo base_url('home/become_model'); ?>"><span><?php echo $this->lang->line('lbl_title_become_model'); ?></span></a></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>log-img2.jpg" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>