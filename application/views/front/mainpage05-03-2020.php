<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_FONTS; ?>icomoon/style.css">

    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link href="<?php echo base_url() . FRONT_CSS; ?>jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>owl.theme.default.min.css">

    <link href="<?php echo base_url() . FRONT_CSS; ?>jquery.fancybox.min.css">

    <link href="<?php echo base_url() . FRONT_CSS; ?>bootstrap-datepicker.css">

    <link rel="stylesheet" href="<?php echo base_url() . FRONT_FONTS; ?>flaticon/font/flaticon.css">

    <link href="<?php echo base_url() . FRONT_CSS; ?>aos.css">

    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">


     <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/css/intlTelInput.css">
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>


    <!--<script src="https://code.jquery.com/jquery-latest.min.js"></script>-->


    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.min.css"> -->
</head>


<body class="site-feed site-home my-feed-p vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <?php
    if ($header_panel) {

        $this->load->view('front/header');

    }
    $this->load->view('front/' . $module);
    if ($footer_panel) {
        $this->load->view('front/footer');
    }
    ?>

</div>
<!--site js -->
<script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>popper.min.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
<!-- <script src="<?php echo base_url() . FRONT_JS; ?>typed.js"></script> -->
<script src="<?php echo base_url() . FRONT_JS; ?>main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/intlTelInput-jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>




<!-- <script>
    var typed = new Typed('.typed-words', {
        strings: [" Business Speakers", " Life Coach"],
        typeSpeed: 80,
        backSpeed: 80,
        backDelay: 4000,
        startDelay: 1000,
        loop: true,
        showCursor: true
    });
</script> -->

<script>

    // SIDEBAR
    $(document).ready(function () {
        $('.button-collapse').sideNav({
                menuWidth: 300, // Default is 300
                edge: 'right', // Choose the horizontal origin
                closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true // Choose whether you can drag to open on touch screens
            }
        );
        // START OPEN
        $('.button-collapse').sideNav('hide');
    });


</script>

<script>
    jQuery(document).ready(function () {
        UIToastr.init();
        $('form').parsley();
    });
</script>


<script>
    <?php if (!empty($this->session->flashdata('success'))) { ?>
    sType = getStatusText(200);
    sText = '<?php echo $this->session->flashdata('success'); ?>';
    Custom.myNotification(sType, sText);
    <?php } ?>
    <?php if (!empty($this->session->flashdata('error'))) { ?>
    sType = getStatusText(412);
    sText = '<?php echo $this->session->flashdata('error'); ?>';
    Custom.myNotification(sType, sText);
    <?php } ?>
</script>

</body>

</html>