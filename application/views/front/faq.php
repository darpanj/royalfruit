<div class="site-faq site-custom">


    <!-- <section class="site-blocks-cover overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 align-self-center">

                    <div class="row">

                    </div>
                </div>

            </div>
        </div>
    </section> -->


    <!-- <div class="cus-nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cus-feed2">
                        <select>
                            <option>Model FAQ</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- model -->
    <section class="faq">
        <div class="container">
            <div class="row">
                <h2 class="model-faq-nchchn">Model FAQ</h2>
                <div class="col mt-1 mb-3">
                    <h2>General</h2>
                    <div id="accordion" class="myaccordion">
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingOne">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  1. WHAT IS The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is the ultimate "one stop shop" social media platform that provides models with the ability to convert fans into paying followers and monetize your brand and content. The Royal Fruit followers pay a recurring monthly subscription fee to access exclusive model content with additional features like tipping, Premium and Private content sharing, as well as pay-per-minute Live Streaming, Premium Snapchat Management, and much more. Models set all of the price points themselves with total autonomy over their content and how they engage with their fans.</p>
                                    <p>The Royal Fruit, in collaboration with Inked Magazine, has an existing social media network of over 35 million fans, setting us apart from other platforms. Many other sites leave you alone on an island with "only" your existing followers while The Royal Fruit works just as hard as our models do to drive new fans to model profiles as the models do.</p>
                                    <p>At The Royal Fruit, we share our models on various social media platforms every day and do our best to help each of our models gain the maximum exposure and revenue possible. We have opportunities for Facebook Live features and Instagram posts to increase model reach, utilizing our huge network of fans eagerly seeking to follow the next great model.</p>
                                    <p>Welcome to The Royal Fruit!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingTwo">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  2. HOW DO I BECOME A MODEL ON The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>To sign up to become a model for The Royal Fruit go to to The Royal Fruit.com and click the gold 'Become a Model' button on the top right of our home page. Create a username, password and select 'Join As: A Model'. Follow the instructions and fill out the application completely.</p>
                                    <p>Be sure to upload identification and a selfie while holding your ID next to your face (ID Verification photo). The selfie ID process ensures identity verification; the person on the ID is the same person signing up for the account.&nbsp;</p>
                                    <p>Be sure to use the correct and public Instagram handle on the application. If you do not have an Instagram for some reason, please share with us your Twitter, Facebook, Snapchat, or whatever social media platform you will be using to promote your The Royal Fruit profile.</p>
                                    <p>Once completed with an application and it is approved, models will receive an email with clear instructions on how to get started uploading content and making money. Models will automatically receive an email newsletter sharing the best practices, weekly emails on site updates and The Secrets of Success.</p>
                                    <p>If any questions may arise during the application process, models have a convenient chat window on the bottom right of any The Royal Fruit profile/Dashboard page. We can also be reached via email at <a href="javascript:;">models@The Royal Fruit.com</a></p>
                                    <p><span style="color: #000000;"><span style="caret-color: #000000; font-size: 12px;">Alternatively,&nbsp;</span></span>models can go to the models portal <a href="javascript:;">The Royal Fruit.com/models</a>&nbsp;to gain more information. Models can also fill out their model application through the model portal.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                3. WHAT SETS The Royal Fruit APART FROM OTHER PLATFORMS?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <ol>
                                    <li>The Royal Fruit treats each and every one of our models like a business partner. The Royal Fruit prides ourselves in the ability to help models increase their following, expand their brand, increase personal profit and become their own bosses.</li>
                                    <li>The Royal Fruit is partnered with Inked Magazine giving our models access to a fan base of over 36 million social media followers. These are fans who seek a direct connection with their favorite model!! The Royal Fruit promotes our models on our social networks including Instagram, Facebook, Twitter, and in our newsletters to all of our fans</li>
                                    <li>The Royal Fruit.com has a homepage for fans to discover new models every day. Many other platforms do not offer this discovery feature, limiting the ability for models to market themselves.</li>
                                    <li>Unlike most other platforms, the The Royal Fruit team offers hands on technical support. We are here to answer any and all of your questions via email and live chat via Intercom in the bottom right corner of any Dashboard page.&nbsp;</li>
                                    <li>The Royal Fruit currently offers 10 different revenue streams, all through your single The Royal Fruit dashboard, making us a one-stop shop for models to monetize their brands online. The Royal Fruit is constantly adding new features to our platform.&nbsp;</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                4. HOW DO I GAIN FOLLOWERS ON The Royal Fruit?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Gaining followers is the primary way to make money on The Royal Fruit. Subscriptions are called "follows" on our site. Follows allows fans to access your photos, videos and direct messaging.</p>
                                    <p>Models choose a price for fans to follow that is charged per month, either $10 or $25. After your application is approved, you can select from other pricing options in your "Settings." This fee is recurring and is billed automatically each month.&nbsp;</p>
                                    <p>Followers can cancel at any time, so keep them engaged with exclusive content or messages so they continue to follow you.</p>
                                    <p>The Royal Fruit has many different revenue streams for you to make money from your followers, such as Premium videos, Live-Streaming, Tipping, and more. However, it is always important to continue to grow your follower base, because simply put, the more followers you have, the more money you make.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                5. CAN I USE OTHER APPS WHILE I AM UPLOADING VIDEOS OR PHOTOS?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is a web application that runs on your browser. Safari in iPhone, Chrome in Android, and most computer browsers as well. While the site works very smoothly, if you are in the process of uploading content to your model profile, the best way to ensure a successful upload is to leave the browser window open while uploading to not potentially interrupt the upload. Opening DMs or text messages during upload can potentially cause an interruption in your download depending on your device behavior and connection speed.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                6. HOW MUCH MONEY CAN I REALLY MAKE ON The Royal Fruit?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit provides a platform that is open-ended and unlimited in terms of how much money a model can earn. We are constantly adding new features and revenue streams to help models maximize their profits. The amount of money you earn is usually directly proportionate to how much your promote your The Royal Fruit link to your social media followers, how often you update your content, and the quality of your content. It is not uncommon for some models to make several thousand dollars per day all from your comfort zone.&nbsp;</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                7. WHO CAN SEE MY CONTENT?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                                <div class="card-body">
                                    <p>All of the public facing content on The Royal Fruit is 100% safe for work. Because we do so much marketing on social media, we do not permit any nudity in front of the pay wall. We keep our model community upscale and classy, and a safe place for models to post. Only paying followers can access your feed content. As a model, you can set up to 10 posts as teasers, and these are the public facing posts, which are more like social media rather than premium content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                8. CAN I GEO-TARGET OR BLOCK PEOPLE FROM SPECIFIC REGIONS TO PROTECT MY PRIVACY?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. The Royal Fruit enables geo-targeting of either countries, cities, or states per model's request. If you are an active model on The Royal Fruit and wish to geo-target and block a specific region, please email <a href="javascript:;">support@The Royal Fruit.com</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                9. WHAT ARE ALL OF THE WAYS I CAN MAKE MONEY ON The Royal Fruit?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit offers many different ways to make money. We refer to each of these as revenue streams. Here are the current revenue streams for models to make money on The Royal Fruit:</p>
                                    <ol>
                                    <li>Follows (new and recurring)</li>
                                    <li>Premium videos</li>
                                    <li>Private video messages</li>
                                    <li>Pay-per-minute livestreaming (group chat)</li>
                                    <li>1-1 FanCam</li>
                                    <li>Premium Snapchat</li>
                                    <li>Tipping</li>
                                    <li>Referrals</li>
                                    </ol>
                                    <p>For detailed explanations of these revenue streams and how you can use them to make money from your fans, read through our entire FAQ. We are constantly adding new revenue streams, so keep up to date with all of our e-mail blasts!&nbsp;</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="headingThree">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                10. I HAVE HEARD THAT THERE ARE MODELS THAT CONSISTENTLY MAKE $30K PER MONTH. HOW CAN I MAKE THE MOST MONEY POSSIBLE?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Many The Royal Fruit models that are top earners consistently take home over $30K per month. As an The Royal Fruit model, you are an entrepreneur and this is your business. As with most businesses, you get out of it what you put into it. Our models work very hard creating content, engaging and messaging with their followers on The Royal Fruit, managing their brand on social media, and of course sharing their The Royal Fruit link in their Instagram bio and posts, Facebook Bio and posts,Twitter bio and posts, and all of their relevant social media networks with "Call to Action" - example: Follow me at [model name].The Royal Fruit.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading12">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                11. HOW MUCH TIME IS REQUIRED TO MAKE DECENT MONEY?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The more time you invest promoting your The Royal Fruit account and uploading new content for your fans, the more money you will make. The beauty of our platform is that you are your own boss so you can create content on your own time. Treat this like your full time or part time job, and you will see money coming in.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading12">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                12. CAN I PROMOTE OR SHARE OTHER EXTERNAL LINKS FROM The Royal Fruit PROFILE?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                                <div class="card-body">
                                    <p>No. We do not allow promotional of any external links whatsoever on The Royal Fruit. We spend a lot of time and effort driving traffic to our models' profiles. We do not allow any traffic leaks.</p>
                                    <p>This includes your Instagram, your website and links to other fan platforms.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading13">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                13. DO I NEED TO BE EXCLUSIVE WITH The Royal Fruit OR CAN I ALSO BE ON OTHER PLATFORMS?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While we would love all of our models to be exclusive with The Royal Fruit and receive all of the benefits of being exclusive The Royal Fruit models, we welcome models that are visible on other platforms. Obviously, any model promoting multiple platforms will make less money from each of their platforms as usually fans are looking for the best place to engage with their favorite models. The models who exclusively promote their The Royal Fruit link receive extra promotions on our social media and also have the opportunity to become featured models on our website.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading14">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                14. CAN I MAKE MONEY WITHOUT DOING NUDE OR SEXUAL CONTENT?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. Your content is your own. Models can post anything they'd like from bikini, lingerie, implied nudity all the way too full nude or hardcore. We welcome all types of models and we make no judgment of your content. The Royal Fruit has many successful models that virtually do little or no nudity whatsoever. The culture of our community is more about interaction and being fun, cute, playful, and responsive than trying to show how hardcore you can be. No one knows your brand and your fans better than you. Fans will often ask for more, but we encourage models to stay in their comfort zone. Most fans on The Royal Fruit are very respectful of our models' boundaries. We don't want to see models doing something they are not comfortable with.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading15">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                15. WHAT KIND OF DEVICES SUPPORT The Royal Fruit ?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is an agnostic web app that works from the browser of most smartphones, computers, and tablets. When you are using a phone, make sure that you do not switch apps while your uploading content in The Royal Fruit. Do your best to leave your browser open and to not interrupt it by opening other apps.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading16">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                16. IS The Royal Fruit A WEBCAM SITE?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While The Royal Fruit offers live streaming that is both group chat and 1-1, we are not a traditional webcam site. Most webcam sites have tons of visitors that expect a lot of nudity for free. All premium content on The Royal Fruit is protected by our paywall. The Royal Fruit has an elevated premium culture for models to offer an exclusive live streaming experience for your paying fans. We do not believe in the philosophy that anyone should be nude for free ever or begging for tips. Additionally, The Royal Fruit pays out 70% for all revenue streams including live streaming while most webcam sites pay closer to 30%. One of our main goals in creating The Royal Fruit was to filter out the noise of fans expecting anything for free. You and your content are valuable and should be treated as such.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading17">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                17. DOES The Royal Fruit HAVE A DOWNLOADABLE APP?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Since many of our models share adult content, the app store does not allow an iOS app that is universal for fans and models. We are, however, launching an app very soon for our models to have an elevated experience posting on The Royal Fruit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading18">
                                <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link collapsed" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                18. WHAT IS MY UNIQUE URL AND HOW DO I SHARE IT?
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                                </button>
                                </h2>
                            </div>
                            <div id="collapse18" class="collapse" aria-labelledby="heading18" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Every The Royal Fruit model is given their own vanity URL (ex: modelname.The Royal Fruit.com). When you log in to your model dashboard, your URL is visible and has a copy-to-clipboard link available for easy sharing.&nbsp;</p>
                                    <p>The more you share your link, the more fans will pay to follow you. Some great places to share your link are:</p>
                                    <ol>
                                    <li>Your Instagram bio</li>
                                    <li>In your Instagram story</li>
                                    <li>In your Instagram feed</li>
                                    <li>In your tweets and Twitter bio</li>
                                    <li>In your Facebook posts and about section</li>
                                    <li>In your Snapchat story</li>
                                    </ol>
                            </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Applying</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse19" aria-expanded="true" aria-controls="collapse19">
                                  1. DOES INKEDGIRL WELCOME ALL TYPES OF MODELS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Anyone 18 years old or over can apply to be a model. Model applications must be complete and include copies of your ID, as well as other verification we ask for. We choose models for The Royal Fruit at our sole discretion, but welcome models of all types to apply.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading20">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse20" aria-expanded="true" aria-controls="collapse20">
                                  2. WHAT IS THE PROCESS TO SIGN UP AND BECOME A MODEL?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#accordion">
                                <div class="card-body">
                                    <p>To sign up, visit the The Royal Fruit.com/models, click "Become a Model" and follow the steps. Make sure you finish your entire application so we can review it and approve your account. Make sure there is a clear government issued photo of your ID showing your birthdate, a clear, unfiltered selfie of you holding the same ID next to your face, and a link to an active social media platform. We do not accept any incomplete applications.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading21">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                  3. HOW LONG DOES IT TAKE TO BE APPROVED?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We receive hundreds of applications per day, so please be patient. Our goal is to approve as many models as possible every day, but we always want to make sure all applications are thoroughly reviewed, so hang tight! If you would like to check on the status of your application, feel free to email us at asst@inkedgirl.com or contact us with the live chat feature on the bottom right of your dashboard.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading21">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse22" aria-expanded="true" aria-controls="collapse22">
                                  4. WHAT TYPE OF PHOTO ID DO I NEED TO PROVIDE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Government-issued photo IDs that are acceptable are: a non-expired driver's license, passport, military ID, or other government-issued photo IDs. We do not accept approve any applications without a verified government photo ID - no exceptions. Please make sure that your photo is clear and that we can easily read your birthdate and ID expiration date.&nbsp;</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading23">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse23" aria-expanded="true" aria-controls="collapse23">
                                  5. WHAT EXACTLY IS THE SELFIE ID AND WHY IS IT NECESSARY?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A selfie ID is a photo of yourself holding your ID next to your face so that we can clearly see BOTH your face and ID in the same photo. This verifies that you are the owner of the ID and that it is in your possession. Please make sure you <strong>do not</strong> use any filters as it distorts the ID.&nbsp;</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading23">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse24" aria-expanded="true" aria-controls="collapse23">
                                  6. WHAT IS THE LEGAL AGE REQUIREMENT TO BE A MODEL ON The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse24" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You must be 18 years or older in order to apply to become a model on our website. No exceptions.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading23">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse25" aria-expanded="true" aria-controls="collapse23">
                                  7. I CAN'T FIGURE OUT SOME OF THE SIGNUP PROCESS. HOW CAN I GET HELP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse25" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you ever need help while logged into the The Royal Fruit platform, click the help center on the bottom right of your screen. There is live response 24/7. If you do not get a response quickly, please e-mail support@The Royal Fruit.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading23">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse26" aria-expanded="true" aria-controls="collapse23">
                                  8. I'VE APPLIED AND AM NOT SURE WHAT TO DO NEXT. WHAT ARE THE NEXT STEPS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse26" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you have completed your application and you are sure that you uploaded your correct ID, selfie ID, and agreed to our terms. Then your application is now pending. Please be patient as we get hundreds of applications each day. Make sure you check your e-mail address you applied with because if something is missing, we will contact you that way. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading23">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse27" aria-expanded="true" aria-controls="collapse23">
                                  9. WHEN CAN I START POSTING CONTENT AND MAKING MONEY?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse27" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Once your application is approved, you will receive an automatic welcome e-mail that will clearly explain step-by-step the content requirements to have your profile launched onto our homepage so you can start sharing your link and making money. The sooner you upload your content and create your header and profile, the sooner you can get to work.</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Payment</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse28" aria-expanded="true" aria-controls="collapse19">
                                  1. CAN I SET MY OWN PRICES FOR FOLLOWS, VIDEO, LIVE ETC?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse28" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Each model gets to choose what their fans will pay to follow them. Located in Settings in your model dashboard, under Follow Price you can select from over 10 options in the dropdown menu. This is the price-per-month for a fan to follow you and will automatically rebill your followers each month until they cancel. Choose your price based on whether you want to attract more followers for a lower price or if you want to charge a higher price for your content.</p>
                                    <p>Models set their own prices for pay-per-minute live streaming. Also located in your model dashboard in Settings, under Livestream Price, you can select either $1, $3, or $5 per minute from the dropdown menu.</p>
                                    <p>Models also set the price for premium videos they post on their profile. Premium videos are extra for your fans and you can set the price per video when you upload it from $5 to $100.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse29" aria-expanded="true" aria-controls="collapse19">
                                  2. WHAT PERCENTAGE OF MY EARNINGS DO I KEEP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse29" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The revenue share on The Royal Fruit is 70%-30% on all follows, live streaming, premium videos, and tipping as well as any other revenue made on the site. You will get the 70% after 12.5% processing fees.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse30" aria-expanded="true" aria-controls="collapse19">
                                  3. HOW DO I GET PAID FROM The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse30" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Cre8 Media LLC, the company that owns The Royal Fruit, pays models twice each month, by the 2nd and the 17th, via your preferred payment method. Models can choose between ACH/Bank Transfer, check or local cash pickup (if available). We can also arrange bank wires for models making $1,000 a month or more.</p>
                                    <p>We require all of our models to earn a minimum of $50 per pay period in order to be paid out.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse31" aria-expanded="true" aria-controls="collapse19">
                                  4. HOW WILL I KNOW HOW MUCH MONEY I AM EARNING?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse31" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>To see your earnings, you can view real time metrics directly from your model dashboard.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse32" aria-expanded="true" aria-controls="collapse19">
                                  5. I'M NOT A U.S RESIDENT, WHAT TAX FORMS DO I NEED?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse32" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>All of our models are required to fill out a tax form. If you are an international model you have to fill a W-8EN. If you don't fill this form we cannot pay you.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse33" aria-expanded="true" aria-controls="collapse19">
                                  6. WHERE DO I PUT MY PAYMENT INFO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse33" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Once your model application is approved, you can find your payment settings in two different ways: On the left bottom side of your model dashboard, and on the the drop down menu at the top right of your model dashboard. Once you click on payment you will see a little gear icon where you can go and update your payment information and fill out your tax form. Make sure to input all of your information correctly.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse34" aria-expanded="true" aria-controls="collapse19">
                                  7. HOW LONG DOES IT TAKE TO GET APPROVED FOR ACH/DIRECT DEPOSIT PAYMENTS IN THE BANK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse34" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When you have your first pending payment, it takes approximately three to five business days for your bank to be approved. Many times it happens a lot faster than that as we always try to pay our models early.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse35" aria-expanded="true" aria-controls="collapse19">
                                  8. CAN I GET PAID OUTSIDE OF THE UNITED STATES?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse35" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. You may get paid outside the United States. We pay models from all over the world. Depending on which country you are in, please make sure that you provide us with the appropriate direct deposit information. This may be an IBAN number, SWIFT code etc. If you have any questions do not hesitate to contact us in the help center at the bottom right of your model dashboard. Some countries also allow for local cash pickup, so please ask if that is an option for you.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse36" aria-expanded="true" aria-controls="collapse19">
                                  9. DO YOU PROVIDE ALL THE TAX FORMS I NEED?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse36" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. It is located on your payment settings and you fill it out online without having to download it. If you are having any trouble signing the tax form, contact our team so we can assist you. All US models fill out a W-9 tax form and all international models fill out a W-8EN tax form. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse37" aria-expanded="true" aria-controls="collapse19">
                                  10. I DONT HAVE A BANK ACCOUNT. HOW DO I GET PAID?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse37" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you don't have a bank account there are several options for you. If you are located within the US, we are more than happy to mail you a check. Regardless of if you live within the US or outside the US, we can possibly send you money to pick at a local cash pickup station. Please reach out to our team to check what options are available for you.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse38" aria-expanded="true" aria-controls="collapse19">
                                  11. HOW OFTEN DO I GET PAID?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse38" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We pay all of our models twice a month, two weeks after every pay period. Our pay periods are from the 1-15th and the 16-31st of every month. For example: The pay period of October 1 - 15th pays November 1st or 2nd and the pay period of October 16 - 31 pays November 15th or 16th and so on.</p>
                                    <p>If it is your first payment, expect it to arrive a few days later since it takes 3-5 business days for the bank to approve your payment information. Our minimum payout is $50; when you hit the $50 payout we deposit the money in your account. It carries over to the next pay period when you don't hit the minimum $50.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse39" aria-expanded="true" aria-controls="collapse19">
                                  12. CAN I GET PAID VIA WESTERN UNION?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse39" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We do not use Western Union, however this might be an option in the future.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse40" aria-expanded="true" aria-controls="collapse19">
                                  13. CAN I GET PAID BY PAYPAL, CASH APP AND VENMO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse40" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We do not pay via paypal, cash app and venmo. However, this might be an option in the future.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse41" aria-expanded="true" aria-controls="collapse19">
                                  14. CAN YOU ADD MONEY TO A PREPAID DEBIT CARD?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse41" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We are currently in the process of being able to add money to a prepaid debit card.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse42" aria-expanded="true" aria-controls="collapse19">
                                  15. I DO NOT LIVE IN THE U.S AND I DON'T HAVE A ROUTING NUMBER, HOW DO I GET YOU MY CORRECT BANKING INFO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse42" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Under bank wire instructions, you can provide us with all your correct banking information, this includes; your bank name, bank address, whatever country code that is affiliated to your bank account.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse43" aria-expanded="true" aria-controls="collapse19">
                                  16. WHAT ARE THE PAY PERIODS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse43" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The pay periods are from the 1st to 15th and 15th to 30th of every month. We pay two weeks after every pay period.</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Followers</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse44" aria-expanded="true" aria-controls="collapse19">
                                  1. HOW WILL MY FANS KNOW ABOUT MY The Royal Fruit PAGE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse44" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You can share your unique The Royal Fruit link with your social media followers. Your URL is right on your profile page as a click and copy link. (Example: jordan.The Royal Fruit.com). There are multiple ways to let your existing fans know that they can join The Royal Fruit and directly connect with you. Add your The Royal Fruit URL in your bios on your social media accounts, especially places your fans go to see your content like Facebook, Instagram, or Twitter. You can share links to your photos and videos anytime you post and you can share content you have previously posted. Fans who follow you will be able to access your profile and those not following your The Royal Fruit account will be prompted to join and pay to unlock your content. The more you promote your posts on your public social media accounts, the more fans you can get. </p>
                                    <p>Of course, you will be gaining new fans from our powerful promotion machine to millions and millions of fans. We treat getting you new followers as a partnership.</p>
                                    <p>Additionally, fans will discover you on our homepage when they browse and search for new models.</p>
                                    <p>The more you promote, the more we promote you, the more money you make!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse45" aria-expanded="true" aria-controls="collapse19">
                                  2. HOW DO I GAIN NEW FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse45" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The best way to gain new followers is to share your personal The Royal Fruit link in all of your social media accounts. Places like Facebook, Instagram, Twitter, and Snapchat are where there are already people interested in you and your brand. Make sure to tag us in all of your posts because as long as you have your link in your bio, there is a higher chance of us reposting you to our social media.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse46" aria-expanded="true" aria-controls="collapse19">
                                  3. HOW WILL MY FANS KNOW THAT I AM LIVE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse46" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When you are going live on your The Royal Fruit model profile, create a "Call to Action" on all of your social media and tell your social media followers that you are live right now at YOURNAME.The Royal Fruit.com. Additionally, once you are live, your avatar appears at the top of the The Royal Fruit homepage. This gives you increased visibility and exposure to our fans as well. When posting on your social media, don't forget to tag us as it greatly increases your chances of us reposting you. Of course, all of your followers/subscribers will receive a notification and e-mail that you are live. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse47" aria-expanded="true" aria-controls="collapse19">
                                  4. DOES SOMEONE HAVE TO BE FOLLOWING ME TO DM ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse47" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. That is a privilege they pay for. Many of our models tell their social media followers that they only respond to DMs on their The Royal Fruit profile. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse48" aria-expanded="true" aria-controls="collapse19">
                                  5. SHOULD I RESPOND TO ALL MY MESSAGES?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse48" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, we highly recommend responding to as many messages as possible. Many fans send tips during messages, especially when you give them a lot of questions. Don't be hesitant to ask.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse49" aria-expanded="true" aria-controls="collapse19">
                                  6. CAN I DIRECT MESSAGE WITH MY FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse49" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes! In the drop-down menu on mobile or on the left side in your desktop, you will see an icon for "Messages." Simply click on it and manage your inbox! Use the "compose" feature to mass message all your paying followers and even mass message your cancelled followers to entice them to come back and follow you again!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse50" aria-expanded="true" aria-controls="collapse19">
                                  7. IS IT POSSIBLE TO HIDE MY PROFILE FROM CERTAIN COUNTRIES OR IP ADDRESSES?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse50" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. If you wish to geo-target a certain country, state,or region for whatever reason, contact support@The Royal Fruit.com and make a request. We do our best to accommodate all requests. We can also block followers that are already following you.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse51" aria-expanded="true" aria-controls="collapse19">
                                  8. WHAT DO I DO IF FOLLOWERS ARE NOT ABLE TO SUBSCRIBE, TIP OR BUY VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse51" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Please tell your fans and followers not to contact you directly about any issues making purchases. Direct your fans to email support@The Royal Fruit.com only. They should not be contacting you for any technical issues they are having with the site or purchases.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse52" aria-expanded="true" aria-controls="collapse19">
                                  9. WHAT PAYMENT TYPES CAN MY FANS USE TO SUBSCRIBE TO FOLLOW ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse52" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit accepts all major credit cards and prepaid cards as well. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse53" aria-expanded="true" aria-controls="collapse19">
                                  10. WHAT DO I DO IF A FOLLOWER IS HARASSING ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse53" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If a fan or follower is bothering or harassing you, please contact models@inkedgirl.com immediately or the live chat feature in the bottom right of your model dashboard. The Royal Fruit has a zero tolerance policy for any mistreatment or disrespect of our models. We strive for InkedGirl.com to be a safe place with a respectful culture so our models can enjoy interacting with their fans and make the most money possible.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse54" aria-expanded="true" aria-controls="collapse19">
                                  10. WHAT DO I DO IF A FOLLOWER IS HARASSING ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse54" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Many of your social media followers on Instagram, Facebook, Twitter, and Snapchat would love a greater level of access to you. It is super important to always let your followers know that you have exclusive premium content available as well as direct messaging, live streaming, and more at your The Royal Fruit profile. The key is to let your fans know on every social network exactly how they can gain access to your content and to interact with you. Create a "Call to Action" in your Instagram story and feed. Fans love a "Swipe Up!" Keep your vanity URL (modelname.inkedgirl.com) in your Instagram bio, Facebook "About Me," and Twitter bio. Tag The Royal Fruit in your posts whenever possible for a greater chance of us reposting you. Tell your Snapchat friends, your Twitter followers, and your Facebook friends exactly where they can connect with you (modelname.inkedgirl.com).</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse55" aria-expanded="true" aria-controls="collapse19">
                                  12. HOW DO I KEEP MY FOLLOWERS PAYING FOR A MONTHLY SUBSCRIPTION?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse55" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Getting your fans to subscribe to follow you is the first step. The key to real recurring revenue is to maintain a large number of recurring followers. How do you do that? The Royal Fruit models should always do their best to keep their fans happy by posting new content often on your The Royal Fruit content feed, responding to and sending direct messages, posting new premium videos, sending new private video messages, and in general being active on your The Royal Fruit profile to give your fans the maximum value and best experience. Let your fans get to know your authentic self. Many fans will unsubscribe soon after signing up if you don't give them a reason to want to continue their subscription. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse56" aria-expanded="true" aria-controls="collapse19">
                                  13. CAN I CHANGE THE COST TO FOLLOW ME AFTER I'VE GAINED PAID FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse56" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, you can change your follow price or video price anytime you like. Subscribers will be rebilled at the rate which they subscribed at. </p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Site Features</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse57" aria-expanded="true" aria-controls="collapse19">
                                  1. CAN I MASS MESSAGE ALL MY FOLLOWERS AT ONCE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse57" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. You can mass message all of your followers at once. When you send a private video message, you have the option in the drop-down menu to select "all followers." Each of your followers will receive the message separately.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse58" aria-expanded="true" aria-controls="collapse19">
                                  2. DOES The Royal Fruit HAVE AN APP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse58" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is a web app due to app store restrictions. We have built our web app to work for you on your phone, computer, or tablet for ease of use.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse59" aria-expanded="true" aria-controls="collapse19">
                                  3. WHAT DEVICES CAN I USE TO ACCESS The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse59" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is fully functional from your smartphone, computer, or tablet. If you are unsure whether or not your device supports The Royal Fruit or if you are receiving an error message, please message us at the help center in the bottom right of your dashboard. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse60" aria-expanded="true" aria-controls="collapse19">
                                  4. CAN I UPLOAD PHOTOS ONE AT A TIME OR MULTIPLE AT ONCE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse60" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Both! You can upload photos one at a time or entire galleries at a time.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse61" aria-expanded="true" aria-controls="collapse19">
                                  5. CAN I UPLOAD VIDEOS ONE AT A TIME OR MULTIPLE AT ONCE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse61" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Both! You can upload videos one at time or several at a time.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse62" aria-expanded="true" aria-controls="collapse19">
                                  6. SHOULD I WRITE A CAPTION FOR EVERY PHOTO OR VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse62" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You don’t have to, but we recommend giving every photo or video a quick caption so your fans can learn more about your personality or refer to it if need be! This is a great way for your fans to get to know the real you! They are here to engage and interact with you! The better the caption, the more your fans will know what they are purchasing, especially if it is premium or private content!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse63" aria-expanded="true" aria-controls="collapse19">
                                  7. DO I GET MY OWN The Royal Fruit URL?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse63" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, having an The Royal Fruit account is like having your own private pay site. If your name was Jordan, your personal URL would be Jordan.The Royal Fruit.com so it is easy for your fans to remember. You can always change your model name or personal URL by messaging us in your help center in the bottom right of your dashboard. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse64" aria-expanded="true" aria-controls="collapse19">
                                  8. WHAT IS MY PROFILE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse64" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Your profile is a feed of the content you post that your fans see. People who are not following you can only see your teaser content, which give them an idea of who you are and the kind of content that you post.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse65" aria-expanded="true" aria-controls="collapse19">
                                  9. WHAT IS MY MODEL DASHBOARD?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse65" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Your model dashboard is where you can access all of the features of The Royal Fruit as a model. You can see: - Your earnings and statistics; like how much money you have been paid so far, the amount of your next payment, or how many people are following you. - You can manage your content; like posting photos or videos, add captions to posts, or reply to comments on posts. - You can interact with your fans; like sending messages or checking new messages. - You can access your live stream to go live. - You can change your settings, update your documents, or change payment information.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse65" aria-expanded="true" aria-controls="collapse19">
                                  10. HOW DOES TIPPING WORK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse65" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>On every photo, video, conversation, etc. that you have, your fans will see a huge "TIP" button. Ask your fans to tip you! There is no maximum to how much they can tip you. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse66" aria-expanded="true" aria-controls="collapse19">
                                  11. HOW DOES LIVE STREAMING WORK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse66" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Live Streaming is now available for you to double your income and more on The Royal Fruit!!!</p>
                                    <p>Our Feature is called 'Go Live' and it is available in your model dashboard simply by clicking the red 'Go Live' button.&nbsp;</p>
                                    <p>'Go Live' works seamlessly from your mobile phone or your computer.<br>​<br>The Live Streaming feature is very easy to use and it’s poised to double your earnings!<br>You will charge a price per minute using tokens. One token is equal to one dollar. You can set your price up to 10 tokens per minute… &nbsp;</p>
                                    <p>Models keep 70% of all revenue earned on the site and live streaming is the same %… &nbsp;</p>
                                    <p>You can do 3 different modes of Live Streaming:</p>
                                    <p>1. &nbsp;“Free to Followers” - This will encourage your followers to keep following you and paying you every month. It’s like a gift to them for their monthly memberships and supporting you in your life and career.&nbsp;</p>
                                    <p>2. “Paid Chat" - This is a group chat like Instagram Live or webcam where everyone who joins must pay per minute. Imagine if you have 10 guys in this chat you will be making 10 times whatever you are charging per minute!! &nbsp;<br>​<br>3. “1-on-1” - This is an exclusive private video chat between you and one fan only. This mode is started by the fans, you can not start this.&nbsp;</p>
                                    <p>4. “FanCam" - This is similar to Facetime where you and the fan can see each other. You can charge more for this type of live experience. If you do not want to see or hear your fan but still want to do exclusive 1-on-1 chat, you also can go in your settings and “opt out” of FanCam and it will still be 1-on-1. At that point the fan can only type in the chat, but he can hear and see you!</p>
                                    <p>Make sure you tell your fans to tip you in the chat, work it girl!</p>
                                    <p>Also make sure to let your fans in social media know that you are going live. This is the best way to make money! Direct your social media followers that they can 1-on-1 Live Stream with you! We payout 70% where most webcam sites pay 30-40%.</p>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse67" aria-expanded="true" aria-controls="collapse19">
                                  12. HOW DO DIRECT MESSAGES WORK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse67" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Direct messages are similar to most messaging systems you see on other social media platforms. You will see messages in your model dashboard.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse68" aria-expanded="true" aria-controls="collapse19">
                                  13. WHAT IS THE The Royal Fruit MODEL SHOP AND HOW DO I START ONE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse68" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The The Royal Fruit model shop is a place where models can sell items such as sweatshirts, t-shirts, pillows and etc. Once you have at least 50 or more followers on The Royal Fruit you reach out to our team so we can set up a shop for you. This is a great way to earn extra revenue. You can also contact crew@inkedgirl.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse69" aria-expanded="true" aria-controls="collapse19">
                                  14. WHAT CAN I SELL ON MY OWN SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse69" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You can sell many things on your model shop, they are not limited to anything and it includes sweat, socks, mugs and many more.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse70" aria-expanded="true" aria-controls="collapse19">
                                  15. WHO IS RESPONSIBLE FOR SHIPPING ITEMS PURCHASED FROM MY SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse70" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>...</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse71" aria-expanded="true" aria-controls="collapse19">
                                  16. CAN I MESSAGE OTHER MODELS ON THE SITE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse71" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Not at the moment. This is a feature we will be releasing on the platform soon. By popular demand we are going to allow models to "friend" each other so they can grow their model squad sisterhood.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse72" aria-expanded="true" aria-controls="collapse19">
                                  17. CAN MY FOLLOWERS TIP ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse72" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. On your followers dashboard when they see your content they have a big tip gold button on every photo, video and message of you. There's no limit for the value they can tip you. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse73" aria-expanded="true" aria-controls="collapse19">
                                  18. WHAT IS A PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse73" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>On the The Royal Fruit platform, a premium video is a video which you charge your fans extra money for beyond the price of their subscription follow. Most The Royal Fruit models save their best and sexiest videos as premium videos. Your premium videos will appear locked in your content feed and will not be visible to anyone unless they specifically purchase that video. Make sure that the description of your video is both juicy and accurate to keep your fans happy. </p>
                                    <p>As an added bonus to help our models make the most sales possible, we share all our models' premium videos into our premium video store, IsMyVids.com ! Premium videos are available to non-followers at a $5 extra surcharge to help you maximize your earnings! If you want to opt out of this option, simply go to your "setting" and un-check the box. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse74" aria-expanded="true" aria-controls="collapse19">
                                  19. WHAT IS A PRIVATE VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse74" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Private video messages are locked videos which can only be sent in a direct message. Many The Royal Fruit models make and sell videos that are only intended for specific fans and not made available to all by making it a premium video. Instead, they opt to possible charge more and send this discreet video as a private video message, rather than a premium video. When you set your price for your private video messages, you can choose exactly which of your fans you would like to send this private video to. We even have a feature for you to send a video to all of your followers at once. Always make your message sound personal so your fans feel like you made these videos just for them. A personal touch goes a long way!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse75" aria-expanded="true" aria-controls="collapse19">
                                  20. WHAT IS A RECURRING SUBSCRIPTION?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse75" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A recurring subscription is when a follower follows you for another month. Make sure to keep uploading content frequently so your fans are more likely to keep subscribing. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse76" aria-expanded="true" aria-controls="collapse19">
                                  21. WHAT INFORMATION IS IN THE NOTIFICATIONS ICON?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse76" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Your notifications will include all the activity that occurs on your model dashboard. This includes one on one requests, when you receive a new follower, when someone sends you a message, when someone tips you, when you earn a reward on our platform etc. We will always keep you updated on any activity on your profile, you will also receive email notifications so you don't miss out on anything.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse77" aria-expanded="true" aria-controls="collapse19">
                                  22. CAN I OPT OUT OF EMAIL NOTIFICATIONS FOR NEW SALES?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse77" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you wish to opt out of email notifications please contact the help center on the bottom right of your model dashboard so we can assist you with it. </p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Promotion</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse78" aria-expanded="true" aria-controls="collapse19">
                                  1. WHAT ARE SOME TIPS TO BE SUCCESSFUL AS A MODEL ON The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse78" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Here are a few tips to become&nbsp;<span style="caret-color: #151010; color: #151010; font-family: Arial; font-size: 12px; white-space: pre-wrap;">successful</span>&nbsp;as a model on The Royal Fruit:</p>
                                    <ol>
                                    <li>Post great content often, whether it be personal selfies or professional photos or videos you have. Selfie content is great; it makes your fans feel a personal connection to you, even more than professional content in some instances.</li>
                                    <li>Let your fans know that you have a profile on The Royal Fruit. A lot of models add their The Royal Fruit link to their bios on Twitter, Instagram, Facebook and more. The more people that you share your profile with, the more likely you are to get followers and make money.</li>
                                    <li>Make premium and private videos that your fans can pay extra for. This is an easy way to make extra money on top of what your fans already pay to follow you. Message your fans and engage with them. Part of the charm of InkedGirl.com is the ability for your fans to have a direct connection with you. Give them a great experience!</li>
                                    <li>Please remember that all it takes to succeed here is to keep uploading a pic/vid or two a day and respond to them. This is the ultimate social experience for you to engage and connect with your fans.&nbsp;</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse79" aria-expanded="true" aria-controls="collapse19">
                                  2. HOW DO I PROMOTE MYSELF TO BRING MORE FANS TO MY The Royal Fruit PAGE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse79" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Putting your The Royal Fruit URL as your live link on your IG and other social media is a great way to convert your fans to paying customers. Mentioning your The Royal Fruit account with a great photo or video in your facebook, Instagram posts, Instagram story, Facebook messenger, Snapchat, Twitter , or anyplace you engage with people is one of the main keys to success. Let your fans know where they can access you!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse80" aria-expanded="true" aria-controls="collapse19">
                                  3. WHY DOES MY INSTAGRAM ACCOUNT HAVE TO BE PUBLIC?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse80" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You do not have to have a public IG account, but we do ask for it to be public for two different reasons.</p>
                                    <ol>
                                    <li>Verification - We check your information against your public IG account to verify that it is actually you.</li>
                                    <li>Promotion - We LOVE to promote our models. By keeping your IG public and by sharing your link, you are giving more users access to know where to find you! We notice that there is a huge difference in earnings from models with public IG accounts vs. models who do not.</li>
                                    </ol>
                                    <p>We fully respect your decision no matter what you decide to choose!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse81" aria-expanded="true" aria-controls="collapse19">
                                  4. HOW DO I GET POSTED ON THE The Royal Fruit INSTAGRAM OR FACEBOOK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse81" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>To be considered for a feature on our The Royal Fruit IG, please message our team with your best, high-quality photos. We also do shout-outs on our story too!. Make sure you send safe-for-work content. We love sharing you all! Make sure that you are including your The Royal Fruit link in your bio :)</p>
                                    <p>If you would like to do a Facebook live, please message our team with your request. This is a great way to get to know our fans and entice them to follow you :) </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse82" aria-expanded="true" aria-controls="collapse19">
                                  5. HOW DOES The Royal Fruit PROMOTE MY PAGE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse82" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We at The Royal Fruit promote our models to over 35 million social media followers. We feature models on our Instagram feed and stories, Facebook, Twitter account as well and sending weekly or daily e-mail blasts. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse83" aria-expanded="true" aria-controls="collapse19">
                                  6. WHY IS PROMOTING MY PROFILE ON MY SOCIAL MEDIA SO IMPORTANT TO BEING SUCCESSFUL ON The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse83" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Promoting your profile on social media is very important to being successful on The Royal Fruit. By sharing your link in your IG, Twitter, and FB bio, by promoting your page with a "swipe up" and by tagging us in all your posts, you are showing your fans where they can access you! You need to tell them where they can find you to interact and engage with you. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse84" aria-expanded="true" aria-controls="collapse19">
                                  7. WHAT ARE TEASERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse84" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Teasers are safe for work photos on your profile that anyone can see, make sure to upload your best photos so your fans will know what they are subscribing to. We recommend uploading at least 3 teasers. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse85" aria-expanded="true" aria-controls="collapse19">
                                  8. HOW DO I BECOME A FEATURED MODEL ON The Royal Fruit ?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse85" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We rotate though out features models all the time. Make sure you are sharing your The Royal Fruit link on your Instagram bio and you may submit a request to be a featured model. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse86" aria-expanded="true" aria-controls="collapse19">
                                  10. HOW DO I GET FEATURED IN THE The Royal Fruit NEWSLETTER?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse86" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We have several different types of newsletter, such as: model of the day, fan favorites, too hot for instagram, etc. If you want to be featured on the The Royal Fruit newsletter please email <a href="javascript:;">posse@inkedgirl.com </a></p>
                                    <p>We receive many requests everyday, we will get to your as soon as we can.</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Content Questions</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse87" aria-expanded="true" aria-controls="collapse19">
                                  1. HOW OFTEN SHOULD I POST?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse87" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When you join it's a good idea to get a lot of content up right away so that fans of the site see your page as a solid purchase. This can even be older content that you feel represents your brand well. Once you are live, we have seen that the best formula is at least one or two images or vids daily vs uploading lots of content and then being absent for long periods of time. Think of your fans as addicted to you daily. With 15 min a day you can post up content saved in your phone or computer and check your messages. This is how you keep them paying and coming back for more! Most of us post on our social media daily. The culture here should be the same, especially since The Royal Fruit is the social network that pays you!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse88" aria-expanded="true" aria-controls="collapse19">
                                  2. WHAT IS THE MINIMUM LENGTH A VIDEO SHOULD BE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse88" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Teaser videos and videos in your subscriber feed can be as long or as short as you like. You want to grab your fans attention so experiment and have fun. Dont feel like they need to be long, just enticing. Consistency is the most important thing. A video in your subscriber feed can be sexy in 5 or 10 seconds, if you do it right. We recommend limiting your videos on your profile to one or two minutes maximum. Premium and Private videos are different and must be at least 30 seconds long, since your fans are paying extra for them. We recommend making your Premium and Private videos a bit longer and more special. Videos you are selling can be priced according to length and content. For premium and private vids, these should be long enough so that the fan feels the value in the purchase. We have seen 1 min to 10 min. That's totally up to you and the prices you set for each. Remember that the premium and private should be different and or more than they get to see on the subscription</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse89" aria-expanded="true" aria-controls="collapse19">
                                  3. WHAT TYPE OF CONTENT SHOULD I POST ON The Royal Fruit ?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse89" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The type of content you post is totally up to you. Your 3-10 teasers, profile picture/avatar, and header/banner need to be safe for work, but everything behind the pay wall is your choice. The Royal Fruit gives you the freedom to express yourself without judgement and engage with your fans as playfully or sexy as you choose.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse90" aria-expanded="true" aria-controls="collapse19">
                                  4. DO I HAVE TO DO NUDE OR SEXUAL CONTENT TO MAKE MONEY?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse90" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is a special platform because we allow all of our models to be their own boss and control the content that they post. We have models who rage from one spectrum to the other, with models being successful on both sides. If you want to incluse nude or sexual content you may, we are a judgment free platform. As long as you post content that you are comfortable with you are good to go. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse91" aria-expanded="true" aria-controls="collapse19">
                                  5. WHAT KIND OF PHOTOS CAN I ADD FOR TEASERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse91" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You can add up to 10 safe for work photos and/or videos as teasers. Please make sure that they are safe for work. If they include content that would be taken down on social medias such as Instagram and Facebook then they cannot be used as teasers. Use your best judgment. These are the photos/videos that your fans will see before subscribing to your page. Keep them wanting more. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse92" aria-expanded="true" aria-controls="collapse19">
                                  6. WHAT IS THE DIFFERENCE BETWEEN VIDEOS IN MY FEED, PREMIUM VIDEOS AND PRIVATE VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse92" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>Videos in your feed are videos that your subscribers see when they first follow you page. Premium and private videos that you followers have to be an extra cost to see.&nbsp;</p>
                                    <p>Here's a little explanation:&nbsp;</p>
                                    <p>1. Private Videos - This is an album where you load private videos and pics that you can sell to your paid followers in your messenger. You set the price for each video and you control who gets them. You can send them to your entire fan base or pick and chose who they go out to. The other way to sell them is to go into a message with a fan and talk to them about a custom video and work your magic! Once you set your price for the video you upload it into the message and then they can pay to unlock it and view.</p>
                                    <p>2. Premium Videos - These are videos you upload into your regular feed for the price you want! After you upload your Premium Video select a price and add a description. Your fans will see it in your feed with a lock over it and then they can click to buy and view! Your Premium Videos are always available on your feed for your subscribed followers to purchase. You are able to change prices of your Premium Videos as well.</p>
                                    <p>A good way to select premium content is to make sure that it's more exciting than what the fans can see on your regular free to see social media accounts, like Instagram or Twitter.&nbsp;</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse93" aria-expanded="true" aria-controls="collapse19">
                                  7. HOW DO YOU SET SOMETHING AS A TEASER?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse93" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>...</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse94" aria-expanded="true" aria-controls="collapse19">
                                  8. WHAT IS THE REQUIRED AMOUNT OF PHOTOS AND VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse94" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>There is no required amount of photos and videos to have but we do recommend uploading at least 20 - 25 photos and at least 5 videos to your content feed so we can make your profile visible on our homepage. Of course the more content you upload is better so don't forget to post often. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse95" aria-expanded="true" aria-controls="collapse19">
                                  9. WHY DO I NEED THE SPECIFIED MINIMUM AMOUNT OF CONTENT?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse95" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">Below is the recommended minimum amount of content we require all our models to have before making their profile visible on our homepage. &nbsp;We recommend this amount because when a follower subscribes to see your page, we want to make sure that there is enough content to see! This helps ensure that you minimize chargebacks and refunds! </span></p>
                                    <p><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">1- Upload a header photo - make sure it is horizontal and is a safe for work picture of you. We also do not allow any text.</span><br style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;"><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">2- Upload an avatar of you that is safe for work</span><br style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;"><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">3- Upload at least 25 photos to your content feed (of course more is better)</span><br style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;"><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">4- Set 3 to 10 safe for work photos as teasers</span><br style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;"><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">5- Upload at least 5 videos on your content feed (they can be short)</span><br style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;"><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">6- Share your The Royal Fruit link in your Instagram bio</span><br style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;"><span style="caret-color: #222222; color: #222222; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; background-color: #ffffff;">7- Upload at least one 'Premium Video'</span></p>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse96" aria-expanded="true" aria-controls="collapse19">
                                  10. CAN I HAVE LOGOS IN MY PHOTOS AND VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse96" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>We do not allow any photos or videos that have logos of other platforms or links to other platforms on our website. We reserve the right to remove anything that violates this.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse97" aria-expanded="true" aria-controls="collapse19">
                                  11. ARE THERE RESTRICTIONS ON THE CONTENT I CAN POST?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse97" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>As the boss of your own content creation, you can post anything that you would like as long as you comply with any legal laws of the country that you live in. Make sure you do not have anyone under 18 on your photos and videos and make sure you are not doing anything illegal.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse98" aria-expanded="true" aria-controls="collapse19">
                                  12. CAN I DELETE A POST?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse98" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>You can always delete or edit a post. Once you enlarge a photo or video on your profile, there is a trashcan icon at the bottom right where you can delete your content. You may also update and edit your captions on all of your photos and videos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse99" aria-expanded="true" aria-controls="collapse19">
                                  13. I DON'T HAVE PROFESSIONAL MODELING PHOTOS. CAN I STILL USE The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse99" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>Yes! You do not need to have professional modeling photos to be a successful model on The Royal Fruit. Taking even selfie photos and videos make a huge difference. Your fans will get to know you a little better on a personal level. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse100" aria-expanded="true" aria-controls="collapse19">
                                  14. WHAT IS THE BEST KIND OF CONTENT TO UPLOAD?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse100" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>There is no right or wrong type of content to post. You know your brand and audience better than we do. Upload content that you are comfortable with :) The point of your profile is to get your audience to engage with you and get to know you!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse101" aria-expanded="true" aria-controls="collapse19">
                                  15. CAN I UPLOAD CONTENT THAT HAS WATERMARKS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse101" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>We do not allow any other watermarks on our platform. Every photos and videos you upload to our platform will automatically have an The Royal Fruit logo attached to it.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse102" aria-expanded="true" aria-controls="collapse19">
                                  16. WHAT CAN I UPLOAD AS MY HEADER?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse102" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>Your header photo must be a horizontal photo of you. This is a very important photo to upload because is the first thing that you subscribers will see when they go to your profile. The better the header the more enticing your profile is. Please make sure to not include any text or any links to other platforms or your social media profiles. If you need any idea of a good header, go to our The Royal Fruit homepage and see examples of how some of our top models do it.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse103" aria-expanded="true" aria-controls="collapse19">
                                  17. WHAT IS A STREAM POSTER?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse103" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>A stream poster is your profile picture when you go live, your stream poster will be shown at the top of the homepage to our users to see. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse104" aria-expanded="true" aria-controls="collapse19">
                                  18. SHOULD I ADD A TITLE TO EACH OF MY VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse104" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="crad-body">
                                    <p>You do not have to, but we highly recommend adding a title to each of your videos so your followers know what they are going to watch.</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Technical Issues</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse105" aria-expanded="true" aria-controls="collapse19">
                                  1. I'M RECEIVING TOO MANY NOTIFICATIONS. HOW DO I STOP THEM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse105" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit sends both models and fans notifications of many different things, like you got a new follower, you sold a new premium video, you got a new direct message, someone is making a 1 on 1 request, etc. You are in complete control of the notifications in your model dashboard under "Settings" When you scroll down in your settings you can "opt in or opt out" to many different notifications. Choose the ones that are important to you and ignore the ones that are not! The Royal Fruit does our best to make as much data available to our models and let our models choose exactly what they would like to be notified about.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse106" aria-expanded="true" aria-controls="collapse19">
                                  2. I CAN'T LOGIN WITH MY USER NAME OR PASSWORD. WHAT DO I DO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse106" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Make sure you are using the correct email address for your login. One common mistake people make is using the wrong email address. Passwords are case sensitive, so make sure your phone is not auto capitalizing something that is not supposed to be. Otherwise, click on "forgot password" and you will receive an email to the account that is registered with The Royal Fruit to to "reset" your password. Alternatively, you can always reach out to the help center in the bottom right of your model dashboard so we can reset your password for you.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse107" aria-expanded="true" aria-controls="collapse19">
                                  3. WHAT ARE THE DIFFERENT WAYS THE The Royal Fruit TEAM WILL CONTACT ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse107" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When the The Royal Fruit team needs to reach out and contact our models, we use different methods of contacting you. Email is very important to us as we share A LOT of information with our models to help keep our community apprised of the latest updates, new features, promotional tools, payment information, social posting and much more, so models should check their email accounts often. If we can not reach you by email, we also text message, DM on Instagram, or even call on the phone or Whatsapp for something urgent. Make sure that you always keep your contact information correct and that your email address, Instagram handle, and phone number are always updated and correct, so we can most efficiently reach out to you and get you the information or help you need to make the most money possible!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse108" aria-expanded="true" aria-controls="collapse19">
                                  4. HOW CAN I CAN CHANGE MY MODEL NAME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse108" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you want to change your model name you can either reach out in the help center chat box in the bottom right corner of your model dashboard, or simply email <a href="javascript:;"> squad@inkedgirl.com.</a> We can change your model name at any time, for any reason at all. Model names must be unique and can't be a name that someone else has registered with The Royal Fruit. If you are unsure about your model name, many of our models use the same name as their instagram handles to keep themselves branded. It makes it easier for people to recognize you and it is definitely less confusing to the fans.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse109" aria-expanded="true" aria-controls="collapse19">
                                  5. HOW DO I GET MY ACCOUNT AND EMAIL DELETED FROM THE WEBSITE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse109" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While we hate to see any of our amazing models leave the site, we understand and respect when someone has a personal issue or a life change and no longer wants to be on our site. If you want to delete your account for any reason, you can either contact support in the help center in the lower right corner of your model dashboard, or you can simply email squad@The Royal Fruit.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse110" aria-expanded="true" aria-controls="collapse19">
                                  6. HOW CAN I CHANGE MY EMAIL ADDRESS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse110" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you want to change the email address associated with your The Royal Fruit account, you can simply email squad@The Royal Fruit.com or use the live chat support in the bottom right corner of your account. Please be patient as we respond as quickly as possible but often have a large amount of requests, and we handle these in the order in which they are received.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse111" aria-expanded="true" aria-controls="collapse19">
                                  7. WHAT ARE THE BEST WAYS TO GET TECHNICAL SUPPORT?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse111" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The best way to get technical support to is to message us in the live chat feature at the bottom right of your model dashboard so we can direct you to a team member who is best suited to handle your request.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse112" aria-expanded="true" aria-controls="collapse19">
                                  8. WHAT IS THE CHAT BOX ICON AT THE BOTTOM OF MY DASHBOARD?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse112" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The chat box at the bottom right of your model dashboard is our help center/live chat feature for all of our models to access our team if you ever have any questions, questions, suggestions, etc. We do our best to answer every one of your questions to the best of our ability and will direct you to the appropriate team to handle your request. We pride ourselves in supporting our models 24/7</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Referrals</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse113" aria-expanded="true" aria-controls="collapse19">
                                  1. WHERE DO I SEE MY REFERRAL EARNINGS ?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse113" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>In your model dashboard, in the drop-down menu on mobile or in the left sidebar on desktop, click on referrals. There you will find your model referral earnings organized by pay period and your 3 money-making referral links for The Royal Fruit, InkedGirl and The Royal Fruit. Don't forget the 5% lifetime commission you receive from models you refer is paid from The Royal Fruit and does not come out of the model's earnings :)</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse114" aria-expanded="true" aria-controls="collapse19">
                                  2. CAN I MAKE MONEY BY REFERRING OTHER MODELS & HOW DOES IT WORK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse114" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes!  Models earn lifetime commissions equal to 5% of whatever the referred model earns.  Don't worry though, the commission is paid from The Royal Fruit, and doesn't come from the model, so invite all your friends and reach out to models you think would be successful in our community and create some passive income! </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse115" aria-expanded="true" aria-controls="collapse19">
                                  3. WHERE CAN I FIND MY REFERRAL LINK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse115" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Your referral link can be found in the drop-down menu on mobile, or on the left sidebar in your desktop. You will find the tab for referrals. Click on it, and you will find your 3 referral links at the top of the page!</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Tipping</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse116" aria-expanded="true" aria-controls="collapse19">
                                  1. CAN MY FOLLOWERS TIP ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse116" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes! Your followers can tip you as much and as often as they'd like. Models do not see the fan's dashboard, but when your fans are viewing your content, there is a button for them to tip you on every photo, video, and message. Be interactive and let them know they can tip you and how much you'll appreciate it. We've seen models receive tips in the multiple thousands. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse117" aria-expanded="true" aria-controls="collapse19">
                                  2. IS THERE A LIMIT TO HOW MUCH THEY CAN TIP ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse117" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit places no limit to how much a fan can tip a model. Any limitations would be at the level of the bank and credit card processor.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse118" aria-expanded="true" aria-controls="collapse19">
                                  3. HOW DO I ENCOURAGE MY FANS TO TIP ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse118" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The more interactive you are with your fans, the more likely they are to tip you. Don't be shy! Let your fans know how much you appreciate generosity!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse119" aria-expanded="true" aria-controls="collapse19">
                                  4. WHERE ON THE SITE CAN MY FANS TIP ME?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse119" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When your followers are logged into their user dashboard, everytime they view a photo, video, or message from you, there is a gold "Tip" button inviting them to tip you. Make sure to let your fans know you appreciate tips.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse120" aria-expanded="true" aria-controls="collapse19">
                                  5. CAN I GET A TIP DURING A LIVE STREAM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse120" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes! Fans must purchase tokens to live stream with you. One token = $1. During your live stream, whether in group paid chat, free-to-followers mode, or 1-1 FanCam, your fans can tip you with tokens very easily. Entice your fans into tipping you. The Royal Fruit is a premium platform and we encourage generosity by our fans to our models.</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Premium Videos</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse121" aria-expanded="true" aria-controls="collapse19">
                                  1. WHAT IS A PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse121" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>On the The Royal Fruit platform, a premium video is a video which you charge your fans extra money for beyond the price of their subscription follow. Most The Royal Fruit models save their best and sexiest videos as premium videos. Your premium videos will appear locked in your content feed and will not be visible to anyone unless they specifically purchase that video. Make sure that the description of your video is both juicy and accurate to keep your fans happy. </p>
                                    <p>As an added bonus to help our models make the most sales possible, we share all our models' premium videos into our premium video store, IsMyVids.com ! Premium videos are availabe to non-followers at a $5 extra surcharge to help you maximize your earnings! If you want to opt out of this option, simply go to your "setting" and un-check the box. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse122" aria-expanded="true" aria-controls="collapse19">
                                  2. WHAT IS THE DIFFERENCE BETWEEN A PREMIUM VIDEO AND A PRIVATE VIDEO MESSAGE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse122" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While both premium and private videos are items which fans must pay extra for and that you set your own price for, the way which the fans access these videos are very different. Premium videos live in your content feed completely locked until your fans pay for them. Premium videos are also available in our premium video store at IsMyVids.com.</p>
                                    <p>Private video messages are locked videos which can only be sent in a direct message. Many The Royal Fruit models make and sell videos that are only intended for specific fans and not made available to all by making it a premium video. Instead, they opt to possible charge more and send this discreet video as a private video message, rather than a premium video. When you set your price for your private video messages, you can choose exactly which of your fans you would like to send this private video to. We even have a feature for you to send a video to all of your followers at once. Always make your message sound personal so your fans feel like you made these videos just for them. A personal touch goes a long way! </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse123" aria-expanded="true" aria-controls="collapse19">
                                  3. WHAT TYPE OF CONTENT WILL MY FOLLOWERS PAY FOR AS A PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse123" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Premium videos should be a little more sexy or "extra" than the videos that you put in your content feed. Your fans expect short videos in your content feed as part of their monthly subscription. So make sure any video that you upload as premium raises the bar and is either sexier, or gives your fans something they ordinarily don't receive or see.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse124" aria-expanded="true" aria-controls="collapse19">
                                  4. WHAT KIND OF RESTRICTIONS ARE THERE FOR PREMIUM VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse124" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>As an The Royal Fruit model, you are the producer of all your own content. You are responsible to comply with any and all laws with respect to the content you produce. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse125" aria-expanded="true" aria-controls="collapse19">
                                  5. HOW MUCH CAN I CHARGE FOR A PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse125" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Models set their own prices for premium videos. Videos can be as low as $5 and as much as $100. The most important thing is to make sure you are giving your fans a good value for what they are spending. Don't overcharge your fans because this can cause problems with refunds and chargebacks. Go to IsMyVids.com and you can see how much other models charge. Please note that fans are charged $5 extra per video at IsMyVids.com than they are when they follow the model.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse126" aria-expanded="true" aria-controls="collapse19">
                                  6. HOW OFTEN SHOULD I POST PREMIUM VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse126" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Simply put, the more videos you post, the more money you are going to make. Most of our models post at least one or two premium videos per week. Other models post more often than that. Make sure you let your fans who follow both on The Royal Fruit and on social media know that you have an amazing premium video for sale!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse127" aria-expanded="true" aria-controls="collapse19">
                                  7. IS THERE A MAXIMUM TO THE FILE SIZE I CAN POST?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse127" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit is optimized to allow our models to upload the biggest files possible. Currently, models can upload photos and videos up to 5GB. If you ever have an issue uploading a file, click support at the bottom right of your model dashboard. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse128" aria-expanded="true" aria-controls="collapse19">
                                  8. IS THERE A MINIMUM TIME DURATION FOR A PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse128" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Premium videos must be at least 30 seconds, but the better value you give your fans, the more likely they will buy more videos from you again!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse130" aria-expanded="true" aria-controls="collapse19">
                                  9. I HAVE PROFESSIONAL VIDEOS, CAN I UPLOAD AS A PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse130" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>As long as you own the video or have the legal right to post the video, you can!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse131" aria-expanded="true" aria-controls="collapse19">
                                  10. HOW WILL MY FANS SEE MY PREMIUM VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse131" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Premium videos appear blurred with a lock on them so your fans know they must pay extra to unlock it. Your premium video will also appear in our our Premium Video Store (IsMyVids.com). Make sure you put a juicy title and description so your fans will know what to expect when they purchase your Premium Video. We also offer the option for you to select a thumbnail for these videos too!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse132" aria-expanded="true" aria-controls="collapse19">
                                  11. HOW CAN I LET MY SOCIAL MEDIA FOLLOWERS KNOW I HAVE A NEW PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse132" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Everytime you post a premium video, you should create a "call to action" on your Instagram, Facebook, Twitter, and Snapchat. Let your fans know to follow your The Royal Fruit profile to access your premium video and share with them your The Royal Fruit vanity link.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse133" aria-expanded="true" aria-controls="collapse19">
                                  12. CAN OTHER PEOPLE APPEAR IN MY PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse133" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. You are the creator and producer of your own content. Other people must be 18 and over and you must make sure you keep a copy of their ID for your records, since you are the producer and record-keeper of your own content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse134" aria-expanded="true" aria-controls="collapse19">
                                  13. DO PREMIUM VIDEOS NEED TO BE NUDE OR SEXUAL IN NATURE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse134" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While The Royal Fruit makes no actual requirements in terms of what the content you create is, we ask our models to understand that you must be honest and transparent with your fans as to what is the nature of your content that you are asking them to pay extra for. You are free to do nude or implied videos as you see fit, as long as you feel that you are giving your fans a fair deal and are pricing your videos accordingly. </p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Private Photos/Videos</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse135" aria-expanded="true" aria-controls="collapse19">
                                  1. WHAT IS THE DIFFERENCE BETWEEN A PRIVATE VIDEO AND PREMIUM VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse135" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While both premium and private videos are items which fans must pay extra for and that you set your own price for, the way which the fans access these videos are very different. Premium videos live in your content feed completely locked until your fans pay for them. Premium videos are also available in our premium video store at IsMyVids.com.</p>
                                    <p>Private video messages are locked videos which can only be sent in a direct message. Many The Royal Fruit models make and sell videos that are only intended for specific fans and not made available to all by making it a premium video. Instead, they opt to possible charge more and send this discreet video as a private video message, rather than a premium video. When you set your price for your private video messages, you can choose exactly which of your fans you would like to send this private video to. We even have a feature for you to send a video to all of your followers at once. Always make your message sound personal so your fans feel like you made these videos just for them. A personal touch goes a long way!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse136" aria-expanded="true" aria-controls="collapse19">
                                  2. ARE THERE ANY RESTRICTIONS ON WHAT TYPE OF CONTENT I CAN POST IN MY PRIVATE VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse136" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>As an The Royal Fruit model, you are the producer of all your own content. You are responsible to comply with any and all laws with respect to the content you produce. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse137" aria-expanded="true" aria-controls="collapse19">
                                  3. WHAT TYPE OF CONTENT WILL MY FOLLOWERS EXPECT AS A PRIVATE VIDEO VS A PREMIUM VIDEO?−
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse137" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Many models will use similar videos between their premium and private. Many fans are interested in private video messages that are up close and personal. If you can make your fans feel that the video is just for them, you may be pleasantly surprised how successful you become on our platform. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse138" aria-expanded="true" aria-controls="collapse19">
                                  4. WHY WOULD I WANT TO MAKE A VIDEO PRIVATE INSTEAD OF PREMIUM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse138" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>With premium videos - while the fans cannot see the actual video, they do see the title and your fans will know exactly what type of video you are selling. Private videos are more discreet and personal and can be sent to fans one-by-one or in a group.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse139" aria-expanded="true" aria-controls="collapse19">
                                  5. WHY SHOULD I NEVER UPLOAD THE SAME VIDEO AS A PREMIUM VIDEO AND A PRIVATE VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse139" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Models set their own prices for private video messages. Videos can be as low as $5 and as much as $100. The most important thing is to make sure you are giving your fans a good value for what they are spending. Don't overcharge your fans because this can cause problems with refunds and chargebacks. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse140" aria-expanded="true" aria-controls="collapse19">
                                  6. IS THERE A MINIMUM AND MAXIMUM PRICE OF WHAT I CAN CHARGE FOR MY PRIVATE VIDEOS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse140" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Models should never try to sell the same video as a premium and private and here is why:</p>
                                    <p>We do not want a fan to pay for the same video twice. This will cause chargebacks and refunds and goes against our terms of service. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse142" aria-expanded="true" aria-controls="collapse19">
                                  7. HOW DO DEAL WITH PRIVATE VIDEO REQUESTS IM NOT COMFORTABLE WITH?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse142" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While the The Royal Fruit community is a safe place for our models to have their boundaries respected, as this is the culture we have created, of course some fans will ask for more. If you get a request you are not comfortable with, you can either simply respond and tell the fans what you will do (not what you won't do) or you can simply choose to ignore these requests. If a fan is every rude to you, email <a href="javascript:;"> squad@The Royal Fruit.com </a>and we will block them for you. The Royal Fruit has a zero tolerance policy if a fan is rude or abusive. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse143" aria-expanded="true" aria-controls="collapse19">
                                  8. CAN I GIVE A PRIVATE VIDEO MESSAGE AS A FREE GIFT TO MY FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse143" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>No. Private video messages must be at least $5 or more. Since private video messaging is a premium feature, it is never free. If you want to give your fans something special, make an amazing video and put it on your content feed for your paying subscribers to see. Also, if you would like to send your fans a bonus, you can for free, send a photo in the private message for free.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse144" aria-expanded="true" aria-controls="collapse19">
                                  9. HOW WILL MY FOLLOWERS KNOW IVE CREATED A NEW PRIVATE VIDEO
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse144" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Your fans will know that you have a private video message when you choose to send it to them in a message. They will get a notification in their The Royal Fruit user dashboard and they will additionally receive an email that you sent them a private video message. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse145" aria-expanded="true" aria-controls="collapse19">
                                  10. WHEN I UPLOAD A PRIVATE VIDEO AND CLICK SHARE, WHAT DOES IT MEAN TO SHARE WITH ALL FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse145" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>This is one of our most clever features. You can send a mass message to all of your paying subscribers (you can also message your cancelled subscribers) with a private video message and if you personalize it correctly, you can make your fans feel as though the video is just for them. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse146" aria-expanded="true" aria-controls="collapse19">
                                  11. WHERE DO MY FOLLOWERS SEE MY PRIVATE PHOTOS/VIDEOS ON THE SITE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse146" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>In your user dashboard, right on the top of the drop down menu in mobile, or the sidebar on desktop, is a section called "Private Video Messages." This is where all of your private photos and video messages will appear. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse147" aria-expanded="true" aria-controls="collapse19">
                                  12. WHEN A FOLLOWER STOPS FOLLOWING ME, DO THEY STILL HAVE ACCESS TO THE PREMIUM OR PRIVATE VIDEOS THEY HAVE ALREADY PURCHASED?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse147" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, any premium products your fans buy from you, they will have access to it in their user profile as long as they have their account. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse148" aria-expanded="true" aria-controls="collapse19">
                                  13. CAN OTHER PEOPLE APPEAR IN MY PRIVATE VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse148" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes. You are the creator and producer of your own content. Other people must be 18 and over and you must make sure you keep a copy of their ID for your records, since you are the producer and record-keeper of your own content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse149" aria-expanded="true" aria-controls="collapse19">
                                  14. DO PRIVATE VIDEOS NEED TO BE NUDE OR SEXUAL IN NATURE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse149" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While The Royal Fruit makes no actual requirements in terms of what the content you create is, we ask our models to understand that you must be honest and transparent with your fans as to what is the nature of your content that you are asking them to pay extra for. You are free to do nude or implied videos as you see fit, as long as you feel that you are giving your fans a fair deal and are pricing your videos accordingly. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse150" aria-expanded="true" aria-controls="collapse19">
                                  15. IF I SEND A PRIVATE VIDEO TO ONE OF MY FOLLOWERS, WILL ANYONE ELSE KNOW ABOUT IT?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse150" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>No. When you send a private video message, it is a closed direct message between you and the follower. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse151" aria-expanded="true" aria-controls="collapse19">
                                  16. WHAT HAPPENS IS I SEND A PRIVATE VIDEO TO A FOLLOWER AND THEY DON'T BUY IT?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse151" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you send a private video to a follower and they choose not to purchase it, it remains locked just like an unpurchased premium video. Nothing is ever exposed. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse152" aria-expanded="true" aria-controls="collapse19">
                                  17. HOW OFTEN SHOULD I SEND MY FANS PRIVATE VIDEO MESSAGES?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse152" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>It is entirely up to the model how often she sends a private video message to her fans. Obviously the more videos you send, the greater the chance your fans will pay to open them and you earn extra money. It also depends on the type of content you make and the relationship with your fans. No one knows your fans better than you do, so trust your instinct.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse153" aria-expanded="true" aria-controls="collapse19">
                                  18. I HAVE PROFESSIONAL VIDEOS, CAN I UPLOAD AS A PRIVATE VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse153" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You can upload any type of video you would like as a private video. You know your fans better than anyone and you know what they like. While professional videos are amazing, we highly recommend uploading personal videos that you take yourself, so your fans can get to know the real you. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse154" aria-expanded="true" aria-controls="collapse19">
                                  19. IS THERE A MAXIMUM OF FILE SIZE I CAN POST?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse154" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Here at The Royal Fruit, we do our best to allow models to upload large files. Currently we allow upwards of 5GB. If you ever have trouble uploading a video, please contact us in the bottom right of your model dashboard for assistance.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse155" aria-expanded="true" aria-controls="collapse19">
                                  20. IS THERE A MINIMUM OR MAXIMUM TIME DURATION FOR A PRIVATE VIDEO?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse155" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Private video messages have a minimum duration of 30 seconds to avoid chargebacks and refunds. The longer your videos, the better!</p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Live Streaming</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse156" aria-expanded="true" aria-controls="collapse19">
                                  1. CAN I BLOCK SOMEBODY WHEN I AM LIVE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse156" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes you can. When you are live, you simply click the blue BLOCK button and then select the user you wish to block from the dropdown menu.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse157" aria-expanded="true" aria-controls="collapse19">
                                  2. CAN I GET TIPPED DURING LIVESTREAMING?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse157" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes you can. Your fans will tip you in tokens and a notification will appear in your chat. You can view your earnings in your model dashboard in the live streaming section.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse158" aria-expanded="true" aria-controls="collapse19">
                                  3. CAN PEOPLE THAT ARE NOT FOLLOWING ME SEE MY LIVESTREAM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse158" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When you are in paid chat (group chat) any user that is a follower or non-follower that wants to spend tokens to join your chat can. This is a great way to gain new followers and make money.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse159" aria-expanded="true" aria-controls="collapse19">
                                  4. DO I NEED TO BE ON WIFI TO LIVESTREAM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse159" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>No, you do not need to be on WiFi, but the stronger your connection and signal is, the better quality and experience you and your followers will have live streaming together. Upgrading your home's WiFi service is recommended if you will be using the live streaming feature extensively to make money.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse161" aria-expanded="true" aria-controls="collapse19">
                                  5. DOES LIVESTREAMING WORK FROM MY MOBILE PHONE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse161" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, you can live stream from your mobile phone, computer, or tablet.</p>
                                    <p>Please use<strong> Safari browser</strong> if you're using an iOS device and <strong>Chrome</strong> browser if you're using an Android device. And for the best experience, please use the latest version of iOS or Android.</p>
                                    <p>However, certain devices do not support live streaming. Huawei devices such as P20 do not have the hardware that support live streaming.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse162" aria-expanded="true" aria-controls="collapse19">
                                  6. HOW DO I SET PRICES PER MINUTE FOR LIVE STREAMING?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse162" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>In your Model Dashboard under<a href="setting.html"> Settings </a>in the dropdown menu scroll down to the "Pricing" section and select your price-per-minute from the dropdown menu. Each token = $1.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse163" aria-expanded="true" aria-controls="collapse19">
                                  7. HOW DO I TURN FANCAM ON OR OFF?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse163" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>To turn FanCam on or off, click on the dropdown menu in your Model Dashboard, go to <a href="setting.html"> Settings</a> and scroll down to the bottom, where you will see a pre-checked blue box that says enable FanCam.</p>
                                    <p>Uncheck this box to disable your fan's camera and mic, so the fans can see you, but you do not see them! We call this 1on1 instead of 1on1 with FanCam.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse164" aria-expanded="true" aria-controls="collapse19">
                                  8. HOW WILL MY FOLLOWERS KNOW THAT IM LIVESTREAMING?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse164" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>There are several ways your followers will know you are live streaming. Followers receive both a pop-up notification as well as an alert in their notifications section that you are now live and they get a link to join your Livestream.</p>
                                    <p>Additionally, you will appear in our Live Models section in the home page as well as appearing in a red circle on top of the home page and user dashboards alerting that you are live.</p>
                                    <p>We also send your followers an email that you are now live and live streaming.</p>
                                    <p>Don't forget to alert your fans on your social media that you are about to go live! Create a "call to action" to let them know when to tune in. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse166" aria-expanded="true" aria-controls="collapse19">
                                  9. MY 1 ON 1 CHAT CLOSED OUT IN THE MIDDLE OF A LIVESTREAM. HOW DO I GET IT BACK?−
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse166" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Refresh your browser and it should automatically reconnect you with your followers. If the problem persists, check your internet connection or speed. If this does not solve the problem, email<a href="javascript:;"> The Royal Fruit support team.</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse167" aria-expanded="true" aria-controls="collapse19">
                                  10. THE VIDEO ON MY LIVESTREAM IS CHOPPY AND FREEZING UP. WHAT DO I DO TO FIX IT?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse167" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Check your internet connection and speed. If you are streaming from your phone try not to move around too much.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse168" aria-expanded="true" aria-controls="collapse19">
                                  11. WHAT ARE THE DIFFERENT TYPES OF LIVE STREAMING ON The Royal Fruit?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse168" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>There are four types of Live Streaming for you to engage with your followers on The Royal Fruit:</p>
                                    <p>1. Paid Chat - Your followers and other The Royal Fruit users can join your Live Stream by purchasing tokens and paying per minute.</p>
                                    <p>2. Free to followers - When you click the Go Live button in your dashboard, you can select your chat room type. When you select Free to Followers Only, all of your paying followers/subscribers can access this exclusive free Live Stream.</p>
                                    <p>3. 1-on-1 with FanCam - This is the most exclusive one on one experience that you can give your followers. In 1-on-1 FanCam mode, you and your followers can see each other just like FaceTime.</p>
                                    <p>4. 1-on-1 without FanCam is for those models that choose not to allow their followers to share their camera and mic. If you opt out of the 1-on-1 FanCam box in your settings, you will be defaulted to the 1-on-1.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse169" aria-expanded="true" aria-controls="collapse19">
                                  12. WHAT DEVICES CAN I LIVESTREAM FROM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse169" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Live Streaming will work from your phone, computer or tablet. We are doing our best to support all devices, but please note a few things:</p>
                                    <ol>
                                    <li>High speed - Wifi and internet speed are definitely helpful. If you are live streaming from your phone and you have poor service, your livestream can be choppy.</li>
                                    <li>Please note that while we are working on a dedicated app for our models to manage their The Royal Fruit accounts, live streaming takes place in your browser, so to avoid interrupting your live stream and not giving your fans the best experience, close out your other apps and do not answer texts or calls or open other apps as they will cut off your live stream.&nbsp;</li>
                                    <li>If you're using an iOS device, please use<strong> Safari</strong> browser and if you're using an Android device, please use <strong>Chrome</strong> browser. Make sure you are updated to the latest software update.</li>
                                    <li>Some devices do not have the hardware to support live streaming such as the Huawei P20. If you are using this device, please use a different device to use live stream.&nbsp;</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse170" aria-expanded="true" aria-controls="collapse19">
                                  13. WHAT DOES IT MEAN WHEN I GET A ONE ON ONE OR FANCAM REQUEST?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse170" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A FanCam request is one of your followers inviting you to a private one on one two-way video chat (ex. FaceTime). You need to make sure to go to settings in your model dashboard to set your prices to what you wish to charge your followers for FanCam.</p>
                                    <p>For those models that do not wish to see their followers, but prefer to do a one-way live stream, models can opt-out of the FanCam feature in which case, private requests will be called One-on-One and the follower can see the model, but the model cannot see the follower.</p>
                                    <p>Regardless if you use FanCam or One-on-One, you can see all your requests in either your One-on-One button or your red notifications badge section. If a user made a prior request and you missed it, message him and let him know you're available.</p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse172" aria-expanded="true" aria-controls="collapse19">
                                  14. WHAT IS A TOKEN?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse172" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A token is a virtual currency. One token equals one US dollar. Followers purchase tokens in bundles and spend them on you and other models. Whether the followers spend tokens on you for FanCam, Paid Chat or if they tip you using tokens, you get the same 70% after processing fees that you do on all your other revenue streams on The Royal Fruit. </p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse173" aria-expanded="true" aria-controls="collapse19">
                                  15. WHAT IS FREE TO FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse173" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Free to Followers is similar to Instagram Live, but exclusive to your subscribed followers. While you do not get paid extra for this, this is a great way to let your followers get to know you and give them a great value and thank you for following. Frequent live chats will make your followers want to continue following you instead of unsubscribing. Also, followers can tip you while doing Free to Followers. </p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse174" aria-expanded="true" aria-controls="collapse19">
                                  16. WHAT IS GO LIVE LIVE STREAMING ON INKEDGIRL.COM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse174" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Go Live is the The Royal Fruit live streaming feature. This is how you can live stream with your followers and greatly increase your revenue. You can do either Free for Followers, Paid group chat, or One-on-One/FanCam live streams. </p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse175" aria-expanded="true" aria-controls="collapse19">
                                  17. WHAT IS ONE ON ONE WITH FANCAM?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse175" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When you receive a "One-on-One" or "One-onOne w/ FanCam" request, this means that a follower is inviting you to an exclusive one-on-one live stream where you set your price-per-minute and where you can engage with your followers exclusively and get tips.  </p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse176" aria-expanded="true" aria-controls="collapse19">
                                  18. WHAT PAID/GROUP CHAT?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse176" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Paid/Group Chat is very similar to live streaming on social media, like Instagram, except everyone that wants to join your live. stream must pay your tokens-per-minute price. Please note that this is a group chat and you can live stream to many followers at once. Ex. 10 followers paying $3/minute in paid chat for 20 minutes would pay you over $350. </p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse177" aria-expanded="true" aria-controls="collapse19">
                                  19. WHEN LIVE STREAMING FROM MY PHONE, DO I NEED TO CLOSE OUT ALL OF MY OTHER APPS FOR OPTIMAL PERFORMANCE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse177" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>While it is not necessary to close out all of your other apps while Livestreaming, it is recommended to not use other apps, such as texting while Livestreaming as it will interrupt your Live Stream feed.</p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse179" aria-expanded="true" aria-controls="collapse19">
                                  20. WHERE CAN I GET SUPPORT FOR LIVESTREAMING?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse179" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Technical support for Livestreaming is like getting support for any other feature on The Royal Fruit. Regardless of what you need help with you can either use our live support chat box in the bottom right of your model dashboard or send an email to <a href="javascript:;"> support@The Royal Fruit.com.</a></p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse180" aria-expanded="true" aria-controls="collapse19">
                                  21. WHERE CAN I SEE MY LIVESTREAM EARNINGS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse180" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>In your model dashboard, there is a summary panel where you can see your Livestream earnings for both paid minutes and tokens that are tipped. Additionally, those earnings are added to your recent total earnings and your earnings per period. </p>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse181" aria-expanded="true" aria-controls="collapse19">
                                  22. WHERE CAN I SEE MY ONE ON ONE REQUESTS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse181" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>In your model dashboard, you can see your One-on-One requests in two places:</p>
                                    <ol>
                                    <li>By clicking the one-on-one button in the live page.</li>
                                    <li>All requests also are viewable and clickable in your notifications section.&nbsp;</li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Model Merchandise</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse182" aria-expanded="true" aria-controls="collapse19">
                                  1. WHAT IS A MODEL SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse182" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit creates branded model merchandise shops for our models who have 50 followers or more on our platform. We make items ranging from hoodies and t-shirts to beach towels and phone cases and much more! If you have 50 or more followers and want a shop, please email shop@The Royal Fruit.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse183" aria-expanded="true" aria-controls="collapse19">
                                  2. HOW DO I GET MY VERY OWN MODEL SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse183" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If you have 50 or more followers, then please email shop@The Royal Fruit.com to get started!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse184" aria-expanded="true" aria-controls="collapse19">
                                  3. HOW MANY ITEMS CAN I SELL IN MY MODEL SHOP?    
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse184" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>New model shops typically launch with four items and while we try to make as many items as possible for our models, please be patient and pick your favorite items!</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse185" aria-expanded="true" aria-controls="collapse19">
                                  4. HOW MANY FOLLOWERS DO I NEED BEFORE I CAN GET MY OWN MODEL SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse185" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We require all of our models to have at least 50 followers in order for you to create your own model shop! </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse186" aria-expanded="true" aria-controls="collapse19">
                                  5. WHO PRODUCES, STOCKS AND SHIPS MY MODEL MERCHANDISE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse186" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The Royal Fruit works with Shopify and Printful to produce and fulfill all models' fan orders. We at the The Royal Fruit HQ will design your products from images provided by the model and add them to your model shop. The items are then dropshipped by Printful to fulfill orders to your fans. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse187" aria-expanded="true" aria-controls="collapse19">
                                  6. HOW MUCH DO I EARN FROM MERCHANDISE SOLD?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse187" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Models receive a royalty of 25% of the sale price of the merchandise sold. 50% covers the cost of the actual merchandise itself and shipping and 25% goes to us for creating and marketing the items and managing the shop. The prices for merchandise are uniform across the shop. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse188" aria-expanded="true" aria-controls="collapse19">
                                  7. HOW DO I PROMOTE MY MODEL SHOP LINK?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse188" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Many of our models promote their Model Shop link in their The Royal Fruit profile bio. You can also link it to your social media platforms.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse189" aria-expanded="true" aria-controls="collapse19">
                                  8. WHAT ITEMS ARE AVAILABLE IN THE MODEL SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse189" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We offer many items ranging from hoodies, to hats to beach towels, and more! </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse190" aria-expanded="true" aria-controls="collapse19">
                                  9. WHAT RESOLUTION MUST MY IMAGES BE TO CREATE PRODUCTS IN MY MODEL SHOP?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse190" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>To create items in our merchandise shop, we require high-res photos with a minimum 300 dpi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse191" aria-expanded="true" aria-controls="collapse19">
                                  10. CAN I BUY MY OWN MERCHANDISE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse191" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Models can buy their own merchandise at a 25% discount. </p>
                                </div>
                            </div>
                        </div>

                        <h2 class="mt-3">Subscriptions-Follows</h2>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse192" aria-expanded="true" aria-controls="collapse19">
                                  1. WHAT ARE THE FOLLOW PRICE OPTIONS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse192" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>We offer many follow price options starting at $10/month to $60/month. Simply go to your "settings," scroll down to "Follow Price," and select from our many options there. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse193" aria-expanded="true" aria-controls="collapse19">
                                  2. WHAT DOES A FOLLOWER GET ACCESS TO WHEN THEY SUBSCRIBE TO MY PAGE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse193" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Once a follower subscribes to your page, your follower will get to see the videos and photos that you post to your content feed. They will also be able to hold a conversation with you. In order to access your private video messages, premium videos, Premium Snapchat, live streaming, 1-1 FanCam, etc. your follower will have to purchase that at an additional cost.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse194" aria-expanded="true" aria-controls="collapse19">
                                  3. WHAT IS A FOLLOW?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse194" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A follow is when a user subscribes to see your page! Every time you get a new follower, you will receive a notification on your model dashboard and in your e-mail.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse195" aria-expanded="true" aria-controls="collapse19">
                                  4. ARE FOLLOWERS AUTOMATICALLY RE-BILLED FOR THEIR MONTHLY SUBSCRIPTION?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse195" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>If a follower doesn't cancel his or her subscription, they will automatically be re-billed for their monthly subscription at the price they originally subscribed to first. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse196" aria-expanded="true" aria-controls="collapse19">
                                  5. WHAT HAPPENS WHEN A FOLLOWER CANCELS THEIR SUBSCRIPTION?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse196" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>When a follower cancels their subscription, they will be able to access your content feed for the remainder of the month. Once their monthly subscription is over, they will not be re-billed, and they will no longer have access to your profile.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse197" aria-expanded="true" aria-controls="collapse19">
                                  6. CAN I DIRECT MESSAGE MY CANCELED FOLLOWERS?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse197" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes! We created a new feature where you now may message your canceled followers to entice them to follow and subscribe to your profile again. This is a great way to earn extra revenue. Use this to your advantage! When you send a private video message, you can now click on the drop-down menu to message to your canceled followers. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse198" aria-expanded="true" aria-controls="collapse19">
                                  7. CAN I BLOCK A FOLLOWER?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse198" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, you can block a follower. If you would like to block a follower, please contact the help center at the bottom right of your model dashboard, and our team will assist in blocking a user. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header card-model-hun" id="heading19">
                              <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn1 btn1-link" data-toggle="collapse" data-target="#collapse199" aria-expanded="true" aria-controls="collapse19">
                                  8. HOW MUCH SHOULD I CHARGE FOR MY FOLLOW PRICE?
                                  <span class="fa-stack fa-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </h2>
                            </div>
                            <div id="collapse199" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                                <div class="card-body">
                                    <p>There is no right or wrong way to choose your follow price. There are benefits to charging more and there are benefits to charging less. We highly recommend playing around with your follow prices to find the right middle ground for you! </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).parent('.panel').find('.fa-minus').show();
        $(this).parent('.panel').find('.fa-plus').hide();
    })
    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).parent('.panel').find('.fa-minus').hide();
        $(this).parent('.panel').find('.fa-plus').show();
    })
</script>
<script>
   $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
  $(e.target)
    .prev()
    .find("i:last-child")
    .toggleClass("fa-minus fa-plus");
});
</script>
<script>
   $("#accordion1").on("hide.bs.collapse show.bs.collapse", e => {
  $(e.target)
    .prev()
    .find("i:last-child")
    .toggleClass("fa-minus fa-plus");
});
</script>
