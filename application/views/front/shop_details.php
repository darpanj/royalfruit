<div class="site-custom site-shop site-detail site-shop-detail">
    <section class="site-blocks-cover overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 align-self-center">

                    <div class="row">

                    </div>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12" style="margin-top: -20%;">

                </div>
            </div>
        </div>
    </section>


    <div class="cus-nav">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="cus-feed2">
                        <ul>
                            <li><a href="">Cart&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url() . FRONT_IMG; ?>cart.png" width="15"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="site-shop">
        <div class="container">
            <div class="row shop-dt">
                <div class="d-flex shop-main">
                    <div class="shop-img-box d-flex">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#flamingo" role="tab" aria-controls="pills-flamingo" aria-selected="true"><img src="<?php echo base_url() . FRONT_IMG; ?>shop-img1.png" width="20"></a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#cuckoo" role="tab" aria-controls="pills-cuckoo" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>shop-img1.png" width="20"></a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#cuckoo" role="tab" aria-controls="pills-cuckoo" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>shop-img1.png" width="20"></a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#cuckoo" role="tab" aria-controls="pills-cuckoo" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>shop-img1.png" width="20"></a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#cuckoo" role="tab" aria-controls="pills-cuckoo" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>shop-img1.png" width="20"></a></li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane fade show active" id="flamingo" role="tabpanel" aria-labelledby="flamingo-tab">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>shop-img2.png" width="150">
                            </div>
                            <div class="tab-pane fade" id="cuckoo" role="tabpanel" aria-labelledby="profile-tab">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>shop-img2.png" width="150">
                            </div>
                            <div class="tab-pane fade" id="ostrich" role="tabpanel" aria-labelledby="ostrich-tab">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>shop-img2.png" width="150">
                            </div>
                            <div class="tab-pane fade" id="tropicbird" role="tabpanel" aria-labelledby="tropicbird-tab">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>shop-img2.png" width="150">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="shop-right">
                    <h2>NIKKI NICHOLE UNISEX SWEATSHIRT</h2>
                    <span>$25.00</span>

                    <div class="shop-color">
                        <p>Color</p>
                        <ul>
                            <li><a href=""></a></li>
                            <li><a href=""></a></li>
                        </ul>
                    </div>
                    <h4 class="shop-size mt-4">Size</h4>
                    <select class="select">
                        <option>Select Size</option>
                        <option>L</option>
                        <option>M</option>
                        <option>Xl</option>
                        <option>XXl</option>
                        <option>XXXl</option>
                    </select>

                    <div class="plus-min">
                        <form method='POST' action='#'>
                            <div class="quantity">
                                <label>Qty</label>
                                <input type='button' value='-' class='minus ml-4' field='quantity' /><input type='text' name='quantity' value='1' class='qty' />
                                <input type='button' value='+' class='plus' field='quantity' />
                                <button type="button" class="cart-btn">Add to Cart</button>
                            </div>
                        </form>
                    </div>

                    <div class="social">
                        <ul>
                            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="shop-pay mt-4">
                        <ul>
                            <li><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>amex.svg" width="25"></a></li>
                            <li><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>apple-pay.png" width="38"></a></li>
                            <li><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>mastercard.svg" width="25"></a></li>
                            <li><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>visa.svg" width="25"></a></li>
                        </ul>
                    </div>
                </div>


            </div>


        </div>
    </section>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="shop-more mb-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"><i>MORE INFO</i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"><i>DATA SEET</i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"><i>VIDEO</i></a>
                        </li>
                    </ul>


                    <div class="tab-content tab-content1">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <p>A sturdy and warm sweatshirt bound to keep you warm in the colder months. A pre-shrunk, classic fit sweater that's made with air-jet spun yarn for a soft feel and reduced pilling.</p>
                            <ul>
                                <li>50% cotton, 50% polyester</li>
                                <li>Pre-shrunk</li>
                                <li>Classic fit with no center crease</li>
                                <li>1x1 athletic rib knit collar with spandex</li>
                                <li>Air-jet spun yarn with a soft feel and reduced pilling</li>
                                <li>Double-needle stitched collar, shoulders, armholes, cuffs, and hem</li>
                            </ul>

                        </div>
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <p>A sturdy and warm sweatshirt bound to keep you warm in the colder months. A pre-shrunk, classic fit sweater that's made with air-jet spun yarn for a soft feel and reduced pilling.</p>
                            <ul>
                                <li>50% cotton, 50% polyester</li>
                                <li>Pre-shrunk</li>
                                <li>Classic fit with no center crease</li>
                                <li>1x1 athletic rib knit collar with spandex</li>
                                <li>Air-jet spun yarn with a soft feel and reduced pilling</li>
                                <li>Double-needle stitched collar, shoulders, armholes, cuffs, and hem</li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="tabs-3" role="tabpanel">
                            <p>A sturdy and warm sweatshirt bound to keep you warm in the colder months. A pre-shrunk, classic fit sweater that's made with air-jet spun yarn for a soft feel and reduced pilling.</p>
                            <ul>
                                <li>50% cotton, 50% polyester</li>
                                <li>Pre-shrunk</li>
                                <li>Classic fit with no center crease</li>
                                <li>1x1 athletic rib knit collar with spandex</li>
                                <li>Air-jet spun yarn with a soft feel and reduced pilling</li>
                                <li>Double-needle stitched collar, shoulders, armholes, cuffs, and hem</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="detail-page">
        <div class="container">
            <h3 class="mt-4">Releted Products</h3>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-lg-3 det-top">
                    <div class="shop-box">
                        <span>Sale</span>
                        <img src="<?php echo base_url() . FRONT_IMG; ?>mimi.png">
                        <p>NIKKI NICHOLE UNISEX SWEATSHIRT</p>

                        <div class="shop-price">
                            <h2>$25.00</h2>
                            <img src="<?php echo base_url() . FRONT_IMG; ?>add-cart.png" width="20">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-lg-3 det-top">
                    <div class="shop-box">
                        <span>Sale</span>
                        <img src="<?php echo base_url() . FRONT_IMG; ?>mimi.png">
                        <p>NIKKI NICHOLE UNISEX SWEATSHIRT</p>

                        <div class="shop-price">
                            <h2>$25.00</h2>
                            <img src="<?php echo base_url() . FRONT_IMG; ?>add-cart.png" width="20">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-lg-3 det-top">
                    <div class="shop-box">
                        <span>Sale</span>
                        <img src="<?php echo base_url() . FRONT_IMG; ?>mimi.png">
                        <p>NIKKI NICHOLE UNISEX SWEATSHIRT</p>

                        <div class="shop-price">
                            <h2>$25.00</h2>
                            <img src="<?php echo base_url() . FRONT_IMG; ?>add-cart.png" width="20">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-lg-3 det-top">
                    <div class="shop-box">
                        <span>Sale</span>
                        <img src="<?php echo base_url() . FRONT_IMG; ?>mimi.png">
                        <p>NIKKI NICHOLE UNISEX SWEATSHIRT</p>

                        <div class="shop-price">
                            <h2>$25.00</h2>
                            <img src="<?php echo base_url() . FRONT_IMG; ?>add-cart.png" width="20">
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>