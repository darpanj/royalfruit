<div class="site-become">
    <section class="site-blocks-cover overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 align-self-center">

                    <div class="row">

                    </div>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12" style="margin-top: -20%;">
                    <div class="slide-one-item home-slider owl-carousel">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" alt="Image" class="img-fluid img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>slider2.png" alt="Image" class="img-fluid img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>slider3.png" alt="Image" class="img-fluid img">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- feature -->
    <section class="model-girl2 model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-4" style="align-self: center;">
                    <h2>Why RoyalFruit?</h2>
                    <p>Premium Social Media has arrived.</p>
                    <p>RoyalFruit is the ultimate one stop shop social media platform that empowers models with the ability to convert fans into paying followers and monetize your brand and content. RoyalFruit boasts ten different revenue streams for our models to maximize earnings in the shortest time possible, including monthly recurring subscriptions, premium videos, private video messages, pay per minute livestreaming, tipping, Premium Snap, and much more! Give your fans the ultimate social experience with ease, right from your mobile dashboard!</p>

                    <button type="button" class="model-btn">Become Model Now</button>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                        <div class="col-sm-4 mt-4">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%" height="120">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="model-girl model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-6 model-img">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>model1.png" width="70%">
                </div>
                <div class="col-md-6">
                    <h2>Why RoyalFruit?</h2>
                    <p>More promotion, more revenue streams, more features, and more money.</p>
                    <p>If you compare RoyalFruit to other fan platforms you will clearly see all the advantages how much more we offer.</p>
                    <p>Most fan platforms, premium websites, and clip sites leave you alone on an island to promote and market yourself and your link. RoyalFruit and our partners, Inked Magazine actively market you and our models to our 40 million social media followers. We treat each and everyone of our models as business partners. And simply put, the more you promote yourself, the more we promote you.Why RoyalFruit?</p>
                    <p>More promotion, more revenue streams, more features, and more money.</p>
                    <p>If you compare RoyalFruit to other fan platforms you will clearly see all the advantages how much more we offer.</p>
                    <p>Most fan platforms, premium websites, and clip sites leave you alone on an island to promote and market yourself and your link. RoyalFruit and our partners, Inked Magazine actively market you and our models to our 40 million social media followers. We treat each and everyone of our models as business partners. And simply put, the more you promote yourself, the more we promote you.</p>

                    <button type="button" class="model-btn">Become Model Now</button>
                </div>
            </div>
        </div>
    </section>

    <section class="model-faq">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-6" style="align-self: center;">
                    <h2>How it's works?</h2>
                    <p>RoyalFruit is designed exclusively for models by models to be the ultimate premium social media platform to easily monetize their personal brand and content.</p>
                    <p>After you complete your model application and you are approved, you will get an email that explains exactly what you need to do to get started, gaining followers and taking advantage of all revenue streams to maximize your profit in the shortest time possible.</p>
                    <p>Models receive a custom vanity link (modelname.RoyalFruit.com). As an approved model, you can share your vanity link everywhere that your fans can find you. Fans can follow/subscribe to access your exclusive content on a recurring monthly basis. Once following you, fans can purchase premium videos, private video messages, tip you, livestream with you, FanCam with you, subscribe to your Premium Snap (if you have one), and much more.</p>
                    <p>RoyalFruit gives you tools to build a real connection with your fans to help you create real recurring revenue all from one easy to use model dashboard.</p>
                    <a href="">Visit our Model FAQ</a>
                </div>
                <div class="col-md-6 model-img2">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider4.png" width="70%;">
                </div>
            </div>
        </div>
    </section>

    <section class="model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-4">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider4.png" width="100%">
                    <h2>Premium Video</h2>
                    <p>Fans can special request custom videos from you, you set your own prices and can negotiate with your fans back and forth exactly what will be in your video and how much. Fans must pay first and you send the special requested video after.</p>
                    <button type="button" class="model-btn model-btn1">Become Model Now</button>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider4.png" width="100%">
                    <h2>Premium Snap</h2>
                    <p>Fans can special request custom videos from you, you set your own prices and can negotiate with your fans back and forth exactly what will be in your video and how much. Fans must pay first and you send the special requested video after.</p>
                    <button type="button" class="model-btn model-btn1">Become Model Now</button>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider4.png" width="100%">
                    <h2>Shoutouts</h2>
                    <p>Fans can special request custom videos from you, you set your own prices and can negotiate with your fans back and forth exactly what will be in your video and how much. Fans must pay first and you send the special requested video after.</p>
                    <button type="button" class="model-btn model-btn1">Become Model Now</button>
                </div>
            </div>
        </div>
    </section>

    <section class="model-video model-money">
        <div class="container">
            <div class="row model-cnt">
                <h2 class="d-flex p-4 m-auto">Ways to make money/revenue streams</h2>
                <p>In an effort to ensure that our models make the most money possible, RoyalFruit is constantly adding new features to our social network for you to be able to interact with your fans however you like all from one platform, instead of managing a bunch of different platforms, wasting your time, and confusing your fans with multiple links. While your fans are hungry for your content, they are even more willing to pay for real interaction and a personal connection. When your fans see that you are spreading yourself thin across many different platforms, they are often disappointed because they know it is impossible for you to actively engage in so many places at the same time. RoyalFruit has created the most robust platform in the industry, with more ways to make money from your fans than any other site on the web. Finally, you can give all your fans what they want from one convenient platform. Work smart, not hard! The following are the current and upcoming RoyalFruit money-making features.</p>
                <div class="col-sm-2 mt-4 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>m-icon1.svg" width="50">
                    </div>
                    <h3>Content Feed</h3>
                </div>
                <div class="col-sm-2 mt-4 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>pre-video.svg" width="50">
                    </div>
                    <h3>Premium Videos</h3>
                </div>
                <div class="col-sm-2 mt-4 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>pvt-video.svg" width="50" height="51">
                    </div>
                    <h3>Private Video Messages</h3>
                </div>
                <div class="col-sm-2 mt-4 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>Referrals.svg" width="50">
                    </div>
                    <h3>Referrals</h3>
                </div>
                <div class="col-sm-2 mt-4 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>m-snap.png" width="50">
                    </div>
                    <h3>Premium Snap</h3>
                </div>
                <div class="col-sm-2 mt-4 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>Custom-Video.svg" width="50">
                    </div>
                    <h3>Custom Video</h3>
                </div>
                <div class="col-sm-2 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>Livestream.svg" width="50">
                    </div>
                    <h3>Livestream</h3>
                </div>
                <div class="col-sm-2 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>fancam.png" width="50">
                    </div>
                    <h3>1 on 1 Fancam</h3>
                </div>
                <div class="col-sm-2 mb-4">
                    <div class="md-img">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>tips.png" width="50">
                    </div>
                    <h3>Tips</h3>
                </div>
            </div>
        </div>
    </section>

    <section class="model-girl model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-6" style="text-align: center;">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>model-c.png" width="50%">
                </div>
                <div class="col-md-6" style="align-self: center;">
                    <h2>Revenue share</h2>
                    <p>RoyalFruit models keep 70% of all revenue stream after credit card processing. Out of RoyalFruit’s revenue share. We provide 24/7 model support, social media promotion to over 40 million fans, and inclusion in our email blast and newsletters. We work constantly to promote and market our models, and are always developing new revenue-generating features.
                    </p>
                    <button type="button" class="model-btn">Become Model Now</button>
                </div>

            </div>
        </div>
    </section>

    <section class="model-girl model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-6" style="align-self: center;">
                    <h2>Marketing and Promotions</h2>
                    <p>While most fan platforms leave you all alone on an island with just your link and your own social media followers, RoyalFruit actively markets and promotes our models to our massive audience of 40 million fans.</p>
                    <p>RoyalFruit has a robust homepage with millions of visitors daily that encourages fans to discover new models, namely you.</p>
                    <p>RoyalFruit has a proven path to success for models who join our community. Everyday, we offer our models incredible promotional opportunities, including going live on our facebook, instagram and twitter promotion, mainstream media publicity, email blasts to millions of fans, billboards and much more.</p>
                    <button type="button" class="model-btn">Become Model Now</button>
                </div>
                <div class="col-md-6" style="text-align: center;">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>model-r.png" width="50%">
                </div>
            </div>
        </div>
    </section>

    <section class="model-girl3 model-girl2 model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-12" style="text-align: center;">
                    <h2 class="pb-5">The Lifestyle, Shoots, Parties, Events</h2>
                    <p>Whether doing sponsored photo shoots with the Hottest Photographers on IG, or Partying in a VIP Cabana @ Tao Beach Las Vegas the RoyalFruit model community is a sisterhood of empowered businesswomen... who work hard and play hard together, free from judgement and labels... we are Girl Gang!</p>

                    <button type="button" class="model-btn mt-5">Become Model Now</button>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial model-cnt">
        <div class="container">
            <h2 style="margin: auto;color: #601f78;display: table;">Testimonial Videos</h2>
            <div class="row">


                <div class="col-md-4 mt-3 mb-3">
                    <div class="testimonial-img video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#myModal">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" width="100%">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>play-button.svg" id="play" class="video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#myModal" width="50" style="cursor: pointer;">
                        <div class="test-cnt">
                            <h2>Sample Work</h2>
                            <p><i>Consulting,Photography,Video Production</i></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-3 mb-3">
                    <div class="testimonial-img video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#myModal">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" width="100%">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>play-button.svg" id="play" class="video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#myModal" width="50" style="cursor: pointer;">
                        <div class="test-cnt">
                            <h2>Sample Work</h2>
                            <p><i>Consulting,Photography,Video Production</i></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mt-3 mb-3">
                    <div class="testimonial-img video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#myModal">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" width="100%">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>play-button.svg" id="play" class="video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#myModal" width="50" style="cursor: pointer;">
                        <div class="test-cnt">
                            <h2>Sample Work</h2>
                            <p><i>Consulting,Photography,Video Production</i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="model-girl model-video">
        <div class="container">
            <div class="row model-cnt">
                <div class="col-md-6" style="text-align: center;">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>model2.jpg" width="100%">
                </div>
                <div class="col-md-6" style="align-self: center;">
                    <h2>Who can apply to be a model?</h2>
                    <p>Anyone 18 and over with a current government-issued photo ID can become an approved model on RoyalFruit. We take extra steps to verify our models’ identity and age. We require our models to have an active social media account (Instagram, Facebook, or Twitter) so that we can be sure that we will be working together to gain the most followers. Our application process also requires a selfie to further verify your identity to approve your account so you can start making money and having fun today.
                    </p>
                    <button type="button" class="model-btn">Become Model Now</button>
                </div>

            </div>
        </div>
    </section>

    <section class="model-girl model-video">
        <div class="container model-cnt">
            <h2 style="text-align: center; color: #601f78;">Are you Ready to Become a Model? <br>Create Your Free Account Now</h2>
            <div class="row model-cnt mt-5" id="become_model">
                <div class="col-md-6" style="text-align: center;">
                    <div class="login-form d-flex">

                        <form data-parsley-validate="" method="post" action="<?php echo base_url('home/signup_user'); ?>">
                            <div class="row">
                                <div class="col-12">
                                    <p>Sign Up Information</p>
                                </div>
                                <div class="col-12">
                                    <input type="text" class="form-control1" id="name" placeholder="<?php echo $this->lang->line('lbl_model_name'); ?>" name="name" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_model_name_required'); ?>">
                                </div>
                                <div class="col-12">
                                    <input type="text" class="form-control1" id="email" placeholder="<?php echo $this->lang->line('lbl_email'); ?>" name="email" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_email_required'); ?>" data-parsley-type="email" data-parsley-type-message="<?php echo $this->lang->line('err_email_valid_type'); ?>">
                                </div>
                                <div class="col-12">
                                    <input type="text" class="form-control1" id="confirm_email" placeholder="<?php echo $this->lang->line('lbl_confirm_email'); ?>" name="confirm_email" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_confirm_email_required'); ?>" data-parsley-type="email" data-parsley-type-message="<?php echo $this->lang->line('err_email_valid_type'); ?>" data-parsley-equalto="#email" data-parsley-equalto-message="<?php echo $this->lang->line('err_email_confirmEmail_same'); ?>">
                                </div>
                                <div class="col-12">
                                    <input type="password" class="form-control1" placeholder="<?php echo $this->lang->line('lbl_password'); ?>" name="password" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_pwd_required'); ?>" data-parsley-maxlength="16" data-parsley-maxlength-message="<?php echo $this->lang->line('err_pwd_maxlength'); ?>" data-parsley-minlength="6" data-parsley-minlength-message="<?php echo $this->lang->line('err_pwd_minlength'); ?>">
                                    <input type="hidden" name="type" value="model">
                                </div>
                                <div class="chk-lft mb-4">
                                    <input type="checkbox" name="is_accept" id="fruit1" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_age_confirmation'); ?>">
                                    <label for="fruit1">&nbsp; By checking this box, I certify that I am at least 18-years old, have the capacity to enter into legally binding contracts, and have read and agree to the terms and conditions.</label>
                                </div>
                                <div class="col-12 mt-4">
                                    <button type="submit">Join as Model</button>
                                </div>
                                <div class="col-12 user-log mt-4">
                                    <p>Already a Model? <a href="<?php echo base_url('home/login') ?>"><span>Login</span></a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 model-img2">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>model3.jpg" width="70%">
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial model-cnt text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mt-3 mb-3">
                    <div class="testimonial-img testimonial-img1">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%">
                        <div class="test-cnt2">
                            <h2 class="mb-4">Blog</h2>
                            <p>To learn more about the culture of our model money-making community, checkout blog.RoyalFruit.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3 mb-3">
                    <div class="testimonial-img testimonial-img1">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" width="100%">
                    </div>
                    <div class="test-cnt2">
                        <h2 class="mb-4">Got Questions?</h2>
                        <p>Head on to our Frequently Asked Questions page or Contact us.</p>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="model-btn">Visit our Model FAQ</button>
    </section>
</div>

<!-- popup -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">


            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always" allow="autoplay"></iframe>
                </div>


            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        // Gets the video src from the data-src on each button

        var $videoSrc;
        $('.video-btn').click(function() {
            $videoSrc = $(this).data("src");
        });
        console.log($videoSrc);



        // when the modal is opened autoplay it
        $('#myModal').on('shown.bs.modal', function(e) {

            // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
            $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        });



        // stop playing the youtube video when I close the modal
        $('#myModal').on('hide.bs.modal', function(e) {
            // a poor man's stop video
            $("#video").attr('src', $videoSrc);
        });

        // Handler for .

        $('html, body').animate({
            scrollTop: $('#become_model').offset().top
        }, 'slow');

        // document ready
    });
</script>