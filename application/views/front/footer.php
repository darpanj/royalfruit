<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto">
                <ul class="footer-list text-center m-0">
                    <li><a href="<?= site_url('home/usc') ?>">18 U.S.C 2257</a></li>
                    <li><a href="<?= site_url('home/copyright') ?>">Copyright</a></li>
                    <li><a href="<?= site_url('home/privacypolicy') ?>">Privacypolicy</a></li>
                    <li><a href="<?= site_url('home/california_privacy_policy') ?>">California Privacypolicy</a></li>
                    <li><a href="<?= site_url('home/terms_and_conditions') ?>">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>