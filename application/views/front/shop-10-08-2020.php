<div class="site-shop site-custom site-shop-detail">
    <section class="site-blocks-cover overflow-hidden">
        <img src="<?php echo base_url('themes/front/images') . '/slider2.png'; ?>">
    </section>

    <!-- <section class="site-blocks-cover overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 align-self-center">
                    <div class="row">

                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <div class="cus-nav">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="cus-feed2 d-flex justify-content-between">
                        <div class="position-relative">
                            <form action="<?php base_url('shop'); ?>" method="post" id="frmSearch">
                                <input type="search" name="search" value="<?php echo $search; ?>">
                                <i class="fa fa-search cart-srh" aria-hidden="true"></i>
                            </form>
                        </div>
                        <ul class="cart-ul">
                            <!--<li><a href="">Cart&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url() . FRONT_IMG; ?>cart.png" width="15"></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="detail-page">
        <div class="container">
            <div class="row">
                <?php
                if (!empty($shop)) {
                    foreach ($shop as $key => $value) {
                        ?>
                        <div class="col-md-6 col-sm-12 col-lg-3 det-top">
                            <div class="shop-box">
                                <span>Sale</span>
                                <img src="<?php echo checkImage(8, $value['image'], 0, 100, FALSE); ?>">
                                <p><?php echo $value['name']; ?></p>

                                <div class="shop-price">
                                    <h2>$<?php echo $value['price']; ?></h2>
                                    <!--<img src="<?php echo base_url() . FRONT_IMG; ?>add-cart.png" width="20">-->
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    echo '<div class="col-md-12 col-sm-12 col-lg-12 det-top"><p>No data found</p></div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(".cart-srh").click(function () {
        $("#frmSearch")[0].submit();
    });
</script>