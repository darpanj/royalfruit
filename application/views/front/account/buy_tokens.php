<!-- buy-token -->
<form id="frm_tokens" action="<?php echo base_url('account/purchase_tokens'); ?>" method="post">
    <section class="site-token">
        <div class="container token-main">
            <h2>Buy Tokens</h2>
            <p>Buy tokens to watch a model livestream. Tokens can only be used for Livestream.</p>
            <input type="hidden" name="token_id" id="token_id">
            <input type="hidden" name="token_qty" id="token_qty">
            <input type="hidden" name="token_price" id="token_price">
            <div class="row justify-content-center mt-3" id="myDIV">
                <?php $i = 0;
                foreach ($tokens as $key => $value) { ?>
                    <?php ($i == 2) ? $i = 0 : $i = $i; ?>
                    <?php if ($i == 0) { ?>
                        <div class="d-flex w-100 justify-content-center">
                        <?php } ?>
                        <div class="">
                            <a href="javascript:" class="token_class">
                                <div class="token-cnt d-flex  btn1" id="<?php echo $value['id']; ?>">
                                    <img src="<?php echo base_url() . FRONT_IMG; ?>token.png" alt="token">
                                    <p><?php echo $value['token_qty'] . ' tokens for ' . $value['token_price']; ?> </p>
                                    <input type="hidden" class="token_main_price" value="<?php echo $value['token_price']; ?>" ?>
                                    <input type="hidden" class="token_main_qty" value="<?php echo $value['token_qty']; ?>" ?>
                                </div>
                            </a>
                        </div>

                        <?php if ($i == 1) { ?>
                        </div>
                    <?php } ?>
                    <?php $i++; ?>
                <?php } ?>


                <div class="d-flex w-100 justify-content-center">
                    <div class="">
                        <button type="button" class="token-btn">Cancel</button>
                    </div>
                    <div class="">
                        <button type="submit" class="token-btn2" id="submit token">Continue</button>
                    </div>
                </div>

            </div>
    </section>
</form>
<script>
    $(document).ready(function() {
        $(document).on('click', '.token_class', function() {
            $('.token-cnt').removeClass('active');
            $(this).find('.token-cnt').addClass('active');
            let tokenId = $(this).find('.token-cnt').attr('id');
            let token_price = $(this).find('.token_main_price').val();
            let token_qty = $(this).find('.token_main_qty').val();
            $("#token_id").val(tokenId);
            $("#token_price").val(token_price);
            $("#token_qty").val(token_qty);
        });
    });
</script>