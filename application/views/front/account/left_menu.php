<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true" data-img="theme-assets/<?php echo base_url() . FRONT_IMG; ?>backgrounds/02.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="your_feed.html">
                    <h3 class="brand-text">THE ROYAL FRUIT</h3>
                </a></li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content ">
        <h2><?php echo ucfirst(check_variable_value($user_detail['name'])); ?></h2>
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="<?php echo ($this->uri->segment(2) == 'dashboard') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/dashboard'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>feed.png" alt="feed"><span class="menu-title" data-i18n="">Your Feed</span></a>
            </li>
            <!-- <div class="border mt-1 mb-1">
            </div> -->
            <li class="<?php echo ($this->uri->segment(2) == '') ? 'active' : ''; ?> nav-item"><a href="#" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>private-video.png" alt="private-video"><span class="menu-title" data-i18n="">Private Video Message</span></a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'premium_videos') ? 'active' : ''; ?> nav-item">
                <a href="<?php echo base_url('account/premium_videos'); ?>" class="nav-a">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>primium-video.png" alt="primium-video">
                    <span class="menu-title" data-i18n="">Premium Video</span>
                    <span class="badge pull-right"> <?php echo $counts['total_premium_videos']; ?> </span>
                </a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'custom_videos') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/custom_videos'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>custom-video.png" alt="custom-video"><span class="menu-title" data-i18n="">Custom Video</span></a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'premium_snap') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/premium_snap'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>primium-snap.png" alt="premium-snap"><span class="menu-title" data-i18n="">Premium Snap</span></a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'fav') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/fav'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>favourite.png" alt="favourite">
                    <span class="menu-title" data-i18n="">Favorites</span>
                    <span class="badge pull-right"> <?php echo $counts['total_favorites']; ?> </span>
                </a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'follows') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/follows'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>folows.png" alt="Follows"><span class="menu-title" data-i18n="">Follows</span></a>
            </li>
            <!-- <div class="border mt-1 mb-1">
            </div> -->
            <li class="<?php echo ($this->uri->segment(2) == 'message') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/message'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>message2.png" alt="message"><span class="menu-title" data-i18n="">Message</span></a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'notification') ? 'active' : ''; ?> nav-item">
                <a href="<?php echo base_url('account/notification'); ?>" class="nav-a">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>notification2.png" alt="notification">
                    <span class="menu-title" data-i18n="">Notifiaction</span>
                    <span class="badge pull-right"> <?php echo $counts['total_notification']; ?> </span>
                </a>
            </li>
            <!-- <div class="border mt-1 mb-1">
            </div> -->
            <li class="<?php echo ($this->uri->segment(2) == 'settings') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/settings'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>setting.png" alt="Setting"><span class="menu-title" data-i18n="">Setting</span></a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'password') ? 'active' : ''; ?> nav-item"><a href="<?php echo base_url('account/password'); ?>" class="nav-a"><img src="<?php echo base_url() . FRONT_IMG; ?>password.png" alt="Password"><span class="menu-title" data-i18n="">Password</span></a>
            </li>
        </ul>
    </div>
    <!-- <div class="navigation-background"></div> -->
</div>