<section class="feed-video">
    <div class="container">
        <div class="row">
            <?php foreach ($video_list as $key => $value) { ?>
                <div class="col-md-12 col-sm-12 col-lg-4 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay" data-toggle="modal" data-target="#smallModal">
                            <video class="mmCardVid" controls>
                                <source src="<?php echo base_url('themes/uploads/models') . '/' . $value['image'] ?>" type="video/mp4" autoplay loop>
                                secure connection could not be established
                            </video>

                            <i class="fa fa-play-circle-o play-btn" aria-hidden="true"></i>
                        </div>

                        <div class="d-flex justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo  checkImage(3, $value['profile_image']) ?>" width="30" height="30" alt="model">

                                <div class="pro-cnt">
                                    <h2><?php echo $value['name']; ?></h2>
                                </div>
                            </div>
                            <!-- <button type="button" class="site-folow">Request</button> -->
                            <a href="<?php echo base_url('account/detail_request_video') . '/' . $value['id']; ?>">
                                <button type="button" class="site-folow">Request</button>
                            </a>
                        </div>
                        <div class="d-flex sug-prs justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <p>Price starts at $<?php echo $value['post_price']; ?></p>
                            <span>00:20</span>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</section>

<!-- <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <img src="<?php echo base_url() . FRONT_IMG; ?>pro3.png" width="50" alt="model">
                <h2>Follos emily nguyen</h2>
                <h2>$10/month</h2>
                <p>You'll get access to daily private content, direct messaging, and more!</p>
                <button type="button" class="site-folow flw2">Become Model Now</button>
                <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div> -->