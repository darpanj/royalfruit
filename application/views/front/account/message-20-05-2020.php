        <!-- password -->
        <section class="site-pass">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 login-form">
                        <div class="messaging">
                            <div class="inbox_msg">
                                <div class="inbox_people">
                                    <div class="headind_srch">
                                        <div class="srch_bar">
                                            <input type="search" name="search_model" id="search_model">
                                            <i class="fa fa-search text-white chat-srh" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="inbox_chat">
                                        <?php if(!empty($chatUsers)) { ?>
                                            <?php 
                                                foreach($chatUsers as $kCU => $vCU) { 
                                                    $date = date("d M", strtotime($vCU['created_date']));
                                            ?>
                                            <div class="chat_list active_chat" onclick="getChat(<?= $vCU['mf_id'] ?>)">
                                                <div class="chat_people">
                                                    <div class="chat_img"> <img src="<?php echo checkImage(2, $vCU['profile_image']); ?>" alt="<?= $vCU['model_name'] ?>"> </div>
                                                    <div class="chat_ib">
                                                        <h5><?= $vCU['model_name'] ?> <span class="chat_date"><?= $date ?></span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <!-- <div class="chat_list active_chat">
                                            <div class="chat_people">
                                                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                                <div class="chat_ib">
                                                    <h5>user one <span class="chat_date">25 Dec</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chat_list active_chat">
                                            <div class="chat_people">
                                                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                                <div class="chat_ib">
                                                    <h5>user one <span class="chat_date">25 Dec</span></h5>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="mesgs chatBlock">
                                    <div class="msg_history">
                                        <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <p></p>
                                                    <span class="time_date"> 11:01 AM    |    June 9</span></div>
                                            </div>
                                        </div>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <p></p>
                                                <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                                        </div>
                                        <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <p></p>
                                                    <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                                            </div>
                                        </div>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <p></p>
                                                <span class="time_date"> 11:01 AM    |    Today</span> </div>
                                        </div>
                                        <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <p></p>
                                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                                            </div>
                                        </div>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <p></p>
                                                <span class="time_date"> 11:01 AM    |    Today</span> </div>
                                        </div>
                                        <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <p></p>
                                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                                            </div>
                                        </div>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <p></p>
                                                <span class="time_date"> 11:01 AM    |    Today</span> </div>
                                        </div>

                                    </div>
                                    <div class="type_msg">
                                        <div class="input_msg_write">
                                            <input type="text" class="write_msg" placeholder="Type a message" />
                                            <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


<script>

var searchRequest = null;

$(function () {
    $("#search_model").keyup(function () {
        var that = this,
        value = $(this).val();
            searchRequest = $.ajax({
                type: "GET",
                url: "<?= site_url('account/get_chat_user') ?>",
                data: {
                    'search_keyword' : value
                },
                dataType: "text",
                success: function(msg){
                    var res = JSON.parse(msg);
                    if(res.status === 200) {
                        if(res.html != '') {
                            $(".inbox_chat").empty();
                            $(".inbox_chat").html(res.html);
                        }
                    }
                    if(res.status === 412) {
                        toastr.error(res.message, 'Error');
                    }
                }
            });
    });
});

function getChat(mf_id) {
  if(mf_id === '') {
    alert('Something went wrong. Please try again later.');
    return false;
  }
    $.ajax({
        type: "POST",
        url: "<?= site_url('account/get_chat') ?>",
        data: {
            'model_follower_id' : mf_id
        },
        dataType: "text",
        success: function(msg){
            var res = JSON.parse(msg);
            if(res.status === 200) {
                if(res.html != '') {
                    $(".chatBlock").empty();
                    $(".chatBlock").html(res.html);
                    $('.chatBlock').scrollTop($('.chatBlock')[0].scrollHeight);
                }
            }
            if(res.status === 412) {
                toastr.error(res.message, 'Error');
            }
        }
    });
}
</script>