<div class="site-setting">
    <div class="site-custom-video site-notification">
        <!-- password -->
        <section class="site-noti">
            <div class="container">

                <h2>Notification</h2>

                <label class="control control--checkbox mt-2">Check All
                    <input type="checkbox" id="all_check" />
                    <span class="control__indicator"></span>
                </label>
                <div class="cus-select pre-cus-lb">
                    <label>
                        <form name="frm_notification" id="frm_notification">
                            <input type="hidden" name="notification_ids" id="notification_ids">
                            <select name="action" id="action_notification">
                                <option value="0">With Selected</option>
                                <option value="1">Mark as Read</option>
                                <option value="2">Delete</option>
                            </select>
                        </form>
                    </label>
                </div>
                <!-- <select class="d-block position-relative w-50 mt-1">
        <option>With Selected</option>
        <option>Mark as read</option>
        <option>Delete</option>
      </select> -->
                <input type="button" class="btn btn-danger mt-2" name="delete_all" id="delete_all" value="Delete all">
                <input type="button" class="btn btn-primary ml-15 mt-2 ml-3" name="mark_all" id="mark_all" value="Mark all as read">

            </div>
        </section>

        <div class="container mt-4">
            <?php
            foreach ($notifications as $key => $value) {
            ?>
                <div class="alert alert-info ">
                    <div class="container">
                        <div class="d-flex justify-content-between">
                            <b><?php echo $value['notification_message']; ?></b>
                            <label class="control control--checkbox r-cntrol">
                                <input type="checkbox" class="chkbox" data-id="<?php echo $value['id']; ?>">
                                <span class="control__indicator r-cnt-ind"></span>
                            </label>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#notification_ids").val(0)
        // on all checkbox click event
        $(document).on('click', '#all_check', function() {
            $('.chkbox').not(this).prop('checked', this.checked);
            let e = 0;
            $(".chkbox").map(function() {
                if ($(this).prop('checked') == true) {
                    tmp = $(this).attr('data-id');
                    e += ',' + tmp;
                } else {
                    tmp = $(this).attr('data-id');
                    e = 0;
                }
            });;
            $("#notification_ids").val(e);

        });

        //on single checkbox click event
        $(document).on('click', '.chkbox', function() {
            let e = $("#notification_ids").val()
            let tmp = $(this).attr('data-id');
            if ($(this).prop('checked') == true) {
                e += ',' + tmp;
            } else {
                e = removeValue(e, tmp);
            }
            $("#notification_ids").val(e);
        });

        //on select change event
        $(document).on('change', '#action_notification', function() {
            let action = $(this).val();
            if (action != 0) {
                let notificationids = $("#notification_ids").val();
                fire_event(notificationids, action);
            }
        });

        //mark all as read clicked event
        $(document).on('click', '#mark_all', function() {
            let action = 3;
            let notificationids = $("#notification_ids").val();
            fire_event(notificationids, action);
        });

        //delete all clicked event
        $(document).on('click', '#delete_all', function() {
            let action = 4;
            let notificationids = $("#notification_ids").val();
            fire_event(notificationids, action);
        });

    });

    //remove comma seprated values
    function removeValue(list, value) {
        return list.replace(new RegExp(",?" + value + ",?"), function(match) {
            var first_comma = match.charAt(0) === ',',
                second_comma;

            if (first_comma &&
                (second_comma = match.charAt(match.length - 1) === ',')) {
                return ',';
            }
            return '';
        });
    };

    //trigger actual event
    function fire_event(notificationids, action) {
        $.ajax({
            url: "<?php echo base_url('account/change_notification_action'); ?>",
            method: 'post',
            dataType: 'Json',
            data: {
                'notification_ids': notificationids,
                'action': action
            },
            success: function(r) {
                sText = r.message;
                sType = r.type;

                swal({
                    title: sType,
                    text: sText,
                    icon: sType,
                }).then(function() {
                    location.reload();
                });
            },
            error: function() {
                console.log('ajax  error');
            }
        });
    }
</script>