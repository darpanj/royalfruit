<!-- password -->
<div class="site-setting">
    <section class="site-pass">
        <div class="container">
            <div class="row">
                <div class="col-md-12 login-form">
                    <form enctype="multipart/form-data" action="<?php echo base_url('account/update_settings');?>" method="post">
                        <div class="circle">
                            <!-- User Profile Image -->
                            <img class="profile-pic" src="<?php echo checkImage(1,$user->profile_image) ?>" alt="model">
                            <div class="p-image">
                                <input class="file-upload" type="file" accept="image/*" name="profile_image"/>
                                <i class="fa fa-pencil upload-button" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control3" name="name" placeholder="<?php echo $this->lang->line('placeholder_name'); ?>" data-parsley-required="true" data-parsley-required-message="<?php echo $this->lang->line('err_name_req'); ?>" data-parsley-maxlenth="20" value="<?php echo check_variable_value($user->name); ?>">
                        </div>
                        <div class="form-group mb-1">
                            <textarea class="form-control3" name="bio" placeholder="<?php echo $this->lang->line('placeholder_bio'); ?>"><?php echo check_variable_value($user->bio);?></textarea>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control3" placeholder="Email" value="<?php echo check_variable_value($user->email);?>" readonly="readonly">
                        </div>
                        <h2 class="pre-cnt text-black">Email Notifications</h2>
                        <label class="control control--checkbox">New Private Message
                            <input type="checkbox" name="private_message"/>
                            <span class="control__indicator"></span>
                        </label>
                        <div class="form-group ">
                            <input type="email" class="form-control3" placeholder="Snapchat Username">
                        </div>
                        <button type="submit">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.profile-pic').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function () {
            readURL(this);
        });
        $(".upload-button").on('click', function () {
            $(".file-upload").click();
        });
    });
</script>