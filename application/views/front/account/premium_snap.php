<div class="app-content content site-prm-snap">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Chart -->
        </div>

        <!-- password -->
        <section class="pre-snap">
            <div class="container snap-main">
                <h2>Subscription</h2>
                <div class="snap-main">

                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th>Model</th>
                            <th>Subcription Start Date</th>
                            <th>Subcription End Date</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Joya</td>
                            <td>21-jan-2020</td>
                            <td>21-jan-2021</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>Joya</td>
                            <td>21-jan-2020</td>
                            <td>21-jan-2021</td>
                            <td>0</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <div class="pro-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex justify-content-between pro-fmain">
                            <ul>
                                <li><a href=""><img src="images/money2.png" alt="money"></a></li>
                                <li><a href=""><img src="images/message2.png" alt="message"></a></li>
                                <li><a href=""><img src="images/notification2.png" alt="notification"></a></li>
                                <li class="dropdown dropdown-user nav-item my-none"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online"><img src="images/avatar-s-19.png" alt="avatar"></span></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <div class="arrow_box_right"><a class="dropdown-item" href="#">

                                                <span class="avatar avatar-online"><img src="images/avatar-s-19.png" alt="avatar"><span class="user-name text-bold-700 ml-1">John Doe</span></span>

                                            </a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#"> Edit Profile</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#"> Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <!-- end -->


    </div>
</div>