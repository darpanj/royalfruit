<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- password -->
        <section class="site-pass">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 login-form">
                        <div class="messaging">
                            <div class="inbox_msg">
                                <div class="inbox_people">
                                    <div class="headind_srch">
                                        <div class="srch_bar">
                                            <input type="search" name="search_model" id="search_model">
                                            <i class="fa fa-search text-white chat-srh" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="inbox_chat">
                                        <?php if(!empty($chatUsers)) { ?>
                                            <?php 
                                                foreach($chatUsers as $kCU => $vCU) { 
                                                    $date = date("d M", strtotime($vCU['created_date']));
                                            ?>
                                            <div class="chat_list active_chat" onclick="getChat(<?= $vCU['mf_id'] ?>)" id="<?= $vCU['mf_id'] ?>">
                                                <div class="chat_people">
                                                    <div class="chat_img"> <img src="<?php echo checkImage(2, $vCU['profile_image']); ?>" alt="<?= $vCU['model_name'] ?>"> </div>
                                                    <div class="chat_ib">
                                                        <h5><?= $vCU['model_name'] ?> <span class="chat_date"><?= $date ?></span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="mesgs chatBlock">
                                    <?php if(!empty($chatMessages)) { ?>
                                    <div class="msg_history">
                                        <input type="hidden" name="model_follower_id" id="model_follower_id" value="<?= $model_follower_id ?>">
                                        <?php 
                                            foreach($chatMessages as $kCM => $vCM) {
                                            $userId = !empty($this->user_id) ? $this->user_id : 0;
                                            $image = checkImage(2, $vCM['profile_image']);
                                            $date = date("M d", strtotime($vCM['created_at']));
                                            $time = date("h:i A", strtotime($vCM['created_at']));
                                        ?>
                                        <?php if($vCM['sender_id'] == $userId) { ?>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <p><?= $vCM['message_text'] ?></p>
                                                <span class="time_date"> <?= $time ?>    |    <?= $date ?></span> </div>
                                        </div>
                                        <?php } else { ?>
                                        <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="<?= $image ?>" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <p><?= $vCM['message_text'] ?></p>
                                                    <span class="time_date"> <?= $time ?>    |    <?= $date ?></span></div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <div class="type_msg">
                                        <form class="sendMessage" id="sendMessage" name="sendMessage" method="post" action="<?= site_url('account/send_message') ?>">
                                        <div class="input_msg_write">
                                            <input type="text" class="write_msg" name="message_text" id="message_text" placeholder="Type a message" />
                                            <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hsubmitidden="true"></i></button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<script type="text/javascript">
var interval = '';
var intervalChatUser = '';
$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);

var searchRequest = null;

$(function () {
    $("#search_model").keyup(function () {
        clearInterval(intervalChatUser);
        var that = this,
        value = $(this).val();
        if(value === '') {
            intervalChatUser = setInterval(function() {
               ajaxGetChatUser();
            }, 5000);
        }
            searchRequest = $.ajax({
                type: "GET",
                url: "<?= site_url('account/get_chat_user') ?>",
                data: {
                    'search_keyword' : value
                },
                dataType: "text",
                success: function(msg){
                    var res = JSON.parse(msg);
                    if(res.status === 200) {
                        if(res.html != '') {
                            $(".inbox_chat").empty();
                            $(".inbox_chat").html(res.html);
                        }
                    }
                    if(res.status === 412) {
                        //toastr.error(res.message, 'Error');
                        $(".inbox_chat").empty();
                        $(".inbox_chat").html('<h3>No models found.</h3>');
                    }
                }
            });
    });
});

function ajaxGetChatUser() {
    $.ajax({
        type: "GET",
        url: "<?= site_url('account/get_chat_user') ?>",
        /*data: {
            'search_keyword' : value
        },
        dataType: "text",*/
        success: function(msg){
            var res = JSON.parse(msg);
            if(res.status === 200) {
                if(res.html != '') {
                    $(".inbox_chat").empty();
                    $(".inbox_chat").html(res.html);
                }
            }
            if(res.status === 412) {
                //toastr.error(res.message, 'Error');
                $(".inbox_chat").empty();
                $(".inbox_chat").html('<h3>No models found.</h3>');
            }
        }
    });
}

$(document).ready(function(){
    intervalChatUser = setInterval(function() {
       ajaxGetChatUser();
    }, 5000);
});

function getChat(mf_id) {
  if(mf_id === '') {
    alert('Something went wrong. Please try again later.');
    return false;
  }
    $.ajax({
        type: "POST",
        url: "<?= site_url('account/get_chat') ?>",
        data: {
            'model_follower_id' : mf_id
        },
        dataType: "text",
        success: function(msg){
            var res = JSON.parse(msg);
            if(res.status === 200) {
                if(res.html != '') {
                    $(".msg_history").empty();
                    $(".msg_history").html(res.html);
                    $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);
                }
            }
            if(res.status === 412) {
                sType = getStatusText(412);
                sText = res.message;
                swal({
                    title: "Error!",
                    text: sText,
                    icon: "error",
                });
            }
        }
    });
}

$(document).on('click','.active_chat',function(){
    clearInterval(interval);
    let mf_id = $( this ).attr("id");
    if(mf_id === "") {
        return false;
    }
    interval = setInterval(function() {
       getChat(mf_id);
    }, 5000);
});


$(document).on('submit','.sendMessage',function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form.
    let message_text = $("#message_text").val();
    if(message_text === '') {
        return false;
    }
    let model_follower_id = $("#model_follower_id").val();
    var form = $(this);
    var url = form.attr('action');
    $('.msg_send_btn').attr('disabled', true);
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize() + "&model_follower_id=" + model_follower_id, // serializes the form's elements.
           success: function(data)
           {
                var res = JSON.parse(data);
                if(res.status === 200) {
                    if(res.html != '') {
                        $("#message_text").val("");
                        $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);
                    }
                }
                if(res.status === 412) {
                    sType = getStatusText(412);
                    sText = res.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                $('.msg_send_btn').attr('disabled', false);
           }
         });
});
</script>