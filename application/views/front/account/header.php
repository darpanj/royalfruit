<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
    <div class="navbar-wrapper">
        <h1 class="mb-0"><a href="<?php echo base_url('home'); ?>" class="h2 mb-0">THE ROYAL FRUIT </a></h1>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse show" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">

                    <?php if ($user_id > 0) { ?>
                        <li class="nav-item  feed-nav "><a class="nav-link nav-link-expand" href="<?php echo base_url('account/dashboard'); ?>">Your Feed</a></li>
                    <?php } ?>
                    <li class="nav-item dropdown navbar-search feed-nav"><a class="nav-link" href="<?php echo base_url('home'); ?>">Model</a></li>
                    <li class="nav-item dropdown navbar-search feed-nav"><a class="nav-link" href="premium_video.html">Premium Video</a></li>
                    <li class="nav-item dropdown navbar-search feed-nav"><a class="nav-link" href="snap_model.html">Snap Model</a></li>
                    <li class="nav-item dropdown navbar-search feed-nav"><a class="nav-link" href="<?php echo base_url('home/explore') ?>">Explore</a></li>
                    <li class="nav-item dropdown navbar-search feed-nav"><a class="nav-link" href="<?php echo base_url('home/live') ?>">LIVEs</a></li>
                </ul>


                <ul class="nav navbar-nav float-right">
                    <?php if ($user_id > 0) { ?>
                        <li class="my-none"><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>money.png" alt="money"></a></li>
                        <li class="my-none"><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>message.png" alt="message"></a></li>
                        <li class="my-none"><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>notification.png" alt="notification"></a></li>


                        <li class="dropdown dropdown-user nav-item my-none"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online"><img src="<?php echo checkImage(1, $user_detail['profile_image']); ?>" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right"><a class="dropdown-item" href="#">

                                        <span class="avatar avatar-online"><img src="<?php echo checkImage(1, $user_detail['profile_image']); ?>" alt="avatar"><span class="user-name text-bold-700 ml-1"><?php echo $user_detail['name']; ?></span></span>

                                    </a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="#"> Edit Profile</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url('home/logout'); ?>"> Logout</a>
                                </div>
                            </div>
                        </li>
                    <?php } else { ?>
                        <li class="my-none"><a href="<?php echo base_url('home/register'); ?>">Join Now</a></li>
                        <li class="my-none"><a href="<?php echo base_url('home/login') ?>">Login</a></li>
                        <li class="my-none"><a href="<?php echo base_url('shop') ?>">Shop</a></li>
                    <?php } ?>
                    <li class="d-xl-none d-md-block">
                        <div id="toggle" data-activates="mobile-demo" class="button-collapse show-on-large">
                            <div class="one"></div>
                            <div class="two"></div>
                            <div class="three"></div>
                        </div>

                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>
<nav class="g1">
    <div class="nav-wrapper">
        <ul class="side-nav grey darken-3 my" id="mobile-demo">
            <li class="white"><a href="#"><i class="fa fa-times close-btn waves-effect waves-blue" aria-hidden="true"></i></a></li>

            <li class="white"><a href="<?php echo base_url('home'); ?>" class="waves-effect waves-blue">Model</a></li>
            <li class="white"><a href="premium_video.html" class="waves-effect waves-blue">Premium Video</a></li>
            <li class="white"><a href="snap_model.html" class="waves-effect waves-blue">Snap Model</a></li>
            <li class="white"><a href="<?php echo base_url('home/explore') ?>" class="waves-effect waves-blue">Explore</a></li>
            <li class="white"><a href="live_model.html" class="waves-effect waves-blue">LIVEs</a></li>

            <!--  <ul class="w-none"> -->
            <li class="active w-none"><a href="<?php echo base_url('account/dashboard'); ?>" class="waves-effect waves-blue">Your Feed</a></li>

            <li class="white w-none"><a href="premium_video.html" class="waves-effect waves-blue">Premium Video</a></li>
            <li class="white w-none"><a href="custom_video.html" class="waves-effect waves-blue">Custom Video</a></li>
            <li class="white w-none"><a href="premium_snap.html" class="waves-effect waves-blue">Premium Snap</a></li>
            <li class="white w-none"><a href="" class="waves-effect waves-blue">Favorites</a></li>
            <li class="white w-none"><a href="" class="waves-effect waves-blue">Follows</a></li>
            <li class="white w-none"><a href="<?php echo base_url('account/message'); ?>" class="waves-effect waves-blue">Message</a></li>
            <li class="white w-none"><a href="" class="waves-effect waves-blue">Notification</a></li>
            <li class="white w-none"><a href="<?php echo base_url('account/settings') ?>" class="waves-effect waves-blue">Setting</a></li>
            <li class="white w-none"><a href="<?php echo base_url('account/password'); ?>" class="waves-effect waves-blue">Password</a></li>
            <li class="white w-none"><a href="<?php echo base_url('home/logout'); ?>" class="waves-effect waves-blue">Logout</a></li>
            <!-- </ul> -->
        </ul>

    </div>
</nav>