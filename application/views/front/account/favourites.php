<!-- <section class="feed-video">
    <div class="container">
        <input type="hidden" id="postIds" value="0">
        <div class="row" id="post_list">

        </div>
    </div>
</section> -->
<div class=" site-setting site-custom-video site-notification">
    <input type="hidden" id="postIds" value="0">
    <section class="site-favourite">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex position-relative fav-main justify-content-between">
                        <h2><i class="fa fa-star"></i> Favorites</h2>
                        <ul class="nav nav-tabs nav4" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link2 active" id="grid-tab" data-toggle="tab" href="#grid" role="tab" aria-controls="grid" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus1.png" alt="royalfruit"></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link2" id="full-tab" data-toggle="tab" href="#full" role="tab" aria-controls="full" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus2.png" alt="royalfruit"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- tab-main-->
    <div class="tab-content">

        <!-- grid-view -->
        <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="grid-tab">
            <div class="row snap-model2 m-0" id="post_list">
                <!-- <div class="col-md-6 col-sm-6 col-lg-6 col-12 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                        </div>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-12 mt-2">
                    <div class="feed-main">
                        <video class="mmCardVid" controls>
                            <source src="video/video1.mp4" type="video/mp4">
                            secure connection could not be established
                        </video>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-12 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                        </div>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-12 mt-2">
                    <div class="feed-main">
                        <video class="mmCardVid" controls>
                            <source src="video/video1.mp4" type="video/mp4">
                            secure connection could not be established
                        </video>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
        <!-- grid end -->


        <!-- list-view -->
        <div class="tab-pane fade" id="full" role="tabpanel" aria-labelledby="full-tab">
            <div class="row snap-model2" id="post_list2">
                <!-- <div class="col-md-12 col-sm-12 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                        </div>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                        </div>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 mt-2">
                    <div class="feed-main">
                        <video class="mmCardVid" controls>
                            <source src="video/video1.mp4" type="video/mp4">
                            secure connection could not be established
                        </video>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                </div>
                            </div>
                            <button type="button" class="site-folow" data-toggle="modal" data-target="#smallModal">Follow</button>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>

        <!--list end  -->



    </div>
</div>


<script>
    $(document).ready(function() {
        /* for load modal first times */
        let limit = 9;
        let target = $("#post_list");
        let target2 = $("#post_list2");
        let postIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'post_ids': postIds
            },
            dataType: "json",
            url: "<?php echo base_url('account/fav_posts');  ?>",
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(r) {
                let html = '';
                let html2 = '';
                $.each(r.feed_models, function(key, value) {
                    if (value.post_type == 0) {
                        html += make_image_post(value);
                        html2 += make_image_post2(value);
                    } else if (value.post_type == 1) {
                        html += make_video_post(value);
                        html2 += make_video_post2(value);
                    }
                    postIds += ',' + value.post_id;
                });

                $(target).html(html);
                $(target2).html(html2);
                $("#loader").hide();
                $('#postIds').val(postIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });
        /* when scroll than load more modals */
        $(window).scroll(function() {
            let postIds1 = $('#postIds').val();
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                let postIds = '0';
                $("#loader").show();
                let final_data = {
                    'limit': limit,
                    'post_ids': postIds1
                };
                $.ajax({
                    type: 'post',
                    data: final_data,
                    dataType: "json",
                    url: "<?php echo base_url('account/load_posts');  ?>",
                    beforeSend: function() {
                        $("#loader").show();
                    },
                    success: function(r) {
                        var html = '';
                        var html2 = '';
                        $.each(r.feed_models, function(key, value) {
                            if (value.post_type == 0) {
                                html += make_image_post(value);
                                html2 += make_image_post2(value);
                            } else if (value.post_type == 1) {
                                html += make_video_post(value);
                                html2 += make_video_post2(value);
                            }
                            postIds += ',' + value.post_id;
                        });
                        $(target).append(html);
                        $(target2).append(html2);
                        $('#postIds').val(postIds1 + ',' + postIds);
                        $("#loader").hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });
    });

    function make_image_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        var html = '';
        html += ` <div class="col-md-6 col-sm-6 col-lg-6 col-12 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay">
                            <img src="${SITE_IMG}uploads/models/${value.post_image}"  alt="model">
                        </div>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">`;

        html += `            </div>     
                        </div>
                    </div>
                </div>`;


        // html += `<div class='col-md-12 col-sm-12 col-lg-4 mt-2'>`;
        // html += "<div class='feed-main'>";
        // html += "<div class='position-relative overlay follow'>";
        // html += `<img src="${SITE_IMG}uploads/models/${value.post_image}" alt='model'>`;
        // html += "</div>";

        // html += "<div class='d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1'>";
        // html += "<div class='d-flex align-items-center'>";
        // html += `<img src="${SITE_IMG}uploads/models/${value.profile_image}"  alt='model' width='30' height='30'>`;
        // html += "<div class='pro-cnt'>";
        // html += `<h2>${value.name}</h2>`;
        // html += `<p class='m-0'>${teaser_date}</p>`;
        // html += "</div>";
        // html += "</div>";
        // html += `<ul id="category-tabs">
        //     <li>`;
        // if (value.is_fav_post == 1) {
        //     html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star"></i>
        //     </a>`;
        // } else {
        //     html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star-o"></i>
        //     </a>`;
        // }
        // html += `</a>
        //         </li>
        //     </ul>`;
        // html += `<button type='button' class='site-folow follow' data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow`;
        // html += "</button>";
        // html += "</div>";
        html += "</div>";
        html += "</div>";
        return html;
    }

    function make_video_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        let html = `<div class="col-md-6 col-sm-6 col-lg-6 col-12 mt-2">
                  <div class="feed-main">
                    <video class="mmCardVid" controls>
                      <source src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4">
                      secure connection could not be established
                    </video>
                    <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                      <div class="d-flex align-items-center">`;
        //     <div class="pro-cnt">
        //       <h2>Joya</h2>
        //       <p class="m-0">${teaser_date}</p>
        //     </div>
        //   </div>
        //   <button type="button" class="site-folow" data-toggle="modal"
        //     data-target="#smallModal">Follow</button>
        html += ` </div>
                  </div>
                </div>`;

        //     let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
        // <div class="feed-main">
        //     <video class="mmCardVid" controls>
        //         <source src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4" autoplay loop>
        //         secure connection could not be established
        //     </video>
        //     </div>
        //     </div>`;

        //         <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
        //             <div class="d-flex align-items-center">
        //                 <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">
        //                 <div class="pro-cnt">
        //                     <h2>${value.name}</h2>
        //                     <?php
                                //                     // $dateDisplay = get_date_in_format(`${value.teaser_created_date}`);
                                //                     
                                ?>
        //                     <p class="m-0">${teaser_date}</p>
        //                 </div>
        //             </div>
        //             <ul id="category-tabs">
        //             <li>`;
        //         if (value.is_fav_post == 1) {
        //             html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
        //             <i class="fa fa-star"></i>
        //             </a>`;
        //         } else {
        //             html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
        //             <i class="fa fa-star-o"></i>
        //             </a>`;
        //         }
        //         html += `</a></li>
        //             </ul>
        //             <button type="button" class="site-folow follow" data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow</button>
        //         </div>
        //     </div>
        // </div>`;
        return html;
    }

    function make_image_post2(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        var html = '';
        html += ` <div class="col-md-12 col-sm-12 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay">
                            <img src="${SITE_IMG}uploads/models/${value.post_image}"  alt="model">
                        </div>
                        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                            <div class="d-flex align-items-center">`;

        html += `            </div>     
                        </div>
                    </div>
                </div>`;


        // html += `<div class='col-md-12 col-sm-12 col-lg-4 mt-2'>`;
        // html += "<div class='feed-main'>";
        // html += "<div class='position-relative overlay follow'>";
        // html += `<img src="${SITE_IMG}uploads/models/${value.post_image}" alt='model'>`;
        // html += "</div>";

        // html += "<div class='d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1'>";
        // html += "<div class='d-flex align-items-center'>";
        // html += `<img src="${SITE_IMG}uploads/models/${value.profile_image}"  alt='model' width='30' height='30'>`;
        // html += "<div class='pro-cnt'>";
        // html += `<h2>${value.name}</h2>`;
        // html += `<p class='m-0'>${teaser_date}</p>`;
        // html += "</div>";
        // html += "</div>";
        // html += `<ul id="category-tabs">
        //     <li>`;
        // if (value.is_fav_post == 1) {
        //     html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star"></i>
        //     </a>`;
        // } else {
        //     html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star-o"></i>
        //     </a>`;
        // }
        // html += `</a>
        //         </li>
        //     </ul>`;
        // html += `<button type='button' class='site-folow follow' data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow`;
        // html += "</button>";
        // html += "</div>";
        html += "</div>";
        html += "</div>";
        return html;
    }

    function make_video_post2(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        let html = `<div class="col-md-12 col-sm-12 mt-2">
                  <div class="feed-main">
                    <video class="mmCardVid" controls>
                      <source src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4">
                      secure connection could not be established
                    </video>
                    <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                      <div class="d-flex align-items-center">`;
        //     <div class="pro-cnt">
        //       <h2>Joya</h2>
        //       <p class="m-0">${teaser_date}</p>
        //     </div>
        //   </div>
        //   <button type="button" class="site-folow" data-toggle="modal"
        //     data-target="#smallModal">Follow</button>
        html += ` </div>
                  </div>
                </div>`;

        //     let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
        // <div class="feed-main">
        //     <video class="mmCardVid" controls>
        //         <source src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4" autoplay loop>
        //         secure connection could not be established
        //     </video>
        //     </div>
        //     </div>`;

        //         <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
        //             <div class="d-flex align-items-center">
        //                 <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">
        //                 <div class="pro-cnt">
        //                     <h2>${value.name}</h2>
        //                     <?php
                                //                     // $dateDisplay = get_date_in_format(`${value.teaser_created_date}`);
                                //                     
                                ?>
        //                     <p class="m-0">${teaser_date}</p>
        //                 </div>
        //             </div>
        //             <ul id="category-tabs">
        //             <li>`;
        //         if (value.is_fav_post == 1) {
        //             html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
        //             <i class="fa fa-star"></i>
        //             </a>`;
        //         } else {
        //             html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
        //             <i class="fa fa-star-o"></i>
        //             </a>`;
        //         }
        //         html += `</a></li>
        //             </ul>
        //             <button type="button" class="site-folow follow" data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow</button>
        //         </div>
        //     </div>
        // </div>`;
        return html;
    }
</script>