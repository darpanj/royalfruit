<section class="suggestion-feed mt-4">
    <div class="container">
        <h2 class="sug-h">Suggested Models For You</h2>
        <div class="row sug-main pro-cnt text-center mb-4" id="suggestion_models">
<!--            <div class="col-md-6 col-sm-6 col-lg-3 mb-2">-->
<!--                <div class="sug-mnt">-->
<!--                    <img src="--><?php //echo base_url() . FRONT_IMG; ?><!--pro3.png" class="my-sug" alt="model">-->
<!--                    <h2 class="mt-1 mb-1">Joya</h2>-->
<!--                    <div class="d-flex justify-content-between  position-relative">-->
<!--                        <div class="sug-cnt">-->
<!--                            <img src="--><?php //echo base_url() . FRONT_IMG; ?><!--icon1.png" alt="model" width="15">-->
<!--                            <span class="sug-span">68</span>-->
<!--                        </div>-->
<!--                        <div class="sug-cnt sug-cnt2">-->
<!--                            <img src="--><?php //echo base_url() . FRONT_IMG; ?><!--icon2.png" alt="model" width="15">-->
<!--                            <span class="sug-span">68</span>-->
<!--                        </div>-->
<!--                        <div class="sug-cnt">-->
<!--                            <img src="--><?php //echo base_url() . FRONT_IMG; ?><!--icon3.png" alt="model" width="15">-->
<!--                            <span>70</span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="d-flex fol-p">-->
<!--                        <img src="--><?php //echo base_url() . FRONT_IMG; ?><!--love2.png" alt="model" width="17"-->
<!--                             height="15">-->
<!--                        <p>Follow</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        var limit = 4;
        var target = $("#suggestion_models");
        $.ajax({
            type: 'post',
            data: {
                'limit': limit
            },
            dataType: "json",
            url: "<?php echo base_url('account/load_suggestion_models');  ?>",
            success: function (r) {
                var html = '';
                $.each(r.data, function (key, value) {
                    html += make_suggestion_model(value);
                });
                $(target).html(html);
            },
            error: function () {
                console.log('ajax error');
            }
        });
    });

    function make_suggestion_model(value) {
        var html = '';
        html += "<div class='col-md-6 col-sm-6 col-lg-3 mb-2'>";
        html += "<div class='sug-mnt'>";
        html += "<img src="+ value.profile_image + " class='my-sug' alt='model'>";
        html += "<h2 class='mt-1 mb-1'>"+value.name+"</h2>";
        html += "<div class='d-flex justify-content-between  position-relative'>";
        html += "<div class='sug-cnt'>";
        html += "<img src='<?php echo base_url() . FRONT_IMG; ?>icon1.png' alt='model' width='15'>";
        html += "<span class='sug-span'>"+value.total_images+"</span>";
        html += "</div>";
        html += "<div class='sug-cnt sug-cnt2'>";
        html += "<img src='<?php echo base_url() . FRONT_IMG; ?>icon2.png' alt='model' width='15'>";
        html += "<span class='sug-span'>"+value.total_videos+"</span>";
        html += "</div>";
        html += "<div class='sug-cnt'>";
        html += "<img src='<?php echo base_url() . FRONT_IMG; ?>icon3.png' alt='model' width='15'>";
        html += "<span>"+value.diamonds+"</span>";
        html += "</div>";
        html += "</div>";
        html += "<div class='d-flex fol-p'>";
        html += "<img src='<?php echo base_url() . FRONT_IMG; ?>love2.png' alt='model' width='17' height='15'>";
<<<<<<< HEAD
        html += "<button type = 'button' class='follow follow-suggestion_model sug-fol-btnr' data-toggle='modal' data-target='#smallModal' data-model-id='" + value.id + "'><?php echo $this->lang->line('lbl_follow'); ?></button>";
=======
        html += "<button type = 'button' class='follow follow-suggestion_model' data-toggle='modal' data-target='#smallModal' data-model-id='" + value.id + "'><?php echo $this->lang->line('lbl_follow'); ?></button>";
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
       // html += "<p><?php echo $this->lang->line('lbl_follow'); ?></p>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        return html;
    }
</script>