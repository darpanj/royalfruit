<section class="feed-video">
    <div class="container">
        <input type="hidden" id="postIds" value="0">
        <div class="row" id="post_list">

        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        /* for load modal first times */
        let limit = 9;
        let target = $("#post_list");
        let postIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'post_ids': postIds
            },
            dataType: "json",
<<<<<<< HEAD
            url: "<?php echo base_url('account/load_follows_posts');  ?>",
=======
            url: "<?php echo base_url('account/load_followers');  ?>",
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(r) {
                let html = '';
                $.each(r.feed_models, function(key, value) {
                    if (value.post_type == 0) {
                        html += make_image_post(value);
                    } else if (value.post_type == 1) {
                        html += make_video_post(value);
                    }
                    postIds += ',' + value.post_id;
                });

                $(target).html(html);
                $("#loader").hide();
                $('#postIds').val(postIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });
        /* when scroll than load more modals */
        $(window).scroll(function() {
            let postIds1 = $('#postIds').val();
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                let postIds = '0';
                $("#loader").show();
                let final_data = {
                    'limit': limit,
                    'post_ids': postIds1
                };
                $.ajax({
                    type: 'post',
                    data: final_data,
                    dataType: "json",
                    url: "<?php echo base_url('account/load_follows_posts');  ?>",
                    beforeSend: function() {
                        $("#loader").show();
                    },
                    success: function(r) {
                        var html = '';
                        $.each(r.feed_models, function(key, value) {
                            if (value.post_type == 0) {
                                html += make_image_post(value);
                            } else if (value.post_type == 1) {
                                html += make_video_post(value);
                            }
                            postIds += ',' + value.post_id;
                        });
                        $(target).append(html);
                        $('#postIds').val(postIds1 + ',' + postIds);
                        $("#loader").hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });
    });

    function make_image_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        var html = '';
        html += `<div class='col-md-12 col-sm-12 col-lg-4 mt-2'>`;
        html += "<div class='feed-main'>";
        html += "<div class='position-relative overlay follow'>";
        html += `<img src="${SITE_IMG}uploads/models/${value.post_image}" alt='model'>`;
        html += "</div>";
        html += "<div class='d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1'>";
        html += "<div class='d-flex align-items-center'>";
        html += `<img src="${SITE_IMG}uploads/models/${value.profile_image}"  alt='model' width='30' height='30'>`;
        html += "<div class='pro-cnt'>";
        html += `<h2>${value.name}</h2>`;
        html += `<p class='m-0'>${teaser_date}</p>`;
        html += "</div>";
        html += "</div>";
        html += `<ul id="category-tabs">
            <li>`;
        if (value.is_fav_post == 1) {
            html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
            <i class="fa fa-star"></i>
            </a>`;
        } else {
            html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
            <i class="fa fa-star-o"></i>
            </a>`;
        }
        html += `</a>
                </li>
            </ul>`;
        // html += `<button type='button' class='site-folow follow' data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow`;
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        return html;
    }

    function make_video_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
    <div class="feed-main">
        <video class="mmCardVid" controls>
            <source src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4" autoplay loop>
            secure connection could not be established
        </video>
        
        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
            <div class="d-flex align-items-center">
                <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">
                <div class="pro-cnt">
                    <h2>${value.name}</h2>
                    <?php
                    // $dateDisplay = get_date_in_format(`${value.teaser_created_date}`);
                    ?>
                    <p class="m-0">${teaser_date}</p>
                </div>
            </div>
            <ul id="category-tabs">
            <li>`;
        if (value.is_fav_post == 1) {
            html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
            <i class="fa fa-star"></i>
            </a>`;
        } else {
            html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
            <i class="fa fa-star-o"></i>
            </a>`;
        }
        html += `</a></li>
            </ul>`;
        //<button type="button" class="site-folow follow" data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow</button>
        html += `</div>
    </div>
</div>`;
        return html;
    }
</script>