<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/admin/image/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <link href="<?php echo base_url(ADM_CSS . 'circle_crop.css'); ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>

    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css">
    
    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
    <!-- Newly Added Starts Here -->
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>intlTelInput.css"> 
    <!-- wizrad -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_arrows.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_circles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_dots.css">
    <!-- Newly Added Ends Here -->
</head>

<body class="vertical-layout site-feed vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="site-wrap">
        <input type="hidden" id="usedId" value="<?php echo check_variable_value($user_id); ?>">
        <!-- main section starts -->
        <?php
        if ($header_panel) {
            if ($user_id > 0) {
                $this->load->view('front/header');
            }
        }
        ?>

        <!-- <script src="build/js/intlTelInput.js"></script>
        <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {

          utilsScript: "js/utils.js",
        });
        </script> -->

        <script src="<?php echo base_url() . FRONT_JS; ?>intlTelInput.js"></script>
          
        <div class="app-content content site-setting friends-list site-premium site-acnt-step ml-0">
            <div class="content-wrapper">
                <section class="account-step-main">
                  <div class="container">
                    <div class="mdl-acnt-main">
                      <div class="home-wizard">
                            <div id="smartwizard2">
                              <ul class="d-flex justify-content-around">
                                <li><a href="#step-1"><span class="step-num">1</span>Account</a></li>
                                <li><a href="#step-2"><span class="step-num">2</span>Verify</a></li>
                                <li><a href="#step-3"><span class="step-num">3</span>Review</a></li>
                                <li><a href="#step-4"><span class="step-num">4</span>Upload</a></li>
                              </ul>
                             <!--  <button class="sw-btn-next">jkkj</button> -->
                            <div>
                            <div id="step-1" class="mt-3">
                                <div class="stage1">
                                    <div class="account-step-1">
                                      <div class="d-flex align-items-center justify-content-between">
                                        <h3>Profile</h3>
                                        <span data-toggle="popover-hover" data-content="Please make sure to fill up the entire application, upload your ID, agree to the terms, and start uploading your content." data-original-title="Profile" title="" class="popper-pro">!</span>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Your Model Name<span>*</span></label>
                                          <input type="text" value="Joya" class="mb-0">
                                          <p>This is your model name that we'll show to your subscribers.</p>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Email Address<span>*</span></label>
                                          <input type="email" value=joya@gmail.com class="mb-0">
                                          <p>This is the email you'll use to login and where we'll send messages and notifications.</p>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Country of Residency<span>*</span></label>
                                          <div class="cus-select pre-cus-lb">
                                            <label class="w-100">
                                              <select class="mb-0"> 
                                                  <option value="" data-code="">Select
                                                  Country</option> <option value="US" data-code="1">United States</option>
                                                  <option value="AF" data-code="93">Afghanistan</option> <option value="AX"
                                                  data-code="358">Åland</option> <option value="AL"
                                                  data-code="355">Albania</option> <option value="DZ"
                                                  data-code="213">Algeria</option> <option value="AS" data-code="1684">American
                                                  Samoa</option> <option value="AD" data-code="376">Andorra</option> <option
                                                  value="AO" data-code="244">Angola</option> <option value="AI"
                                                  data-code="1264">Anguilla</option> <option value="AQ"
                                                  data-code="672">Antarctica</option> <option value="AG"
                                                  data-code="1268">Antigua and Barbuda</option> <option value="AR"
                                                  data-code="54">Argentina</option> <option value="AM"
                                                  data-code="374">Armenia</option> <option value="AW"
                                                  data-code="297">Aruba</option> <option value="AU"
                                                  data-code="61">Australia</option> <option value="AT"
                                                  data-code="43">Austria</option> <option value="AZ"
                                                  data-code="994">Azerbaijan</option> <option value="BS"
                                                  data-code="1242">Bahamas</option> <option value="BH"
                                                  data-code="973">Bahrain</option> <option value="BD"
                                                  data-code="880">Bangladesh</option> <option value="BB"
                                                  data-code="1246">Barbados</option> <option value="BY"
                                                  data-code="375">Belarus</option> <option value="BE"
                                                  data-code="32">Belgium</option> <option value="BZ"
                                                  data-code="501">Belize</option> <option value="BJ"
                                                  data-code="229">Benin</option> <option value="BM"
                                                  data-code="1441">Bermuda</option> <option value="BT"
                                                  data-code="975">Bhutan</option> <option value="BO"
                                                  data-code="591">Bolivia</option> <option value="BQ"
                                                  data-code="">Bonaire</option> <option value="BA" data-code="387">Bosnia and
                                                  Herzegovina</option> <option value="BW" data-code="267">Botswana</option>
                                                  <option value="BV" data-code="">Bouvet Island</option> <option value="BR"
                                                  data-code="55">Brazil</option> <option value="IO" data-code="246">British
                                                  Indian Ocean Territory</option> <option value="VG" data-code="1284">British
                                                  Virgin Islands</option> <option value="BN" data-code="673">Brunei</option>
                                                  <option value="BG" data-code="359">Bulgaria</option> <option value="BF"
                                                  data-code="226">Burkina Faso</option> <option value="BI"
                                                  data-code="257">Burundi</option> <option value="CV" data-code="238">Cabo
                                                  Verde</option> <option value="KH" data-code="855">Cambodia</option> <option
                                                  value="CM" data-code="237">Cameroon</option> <option value="CA"
                                                  data-code="1">Canada</option> <option value="KY" data-code="345">Cayman
                                                  Islands</option> <option value="CF" data-code="236">Central African
                                                  Republic</option> <option value="TD" data-code="235">Chad</option> <option
                                                  value="CL" data-code="56">Chile</option> <option value="CN"
                                                  data-code="86">China</option> <option value="CX" data-code="61">Christmas
                                                  Island</option> <option value="CC" data-code="61">Cocos [Keeling]
                                                  Islands</option> <option value="CO" data-code="57">Colombia</option> <option
                                                  value="KM" data-code="269">Comoros</option> <option value="CG"
                                                  data-code="242">Congo Republic</option> <option value="CK"
                                                  data-code="682">Cook Islands</option> <option value="CR" data-code="506">Costa
                                                  Rica</option> <option value="HR" data-code="385">Croatia</option> <option
                                                  value="CU" data-code="53">Cuba</option> <option value="CW"
                                                  data-code="">Curaçao</option> <option value="CY"
                                                  data-code="357">Cyprus</option> <option value="CZ"
                                                  data-code="420">Czechia</option> <option value="DK"
                                                  data-code="45">Denmark</option> <option value="DJ"
                                                  data-code="253">Djibouti</option> <option value="DM"
                                                  data-code="1767">Dominica</option> <option value="DO"
                                                  data-code="1849">Dominican Republic</option> <option value="CD"
                                                  data-code="243">DR Congo</option> <option value="TL" data-code="670">East
                                                  Timor</option> <option value="EC" data-code="593">Ecuador</option> <option
                                                  value="EG" data-code="20">Egypt</option> <option value="SV" data-code="503">El
                                                  Salvador</option> <option value="GQ" data-code="240">Equatorial
                                                  Guinea</option> <option value="ER" data-code="291">Eritrea</option> <option
                                                  value="EE" data-code="372">Estonia</option> <option value="SZ"
                                                  data-code="268">Eswatini</option> <option value="ET"
                                                  data-code="251">Ethiopia</option> <option value="FK" data-code="500">Falkland
                                                  Islands</option> <option value="FO" data-code="298">Faroe Islands</option>
                                                  <option value="FM" data-code="691">Federated States of Micronesia</option>
                                                  <option value="FJ" data-code="679">Fiji</option> <option value="FI"
                                                  data-code="358">Finland</option> <option value="FR"
                                                  data-code="33">France</option> <option value="GF" data-code="594">French
                                                  Guiana</option> <option value="PF" data-code="689">French Polynesia</option>
                                                  <option value="TF" data-code="">French Southern Territories</option> <option
                                                  value="GA" data-code="241">Gabon</option> <option value="GM"
                                                  data-code="220">Gambia</option> <option value="GE"
                                                  data-code="995">Georgia</option> <option value="DE"
                                                  data-code="49">Germany</option> <option value="GH"
                                                  data-code="233">Ghana</option> <option value="GI"
                                                  data-code="350">Gibraltar</option> <option value="GR"
                                                  data-code="30">Greece</option> <option value="GL"
                                                  data-code="299">Greenland</option> <option value="GD"
                                                  data-code="1473">Grenada</option> <option value="GP"
                                                  data-code="590">Guadeloupe</option> <option value="GU"
                                                  data-code="1671">Guam</option> <option value="GT"
                                                  data-code="502">Guatemala</option> <option value="GG"
                                                  data-code="44">Guernsey</option> <option value="GN"
                                                  data-code="224">Guinea</option> <option value="GW"
                                                  data-code="245">Guinea-Bissau</option> <option value="GY"
                                                  data-code="595">Guyana</option> <option value="HT"
                                                  data-code="509">Haiti</option> <option value="JO" data-code="962">Hashemite
                                                  Kingdom of Jordan</option> <option value="HM" data-code="">Heard Island and
                                                  McDonald Islands</option> <option value="HN" data-code="504">Honduras</option>
                                                  <option value="HK" data-code="852">Hong Kong</option> <option value="HU"
                                                  data-code="36">Hungary</option> <option value="IS"
                                                  data-code="354">Iceland</option> <option value="IN"
                                                  data-code="91">India</option> <option value="ID"
                                                  data-code="62">Indonesia</option> <option value="IR"
                                                  data-code="98">Iran</option> <option value="IQ" data-code="964">Iraq</option>
                                                  <option value="IE" data-code="353">Ireland</option> <option value="IM"
                                                  data-code="44">Isle of Man</option> <option value="IL"
                                                  data-code="972">Israel</option> <option value="IT"
                                                  data-code="39">Italy</option> <option value="CI" data-code="225">Ivory
                                                  Coast</option> <option value="JM" data-code="1876">Jamaica</option> <option
                                                  value="JP" data-code="81">Japan</option> <option value="JE"
                                                  data-code="44">Jersey</option> <option value="KZ"
                                                  data-code="77">Kazakhstan</option> <option value="KE"
                                                  data-code="254">Kenya</option> <option value="KI"
                                                  data-code="686">Kiribati</option> <option value="XK"
                                                  data-code="">Kosovo</option> <option value="KW"
                                                  data-code="965">Kuwait</option> <option value="KG"
                                                  data-code="996">Kyrgyzstan</option> <option value="LA"
                                                  data-code="856">Laos</option> <option value="LV"
                                                  data-code="371">Latvia</option> <option value="LB"
                                                  data-code="961">Lebanon</option> <option value="LS"
                                                  data-code="266">Lesotho</option> <option value="LR"
                                                  data-code="231">Liberia</option> <option value="LY"
                                                  data-code="218">Libya</option> <option value="LI"
                                                  data-code="423">Liechtenstein</option> <option value="LU"
                                                  data-code="352">Luxembourg</option> <option value="MO"
                                                  data-code="853">Macao</option> <option value="MG"
                                                  data-code="261">Madagascar</option> <option value="MW"
                                                  data-code="265">Malawi</option> <option value="MY"
                                                  data-code="60">Malaysia</option> <option value="MV"
                                                  data-code="960">Maldives</option> <option value="ML"
                                                  data-code="223">Mali</option> <option value="MT"
                                                  data-code="356">Malta</option> <option value="MH" data-code="692">Marshall
                                                  Islands</option> <option value="MQ" data-code="596">Martinique</option>
                                                  <option value="MR" data-code="222">Mauritania</option> <option value="MU"
                                                  data-code="230">Mauritius</option> <option value="YT"
                                                  data-code="262">Mayotte</option> <option value="MX"
                                                  data-code="52">Mexico</option> <option value="MC"
                                                  data-code="377">Monaco</option> <option value="MN"
                                                  data-code="976">Mongolia</option> <option value="ME"
                                                  data-code="382">Montenegro</option> <option value="MS"
                                                  data-code="1664">Montserrat</option> <option value="MA"
                                                  data-code="212">Morocco</option> <option value="MZ"
                                                  data-code="258">Mozambique</option> <option value="MM"
                                                  data-code="95">Myanmar</option> <option value="NA"
                                                  data-code="264">Namibia</option> <option value="NR"
                                                  data-code="674">Nauru</option> <option value="NP"
                                                  data-code="977">Nepal</option> <option value="NL"
                                                  data-code="31">Netherlands</option> <option value="NC" data-code="687">New
                                                  Caledonia</option> <option value="NZ" data-code="64">New Zealand</option>
                                                  <option value="NI" data-code="505">Nicaragua</option> <option value="NE"
                                                  data-code="227">Niger</option> <option value="NG"
                                                  data-code="234">Nigeria</option> <option value="NU"
                                                  data-code="683">Niue</option> <option value="NF" data-code="672">Norfolk
                                                  Island</option> <option value="KP" data-code="850">North Korea</option>
                                                  <option value="MK" data-code="389">North Macedonia</option> <option value="MP"
                                                  data-code="1670">Northern Mariana Islands</option> <option value="NO"
                                                  data-code="47">Norway</option> <option value="OM"
                                                  data-code="968">Oman</option> <option value="PK"
                                                  data-code="92">Pakistan</option> <option value="PW"
                                                  data-code="680">Palau</option> <option value="PS"
                                                  data-code="970">Palestine</option> <option value="PA"
                                                  data-code="507">Panama</option> <option value="PG" data-code="675">Papua New
                                                  Guinea</option> <option value="PY" data-code="595">Paraguay</option> <option
                                                  value="PE" data-code="51">Peru</option> <option value="PH"
                                                  data-code="63">Philippines</option> <option value="PN"
                                                  data-code="872">Pitcairn Islands</option> <option value="PL"
                                                  data-code="48">Poland</option> <option value="PT"
                                                  data-code="351">Portugal</option> <option value="PR" data-code="1939">Puerto
                                                  Rico</option> <option value="QA" data-code="974">Qatar</option> <option
                                                  value="LT" data-code="370">Republic of Lithuania</option> <option value="MD"
                                                  data-code="373">Republic of Moldova</option> <option value="RE"
                                                  data-code="262">Réunion</option> <option value="RO"
                                                  data-code="40">Romania</option> <option value="RU"
                                                  data-code="7">Russia</option> <option value="RW"
                                                  data-code="250">Rwanda</option> <option value="BL" data-code="590">Saint
                                                  Barthélemy</option> <option value="SH" data-code="290">Saint Helena</option>
                                                  <option value="LC" data-code="1758">Saint Lucia</option> <option value="MF"
                                                  data-code="590">Saint Martin</option> <option value="PM" data-code="508">Saint
                                                  Pierre and Miquelon</option> <option value="VC" data-code="1784">Saint Vincent
                                                  and the Grenadines</option> <option value="WS" data-code="685">Samoa</option>
                                                  <option value="SM" data-code="378">San Marino</option> <option value="ST"
                                                  data-code="239">São Tomé and Príncipe</option> <option value="SA"
                                                  data-code="966">Saudi Arabia</option> <option value="SN"
                                                  data-code="221">Senegal</option> <option value="RS"
                                                  data-code="381">Serbia</option> <option value="SC"
                                                  data-code="248">Seychelles</option> <option value="SL" data-code="232">Sierra
                                                  Leone</option> <option value="SG" data-code="65">Singapore</option> <option
                                                  value="SX" data-code="">Sint Maarten</option> <option value="SK"
                                                  data-code="421">Slovakia</option> <option value="SI"
                                                  data-code="386">Slovenia</option> <option value="SB" data-code="677">Solomon
                                                  Islands</option> <option value="SO" data-code="252">Somalia</option> <option
                                                  value="ZA" data-code="27">South Africa</option> <option value="GS"
                                                  data-code="500">South Georgia and the South Sandwich Islands</option> <option
                                                  value="KR" data-code="82">South Korea</option> <option value="SS"
                                                  data-code="">South Sudan</option> <option value="ES"
                                                  data-code="34">Spain</option> <option value="LK" data-code="94">Sri
                                                  Lanka</option> <option value="KN" data-code="1869">St Kitts and Nevis</option>
                                                  <option value="SD" data-code="249">Sudan</option> <option value="SR"
                                                  data-code="597">Suriname</option> <option value="SJ" data-code="47">Svalbard
                                                  and Jan Mayen</option> <option value="SE" data-code="46">Sweden</option>
                                                  <option value="CH" data-code="41">Switzerland</option> <option value="SY"
                                                  data-code="963">Syria</option> <option value="TW"
                                                  data-code="886">Taiwan</option> <option value="TJ"
                                                  data-code="992">Tajikistan</option> <option value="TZ"
                                                  data-code="255">Tanzania</option> <option value="TH"
                                                  data-code="66">Thailand</option> <option value="TG"
                                                  data-code="228">Togo</option> <option value="TK"
                                                  data-code="690">Tokelau</option> <option value="TO"
                                                  data-code="676">Tonga</option> <option value="TT" data-code="1868">Trinidad
                                                  and Tobago</option> <option value="TN" data-code="216">Tunisia</option>
                                                  <option value="TR" data-code="90">Turkey</option> <option value="TM"
                                                  data-code="993">Turkmenistan</option> <option value="TC"
                                                  data-code="1649">Turks and Caicos Islands</option> <option value="TV"
                                                  data-code="688">Tuvalu</option> <option value="UM" data-code="">U.S. Minor
                                                  Outlying Islands</option> <option value="VI" data-code="1340">U.S. Virgin
                                                  Islands</option> <option value="UG" data-code="256">Uganda</option> <option
                                                  value="UA" data-code="380">Ukraine</option> <option value="AE"
                                                  data-code="971">United Arab Emirates</option> <option value="GB"
                                                  data-code="44">United Kingdom</option> <option value="UY"
                                                  data-code="598">Uruguay</option> <option value="UZ"
                                                  data-code="998">Uzbekistan</option> <option value="VU"
                                                  data-code="678">Vanuatu</option> <option value="VA" data-code="379">Vatican
                                                  City</option> <option value="VE" data-code="58">Venezuela</option> <option
                                                  value="VN" data-code="84">Vietnam</option> <option value="WF"
                                                  data-code="681">Wallis and Futuna</option> <option value="EH"
                                                  data-code="">Western Sahara</option> <option value="YE"
                                                  data-code="967">Yemen</option> <option value="ZM"
                                                  data-code="260">Zambia</option> <option value="ZW"
                                                  data-code="263">Zimbabwe</option> 
                                              </select>
                                              </label>
                                          </div>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Phone Number<span>*</span></label>
                                          <form>
                                            <input id="phone" name="phone" type="tel">
                                          </form>
                                          <p>This is the phone number we'll use if we need to reach you directly.</p>
                                      </div>
                                      <div class="row">
                                        <div class="col-12">
                                          <div class="d-flex align-items-center justify-content-between mt-3">
                                            <h3>Social Networks</h3>
                                            <span data-toggle="popover-hover" data-content="Your Social Networks (Instagram or Twitter is required). Without an active social media account your application will not be approved." data-original-title="SOCIAL NETWORKS" title="" class="popper-pro">!</span>
                                          </div>
                                          <p style="font-size: 12px;color: #151010;opacity: .5;margin-top: 6px;">We require models to enter their public Instagram accounts in the model application in order to -- verify your identity and see your social reach. If you don't have Instagram, please enter your Twitter account.</p>
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <label>Your Social Networks (Instagram or Twitter is required)<span>*</span></label>
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="images/twitter.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Twitter Username" class="mb-0">
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="images/instagram.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Instagram Username" class="mb-0">
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="images/tiktok.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Tiktok Username" class="mb-0">
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="images/facebook.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Facebook Username" class="mb-0">
                                        </div>
                                      </div>
                                      <div class="d-flex align-items-center justify-content-between mt-3">
                                        <h3>Settings</h3>
                                        <span data-toggle="popover-hover" data-content="Your Model settings. This will be your model username and subscription price. Your subscription price is how much a user will be charged to subscribe to you." data-original-title="Settings" title="" class="popper-pro">!</span>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Username<span>*</span></label>
                                          <input type="text" value="Joya" class="mb-0">
                                          <p>This is your model username and will be used to view your profile URL. Must ONLY contain letters and numbers.</p>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>The Royal Fruit</label>
                                          <div class="cus-select pre-cus-lb">
                                            <label class="w-100">
                                              <select class="mb-0">
                                                <option value="7">$1 for 24 hours, $10/month</option>
                                                <option value="8">$1 for 24 hours, $15/month</option>
                                                <option value="9">$1 for 24 hours, $20/month</option>
                                                <option value="10">$1 for 24 hours, $25/month</option>             
                                                <option value="63">$1 for 30 days, $10/month</option>
                                                <option value="11">$3 for 7 days, $10/month</option>
                                                <option value="12">$3 for 7 days, $15/month</option>
                                                <option value="13">$3 for 7 days, $20/month</option>                                    <option value="14">$3 for 7 days, $25/month</option>                                       
                                                <option value="1" selected="">$10/month</option>
                                                <option value="2">$15/month</option>
                                                <option value="3">$20/month</option>                                           
                                                <option value="4">$25/month</option>
                                              </select>
                                              <p>This is how much a user will be charged to subscribe to you.</p>
                                            </label>
                                          </div>
                                      </div>
                                      <button type="submit" class="btn btn-gradient btn-sub sw-btn-next" id="step2">Next - Verify</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step-2" class="mt-3 wiz-step2">
                              <div class="d-flex align-items-center justify-content-between account-step-1">
                                <h3>Upload IDs</h3>
                                <span data-toggle="popover-hover" data-content="Your ID must be valid with visible birthdate. This can be your Driver's License, State ID, Military ID, or Passport." data-original-title="UPLOAD IDS" title="" class="popper-pro">!</span>
                              </div>
                              <div class="photo-id-sec">
                                <h4>1.Photo ID</h4>
                                <div class="id-img-block">
                                  <img src="./images/photo_id.svg">
                                </div>
                                <div class="good-ex-list">
                                  <h4>✓ Good Example</h4>
                                  <ul>
                                    <li>Clearly show ID document with text clearly visible and with minimum background.</li>
                                    <li>The image should not be edited, resized or rotated.</li>
                                    <li>The file must be .PNG or .JPG under 7MB in size.</li>
                                    <li>Any ID uploaded must be a valid government-issued ID with visible birthdate.</li>
                                  </ul>
                                </div>
                                <div class="d-flex align-items-center">
                                  <p class="phhoto-id">Photo ID<span style="color: red">*</span></p>
                                  <div class="image-upload w-100">
                                    <div class="file-upload-btn">
                                      Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="photo-id-sec mt-3">
                                <h4>2. Selfie with ID</h4>
                                <div class="id-img-block">
                                  <img src="./images/selfie_id.svg">
                                </div>
                                <div class="good-ex-list">
                                  <h4>✓ Good Example</h4>
                                  <ul>
                                    <li>Please take a selfie holding your ID so we can clearly see your face and ID in the photo. We need this to accurately verify your identity.</li>
                                  </ul>
                                </div>
                                <div class="d-flex align-items-center">
                                  <p class="phhoto-id" style="width: 22%;">Selfie With ID<span style="color: red">*</span></p>
                                  <div class="image-upload w-100">
                                    <div class="file-upload-btn">
                                      Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="border-btm"></div>
                              <div class="d-flex align-items-center justify-content-between account-step-1 mt-2">
                                <h3>ID Information</h3>
                                <span data-toggle="popover-hover" data-content="Your ID information will be used to prepare your legal documents and to receive payments." data-original-title="ID Information" title="" class="popper-pro">!</span>
                              </div>
                              <div class="id-inf-main">
                                <div class="set-mdl-main mt-1">
                                  <label>Government Issued ID Type<span>*</span></label>
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Select One</option>
                                        <option value="drivers-license">Driver's License</option>
                                        <option value="state-id">State ID</option>
                                        <option value="military-id">Military ID</option>
                                        <option value="non-us-passport">Non-US Passport</option>
                                        <option value="us-passport">US Passport</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                                <div class="set-mdl-main mt-1">
                                  <label>Issued By (State/Province)<span>*</span></label>
                                  <input type="text" name="" class="mb-0">
                                </div>
                                <div class="set-mdl-main mt-1">
                                  <label>ID Number<span>*</span></label>
                                  <input type="text" name="" class="mb-0">
                                </div>
                                <label style="color: #000;font-weight: 500;" class="mt-1">ID Expiration</label>
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="set-mdl-main">
                                      <div class="cus-select pre-cus-lb">
                                        <label class="w-100">
                                          <select class="mb-0">
                                            <option value="">Month</option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="set-mdl-main">
                                      <div class="cus-select pre-cus-lb">
                                        <label class="w-100">
                                          <select class="mb-0">
                                            <option value="">Day</option>
                                              <option value="01">1</option>
                                              <option value="02">2</option>
                                              <option value="03">3</option>
                                              <option value="04">4</option>
                                              <option value="05">5</option>
                                              <option value="06">6</option>
                                              <option value="07">7</option>
                                              <option value="08">8</option>
                                              <option value="09">9</option>
                                              <option value="10">10</option>
                                              <option value="11">11</option>
                                              <option value="12">12</option>
                                              <option value="13">13</option>
                                              <option value="14">14</option>
                                              <option value="15">15</option>
                                              <option value="16">16</option>
                                              <option value="17">17</option>
                                              <option value="18">18</option>
                                              <option value="19">19</option>
                                              <option value="20">20</option>
                                              <option value="21">21</option>
                                              <option value="22">22</option>
                                              <option value="23">23</option>
                                              <option value="24">24</option>
                                              <option value="25">25</option>
                                              <option value="26">26</option>
                                              <option value="27">27</option>
                                              <option value="28">28</option>
                                              <option value="29">29</option>
                                              <option value="30">30</option>
                                              <option value="31">31</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="set-mdl-main">
                                      <div class="cus-select pre-cus-lb">
                                        <label class="w-100">
                                          <select class="mb-0">
                                            <option value="">Year</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                            <option value="2030">2030</option>
                                            <option value="2031">2031</option>
                                            <option value="2032">2032</option>
                                            <option value="2033">2033</option>
                                            <option value="2034">2034</option>
                                            <option value="2035">2035</option>
                                            <option value="2036">2036</option>
                                            <option value="2037">2037</option>
                                            <option value="2038">2038</option>
                                            <option value="2039">2039</option>
                                            <option value="2040">2040</option>
                                            <option value="2041">2041</option>
                                            <option value="2042">2042</option>
                                            <option value="2043">2043</option>
                                            <option value="2044">2044</option>
                                            <option value="2045">2045</option>
                                            <option value="2046">2046</option>
                                            <option value="2047">2047</option>
                                            <option value="2048">2048</option>
                                            <option value="2049">2049</option>
                                            <option value="2050">2050</option>
                                            <option value="2051">2051</option>
                                            <option value="2052">2052</option>
                                            <option value="2053">2053</option>
                                            <option value="2054">2054</option>
                                            <option value="2055">2055</option>
                                            <option value="2056">2056</option>
                                            <option value="2057">2057</option>
                                            <option value="2058">2058</option>
                                            <option value="2059">2059</option>
                                            <option value="2060">2060</option>
                                            <option value="2061">2061</option>
                                            <option value="2062">2062</option>
                                            <option value="2063">2063</option>
                                            <option value="2064">2064</option>
                                            <option value="2065">2065</option>
                                            <option value="2066">2066</option>
                                            <option value="2067">2067</option>
                                            <option value="2068">2068</option>
                                            <option value="2069">2069</option>
                                            <option value="2070">2070</option>
                                            <option value="2071">2071</option>
                                            <option value="2072">2072</option>
                                            <option value="2073">2073</option>
                                            <option value="2074">2074</option>
                                            <option value="2075">2075</option>
                                            <option value="2076">2076</option>
                                            <option value="2077">2077</option>
                                            <option value="2078">2078</option>
                                            <option value="2079">2079</option>
                                            <option value="2080">2080</option>
                                            <option value="2081">2081</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-12 mt-1">
                                    <label class="control control--checkbox">No Expiration
                                      <input type="checkbox">
                                      <span class="control__indicator"></span>
                                    </label>
                                  </div>
                                </div>
                                <div class="set-mdl-main">
                                  <label>Full Legal Name<span>*</span></label>
                                  <input type="text" name="">
                                </div>
                                <label style="color: #000;font-weight: 500;">Date of Birth<span style="color: red;">*</span></label>
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="set-mdl-main">
                                      <div class="cus-select pre-cus-lb">
                                        <label class="w-100">
                                          <select class="mb-0">
                                            <option value="">Month</option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="set-mdl-main">
                                      <div class="cus-select pre-cus-lb">
                                        <label class="w-100">
                                          <select class="mb-0">
                                            <option value="">Day</option>
                                              <option value="01">1</option>
                                              <option value="02">2</option>
                                              <option value="03">3</option>
                                              <option value="04">4</option>
                                              <option value="05">5</option>
                                              <option value="06">6</option>
                                              <option value="07">7</option>
                                              <option value="08">8</option>
                                              <option value="09">9</option>
                                              <option value="10">10</option>
                                              <option value="11">11</option>
                                              <option value="12">12</option>
                                              <option value="13">13</option>
                                              <option value="14">14</option>
                                              <option value="15">15</option>
                                              <option value="16">16</option>
                                              <option value="17">17</option>
                                              <option value="18">18</option>
                                              <option value="19">19</option>
                                              <option value="20">20</option>
                                              <option value="21">21</option>
                                              <option value="22">22</option>
                                              <option value="23">23</option>
                                              <option value="24">24</option>
                                              <option value="25">25</option>
                                              <option value="26">26</option>
                                              <option value="27">27</option>
                                              <option value="28">28</option>
                                              <option value="29">29</option>
                                              <option value="30">30</option>
                                              <option value="31">31</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="set-mdl-main">
                                      <div class="cus-select pre-cus-lb">
                                        <label class="w-100">
                                          <select class="mb-0">
                                            <option value="">Year</option>
                                            <option value="2002">2002</option>
                                            <option value="2001">2001</option>
                                            <option value="2000">2000</option>
                                            <option value="1999">1999</option>
                                            <option value="1998">1998</option>
                                            <option value="1997">1997</option>
                                            <option value="1996">1996</option>
                                            <option value="1995">1995</option>
                                            <option value="1994">1994</option>
                                            <option value="1993">1993</option>
                                            <option value="1992">1992</option>
                                            <option value="1991">1991</option>
                                            <option value="1990">1990</option>
                                            <option value="1989">1989</option>
                                            <option value="1988">1988</option>
                                            <option value="1987">1987</option>
                                            <option value="1986">1986</option>
                                            <option value="1985">1985</option>
                                            <option value="1984">1984</option>
                                            <option value="1983">1983</option>
                                            <option value="1982">1982</option>
                                            <option value="1981">1981</option>
                                            <option value="1980">1980</option>
                                            <option value="1979">1979</option>
                                            <option value="1978">1978</option>
                                            <option value="1977">1977</option>
                                            <option value="1976">1976</option>
                                            <option value="1975">1975</option>
                                            <option value="1974">1974</option>
                                            <option value="1973">1973</option>
                                            <option value="1972">1972</option>
                                            <option value="1971">1971</option>
                                            <option value="1970">1970</option>
                                            <option value="1969">1969</option>
                                            <option value="1968">1968</option>
                                            <option value="1967">1967</option>
                                            <option value="1966">1966</option>
                                            <option value="1965">1965</option>
                                            <option value="1964">1964</option>
                                            <option value="1963">1963</option>
                                            <option value="1962">1962</option>
                                            <option value="1961">1961</option>
                                            <option value="1960">1960</option>
                                            <option value="1959">1959</option>
                                            <option value="1958">1958</option>
                                            <option value="1957">1957</option>
                                            <option value="1956">1956</option>
                                            <option value="1955">1955</option>
                                            <option value="1954">1954</option>
                                            <option value="1953">1953</option>
                                            <option value="1952">1952</option>
                                            <option value="1951">1951</option>
                                            <option value="1950">1950</option>
                                            <option value="1949">1949</option>
                                            <option value="1948">1948</option>
                                            <option value="1947">1947</option>
                                            <option value="1946">1946</option>
                                            <option value="1945">1945</option>
                                            <option value="1944">1944</option>
                                            <option value="1943">1943</option>
                                            <option value="1942">1942</option>
                                            <option value="1941">1941</option>
                                            <option value="1940">1940</option>
                                            <option value="1939">1939</option>
                                            <option value="1938">1938</option>
                                            <option value="1937">1937</option>
                                            <option value="1936">1936</option>
                                            <option value="1935">1935</option>
                                            <option value="1934">1934</option>
                                            <option value="1933">1933</option>
                                            <option value="1932">1932</option>
                                            <option value="1931">1931</option>
                                            <option value="1930">1930</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-12">
                                    <div class="border-btm w-100"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="d-flex align-items-center justify-content-between account-step-1 mt-2">
                                <h3>Aliases/Stage Names</h3>
                                <span data-toggle="popover-hover" data-content="Your alias or other names you are known or have been known by, including stage names, Internet usernames, aliases, nicknames, and married or maiden names. This is optional." data-original-title="Aliases/Stage Names" title="" class="popper-pro">!</span>
                              </div>
                              <div class="alisa-main">
                                <div class="set-mdl-main mt-1">
                                  <label>Your Alias/Stage Names (enter at least one)</label>
                                  <input type="text" name="" class="mb-0" placeholder="Alisa or Stage Name"> 
                                  <input type="text" name="" class="mb-0 mt-1" placeholder="Other Alisa or Stage Name">
                                </div>
                                <div class="d-flex sec-stepn-btn sec-stepn-btn2 justify-content-between">
                                  <button type="submit" class="btn btn-gradient btn-sub sw-btn-prev" id="step1">Back</button>
                                  <button type="submit" class="btn btn-gradient btn-sub sw-btn-next" id="step3">Next - Review</button>
                                </div>
                              </div>
                            </div>
                            <div id="step-3" class="mt-3 wiz-step3 wiz-step2">
                              <div class="review-maiin">
                                <div class="d-flex align-items-center justify-content-between account-step-1 w-100">
                                  <h3>Review Documents</h3>
                                  <span data-toggle="popover-hover" data-content="This is a summary of your uploaded IDs and ID information." data-original-title="Review Documents" title="" class="popper-pro">!</span>
                                </div>
                                <div class="border-btm w-100"></div>
                                <div class="photo-id-upd mt-2">
                                  <div class="d-flex">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <p>Photo ID</p>   
                                  </div>
                                  <div class="d-flex view-btn">
                                    <button type="button">View</button>
                                    <div class="image-upload w-100">
                                      <div class="res-upload-btn mt-0">
                                        Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="photo-id-upd">
                                  <div class="d-flex">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <p>Selfie with ID</p>   
                                  </div>
                                  <div class="d-flex view-btn">
                                    <button type="button">View</button>
                                    <div class="image-upload w-100">
                                      <div class="res-upload-btn mt-0">
                                        Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="border-btm w-100 p-0"></div>
                                <div class="photo-id-upd mt-1">
                                  <div class="d-flex">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <p>ID Information</p>   
                                  </div>
                                  <div class="d-flex view-btn">
                                    <div class="image-upload w-100">
                                      <div class="res-upload-btn mt-0">
                                        Update<input type="button" class="item-img file center-block hide_file">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="gov-cnt">
                                  <p>Government Issued ID type:</p>
                                  <span>State ID</span>
                                </div>
                                <div class="gov-cnt mt-1">
                                  <p>Issued By (State/Province)</p>
                                  <span>xyz</span>
                                </div>
                                <div class="gov-cnt mt-1">
                                  <p>ID Number</p>
                                  <span>1201920929</span>
                                </div>
                                <div class="gov-cnt mt-1">
                                  <p>ID Expiration:</p>
                                  <span>No Expiration</span>
                                </div>
                                <div class="gov-cnt mt-1">
                                  <p>Full Legal Name:</p>
                                  <span>joya</span>
                                </div>
                                <div class="gov-cnt mt-1">
                                  <p>Date Of Birth:</p>
                                  <span>August 15, 1997</span>
                                </div>
                                <div class="border-btm w-100"></div>
                                <div class="photo-id-upd mt-1">
                                  <div class="d-flex">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <p style="font-weight: normal;">Alias:</p>   
                                  </div>
                                </div>
                                <div class="border-btm w-100"></div>
                                <div class="d-flex align-items-center justify-content-between account-step-1 w-100 mt-2">
                                  <h3>Terms & Conditions</h3>
                                  <span data-toggle="popover-hover" data-content="These are the terms and conditions you need to agree on to become a model." data-original-title="Terms & Conditions" title="" class="popper-pro">!</span>
                                </div>
                                <div class="set-mdl-main agree-span mt-1">
                                  <label>Model Agreement<span>*</span></label>
                                    <label class="control control--checkbox w-100"> By checking this checkbox, I hereby indicate that I have read and understood and agree to be bound by the <a href="model_agreement.html" target="_blank">The Royal Fruit Model Agreement</a>
                                      <input type="checkbox">
                                      <span class="control__indicator"></span>
                                  </label>
                                </div>
                                <div class="set-mdl-main agree-span mt-1">
                                  <label>2257 Agreement<span>*</span></label>
                                    <label class="control control--checkbox w-100"> By checking this checkbox, I hereby indicate that I have read and understood and agree to be bound by the <a href="model_2257_agreement.html" target="_blank">The Royal Fruit 2257 Self-Production Records Keeping Compliance Agreement</a>
                                      <input type="checkbox">
                                      <span class="control__indicator"></span>
                                    </label>
                                </div>
                                <div class="set-mdl-main agree-span mt-1">
                                  <label>Foreign Performer Agreement*</label>
                                    <label class="control control--checkbox w-100"> By checking this checkbox, I hereby indicate that I have read and understood and agree to be bound by the <a href="model_foreign_agreement.html" target="_blank">The Royal Fruit Foreign Performer Agreement</a>
                                      <input type="checkbox">
                                      <span class="control__indicator"></span>
                                  </label>
                                </div>
                                <div class="d-flex sec-stepn-btn sec-stepn-btn3 justify-content-between">
                                  <button type="submit" class="btn btn-gradient btn-sub sw-btn-prev" id="step2">Back</button>
                                  <button type="submit" class="btn btn-gradient btn-sub sw-btn-next submit-btn-ac" id="step4">Submit My Application</button>
                                </div>
                              </div>
                            </div>
                            <div id="step-4" class="wiz-step4 wiz-step2">
                              <div class="upload-main">
                                <img src="./images/clock.svg" width="20">
                                <p class="mt-2">Your Application is now pending. Please be patient and we will contact you to let you know that you are approved, or if we need something else to approve your application. Thanks for your patience in advance as we get a large amount of applications each day.</p>
                                <div>
                                  <a href="getting_started.html">Getting Started</a>
                                </div>
                                <div class="mt-1">
                                  <a href="make_money.html">How To Make Money</a>
                                </div>
                              </div>
                              <div class="teaster-part text-center">
                                <h4>Upload At Least 10 Teaser Posts</h4>
                                <div class="border-btm p-0 w-100"></div>
                                  <div class="add-phtt">
                                    <div class="image-upload mt-2">
                                      <div class="add-photo-btn mt-0">
                                        <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp;Add Photo<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                      </div>
                                    </div>
                                    <div class="image-upload mt-2">
                                      <div class="add-photo-btn mt-0">
                                        <i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Add Video<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <div class="sec-stepn-btn sec-stepn-btn4">
                                <button type="submit" class="btn btn-gradient btn-sub sw-btn-prev w-100 back-sec" id="step3">Back</button>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div id="ts1">
                      <div class="d-flex teaser-block">
                        <h4><i class="fa fa-rss"></i>Your Teaser Posts</h4>
                        <div>
                          <ul class="nav nav-tabs nav3" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link nav-link7 model1" id="grid-tab" data-toggle="tab" href="#grid" role="tab" aria-controls="grid" aria-selected="false"><img src="images/cus1.png" class="crus-img" alt="grid"></a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link nav-link7 model1" id="full-tab" data-toggle="tab" href="#full" role="tab" aria-controls="full" aria-selected="false"><img src="images/cus2.png" class="crus-img" alt="view"></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="tab-content mt-2">
                        <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="grid-tab">
                          <div class="row">
                            <div class="col-12 mb-2">
                              <div class="feed-main">
                                <div class="position-relative overlay">
                                  <img src="images/download.png" alt="model">
                                </div>
                                <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                  <div class="d-flex align-items-center">
                                    <img src="images/pro2.png" alt="model" width="30" height="30">
                                    <div class="pro-cnt">
                                      <h2>Joya</h2>
                                      <p class="m-0">5 min ago</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="full" role="tabpanel" aria-labelledby="full-tab">
                          <div class="row">
                            <div class="col-md-4 mb-2 col-6">
                              <div class="feed-main">
                                <div class="position-relative overlay">
                                  <img src="images/download.png" alt="model">
                                </div>
                                <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                  <div class="d-flex align-items-center">
                                    <img src="images/pro2.png" alt="model" width="30" height="30">
                                    <div class="pro-cnt">
                                      <h2>Joya</h2>
                                      <p class="m-0">5 min ago</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-4 mb-2 col-6">
                              <div class="feed-main">
                                <div class="position-relative overlay">
                                  <img src="images/download.png" alt="model">
                                </div>
                                <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                  <div class="d-flex align-items-center">
                                    <img src="images/pro2.png" alt="model" width="30" height="30">
                                    <div class="pro-cnt">
                                      <h2>Joya</h2>
                                      <p class="m-0">5 min ago</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-4 mb-2 col-6">
                              <div class="feed-main">
                                  <video class="mmCardVid" controls="">
                                  <source src="video/video1.mp4" type="video/mp4">
                                  secure connection could not be established
                                  </video>
                                  <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                    <div class="d-flex align-items-center">
                                      <img src="images/pro2.png" alt="model" width="30" height="30">
                                      <div class="pro-cnt">
                                        <h2>Joya</h2>
                                        <p class="m-0">2 week ago</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>

                <!-- <section class="blocked-main mt-2">
                    <div class="container">
                        <h6>ADD LOCATION TO BLOCK</h6>
                        <div class="block-location-body mt-2">
                            <form action="<?php echo  base_url('account/add_geo_blocking_data'); ?>" method="post">
                                <div class="row pb-2">
                                    <div class="col-md-4 col-sm-4 mb-2">
                                        <label for="countries" class="lead lead-jni">Countries</label>
                                        <select class="form-control" id="countries">
                                            <option value='0'>Select Country</option>
                                        </select>
                                        <input type="hidden" id="country_name" name="country_name">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </section> -->
            </div>
        </div>

        <!-- main section ends -->
        <div class="site-feed">
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="modelId" id="modelId">
                            <input type="hidden" name="type" id="type">
                            <!--  // 0 for token purchase 
                        // 1 for follow -->
                            <input type="hidden" name="price" id="price">
                            <img src="" id="image_model_modal" width="50" alt="model">
                            <h2>Follow <span id="model_name_modal"></span></h2>
                            <h2>$<span id="model_price"></span>/month</h2>
                            <p>You'll get access to daily private content, direct messaging, and more!</p>
                            <button type="button" class="site-folow flw2" id="purchase_now">Continue</button>
                            <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a>
                            </p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url() . FRONT_THEME_ASSETS_VENDOR; ?>js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- Newly Added Starts Here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.smartWizard.js"></script>
    <!-- Newly Added Ends Here -->
    <script>
        <?php if (!empty($this->session->flashdata('success'))) { ?>
            sType = getStatusText(200);
            sText = '<?php echo $this->session->flashdata('success'); ?>';
            //Custom.myNotification(sType, sText);
            swal({
                title: "Success",
                text: sText,
                icon: "success",
            });
        <?php } ?>
        <?php if (!empty($this->session->flashdata('error'))) { ?>
            sType = getStatusText(412);
            sText = '<?php echo $this->session->flashdata('error'); ?>';
            // Custom.myNotification(sType, sText);
            swal({
                title: "Error!",
                text: sText,
                icon: "error",
            });
        <?php } ?>
    </script>
</body>

</html>
<script>
    $(document).ready(function() {
        let country_id = $("#countries").val();
        get_countries();
        $("#countries").on('select', function() {
            var selectedText = $("#countries option:selected").attr('data-value');
            $("#country_name").val(selectedText);
        });
    });

    function get_countries() {
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value='0'>Select Country</option>";
                $.each(r, function(key, val) {
                    html += `<option value="${val.id}" data-value="${val.name}">${val.name}</option>`;
                });
                $("#countries").html(html);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

</script>

<script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {

      utilsScript: "<?php echo base_url() . FRONT_JS; ?>utils.js",
    });
</script>
<script>
// SIDEBAR
$(document).ready(function(){
  $('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'right', // Choose the horizontal origin
      closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: false // Choose whether you can drag to open on touch screens
    }
  );
  // START OPEN
  $('.button-collapse').sideNav('hide');
});

</script>
<script>
$(function () {
  $('[data-toggle="popover-hover"]').popover({
    trigger: 'hover',
  })
})
  
</script>
<script>
  function format(item, state) {
  if (!item.id) {
    return item.text;
  }
  var countryUrl = "";
  var stateUrl = "https://oxguy3.github.io/flags/svg/us/";
  var url = state ? stateUrl : countryUrl;
  var img = $("<img>", {
    
    
  });
  var span = $("<span>", {
    text: " " + item.text
  });
  span.prepend(img);
  return span;
}

$(document).ready(function() {
  $("#countries").select2({
    templateResult: function(item) {
      return format(item, false);
    }
  });
  $("#state").select2({
    templateResult: function(item) {
      return format(item, true);
    }
  });
});

</script>

<!-- wizard -->
<script type="text/javascript">
        $(document).ready(function(){
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'arrows',
                    transitionEffect:'fade',
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            $('#smartwizard2').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: false
            });
        });
</script>

<script>
  $(document).ready(function(){
    $('#file-input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {
            
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                    return function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element 
                        $('#thumb-output').append(img); //append image to output element
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                }
            });
            
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });
  
  $(".remove").click(function (e) {
        e.preventDefault();
        data.splice(0, 1);
        $('#thumb-output a').eq(data.length).remove();
    });
});
</script>

<script>
$(document).ready(function () {
  $("#step4").click(function () {
      if ($("").hide()) {
          $("#ts1").show();
      }
  });
});
$(document).ready(function () {
  $(".back-sec").click(function () {
      if ($("").show()) {
          $("#ts1").hide();
      }
  });
});
</script>