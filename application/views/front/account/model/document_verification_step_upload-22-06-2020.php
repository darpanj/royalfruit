<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/admin/image/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <link href="<?php echo base_url(ADM_CSS . 'circle_crop.css'); ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>

    <!-- <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script> -->
    <!-- <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script> -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css">
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
    <!-- Newly Added Starts Here -->
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>intlTelInput.css"> 
    <!-- wizrad -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_arrows.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_circles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_dots.css">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <!-- Newly Added Ends Here -->
</head>

<body class="vertical-layout site-feed vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="site-wrap">
        <input type="hidden" id="usedId" value="<?php echo check_variable_value($user_id); ?>">
        <!-- main section starts -->
        <?php
        if ($header_panel) {
            if ($user_id > 0) {
                $this->load->view('front/header');
            }
        }
        ?>
        <style type="text/css">
            .loader-spinner { 
                display:block;
                border: 12px solid #f3f3f3; 
                border-radius: 50%; 
                border-top: 12px solid #444444; 
                width: 70px; 
                height: 70px; 
                animation: spin 1s linear infinite; 
            }
             
            @keyframes spin { 
                100% { 
                    transform: rotate(360deg); 
                } 
            } 
            
            .overlay {
                position:absolute;
                top:0;
                left:0;
                right:0;
                bottom:0;
                background-color:rgba(0, 0, 0, 0.85);
                z-index:9999;
                display:none;
            }
            
            .center { 
                position: fixed; 
                top: 0; 
                bottom: 0; 
                left: 0; 
                right: 0; 
                margin: auto; 
            } 
            
            .site-wrap{
                position:relative;
            }
            
            .site-listing .list-price {
                width: 85%;
                margin: 25px auto;
            }
            .completePM {
                background-color: #008000 !important;
            }
              
        </style>

        <div class='overlay' id="loader">
        <div class="loader-spinner center"></div>
        </div>

        <script src="<?php echo base_url() . FRONT_JS; ?>intlTelInput.js"></script>
              
        <div class="app-content content site-setting friends-list site-premium site-acnt-step ml-0">
          <div class="content-wrapper">
            <section class="account-step-main">
              <div class="container">
                <div class="mdl-acnt-main">
                  <div class="home-wizard">
                        <div id="smartwizard2">
                          <ul class="d-flex justify-content-around">
                            <li><a href="javascript:;"><span class="step-num">1</span>Account</a></li>
                            <li><a href="javascript:;"><span class="step-num">2</span>Verify</a></li>
                            <li><a href="javascript:;"><span class="step-num">3</span>Review</a></li>
                            <li class="active"><a href="#step-4"><span class="step-num">4</span>Upload</a></li>
                          </ul>
                         <!--  <button class="sw-btn-next">jkkj</button> -->
                        <div>
                        <div id="step-4" class="wiz-step4 wiz-step2">
                          <div class="upload-main">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>clock.svg" width="20">
                            <p class="mt-2">Your Application is now pending. Please be patient and we will contact you to let you know that you are approved, or if we need something else to approve your application. Thanks for your patience in advance as we get a large amount of applications each day.</p>
                            <div>
                              <a href="<?= site_url('account/get_started') ?>" target="_blank">Getting Started</a>
                            </div>
                            <div class="mt-1">
                              <a href="<?= site_url('account/make_money') ?>" target="_blank">How To Make Money</a>
                            </div>
                          </div>
                          <div class="teaster-part text-center">
                            <h4>Upload At Least 10 Teaser Posts</h4>
                            <div class="border-btm p-0 w-100"></div>
                              <div class="add-phtt">
                                <form method="post" name="teaserImageUpload" id="teaserImageUpload" enctype="multipart/form-data">
                                  <div class="image-upload mt-2">
                                    <div class="add-photo-btn mt-0">
                                      <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp;Add Photo<input type="file" class="item-img file center-block hide_file" accept="image/*" name="teaser_image" id="teaser_image">
                                    </div>
                                  </div>
                                </form>
                                <div class="image-upload mt-2">
                                  <div class="add-photo-btn mt-0">
                                    <i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Add Video<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="sec-stepn-btn sec-stepn-btn4">
                            <a href="<?= site_url('account/document_verification_step_review') ?>"><button type="button" class="btn btn-gradient btn-sub w-100">Back</button></a>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="ts1">
                  <div class="d-flex teaser-block">
                    <h4><i class="fa fa-rss"></i>Your Teaser Posts</h4>
                    <div>
                      <ul class="nav nav-tabs nav3" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link nav-link7 model1" id="grid-tab" data-toggle="tab" href="#grid" role="tab" aria-controls="grid" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus1.png" class="crus-img" alt="grid"></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link nav-link7 model1" id="full-tab" data-toggle="tab" href="#full" role="tab" aria-controls="full" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus2.png" class="crus-img" alt="view"></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="tab-content mt-2">
                    <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="grid-tab">
                      <div class="row">
                        <div class="col-12 mb-2">
                          <div class="feed-main">
                            <div class="position-relative overlay">
                              <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                            </div>
                            <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                              <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                  <h2>Joya</h2>
                                  <p class="m-0">5 min ago</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="full" role="tabpanel" aria-labelledby="full-tab">
                      <div class="row">
                        <div class="col-md-4 mb-2 col-6">
                          <div class="feed-main">
                            <div class="position-relative overlay">
                              <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                            </div>
                            <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                              <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                  <h2>Joya</h2>
                                  <p class="m-0">5 min ago</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 mb-2 col-6">
                          <div class="feed-main">
                            <div class="position-relative overlay">
                              <img src="<?php echo base_url() . FRONT_IMG; ?>download.png" alt="model">
                            </div>
                            <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                              <div class="d-flex align-items-center">
                                <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                <div class="pro-cnt">
                                  <h2>Joya</h2>
                                  <p class="m-0">5 min ago</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 mb-2 col-6">
                          <div class="feed-main">
                              <video class="mmCardVid" controls="">
                              <source src="video/video1.mp4" type="video/mp4">
                              secure connection could not be established
                              </video>
                              <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                <div class="d-flex align-items-center">
                                  <img src="<?php echo base_url() . FRONT_IMG; ?>pro2.png" alt="model" width="30" height="30">
                                  <div class="pro-cnt">
                                    <h2>Joya</h2>
                                    <p class="m-0">2 week ago</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>

        <!-- main section ends -->
        <div class="site-feed">
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="modelId" id="modelId">
                            <input type="hidden" name="type" id="type">
                            <!--  // 0 for token purchase 
                            // 1 for follow -->
                            <input type="hidden" name="price" id="price">
                            <img src="" id="image_model_modal" width="50" alt="model">
                            <h2>Follow <span id="model_name_modal"></span></h2>
                            <h2>$<span id="model_price"></span>/month</h2>
                            <p>You'll get access to daily private content, direct messaging, and more!</p>
                            <button type="button" class="site-folow flw2" id="purchase_now">Continue</button>
                            <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a>
                            </p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <!-- <script src="<?php echo base_url() . FRONT_THEME_ASSETS_VENDOR; ?>js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- Newly Added Starts Here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.smartWizard.js"></script>
    <!-- Newly Added Ends Here -->
    <script>
        <?php if (!empty($this->session->flashdata('success'))) { ?>
            sType = getStatusText(200);
            sText = '<?php echo $this->session->flashdata('success'); ?>';
            swal({
                title: "Success",
                text: sText,
                icon: "success",
            });
        <?php } ?>
        <?php if (!empty($this->session->flashdata('error'))) { ?>
            sType = getStatusText(412);
            sText = '<?php echo $this->session->flashdata('error'); ?>';
            swal({
                title: "Error!",
                text: sText,
                icon: "error",
            });
        <?php } ?>
    </script>
</body>

</html>

<script>
$(function () {
  $('[data-toggle="popover-hover"]').popover({
    trigger: 'hover',
  })
})
  
</script>

<!-- wizard -->
<script type="text/javascript">
        $(document).ready(function(){
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'arrows',
                    transitionEffect:'fade',
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            $('#smartwizard2').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: false
            });
        });
</script>

<script type="text/javascript">
$(document).ready(function () {

    $('#teaser_image').change(function(){
      ajaxTeaserImageUpload();
    });

    function ajaxTeaserImageUpload(){
        var data = new FormData($('#teaserImageUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_teaser_image') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }

});
</script>