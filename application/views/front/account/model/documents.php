<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
    .completePM {
        background-color: #008000 !important;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>
<div class="app-content content site-setting friends-list site-premium">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>
    <div class="content-header row"></div>
    <div class="content-body"></div>

    <section class="payments-tab documents-main">
      <div class="row no-gutters">
        <div class="col-lg-12">
          <div class="payment-form">
            <div class="pymnt-cnt">
              <p>DOCUMENTS</p>
            </div>
            <h4>Identification</h4>
              <?php
              $statusPhotoID = ($userData['document_photo_id'] != '') ? 'Completed' : 'Incomplete';
              $openPhotoIDImage = ($userData['document_photo_id'] != '') ? base_url().MODEL_UPD . $userData['document_photo_id'] : 'javascript:;';
              $tBlankPhotoID = ($userData['document_photo_id'] != '') ? 'target="_blank"' : '';
              ?>
            <div class="idn-main">
                <div class="identification">
                    <div class="idn-photo">
                      <div class="idn-cnt">
                        <p>Photo ID</p>
                        <span><?= $statusPhotoID ?></span>
                      </div>
                    </div>
                    <div class="idn-btn">
                      <!-- <input type="button" value="View" class="update-nets"> -->
                      <a href="<?= $openPhotoIDImage ?>" class="update-nets" <?= $tBlankPhotoID ?>>View</a>
                        <form method="post" name="PhotoIDUpload" id="PhotoIDUpload" enctype="multipart/form-data">
                            <div class="image-upload w-100">
                                <div class="file-upload-btn">
                                  Update<input type="file" class="item-img file center-block hide_file" accept="image/*" name="document_photo_id" id="document_photo_id">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              <?php
              $statusVerificationImage = ($userData['document_id_verification_selfie'] != '') ? 'Completed' : 'Incomplete';
              $openVerificationImage = ($userData['document_id_verification_selfie'] != '') ? base_url().MODEL_UPD . $userData['document_id_verification_selfie'] : 'javascript:;';
              $tBlankVerificationImage = ($userData['document_id_verification_selfie'] != '') ? 'target="_blank"' : '';
              ?>
              <div class="identification idn-brd-none">
                <div class="idn-photo">
                  <div class="idn-cnt">
                    <p>ID Verification Selfie</p>
                    <span><?= $statusVerificationImage ?></span>
                  </div>
                </div>
                <div class="idn-btn">
                  <!-- <input type="button" value="View" class="update-nets"> -->
                  <a href="<?= $openVerificationImage ?>" class="update-nets" <?= $tBlankVerificationImage ?>>View</a>
                    <form method="post" name="idVerificationSelfieUpload" id="idVerificationSelfieUpload" enctype="multipart/form-data">
                      <div class="image-upload w-100">
                        <div class="file-upload-btn">
                          Update<input type="file" class="item-img file center-block hide_file" accept="image/*" name="document_id_verification_selfie" id="document_id_verification_selfie">
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
          <div class="payment-form pb-3">
            <div class="pymnt-cnt idn-brd-none">
            </div>
            <div class="agree-main">
              <h4>Agreements</h4>
              <!-- <a href="javascript:;"><img src="./images/eye.svg" width="20"> View Agreement</a> -->
            </div>
              <?php
              $statusAgreementModel = ($userData['agreement_model'] == 1) ? 'Completed' : 'Incomplete';
              $statusAgreement2257 = ($userData['agreement_2257'] == 1) ? 'Completed' : 'Incomplete';
              $openModelAgreement = ($agreements['agreement_model'] != '') ? base_url().SITE_UPD . 'agreements/' . $agreements['agreement_model'] : 'javascript:;';
              $tBlankModelAgreement = ($userData['agreement_model'] != '') ? 'target="_blank"' : '';
              $open2257Agreement = ($agreements['agreement_2257'] != '') ? base_url().SITE_UPD . 'agreements/' . $agreements['agreement_2257'] : 'javascript:;';
              $tBlank2257Agreement = ($userData['agreement_2257'] != '') ? 'target="_blank"' : '';
              ?>
            <div class="idn-main">
              <div class="identification">
                <div class="idn-photo">
                  <div class="idn-cnt">
                    <p>Model Agreement</p>
                    <span><?= $statusAgreementModel ?></span>
                  </div>
                </div>
                <?php if($userData['agreement_model'] == 1) { ?>
                <div class="idn-btn">
                    <a href="<?= $openModelAgreement ?>" <?= $tBlankModelAgreement ?>><img src="./images/eye.svg" width="20"> View Agreement</a>
                </div>
                <?php } ?>
              </div>
              <div class="identification idn-brd-none">
                <div class="idn-photo">
                  <div class="idn-cnt">
                    <p>2257 Agreement</p>
                    <span><?= $statusAgreement2257 ?></span>
                  </div>
                </div>
                <?php if($userData['agreement_2257'] == 1) { ?>
                <div class="idn-btn">
                    <a href="<?= $open2257Agreement ?>" <?= $tBlank2257Agreement ?>><img src="./images/eye.svg" width="20"> View Agreement</a>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<script type="text/javascript">

$(document).ready(function () {

    $('#document_photo_id').change(function(){
      ajaxPhotoIDUpload();
    });

    function ajaxPhotoIDUpload(){
        var data = new FormData($('#PhotoIDUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_photo_id') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }

    $('#document_id_verification_selfie').change(function(){
      ajaxIDVerificationSelfieUpload();
    });

    function ajaxIDVerificationSelfieUpload(){
        var data = new FormData($('#idVerificationSelfieUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_id_verification_selfie') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }
    

});
</script>