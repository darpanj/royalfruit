<div class="app-content content site-setting friends-list">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"></div>
        <section class="site-pr-video">
            <div class="container">
                <div class="row align-items-center">
                    <div class="w-100 friend-feed-main">
                        <h2>Geo Blocking</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="blocked-main">
            <div class="container">
                <h6>BLOCKED LOCATIONS</h6>
                <div class="block-location-body mt-2">
                    <?php if (count($model_geo_blocking_address) > 0) { ?>
                        <?php foreach ($model_geo_blocking_address as $key => $value) { ?>
                            <div class="row pb-2">
                                <div class="col-md-4 col-sm-6 mb-1">
                                    <strong><?php echo $value['country']; ?>,</strong><span><?php echo $value['state']; ?>,</span><span><?php echo $value['city']; ?></span>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <a href="javascript:;" class="remove_block_address" data-value="<?php echo $value['id']; ?>">Remove</a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="blocked-main mt-2">
            <div class="container">
                <h6>ADD LOCATION TO BLOCK</h6>
                <div class="block-location-body mt-2">
                    <form action="<?php echo  base_url('account/add_geo_blocking_data'); ?>" method="post">
                        <div class="row pb-2">
                            <div class="col-md-4 col-sm-4 mb-2">
                                <label for="countries" class="lead lead-jni">Countries</label>
                                <select class="form-control" id="countries" name="country">
                                    <option value='0'>Select Country</option>
                                </select>

                            </div>
                            <div class="col-md-4 col-sm-4 mb-2">
                                <label for="state" class="lead lead-jni">State</label>
                                <select class="form-control" id="state" name="state">
                                    <option value='0'>Select States</option>

                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 mb-2">
                                <label for="city" class="lead lead-jni">City</label>
                                <select class="form-control" id="city" name="city">
                                    <option value="0">Select City</option>

                                </select>
                            </div>
                            <button>Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function() {
        let country_id = $("#countries").val();
        let state = $("#state").val();
        get_countries();

        // get_states(country_id);
        //get_cities(state);
        $("#countries").on('change', function() {
            //let country_id = $(this).val();
            //get_states(country_id);

            let country_id = $("#countries").find(':selected').data('id');
            get_states(country_id);
            // $("#country_code").val($(that).find(':selected').data('country_code'));
            // let state = $("#state").val();
            // get_cities(state);
        });
        $("#state").on('change', function() {
            let state_id = $("#state").find(':selected').data('id');
            get_cities(state_id);
            // let state = $("#state").val();
            // get_cities(state);
        });
        $(".remove_block_address").on('click', function() {

            let blockId = $(this).attr('data-value');
            $.ajax({
                url: "<?php echo base_url('account/remove_from_geo_block'); ?>",
                dataType: 'Json',
                method: 'post',
                data: {
                    'block_id': blockId
                },
                success: function(r) {

                    swal({
                        title: "Success!",
                        text: "Location removed successfully",
                        type: "success"
                    }).then(function() {
                        window.location = "<?php echo base_url() . 'account/geo_block'; ?>";
                    });
                },
                error: function() {
                    console.log('ajax error');
                }
            });
        });
    });


    function get_countries() {
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value='0'>Select Country</option>";
                $.each(r, function(key, val) {
                    //  console.log(val);
                    html += `<option value="${val.name}" data-id="${val.id}">${val.name}</option>`;
                });
                //console.log(html);
                $("#countries").html(html);
                // var selectedText = $("#countries option:selected").html();
                // $("#country2").val(selectedText);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

    function get_states(country_id) {
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/states.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value='0'>Select States</option>";
                $.each(r, function(key, val) {
                    // console.log(val);
                    if (val.country_id == country_id) {
                        html += `<option value="${val.name}" data-id="${val.id}">${val.name}</option>`;
                    }
                });
                //console.log(html);
                $("#state").html(html);

            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

    function get_cities(state_id) {
        //   console.log(state_id);
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value='0'>Select City</option>";
                $.each(r, function(key, val) {
                    console.log(val);
                    if (val.state_id == state_id) {
                        html += `<option value="${val.name}" data-id="${val.id}">${val.name}</option>`;
                    }
                });
                //console.log(html);
                $("#city").html(html);

            },
            error: function() {
                console.log('ajax error');
            }
        });
    }
</script>