<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
    .edit_model{
    	top: 0 !important;
    	z-index: 99999!important;
    }
    .modal .modal-footer{
    height: 115px !important;
    }
    .modal-footer button:first-child{
    	margin: 0 15px !important;
    	background: #ff0000b8;
    }
    .modal-footer button:last-child{
    	background: #008000c4;
    	margin-right: 15px !important;
    }
    .hide_file {
	    position: absolute;
	    z-index: 1000;
	    opacity: 0;
	    right: 0;
	    top: 0;
	    height: 100%;
	    font-size: 24px;
	    width: 100%;
	}
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
    .completePM {
        background-color: #008000 !important;
    }
      
    #upload-demo, #upload-demo-stream{
        width: 80%;
        height: 350px !important;
    }
    
    #pcropImagePop .croppie-container{
        width: 100%;
        height: 650px !important;
    }
    
    #item-img-output, #item-img-output2{
        height: 350px;
    }
    .modal {
        width: 800px !important;
    }
    #cropImagePop .modal-dialog {
        max-width: 1000px;
    }
    #streamImagePop .modal-dialog {
        max-width: 1000px;
    }
    /*.modal-dialog {
        max-width: auto !important;
        width: 800px !important;
    }*/
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>

<div class="app-content content site-setting friends-list site-premium">
  <div class="content-wrapper"> 
    <section class="model-setting-main">
      <div class="model-setting-cnt">
        <p>BIO</p>
        <form method="post" name="modelBio" id="modelBio">
        <div class="row">
          <div class="col edit-pro-rmainn">
            <?php
              $bio = (!empty($user['bio'])) ? $user['bio'] : '';
            ?>
            <label>Bio</label>
            <textarea placeholder="Tell your fans about yourself" name="bio" id="bio"><?= $bio ?></textarea>
            <span>Limited to 300 characters</span><br>
            <button type="submit" class="updaet-dd updateBio">Update Bio</button>
          </div>
        </div>
        </form>
      </div>
      <div class="model-setting-cnt mt-2">
        <p>AVATAR</p>
        <form method="post" name="modelAvatar" id="modelAvatar" enctype="multipart/form-data">
        <div class="row">
          <div class="col-12 edit-pro-rmainn">
            <?php
              $avatar = checkimage(5, $user['profile_image']);
            ?>
            <img src="<?= $avatar ?>"><br>
            <input type="file" class="mt-2 avatar" id="profile_image" name="profile_image"><br>
            <span>Recommended: 400px x 400px</span><br>
            <button type="submit" class="updaet-dd updateAvatar">Update Photo</button>
          </div>
        </div>
        </form>
      </div>
      <div class="model-setting-cnt mt-2">
        <p>header</p>
        <form method="post" name="modelHeader" id="modelHeader" enctype="multipart/form-data">
        <div class="row">
          <div class="col-12">
            <?php
              $headerImage = checkimage(6, $user['cover_image']);
            ?>
            <img src="<?= $headerImage ?>" width="100%" class="mb-2" id="header_image"><br>
            <span>Recommended: 1400 x 550px</span><br>
            <div class="image-upload">
                <!-- <label for="file-input">
                    <p class="file-upload-btn">Update Photo</p>
                </label> -->
                <!-- <input type="file" accept="image/*" name="image_input" id="image_input"/> -->

                <div class="file-upload-btn">
                    Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                    </div>

                <!-- <input type="file" class="item-img file center-block" accept="image/*" name="image_input" id="image_input" style="display: block;"> -->
                <!-- <input type="hidden" name="header" value="" id="header"/> -->
            </div>
          </div>
        </div>
        </form>
      </div>
      <div class="model-setting-cnt mt-2">
        <p>STREAM POSTER</p>
        <form method="post" name="modelStream" id="modelStream" enctype="multipart/form-data">
        <div class="row">
          <div class="col-12">
            <?php
              $streamImage = checkimage(7, $user['streamer_image']);
            ?>
            <img src="<?= $streamImage ?>" width="100%" class="mb-2" id="stream_image"><br>
            <span>Recommended: 1080 x 720px</span><br>
            <div class="image-upload">
                <!-- <label for="file-input">
                    <p class="file-upload-btn">Update Photo</p>
                </label>
                <input id="file-input" type="file"/> -->

                <div class="file-upload-btn">
                    Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="stream_input" id="stream_input">
                </div>

                <!-- <input type="file" class="item-img file center-block" accept="image/*" name="stream_input" id="stream_input" style="display: block;"> -->
                
            </div>
          </div>
        </div>
        </form>
      </div>
    </section>
  </div>
</div>


<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="max-width: auto !important; width: 800px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('user_lbl_img'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <label><b>Note : </b>
                    <small style="text-align: center;">Zoom the image in or out using the scrollbar at the bottom. If the mouse has a wheel, the wheel will also zoom. You can also adjust the image to the left, to the right, up or down.</small>
                </label>
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade edit_model" id="streamImagePop" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="max-width: auto !important; width: 800px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('user_lbl_img'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <label><b>Note : </b>
                    <small style="text-align: center;">Zoom the image in or out using the scrollbar at the bottom. If the mouse has a wheel, the wheel will also zoom. You can also adjust the image to the left, to the right, up or down.</small>
                </label>
                <div id="upload-demo-stream" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="cropStreamImageBtn" class="btn btn-primary">Crop</button>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.modal').modal({
            dismissible: true
        });
    });

    var $pImageCrop, tempFilename, rawImg, imageId, file_check, number_check, link;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');
                //$('#cropImagePop').modal('show');
                $("#cropImagePop").modal('open');
                rawImg = e.target.result;

                $pImageCrop.croppie('bind', {
                    url: rawImg,
                }).then(function () {
                    console.log('jQuery bind complete cover');
                });

            };
            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $pImageCrop = $('#upload-demo').croppie({
        viewport: {
            width: 300,
            height: 200,
            type: 'square'
        },
        //enforceBoundary: true,
        enforceBoundary: false,
        enableExif: true,
        showZoomer: true,
    });
    /*$('#cropImagePop').on('shown.bs.modal', function () {
        alert("sdf");
        $pImageCrop.croppie('bind', {
            url: rawImg,
        }).then(function () {
            console.log('jQuery bind complete cover');
        });
    });*/
    /*$('#cropImagePop').modal({
      onOpenEnd: function() {
        $pImageCrop.croppie('bind', {
            url: rawImg,
        }).then(function () {
            console.log('jQuery bind complete cover');
        });
      }
    });*/

    $('#cropImageBtn').on('click', function (ev) {
        $pImageCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: {width: 1400, height: 550}
        }).then(function (resp) {
            $('#header').val(resp);
            //$('#item-img-output').attr('src', resp);
            //$('.cover_picture').css('background-image', 'url(' + resp + ')');
            $('#header_image').attr('src', resp);
            //$('#cropImagePop').modal('hide');
            $('#cropImagePop').modal('close');
            upload_header_image();
        });
    });

    $('#image_input').change(function () {
        readFile(this, 0);
    });




$("#modelBio").validate({
    ignore: [],
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        var data = new FormData($('#modelBio')[0]);
        $(".updateBio").attr('disabled', true);
        $(".updateBio").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?php echo base_url('account/update_bio')?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                $(".updateBio").attr('disabled', false);
                $(".updateBio").removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });
    }
});

$("#modelAvatar").validate({
    ignore: [],
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        var data = new FormData($('#modelAvatar')[0]);
        $(".updateAvatar").attr('disabled', true);
        $(".updateAvatar").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?php echo base_url('account/update_avatar')?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                $(".updateAvatar").attr('disabled', false);
                $(".updateAvatar").removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });
    }
});


$(document).ready(function(){
  var maxchars = 300;
  $('#bio').keyup(function () {
      var tlength = $(this).val().length;
      $(this).val($(this).val().substring(0, maxchars));
      var tlength = $(this).val().length;
      remain = maxchars - parseInt(tlength);
      //$('#remain').text(remain);
  });
});



function upload_header_image() {
    var data = new FormData($('#modelHeader')[0]);
    $.ajax({
        type: 'post',
        data: data,
        dataType: "json",
        url: "<?php echo base_url('account/update_header')?>",
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
         // Show image container
         $("#loader").show();
        },
        success: function (r) {
            if (r.status == 200) {
                sType = getStatusText(r.status);
                sText = r.message;
                swal({
                    title: "Success",
                    text: sText,
                    icon: "success",
                });
                setInterval(function () {
                    location.reload();
                },1000);
            } else {
                sType = getStatusText(r.status);
                sText = r.message;
                swal({
                    title: "Error!",
                    text: sText,
                    icon: "error",
                });
            }
            },
            //complete: removeOverlay,
            complete:function(data){
             // Hide image container
             $("#loader").hide();
            }
    });
}


    var $ImageCrop, ptempFilename, prawImg, pimageId, pfile_check, pnumber_check, plink;

    function readFile1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.pupload-demo').addClass('ready');
                $('#streamImagePop').modal('open');
                prawImg = e.target.result;

                $ImageCrop.croppie('bind', {
                    url: prawImg,
                }).then(function () {
                    console.log('jQuery bind complete cover');
                });

            };
            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $ImageCrop = $('#upload-demo-stream').croppie({
        viewport: {
            width: 300,
            height: 200,
            type: 'square'
        },
        enforceBoundary: false,
        enableExif: true,
        showZoomer: true,
    });

    $('#cropStreamImageBtn').on('click', function (ev) {
        $ImageCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: {width: 500, height: 500}
        }).then(function (resp) {
            $('#stream').val(resp);
            //$('#item-img-output2').attr('src', resp);
            $('#stream_image').attr('src', resp);
            $('#streamImagePop').modal('close');
            upload_stream_image();
        });
    });

    $('#stream_input').change(function () {
        readFile1(this, 0);
    });


function upload_stream_image() {
    var data = new FormData($('#modelStream')[0]);
    $.ajax({
        type: 'post',
        data: data,
        dataType: "json",
        url: "<?php echo base_url('account/update_stream')?>",
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
         // Show image container
         $("#loader").show();
        },
        success: function (r) {
            if (r.status == 200) {
                sType = getStatusText(r.status);
                sText = r.message;
                swal({
                    title: "Success",
                    text: sText,
                    icon: "success",
                });
                setInterval(function () {
                    location.reload();
                },1000);
            } else {
                sType = getStatusText(r.status);
                sText = r.message;
                swal({
                    title: "Error!",
                    text: sText,
                    icon: "error",
                });
            }
            },
            //complete: removeOverlay,
            complete:function(data){
             // Hide image container
             $("#loader").hide();
            }
    });
}
</script>

<script>
	<script>
  $(document).ready(function(){
    $('#image_input,#stream_input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {
            
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                    return function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element 
                        $('#thumb-output').append(img); //append image to output element
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                }
            });
            
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });
  
  $(".remove").click(function (e) {
        e.preventDefault();
        data.splice(0, 1);
        $('#thumb-output a').eq(data.length).remove();
    });
});


</script>
</script>