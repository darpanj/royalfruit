<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/admin/image/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <link href="<?php echo base_url(ADM_CSS . 'circle_crop.css'); ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>

    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css">
    
    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
    <!-- Newly Added Starts Here -->
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>intlTelInput.css"> 
    <!-- wizrad -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_arrows.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_circles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_dots.css">
    <!-- Newly Added Ends Here -->
</head>

<body class="vertical-layout site-feed vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="site-wrap">
        <input type="hidden" id="usedId" value="<?php echo check_variable_value($user_id); ?>">
        <!-- main section starts -->
        <?php
        if ($header_panel) {
            if ($user_id > 0) {
                $this->load->view('front/header');
            }
        }
        ?>

        <!-- <script src="build/js/intlTelInput.js"></script>
        <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {

          utilsScript: "js/utils.js",
        });
        </script> -->

        <script src="<?php echo base_url() . FRONT_JS; ?>intlTelInput.js"></script>
          
        <div class="app-content content site-setting friends-list site-premium site-acnt-step ml-0">
            <div class="content-wrapper">
                <section class="account-step-main">
                  <div class="container">
                    <div class="mdl-acnt-main">
                      <div class="home-wizard">
                            <div id="smartwizard2">
                              <ul class="d-flex justify-content-around">
                                <li><a href="#step-1"><span class="step-num">1</span>Account</a></li>
                                <li><a href="#step-2"><span class="step-num">2</span>Verify</a></li>
                                <li><a href="#step-3"><span class="step-num">3</span>Review</a></li>
                                <li><a href="#step-4"><span class="step-num">4</span>Upload</a></li>
                              </ul>
                             <!--  <button class="sw-btn-next">jkkj</button> -->
                            <div>
                            <div id="step-1" class="mt-3">
                                <div class="stage1">
                                    <div class="account-step-1">
                                      <div class="d-flex align-items-center justify-content-between">
                                        <h3>Profile</h3>
                                        <span data-toggle="popover-hover" data-content="Please make sure to fill up the entire application, upload your ID, agree to the terms, and start uploading your content." data-original-title="Profile" title="" class="popper-pro">!</span>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Your Model Name<span>*</span></label>
                                          <input type="text" value="<?= $userData['name'] ?>" class="mb-0">
                                          <p>This is your model name that we'll show to your subscribers.</p>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Email Address<span>*</span></label>
                                          <input type="email" value="<?= $userData['email'] ?>" class="mb-0">
                                          <p>This is the email you'll use to login and where we'll send messages and notifications.</p>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Country of Residency<span>*</span></label>
                                          <div class="cus-select pre-cus-lb">
                                            <label class="w-100">

                                              <!-- <label for="countries" class="lead lead-jni">Countries</label> -->
                                              <select class="form-control mb-0" id="countries">
                                                  <option value='0'>Select Country</option>
                                              </select>
                                              <input type="hidden" id="country_name" name="country_name">
                                              </label>
                                          </div>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Phone Number<span>*</span></label>
                                          <form>
                                            <input id="phone" name="phone" type="tel" value="<?= $userData['country_code'] ?> <?= $userData['mobile'] ?>">
                                          </form>
                                          <p>This is the phone number we'll use if we need to reach you directly.</p>
                                      </div>
                                      <div class="row">
                                        <div class="col-12">
                                          <div class="d-flex align-items-center justify-content-between mt-3">
                                            <h3>Social Networks</h3>
                                            <span data-toggle="popover-hover" data-content="Your Social Networks (Instagram or Twitter is required). Without an active social media account your application will not be approved." data-original-title="SOCIAL NETWORKS" title="" class="popper-pro">!</span>
                                          </div>
                                          <p style="font-size: 12px;color: #151010;opacity: .5;margin-top: 6px;">We require models to enter their public Instagram accounts in the model application in order to -- verify your identity and see your social reach. If you don't have Instagram, please enter your Twitter account.</p>
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <label>Your Social Networks (Instagram or Twitter is required)<span>*</span></label>
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="<?php echo base_url() . FRONT_IMG; ?>twitter.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Twitter Username" class="mb-0" value="<?= $userData['twitter_url'] ?>">
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="<?php echo base_url() . FRONT_IMG; ?>instagram.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Instagram Username" class="mb-0" value="<?= $userData['instagram_url'] ?>">
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="<?php echo base_url() . FRONT_IMG; ?>tiktok.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Tiktok Username" class="mb-0" value="<?= $userData['tiktok_url'] ?>">
                                        </div>
                                      </div>
                                      <div class="set-mdl-main mb-2">
                                        <div class="d-flex align-items-center">
                                          <div class="social-icon-imgs">
                                            <img src="<?php echo base_url() . FRONT_IMG; ?>facebook.svg">
                                          </div>
                                          <span class="input-group-addon">@</span>
                                          <input type="text" placeholder="Facebook Username" class="mb-0" value="<?= $userData['facebook_url'] ?>">
                                        </div>
                                      </div>
                                      <div class="d-flex align-items-center justify-content-between mt-3">
                                        <h3>Settings</h3>
                                        <span data-toggle="popover-hover" data-content="Your Model settings. This will be your model username and subscription price. Your subscription price is how much a user will be charged to subscribe to you." data-original-title="Settings" title="" class="popper-pro">!</span>
                                      </div>
                                      <div class="set-mdl-main mt-1">
                                          <label>Username<span>*</span></label>
                                          <input type="text" value="Joya" class="mb-0" value="<?= $userData['username'] ?>">
                                          <p>This is your model username and will be used to view your profile URL. Must ONLY contain letters and numbers.</p>
                                      </div>
                                      <?php
                                          $subscriptionPriceArray = array(
                                                              '1' => '$10/month',
                                                              '2' => '$15/month',
                                                              '3' => '$20/month',
                                                              '4' => '$25/month',
                                                              );
                                      ?>
                                      <div class="set-mdl-main mt-1">
                                          <label>The Royal Fruit</label>
                                          <div class="cus-select pre-cus-lb">
                                            <label class="w-100">
                                              <select class="mb-0">
                                              <?php 
                                                  foreach($subscriptionPriceArray as $kSPA => $vSPA) {
                                                      $selectedSP = (!empty($userData) && $userData['subscription_price'] == $kSPA) ? 'selected' : '';
                                                      ?>
                                                      <option value="<?= $kSPA ?>" <?= $selectedSP ?>><?= $vSPA ?></option>
                                                      <?php
                                                  }
                                              ?>
                                              </select>
                                              <p>This is how much a user will be charged to subscribe to you.</p>
                                            </label>
                                          </div>
                                      </div>
                                      <button type="submit" class="btn btn-gradient btn-sub sw-btn-next" id="step2">Next - Verify</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
            </div>
        </div>

        <!-- main section ends -->
        <div class="site-feed">
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="modelId" id="modelId">
                            <input type="hidden" name="type" id="type">
                            <!--  // 0 for token purchase 
                            // 1 for follow -->
                            <input type="hidden" name="price" id="price">
                            <img src="" id="image_model_modal" width="50" alt="model">
                            <h2>Follow <span id="model_name_modal"></span></h2>
                            <h2>$<span id="model_price"></span>/month</h2>
                            <p>You'll get access to daily private content, direct messaging, and more!</p>
                            <button type="button" class="site-folow flw2" id="purchase_now">Continue</button>
                            <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a>
                            </p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url() . FRONT_THEME_ASSETS_VENDOR; ?>js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- Newly Added Starts Here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.smartWizard.js"></script>
    <!-- Newly Added Ends Here -->
    <script>
        <?php if (!empty($this->session->flashdata('success'))) { ?>
            sType = getStatusText(200);
            sText = '<?php echo $this->session->flashdata('success'); ?>';
            //Custom.myNotification(sType, sText);
            swal({
                title: "Success",
                text: sText,
                icon: "success",
            });
        <?php } ?>
        <?php if (!empty($this->session->flashdata('error'))) { ?>
            sType = getStatusText(412);
            sText = '<?php echo $this->session->flashdata('error'); ?>';
            // Custom.myNotification(sType, sText);
            swal({
                title: "Error!",
                text: sText,
                icon: "error",
            });
        <?php } ?>
    </script>
</body>

</html>
<script>
    $(document).ready(function() {
        let country_id = $("#countries").val();
        get_countries();
        $("#countries").on('select', function() {
            var selectedText = $("#countries option:selected").attr('data-value');
            $("#country_name").val(selectedText);
        });
    });

    function get_countries() {
      let selected = '';
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value='0'>Select Country</option>";
                $.each(r, function(key, val) {
                    if(val.name == '<?php echo $userData['country']; ?>') {
                      selected = 'selected'; 
                    } else {
                      selected = '';
                    }
                    html += `<option value="${val.id}" data-value="${val.name}" ${selected}>${val.name}</option>`;
                });
                $("#countries").html(html);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

</script>

<script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {

      utilsScript: "<?php echo base_url() . FRONT_JS; ?>utils.js",
    });
</script>
<script>
// SIDEBAR
$(document).ready(function(){
  $('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'right', // Choose the horizontal origin
      closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: false // Choose whether you can drag to open on touch screens
    }
  );
  // START OPEN
  $('.button-collapse').sideNav('hide');
});

</script>
<script>
$(function () {
  $('[data-toggle="popover-hover"]').popover({
    trigger: 'hover',
  })
})
  
</script>
<script>
  function format(item, state) {
  if (!item.id) {
    return item.text;
  }
  var countryUrl = "";
  var stateUrl = "https://oxguy3.github.io/flags/svg/us/";
  var url = state ? stateUrl : countryUrl;
  var img = $("<img>", {
    
    
  });
  var span = $("<span>", {
    text: " " + item.text
  });
  span.prepend(img);
  return span;
}

$(document).ready(function() {
  $("#countries").select2({
    templateResult: function(item) {
      return format(item, false);
    }
  });
  $("#state").select2({
    templateResult: function(item) {
      return format(item, true);
    }
  });
});

</script>

<!-- wizard -->
<script type="text/javascript">
        $(document).ready(function(){
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'arrows',
                    transitionEffect:'fade',
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            $('#smartwizard2').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: false
            });
        });
</script>

<script>
  $(document).ready(function(){
    $('#file-input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {
            
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                    return function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element 
                        $('#thumb-output').append(img); //append image to output element
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                }
            });
            
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });
  
  $(".remove").click(function (e) {
        e.preventDefault();
        data.splice(0, 1);
        $('#thumb-output a').eq(data.length).remove();
    });
});
</script>

<script>
$(document).ready(function () {
  $("#step4").click(function () {
      if ($("").hide()) {
          $("#ts1").show();
      }
  });
});
$(document).ready(function () {
  $(".back-sec").click(function () {
      if ($("").show()) {
          $("#ts1").hide();
      }
  });
});
</script>