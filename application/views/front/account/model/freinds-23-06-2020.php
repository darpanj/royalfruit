<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>

<div class="app-content content site-setting friends-list">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"></div>
        <section class="site-pr-video">
            <div class="container">
                <div class="row align-items-center">
                    <div class="w-100 friend-feed-main">
                        <h2><img src="<?php echo base_url() . FRONT_IMG; ?>/friends.png" width="18">&nbsp;&nbsp;Friends</h2>
                        <section class="prv-brw mt-2">
                            <div class="d-flex justify-content-between d-brw">
                                <p class="pl-2">This is your list of model friends</p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <!--<section class="model-slider">
            <div class="owl-slider">
                <div id="carousel" class="owl-carousel">
                    <div class="item">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                        <h4>Joya</h4>
                        <button>Add Friend</button>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                        <h4>Joya</h4>
                        <button>Add Friend</button>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                        <h4>Joya</h4>
                        <button>Add Friend</button>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                        <h4>Joya</h4>
                        <button>Add Friend</button>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                        <h4>Joya</h4>
                        <button>Add Friend</button>
                    </div>
                </div>
            </div>
        </section>-->
        
        <section class="model-slider">
            <?php if(!empty($getSuggestions)) { ?>
            <div class="owl-slider">
                <div id="carousel" class="owl-carousel">
                    <?php foreach($getSuggestions as $kGS => $vGS) { ?>
                    <div class="item">
                        <img src="<?php echo checkImage(3, $vGS['profile_image']); ?>">
                        <h4><?= $vGS['name'] ?></h4>
                        <button class="addFriend" id="<?= $vGS['id'] ?>">Add Friend</button>
                        
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php } else { ?>
                <h3 class="text-center">No models found</h3>
            <?php } ?>
        </section>

        <section class="frnd-list-main mt-2">
            <div class="prv-brw justify-content-center d-flex">
                <div class="d-flex justify-content-between d-brw">
                    <p class="pl-2">Friend list is empty. Visit a model profile to add a model as a friend.</p>
                </div>
        </section>

        <section class="subscribe-box site-subscriber">
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="subscriber-block">
                <img src="http://deluxcoder.com/beta/royal_fruit/image-thumb.php?w=336&amp;h=175&amp;zc=0&amp;q=100&amp;src=http://deluxcoder.com/beta/royal_fruit/themes/uploads/models/8/profile_pic/profile.jpg">
                <div class="sub-btn">
                  <h5>Joya</h5>
                  <button type="button">Block</button>
                  <button type="button">Unfriend</button>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="subscriber-block">
                <img src="http://deluxcoder.com/beta/royal_fruit/image-thumb.php?w=336&amp;h=175&amp;zc=0&amp;q=100&amp;src=http://deluxcoder.com/beta/royal_fruit/themes/uploads/models/8/profile_pic/profile.jpg">
                <div class="sub-btn">
                  <h5>Joya</h5>
                  <button type="button">Block</button>
                  <button type="button">Unfriend</button>
                </div>
              </div>
            </div>
          </div>
      </section>
    </div>
</div>
<script>
    // SIDEBAR
    $(document).ready(function() {
        $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'right', // Choose the horizontal origin
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: false // Choose whether you can drag to open on touch screens
        });
        // START OPEN
        $('.button-collapse').sideNav('hide');
    });
</script>
<script>
    jQuery("#carousel").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 3
            },

            1024: {
                items: 4
            },

            1366: {
                items: 4
            }
        }
    });
    
    $(document).on('click','.addFriend',function(){
        $("#loader").show();
        let model_id = $(this).attr('id');
        $(this).attr('disabled', true);
        $(this).addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: {'model_id': model_id},
            //dataType: "json",
            url: "<?= site_url('account/add_friend') ?>",
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                var r = JSON.parse(r);
                $(this).attr('disabled', false);
                $(this).removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 $("#loader").hide();
                }
        });

    });
</script>