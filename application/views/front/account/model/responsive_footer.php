<div class="pro-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-between pro-fmain">
                    <ul>
                        <li><a href="<?php echo base_url('home/buy_tokens'); ?>"><img src="<?php echo base_url() . FRONT_IMG; ?>money2.png" alt="money"></a></li>
                        <li><a href="<?php echo base_url('home/message'); ?>"><img src="<?php echo base_url() . FRONT_IMG; ?>message2.png" alt="message"></a></li>
                        <li><a href=""><img src="<?php echo base_url() . FRONT_IMG; ?>notification2.png" alt="notification"></a></li>
                        <li class="dropdown dropdown-user nav-item my-none"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online"><img src="<?php echo base_url() . FRONT_IMG; ?>avatar-s-19.png" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right"><a class="dropdown-item" href="#">
                                        <span class="avatar avatar-online"><img src="<?php echo base_url() . FRONT_IMG; ?>avatar-s-19.png" alt="avatar"><span class="user-name text-bold-700 ml-1"><?php echo $user_detail['name']; ?></span></span>
                                    </a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url('account/settings') ?>"> Edit Profile</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url('home/logout') ?>"> Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>