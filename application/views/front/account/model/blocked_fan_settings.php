<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
    .completePM {
        background-color: #008000 !important;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>

<div class="app-content content site-setting friends-list site-premium site-subscriber">
  <div class="content-wrapper"> 
    <section class="site-pr-video">
      <div class="container">
        <div class="row align-items-center">
          <div class="w-100 friend-feed-main">
          <h2 class="mt-0"><img src="./images/block-user.svg" width="18">&nbsp;&nbsp;Blocked Fan Settings</h2>
          <section class="prv-brw mt-2">
            <div class="d-flex justify-content-between d-brw">
              <p class="pl-2">This shows the status of friend requests you have made</p>
            </div>
          </section>
        </div>
      </div>
    </section>
    <?php if(!empty($blocked_fans)) { ?>
    <section class="subscribe-box">
      <div class="row">
        <?php foreach($blocked_fans as $kBF => $vBF) { ?>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="subscriber-block">
            <img src="<?= checkimage(1, $vBF['fan_image']) ?>">
            <div class="sub-btn">
              <h5><?= $vBF['fan_name'] ?></h5>
              <!-- <button type="button">Cancel</button> -->
              <button type="button" onClick="unblockFan(<?= $vBF['block_id'] ?>)">Unblock</button>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </section>
    <?php } else { ?>
    <section class="frnd-list-main mt-2">
      <div class="prv-brw justify-content-center d-flex">
        <div class="d-flex justify-content-between d-brw">
        <p class="pl-2">Blocked Fan list is empty.</p>
      </div>
    </section>
    <?php } ?>
  </div>
</div>

<script type="text/javascript">
    function unblockFan(block_id){
        $.ajax({
            type: 'post',
            data: { 'block_id': block_id},
            dataType: "json",
            url: "<?= site_url('account/unblock_fan') ?>",
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }
</script>