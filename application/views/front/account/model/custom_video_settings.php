<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/0.10.1/trix.css">
<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
    .completePM {
        background-color: #008000 !important;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>

<div class="app-content content site-setting friends-list site-premium site-cus-video-stngs site-your-content">
   <div class="content-wrapper">
      <section class="cus-video-stngs">
         <h4><i class="fa fa-video-camera" aria-hidden="true"></i>Custom Video Settings</h4>
         <form method="post" name="formAllowCustomVideoRequest" id="formAllowCustomVideoRequest">
            <?php $extra_data_id = (!empty($model_extra_data) && $model_extra_data['id'] > 0) ? $model_extra_data['id'] : 0; ?>
            <input type="hidden" name="id" id="id" value="<?= $extra_data_id ?>">
             <div class="video-box">
                <video class="mmCardVid" controls="">
                   <source src="<?= base_url() . FRONT_VIDEOS ?>video1.mp4" type="video/mp4">
                </video>
                <?php
                $checked = (!empty($model_extra_data) && $model_extra_data['is_allow_coustom_video_request'] == 1) ? 'checked' : '';
                $displayCss = (!empty($model_extra_data) && $model_extra_data['is_allow_coustom_video_request'] == 1) ? '' : 'style="display: none;"';
                ?>
                <label class="control control--checkbox mt-2 w-100">Allow custom videos request
                <input type="checkbox" name="is_allow_coustom_video_request" id="is_allow_coustom_video_request" <?= $checked ?>>
                <span class="control__indicator"></span>
                </label>
                <p class="chk-p">If checked, fans can request custom videos from you.</p>
                <div id="check-aft" class="set-mdl-main mt-2" <?= $displayCss ?>>
                    <?php
                    $price = (!empty($model_extra_data) && $model_extra_data['custom_video_price'] > 0.00) ? (double) $model_extra_data['custom_video_price'] : '';
                    ?>
                   <div class="d-flex justify-content-between">
                      <label>Min price</label>
                      <p>Max amount is $400</p>
                   </div>
                   <div class="d-flex align-items-center">
                      <span class="input-group-addon">$</span>
                      <input type="number" placeholder="" name="custom_video_price" id="custom_video_price" value="<?= $price ?>">
                   </div>
                    <?php
                    $rulesValue = (!empty($model_extra_data) && $model_extra_data['rules'] != "") ? $model_extra_data['rules'] : '';
                    ?>
                   <div class="position-relative">
                      <input type="hidden" name="rules" id="rules" placeholder="" value="<?= $rulesValue ?>">
                      <?php if($rulesValue == '') { ?>
                      <div class="placeholder" style="">
                         Example: Hey fans, I'm offering custom videos which include solo, toys, twerking, personalization of your name, sexy happy birthday wishes,
                         and more!
                         <br><br>
                         I also do shoots with other models whom I friends with.
                         <br><br>
                         I do NOT do anything video with guys but if you have a special request or fetish, hit me up and I will let you know if I will grant your request.
                      </div>
                      <?php } ?>
                      <trix-editor input="rules" id="trix-editor"></trix-editor>
                   </div>
                   <p class="gray small">
                      <b>Example:</b> Hey fans, I'm offering custom videos which include solo, toys, twerking, personalization of your name, sexy happy birthday wishes,
                      and more!
                      <br><br>
                      I also do shoots with other models whom I friends with.
                      <br><br>
                      I do NOT do anything video with guys but if you have a special request or fetish, hit me up and I will let you know if I will grant your request.
                   </p>
                   <?php
                    $deliveryTimeArray = array(
                                        'A day' => 'A day',
                                        '2-3 Days' => '2-3 Days',
                                        'A week' => 'A week',
                                        );
                   ?>
                   <div class="mt-2 mb-3 w-100">
                      <label>Delivery Time</label>
                      <select class="form-control w-100 asa" id="custom_video_delivery_time" name="custom_video_delivery_time">
                         <option value="" data-capital="Kabul">Time For Video to be ready</option>
                         <?php 
                            foreach($deliveryTimeArray as $kDT => $vDT) { 
                                $selected = ($vDT == $model_extra_data['custom_video_delivery_time']) ? 'selected="selected"' : '';
                         ?>
                            <option value="<?= $vDT ?>" <?= $selected ?>><?= $vDT ?></option>
                         <?php } ?>
                      </select>
                   </div>
                </div>
                <button type="submit" class="save-btn" id="button">Save</button>
             </div>
         </form>
      </section>
   </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/0.10.1/trix.js"></script> 

<script>
  $(document).ready(function () {
    $(".placeholder").click(function () {
        if ($("input").show()) {
            $(".placeholder").hide();
        }
    });
  });
</script>
<script>
$(document).ready(function(){
  $("#is_allow_coustom_video_request").click(function(){
    /*$("#check-aft").toggle();*/
    if($(this).is(":checked"))   
        $("#check-aft").show();
    else
        $("#check-aft").hide();
  });
});
</script>

<script type="text/javascript">
$("#formAllowCustomVideoRequest").validate({
    rules: {
        custom_video_price:{
          required: function (element) {
             if($("#is_allow_coustom_video_request").is(':checked')){
                 return true;                            
             }
             else
             {
                 return false;
             }  
          }  
        },
        rules:{
          required: function (element) {
             if($("#is_allow_coustom_video_request").is(':checked')){
                 return true;                            
             }
             else
             {
                 return false;
             }  
          }  
        },
        custom_video_delivery_time:{
          required: function (element) {
             if($("#is_allow_coustom_video_request").is(':checked')){
                 return true;                            
             }
             else
             {
                 return false;
             }  
          }  
        }
    },
    messages: {
        custom_video_price: {
            required: 'Price Must be Required!'
        },
        rules: {
            required: 'Rules Must be Required!',
        },
        custom_video_delivery_time: {
            required: 'Delivery Time Must be Required!',
        }
    },
    ignore: ":hidden",
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        let checkbox = $("#is_allow_coustom_video_request").val();
        let price = $("#custom_video_price").val();
        if(checkbox === 'on' && price > 400) {
            alert('Max amount is $400');
            return false;
        }
        var data = new FormData($('#formAllowCustomVideoRequest')[0]);
        $(".save-btn").attr('disabled', true);
        $(".save-btn").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?php echo base_url('account/update_custom_video_settings')?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                $(".save-btn").attr('disabled', false);
                $(".save-btn").removeClass('kt-spinner');
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }
});
</script>