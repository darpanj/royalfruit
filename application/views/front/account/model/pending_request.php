<div class="app-content content site-setting friends-list">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"></div>
        <section class="site-pr-video">
            <div class="container">
                <div class="row align-items-center">
                    <div class="w-100 friend-feed-main">
                        <h2><img src="<?php echo base_url() . FRONT_IMG; ?>/friends.png" width="18">&nbsp;&nbsp;Friend Requests</h2>
                        <section class="prv-brw mt-2">
                            <div class="d-flex justify-content-between d-brw">
                                <p class="pl-2">This shows the status of friend requests you have made</p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
<<<<<<< HEAD
        <?php if(!empty($sendRequests)) { ?>
        <section class="pending-sec">
            <div class="container">
                <div class="row">
                    <?php foreach($sendRequests as $kSR => $vSR) { ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo checkImage(3, $vSR['profile_image']); ?>">
                            <div class="pen-cnt pl-1">
                                <h4><?= $vSR['name'] ?></h4>
                                <p>
                                <?php
                                    $time_ago = strtotime($vSR['sent_request_date']);
                                    echo time_Ago($time_ago);
                                ?>  
                                </p>
                                <!-- <button>Cancel Request</button> -->
                                <button class="cancelFriend" id="<?= $vSR['request_id'] ?>">Cancel Request</button>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php } else { ?>
        <section class="frnd-list-main mt-2">
            <div class="prv-brw justify-content-center d-flex">
                <div class="d-flex justify-content-between d-brw">
                    <p class="pl-2">No pending friend requests.</p>
                </div>
        </section>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    
    $(document).on('click','.cancelFriend',function(){
        $("#loader").show();
        let request_id = $(this).attr('id');
        $(this).attr('disabled', true);
        $(this).addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: {'request_id': request_id},
            //dataType: "json",
            url: "<?= site_url('account/cancel_friend_request') ?>",
            /*cache: false,
            contentType: false,
            processData: false,*/
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                var r = JSON.parse(r);
                $(this).attr('disabled', false);
                $(this).removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });

    });
</script>
=======

        <section class="frnd-list-main mt-2">
            <div class="prv-brw justify-content-center d-flex">
                <div class="d-flex justify-content-between d-brw">
                    <p class="pl-2">No pending friend requests.</p>
                </div>
        </section>
        </section>

        <section class="pending-sec">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                            <div class="pen-cnt pl-1">
                                <h4>Mary Bellavita</h4>
                                <p>9 seconds ago</p>
                                <button>Cancel Request</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                            <div class="pen-cnt pl-1">
                                <h4>Mary Bellavita</h4>
                                <p>9 seconds ago</p>
                                <button>Cancel Request</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                            <div class="pen-cnt pl-1">
                                <h4>Mary Bellavita</h4>
                                <p>9 seconds ago</p>
                                <button>Cancel Request</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
