<div class="site-setting site-feed friend_feed">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Chart -->
            </div>
            <section class="site-pr-video">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="w-100 friend-feed-main">
                            <h2><i class="fa fa-rss" aria-hidden="true"></i>&nbsp;&nbsp;Friend Feed</h2>
                            <label class="control control--checkbox mt-2 w-100">Allow my friends to view my content feed. This
                                will allow my model friends to view my content for subscribers only.
                                <input type="checkbox" checked>
                                <span class="control__indicator"></span>
                            </label>
                            <section class="prv-brw mt-2">
                                <div class="d-flex justify-content-between d-brw">
                                    <p class="pl-2">This is the content of all your model friends and suggestions of some amazing
                                        models from our community </p>
                                </div>
                            </section>
                            <section class="feed-video">
                                <div class="container">
                                    <input type="hidden" id="postIds" value="0">
                                    <div class="row" id="post_list">

                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        /* for load modal first times */
        let limit = 9;
        let target = $("#post_list");
        let postIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'post_ids': postIds
            },
            dataType: "json",
            url: "<?php echo base_url('account/load_freinds_feed');  ?>",
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(r) {
                let html = '';
                $.each(r.feed_models, function(key, value) {
                    if (value.post_type == 0) {
                        html += make_image_post(value);
                    } else if (value.post_type == 1) {
                        html += make_video_post(value);
                    }
                    postIds += ',' + value.post_id;
                });

                $(target).html(html);
                $("#loader").hide();
                $('#postIds').val(postIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });
        /* when scroll than load more modals */
        $(window).scroll(function() {
            let postIds1 = $('#postIds').val();
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                let postIds = '0';
                $("#loader").show();
                let final_data = {
                    'limit': limit,
                    'post_ids': postIds1
                };
                $.ajax({
                    type: 'post',
                    data: final_data,
                    dataType: "json",
                    url: "<?php echo base_url('account/load_posts');  ?>",
                    beforeSend: function() {
                        $("#loader").show();
                    },
                    success: function(r) {
                        var html = '';
                        $.each(r.feed_models, function(key, value) {
                            if (value.post_type == 0) {
                                html += make_image_post(value);
                            } else if (value.post_type == 1) {
                                html += make_video_post(value);
                            }
                            postIds += ',' + value.post_id;
                        });
                        $(target).append(html);
                        $('#postIds').val(postIds1 + ',' + postIds);
                        $("#loader").hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });
    });

    function make_image_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        var html = '';
        html += `<div class='col-md-12 col-sm-12 col-lg-4 mt-2'>`;
        html += "<div class='feed-main'>";
        html += "<div class='position-relative overlay follow'>";
        html += `<img src="${SITE_IMG}uploads/models/${value.post_image}" alt='model'>`;
        html += "</div>";
        html += "<div class='d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1'>";
        html += "<div class='d-flex align-items-center'>";
        html += `<img src="${SITE_IMG}uploads/models/${value.profile_image}"  alt='model' width='30' height='30'>`;
        html += "<div class='pro-cnt'>";
        html += `<h2>${value.name}</h2>`;
        html += `<p class='m-0'>${teaser_date}</p>`;
        html += "</div>";
        html += "</div>";
        // html += `<ul id="category-tabs">
        //     <li>`;
        // if (value.is_fav_post == 1) {
        //     html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star"></i>
        //     </a>`;
        // } else {
        //     html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star-o"></i>
        //     </a>`;
        // }
        // html += `</a>
        //         </li>
        //     </ul>`;
        // html += `<button type='button' class='site-folow follow' data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow`;
        // html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        return html;
    }

    function make_video_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
    <div class="feed-main">
    <div class="video-wrapper">
        <video class="mmCardVid" controls>
            <source src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4" autoplay loop>
            secure connection could not be established
        </video>
    </div>

        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
            <div class="d-flex align-items-center">
                <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">
                <div class="pro-cnt">
                    <h2>${value.name}</h2>
                    <?php
                    // $dateDisplay = get_date_in_format(`${value.teaser_created_date}`);
                    ?>
                    <p class="m-0">${teaser_date}</p>
                </div>
            </div>"`;
        // <ul id="category-tabs">
        // <li>`;
        // if (value.is_fav_post == 1) {
        //     html += `<a href="javascript:void" id="remove_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star"></i>
        //     </a>`;
        // } else {
        //     html += `<a href="javascript:void" id="add_fav" data-post-id="${value.post_id}">
        //     <i class="fa fa-star-o"></i>
        //     </a>`;
        // }
        // html += `</a></li>
        //     </ul>
        //     <button type="button" class="site-folow follow" data-toggle='modal' data-target='#smallModal' data-model-id='${value.id}'>Follow</button>
        html += `</div>
    </div>
</div>`;
        return html;
    }
</script>