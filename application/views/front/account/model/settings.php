<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
    .completePM {
        background-color: #008000 !important;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>

    <div class="app-content content site-setting friends-list site-premium">
      <div class="content-wrapper"> 
        <section class="model-setting-main">
          <div class="model-setting-cnt">
            <p>Settings</p>

            <form method="post" name="modelSettings" id="modelSettings">
            <div class="row set-mdl-main">
                <?php
                $name = (!empty($user) && $user['name'] != '') ? $user['name'] : '';
                ?>
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Legal Name<span data-toggle="popover-hover" data-content="This is the name we use for your legal documents" data-original-title="" title="Legal Name">!</span></label>
              </div>
              <div class="col-lg-10 col-md-9">
                <input type="text" id="name" name="name" value="<?= $name ?>">
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Residency<span data-toggle="popover-hover" data-content="Do you live in the United States?" data-original-title="" title="Residency">!</span></label>
              </div>
              <div class="col-lg-10 col-md-9">
                <div class="cus-select pre-cus-lb">
                  <label class="w-100">
                    <?php
                        $residencyArray = array(
                                            'Us Resident' => 'Us Resident',
                                            'Non-US Resident' => 'Non-US Resident'
                                            )
                    ?>
                    <select name="residency" id="residency">
                        <?php 
                            foreach($residencyArray as $kRA => $vRA) {
                                $selected = (!empty($user) && $user['residency'] == $vRA) ? 'selected' : '';
                                ?>
                                <option value="<?= $vRA ?>" <?= $selected ?>><?= $vRA ?></option>
                                <?php
                            }
                        ?>
                    </select>
                  </label>
                </div>
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 ern-cnt col-md-3">
                <label>Mailing Address<span data-toggle="popover-hover" data-content="This is the mailing address associated with your bank information. We also use this address if we need to send you a package." data-original-title="" title="Mailing Address">!</span></label>
              </div>
                <?php
                $mAddress = (!empty($user) && $user['address'] != '') ? $user['address'] : '';
                ?>
              <div class="col-lg-10 col-md-9">
                <textarea name="address" id="address"><?= $mAddress ?></textarea>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main mt-2">
                <?php
                $mobile = (!empty($user) && $user['mobile'] != '') ? $user['mobile'] : '';
                ?>
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Phone<span data-toggle="popover-hover" data-content="This is the phone we'll use if we need to reach you directly" data-original-title="" title="Phone">!</span></label>
              </div>
              <div class="col-lg-10 col-md-9">
                <input type="text" name="mobile" id="mobile" value="<?= $mobile ?>">
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>Location</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <label class="control control--checkbox w-100">When enabled, your location will be displayed in your Profile Page.
                <?php
                $checkedDL = (!empty($user) && $user['display_location'] == '1') ? 'checked="checked"' : '';
                ?>
                  <input type="checkbox" name="display_location" id="display_location" <?= $checkedDL ?>>
                  <span class="control__indicator"></span>
                </label>
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>Country</label>
              </div>
              <div class="col-lg-10 col-md-9">
                  <!-- <select class="form-control" id="countries">
                      <option value="AF" data-capital="Kabul">United State</option>
                      <option value="AF" data-capital="Kabul">Afghanistan</option>
                      <option value="AX" data-capital="Mariehamn">Aland Islands</option>
                      <option value="AL" data-capital="Tirana">Albania</option>
                      <option value="DZ" data-capital="Algiers">Algeria</option>
                      <option value="AS" data-capital="Pago Pago">American Samoa</option>
                      <option value="AD" data-capital="Andorra la Vella">Andorra</option>
                      <option value="AO" data-capital="Luanda">Angola</option>
                      <option value="AI" data-capital="The Valley">Anguilla</option>
                      <option value="AG" data-capital="St. John's">Antigua and Barbuda</option>
                      <option value="AR" data-capital="Buenos Aires">Argentina</option>
                      <option value="AM" data-capital="Yerevan">Armenia</option>
                      <option value="AW" data-capital="Oranjestad">Aruba</option>
                      <option value="AU" data-capital="Canberra">Australia</option>
                      <option value="AT" data-capital="Vienna">Austria</option>
                      <option value="AZ" data-capital="Baku">Azerbaijan</option>
                      <option value="BS" data-capital="Nassau">Bahamas</option>
                      <option value="BH" data-capital="Manama">Bahrain</option>
                      <option value="BD" data-capital="Dhaka">Bangladesh</option>
                      <option value="BB" data-capital="Bridgetown">Barbados</option>
                      <option value="BY" data-capital="Minsk">Belarus</option>
                      <option value="BE" data-capital="Brussels">Belgium</option>
                      <option value="BZ" data-capital="Belmopan">Belize</option>
                      <option value="BJ" data-capital="Porto-Novo">Benin</option>
                      <option value="BM" data-capital="Hamilton">Bermuda</option>
                      <option value="BT" data-capital="Thimphu">Bhutan</option>
                      <option value="BO" data-capital="Sucre">Bolivia</option>
                      <option value="BQ" data-capital="Kralendijk">Bonaire, Sint Eustatius and Saba</option>
                      <option value="BA" data-capital="Sarajevo">Bosnia and Herzegovina</option>
                      <option value="BW" data-capital="Gaborone">Botswana</option>
                      <option value="BR" data-capital="Brasília">Brazil</option>
                      <option value="IO" data-capital="Diego Garcia">British Indian Ocean Territory</option>
                      <option value="BN" data-capital="Bandar Seri Begawan">Brunei Darussalam</option>
                      <option value="BG" data-capital="Sofia">Bulgaria</option>
                      <option value="BF" data-capital="Ouagadougou">Burkina Faso</option>
                      <option value="BI" data-capital="Bujumbura">Burundi</option>
                      <option value="CV" data-capital="Praia">Cabo Verde</option>
                      <option value="KH" data-capital="Phnom Penh">Cambodia</option>
                      <option value="CM" data-capital="Yaoundé">Cameroon</option>
                      <option value="CA" data-capital="Ottawa">Canada</option>
                      <option value="KY" data-capital="George Town">Cayman Islands</option>
                      <option value="CF" data-capital="Bangui">Central African Republic</option>
                      <option value="TD" data-capital="N'Djamena">Chad</option>
                      <option value="CL" data-capital="Santiago">Chile</option>
                      <option value="CN" data-capital="Beijing">China</option>
                      <option value="CX" data-capital="Flying Fish Cove">Christmas Island</option>
                      <option value="CC" data-capital="West Island">Cocos (Keeling) Islands</option>
                      <option value="CO" data-capital="Bogotá">Colombia</option>
                      <option value="KM" data-capital="Moroni">Comoros</option>
                      <option value="CK" data-capital="Avarua">Cook Islands</option>
                      <option value="CR" data-capital="San José">Costa Rica</option>
                      <option value="HR" data-capital="Zagreb">Croatia</option>
                      <option value="CU" data-capital="Havana">Cuba</option>
                      <option value="CW" data-capital="Willemstad">Curaçao</option>
                      <option value="CY" data-capital="Nicosia">Cyprus</option>
                      <option value="CZ" data-capital="Prague">Czech Republic</option>
                      <option value="CI" data-capital="Yamoussoukro">Côte d'Ivoire</option>
                      <option value="CD" data-capital="Kinshasa">Democratic Republic of the Congo</option>
                      <option value="DK" data-capital="Copenhagen">Denmark</option>
                      <option value="DJ" data-capital="Djibouti">Djibouti</option>
                      <option value="DM" data-capital="Roseau">Dominica</option>
                      <option value="DO" data-capital="Santo Domingo">Dominican Republic</option>
                      <option value="EC" data-capital="Quito">Ecuador</option>
                      <option value="EG" data-capital="Cairo">Egypt</option>
                      <option value="SV" data-capital="San Salvador">El Salvador</option>
                      <option value="GQ" data-capital="Malabo">Equatorial Guinea</option>
                      <option value="ER" data-capital="Asmara">Eritrea</option>
                      <option value="EE" data-capital="Tallinn">Estonia</option>
                      <option value="ET" data-capital="Addis Ababa">Ethiopia</option>
                      <option value="FK" data-capital="Stanley">Falkland Islands</option>
                      <option value="FO" data-capital="Tórshavn">Faroe Islands</option>
                      <option value="FM" data-capital="Palikir">Federated States of Micronesia</option>
                      <option value="FJ" data-capital="Suva">Fiji</option>
                      <option value="FI" data-capital="Helsinki">Finland</option>
                      <option value="MK" data-capital="Skopje">Former Yugoslav Republic of Macedonia</option>
                      <option value="FR" data-capital="Paris">France</option>
                      <option value="GF" data-capital="Cayenne">French Guiana</option>
                      <option value="PF" data-capital="Papeete">French Polynesia</option>
                      <option value="TF" data-capital="Saint-Pierre, Réunion">French Southern Territories</option>
                      <option value="GA" data-capital="Libreville">Gabon</option>
                      <option value="GM" data-capital="Banjul">Gambia</option>
                      <option value="GE" data-capital="Tbilisi">Georgia</option>
                      <option value="DE" data-capital="Berlin">Germany</option>
                      <option value="GH" data-capital="Accra">Ghana</option>
                      <option value="GI" data-capital="Gibraltar">Gibraltar</option>
                      <option value="GR" data-capital="Athens">Greece</option>
                      <option value="GL" data-capital="Nuuk">Greenland</option>
                      <option value="GD" data-capital="St. George's">Grenada</option>
                      <option value="GP" data-capital="Basse-Terre">Guadeloupe</option>
                      <option value="GU" data-capital="Hagåtña">Guam</option>
                      <option value="GT" data-capital="Guatemala City">Guatemala</option>
                      <option value="GG" data-capital="Saint Peter Port">Guernsey</option>
                      <option value="GN" data-capital="Conakry">Guinea</option>
                      <option value="GW" data-capital="Bissau">Guinea-Bissau</option>
                      <option value="GY" data-capital="Georgetown">Guyana</option>
                      <option value="HT" data-capital="Port-au-Prince">Haiti</option>
                      <option value="VA" data-capital="Vatican City">Holy See</option>
                      <option value="HN" data-capital="Tegucigalpa">Honduras</option>
                      <option value="HK" data-capital="Hong Kong">Hong Kong</option>
                      <option value="HU" data-capital="Budapest">Hungary</option>
                      <option value="IS" data-capital="Reykjavik">Iceland</option>
                      <option value="IN" data-capital="New Delhi">India</option>
                      <option value="ID" data-capital="Jakarta">Indonesia</option>
                      <option value="IR" data-capital="Tehran">Iran</option>
                      <option value="IQ" data-capital="Baghdad">Iraq</option>
                      <option value="IE" data-capital="Dublin">Ireland</option>
                      <option value="IM" data-capital="Douglas">Isle of Man</option>
                      <option value="IL" data-capital="Jerusalem">Israel</option>
                      <option value="IT" data-capital="Rome">Italy</option>
                      <option value="JM" data-capital="Kingston">Jamaica</option>
                      <option value="JP" data-capital="Tokyo">Japan</option>
                      <option value="JE" data-capital="Saint Helier">Jersey</option>
                      <option value="JO" data-capital="Amman">Jordan</option>
                      <option value="KZ" data-capital="Astana">Kazakhstan</option>
                      <option value="KE" data-capital="Nairobi">Kenya</option>
                      <option value="KI" data-capital="South Tarawa">Kiribati</option>
                      <option value="KW" data-capital="Kuwait City">Kuwait</option>
                      <option value="KG" data-capital="Bishkek">Kyrgyzstan</option>
                      <option value="LA" data-capital="Vientiane">Laos</option>
                      <option value="LV" data-capital="Riga">Latvia</option>
                      <option value="LB" data-capital="Beirut">Lebanon</option>
                      <option value="LS" data-capital="Maseru">Lesotho</option>
                      <option value="LR" data-capital="Monrovia">Liberia</option>
                      <option value="LY" data-capital="Tripoli">Libya</option>
                      <option value="LI" data-capital="Vaduz">Liechtenstein</option>
                      <option value="LT" data-capital="Vilnius">Lithuania</option>
                      <option value="LU" data-capital="Luxembourg City">Luxembourg</option>
                      <option value="MO" data-capital="Macau">Macau</option>
                      <option value="MG" data-capital="Antananarivo">Madagascar</option>
                      <option value="MW" data-capital="Lilongwe">Malawi</option>
                      <option value="MY" data-capital="Kuala Lumpur">Malaysia</option>
                      <option value="MV" data-capital="Malé">Maldives</option>
                      <option value="ML" data-capital="Bamako">Mali</option>
                      <option value="MT" data-capital="Valletta">Malta</option>
                      <option value="MH" data-capital="Majuro">Marshall Islands</option>
                      <option value="MQ" data-capital="Fort-de-France">Martinique</option>
                      <option value="MR" data-capital="Nouakchott">Mauritania</option>
                      <option value="MU" data-capital="Port Louis">Mauritius</option>
                      <option value="YT" data-capital="Mamoudzou">Mayotte</option>
                      <option value="MX" data-capital="Mexico City">Mexico</option>
                      <option value="MD" data-capital="Chișinău">Moldova</option>
                      <option value="MC" data-capital="Monaco">Monaco</option>
                      <option value="MN" data-capital="Ulaanbaatar">Mongolia</option>
                      <option value="ME" data-capital="Podgorica">Montenegro</option>
                      <option value="MS" data-capital="Little Bay, Brades, Plymouth">Montserrat</option>
                      <option value="MA" data-capital="Rabat">Morocco</option>
                      <option value="MZ" data-capital="Maputo">Mozambique</option>
                      <option value="MM" data-capital="Naypyidaw">Myanmar</option>
                      <option value="NA" data-capital="Windhoek">Namibia</option>
                      <option value="NR" data-capital="Yaren District">Nauru</option>
                      <option value="NP" data-capital="Kathmandu">Nepal</option>
                      <option value="NL" data-capital="Amsterdam">Netherlands</option>
                      <option value="NC" data-capital="Nouméa">New Caledonia</option>
                      <option value="NZ" data-capital="Wellington">New Zealand</option>
                      <option value="NI" data-capital="Managua">Nicaragua</option>
                      <option value="NE" data-capital="Niamey">Niger</option>
                      <option value="NG" data-capital="Abuja">Nigeria</option>
                      <option value="NU" data-capital="Alofi">Niue</option>
                      <option value="NF" data-capital="Kingston">Norfolk Island</option>
                      <option value="KP" data-capital="Pyongyang">North Korea</option>
                      <option value="MP" data-capital="Capitol Hill">Northern Mariana Islands</option>
                      <option value="NO" data-capital="Oslo">Norway</option>
                      <option value="OM" data-capital="Muscat">Oman</option>
                      <option value="PK" data-capital="Islamabad">Pakistan</option>
                      <option value="PW" data-capital="Ngerulmud">Palau</option>
                      <option value="PA" data-capital="Panama City">Panama</option>
                      <option value="PG" data-capital="Port Moresby">Papua New Guinea</option>
                      <option value="PY" data-capital="Asunción">Paraguay</option>
                      <option value="PE" data-capital="Lima">Peru</option>
                      <option value="PH" data-capital="Manila">Philippines</option>
                      <option value="PN" data-capital="Adamstown">Pitcairn</option>
                      <option value="PL" data-capital="Warsaw">Poland</option>
                      <option value="PT" data-capital="Lisbon">Portugal</option>
                      <option value="PR" data-capital="San Juan">Puerto Rico</option>
                      <option value="QA" data-capital="Doha">Qatar</option>
                      <option value="CG" data-capital="Brazzaville">Republic of the Congo</option>
                      <option value="RO" data-capital="Bucharest">Romania</option>
                      <option value="RU" data-capital="Moscow">Russia</option>
                      <option value="RW" data-capital="Kigali">Rwanda</option>
                      <option value="RE" data-capital="Saint-Denis">Réunion</option>
                      <option value="BL" data-capital="Gustavia">Saint Barthélemy</option>
                      <option value="SH" data-capital="Jamestown">Saint Helena, Ascension and Tristan da Cunha</option>
                      <option value="KN" data-capital="Basseterre">Saint Kitts and Nevis</option>
                      <option value="LC" data-capital="Castries">Saint Lucia</option>
                      <option value="MF" data-capital="Marigot">Saint Martin</option>
                      <option value="PM" data-capital="Saint-Pierre">Saint Pierre and Miquelon</option>
                      <option value="VC" data-capital="Kingstown">Saint Vincent and the Grenadines</option>
                      <option value="WS" data-capital="Apia">Samoa</option>
                      <option value="SM" data-capital="San Marino">San Marino</option>
                      <option value="ST" data-capital="São Tomé">Sao Tome and Principe</option>
                      <option value="SA" data-capital="Riyadh">Saudi Arabia</option>
                      <option value="SN" data-capital="Dakar">Senegal</option>
                      <option value="RS" data-capital="Belgrade">Serbia</option>
                      <option value="SC" data-capital="Victoria">Seychelles</option>
                      <option value="SL" data-capital="Freetown">Sierra Leone</option>
                      <option value="SG" data-capital="Singapore">Singapore</option>
                      <option value="SX" data-capital="Philipsburg">Sint Maarten</option>
                      <option value="SK" data-capital="Bratislava">Slovakia</option>
                      <option value="SI" data-capital="Ljubljana">Slovenia</option>
                      <option value="SB" data-capital="Honiara">Solomon Islands</option>
                      <option value="SO" data-capital="Mogadishu">Somalia</option>
                      <option value="ZA" data-capital="Pretoria">South Africa</option>
                      <option value="GS" data-capital="King Edward Point">South Georgia and the South Sandwich Islands</option>
                      <option value="KR" data-capital="Seoul">South Korea</option>
                      <option value="SS" data-capital="Juba">South Sudan</option>
                      <option value="ES" data-capital="Madrid">Spain</option>
                      <option value="LK" data-capital="Sri Jayawardenepura Kotte, Colombo">Sri Lanka</option>
                      <option value="PS" data-capital="Ramallah">State of Palestine</option>
                      <option value="SD" data-capital="Khartoum">Sudan</option>
                      <option value="SR" data-capital="Paramaribo">Suriname</option>
                      <option value="SJ" data-capital="Longyearbyen">Svalbard and Jan Mayen</option>
                      <option value="SZ" data-capital="Lobamba, Mbabane">Swaziland</option>
                      <option value="SE" data-capital="Stockholm">Sweden</option>
                      <option value="CH" data-capital="Bern">Switzerland</option>
                      <option value="SY" data-capital="Damascus">Syrian Arab Republic</option>
                      <option value="TW" data-capital="Taipei">Taiwan</option>
                      <option value="TJ" data-capital="Dushanbe">Tajikistan</option>
                      <option value="TZ" data-capital="Dodoma">Tanzania</option>
                      <option value="TH" data-capital="Bangkok">Thailand</option>
                      <option value="TL" data-capital="Dili">Timor-Leste</option>
                      <option value="TG" data-capital="Lomé">Togo</option>
                      <option value="TK" data-capital="Nukunonu, Atafu,Tokelau">Tokelau</option>
                      <option value="TO" data-capital="Nukuʻalofa">Tonga</option>
                      <option value="TT" data-capital="Port of Spain">Trinidad and Tobago</option>
                      <option value="TN" data-capital="Tunis">Tunisia</option>
                      <option value="TR" data-capital="Ankara">Turkey</option>
                      <option value="TM" data-capital="Ashgabat">Turkmenistan</option>
                      <option value="TC" data-capital="Cockburn Town">Turks and Caicos Islands</option>
                      <option value="TV" data-capital="Funafuti">Tuvalu</option>
                      <option value="UG" data-capital="Kampala">Uganda</option>
                      <option value="UA" data-capital="Kiev">Ukraine</option>
                      <option value="AE" data-capital="Abu Dhabi">United Arab Emirates</option>
                      <option value="GB" data-capital="London">United Kingdom</option>
                      <option value="UM" data-capital="Washington, D.C.">United States Minor Outlying Islands</option>
                      <option value="US" data-capital="Washington, D.C.">United States of America</option>
                      <option value="UY" data-capital="Montevideo">Uruguay</option>
                      <option value="UZ" data-capital="Tashkent">Uzbekistan</option>
                      <option value="VU" data-capital="Port Vila">Vanuatu</option>
                      <option value="VE" data-capital="Caracas">Venezuela</option>
                      <option value="VN" data-capital="Hanoi">Vietnam</option>
                      <option value="VG" data-capital="Road Town">Virgin Islands (British)</option>
                      <option value="VI" data-capital="Charlotte Amalie">Virgin Islands (U.S.)</option>
                      <option value="WF" data-capital="Mata-Utu">Wallis and Futuna</option>
                      <option value="EH" data-capital="Laayoune">Western Sahara</option>
                      <option value="YE" data-capital="Sana'a">Yemen</option>
                      <option value="ZM" data-capital="Lusaka">Zambia</option>
                      <option value="ZW" data-capital="Harare">Zimbabwe</option>
                    </select> -->
                <select class="form-control" id="countries" name="country">  
                <?php
                $countryVal = (!empty($user) && $user['country'] != '') ? $user['country'] : '';
                ?>
                    <option value=''>Select Country</option>
                    <!-- <option value='<?= $countryVal ?>'><?= $countryVal ?></option> -->
                </select>
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>State</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <!-- <input type="text" name=""> -->
                <select class="form-control" id="state" name="state">
                <?php
                $stateVal = (!empty($user) && $user['state'] != '') ? $user['state'] : '';
                ?>
                <?php 
                if(!empty($user) && $user['state'] != '') {
                    ?>
                    <option value='<?= $stateVal ?>'><?= $stateVal ?></option>
                <?php
                } else {
                ?>
                    <option value=''>Select States</option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>City</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <!-- <input type="text" name=""> -->
                <select class="form-control" id="city" name="city">
                <?php
                $cityVal = (!empty($user) && $user['city'] != '') ? $user['city'] : '';
                ?>
                <?php 
                if(!empty($user) && $user['city'] != '') {
                    ?>
                    <option value='<?= $cityVal ?>'><?= $cityVal ?></option>
                <?php
                } else {
                ?>
                    <option value="">Select City</option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 ern-cnt col-md-3">
                <label>Instagram<span data-toggle="popover-hover" data-content="You must have an active and public Instagram account with followers so we verify your identity and approve your application. We cannot approve your account without this information." data-original-title="" title="Instagram">!</span></label>
              </div>
                <?php
                $instagramURL = (!empty($user) && $user['instagram_url'] != '') ? $user['instagram_url'] : '';
                ?>
              <div class="col-lg-10 col-md-9 d-flex align-items-center">
                <span class="input-group-addon">@</span>
                <input type="text" name="instagram_url" id="instagram_url" value="<?= $instagramURL ?>">
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 ern-cnt col-md-3">
                <label>Twitter<span data-toggle="popover-hover" data-content="This is so we can see your existing social media following" data-original-title="" title="Twitter">!</span></label>
              </div>
                <?php
                $twitterURL = (!empty($user) && $user['twitter_url'] != '') ? $user['twitter_url'] : '';
                ?>
              <div class="col-lg-10 col-md-9 d-flex align-items-center">
                <span class="input-group-addon">@</span>
                <input type="text" name="twitter_url" id="twitter_url" value="<?= $twitterURL ?>">
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 ern-cnt col-md-3">
                <label>Facebook<span data-toggle="popover-hover" data-content="This is so we can see your existing social media following" data-original-title="" title="Facebook">!</span></label>
              </div>
                <?php
                $facebookURL = (!empty($user) && $user['facebook_url'] != '') ? $user['facebook_url'] : '';
                ?>
              <div class="col-lg-10 d-flex align-items-center col-md-9">
                <span class="input-group-addon">@</span>
                <input type="text" name="facebook_url" id="facebook_url" value="<?= $facebookURL ?>">
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Tiktok<span data-toggle="popover-hover" data-content="This is so we can see your existing social media following" data-original-title="" title="Tiktok">!</span></label>
              </div>
                <?php
                $tiktokURL = (!empty($user) && $user['tiktok_url'] != '') ? $user['tiktok_url'] : '';
                ?>
              <div class="col-lg-10 col-md-9 d-flex align-items-center">
                <span class="input-group-addon">@</span>
                <input type="text" name="tiktok_url" id="tiktok_url" value="<?= $tiktokURL ?>">
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Subscription Price<span data-toggle="popover-hover" data-content="This is how much a user will be charged to subscribe to you" data-original-title="" title="Subscription Price">!</span></label>
              </div>
            <?php
                $subscriptionPriceArray = array(
                                    '1' => '$10/month',
                                    '2' => '$15/month',
                                    '3' => '$20/month',
                                    '4' => '$25/month',
                                    );
            ?>
              <div class="col-lg-10 col-md-9">
                <div class="cus-select pre-cus-lb sdsdsds">
                  <label class="w-100">RoyalFruit
                    <select style="margin-top: 3px;" name="subscription_price" id="subscription_price">
                        <?php 
                            foreach($subscriptionPriceArray as $kSPA => $vSPA) {
                                $selectedSP = (!empty($user) && $user['subscription_price'] == $kSPA) ? 'selected' : '';
                                ?>
                                <option value="<?= $kSPA ?>" <?= $selectedSP ?>><?= $vSPA ?></option>
                                <?php
                            }
                        ?>           
                      <!-- <option value="7" selected="">$1 for 24 hours, $10/month</option>
                      <option value="8">$1 for 24 hours, $15/month</option>
                      <option value="9">$1 for 24 hours, $20/month</option>
                      <option value="10">$1 for 24 hours, $25/month</option>
                      <option value="11">$3 for 7 days, $10/month</option>
                      <option value="12">$3 for 7 days, $15/month</option>
                      <option value="13">$3 for 7 days, $20/month</option>
                      <option value="14">$3 for 7 days, $25/month</option>   
                      <option value="63">$1 for 30 days, $10/month</option> -->
                    </select>
                  </label>
                </div>
                <h3><b style="color: #737373;">Subscription Price</b> (<b style="color: #737373;">Important:</b> Changing your price will NOT affect the subscription price of past or existing subscribers.)</h3>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Premium Snap Price<span data-toggle="popover-hover" data-content="This is how much a user will be charged to follow you on snapchat" data-original-title="" title="Premium Snap Price">!</span></label>
              </div>
            <?php
                $premiumPriceArray = array(
                                    '1' => '$10/month',
                                    '2' => '$15/month',
                                    '3' => '$20/month',
                                    '4' => '$25/month',
                                    );
            ?>
              <div class="col-lg-10 col-md-9">
                <div class="cus-select pre-cus-lb sdsdsds">
                  <label class="w-100">RoyalFruit
                    <select style="margin-top: 3px;" name="premium_price" id="premium_price">
                        <?php 
                            foreach($premiumPriceArray as $kPPA => $vPPA) {
                                $selectedPP = (!empty($user) && $user['premium_price'] == $kPPA) ? 'selected' : '';
                                ?>
                                <option value="<?= $kPPA ?>" <?= $selectedPP ?>><?= $vPPA ?></option>
                                <?php
                            }
                        ?>
                      <!-- <option value="33">$15/month</option>
                      <option value="34">$20/month</option>
                      <option value="35">$30/month</option>
                      <option value="36">$40/month</option>
                      <option value="37">$50/month</option> -->
                    </select>
                  </label>
                </div>
                <h3><b style="color: #737373;">Premium Snap Price </b> (<b style="color: #737373;">Important:</b> Changing your price will NOT affect the subscription price of past or existing subscribers.)</h3>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Pricing<span data-toggle="popover-hover" data-content="This is how much a user will be charged to subscribe to you" data-original-title="" title="Pricing">!</span></label>
              </div>
            <?php
                $lsRateArray = array(
                                    '1' => '1 tokens/min',
                                    '2' => '2 tokens/min',
                                    '3' => '3 tokens/min',
                                    '4' => '4 tokens/min',
                                    );
            ?>
              <div class="col-lg-10 col-md-9">
                <div class="cus-select pre-cus-lb sdsdsds1">
                  <label class="w-100">
                    <select style="margin-top: 3px;margin-bottom: 4px;" name="livestreaming_rate" id="livestreaming_rate">
                        <?php 
                            foreach($lsRateArray as $kLRA => $vLRA) {
                                $selectedLR = (!empty($user) && $user['livestreaming_rate'] == $kLRA) ? 'selected' : '';
                                ?>
                                <option value="<?= $kLRA ?>" <?= $selectedLR ?>><?= $vLRA ?></option>
                                <?php
                            }
                        ?>
                      <!-- <option value="1">1 tokens/min</option>
                      <option value="2">2 tokens/min</option>
                      <option value="3">3 tokens/min</option>
                      <option value="4">4 tokens/min</option>
                      <option value="5">5 tokens/min</option>
                      <option value="6">6 tokens/min</option>
                      <option value="7">7 tokens/min</option>
                      <option value="8">8 tokens/min</option>
                      <option value="9">9 tokens/min</option>
                      <option value="10">10 tokens/min</option> -->
                    </select>
                  </label>
                  <h4><b style="color: #737373;">Livestream Viewing Rate</b> (tokens/minute)</h4>
                </div>
                <?php
                    $ls1ON1Array = array(
                                        '1' => '1 tokens/min',
                                        '2' => '2 tokens/min',
                                        '3' => '3 tokens/min',
                                        '4' => '4 tokens/min',
                                        );
                ?>
                <div class="cus-select pre-cus-lb sdsdsds1 mt-2">
                  <label class="w-100">
                    <select style="margin-top: 3px;margin-bottom: 4px;" name="livestreaming_rate_1_on_1" id="livestreaming_rate_1_on_1">
                        <?php 
                            foreach($ls1ON1Array as $kL1O1A => $vL1O1A) {
                                $selectedL1O1 = (!empty($user) && $user['livestreaming_rate_1_on_1'] == $kL1O1A) ? 'selected' : '';
                                ?>
                                <option value="<?= $kL1O1A ?>" <?= $selectedL1O1 ?> ><?= $vL1O1A ?></option>
                                <?php
                            }
                        ?>
                      <!-- <option value="1">1 tokens/min</option>
                        <option value="2">2 tokens/min</option>
                        <option value="3">3 tokens/min</option>
                        <option value="4">4 tokens/min</option>
                        <option value="5">5 tokens/min</option>
                        <option value="6">6 tokens/min</option>
                        <option value="7">7 tokens/min</option>
                        <option value="8">8 tokens/min</option>
                        <option value="9">9 tokens/min</option>
                        <option value="10">10 tokens/min</option>
                        <option value="11">11 tokens/min</option>
                        <option value="12">12 tokens/min</option>
                        <option value="13">13 tokens/min</option>
                        <option value="14">14 tokens/min</option>
                        <option value="15">15 tokens/min</option>
                        <option value="16">16 tokens/min</option>
                        <option value="17">17 tokens/min</option>
                        <option value="18">18 tokens/min</option>
                        <option value="19">19 tokens/min</option>
                        <option value="20">20 tokens/min</option> -->
                    </select>
                  </label>
                  <h4><b style="color: #737373;">1-on-1 Livestream With FanCam Rate</b> (tokens/minute)</h4>
                </div>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>Allow FanCam Requests</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <label class="control control--checkbox w-100"> When enabled, this will allow your fans to send you FanCam requests. When you accept, it will enable them to share their mic/camera and let you see and hear each other. This will apply the<b> 1-on-1 Livestream w/ FanCam token rate.</b>
                <?php
                $checkedAFCR = (!empty($user) && $user['allow_fancam_request'] == '1') ? 'checked="checked"' : '';
                ?>
                  <input type="checkbox" name="allow_fancam_request" id="allow_fancam_request" <?= $checkedAFCR ?>>
                  <span class="control__indicator"></span>
                </label>
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>Allow Stream Requests</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <?php
                $checkedASR = (!empty($user) && $user['allow_stream_request'] == '1') ? 'checked="checked"' : '';
                ?>
                <label class="control control--checkbox w-100"> When enabled, this will allow your fans to send you stream requests.</b>
                  <input type="checkbox" name="allow_stream_request" id="allow_stream_request" <?= $checkedASR ?>>
                  <span class="control__indicator"></span>
                </label>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>Email Notifications</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <div class="row no-gutters">
                  <div class="col-lg-6">
                    <?php
                    $checkedNS = (!empty($user) && $user['new_subscriber'] == '1') ? 'checked="checked"' : '';
                    ?>
                    <label class="control control--checkbox w-100">New Subscriber
                    <input type="checkbox" name="new_subscriber" id="new_subscriber" <?= $checkedNS ?>>
                    <span class="control__indicator"></span>
                    </label>
                  </div>
                  <div class="col-lg-6">
                    <?php
                    $checkedNCP = (!empty($user) && $user['new_content_purchase'] == '1') ? 'checked="checked"' : '';
                    ?>
                    <label class="control control--checkbox w-100">New Content Purchase
                    <input type="checkbox" name="new_content_purchase" id="new_content_purchase" <?= $checkedNCP ?>>
                    <span class="control__indicator"></span>
                    </label>
                  </div>
                  <div class="col-lg-6">
                    <?php
                    $checkedNPM = (!empty($user) && $user['new_private_message'] == '1') ? 'checked="checked"' : '';
                    ?>
                    <label class="control control--checkbox w-100">New Private Message
                    <input type="checkbox" name="new_private_message" id="new_private_message" <?= $checkedNPM ?>>
                    <span class="control__indicator"></span>
                    </label>
                  </div>
                  <div class="col-lg-6">
                    <?php
                    $checkedRLNU = (!empty($user) && $user['receive_latest_news_updates'] == '1') ? 'checked="checked"' : '';
                    ?>
                    <label class="control control--checkbox w-100">Receive latest news and updates
                    <input type="checkbox" name="receive_latest_news_updates" id="receive_latest_news_updates" <?= $checkedRLNU ?>>
                    <span class="control__indicator"></span>
                    </label>
                  </div>
                  <div class="col-lg-6">
                    <?php
                    $checkedNMFR = (!empty($user) && $user['new_model_friend_request'] == '1') ? 'checked="checked"' : '';
                    ?>
                    <label class="control control--checkbox w-100">New Model Friend Request
                    <input type="checkbox" name="new_model_friend_request" id="new_model_friend_request" <?= $checkedNMFR ?>>
                    <span class="control__indicator"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3 ern-cnt">
                <label>Premium Content<span data-toggle="popover-hover" data-content="" data-original-title="" title="When checked, your Premium Videos will be made available in the Premium Video Store (RoyalFruitVids)">!</span></label>
              </div>
              <div class="col-lg-10 col-md-9">
                <?php
                $checkedAPVS = (!empty($user) && $user['available_in_premium_video_store'] == '1') ? 'checked="checked"' : '';
                ?>
                <label class="control control--checkbox w-100">Available in Premium Video Store (RoyalFruitVids)</b>
                  <input type="checkbox" name="available_in_premium_video_store" id="available_in_premium_video_store" <?= $checkedAPVS ?>>
                  <span class="control__indicator"></span>
                </label>
              </div>
            </div>
            <div class="border-hhuh"></div>
            <div class="row set-mdl-main">
              <div class="col-lg-2 col-md-3">
                <label>Model Friend Feature</label>
              </div>
              <div class="col-lg-10 col-md-9">
                <?php
                $checkedAMF = (!empty($user) && $user['allow_my_friends'] == '1') ? 'checked="checked"' : '';
                ?>
                <label class="control control--checkbox w-100">Allow my friends to view my content feed. This will allow my model friends to view my content for subscribers only.
                  <input type="checkbox" name="allow_my_friends" id="allow_my_friends" <?= $checkedAMF ?>>
                  <span class="control__indicator"></span>
                </label>
              </div>
            </div>
            <div class="row set-mdl-main">
              <div class="col">
                <!-- <button type="button">Update</button> -->
                <button type="submit">Update</button>
              </div>
            </div>
            </form>


          </div>
        </section>
      </div>
    </div>

<script>
    $(document).ready(function() {
        let country_id = $("#countries").val();
        let state = $("#state").val();
        get_countries();
        $("#countries").on('change', function() {
            let country_id = $("#countries").find(':selected').data('id');
            get_states(country_id);
            $("#city").html('');
            $("#city").html('<option value="">Select City</option');
        });
        $("#state").on('change', function() {
            let state_id = $("#state").find(':selected').data('id');
            get_cities(state_id);
        });
    });

    var selected = '';
    function get_countries() {
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value=''>Select Country</option>";
                $.each(r, function(key, val) {
                    if('<?= $countryVal ?>' !== '' && '<?= $countryVal ?>' == val.name) {
                        selected = 'selected';
                    } else {
                        selected = '';
                    }
                    html += `<option value="${val.name}" data-id="${val.id}" ${selected} >${val.name}</option>`;
                });
                $("#countries").html(html);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

    function get_states(country_id) {
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/states.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value=''>Select States</option>";
                $.each(r, function(key, val) {
                    if (val.country_id == country_id) {
                        html += `<option value="${val.name}" data-id="${val.id}">${val.name}</option>`;
                    }
                });
                $("#state").html(html);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

    function get_cities(state_id) {
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value=''>Select City</option>";
                $.each(r, function(key, val) {
                    console.log(val);
                    if (val.state_id == state_id) {
                        html += `<option value="${val.name}" data-id="${val.id}">${val.name}</option>`;
                    }
                });
                $("#city").html(html);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }
</script>

<script type="text/javascript">
    

$("#modelSettings").validate({
    rules: {
        name: {
            required: true
        },
        mobile: {
          required: true,
          number: true
        }
    },
    messages: {
        name: {
            required: 'Legal Name Must be Required!'
        },
        mobile: {
            required: 'Mobile Must be Required!',
        }
    },
    ignore: ":hidden",
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        var data = new FormData($('#modelSettings')[0]);
        $(".payB").attr('disabled', true);
        $(".payB").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?php echo base_url('account/update_model_settings')?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                $(".payB").attr('disabled', false);
                $(".payB").removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });
    }
});

</script>