<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/admin/image/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <link href="<?php echo base_url(ADM_CSS . 'circle_crop.css'); ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>

    <!-- <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script> -->
    <!-- <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script> -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css">
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
    <!-- Newly Added Starts Here -->
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>intlTelInput.css"> 
    <!-- wizrad -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_arrows.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_circles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_dots.css">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <!-- Newly Added Ends Here -->
</head>

<body class="vertical-layout site-feed vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="site-wrap">
        <input type="hidden" id="usedId" value="<?php echo check_variable_value($user_id); ?>">
        <!-- main section starts -->
        <?php
        if ($header_panel) {
            if ($user_id > 0) {
                $this->load->view('front/header');
            }
        }
        ?>
        <script src="<?php echo base_url() . FRONT_JS; ?>intlTelInput.js"></script>
        
        <div class="app-content content site-setting friends-list site-premium site-acnt-step ml-0">
          <div class="content-wrapper">
            <section class="account-step-main">
              <div class="container">
                <div class="mdl-acnt-main">
                  <div class="home-wizard">
                        <div id="smartwizard2">
                          <ul class="d-flex justify-content-around">
                            <li><a href="javascript:;"><span class="step-num">1</span>Account</a></li>
                            <li><a href="javascript:;"><span class="step-num">2</span>Verify</a></li>
                            <li class="active"><a href="#step-3"><span class="step-num">3</span>Review</a></li>
                            <li><a href="javascript:;"><span class="step-num">4</span>Upload</a></li>
                          </ul>
                         <!--  <button class="sw-btn-next">jkkj</button> -->
                        <div>
                        <div id="step-3" class="mt-3 wiz-step3 wiz-step2">
                          <div class="review-maiin">
                            <div class="d-flex align-items-center justify-content-between account-step-1 w-100">
                              <h3>Review Documents</h3>
                              <span data-toggle="popover-hover" data-content="This is a summary of your uploaded IDs and ID information." data-original-title="Review Documents" title="" class="popper-pro">!</span>
                            </div>
                            <div class="border-btm w-100"></div>
                            <div class="photo-id-upd mt-2">
                              <div class="d-flex">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <p>Photo ID</p>   
                              </div>
                              <div class="d-flex view-btn">
                              <?php
                                    if(!empty($userData) && $userData['document_photo_id'] != '') {
                                      $openPhotoIDImage = ($userData['document_photo_id'] != '') ? base_url().MODEL_UPD . $userData['document_photo_id'] : 'javascript:;';
                                      $tBlankPhotoID = ($userData['document_photo_id'] != '') ? 'target="_blank"' : '';
                              ?>
                                <a href="<?= $openPhotoIDImage ?>" <?= $tBlankPhotoID ?>><button type="button">View</button></a>
                              <?php } ?>
                                  <form method="post" name="PhotoIDUpload" id="PhotoIDUpload" enctype="multipart/form-data">
                                      <div class="image-upload w-100">
                                          <div class="file-upload-btn">
                                            Update<input type="file" class="item-img file center-block hide_file" accept="image/*" name="document_photo_id" id="document_photo_id">
                                          </div>
                                      </div>
                                  </form>
                              </div>
                            </div>
                            <div class="photo-id-upd">
                              <div class="d-flex">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <p>Selfie with ID</p>   
                              </div>
                              <div class="d-flex view-btn">
                              <?php
                                    if(!empty($userData) && $userData['document_id_verification_selfie'] != '') {
                                    $openVerificationImage = ($userData['document_id_verification_selfie'] != '') ? base_url().MODEL_UPD . $userData['document_id_verification_selfie'] : 'javascript:;';
                                    $tBlankVerificationImage = ($userData['document_id_verification_selfie'] != '') ? 'target="_blank"' : '';
                              ?>
                                <a href="<?= $openVerificationImage ?>" <?= $tBlankVerificationImage ?>><button type="button">View</button></a>
                              <?php } ?>
                                <form method="post" name="idVerificationSelfieUpload" id="idVerificationSelfieUpload" enctype="multipart/form-data">
                                  <div class="image-upload w-100">
                                    <div class="file-upload-btn">
                                      Update<input type="file" class="item-img file center-block hide_file" accept="image/*" name="document_id_verification_selfie" id="document_id_verification_selfie">
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            <div class="border-btm w-100 p-0"></div>
                            <div class="photo-id-upd mt-1">
                              <div class="d-flex">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <p>ID Information</p>   
                              </div>
                              <div class="d-flex view-btn">
                                <div class="image-upload w-100">
                                  <div class="res-upload-btn mt-0">
                                    <a href="<?= site_url('account/document_verification_step_verify') ?>">Update<input type="button" class="item-img file center-block hide_file"></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php
                                $govtIssuedIDType = array(
                                                    '1' => "Driver's License",
                                                    '2' => 'State ID',
                                                    '3' => 'Military ID',
                                                    '4' => 'Non-US Passport',
                                                    '5' => 'US Passport',
                                                    );
                              if(!empty($userData) && $userData['government_issued_id_type'] != 0) {
                                $government_issued_id_type = $govtIssuedIDType[$userData['government_issued_id_type']];
                              } else {
                                $government_issued_id_type = 'N/A';
                              }
                            ?>
                            <div class="gov-cnt">
                              <p>Government Issued ID type:</p>
                              <span><?= $government_issued_id_type ?></span>
                            </div>
                            <?php
                              if(!empty($userData) && $userData['government_id_issued_by_state'] != "") {
                                $government_id_issued_by_state = $userData['government_id_issued_by_state'];
                              } else {
                                $government_id_issued_by_state = 'N/A';
                              }
                            ?>
                            <div class="gov-cnt mt-1">
                              <p>Issued By (State/Province)</p>
                              <span><?= $government_id_issued_by_state ?></span>
                            </div>
                            <?php
                              if(!empty($userData) && $userData['government_id_number'] != "") {
                                $government_id_number = $userData['government_id_number'];
                              } else {
                                $government_id_number = 'N/A';
                              }
                            ?>
                            <div class="gov-cnt mt-1">
                              <p>ID Number</p>
                              <span><?= $government_id_number ?></span>
                            </div>
                              <?php
                                  $months = array(
                                      '01' => 'January',
                                      '02' => 'February',
                                      '03' => 'March',
                                      '04' => 'April',
                                      '05' => 'May',
                                      '06' => 'June',
                                      '07' => 'July ',
                                      '08' => 'August',
                                      '09' => 'September',
                                      '10' => 'October',
                                      '11' => 'November',
                                      '12' => 'December',
                                  );
                              ?>
                            <?php
                              if(!empty($userData)) {
                                if($userData['no_expiration'] == 1) {
                                  $expiration = "No Expiration";
                                } else {
                                  $expiration = $months[$userData['government_id_expiration_month']].' '.$userData['government_id_expiration_day'].', '.$userData['government_id_expiration_year'];
                                }
                              } else {
                                $expiration = 'N/A';
                              }
                            ?>
                            <div class="gov-cnt mt-1">
                              <p>ID Expiration:</p>
                              <span><?= $expiration ?></span>
                            </div>
                            <?php
                              if(!empty($userData) && $userData['legal_name'] != "") {
                                $legal_name = $userData['legal_name'];
                              } else {
                                $legal_name = 'N/A';
                              }
                            ?>
                            <div class="gov-cnt mt-1">
                              <p>Full Legal Name:</p>
                              <span><?= $legal_name ?></span>
                            </div>
                            <?php
                              if(!empty($userData)) {
                                if($userData['dob_month'] != "" && $userData['dob_day'] != "" && $userData['dob_year'] != "") {
                                  $dob = $months[$userData['dob_month']].' '.$userData['dob_day'].', '.$userData['dob_year'];
                                } else {
                                  $dob = 'N/A';
                                }
                              } else {
                                $dob = 'N/A';
                              }
                            ?>
                            <div class="gov-cnt mt-1">
                              <p>Date Of Birth:</p>
                              <span><?= $dob ?></span>
                            </div>
                            <?php
                              $alias_one = (!empty($userData) && $userData['alias_one'] != '') ? $userData['alias_one'] : '';
                              $alias_two = (!empty($userData) && $userData['alias_two'] != '') ? $userData['alias_two'] : '';
                            ?>
                            <div class="border-btm w-100"></div>
                            <div class="photo-id-upd mt-1">
                              <div class="d-flex">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <p style="font-weight: normal;">Alias:</p><br><?= $alias_one ?><br><?= $alias_two ?>
                              </div>
                            </div>
                            <div class="border-btm w-100"></div>
                            <div class="d-flex align-items-center justify-content-between account-step-1 w-100 mt-2">
                              <h3>Terms & Conditions</h3>
                              <span data-toggle="popover-hover" data-content="These are the terms and conditions you need to agree on to become a model." data-original-title="Terms & Conditions" title="" class="popper-pro">!</span>
                            </div>

                            <form action="<?php echo base_url('account/document_verification_step_review'); ?>" method="post" id="form_step_review">
                              <?php
                                $checkedAgreementModel = (!empty($userData) && $userData['agreement_model'] == 1) ? 'checked' : '';
                              ?>
                              <div class="set-mdl-main agree-span mt-1">
                                <label>Model Agreement<span>*</span></label>
                                  <label class="control control--checkbox w-100"> By checking this checkbox, I hereby indicate that I have read and understood and agree to be bound by the <a href="model_agreement.html" target="_blank">The Royal Fruit Model Agreement</a>
                                    <input type="checkbox" name="agreement_model" id="agreement_model" <?= $checkedAgreementModel ?>>
                                    <span class="control__indicator"></span>
                                </label>
                              </div>
                              <?php
                                $checkedAgreement2257 = (!empty($userData) && $userData['agreement_2257'] == 1) ? 'checked' : '';
                              ?>
                              <div class="set-mdl-main agree-span mt-1">
                                <label>2257 Agreement<span>*</span></label>
                                  <label class="control control--checkbox w-100"> By checking this checkbox, I hereby indicate that I have read and understood and agree to be bound by the <a href="model_2257_agreement.html" target="_blank">The Royal Fruit 2257 Self-Production Records Keeping Compliance Agreement</a>
                                    <input type="checkbox" name="agreement_2257" id="agreement_2257" <?= $checkedAgreement2257 ?>>
                                    <span class="control__indicator"></span>
                                  </label>
                              </div>
                              <!-- <div class="set-mdl-main agree-span mt-1">
                                <label>Foreign Performer Agreement*</label>
                                  <label class="control control--checkbox w-100"> By checking this checkbox, I hereby indicate that I have read and understood and agree to be bound by the <a href="model_foreign_agreement.html" target="_blank">The Royal Fruit Foreign Performer Agreement</a>
                                    <input type="checkbox">
                                    <span class="control__indicator"></span>
                                </label>
                              </div> -->
                              <div class="d-flex sec-stepn-btn  sec-stepn-btn2 justify-content-between">
                                <a href="<?= site_url('account/document_verification_step_verify') ?>"><button type="button" class="btn btn-gradient btn-sub">Back</button></a>
                                <button type="submit" class="btn btn-gradient btn-sub submit-btn-ac acc-vrf-nxt">Submit My Application</button>
                              </div>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </section>
          </div>
        </div>

        <!-- main section ends -->
        <div class="site-feed">
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="modelId" id="modelId">
                            <input type="hidden" name="type" id="type">
                            <!--  // 0 for token purchase 
                            // 1 for follow -->
                            <input type="hidden" name="price" id="price">
                            <img src="" id="image_model_modal" width="50" alt="model">
                            <h2>Follow <span id="model_name_modal"></span></h2>
                            <h2>$<span id="model_price"></span>/month</h2>
                            <p>You'll get access to daily private content, direct messaging, and more!</p>
                            <button type="button" class="site-folow flw2" id="purchase_now">Continue</button>
                            <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a>
                            </p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <!-- <script src="<?php echo base_url() . FRONT_THEME_ASSETS_VENDOR; ?>js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- Newly Added Starts Here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.smartWizard.js"></script>
    <!-- Newly Added Ends Here -->
    <script>
        <?php if (!empty($this->session->flashdata('success'))) { ?>
            sType = getStatusText(200);
            sText = '<?php echo $this->session->flashdata('success'); ?>';
            //Custom.myNotification(sType, sText);
            swal({
                title: "Success",
                text: sText,
                icon: "success",
            });
        <?php } ?>
        <?php if (!empty($this->session->flashdata('error'))) { ?>
            sType = getStatusText(412);
            sText = '<?php echo $this->session->flashdata('error'); ?>';
            // Custom.myNotification(sType, sText);
            swal({
                title: "Error!",
                text: sText,
                icon: "error",
            });
        <?php } ?>
    </script>
</body>

</html>


<script>
$(function () {
  $('[data-toggle="popover-hover"]').popover({
    trigger: 'hover',
  })
})
  
</script>

<!-- wizard -->
<script type="text/javascript">
        $(document).ready(function(){
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'arrows',
                    transitionEffect:'fade',
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            $('#smartwizard2').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: false
            });
        });
</script>


<script type="text/javascript">

$(document).ready(function () {

    $('#document_photo_id').change(function(){
      ajaxPhotoIDUpload();
    });

    function ajaxPhotoIDUpload(){
        var data = new FormData($('#PhotoIDUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_photo_id') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }

    $('#document_id_verification_selfie').change(function(){
      ajaxIDVerificationSelfieUpload();
    });

    function ajaxIDVerificationSelfieUpload(){
        var data = new FormData($('#idVerificationSelfieUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_id_verification_selfie') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }

});
</script>

<script>
    $(document).ready(function () {
        $("#form_step_review").validate({
            rules: {
                agreement_model: {required: true},
                agreement_2257: {required: true},
            },
            messages: {
                agreement_model: {required: "Please agree to the model agreement"},
                agreement_2257: {required: "Please agree to the model 2257 agreement"},
            },
            ignore: ":hidden"
        });
    });
</script>