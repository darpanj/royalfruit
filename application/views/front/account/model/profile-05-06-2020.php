<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
    .completePM {
        background-color: #008000 !important;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>

<div class="app-content content site-setting friends-list site-premium">
  <div class="content-wrapper"> 
    <section class="model-setting-main">
      <div class="model-setting-cnt">
        <p>BIO</p>
        <form method="post" name="modelBio" id="modelBio">
        <div class="row">
          <div class="col edit-pro-rmainn">
            <?php
              $bio = (!empty($user['bio'])) ? $user['bio'] : '';
            ?>
            <label>Bio</label>
            <textarea placeholder="Tell your fans about yourself" name="bio" id="bio"><?= $bio ?></textarea>
            <span>Limited to 300 characters</span><br>
            <button type="submit" class="updaet-dd updateBio">Update Bio</button>
          </div>
        </div>
        </form>
      </div>
      <div class="model-setting-cnt mt-2">
        <p>AVATAR</p>
        <form method="post" name="modelAvatar" id="modelAvatar" enctype="multipart/form-data">
        <div class="row">
          <div class="col-12 edit-pro-rmainn">
            <?php
              $avatar = checkimage(5, $user['profile_image']);
            ?>
            <img src="<?= $avatar ?>"><br>
            <input type="file" class="mt-2 avatar" id="profile_image" name="profile_image"><br>
            <span>Recommended: 400px x 400px</span><br>
            <button type="submit" class="updaet-dd updateAvatar">Update Photo</button>
          </div>
        </div>
        </form>
      </div>
      <div class="model-setting-cnt mt-2">
        <p>header</p>
        <form method="post" name="modelSettings" id="modelSettings">
        <div class="row">
          <div class="col-12">
            <img src="<?php echo base_url() . FRONT_IMG; ?>slider6.png" width="100%" class="mb-2"><br>
            <span>Recommended: 1400 x 550px</span><br>
            <div class="image-upload">
                <label for="file-input">
                    <p class="file-upload-btn">Update Photo</p>
                </label>
                <input id="file-input" type="file"/>
            </div>
          </div>
        </div>
      </div>
      <div class="model-setting-cnt mt-2">
        <p>STREAM POSTER</p>
        <div class="row">
          <div class="col-12">
            <img src="<?php echo base_url() . FRONT_IMG; ?>slider6.png" width="100%" class="mb-2"><br>
            <span>Recommended: 1080 x 720px</span><br>
            <div class="image-upload">
                <label for="file-input">
                    <p class="file-upload-btn">Update Photo</p>
                </label>
                <input id="file-input" type="file"/>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<script type="text/javascript">


$("#modelBio").validate({
    ignore: [],
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        var data = new FormData($('#modelBio')[0]);
        $(".updateBio").attr('disabled', true);
        $(".updateBio").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?php echo base_url('account/update_bio')?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                $(".updateBio").attr('disabled', false);
                $(".updateBio").removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });
    }
});

$("#modelAvatar").validate({
    ignore: [],
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        var data = new FormData($('#modelAvatar')[0]);
        $(".updateAvatar").attr('disabled', true);
        $(".updateAvatar").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?php echo base_url('account/update_avatar')?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                $(".updateAvatar").attr('disabled', false);
                $(".updateAvatar").removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });
    }
});


$(document).ready(function(){
  var maxchars = 300;
  $('#bio').keyup(function () {
      var tlength = $(this).val().length;
      $(this).val($(this).val().substring(0, maxchars));
      var tlength = $(this).val().length;
      remain = maxchars - parseInt(tlength);
      //$('#remain').text(remain);
  });
});


</script>