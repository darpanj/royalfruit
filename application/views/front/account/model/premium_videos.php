<div class="site-setting site-feed friend_feed">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Chart -->
            </div>
            <section class="feed-video">
                <div class="container">
                    <input type="hidden" id="postIds" value="0">
                    <div class="row" id="post_list">

                    </div>
                </div>
            </section>

            <!-- end -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12 col-lg-3">
        <div class="loader" id="loader" style="">
            <img src="<?php echo base_url('themes') . '/loading.gif'; ?>">
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        /* for load modal first times */
        let limit = 9;
        let target = $("#post_list");
        let postIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'post_ids': postIds
            },
            dataType: "json",
            url: "<?php echo base_url('account/load_premium_videos_model');  ?>",
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(r) {
                let html = '';
                $.each(r.model_post_images, function(key, value) {
                    if (value.post_type == 1) {
                        html += make_video_post(value);
                    }
                    postIds += ',' + value.post_id;
                });

                $(target).html(html);
                $("#loader").hide();
                $('#postIds').val(postIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });
        /* when scroll than load more modals */
        $(window).scroll(function() {
            let postIds1 = $('#postIds').val();
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                let postIds = '0';
                $("#loader").show();
                let final_data = {
                    'limit': limit,
                    'post_ids': postIds1
                };
                $.ajax({
                    type: 'post',
                    data: final_data,
                    dataType: "json",
                    url: "<?php echo base_url('account/load_premium_videos_model');  ?>",
                    beforeSend: function() {
                        $("#loader").show();
                    },
                    success: function(r) {
                        var html = '';
                        $.each(r.model_post_images, function(key, value) {
                            if (value.post_type == 1) {
                                html += make_video_post(value);
                            }
                            postIds += ',' + value.post_id;
                        });
                        $(target).append(html);
                        $('#postIds').val(postIds1 + ',' + postIds);
                        $("#loader").hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });
    });



    function make_video_post(value) {
        let teaser_date = moment(value.created_date).fromNow();
        let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
    <div class="feed-main">
    <div class="video-wrapper">
        <video class="mmCardVid" controls>
            <source src="${SITE_IMG}uploads/models/${value.image}" type="video/mp4" autoplay loop>
            secure connection could not be established
        </video>
    </div>

        <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
            <div class="d-flex align-items-center">
                <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">
                <div class="pro-cnt">
                    <h2>${value.name}</h2>
                    <?php
                    // $dateDisplay = get_date_in_format(`${value.teaser_created_date}`);
                    ?>
                    <p class="m-0">${teaser_date}</p>
                </div>
            </div>
        </div>
    </div>
</div>`;
        return html;
    }
</script>