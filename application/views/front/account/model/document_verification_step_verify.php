<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/admin/image/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <link href="<?php echo base_url(ADM_CSS . 'circle_crop.css'); ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>

    <!-- <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script> -->
    <!-- <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script> -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css">
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
    <!-- Newly Added Starts Here -->
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>intlTelInput.css"> 
    <!-- wizrad -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_arrows.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_circles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_dots.css">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <!-- Newly Added Ends Here -->
</head>

<body class="vertical-layout site-feed vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="site-wrap">
        <input type="hidden" id="usedId" value="<?php echo check_variable_value($user_id); ?>">
        <!-- main section starts -->
        <?php
        if ($header_panel) {
            if ($user_id > 0) {
                $this->load->view('front/header');
            }
        }
        ?>
        <style type="text/css">
            .loader-spinner { 
                display:block;
                border: 12px solid #f3f3f3; 
                border-radius: 50%; 
                border-top: 12px solid #444444; 
                width: 70px; 
                height: 70px; 
                animation: spin 1s linear infinite; 
            }
             
            @keyframes spin { 
                100% { 
                    transform: rotate(360deg); 
                } 
            } 
            
            .overlay {
                position:absolute;
                top:0;
                left:0;
                right:0;
                bottom:0;
                background-color:rgba(0, 0, 0, 0.85);
                z-index:9999;
                display:none;
            }
            
            .center { 
                position: fixed; 
                top: 0; 
                bottom: 0; 
                left: 0; 
                right: 0; 
                margin: auto; 
            } 
            
            .site-wrap{
                position:relative;
            }
            
            .site-listing .list-price {
                width: 85%;
                margin: 25px auto;
            }
            .completePM {
                background-color: #008000 !important;
            }
              
        </style>

        <div class='overlay' id="loader">
        <div class="loader-spinner center"></div>
        </div>
        <div class="app-content content site-setting friends-list site-premium site-acnt-step ml-0">
          <div class="content-wrapper">
            <section class="account-step-main">
              <div class="container">
                <div class="mdl-acnt-main">
                  <div class="home-wizard">
                        <div id="smartwizard2">
                          <ul class="d-flex justify-content-around">
                            <li><a href="javascript:;"><span class="step-num">1</span>Account</a></li>
                            <li class="active"><a href="#step-2 active"><span class="step-num">2</span>Verify</a></li>
                            <li><a href="javascript:;"><span class="step-num">3</span>Review</a></li>
                            <li><a href="javascript:;"><span class="step-num">4</span>Upload</a></li>
                          </ul>
                        <div>
                        <?php
                            $photoIDImage = ($userData['document_photo_id'] != '') ? base_url().MODEL_UPD . $userData['document_photo_id'] : base_url() . FRONT_IMG . 'photo_id.svg';

                            $verificationImage = ($userData['document_id_verification_selfie'] != '') ? base_url().MODEL_UPD . $userData['document_id_verification_selfie'] : base_url() . FRONT_IMG . 'selfie_id.svg';
                        ?>
                        <div id="step-2" class="mt-3 wiz-step2">
                          <div class="d-flex align-items-center justify-content-between account-step-1">
                            <h3>Upload IDs</h3>
                            <span data-toggle="popover-hover" data-content="Your ID must be valid with visible birthdate. This can be your Driver's License, State ID, Military ID, or Passport." data-original-title="UPLOAD IDS" title="" class="popper-pro">!</span>
                          </div>
                            <div class="photo-id-sec">
                              <h4>1.Photo ID</h4>
                              <div class="id-img-block">
                                <img src="<?php echo $photoIDImage; ?>">
                              </div>
                              <div class="good-ex-list">
                                <h4>✓ Good Example</h4>
                                <ul>
                                  <li>Clearly show ID document with text clearly visible and with minimum background.</li>
                                  <li>The image should not be edited, resized or rotated.</li>
                                  <li>The file must be .PNG or .JPG under 7MB in size.</li>
                                  <li>Any ID uploaded must be a valid government-issued ID with visible birthdate.</li>
                                </ul>
                              </div>
                              <div class="d-flex align-items-center">
                                <p class="phhoto-id">Photo ID<span style="color: red">*</span></p>
                              </div>
                                <form method="post" name="PhotoIDUpload" id="PhotoIDUpload" enctype="multipart/form-data">
                                  <div class="image-upload w-100">
                                    <div class="file-upload-btn">
                                      Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="document_photo_id" id="document_photo_id">
                                    </div>
                                  </div>
                                </form>
                            </div>
                            <div class="photo-id-sec mt-3">
                              <h4>2. Selfie with ID</h4>
                              <div class="id-img-block">
                                <img src="<?php echo $verificationImage; ?>">
                              </div>
                              <div class="good-ex-list">
                                <h4>✓ Good Example</h4>
                                <ul>
                                  <li>Please take a selfie holding your ID so we can clearly see your face and ID in the photo. We need this to accurately verify your identity.</li>
                                </ul>
                              </div>
                              <div class="d-flex align-items-center">
                                <p class="phhoto-id" style="width: 22%;">Selfie With ID<span style="color: red">*</span></p>
                              </div>
                              <form method="post" name="idVerificationSelfieUpload" id="idVerificationSelfieUpload" enctype="multipart/form-data">
                                <div class="image-upload w-100">
                                  <div class="file-upload-btn">
                                    Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="document_id_verification_selfie" id="document_id_verification_selfie">
                                  </div>
                                </div>
                              </form>
                            </div>
                            <div class="border-btm"></div>
                          <form action="<?php echo base_url('account/document_verification_step_verify'); ?>" method="post" id="form_step_verify" enctype="multipart/form-data">
                            <div class="d-flex align-items-center justify-content-between account-step-1 mt-2">
                              <h3>ID Information</h3>
                              <span data-toggle="popover-hover" data-content="Your ID information will be used to prepare your legal documents and to receive payments." data-original-title="ID Information" title="" class="popper-pro">!</span>
                            </div>
                            <?php
                                $govtIssuedIDType = array(
                                                    '1' => "Driver's License",
                                                    '2' => 'State ID',
                                                    '3' => 'Military ID',
                                                    '4' => 'Non-US Passport',
                                                    '5' => 'US Passport',
                                                    );
                            ?>
                            <div class="id-inf-main">
                              <div class="set-mdl-main mt-1">
                                <label>Government Issued ID Type<span>*</span></label>
                                <div class="cus-select pre-cus-lb">
                                  <label class="w-100">
                                    <select class="mb-0" name="government_issued_id_type" id="government_issued_id_type">
                                        <option value="">Select One</option>
                                        <?php 
                                            foreach($govtIssuedIDType as $kIssuedID => $vIssuedID) {
                                                $selectedIDType = (!empty($userData) && $userData['government_issued_id_type'] == $kIssuedID) ? 'selected' : '';
                                                ?>
                                                <option value="<?= $kIssuedID ?>" <?= $selectedIDType ?>><?= $vIssuedID ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                  </label>
                                </div>
                              </div>
                              <div class="set-mdl-main mt-1">
                                <label>Issued By (State/Province)<span>*</span></label>
                                <input type="text" name="government_id_issued_by_state" id="government_id_issued_by_state" class="mb-0" value="<?= $userData['government_id_issued_by_state'] ?>">
                              </div>
                              <div class="set-mdl-main mt-1">
                                <label>ID Number<span>*</span></label>
                                <input type="text" name="government_id_number" id="government_id_number" class="mb-0" value="<?= $userData['government_id_number'] ?>">
                              </div>
                              <label style="color: #000;font-weight: 500;" class="mt-1">ID Expiration</label>
                              <?php
                                  $months = array(
                                      '01' => 'January',
                                      '02' => 'February',
                                      '03' => 'March',
                                      '04' => 'April',
                                      '05' => 'May',
                                      '06' => 'June',
                                      '07' => 'July ',
                                      '08' => 'August',
                                      '09' => 'September',
                                      '10' => 'October',
                                      '11' => 'November',
                                      '12' => 'December',
                                  );
                              ?>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="set-mdl-main">
                                    <div class="cus-select pre-cus-lb">
                                      <label class="w-100">
                                        <select class="mb-0" name="government_id_expiration_month" id="government_id_expiration_month">
                                          <option value="">Month</option>
                                          <?php 
                                              foreach($months as $kMonth => $vMonth) {
                                                  $selectedMonth = (!empty($userData) && $userData['government_id_expiration_month'] == $kMonth) ? 'selected' : '';
                                                  ?>
                                                  <option value="<?= $kMonth ?>" <?= $selectedMonth ?>><?= $vMonth ?></option>
                                                  <?php
                                              }
                                          ?>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <?php
                                  $dayArray = array();
                                  for($d = 1; $d <= 31; $d++) {
                                    $dayArray[$d] = $d;
                                  }
                                ?>
                                <div class="col-md-4">
                                  <div class="set-mdl-main">
                                    <div class="cus-select pre-cus-lb">
                                      <label class="w-100">
                                        <select class="mb-0" name="government_id_expiration_day" id="government_id_expiration_day">
                                          <option value="">Day</option>
                                          <?php 
                                              foreach($dayArray as $kDay => $vDay) {
                                                  $selectedDay = (!empty($userData) && $userData['government_id_expiration_day'] == $kDay) ? 'selected' : '';
                                                  ?>
                                                  <option value="<?= $kDay ?>" <?= $selectedDay ?> ><?= $vDay ?></option>
                                                  <?php
                                              }
                                          ?>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <?php
                                  $yearArray = array();
                                  for($y = 2020; $y <= 2080; $y++) {
                                    $yearArray[$y] = $y;
                                  }
                                ?>
                                <div class="col-md-4">
                                  <div class="set-mdl-main">
                                    <div class="cus-select pre-cus-lb">
                                      <label class="w-100">
                                        <select class="mb-0"  name="government_id_expiration_year" id="government_id_expiration_year">
                                          <option value="">Year</option>
                                          <?php 
                                              foreach($yearArray as $kYear => $vYear) {
                                                  $selectedYear = (!empty($userData) && $userData['government_id_expiration_year'] == $kYear) ? 'selected' : '';
                                                  ?>
                                                  <option value="<?= $kYear ?>" <?= $selectedYear ?>><?= $vYear ?></option>
                                                  <?php
                                              }
                                          ?>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <?php 
                                  $checkedNoExpiration = (!empty($userData) && $userData['no_expiration'] == 1) ? 'checked' : '';
                                ?>
                                <div class="col-12 mt-1">
                                  <label class="control control--checkbox">No Expiration
                                    <input type="checkbox" name="no_expiration" id="no_expiration" <?= $checkedNoExpiration ?>>
                                    <span class="control__indicator"></span>
                                  </label>
                                </div>
                              </div>
                              <div class="set-mdl-main">
                                <label>Full Legal Name<span>*</span></label>
                                <input type="text" name="legal_name" id="legal_name" value="<?= $userData['legal_name'] ?>">
                              </div>
                              <label style="color: #000;font-weight: 500;">Date of Birth<span style="color: red;">*</span></label>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="set-mdl-main">
                                    <div class="cus-select pre-cus-lb">
                                      <label class="w-100">
                                        <select class="mb-0" name="dob_month" id="dob_month">
                                          <option value="">Month</option>
                                          <?php 
                                              foreach($months as $kMonth => $vMonth) {
                                                  $selectedDOBmonth = (!empty($userData) && $userData['dob_month'] == $kMonth) ? 'selected' : '';
                                                  ?>
                                                  <option value="<?= $kMonth ?>" <?= $selectedDOBmonth ?>><?= $vMonth ?></option>
                                                  <?php
                                              }
                                          ?>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="set-mdl-main">
                                    <div class="cus-select pre-cus-lb">
                                      <label class="w-100">
                                        <select class="mb-0" name="dob_day" id="dob_day">
                                          <option value="">Day</option>
                                          <?php 
                                              foreach($dayArray as $kDay => $vDay) {
                                                  $selectedDOBday = (!empty($userData) && $userData['dob_day'] == $kDay) ? 'selected' : '';
                                                  ?>
                                                  <option value="<?= $kDay ?>" <?= $selectedDOBday ?>><?= $vDay ?></option>
                                                  <?php
                                              }
                                          ?>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <?php
                                  $dobYearArray = array();
                                  for($y = 1950; $y <= date("Y"); $y++) {
                                    $dobYearArray[$y] = $y;
                                  }
                                  $k = array_keys($dobYearArray);
                                  $v = array_values($dobYearArray);
                                  $rk = array_reverse($k);
                                  $rv = array_reverse($v);
                                  $dobYearArray = array_combine($rk, $rv);
                                ?>
                                <div class="col-md-4">
                                  <div class="set-mdl-main">
                                    <div class="cus-select pre-cus-lb">
                                      <label class="w-100">
                                        <select class="mb-0" name="dob_year" id="dob_year">
                                          <option value="">Year</option>
                                          <?php 
                                              foreach($dobYearArray as $kYear => $vYear) {
                                                  $selectedDOByear = (!empty($userData) && $userData['dob_year'] == $kYear) ? 'selected' : '';
                                                  ?>
                                                  <option value="<?= $kYear ?>" <?= $selectedDOByear ?>><?= $vYear ?></option>
                                                  <?php
                                              }
                                          ?>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="border-btm w-100"></div>
                                </div>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between account-step-1 mt-2">
                              <h3>Aliases/Stage Names</h3>
                              <span data-toggle="popover-hover" data-content="Your alias or other names you are known or have been known by, including stage names, Internet usernames, aliases, nicknames, and married or maiden names. This is optional." data-original-title="Aliases/Stage Names" title="" class="popper-pro">!</span>
                            </div>
                            <div class="alisa-main">
                              <div class="set-mdl-main mt-1">
                                <label>Your Alias/Stage Names (enter at least one)</label>
                                <input type="text" name="alias_one" id="alias_one" class="mb-0" placeholder="Alisa or Stage Name" value="<?= $userData['alias_one'] ?>"> 
                                <input type="text" name="alias_two" id="alias_two" class="mb-0 mt-1" placeholder="Other Alisa or Stage Name" value="<?= $userData['alias_two'] ?>">
                              </div>
                              <div class="d-flex sec-stepn-btn sec-stepn-btn2 justify-content-between">
                                <a href="<?= site_url('account/document_verification_step_account') ?>"><button type="button" class="btn btn-gradient btn-sub acc-vrf-prv">Back</button></a>
                                <!-- <a href="account_review.html"><button type="submit" class="btn btn-gradient btn-sub acc-vrf-nxt">Next - Review</button></a> -->
                                <button type="submit" class="btn btn-gradient btn-sub" id="step2">Next - Review</button>
                              </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
              </div>
            </section>
          </div>
        </div>

        <!-- main section ends -->
        <div class="site-feed">
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="modelId" id="modelId">
                            <input type="hidden" name="type" id="type">
                            <!--  // 0 for token purchase 
                            // 1 for follow -->
                            <input type="hidden" name="price" id="price">
                            <img src="" id="image_model_modal" width="50" alt="model">
                            <h2>Follow <span id="model_name_modal"></span></h2>
                            <h2>$<span id="model_price"></span>/month</h2>
                            <p>You'll get access to daily private content, direct messaging, and more!</p>
                            <button type="button" class="site-folow flw2" id="purchase_now">Continue</button>
                            <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a>
                            </p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <!-- <script src="<?php echo base_url() . FRONT_THEME_ASSETS_VENDOR; ?>js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- Newly Added Starts Here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.smartWizard.js"></script>
    <!-- Newly Added Ends Here -->
    <script>
        <?php if (!empty($this->session->flashdata('success'))) { ?>
            sType = getStatusText(200);
            sText = '<?php echo $this->session->flashdata('success'); ?>';
            //Custom.myNotification(sType, sText);
            swal({
                title: "Success",
                text: sText,
                icon: "success",
            });
        <?php } ?>
        <?php if (!empty($this->session->flashdata('error'))) { ?>
            sType = getStatusText(412);
            sText = '<?php echo $this->session->flashdata('error'); ?>';
            // Custom.myNotification(sType, sText);
            swal({
                title: "Error!",
                text: sText,
                icon: "error",
            });
        <?php } ?>
    </script>
</body>

</html>

<script>
// SIDEBAR
$(document).ready(function(){
  $('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'right', // Choose the horizontal origin
      closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: false // Choose whether you can drag to open on touch screens
    }
  );
  // START OPEN
  $('.button-collapse').sideNav('hide');
});

</script>
<script>
$(function () {
  $('[data-toggle="popover-hover"]').popover({
    trigger: 'hover',
  })
})
  
</script>

<!-- wizard -->
<script type="text/javascript">
        $(document).ready(function(){
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'arrows',
                    transitionEffect:'fade',
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            $('#smartwizard2').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: false
            });
        });
</script>


<script>
    $(document).ready(function () {
        $("#form_step_verify").validate({
            rules: {
                government_issued_id_type: {required: true},
                government_id_issued_by_state: {required: true},
                government_id_number: {required: true},
                legal_name: {required: true},
                dob_month: {required: true},
                dob_day: {required: true},
                dob_year: {required: true},

            },
            messages: {
                government_issued_id_type: {required: "Government Issued ID Type is required"},
                government_id_issued_by_state: {required: "Issued By (State/Province) is required"},
                government_id_number: {required: "ID number is required"},
                legal_name: {required: "Full Legal Name is required"},
                dob_month: {required: "Month is required"},
                dob_day: {required: "Day is required"},
                dob_year: {required: "Year is required"},
            },
            ignore: ":hidden"
        });
    });
</script>

<script type="text/javascript">

$(document).ready(function () {

    $('#document_photo_id').change(function(){
      ajaxPhotoIDUpload();
    });

    function ajaxPhotoIDUpload(){
        var data = new FormData($('#PhotoIDUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_photo_id') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }

    $('#document_id_verification_selfie').change(function(){
      ajaxIDVerificationSelfieUpload();
    });

    function ajaxIDVerificationSelfieUpload(){
        var data = new FormData($('#idVerificationSelfieUpload')[0]);
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/upload_id_verification_selfie') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $("#loader").show();
            },
            success: function (r) {
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function () {
                            location.reload();
                        },1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete:function(data){
                 $("#loader").hide();
                }
        });
    }

});
</script>