<style>
    #container {
        height: 400px;
    }

    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 310px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<div class="site-setting site-feed friend_feed">
    <div class="app-content content">
        <div class="content-wrapper">
<<<<<<< HEAD
            <!-- <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">

            </div> -->

            <section class="feed-video">
                <div class="container">
                    <input type="hidden" id="postIds" value="0">
                    <div class="row">
                        <select name="filter" class="earning-main" id="filter">
                            <option value="last_seven_days">Last 7 days</option>
                            <option value="last_fifteen_days">Last 15 days</option>
                            <option value="last_thirty_days">Last 30 days</option>
                        </select>
                    </div>


=======
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Chart -->
            </div>
            <section class="feed-video">
                <div class="container">
                    <input type="hidden" id="postIds" value="0">
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
                    <div class="row" id="post_list">
                        <figure class="highcharts-figure">
                            <div id="container"></div>
                            <p class="highcharts-description">
                                This chart shows you followers detail and premium video and custom video purchased information.
                            </p>
                        </figure>
                    </div>
                </div>
            </section>
<<<<<<< HEAD
            <section class="earning-main">
                <div class="row">
                    <div class="col-md-6 mb-2">
                        <div class="earning-box">
                            <div class="ern-cnt">
                                <h4>$ RECENT (TOTAL EARNINGS)</h4>
                                <span data-toggle="popover-hover" title="RECENT (TOTAL EARNINGS)" data-content="This is an overview of your estimated earnings. Things like chargebacks, cancellations failed recurring payments will be calculated into your final earnings for each period.">?</span>
                            </div>
                            <div class="row rec-brhd">
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Today</p>
                                        <h5><span>$</span><?php echo isset($today_earning) ? $today_earning : 0; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Yesterday</p>
                                        <h5><span>$</span><?php echo isset($yesterday_earning) ? $yesterday_earning : 0; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Last 7 Days</p>
                                        <h5><span>$</span><?php echo isset($last_sevenDays_earning) ? $last_sevenDays_earning : 0; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Last 30 Days</p>
                                        <h5><span>$</span><?php echo isset($month_earning) ? $month_earning : 0; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="earning-box">
                            <div class="ern-cnt">
                                <h4>$ EARNINGS PER PERIOD</h4>
                                <span data-toggle="popover-hover" title="EARNINGS PER PERIOD" data-content="This is your estimated earnings, per period. Things like chargebacks, cancellations failed recurring payments will be calculated into your final earnings for each period.">?</span>
                            </div>
                            <div class="row rec-brhd">
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0"><?php echo $fourperiod['first_ele']; ?></p>
                                        <h5><span>$</span><?php echo $first_peroid_value; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0"><?php echo $fourperiod['second_ele']; ?></p>
                                        <h5><span>$</span><?php echo $second_peroid_value; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0"><?php echo $fourperiod['third_ele']; ?></p>
                                        <h5><span>$</span><?php echo $third_peroid_value; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0"><?php echo $fourperiod['fourth_ele']; ?></p>
                                        <h5><span>$</span><?php echo $fourth_peroid_value; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="earning-box">
                            <div class="ern-cnt">
                                <h4>$ PAYMENTS</h4>
                                <span data-toggle="popover-hover" title="PAYMENTS" data-content="This is an overview of your payments. Payments are made 2 weeks after each period, on the 2nd and 17th of each month.">?</span>
                            </div>
                            <div class="row rec-brhd">
                                <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Upcoming</p>
                                        <h5>TBD</h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Last Payment</p>
                                        <h5><span>$</span><?php echo isset($last_payment) ? $last_payment : 0; ?></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Lifetime Paid</p>
                                        <h5><span>$</span><?php echo isset($lifetime_payment) ? $lifetime_payment : 0; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="earning-box">
                            <div class="ern-cnt">
                                <h4>ACCOUNT HEALTH</h4>
                                <span data-toggle="popover-hover" title="ACCOUNT HEALTH" data-content="These metrics help determine the health of your account.Follower retention is the percentage of followers that continued to follow you without canceling or having billing issues in the last 60 days.Followers contacted is the percentage of users that you've sent at least 1 message.Average Uploads is, on average, how many photos you're uploading per day.">?</span>
                            </div>
                            <div class="row rec-brhd">
                                <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Follower Retention</p>
                                        <h5>0<span>%</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Followers Contacted</p>
                                        <h5>0<span>%</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Average Uploads</p>
                                        <h5>0.00 a day</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6 mb-2">
                        <div class="earning-box">
                            <div class="ern-cnt">
                                <h4>$ REWARDS EARNED PER PERIOD</h4>
                                <span data-toggle="popover-hover" title="REWARDS EARNED PER PERIOD" data-content="This is your rewards for achieving new followers, per period.">?</span>
                            </div>
                            <div class="row rec-brhd">
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">May 16 - 31</p>
                                        <h5><span>$</span>0</h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">May 1 - 15</p>
                                        <h5><span>$</span>0</h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Apr 16 - 30</p>
                                        <h5><span>$</span>0</h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                                    <div class="rec-td-cnt">
                                        <p class="m-0">Apr 1 - 15</p>
                                        <h5><span>$</span>0</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </section>
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

            <!-- end -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12 col-lg-3">
        <div class="loader" id="loader" style="">
            <img src="<?php echo base_url('themes') . '/loading.gif'; ?>">
        </div>
    </div>
</div>
<script>
<<<<<<< HEAD
    let now = moment();
    //now.format('dd-mm-yy');
    // let rr = now.subtract('2', 'years');
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    var firstDate = 0;
    var secondDate = 0;
    var thirdDate = 0;
    var fourthDate = 0;
    var fifthDate = 0;
    var sixthdDate = 0;
    var seventhDate = 0;
<<<<<<< HEAD
    var eightDate = 0;
    var ninehDate = 0;
    var tenthDate = 0;
    var eleventhDate = 0;
    var twelevthDate = 0;
    var thirteenDate = 0;
    var fourteenDate = 0;
    var fifteenDate = 0;
    var sixteenDate = 0;
    var seventeenDate = 0;
    var eighteenDate = 0;
    var nineteenDate = 0;
    var twentyDate = 0;
    var twentyonenDate = 0;
    var twentytwoDate = 0;
    var twentythreeDate = 0;
    var twentyfourDate = 0;
    var twentyfiveDate = 0;
    var twentysixDate = 0;
    var twentysevenDate = 0;
    var twentyeightDate = 0;
    var twentynineDate = 0;
    var thrirtyDate = 0;
    var thrirtyoneDate = 0;
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

    var firstDate2 = 0;
    var secondDate2 = 0;
    var thirdDate2 = 0;
    var fourthDate2 = 0;
    var fifthDate2 = 0;
    var sixthdDate2 = 0;
    var seventhDate2 = 0;
<<<<<<< HEAD
    var eightDate2 = 0;
    var ninehDate2 = 0;
    var tenthDate2 = 0;
    var eleventhDate2 = 0;
    var twelevthDate2 = 0;
    var thirteenDate2 = 0;
    var fourteenDate2 = 0;
    var fifteenDate2 = 0;
    var sixteenDate2 = 0;
    var seventeenDate2 = 0;
    var eighteenDate2 = 0;
    var nineteenDate2 = 0;
    var twentyDate2 = 0;
    var twentyonenDate2 = 0;
    var twentytwoDate2 = 0;
    var twentythreeDate2 = 0;
    var twentyfourDate2 = 0;
    var twentyfiveDate2 = 0;
    var twentysixDate2 = 0;
    var twentysevenDate2 = 0;
    var twentyeightDate2 = 0;
    var twentynineDate2 = 0;
    var thrirtyDate2 = 0;
    var thrirtyoneDate2 = 0;
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

    var firstDate3 = 0;
    var secondDate3 = 0;
    var thirdDate3 = 0;
    var fourthDate3 = 0;
    var fifthDate3 = 0;
    var sixthdDate3 = 0;
    var seventhDate3 = 0;
<<<<<<< HEAD
    var eightDate3 = 0;
    var ninehDate3 = 0;
    var tenthDate3 = 0;
    var eleventhDate3 = 0;
    var twelevthDate3 = 0;
    var thirteenDate3 = 0;
    var fourteenDate3 = 0;
    var fifteenDate3 = 0;
    var sixteenDate3 = 0;
    var seventeenDate3 = 0;
    var eighteenDate3 = 0;
    var nineteenDate3 = 0;
    var twentyDate3 = 0;
    var twentyonenDate3 = 0;
    var twentytwoDate3 = 0;
    var twentythreeDate3 = 0;
    var twentyfourDate3 = 0;
    var twentyfiveDate3 = 0;
    var twentysixDate3 = 0;
    var twentysevenDate3 = 0;
    var twentyeightDate3 = 0;
    var twentynineDate3 = 0;
    var thrirtyDate3 = 0;
    var thrirtyoneDate3 = 0;


    function getLastSevenDaysDate() {

=======


    function getLastSevenDaysDate() { //aa kai kamnu nathy
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        var fullDate = new Date()

        //Thu May 19 2011 17:25:38 GMT+1000 {}

        //convert month to 2 digits
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
        var seventhDate = fullDate.getDate() - 1 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var sixthDate = fullDate.getDate() - 2 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var fifthhDate = fullDate.getDate() - 3 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var fourthhDate = fullDate.getDate() - 4 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var thirdDate = fullDate.getDate() - 5 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var secondDate = fullDate.getDate() - 6 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var firstDate = fullDate.getDate() - 7 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

        var arr = {
            firstDate,
            secondDate,
            thirdDate,
            fourthhDate,
            fifthhDate,
            sixthDate,
            seventhDate
        }
        return arr;

    }

<<<<<<< HEAD
    function getFifteenDaysDate() {
        var fullDate = new Date()

        // dateTo = moment().format('YYYY-MM-DD');
        // dateFrom = moment().subtract(15, 'd').format('YYYY-MM-DD');
        //Thu May 19 2011 17:25:38 GMT+1000 {}

        //convert month to 2 digits
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

        var fifteenDate = fullDate.getDate() - 1 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var fourteenDate = fullDate.getDate() - 2 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var thirteenDate = fullDate.getDate() - 3 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var twelevthDate = fullDate.getDate() - 4 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var eleventhDate = fullDate.getDate() - 5 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var tenthDate = fullDate.getDate() - 6 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var ninehDate = fullDate.getDate() - 7 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var eightDate = fullDate.getDate() - 8 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var seventhDate = fullDate.getDate() - 9 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var sixthDate = fullDate.getDate() - 10 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var fifthhDate = fullDate.getDate() - 11 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var fourthhDate = fullDate.getDate() - 12 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var thirdDate = fullDate.getDate() - 13 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var secondDate = fullDate.getDate() - 14 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var firstDate = fullDate.getDate() - 15 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

        var arr = {
            firstDate,
            secondDate,
            thirdDate,
            fourthhDate,
            fifthhDate,
            sixthDate,
            seventhDate,
            eightDate,
            ninehDate,
            tenthDate,
            eleventhDate,
            twelevthDate,
            thirteenDate,
            fourteenDate,
            fifteenDate

        }
        return arr;

    }

    function getFullMonthDate() {
        var fullDate = new Date();
        //console.log(now.subtract('2', 'years'));
        // dateTo = moment().format('YYYY-MM-DD');
        // dateFrom = moment().subtract(15, 'd').format('YYYY-MM-DD');
        //Thu May 19 2011 17:25:38 GMT+1000 {}

        //convert month to 2 digits
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
        var thrirtyoneDate = now.subtract('1', 'days').format('DD/MM/YY');
        now = moment();
        var thrirtyDate = now.subtract('2', 'days').format('DD/MM/YY');
        now = moment();
        var twentynineDate = now.subtract('3', 'days').format('DD/MM/YY');
        now = moment();
        var twentyeightDate = now.subtract('4', 'days').format('DD/MM/YY');
        now = moment();
        var twentysevenDate = now.subtract('5', 'days').format('DD/MM/YY');
        now = moment();
        var twentysixDate = now.subtract('6', 'days').format('DD/MM/YY');
        now = moment();
        var twentyfiveDate = now.subtract('7', 'days').format('DD/MM/YY');
        now = moment();
        var twentyfourDate = now.subtract('8', 'days').format('DD/MM/YY');
        now = moment();
        var twentythreeDate = now.subtract('9', 'days').format('DD/MM/YY');
        now = moment();
        var twentytwoDate = now.subtract('10', 'days').format('DD/MM/YY');
        now = moment();
        var twentyonenDate = now.subtract('11', 'days').format('DD/MM/YY');
        now = moment();
        var twentyDate = now.subtract('12', 'days').format('DD/MM/YY');
        now = moment();
        var nineteenDate = now.subtract('13', 'days').format('DD/MM/YY');
        now = moment();
        var eighteenDate = now.subtract('14', 'days').format('DD/MM/YY');
        now = moment();
        var seventeenDate = now.subtract('15', 'days').format('DD/MM/YY');
        now = moment();
        var sixteenDate = now.subtract('16', 'days').format('DD/MM/YY');
        now = moment();
        var fifteenDate = now.subtract('17', 'days').format('DD/MM/YY');
        now = moment();
        var fourteenDate = now.subtract('18', 'days').format('DD/MM/YY');
        now = moment();
        var thirteenDate = now.subtract('19', 'days').format('DD/MM/YY');
        now = moment();
        var twelevthDate = now.subtract('20', 'days').format('DD/MM/YY');
        now = moment();
        var eleventhDate = now.subtract('21', 'days').format('DD/MM/YY');
        now = moment();
        var tenthDate = now.subtract('22', 'days').format('DD/MM/YY');
        now = moment();
        var ninehDate = now.subtract('23', 'days').format('DD/MM/YY');
        now = moment();
        var eightDate = now.subtract('24', 'days').format('DD/MM/YY');
        now = moment();
        var seventhDate = now.subtract('25', 'days').format('DD/MM/YY');
        now = moment();
        var sixthDate = now.subtract('26', 'days').format('DD/MM/YY');
        now = moment();
        var fifthhDate = now.subtract('27', 'days').format('DD/MM/YY');
        now = moment();
        var fourthhDate = now.subtract('28', 'days').format('DD/MM/YY');
        now = moment();
        var thirdDate = now.subtract('29', 'days').format('DD/MM/YY');
        now = moment();
        var secondDate = now.subtract('30', 'days').format('DD/MM/YY');
        now = moment();
        var firstDate = now.subtract('31', 'days').format('DD/MM/YY');
        now = moment();

        var arr = {
            firstDate,
            secondDate,
            thirdDate,
            fourthhDate,
            fifthhDate,
            sixthDate,
            seventhDate,
            eightDate,
            ninehDate,
            tenthDate,
            eleventhDate,
            twelevthDate,
            thirteenDate,
            fourteenDate,
            fifteenDate,
            thrirtyoneDate,
            thrirtyDate,
            twentynineDate,
            twentyeightDate,
            twentysevenDate,
            twentysixDate,
            twentyfiveDate,
            twentyfourDate,
            twentythreeDate,
            twentytwoDate,
            twentyonenDate,
            twentyDate,
            nineteenDate,
            eighteenDate,
            seventeenDate,
            sixteenDate,

        }
        return arr;

    }
=======

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

    $(document).ready(function() {
        // debugger;
        load_followers();
        var arr = [];
        // var d = getLastSevenDaysDate();
        // var followers = get_followers();
        //console.log(arr2);
        // console.log(firstDate, secondDate, thirdDate, fourthDate, fifthDate, sixthdDate, seventhDate);

    });

<<<<<<< HEAD
    $(document).on('change', '#filter', function() {
        load_followers();
    });

    function load_followers() {
        var filter = $("#filter").val();
        //console.log(filter);
=======
    function load_followers() {

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        var arr = [];
        $.ajax({
            url: "<?php echo base_url('account/load_models_followers'); ?>",
            method: "Post",
            dataType: "Json",
            data: {
<<<<<<< HEAD
                'filter': filter
            },
            success: function(r) {
                if (filter == "last_seven_days") {


                    var d = getLastSevenDaysDate();
                    $.each(r.modelsData, function(key, val) {
                        // console.log(key);
                        // console.log(val);

                        for (i = 1; i <= 7; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate = val;
                                        break;
                                    case 2:
                                        secondDate = val;
                                        break;
                                    case 3:
                                        thirdDate = val;
                                        break;
                                    case 4:
                                        fourthDate = val;
                                        break;
                                    case 5:
                                        fifthDate = val;
                                        break;
                                    case 6:
                                        sixthdDate = val;
                                        break;
                                    case 7:
                                        seventhDate = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });
                    $.each(r.customvideo, function(key, val) {
                        // console.log(key);
                        // console.log(val);

                        for (i = 1; i <= 7; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate2 = val;
                                        break;
                                    case 2:
                                        secondDate2 = val;
                                        break;
                                    case 3:
                                        thirdDate2 = val;
                                        break;
                                    case 4:
                                        fourthDate2 = val;
                                        break;
                                    case 5:
                                        fifthDate2 = val;
                                        break;
                                    case 6:
                                        sixthdDate2 = val;
                                        break;
                                    case 7:
                                        seventhDate2 = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });
                    $.each(r.privatevideo, function(key, val) {
                        // console.log(key);
                        // console.log(val);

                        for (i = 1; i <= 7; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate3 = val;
                                        break;
                                    case 2:
                                        secondDate3 = val;
                                        break;
                                    case 3:
                                        thirdDate3 = val;
                                        break;
                                    case 4:
                                        fourthDate3 = val;
                                        break;
                                    case 5:
                                        fifthDate3 = val;
                                        break;
                                    case 6:
                                        sixthdDate3 = val;
                                        break;
                                    case 7:
                                        seventhDate3 = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });

                    Highcharts.chart('container', {
                        chart: {
                            type: 'areaspline'
                        },
                        title: {
                            text: 'Total Earnings'
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'left',
                            verticalAlign: 'top',
                            x: 150,
                            y: 100,
                            floating: true,
                            borderWidth: 1,
                            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
                        },
                        xAxis: {
                            categories: [
                                d.firstDate,
                                d.secondDate,
                                d.thirdDate,
                                d.fourthhDate,
                                d.fifthhDate,
                                d.sixthDate,
                                d.seventhDate
                            ],
                            plotBands: [{ // visualize the weekend
                                from: 4.5,
                                to: 6.5,
                                color: 'rgba(68, 170, 213, .2)'
                            }]
                        },
                        yAxis: {
                            title: {
                                text: 'Total Earnings'
                            }
                        },
                        tooltip: {
                            shared: true,
                            valueSuffix: ' units'
                        },
                        credits: {
                            enabled: false
                        },
                        plotOptions: {
                            areaspline: {
                                fillOpacity: 0.5
                            }
                        },
                        series: [{
                                name: 'Followers',
                                // data: [1, 2, 3, 4, 5, 6, 7]
                                data: [seventhDate, sixthdDate, fifthDate, fourthDate, thirdDate, secondDate, firstDate]
                            },
                            {
                                name: 'Custom Video',
                                data: [seventhDate2, sixthdDate2, fifthDate2, fourthDate2, thirdDate2, secondDate2, firstDate2]
                            },
                            {
                                name: 'Private Video',
                                data: [seventhDate3, sixthdDate3, fifthDate3, fourthDate3, thirdDate3, secondDate3, firstDate3]
                            }
                        ]
                    });
                } else if (filter == "last_fifteen_days") {

                    dateTo = moment().format('YYYY-MM-DD');
                    dateFrom = moment().subtract(15, 'd').format('YYYY-MM-DD');
                    // console.log(dateFrom + ' and  to is ' + dateTo);
                    var d = getFifteenDaysDate();
                    $.each(r.modelsData, function(key, val) {
                        // console.log(key);
                        // console.log(val);

                        for (i = 1; i <= 15; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate = val;
                                        break;
                                    case 2:
                                        secondDate = val;
                                        break;
                                    case 3:
                                        thirdDate = val;
                                        break;
                                    case 4:
                                        fourthDate = val;
                                        break;
                                    case 5:
                                        fifthDate = val;
                                        break;
                                    case 6:
                                        sixthdDate = val;
                                        break;
                                    case 7:
                                        seventhDate = val;
                                        break;
                                    case 8:
                                        eightDate = val;
                                        break;
                                    case 9:
                                        ninehDate = val;
                                        break;
                                    case 10:
                                        tenthDate = val;
                                        break;
                                    case 11:
                                        eleventhDate = val;
                                        break;
                                    case 12:
                                        twelevthDate = val;
                                        break;
                                    case 13:
                                        thirteenDate = val;
                                        break;
                                    case 14:
                                        fourteenDate = val;
                                        break;
                                    case 15:
                                        fifteenDate = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });
                    $.each(r.customvideo, function(key, val) {
                        for (i = 1; i <= 15; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate2 = val;
                                        break;
                                    case 2:
                                        secondDate2 = val;
                                        break;
                                    case 3:
                                        thirdDate2 = val;
                                        break;
                                    case 4:
                                        fourthDate2 = val;
                                        break;
                                    case 5:
                                        fifthDate2 = val;
                                        break;
                                    case 6:
                                        sixthdDate2 = val;
                                        break;
                                    case 7:
                                        seventhDate2 = val;
                                        break;
                                    case 8:
                                        eightDate2 = val;
                                        break;
                                    case 9:
                                        ninehDate2 = val;
                                        break;
                                    case 10:
                                        tenthDate2 = val;
                                        break;
                                    case 11:
                                        eleventhDate2 = val;
                                        break;
                                    case 12:
                                        twelevthDate2 = val;
                                        break;
                                    case 13:
                                        thirteenDate2 = val;
                                        break;
                                    case 14:
                                        fourteenDate2 = val;
                                        break;
                                    case 15:
                                        fifteenDate2 = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });
                    $.each(r.privatevideo, function(key, val) {
                        for (i = 1; i <= 15; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate3 = val;
                                        break;
                                    case 2:
                                        secondDate3 = val;
                                        break;
                                    case 3:
                                        thirdDate3 = val;
                                        break;
                                    case 4:
                                        fourthDate3 = val;
                                        break;
                                    case 5:
                                        fifthDate3 = val;
                                        break;
                                    case 6:
                                        sixthdDate3 = val;
                                        break;
                                    case 7:
                                        seventhDate3 = val;
                                        break;
                                    case 8:
                                        eightDate3 = val;
                                        break;
                                    case 9:
                                        ninehDate3 = val;
                                        break;
                                    case 10:
                                        tenthDate3 = val;
                                        break;
                                    case 11:
                                        eleventhDate3 = val;
                                        break;
                                    case 12:
                                        twelevthDate3 = val;
                                        break;
                                    case 13:
                                        thirteenDate3 = val;
                                        break;
                                    case 14:
                                        fourteenDate3 = val;
                                        break;
                                    case 15:
                                        fifteenDate3 = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });

                    Highcharts.chart('container', {
                        chart: {
                            type: 'areaspline'
                        },
                        title: {
                            text: 'Total Earnings'
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'left',
                            verticalAlign: 'top',
                            x: 150,
                            y: 100,
                            floating: true,
                            borderWidth: 1,
                            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
                        },
                        xAxis: {
                            categories: [
                                d.firstDate,
                                d.secondDate,
                                d.thirdDate,
                                d.fourthhDate,
                                d.fifthhDate,
                                d.sixthDate,
                                d.seventhDate,
                                d.eightDate,
                                d.ninehDate,
                                d.tenthDate,
                                d.eleventhDate,
                                d.twelevthDate,
                                d.thirteenDate,
                                d.fourteenDate,
                                d.fifteenDate
                            ],
                            plotBands: [{ // visualize the weekend
                                from: 4.5,
                                to: 6.5,
                                color: 'rgba(68, 170, 213, .2)'
                            }]
                        },
                        yAxis: {
                            title: {
                                text: 'Total Earnings'
                            }
                        },
                        tooltip: {
                            shared: true,
                            valueSuffix: ' units'
                        },
                        credits: {
                            enabled: false
                        },
                        plotOptions: {
                            areaspline: {
                                fillOpacity: 0.5
                            }
                        },
                        series: [{
                                name: 'Followers',
                                // data: [1, 2, 3, 4, 5, 6, 7]
                                data: [fifteenDate, fourteenDate, thirteenDate, twelevthDate, eleventhDate, tenthDate, ninehDate, eightDate, seventhDate, sixthdDate, fifthDate, fourthDate, thirdDate, secondDate, firstDate]
                            },
                            {
                                name: 'Custom Video',
                                data: [fifteenDate2, fourteenDate2, thirteenDate2, twelevthDate2, eleventhDate2, tenthDate2, ninehDate2, eightDate2, seventhDate2, sixthdDate2, fifthDate2, fourthDate2, thirdDate2, secondDate2, firstDate2]
                            },
                            {
                                name: 'Private Video',
                                data: [fifteenDate3, fourteenDate3, thirteenDate3, twelevthDate3, eleventhDate3, tenthDate3, ninehDate3, eightDate3, seventhDate3, sixthdDate3, fifthDate3, fourthDate3, thirdDate3, secondDate3, firstDate3]
                            }
                        ]
                    });
                } else if (filter = "last_thirty_days") {
                    const startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
                    const endOfMonth = moment().endOf('month').format('YYYY-MM-DD');

                    var dates = dateRange('2013-11-01', '2014-06-01');
                    var d = getFullMonthDate();
                    $.each(r.modelsData, function(key, val) {
                        // console.log(key);
                        console.log(val);

                        for (i = 1; i <= 30; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate = val;
                                        break;
                                    case 2:
                                        secondDate = val;
                                        break;
                                    case 3:
                                        thirdDate = val;
                                        break;
                                    case 4:
                                        fourthDate = val;
                                        break;
                                    case 5:
                                        fifthDate = val;
                                        break;
                                    case 6:
                                        sixthdDate = val;
                                        break;
                                    case 7:
                                        seventhDate = val;
                                        break;
                                    case 8:
                                        eightDate = val;
                                        break;
                                    case 9:
                                        ninehDate = val;
                                        break;
                                    case 10:
                                        tenthDate = val;
                                        break;
                                    case 11:
                                        eleventhDate = val;
                                        break;
                                    case 12:
                                        twelevthDate = val;
                                        break;
                                    case 13:
                                        thirteenDate = val;
                                        break;
                                    case 14:
                                        fourteenDate = val;
                                        break;
                                    case 15:
                                        fifteenDate = val;
                                        break;
                                    case 16:
                                        sixteenDate = val;
                                        break;
                                    case 17:
                                        seventeenDate = val;
                                        break;
                                    case 18:
                                        eighteenDate = val;
                                        break;
                                    case 19:
                                        nineteenDate = val;
                                        break;
                                    case 20:
                                        twentyDate = val;
                                        break;
                                    case 21:
                                        twentyonenDate = val;
                                        break;
                                    case 22:
                                        twentytwoDate = val;
                                        break;
                                    case 23:
                                        twentythreeDate = val;
                                        break;
                                    case 24:
                                        twentyfourDate = val;
                                        break;
                                    case 25:
                                        twentyfiveDate = val;
                                        break;
                                    case 26:
                                        twentysixDate = val;
                                        break;
                                    case 27:
                                        twentysevenDate = val;
                                        break;
                                    case 28:
                                        twentyeightDate = val;
                                        break;
                                    case 29:
                                        twentynineDate = val;
                                        break;
                                    case 30:
                                        thrirtyDate = val;
                                        break;
                                    case 31:
                                        thrirtyoneDate = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });
                    $.each(r.customvideo, function(key, val) {
                        // console.log(key);
                        // console.log(val);

                        for (i = 1; i <= 7; i++) {
                            if (key == i) {
                                switch (i) {
                                    case 1:
                                        firstDate2 = val;
                                        break;
                                    case 2:
                                        secondDate2 = val;
                                        break;
                                    case 3:
                                        thirdDate2 = val;
                                        break;
                                    case 4:
                                        fourthDate2 = val;
                                        break;
                                    case 5:
                                        fifthDate2 = val;
                                        break;
                                    case 6:
                                        sixthdDate2 = val;
                                        break;
                                    case 7:
                                        seventhDate2 = val;
                                        break;
                                    case 8:
                                        eightDate2 = val;
                                        break;
                                    case 9:
                                        ninehDate2 = val;
                                        break;
                                    case 10:
                                        tenthDate2 = val;
                                        break;
                                    case 11:
                                        eleventhDate2 = val;
                                        break;
                                    case 12:
                                        twelevthDate2 = val;
                                        break;
                                    case 13:
                                        thirteenDate2 = val;
                                        break;
                                    case 14:
                                        fourteenDate2 = val;
                                        break;
                                    case 15:
                                        fifteenDate2 = val;
                                        break;
                                    case 16:
                                        sixteenDate2 = val;
                                        break;
                                    case 17:
                                        seventeenDate2 = val;
                                        break;
                                    case 18:
                                        eighteenDate2 = val;
                                        break;
                                    case 19:
                                        nineteenDate2 = val;
                                        break;
                                    case 20:
                                        twentyDate2 = val;
                                        break;
                                    case 21:
                                        twentyonenDate2 = val;
                                        break;
                                    case 22:
                                        twentytwoDate2 = val;
                                        break;
                                    case 23:
                                        twentythreeDate2 = val;
                                        break;
                                    case 24:
                                        twentyfourDate2 = val;
                                        break;
                                    case 25:
                                        twentyfiveDate2 = val;
                                        break;
                                    case 26:
                                        twentysixDate2 = val;
                                        break;
                                    case 27:
                                        twentysevenDate2 = val;
                                        break;
                                    case 28:
                                        twentyeightDate2 = val;
                                        break;
                                    case 29:
                                        twentynineDate2 = val;
                                        break;
                                    case 30:
                                        thrirtyDate2 = val;
                                        break;
                                    case 31:
                                        thrirtyoneDate2 = val;
                                        break;
                                    default:
                                        // code block
                                }
                            }
                        }
                    });
                    $.each(r.privatevideo, function(key, val) {
                        // console.log(key);
                        // console.log(val);

                        for (i = 1; i <= 7; i++) {
                            if (key == i) {
                                if (key == i) {
                                    switch (i) {
                                        case 1:
                                            firstDate3 = val;
                                            break;
                                        case 2:
                                            secondDate3 = val;
                                            break;
                                        case 3:
                                            thirdDate3 = val;
                                            break;
                                        case 4:
                                            fourthDate3 = val;
                                            break;
                                        case 5:
                                            fifthDate3 = val;
                                            break;
                                        case 6:
                                            sixthdDate3 = val;
                                            break;
                                        case 7:
                                            seventhDate3 = val;
                                            break;
                                        case 8:
                                            eightDate3 = val;
                                            break;
                                        case 9:
                                            ninehDate3 = val;
                                            break;
                                        case 10:
                                            tenthDate3 = val;
                                            break;
                                        case 11:
                                            eleventhDate3 = val;
                                            break;
                                        case 12:
                                            twelevthDate3 = val;
                                            break;
                                        case 13:
                                            thirteenDate3 = val;
                                            break;
                                        case 14:
                                            fourteenDate3 = val;
                                            break;
                                        case 15:
                                            fifteenDate3 = val;
                                            break;
                                        case 16:
                                            sixteenDate3 = val;
                                            break;
                                        case 17:
                                            seventeenDate3 = val;
                                            break;
                                        case 18:
                                            eighteenDate3 = val;
                                            break;
                                        case 19:
                                            nineteenDate3 = val;
                                            break;
                                        case 20:
                                            twentyDate3 = val;
                                            break;
                                        case 21:
                                            twentyonenDate3 = val;
                                            break;
                                        case 22:
                                            twentytwoDate3 = val;
                                            break;
                                        case 23:
                                            twentythreeDate3 = val;
                                            break;
                                        case 24:
                                            twentyfourDate3 = val;
                                            break;
                                        case 25:
                                            twentyfiveDate3 = val;
                                            break;
                                        case 26:
                                            twentysixDate3 = val;
                                            break;
                                        case 27:
                                            twentysevenDate3 = val;
                                            break;
                                        case 28:
                                            twentyeightDate3 = val;
                                            break;
                                        case 29:
                                            twentynineDate3 = val;
                                            break;
                                        case 30:
                                            thrirtyDate3 = val;
                                            break;
                                        case 31:
                                            thrirtyoneDate3 = val;
                                            break;
                                        default:
                                            // code block
                                    }
                                }
                            }
                        }
                    });

                    Highcharts.chart('container', {
                        chart: {
                            type: 'areaspline'
                        },
                        title: {
                            text: 'Total Earnings'
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'left',
                            verticalAlign: 'top',
                            x: 150,
                            y: 100,
                            floating: true,
                            borderWidth: 1,
                            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
                        },
                        xAxis: {
                            categories: [
                                d.firstDate,
                                d.secondDate,
                                d.thirdDate,
                                d.fourthhDate,
                                d.fifthhDate,
                                d.sixthDate,
                                d.seventhDate,
                                d.eightDate,
                                d.ninehDate,
                                d.tenthDate,
                                d.eleventhDate,
                                d.twelevthDate,
                                d.thirteenDate,
                                d.fourteenDate,
                                d.fifteenDate,
                                d.sixteenDate,
                                d.seventeenDate,
                                d.eighteenDate,
                                d.nineteenDate,
                                d.twentyDate,
                                d.twentyonenDate,
                                d.twentytwoDate,
                                d.twentythreeDate,
                                d.twentyfourDate,
                                d.twentyfiveDate,
                                d.twentysixDate,
                                d.twentysevenDate,
                                d.twentyeightDate,
                                d.twentynineDate,
                                d.thrirtyDate,
                                d.thrirtyoneDate
                            ],
                            plotBands: [{ // visualize the weekend
                                from: 4.5,
                                to: 6.5,
                                color: 'rgba(68, 170, 213, .2)'
                            }]
                        },
                        yAxis: {
                            title: {
                                text: 'Total Earnings'
                            }
                        },
                        tooltip: {
                            shared: true,
                            valueSuffix: ' units'
                        },
                        credits: {
                            enabled: false
                        },
                        plotOptions: {
                            areaspline: {
                                fillOpacity: 0.5
                            }
                        },
                        series: [{
                                name: 'Followers',
                                // data: [1, 2, 3, 4, 5, 6, 7]
                                data: [thrirtyoneDate, thrirtyDate, twentynineDate, twentyeightDate, twentysevenDate, twentysixDate, twentyfiveDate, twentyfourDate, twentythreeDate, twentytwoDate, twentyonenDate, twentyDate, nineteenDate, eighteenDate, seventeenDate, sixteenDate, fifteenDate, fourteenDate, thirteenDate, twelevthDate, eleventhDate, tenthDate, ninehDate, eightDate, seventhDate, sixthdDate, fifthDate, fourthDate, thirdDate, secondDate, firstDate]
                            },
                            {
                                name: 'Custom Video',
                                data: [thrirtyoneDate2, thrirtyDate2, twentynineDate2, twentyeightDate2, twentysevenDate2, twentysixDate2, twentyfiveDate2, twentyfourDate2, twentythreeDate2, twentytwoDate2, twentyonenDate2, twentyDate2, nineteenDate2, eighteenDate2, seventeenDate2, sixteenDate2, fifteenDate2, fourteenDate2, thirteenDate2, twelevthDate2, eleventhDate2, tenthDate2, ninehDate2, eightDate2, seventhDate2, sixthdDate2, fifthDate2, fourthDate2, thirdDate2, secondDate2, firstDate]
                            },
                            {
                                name: 'Private Video',
                                data: [thrirtyoneDate3, thrirtyDate3, twentynineDate3, twentyeightDate3, twentysevenDate3, twentysixDate3, twentyfiveDate3, twentyfourDate3, twentythreeDate3, twentytwoDate3, twentyonenDate3, twentyDate3, nineteenDate3, eighteenDate3, seventeenDate3, sixteenDate3, fifteenDate3, fourteenDate3, thirteenDate3, twelevthDate3, eleventhDate3, tenthDate3, ninehDate3, eightDate3, seventhDate3, sixthdDate3, fifthDate3, fourthDate3, thirdDate3, secondDate3, firstDate]
                            }
                        ]
                    });
                }
=======
                'filter': 'last_seven_days'
            },
            success: function(r) {
                // console.log(r);
                var d = getLastSevenDaysDate();
                $.each(r.modelsData, function(key, val) {
                    // console.log(key);
                    // console.log(val);

                    for (i = 1; i <= 7; i++) {
                        if (key == i) {
                            switch (i) {
                                case 1:
                                    firstDate = val;
                                    break;
                                case 2:
                                    secondDate = val;
                                    break;
                                case 3:
                                    thirdDate = val;
                                    break;
                                case 4:
                                    fourthDate = val;
                                    break;
                                case 5:
                                    fifthDate = val;
                                    break;
                                case 6:
                                    sixthdDate = val;
                                    break;
                                case 7:
                                    seventhDate = val;
                                    break;
                                default:
                                    // code block
                            }
                        }
                    }
                });
                $.each(r.customvideo, function(key, val) {
                    // console.log(key);
                    // console.log(val);

                    for (i = 1; i <= 7; i++) {
                        if (key == i) {
                            switch (i) {
                                case 1:
                                    firstDate2 = val;
                                    break;
                                case 2:
                                    secondDate2 = val;
                                    break;
                                case 3:
                                    thirdDate2 = val;
                                    break;
                                case 4:
                                    fourthDate2 = val;
                                    break;
                                case 5:
                                    fifthDate2 = val;
                                    break;
                                case 6:
                                    sixthdDate2 = val;
                                    break;
                                case 7:
                                    seventhDate2 = val;
                                    break;
                                default:
                                    // code block
                            }
                        }
                    }
                });
                $.each(r.privatevideo, function(key, val) {
                    // console.log(key);
                    // console.log(val);

                    for (i = 1; i <= 7; i++) {
                        if (key == i) {
                            switch (i) {
                                case 1:
                                    firstDate3 = val;
                                    break;
                                case 2:
                                    secondDate3 = val;
                                    break;
                                case 3:
                                    thirdDate3 = val;
                                    break;
                                case 4:
                                    fourthDate3 = val;
                                    break;
                                case 5:
                                    fifthDate3 = val;
                                    break;
                                case 6:
                                    sixthdDate3 = val;
                                    break;
                                case 7:
                                    seventhDate3 = val;
                                    break;
                                default:
                                    // code block
                            }
                        }
                    }
                });

                Highcharts.chart('container', {
                    chart: {
                        type: 'areaspline'
                    },
                    title: {
                        text: 'Total Earnings'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        verticalAlign: 'top',
                        x: 150,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
                    },
                    xAxis: {
                        categories: [
                            d.firstDate,
                            d.secondDate,
                            d.thirdDate,
                            d.fourthhDate,
                            d.fifthhDate,
                            d.sixthDate,
                            d.seventhDate
                        ],
                        plotBands: [{ // visualize the weekend
                            from: 4.5,
                            to: 6.5,
                            color: 'rgba(68, 170, 213, .2)'
                        }]
                    },
                    yAxis: {
                        title: {
                            text: 'Total Earnings'
                        }
                    },
                    tooltip: {
                        shared: true,
                        valueSuffix: ' units'
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        areaspline: {
                            fillOpacity: 0.5
                        }
                    },
                    series: [{
                            name: 'Followers',
                            // data: [1, 2, 3, 4, 5, 6, 7]
                            data: [seventhDate, sixthdDate, fifthDate, fourthDate, thirdDate, secondDate, firstDate]
                        },
                        {
                            name: 'Custom Video',
                            data: [seventhDate2, sixthdDate2, fifthDate2, fourthDate2, thirdDate2, secondDate2, firstDate2]
                        },
                        {
                            name: 'Private Video',
                            data: [seventhDate3, sixthdDate3, fifthDate3, fourthDate3, thirdDate3, secondDate3, firstDate3]
                        }
                    ]
                });
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

            },
            error: function() {

            }
        });
        // console.log(arr);
        //   return arr;

    }
<<<<<<< HEAD

    function dateRange(startDate, endDate) {
        var start = startDate.split('-');
        var end = endDate.split('-');

        var startYear = parseInt(start[0]);
        var endYear = parseInt(end[0]);
        var dates = [];

        for (var i = startYear; i <= endYear; i++) {
            var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
            var startMon = i === startYear ? parseInt(start[1]) - 1 : 0;
            for (var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
                var month = j + 1;
                var displayMonth = month < 10 ? '0' + month : month;
                dates.push([i, displayMonth, '01'].join('-'));
            }
        }

        return dates;
    }
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
</script>