
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Chart -->
            <div class="row">
                <div class="col-md-6 col-sm-12 col-lg-3">
                    <div class="loader" id="loader" style="">
                        <img src="<?php echo base_url('themes') . '/loading.gif'; ?>">
                    </div>
                </div>
            </div>
        </div>
         <div class="content-body site-setting"><!-- Chart -->
            <section class="site-pass">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 login-form">
                            <form action="<?php echo base_url('account/change_password')?>" method="post">
                                <div class="form-group">
                                <input type="hidden" id="userId" name="id" value="<?php echo $user_id;?>">
                                    <input type="password" class="form-control3" placeholder="Old Password" name="old_password" autocomplete="off" data-parsley-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="password" id="password" class="form-control3" name="vPassword" placeholder="<?php echo $this->lang->line('placeholder_new_password');?>" autocomplete="off" data-parsley-required="true" data-parsley-equalto="#confirm_password" data-parsley-required-message="<?php echo $this->lang->line('err_pwd_required');?>" data-parsley-equalto-message="<?php echo $this->lang->line('err_pswrd_same_cnfrm_pswrd');?>" data-parsley-maxlength="16" data-parsley-maxlength-message="<?php echo $this->lang->line('err_pwd_maxlength'); ?>" data-parsley-minlength="6" data-parsley-minlength-message="<?php echo $this->lang->line('err_pwd_minlength'); ?>">
                                </div>
                                <div class="form-group">
                                    <input type="password" id="confirm_password" class="form-control3" name="cPassword" placeholder="<?php echo $this->lang->line('placeholder_confirm_password');?>" autocomplete="off" data-parsley-required="true" data-parsley-equalto="#password" data-parsley-required-message="<?php echo $this->lang->line('err_confirm_pwd_required');?>" data-parsley-equalto-message="<?php echo $this->lang->line('cnfrm_pswrd_same_err_pswrd');?>" data-parsley-maxlength="16" data-parsley-maxlength-message="<?php echo $this->lang->line('err_pwd_maxlength'); ?>" data-parsley-minlength="6" data-parsley-minlength-message="<?php echo $this->lang->line('err_pwd_minlength'); ?>">
                                </div>
                                <button type="submit">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
         </div>
    </div>
</div>
