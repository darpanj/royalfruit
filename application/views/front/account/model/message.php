<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-lg-3">
                    <div class="loader" id="loader" style="">
                        <img src="<?php echo base_url('themes') . '/loading.gif'; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body site-setting">
            <!-- Message Page -->
            <section class="site-pass">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 login-form">
                            <div class="messaging">
                                <div class="inbox_msg">
                                    <div class="inbox_people">
                                        <div class="headind_srch">
                                            <div class="srch_bar">
                                                <input type="search" name="search_model" id="search_model">
                                                <i class="fa fa-search text-white chat-srh" aria-hidden="true"></i>
                                                <div class="cus-select pre-cus-lb">
                                                    <label style="width: 100%;margin-top: 10px;">
                                                      <select id="typeDrop">
                                                        <option value="0">All</option>
                                                        <option value="1">Subscribers</option>
                                                        <option value="2">Model</option>
                                                      </select>
                                                    </label>
                                                </div>
                                                <!-- <select class="form-control" id="typeDrop">
                                                    <option value="0">All</option>
                                                    <option value="1">Subscribers</option>
                                                    <option value="2">Models</option>
                                                </select> -->
                                            </div>
                                        </div>
                                        <div class="inbox_chat">
                                            <?php if(!empty($chatUsers)) { ?>
                                                <?php 
                                                    foreach($chatUsers as $kCU => $vCU) { 
                                                        $date = date("d M", strtotime($vCU['created_date']));
                                                        if($vCU['type'] == 'model') {
                                                            $uImage = checkImage(5, $vCU['profile_image']);
                                                        } else {
                                                            $uImage = checkImage(1, $vCU['profile_image']);
                                                        }
                                                ?>
                                                <div class="chat_list active_chat" onclick="getChat(<?= $vCU['thread_id'] ?>)" id="<?= $vCU['thread_id'] ?>">
                                                    <div class="chat_people">
                                                        <div class="chat_img"> <img src="<?php echo $uImage; ?>" alt="<?= $vCU['user_name'] ?>"> </div>
                                                        <div class="chat_ib">
                                                            <h5><?= $vCU['user_name'] ?> <span class="chat_date"><?= $date ?></span></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="mesgs chatBlock">
                                        <div class="msg_history">
                                        <?php if(!empty($chatMessages)) { ?>
                                            <input type="hidden" name="model_follower_id" id="model_follower_id" value="<?= $model_follower_id ?>">
                                            <?php 
                                                foreach($chatMessages as $kCM => $vCM) {
                                                $userId = !empty($this->user_id) ? $this->user_id : 0;
                                                $image = checkImage(2, $vCM['profile_image']);
                                                $date = date("M d", strtotime($vCM['created_at']));
                                                $time = date("h:i A", strtotime($vCM['created_at']));
                                            ?>
                                            <?php if($vCM['sender_id'] == $userId) { ?>
                                            <div class="outgoing_msg">
                                                <div class="sent_msg">
                                                    <p><?= $vCM['message_text'] ?></p>
                                                    <span class="time_date"> <?= $time ?>    |    <?= $date ?></span> </div>
                                            </div>
                                            <?php } else { ?>
                                            <div class="incoming_msg">
                                                <div class="incoming_msg_img"> <img src="<?= $image ?>" alt="sunil"> </div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p><?= $vCM['message_text'] ?></p>
                                                        <span class="time_date"> <?= $time ?>    |    <?= $date ?></span></div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        </div>
                                        <div class="type_msg">
                                            <form class="sendMessage" id="sendMessage" name="sendMessage" method="post" action="<?= site_url('account/send_message_from_model') ?>">
                                            <div class="input_msg_write">
                                                <input type="text" class="write_msg" name="message_text" id="message_text" placeholder="Type a message" />
                                                <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hsubmitidden="true"></i></button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript">
var interval = '';
var intervalChatUser = '';
$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);

var searchRequest = null;

$(function () {
    $("#search_model").keyup(function () {
        clearInterval(intervalChatUser);
        var that = this,
        value = $(this).val();
        if(value === '') {
            intervalChatUser = setInterval(function() {
               ajaxGetChatUser();
            }, 5000);
        }
            searchRequest = $.ajax({
                type: "GET",
                url: "<?= site_url('account/get_chat_user') ?>",
                data: {
                    'search_keyword' : value
                },
                dataType: "text",
                success: function(msg){
                    var res = JSON.parse(msg);
                    if(res.status === 200) {
                        if(res.html != '') {
                            $(".inbox_chat").empty();
                            $(".inbox_chat").html(res.html);
                        }
                    }
                    if(res.status === 412) {
                        $(".inbox_chat").empty();
                        $(".inbox_chat").html('<h3 style="font-size: 18px;text-align: center;">No models found.</h3>');
                    }
                }
            });
    });

    $("#typeDrop").change(function () {
        clearInterval(intervalChatUser);
        value = $(this).val();
        if(value === '') {
            intervalChatUser = setInterval(function() {
               ajaxGetChatUser();
            }, 5000);
        }
            searchRequest = $.ajax({
                type: "GET",
                url: "<?= site_url('account/get_chat_user_by_type') ?>",
                data: {
                    'type' : value
                },
                dataType: "text",
                success: function(msg){
                    var res = JSON.parse(msg);
                    if(res.status === 200) {
                        if(res.html != '') {
                            $(".inbox_chat").empty();
                            $(".inbox_chat").html(res.html);
                        }
                    }
                    if(res.status === 412) {
                        $(".inbox_chat").empty();
                        $(".inbox_chat").html('<h3 style="font-size: 18px;text-align: center;">No models found.</h3>');
                    }
                }
            });
    });
});

function ajaxGetChatUser() {
    $.ajax({
        type: "GET",
        url: "<?= site_url('account/get_chat_user') ?>",
        /*data: {
            'search_keyword' : value
        },
        dataType: "text",*/
        success: function(msg){
            var res = JSON.parse(msg);
            if(res.status === 200) {
                if(res.html != '') {
                    $(".inbox_chat").empty();
                    $(".inbox_chat").html(res.html);
                }
            }
            if(res.status === 412) {
                $(".inbox_chat").empty();
                $(".inbox_chat").html('<h3 style="font-size: 18px;text-align: center;">No models found.</h3>');
            }
        }
    });
}

/*$(document).ready(function(){
    intervalChatUser = setInterval(function() {
       ajaxGetChatUser();
    }, 5000);
});*/

function getChat(threadID) {
  if(threadID === '') {
    alert('Something went wrong. Please try again later.');
    return false;
  }
    $.ajax({
        type: "POST",
        url: "<?= site_url('account/get_chat_by_thread_id') ?>",
        data: {
            'thread_id' : threadID
        },
        dataType: "text",
        success: function(msg){
            var res = JSON.parse(msg);
            if(res.status === 200) {
                if(res.html != '') {
                    $(".msg_history").empty();
                    $(".msg_history").html(res.html);
                    $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);
                }
            }
            if(res.status === 412) {
                sType = getStatusText(412);
                sText = res.message;
                swal({
                    title: "Error!",
                    text: sText,
                    icon: "error",
                });
            }
        }
    });
}

/*$( ".active_chat" ).on( "click", function() {*/
$(document).on('click','.active_chat',function(){
    clearInterval(interval);
    let mf_id = $( this ).attr("id");
    if(mf_id === "") {
        return false;
    }
    interval = setInterval(function() {
       getChat(mf_id);
    }, 5000);
});

$( ".active_chat" ).on( "click", function() {});
$(document).on('click','.active_chat',function(){});

$(document).on('submit','.sendMessage',function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form.
    let message_text = $("#message_text").val();
    if(message_text === '') {
        return false;
    }
    let thread_id = $("#thread_id").val();
    var form = $(this);
    var url = form.attr('action');
    $('.msg_send_btn').attr('disabled', true);
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize() + "&thread_id=" + thread_id, // serializes the form's elements.
           success: function(data)
           {
                var res = JSON.parse(data);
                if(res.status === 200) {
                    if(res.html != '') {
                        $("#message_text").val("");
                        $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);
                    }
                }
                if(res.status === 412) {
                    sType = getStatusText(412);
                    sText = res.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                $('.msg_send_btn').attr('disabled', false);
           }
         });
});
</script>