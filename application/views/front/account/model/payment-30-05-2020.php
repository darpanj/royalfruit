<style type="text/css">
    .loader-spinner { 
        display:block;
        border: 12px solid #f3f3f3; 
        border-radius: 50%; 
        border-top: 12px solid #444444; 
        width: 70px; 
        height: 70px; 
        animation: spin 1s linear infinite; 
    }
     
    @keyframes spin { 
        100% { 
            transform: rotate(360deg); 
        } 
    } 
    
    .overlay {
        position:absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:rgba(0, 0, 0, 0.85);
        z-index:9999;
        display:none;
    }
    
    .center { 
        position: fixed; 
        top: 0; 
        bottom: 0; 
        left: 0; 
        right: 0; 
        margin: auto; 
    } 
    
    .site-wrap{
        position:relative;
    }
    
    .site-listing .list-price {
        width: 85%;
        margin: 25px auto;
    }
      
</style>

<div class='overlay' id="loader">
<div class="loader-spinner center"></div>
</div>
<div class="app-content content site-setting friends-list site-premium">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"></div>
        <section class="site-pr-video">
            <div class="container">
                <div class="row align-items-center">
                    <div class="w-100 friend-feed-main">
                        <h2 class="res-patuml">Payments</h2>
                    </div>
                </div>
            </div>
        </section>

        <div class="alert alert-danger alrt-stng">
            <p>In order to get paid, you <strong>MUST</strong> choose your payment method, fill in your banking information, and submit your W9. Click the <strong><a href="#payment-settings">Settings</a></strong> menu below to get started.</p>
        </div>

        <section class="blocked-main">
            <div class="container">
                <h6>PAYMENTS</h6>
                <div class="block-location-body mt-2 block-payment">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <span>Upcoming Payment</span>
                            <p>TBD</p>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span>Last Payment</span>
                            <p>$0.00</p>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span>Lifetime Paid</span>
                            <p>$0.00</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="payments-tab">
            <div class="prem-main d-flex prem-main2 p-0">
                <ul class="nav nav-tabs nav3 m-nv-2 w-100 justify-content-between d-flex" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Payments</a>
                    </li>
                    <li class="nav-item ml-2">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Payment Settings</a>
                    </li>
                </ul>
            </div>

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <!-- <p>hhh</p> -->
            </div>
            <div class="tab-pane fade mt-2" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row no-gutters">
                    <div class="col-lg-8 pr-2 mb-2">
                        <div class="payment-form">
                            <div class="d-flex justify-content-between align-items-center pymnt-cnt">
                                <p>PAYMENT METHOD</p>
                                <a href="javascript:;">Incompleted</a>
                            </div>
                            <form method="post" name="paymentMethodForm" id="paymentMethodForm">
                                <input type="hidden" name="id" id="id" value="<?php echo $pd_id; ?>" />
                                <div class="payment-via">
                                    <div class="cus-select pre-cus-lb">
                                        <p>Payment via</p>
                                        <label class="w-100">
                                            <select name="payment_via" class="payment_via" id="payment_via">
                                                <option value="1">ACH(Direct Bank Deposit)-US Only</option>
                                                <option value="2">Check (via mail)</option>
                                                <option value="3">Paxum (Not Recommended)</option>
                                            </select>
                                        </label>
                                        <span>Select your preferred payment method. Contact <a href="javascript:;">info@royalfruit.com</a> if you have additional questions.</span>
                                    </div>
                                </div>
                                <div class="payment-forms ach">
                                    <div class="form-group">
                                        <label for="name">Account Holder</label>
                                        <input type="text" class="form-control" placeholder="Legal Name on Bank Account" name="account_holder" id="account_holder">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="text" class="form-control" placeholder="Your email address" name="email_address" id="email_address">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank">Bank Name</label>
                                        <input type="text" class="form-control" placeholder="Your banking institution" name="bank_name" id="bank_name">
                                    </div>
                                    <div class="cus-select pre-cus-lb">
                                        <p>Account Type</p>
                                        <label class="w-100">
                                            <select name="account_type" id="account_type" name="account_type">
                                                <option value="1">Checking</option>
                                                <option value="2">Saving</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="bank">Routing Number</label>
                                        <input type="text" class="form-control" placeholder="Your Bank Routing Number" name="routing_number" id="routing_number">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank">Account Number</label>
                                        <input type="text" class="form-control" placeholder="Your Bank Account Number" name="account_number" id="account_number">
                                    </div>
                                    <button type="submit" class="update-nets payB">Update</button>
                                </div>
                                <div class="payment-forms check" style="display: none;">
                                    <div class="form-group">
                                        <label for="name">Pay To The Order Of</label>
                                        <input type="text" class="form-control" placeholder="The full name your check should be made out to" name="pay_to" id="pay_to">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Mailing Address</label>
                                        <textarea class="form-control" name="mailing_address" id="mailing_address" placeholder="Your mailing address"></textarea>
                                    </div>
                                    <button type="submit" class="update-nets payB">Update</button>
                                </div>
                                <div class="payment-forms paxum" style="display: none;">
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="text" class="form-control" placeholder="Your email address" name="paxum_email_address" id="paxum_email_address">
                                    </div>
                                    <button type="submit" class="update-nets payB">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="payment-form">
                            <div class="d-flex justify-content-between align-items-center pymnt-cnt">
                                <p>W9</p>
                                <a href="javascript:;">Incompleted</a>
                            </div>
                            <div class="w-upload">
                                <p>W9 Form Upload</p>
                                <span>Upload a completed W9 form</span>
                                <a href="javascript:;">(Download a blank W9 form)</a>
                                <div class="w-100">
                                    <input type="file" id="fileLoader" name="files" title="Load File" />
                                    <input type="button" id="btnOpenFileDialog" value="Upload" onclick="openfileDialog();" class="update-nets mt-1" />
                                </div>
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<b>Or</b>&nbsp;&nbsp;&nbsp;&nbsp;</h5>
                                <div class="w9-sign text-center">
                                    <a href="javascript:;">Sign W9 Form Online</a>
                                    <p>(Recommended)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    // SIDEBAR
    $(document).ready(function() {
        $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'right', // Choose the horizontal origin
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: false // Choose whether you can drag to open on touch screens
        });
        // START OPEN
        $('.button-collapse').sideNav('hide');
    });
</script>

<script>
    function openfileDialog() {
        $("#fileLoader").click();
    }

    /*$(document).on('change','#payment_via',function(){*/
    /*$( ".payment_via" ).on( "change", function() {*/

$(document).ready(function () {
    $("#payment_via").change(function(){
        if( $(this).val() == 2 ) {
            /*$(".ach").css("display", "none");
            $(".paxum").css("display", "none");
            $(".check").css("display", "block");*/
            $(".ach").hide();
            $(".paxum").hide();
            $(".check").show();
        } else if ($(this).val() == 3) {
            $(".ach").hide();
            $(".check").hide();
            $(".paxum").show();
        } else {
            $(".check").hide();
            $(".paxum").hide();
            $(".ach").show();
        }
    });
});


$("#paymentMethodForm").validate({
    rules: {
        payment_via: {
            required: true
        },
        account_holder: {
          required: function(element) {
            return $("#payment_via").val() == 1;
          }
        },
        email_address: {
          required: function(element) {
            return $("#payment_via").val() == 1;
          },
          email: true
        },
        bank_name: {
          required: function(element) {
            return $("#payment_via").val() == 1;
          }
        },
        account_type: {
          required: function(element) {
            return $("#payment_via").val() == 1;
          }
        },
        routing_number: {
          required: function(element) {
            return $("#payment_via").val() == 1;
          }
        },
        account_number: {
          required: function(element) {
            return $("#payment_via").val() == 1;
          }
        },
        pay_to: {
          required: function(element) {
            return $("#payment_via").val() == 2;
          }
        },
        mailing_address: {
          required: function(element) {
            return $("#payment_via").val() == 2;
          }
        },
        paxum_email_address: {
          required: function(element) {
            return $("#payment_via").val() == 3;
          },
          email: true
        },
    },
    messages: {
        payment_via: {
            required: 'Payment Via Must be Required!'
        },
        account_holder: {
            required: 'Account Holder Must be Required!',
        },
        email_address: {
            required: 'Email Address Must be Required!',
        },
        bank_name: {
            required: 'Bank Name Must be Required!',
        },
        account_type: {
            required: 'Account Type Must be Required!',
        },
        routing_number: {
            required: 'Routing Number Must be Required!',
        },
        account_number: {
            required: 'Account Number Must be Required!',
        },
        pay_to: {
            required: 'Pay To The Order Of Must be Required!',
        },
        mailing_address: {
            required: 'Mailing Address Must be Required!',
        },
        paxum_email_address: {
            required: 'Email Address Must be Required!',
        },
    },
    ignore: ":hidden",
    invalidHandler: function (event, validator) {
        var alert = $('#kt_form_1_msg');
        alert.removeClass('kt--hide').show();
    },
    submitHandler: function (form) {
        var data = new FormData($('#paymentMethodForm')[0]);
        $(".payB").attr('disabled', true);
        $(".payB").addClass('kt-spinner');
        $.ajax({
            type: 'post',
            data: data,
            dataType: "json",
            url: "<?= site_url('account/add_payment_details') ?>",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             // Show image container
             $("#loader").show();
            },
            success: function (r) {
                $(".payB").attr('disabled', false);
                $(".payB").removeClass('kt-spinner');
                if (r.status == 200) {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Success",
                        text: sText,
                        icon: "success",
                    });
                    setInterval(function () {
                        location.reload();
                    },1000);
                } else {
                    sType = getStatusText(r.status);
                    sText = r.message;
                    swal({
                        title: "Error!",
                        text: sText,
                        icon: "error",
                    });
                }
                },
                //complete: removeOverlay,
                complete:function(data){
                 // Hide image container
                 $("#loader").hide();
                }
        });
    }
});
</script>