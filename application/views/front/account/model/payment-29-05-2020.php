<div class="app-content content site-setting friends-list site-premium">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"></div>
        <section class="site-pr-video">
            <div class="container">
                <div class="row align-items-center">
                    <div class="w-100 friend-feed-main">
                        <h2 class="res-patuml">Payments</h2>
                    </div>
                </div>
            </div>
        </section>

        <div class="alert alert-danger alrt-stng">
            <p>In order to get paid, you <strong>MUST</strong> choose your payment method, fill in your banking information, and submit your W9. Click the <strong><a href="#payment-settings">Settings</a></strong> menu below to get started.</p>
        </div>

        <section class="blocked-main">
            <div class="container">
                <h6>PAYMENTS</h6>
                <div class="block-location-body mt-2 block-payment">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <span>Upcoming Payment</span>
                            <p>TBD</p>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span>Last Payment</span>
                            <p>$0.00</p>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span>Lifetime Paid</span>
                            <p>$0.00</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="payments-tab">
            <div class="prem-main d-flex prem-main2 p-0">
                <ul class="nav nav-tabs nav3 m-nv-2 w-100 justify-content-between d-flex" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Payments</a>
                    </li>
                    <li class="nav-item ml-2">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Payment Settings</a>
                    </li>
                </ul>
            </div>

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <!-- <p>hhh</p> -->
            </div>
            <div class="tab-pane fade mt-2" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row no-gutters">
                    <div class="col-lg-8 pr-2 mb-2">
                        <div class="payment-form">
                            <div class="d-flex justify-content-between align-items-center pymnt-cnt">
                                <p>PAYMENT METHOD</p>
                                <a href="javascript:;">Incompleted</a>
                            </div>
                            <div class="payment-via">
                                <div class="cus-select pre-cus-lb">
                                    <p>Payment via</p>
                                    <label class="w-100">
                                        <select>
                                            <option value="">ACH(Direct Bank Deposit)-US Only</option>
                                            <option value="">Check (via mail)</option>
                                            <option value="">Paxum (Not Recommended)</option>
                                        </select>
                                    </label>
                                    <span>Select your preferred payment method. Contact <a href="javascript:;">info@royalfruit.com</a> if you have additional questions.</span>
                                </div>
                            </div>
                            <div class="payment-forms payment-via">
                                <form action="/action_page.php">
                                    <div class="form-group">
                                        <label for="name">Account Holder</label>
                                        <input type="text" class="form-control" placeholder="Legal Name on Bank Account">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="text" class="form-control" placeholder="Your email address">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank">Bank Name</label>
                                        <input type="text" class="form-control" placeholder="Your banking institution">
                                    </div>
                                    <div class="cus-select pre-cus-lb">
                                        <p>Payment via</p>
                                        <label class="w-100">
                                            <select>
                                                <option value="">Checking</option>
                                                <option value="">Saving</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="bank">Routing Number</label>
                                        <input type="text" class="form-control" placeholder="Your Bank Routing Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank">Account Number</label>
                                        <input type="text" class="form-control" placeholder="Your Bank Account Number">
                                    </div>
                                    <button type="button" class="update-nets">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="payment-form">
                            <div class="d-flex justify-content-between align-items-center pymnt-cnt">
                                <p>W9</p>
                                <a href="javascript:;">Incompleted</a>
                            </div>
                            <div class="w-upload">
                                <p>W9 Form Upload</p>
                                <span>Upload a completed W9 form</span>
                                <a href="javascript:;">(Download a blank W9 form)</a>
                                <div class="w-100">
                                    <input type="file" id="fileLoader" name="files" title="Load File" />
                                    <input type="button" id="btnOpenFileDialog" value="Upload" onclick="openfileDialog();" class="update-nets mt-1" />
                                </div>
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<b>Or</b>&nbsp;&nbsp;&nbsp;&nbsp;</h5>
                                <div class="w9-sign text-center">
                                    <a href="javascript:;">Sign W9 Form Online</a>
                                    <p>(Recommended)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    // SIDEBAR
    $(document).ready(function() {
        $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'right', // Choose the horizontal origin
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: false // Choose whether you can drag to open on touch screens
        });
        // START OPEN
        $('.button-collapse').sideNav('hide');
    });
</script>

<script>
    function openfileDialog() {
        $("#fileLoader").click();
    }
</script>