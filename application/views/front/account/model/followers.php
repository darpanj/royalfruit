<div class="site-setting site-feed friend_feed">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Chart -->
            </div>
            <section class="site-pr-video">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="w-100 friend-feed-main">
                            <h2><i class="fa fa-rss" aria-hidden="true"></i>&nbsp;&nbsp;Followers</h2>


                        </div>
                    </div>
                </div>
            </section>
            <section class="feed-video">
                <div class="container">
                    <input type="hidden" id="postIds" value="0">
                    <div class="row" id="post_list">

                    </div>
                </div>
            </section>
            <!-- end -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        /* for load modal first times */
        let limit = 9;
        let target = $("#post_list");
        let postIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'post_ids': postIds
            },
            dataType: "json",
            url: "<?php echo base_url('account/load_followers');  ?>",
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(r) {
                let html = '';
                $.each(r.users, function(key, value) {

                    html += make_image_post(value);

                    postIds += ',' + value.id;
                });

                $(target).html(html);
                $("#loader").hide();
                $('#postIds').val(postIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });
        /* when scroll than load more modals */
        $(window).scroll(function() {
            let postIds1 = $('#postIds').val();
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                let postIds = '0';
                $("#loader").show();
                let final_data = {
                    'limit': limit,
                    'post_ids': postIds1
                };
                $.ajax({
                    type: 'post',
                    data: final_data,
                    dataType: "json",
                    url: "<?php echo base_url('account/load_followers');  ?>",
                    beforeSend: function() {
                        $("#loader").show();
                    },
                    success: function(r) {
                        var html = '';
                        $.each(r.users, function(key, value) {
                            html += make_image_post(value);
                            postIds += ',' + value.id;
                        });
                        $(target).append(html);
                        $('#postIds').val(postIds1 + ',' + postIds);
                        $("#loader").hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });
    });

    function make_image_post(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        var html = '';
        html += `<div class='col-md-12 col-sm-12 col-lg-4 mt-2'>`;
        html += "<div class='feed-main'>";
        html += "<div class='position-relative overlay follow'>";
        //html += `<img src="${SITE_IMG}uploads/models/${value.post_image}" alt='model'>`;
        html += "</div>";
        html += "<div class='d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1'>";
        html += "<div class='d-flex align-items-center'>";
        html += `<img src="${SITE_IMG}uploads/users/${value.profile_image}"  alt='user' width='30' height='30'>`;
        html += "<div class='pro-cnt'>";
        html += `<h2>${value.name}</h2>`;
        html += "</div>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        return html;
    }
</script>