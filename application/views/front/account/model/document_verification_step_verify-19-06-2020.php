<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $headTitle; ?> | <?php echo SITENAME; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/admin/image/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=devanagari,latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/vendors.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/app-lite.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>style2.css">
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_THEME_ASSETS; ?>css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
    <link href="<?php echo base_url(ADM_CSS . 'circle_crop.css'); ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-3.3.1.min.js"></script>

    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . ADM_THEME_VENDOR; ?>custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css">
    
    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>ui-toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script src="<?php echo base_url() . FRONT_JS; ?>bootstrap.min.js"></script>
    <!-- Newly Added Starts Here -->
    <link rel="stylesheet" href="<?php echo base_url() . FRONT_CSS; ?>intlTelInput.css"> 
    <!-- wizrad -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_arrows.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_circles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . FRONT_CSS; ?>smart_wizard_theme_dots.css">
    <!-- Newly Added Ends Here -->
</head>

<body class="vertical-layout site-feed vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="site-wrap">
        <input type="hidden" id="usedId" value="<?php echo check_variable_value($user_id); ?>">
        <!-- main section starts -->
        <?php
        if ($header_panel) {
            if ($user_id > 0) {
                $this->load->view('front/header');
            }
        }
        ?>
        <!-- <script src="<?php echo base_url() . FRONT_JS; ?>intlTelInput.js"></script>

        <script>
            var input = document.querySelector("#phone");
            window.intlTelInput(input, {

              utilsScript: "<?php echo base_url() . FRONT_JS; ?>utils.js",
            });
        </script> -->
          
        <div class="app-content content site-setting friends-list site-premium site-acnt-step ml-0">
          <div class="content-wrapper">
            <section class="account-step-main">
              <div class="container">
                <div class="mdl-acnt-main">
                  <div class="home-wizard">
                        <div id="smartwizard2">
                          <ul class="d-flex justify-content-around">
                            <li><a href="javascript:;"><span class="step-num">1</span>Account</a></li>
                            <li class="active"><a href="#step-2 active"><span class="step-num">2</span>Verify</a></li>
                            <li><a href="javascript:;"><span class="step-num">3</span>Review</a></li>
                            <li><a href="javascript:;"><span class="step-num">4</span>Upload</a></li>
                          </ul>
                        <div>
                        <div id="step-2" class="mt-3 wiz-step2">
                          <div class="d-flex align-items-center justify-content-between account-step-1">
                            <h3>Upload IDs</h3>
                            <span data-toggle="popover-hover" data-content="Your ID must be valid with visible birthdate. This can be your Driver's License, State ID, Military ID, or Passport." data-original-title="UPLOAD IDS" title="" class="popper-pro">!</span>
                          </div>
                          <div class="photo-id-sec">
                            <h4>1.Photo ID</h4>
                            <div class="id-img-block">
                              <img src="<?php echo base_url() . FRONT_IMG; ?>photo_id.svg">
                            </div>
                            <div class="good-ex-list">
                              <h4>✓ Good Example</h4>
                              <ul>
                                <li>Clearly show ID document with text clearly visible and with minimum background.</li>
                                <li>The image should not be edited, resized or rotated.</li>
                                <li>The file must be .PNG or .JPG under 7MB in size.</li>
                                <li>Any ID uploaded must be a valid government-issued ID with visible birthdate.</li>
                              </ul>
                            </div>
                            <div class="d-flex align-items-center">
                              <p class="phhoto-id">Photo ID<span style="color: red">*</span></p>
                              <div class="image-upload w-100">
                                <div class="file-upload-btn">
                                  Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="photo-id-sec mt-3">
                            <h4>2. Selfie with ID</h4>
                            <div class="id-img-block">
                              <img src="<?php echo base_url() . FRONT_IMG; ?>selfie_id.svg">
                            </div>
                            <div class="good-ex-list">
                              <h4>✓ Good Example</h4>
                              <ul>
                                <li>Please take a selfie holding your ID so we can clearly see your face and ID in the photo. We need this to accurately verify your identity.</li>
                              </ul>
                            </div>
                            <div class="d-flex align-items-center">
                              <p class="phhoto-id" style="width: 22%;">Selfie With ID<span style="color: red">*</span></p>
                              <div class="image-upload w-100">
                                <div class="file-upload-btn">
                                  Upload<input type="file" class="item-img file center-block hide_file" accept="image/*" name="image_input" id="image_input">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="border-btm"></div>
                          <div class="d-flex align-items-center justify-content-between account-step-1 mt-2">
                            <h3>ID Information</h3>
                            <span data-toggle="popover-hover" data-content="Your ID information will be used to prepare your legal documents and to receive payments." data-original-title="ID Information" title="" class="popper-pro">!</span>
                          </div>
                          <div class="id-inf-main">
                            <div class="set-mdl-main mt-1">
                              <label>Government Issued ID Type<span>*</span></label>
                              <div class="cus-select pre-cus-lb">
                                <label class="w-100">
                                  <select class="mb-0">
                                    <option value="">Select One</option>
                                    <option value="drivers-license">Driver's License</option>
                                    <option value="state-id">State ID</option>
                                    <option value="military-id">Military ID</option>
                                    <option value="non-us-passport">Non-US Passport</option>
                                    <option value="us-passport">US Passport</option>
                                  </select>
                                </label>
                              </div>
                            </div>
                            <div class="set-mdl-main mt-1">
                              <label>Issued By (State/Province)<span>*</span></label>
                              <input type="text" name="" class="mb-0">
                            </div>
                            <div class="set-mdl-main mt-1">
                              <label>ID Number<span>*</span></label>
                              <input type="text" name="" class="mb-0">
                            </div>
                            <label style="color: #000;font-weight: 500;" class="mt-1">ID Expiration</label>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="set-mdl-main">
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="set-mdl-main">
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Day</option>
                                          <option value="01">1</option>
                                          <option value="02">2</option>
                                          <option value="03">3</option>
                                          <option value="04">4</option>
                                          <option value="05">5</option>
                                          <option value="06">6</option>
                                          <option value="07">7</option>
                                          <option value="08">8</option>
                                          <option value="09">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>
                                          <option value="21">21</option>
                                          <option value="22">22</option>
                                          <option value="23">23</option>
                                          <option value="24">24</option>
                                          <option value="25">25</option>
                                          <option value="26">26</option>
                                          <option value="27">27</option>
                                          <option value="28">28</option>
                                          <option value="29">29</option>
                                          <option value="30">30</option>
                                          <option value="31">31</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="set-mdl-main">
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                        <option value="2031">2031</option>
                                        <option value="2032">2032</option>
                                        <option value="2033">2033</option>
                                        <option value="2034">2034</option>
                                        <option value="2035">2035</option>
                                        <option value="2036">2036</option>
                                        <option value="2037">2037</option>
                                        <option value="2038">2038</option>
                                        <option value="2039">2039</option>
                                        <option value="2040">2040</option>
                                        <option value="2041">2041</option>
                                        <option value="2042">2042</option>
                                        <option value="2043">2043</option>
                                        <option value="2044">2044</option>
                                        <option value="2045">2045</option>
                                        <option value="2046">2046</option>
                                        <option value="2047">2047</option>
                                        <option value="2048">2048</option>
                                        <option value="2049">2049</option>
                                        <option value="2050">2050</option>
                                        <option value="2051">2051</option>
                                        <option value="2052">2052</option>
                                        <option value="2053">2053</option>
                                        <option value="2054">2054</option>
                                        <option value="2055">2055</option>
                                        <option value="2056">2056</option>
                                        <option value="2057">2057</option>
                                        <option value="2058">2058</option>
                                        <option value="2059">2059</option>
                                        <option value="2060">2060</option>
                                        <option value="2061">2061</option>
                                        <option value="2062">2062</option>
                                        <option value="2063">2063</option>
                                        <option value="2064">2064</option>
                                        <option value="2065">2065</option>
                                        <option value="2066">2066</option>
                                        <option value="2067">2067</option>
                                        <option value="2068">2068</option>
                                        <option value="2069">2069</option>
                                        <option value="2070">2070</option>
                                        <option value="2071">2071</option>
                                        <option value="2072">2072</option>
                                        <option value="2073">2073</option>
                                        <option value="2074">2074</option>
                                        <option value="2075">2075</option>
                                        <option value="2076">2076</option>
                                        <option value="2077">2077</option>
                                        <option value="2078">2078</option>
                                        <option value="2079">2079</option>
                                        <option value="2080">2080</option>
                                        <option value="2081">2081</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-12 mt-1">
                                <label class="control control--checkbox">No Expiration
                                  <input type="checkbox">
                                  <span class="control__indicator"></span>
                                </label>
                              </div>
                            </div>
                            <div class="set-mdl-main">
                              <label>Full Legal Name<span>*</span></label>
                              <input type="text" name="">
                            </div>
                            <label style="color: #000;font-weight: 500;">Date of Birth<span style="color: red;">*</span></label>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="set-mdl-main">
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="set-mdl-main">
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Day</option>
                                          <option value="01">1</option>
                                          <option value="02">2</option>
                                          <option value="03">3</option>
                                          <option value="04">4</option>
                                          <option value="05">5</option>
                                          <option value="06">6</option>
                                          <option value="07">7</option>
                                          <option value="08">8</option>
                                          <option value="09">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>
                                          <option value="21">21</option>
                                          <option value="22">22</option>
                                          <option value="23">23</option>
                                          <option value="24">24</option>
                                          <option value="25">25</option>
                                          <option value="26">26</option>
                                          <option value="27">27</option>
                                          <option value="28">28</option>
                                          <option value="29">29</option>
                                          <option value="30">30</option>
                                          <option value="31">31</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="set-mdl-main">
                                  <div class="cus-select pre-cus-lb">
                                    <label class="w-100">
                                      <select class="mb-0">
                                        <option value="">Year</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>
                                        <option value="1999">1999</option>
                                        <option value="1998">1998</option>
                                        <option value="1997">1997</option>
                                        <option value="1996">1996</option>
                                        <option value="1995">1995</option>
                                        <option value="1994">1994</option>
                                        <option value="1993">1993</option>
                                        <option value="1992">1992</option>
                                        <option value="1991">1991</option>
                                        <option value="1990">1990</option>
                                        <option value="1989">1989</option>
                                        <option value="1988">1988</option>
                                        <option value="1987">1987</option>
                                        <option value="1986">1986</option>
                                        <option value="1985">1985</option>
                                        <option value="1984">1984</option>
                                        <option value="1983">1983</option>
                                        <option value="1982">1982</option>
                                        <option value="1981">1981</option>
                                        <option value="1980">1980</option>
                                        <option value="1979">1979</option>
                                        <option value="1978">1978</option>
                                        <option value="1977">1977</option>
                                        <option value="1976">1976</option>
                                        <option value="1975">1975</option>
                                        <option value="1974">1974</option>
                                        <option value="1973">1973</option>
                                        <option value="1972">1972</option>
                                        <option value="1971">1971</option>
                                        <option value="1970">1970</option>
                                        <option value="1969">1969</option>
                                        <option value="1968">1968</option>
                                        <option value="1967">1967</option>
                                        <option value="1966">1966</option>
                                        <option value="1965">1965</option>
                                        <option value="1964">1964</option>
                                        <option value="1963">1963</option>
                                        <option value="1962">1962</option>
                                        <option value="1961">1961</option>
                                        <option value="1960">1960</option>
                                        <option value="1959">1959</option>
                                        <option value="1958">1958</option>
                                        <option value="1957">1957</option>
                                        <option value="1956">1956</option>
                                        <option value="1955">1955</option>
                                        <option value="1954">1954</option>
                                        <option value="1953">1953</option>
                                        <option value="1952">1952</option>
                                        <option value="1951">1951</option>
                                        <option value="1950">1950</option>
                                        <option value="1949">1949</option>
                                        <option value="1948">1948</option>
                                        <option value="1947">1947</option>
                                        <option value="1946">1946</option>
                                        <option value="1945">1945</option>
                                        <option value="1944">1944</option>
                                        <option value="1943">1943</option>
                                        <option value="1942">1942</option>
                                        <option value="1941">1941</option>
                                        <option value="1940">1940</option>
                                        <option value="1939">1939</option>
                                        <option value="1938">1938</option>
                                        <option value="1937">1937</option>
                                        <option value="1936">1936</option>
                                        <option value="1935">1935</option>
                                        <option value="1934">1934</option>
                                        <option value="1933">1933</option>
                                        <option value="1932">1932</option>
                                        <option value="1931">1931</option>
                                        <option value="1930">1930</option>
                                      </select>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="border-btm w-100"></div>
                              </div>
                            </div>
                          </div>
                          <div class="d-flex align-items-center justify-content-between account-step-1 mt-2">
                            <h3>Aliases/Stage Names</h3>
                            <span data-toggle="popover-hover" data-content="Your alias or other names you are known or have been known by, including stage names, Internet usernames, aliases, nicknames, and married or maiden names. This is optional." data-original-title="Aliases/Stage Names" title="" class="popper-pro">!</span>
                          </div>
                          <div class="alisa-main">
                            <div class="set-mdl-main mt-1">
                              <label>Your Alias/Stage Names (enter at least one)</label>
                              <input type="text" name="" class="mb-0" placeholder="Alisa or Stage Name"> 
                              <input type="text" name="" class="mb-0 mt-1" placeholder="Other Alisa or Stage Name">
                            </div>
                            <div class="d-flex sec-stepn-btn sec-stepn-btn2 justify-content-between">
                              <a href="document_step1.html"><button type="submit" class="btn btn-gradient btn-sub acc-vrf-prv">Back</button></a>
                              <a href="account_review.html"><button type="submit" class="btn btn-gradient btn-sub acc-vrf-nxt">Next - Review</button></a>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </section>
          </div>
        </div>

        <!-- main section ends -->
        <div class="site-feed">
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="modelId" id="modelId">
                            <input type="hidden" name="type" id="type">
                            <!--  // 0 for token purchase 
                            // 1 for follow -->
                            <input type="hidden" name="price" id="price">
                            <img src="" id="image_model_modal" width="50" alt="model">
                            <h2>Follow <span id="model_name_modal"></span></h2>
                            <h2>$<span id="model_price"></span>/month</h2>
                            <p>You'll get access to daily private content, direct messaging, and more!</p>
                            <button type="button" class="site-folow flw2" id="purchase_now">Continue</button>
                            <p>All transactions are handled securely and discretely by our authorized merchant, <a href="" style="color: #c39940;">CentroBill.com </a>
                            </p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url() . FRONT_THEME_ASSETS_VENDOR; ?>js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->

    <script src="<?php echo base_url() . FRONT_JS; ?>jquery-ui.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.countdown.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>aos.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.sticky.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.8/js/utils.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>custom.js?<?php echo filemtime(FCPATH . ADM_JS . 'custom.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- Newly Added Starts Here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url() . FRONT_JS; ?>jquery.smartWizard.js"></script>
    <!-- Newly Added Ends Here -->
    <script>
        <?php if (!empty($this->session->flashdata('success'))) { ?>
            sType = getStatusText(200);
            sText = '<?php echo $this->session->flashdata('success'); ?>';
            //Custom.myNotification(sType, sText);
            swal({
                title: "Success",
                text: sText,
                icon: "success",
            });
        <?php } ?>
        <?php if (!empty($this->session->flashdata('error'))) { ?>
            sType = getStatusText(412);
            sText = '<?php echo $this->session->flashdata('error'); ?>';
            // Custom.myNotification(sType, sText);
            swal({
                title: "Error!",
                text: sText,
                icon: "error",
            });
        <?php } ?>
    </script>
</body>

</html>
<script>
    $(document).ready(function() {
        let country_id = $("#countries").val();
        get_countries();
        $("#countries").on('select', function() {
            var selectedText = $("#countries option:selected").attr('data-value');
            $("#country_name").val(selectedText);
        });
    });

    function get_countries() {
      let selected = '';
        $.ajax({
            url: "https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json",
            method: 'get',
            dataType: 'Json',
            success: function(r) {
                var html = "<option value='0'>Select Country</option>";
                $.each(r, function(key, val) {
                    if(val.name == '<?php echo $userData['country']; ?>') {
                      selected = 'selected'; 
                    } else {
                      selected = '';
                    }
                    html += `<option value="${val.id}" data-value="${val.name}" ${selected}>${val.name}</option>`;
                });
                $("#countries").html(html);
            },
            error: function() {
                console.log('ajax error');
            }
        });
    }

</script>

<script>
// SIDEBAR
$(document).ready(function(){
  $('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'right', // Choose the horizontal origin
      closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: false // Choose whether you can drag to open on touch screens
    }
  );
  // START OPEN
  $('.button-collapse').sideNav('hide');
});

</script>
<script>
$(function () {
  $('[data-toggle="popover-hover"]').popover({
    trigger: 'hover',
  })
})
  
</script>
<script>
  function format(item, state) {
  if (!item.id) {
    return item.text;
  }
  var countryUrl = "";
  var stateUrl = "https://oxguy3.github.io/flags/svg/us/";
  var url = state ? stateUrl : countryUrl;
  var img = $("<img>", {
    
    
  });
  var span = $("<span>", {
    text: " " + item.text
  });
  span.prepend(img);
  return span;
}

$(document).ready(function() {
  $("#countries").select2({
    templateResult: function(item) {
      return format(item, false);
    }
  });
  $("#state").select2({
    templateResult: function(item) {
      return format(item, true);
    }
  });
});

</script>

<!-- wizard -->
<script type="text/javascript">
        $(document).ready(function(){
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'arrows',
                    transitionEffect:'fade',
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            $('#smartwizard2').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: false
            });
        });
</script>

<script>
  $(document).ready(function(){
    $('#file-input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {
            
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                    return function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element 
                        $('#thumb-output').append(img); //append image to output element
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                }
            });
            
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });
  
  $(".remove").click(function (e) {
        e.preventDefault();
        data.splice(0, 1);
        $('#thumb-output a').eq(data.length).remove();
    });
});
</script>

<script>
$(document).ready(function () {
  $("#step4").click(function () {
      if ($("").hide()) {
          $("#ts1").show();
      }
  });
});
$(document).ready(function () {
  $(".back-sec").click(function () {
      if ($("").show()) {
          $("#ts1").hide();
      }
  });
});
</script>