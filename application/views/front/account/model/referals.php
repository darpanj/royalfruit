<div class="app-content content site-setting friends-list site-premium">
    <div class="content-wrapper">
        <section class="referrals-main">
            <div class="referrals-yr">
                <p class="m-0">YOUR REFERRAL URLS</p>
            </div>
            <div class="referrals-link">
                <h6>You can now refer models to any site in our social network and earn 5% commissions for life.</h6>
                <h3>RoyalFruit Referral Url</h3>
                <div class="row no-gutters">
                    <div class="col-lg-8 col-10">
                        <input type="text" id="referral-url-1" class="form-control ref-control" value="https://royalfruit.com/referral/invite?code=<?php echo $modelData['refreal_code']; ?>" readonly="">
                    </div>
                    <div class="col-lg-4 col-2 align-self-center">
                        <a href="javascript:;" onclick="myFunction()" title="Copy to clipboard" class="ml-2"><img src="<?php echo base_url() . FRONT_IMG; ?>clippy.svg" width="20"></a>
                    </div>
                </div>
                <span>*Earn from referrals by sharing this link to your friends.</span>
            </div>
            <div class="referrals-earning-perd">
                <h3>(0) Referrals - Referral Earnings (5% of Model Earnings)</h3>
            </div>
            <div class="col mb-2 p-0">
                <div class="earning-box">
                    <div class="ern-cnt">
                        <h4>TOTAL REFERRAL EARNINGS PER PERIOD</h4>
                        <span data-toggle="popover-hover" data-content="This is your estimated earnings from total referral earnings, per period.">?</span>
                    </div>
                    <div class="row rec-brhd">
                        <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                            <div class="rec-td-cnt">
                                <p class="m-0">May 16 - 31</p>
                                <h5><span>$</span>0</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                            <div class="rec-td-cnt">
                                <p class="m-0">May 1 - 15</p>
                                <h5><span>$</span>0</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                            <div class="rec-td-cnt">
                                <p class="m-0">Apr 16 - 30</p>
                                <h5><span>$</span>0</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-3 col-6">
                            <div class="rec-td-cnt">
                                <p class="m-0">Apr 1 - 15</p>
                                <h5><span>$</span>0</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'right', // Choose the horizontal origin
            closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: false // Choose whether you can drag to open on touch screens
        });
        // START OPEN
        $('.button-collapse').sideNav('hide');


    });

    $(function() {
        $('[data-toggle="popover-hover"]').popover({
            trigger: 'hover',
        })
    });

    function myFunction() {
        var copyText = document.getElementById("referral-url-1");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        swal({
            title: "Success",
            text: "Code Copied..",
            icon: "success",
        });
        //alert("Copied the text: " + copyText.value);
    }
</script>