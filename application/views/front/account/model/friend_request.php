<div class="app-content content site-setting friends-list">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"></div>
        <section class="site-pr-video">
            <div class="container">
                <div class="row align-items-center">
                    <div class="w-100 friend-feed-main">
                        <h2><img src="<?php echo base_url() . FRONT_IMG; ?>/friends.png" width="18">&nbsp;&nbsp;Friend Requests</h2>
                        <section class="prv-brw mt-2">
                            <div class="d-flex justify-content-between d-brw">
                                <p class="pl-2">This shows the status of friend requests you have made</p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <section class="frnd-list-main mt-2">
            <div class="prv-brw justify-content-center d-flex">
                <div class="d-flex justify-content-between d-brw">
                    <p class="pl-2">No pending friend requests.</p>
                </div>
        </section>
        </section>

        <section class="pending-sec">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                            <div class="pen-cnt pl-1">
                                <h4>Mary Bellavita</h4>
                                <p>9 seconds ago</p>
                                <button>Cancel Request</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                            <div class="pen-cnt pl-1">
                                <h4>Mary Bellavita</h4>
                                <p>9 seconds ago</p>
                                <button>Cancel Request</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                        <div class="pen-req-box d-flex">
                            <img src="<?php echo base_url() . FRONT_IMG; ?>/detail1.png">
                            <div class="pen-cnt pl-1">
                                <h4>Mary Bellavita</h4>
                                <p>9 seconds ago</p>
                                <button>Cancel Request</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>