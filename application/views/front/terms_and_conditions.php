<div class="site-live site-premium site-custom-video site-explore site-snap">
        <h2>Terms and Conditions</h2>
    <!-- snap-model -->
    <section class="site-profile">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 mt-2">
                    <?php
                    if (!empty($tc_content)) {
                        echo $tc_content['content'];
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- end -->