<div class="site-premium site-custom-video site-explore site-snap site-custom">

    <div class="prem-main d-flex prem-main2 prem-main4">
        <div class="cus-select">
            <label>
                <select id="filter">
                    <option value="new">Newest</option>
                    <option value="old">Oldest</option>
                </select>
            </label>
        </div>
        <!-- <select id="filter">
                <option value="new">Newest</option>
                <option value="old">Oldest</option>
            </select> -->

        <ul class="nav nav-tabs nav4" role="tablist">
            <li class="nav-item">
                <a class="active nav-link2" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><img src="<?php echo base_url() . FRONT_IMG; ?>cus1.png" alt="money"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link2" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus2.png" alt="money"></a>
            </li>
        </ul>
        <input type="hidden" id="postIds" value="0">
    </div>

    <div class="container">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row" id="home_view">

                </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row snap-model2" id="profile_view">


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        let limit = 4;
        let target = $("#home_view");
        let target2 = $("#profile_view");
        /* for load modal first times */
        let filter = $("#filter").val();
        let final_orderby;
        if (filter == 'new') {
            final_orderby = 'DESC';
        } else if (filter == 'old') {
            final_orderby = 'ASC';
        }
        let postIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'order_by': final_orderby,
                'post_ids': postIds
            },
            dataType: "json",
            url: "<?php echo base_url('home/load_posts');  ?>",
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(r) {
                let html = '';
                let html2 = '';
                $.each(r.data, function(key, value) {
                    if (value.post_type == 0) {
                        html += make_model_image_for_home(value);
                        html2 += make_model_image_for_profile(value);
                    } else if (value.post_type == 1) {
                        html += make_model_video_for_home(value);
                        html2 += make_model_video_for_profile(value);
                    }
                    postIds += ',' + value.post_id;
                });

                $(target).html(html);
                $(target2).html(html2);
                $("#loader").hide();
                $('#postIds').val(postIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });

        /* when scroll than load more modals */
        $(window).scroll(function() {
            let filter = $("#filter").val();
            if (filter == 'new') {
                let final_orderby = 'DESC';
            } else if (filter == 'old') {
                let final_orderby = 'ASC';
            }
            let postIds1 = $('#postIds').val();
            // if ($(window).scrollTop() + 1 >= $(document).height() - $(window).height()) {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                let postIds = '0';
                let postIds1 = $('#postIds').val();
                $.ajax({
                    type: 'post',
                    data: {
                        'limit': limit,
                        'order_by': final_orderby,
                        'post_ids': postIds1
                    },
                    dataType: "json",
                    url: "<?php echo base_url('home/load_posts');  ?>",
                    beforeSend: function() {
                        $('#loader').show();
                    },
                    success: function(r) {
                        let html = '';
                        let html2 = '';
                        $.each(r.data, function(key, value) {
                            if (value.post_type == 0) {
                                html += make_model_image_for_home(value);
                                html2 += make_model_image_for_profile(value);
                            } else if (value.post_type == 1) {
                                html += make_model_video_for_home(value);
                                html2 += make_model_video_for_profile(value);
                            }
                            postIds += ',' + value.post_id;
                        });
                        $(target).append(html);
                        $(target2).append(html2);
                        $('#postIds').val(postIds1 + ',' + postIds);
                        $('#loader').hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });

        /* when filter */
        $(document).on('change', '#filter', function() {
            let filter = $(this).val();
            let limit = 4;
            let target = $("#home_view");
            let target2 = $("#profile_view");
            /* for load modal first times */
            let final_orderby;
            if (filter == 'new') {
                final_orderby = 'DESC';
            } else if (filter == 'old') {
                final_orderby = 'ASC';
            }
            let postIds = '0';
            $.ajax({
                type: 'post',
                data: {
                    'limit': limit,
                    'order_by': final_orderby,
                    'post_ids': postIds
                },
                dataType: "json",
                url: "<?php echo base_url('home/load_posts');  ?>",
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(r) {
                    let html = '';
                    let html2 = '';
                    $.each(r.data, function(key, value) {
                        if (value.post_type == 0) {
                            html += make_model_image_for_home(value);
                            html2 += make_model_image_for_profile(value);
                        } else if (value.post_type == 1) {
                            html += make_model_video_for_home(value);
                            html2 += make_model_video_for_profile(value);
                        }
                        postIds += ',' + value.post_id;
                    });

                    $(target).html(html);
                    $(target2).html(html2);
                    $("#loader").hide();
                    $('#postIds').val(postIds);
                },
                error: function() {
                    console.log('ajax error');
                }
            });
        });
    });

    /* for home tab */
    function make_model_image_for_home(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
                    <div class="feed-main">
                        <div class="position-relative overlay" data-toggle="modal" data-target="#smallModal">
                            <img src="${SITE_IMG}uploads/models/${value.post_image}" alt="model" class="ex-dtl">
                        </div>`;
        // <div class="d-flex justify-content-between align-items-center mt-1 ml-1 mr-1">
        //     <div class="d-flex align-items-center">
        //         <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">
        //         <div class="pro-cnt">
        //             <h2>${value.name}</h2>
        //             <p class="m-0">${teaser_date}</p>
        //         </div>
        //     </div>
        //     <button type="button" class="site-folow follow"  data-toggle='modal' data-target='#smallModal' data-model-id=${value.id}>Follow</button>
        // </div>
        html += `</div>
                </div>`;
        return html;
    }

    function make_model_video_for_home(value) {
        let teaser_date = moment(value.teaser_created_date).fromNow();
        let html = `<div class="col-md-12 col-sm-12 col-lg-4 mt-2">
                    <div class="feed-main">
                        <video class="mmCardVid" controls>
                            <source  src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4" autoplay loop>
                            secure connection could not be established
                        </video>`;

        // <div class="d-flex justify-content-between align-items-center mt-1 ml-1 mr-1">
        //     <div class="d-flex align-items-center">
        //         <img src="${SITE_IMG}uploads/models/${value.profile_image}" alt="model" width="30" height="30">

        //         <div class="pro-cnt">
        //              <h2>${value.name}</h2>
        //             <p class="m-0">${teaser_date}</p>
        //         </div>
        //     </div>
        //     <button type="button" class="site-folow follow"  data-toggle='modal' data-target='#smallModal' data-model-id=${value.id}>Follow</button>
        // </div>
        html += `</div>
                </div>`;
        return html;
    }

    /* for profile tab */
    function make_model_image_for_profile(value) {
        let html = `<div class="col-md-12 mt-2">
                        <div class="feed-main">
                            <img src="${SITE_IMG}uploads/models/${value.post_image}" alt="image" class="ex-dtl">
                        </div>
                  </div>`;
        return html;
    }

    function make_model_video_for_profile(value) {
        let html = `<div class="col-md-12 mt-2">
                        <div class="feed-main"> 
                            <video class="mmCardVid" controls>
                                <source  src="${SITE_IMG}uploads/models/${value.post_image}" type="video/mp4" autoplay loop>
                                secure connection could not be established
                            </video>
                        </div>
                </div>`;
        return html;
    }
</script>