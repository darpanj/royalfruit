<style type="text/css">
    #requestForm {
        width: 100%;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="site-become site-custom site-premium site-explore site-detail">
    <section class="site-blocks-cover overflow-hidden">
        <img src="<?php echo checkImage(3, $modelData['cover_image']); ?>">
    </section>

    <div class="container">
        <div class="row">
            <div class="cust-main d-flex">
                <div class="cus-pro">
                    <img src="<?php echo checkImage(3, $modelData['profile_image']); ?>" alt="Model">
                </div>
                <div class="cus-cnt mt-1 ml-1">
                    <h2><?php echo $modelData['name']; ?></h2>
                    <p class="mt-0"><a href="<?php echo base_url('model') . '/' . $modelData['user_slug']; ?>"><?php echo base_url('model') . '/' . $modelData['user_slug']; ?></a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" style="justify-content: space-between;">
            <div class="col-xl-6 col-md-6">
                <p><?php echo $modelData['bio']; ?></p>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="d-flex pro-folow col-12">
                    <button type="button" data-toggle="modal" data-target="#smallModal"><img src="<?php echo base_url() . FRONT_IMG; ?>heart.svg" width="15" alt="Model">&nbsp;&nbsp;$<?php echo $modelData['follow_price']; ?>/ Month</button>
                    <button type="button" data-toggle="modal" data-target="#smallModal1"><img src="<?php echo base_url() . FRONT_IMG; ?>snap.png" width="15" alt="Model">&nbsp;&nbsp;$<?php echo $modelData['snapchat_price']; ?>/ Month</button>
                </div>
                <div class="d-flex icon-main mt-3">
                    <div class="col-md-4 pro-border justify-content-center">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>icon1.png" class="pull-left" alt="Model">
                        <h4><?php echo $modelData['total_images']; ?></h4>
                    </div>
                    <div class="col-md-4 pro-border justify-content-center">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>icon2.png" class="pull-left" alt="Model">
                        <h4><?php echo $modelData['total_videos']; ?></h4>
                    </div>
                    <div class="col-md-4 pro-border justify-content-center">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>icon3.png" class="pull-left" alt="Model">
                        <h4><?php echo $modelData['diamonds']; ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="prem-main d-flex prem-main2 mt-2 prem-main4">


        <input type="search" name="search" class="pre-srh2">
        <i class="fa fa-search pre-srh2 prem-srh1" aria-hidden="true"></i>

        <!--<ul class="nav nav-tabs nav3 my-tp1" role="tablist">
            <li class="nav-item">
                <a class="nav-link model1" href="model_feed.html"><i class="fa fa-rss" aria-hidden="true"></i>&nbsp;&nbsp;
                    Model Feed</a>
            </li>
            <li class="nav-item">
                <a class="nav-link model1" href="detail_page.html"><i class="fa fa-diamond" aria-hidden="true"></i>&nbsp;&nbsp;Premium Videos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link model1" href="custom_video.html"><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Custom Videos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link nav-link7 model1" id="grid-tab" data-toggle="tab" href="#grid" role="tab" aria-controls="grid" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus1.png" class="crus-img" alt="grid"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link7 model1" id="full-tab" data-toggle="tab" href="#full" role="tab" aria-controls="full" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus2.png" class="crus-img" alt="view"></a>
            </li>
        </ul>-->

        <ul class="nav nav-tabs nav3 my-tp1" role="tablist">
            <li class="nav-item">
                <a class="nav-link modelFeed" href="javascrit:;"><i class="fa fa-rss" aria-hidden="true"></i>&nbsp;&nbsp;
                    Model Feed</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('home/premium_video') . '/' . $modelData['user_slug']; ?>"><i class="fa fa-diamond" aria-hidden="true"></i>&nbsp;&nbsp;Premium Videos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link customVideos" href="javascript:;"><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Custom Videos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link nav-link7 model1" id="grid-tab" data-toggle="tab" href="#grid" role="tab" aria-controls="grid" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus1.png" class="crus-img" alt="grid"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link7 model1" id="full-tab" data-toggle="tab" href="#full" role="tab" aria-controls="full" aria-selected="false"><img src="<?php echo base_url() . FRONT_IMG; ?>cus2.png" class="crus-img" alt="view"></a>
            </li>
        </ul>



        <input type="search" name="search" class="pre-srh">
        <i class="fa fa-search pre-srh prem-srh1" aria-hidden="true"></i>
    </div>

    <div class="req-form">
        <div class="container">
            <img src="<?php echo base_url() . FRONT_IMG; ?>req-img.png" alt="Model">
            <h2>Request a Custom Video from <?= $modelData['name'] ?></h2>
            <h3>My Rules. What I do. What I don't.</h3>
            <?php if ($rules['rules'] != '') { ?>
                <p class="mt-3"><?= $rules['rules'] ?></p>
            <?php } else { ?>
                <p class="mt-3">N/A</p>
            <?php } ?>
            <h2>Typically responds in 2-3 days</h2>
            <h4>How much are you willing to pay?</h4>

            <div class="row">
                <div class="col-12">
                    <p>Login to your <span>Fan</span> or <span>Model</span> account below</p>
                </div>
                <form id="requestForm" name="requestForm" method="post">
                    <div class="col-12">
                        <input type="number" class="form-control1" id="price" name="price" placeholder="<?= $modelData['name'] ?>'s price range starts from $5" min="5">
                        <input type="hidden" name="model_id" id="model_id" value="<?= $modelData['id'] ?>">
                    </div>
                    <div class="col-12 cust-text">
                        <textarea class="form-control1" name="instruction" id="instruction" placeholder="Video Instructions for <?= $modelData['name'] ?>"></textarea>
                    </div>
                    <div class="col-12 mt-4">
                        <button type="submit" id="submitRequest">Request now for $</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="tab-content">

            <!-- grid view -->
            <div class="tab-pane fade" id="grid" role="tabpanel" aria-labelledby="grid-tab">
                <div class="row snap-model2">
                    <?php foreach ($model_post_images as $key => $value) { ?>
                        <?php if ($value['post_type'] == 0) { ?>
                            <div class="col-md-12 col-sm-12 col-lg-6 mt-2">
                                <div class="feed-main">
                                    <div class="position-relative overlay">
                                        <img src="<?php echo checkImage(3, $value['image']); ?>" alt="model">
                                    </div>
                                    <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                        <!-- <div class="d-flex align-items-center">
                                            <img src="<?php //echo $value['profile_image']; 
                                                        ?>" alt="model" width="30" height="30">
                                            <div class="pro-cnt">
                                                <h2>Joya</h2>
                                                <p class="m-0">2 week ago</p>
                                            </div>
                                        </div> -->
                                        <?php if ($is_follow <= 0 && $user_id > 0) { ?>
                                            <ul id="category-tabs">
                                                <li>
                                                    <?php $fav = ($value['is_fav_post'] == 1) ? 'remove_fav' : 'add_fav';
                                                    $starclass = ($value['is_fav_post'] == 1) ? 'fa fa-star' : 'fa fa-star-o';
                                                    ?>
                                                    <a href="javascript:void" id="<?php echo $fav; ?>" data-post-id="<?php echo $value['id']; ?>">
                                                        <i class="<?php echo $starclass; ?>"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <button type="button" class="site-folow follow" data-toggle="modal" data-target="#smallModal" data-model-id='<?php echo $modelData['id']; ?>'>Follow</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-12 col-sm-12 col-lg-6 mt-2">
                                <div class="feed-main">
                                    <video class="mmCardVid" controls>
                                        <source src="<?php echo base_url('themes/uploads/models/') . '/' . $value['image']; ?>" type="video/mp4">
                                        secure connection could not be established
                                    </video>
                                    <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                        <?php if ($is_follow <= 0 && $user_id > 0) { ?> <ul id="category-tabs">
                                                <li>
                                                    <?php $fav = ($value['is_fav_post'] == 1) ? 'remove_fav' : 'add_fav';
                                                    $starclass = ($value['is_fav_post'] == 1) ? 'fa fa-star' : 'fa fa-star-o';
                                                    ?>
                                                    <a href="javascript:void" id="<?php echo $fav; ?>" data-post-id="<?php echo $value['id']; ?>">
                                                        <i class="<?php echo $starclass; ?>"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <button type="button" class="site-folow follow" data-toggle="modal" data-target="#smallModal" data-model-id='<?php echo $modelData['id']; ?>'>Follow</button>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <!-- end -->

            <!-- list-view -->
            <div class="tab-pane fade" id="full" role="tabpanel" aria-labelledby="full-tab">
                <div class="row snap-model2">
                    <?php foreach ($model_post_images as $key => $value) { ?>
                        <?php if ($value['post_type'] == 0) { ?>
                            <div class="col-md-12 col-sm-12 mt-2">
                                <div class="feed-main">
                                    <div class="position-relative overlay">
                                        <img src="<?php echo checkImage(3, $value['image']); ?>" alt="model">
                                    </div>
                                    <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                        <!-- <div class="d-flex align-items-center">
                                            <img src="<?php //echo $value['profile_image']; 
                                                        ?>" alt="model" width="30" height="30">
                                            <div class="pro-cnt">
                                                <h2>Joya</h2>
                                                <p class="m-0">2 week ago</p>
                                            </div>
                                        </div> -->
                                        <?php if ($is_follow <= 0 && $user_id > 0) { ?>
                                            <ul id="category-tabs">
                                                <li>
                                                    <?php $fav = ($value['is_fav_post'] == 1) ? 'remove_fav' : 'add_fav';
                                                    $starclass = ($value['is_fav_post'] == 1) ? 'fa fa-star' : 'fa fa-star-o';
                                                    ?>
                                                    <a href="javascript:void" id="<?php echo $fav; ?>" data-post-id="<?php echo $value['id']; ?>">
                                                        <i class="<?php echo $starclass; ?>"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <button type="button" class="site-folow follow" data-toggle="modal" data-target="#smallModal" data-model-id='<?php echo $modelData['id']; ?>'>Follow</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-12 col-sm-12 mt-2">
                                <div class="feed-main">
                                    <video class="mmCardVid" controls>
                                        <source src="<?php echo base_url('themes/uploads/models/') . '/' . $value['image']; ?>" type="video/mp4">
                                        secure connection could not be established
                                    </video>
                                    <div class="d-flex position-relative justify-content-between align-items-center mt-1 ml-1 mr-1">
                                        <?php if ($is_follow <= 0 && $user_id > 0) { ?> <ul id="category-tabs">
                                                <li>
                                                    <?php $fav = ($value['is_fav_post'] == 1) ? 'remove_fav' : 'add_fav';
                                                    $starclass = ($value['is_fav_post'] == 1) ? 'fa fa-star' : 'fa fa-star-o';
                                                    ?>
                                                    <a href="javascript:void" id="<?php echo $fav; ?>" data-post-id="<?php echo $value['id']; ?>">
                                                        <i class="<?php echo $starclass; ?>"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <button type="button" class="site-folow follow" data-toggle="modal" data-target="#smallModal" data-model-id='<?php echo $modelData['id']; ?>'>Follow</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <!-- end -->

        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $(".modelFeed").click(function() {
            if ($(".tab-content").show()) {
                $(".req-form").hide();
                $("#grid").addClass('fade show active');
                $("#grid").show();
            }
        });

        $(".customVideos").click(function() {
            if ($(".tab-content").hide()) {
                $(".req-form").show();
                /*$("#grid").addClass('fade show active');
                $("#grid").show();*/
            }
        });

    });

    $("#requestForm").validate({
        rules: {
            price: {
                required: true,
                number: true
            },
            instruction: {
                required: true,
            },
        },
        messages: {
            price: {
                required: 'Price Must be Required!'
            },
            instruction: {
                required: 'Instruction Must be Required!',
            },
        },
        ignore: ":hidden",
        invalidHandler: function(event, validator) {
            var alert = $('#kt_form_1_msg');
            alert.removeClass('kt--hide').show();
        },
        submitHandler: function(form) {
            var data = new FormData($('#requestForm')[0]);
            $("#submitRequest").attr('disabled', true);
            $("#submitRequest").addClass('kt-spinner');
            $.ajax({
                type: 'post',
                data: data,
                dataType: "json",
                url: "<?= site_url('home/submit_request_for_custom_video') ?>",
                cache: false,
                contentType: false,
                processData: false,
                success: function(r) {
                    $("#submitRequest").attr('disabled', false);
                    $("#submitRequest").removeClass('kt-spinner');
                    if (r.status == 200) {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Success",
                            text: sText,
                            icon: "success",
                        });
                        setInterval(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        sType = getStatusText(r.status);
                        sText = r.message;
                        swal({
                            title: "Error!",
                            text: sText,
                            icon: "error",
                        });
                    }
                },
                complete: removeOverlay
            });
        }
    });
</script>