<section class="site-blocks-cover overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 align-self-center">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12" style="margin-top: -20%;">
                <div class="slide-one-item home-slider owl-carousel">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider1.png" alt="Image" class="img-fluid img">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider2.png" alt="Image" class="img-fluid img">
                    <img src="<?php echo base_url() . FRONT_IMG; ?>slider3.png" alt="Image" class="img-fluid img">
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container home-srh position-relative">
    <input type="text" placeholder="search model" id="search" name="search" autocomplete="off">
    <i class="fa fa-search" aria-hidden="true" id="search_models"></i>
</div>
<section class="site-feature">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-4 fea-top">
                <a href="javascript:" id="feature_models">
                    <div class="feature-list">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>image1.png">
                        <h3>Featured</h3>
                    </div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 fea-top">
                <a href="javascript:" id="newest_models">
                    <div class="feature-list">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>image1.png">
                        <h3>Newest</h3>
                    </div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 fea-top">
                <a href="javascript:" id="most_followed_models">
                    <div class="feature-list">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>image1.png">
                        <h3>Most Followed</h3>
                    </div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 fea-top">
                <a href="javascript:" id="recently_active_models">
                    <div class="feature-list">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>image1.png">
                        <h3>Recently Active</h3>
                    </div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 fea-top">
                <a href="<?php echo base_url('home/explore'); ?>" id="explore_models">
                    <div class="feature-list">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>image1.png">
                        <h3>Explore</h3>
                    </div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 fea-top">
                <a href="javascript:" id="live_now_models">
                    <div class="feature-list">
                        <img src="<?php echo base_url() . FRONT_IMG; ?>image1.png">
                        <h3>Live Now</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- end -->
<!-- online -->
<section class="site-online">
    <div class="container">
        <div class="row" id="live_model">
            <!--            <div class="col-md-2 col-sm-4 fea-top">-->
            <!--                <div class="online-list">-->
            <!--                    <img src="--><?php //echo base_url() . FRONT_IMG; 
                                                    ?>
            <!--online1.png">-->
            <!--                    <h3>Sakira</h3>-->
            <!--                    <h3>Online</h3>-->
            <!--                </div>-->
            <!--            </div>-->
        </div>
    </div>
</section>
<!-- end -->
<!-- profile -->
<input type="hidden" id="offset" value="">
<input type="hidden" id="order" value="random">
<input type="hidden" id="usedIds" value="0">

<section class="site-profile" id="models" data-total_rec="0">
    <div class="container">
        <div class="row" id="model_list">
            <!-- <div class="col-md-6 col-sm-12 col-lg-3">
            <div class="profile-box">

                <div class="box1">

                    <img src="<?php echo base_url() . FRONT_IMG; ?>profile1.png">

                </div>

                <div class="box2">

                    <div class="d-flex mt-2 ml-2">

                        <img src="<?php echo base_url() . FRONT_IMG; ?>pro1.png" width="40px">

                        <p>Joya</p>

                    </div>

                    <div class="d-flex icon-main">

                        <div class="col-md-4 pro-border">

                            <img src="<?php echo base_url() . FRONT_IMG; ?>icon1.png" class="pull-left">

                            <h4>68</h4>

                        </div>

                        <div class="col-md-4 pro-border">

                            <img src="<?php echo base_url() . FRONT_IMG; ?>icon2.png" class="pull-left">

                            <h4>68</h4>

                        </div>

                        <div class="col-md-4 pro-border">

                            <img src="<?php echo base_url() . FRONT_IMG; ?>icon3.png" class="pull-left">

                            <h4>70</h4>

                        </div>

                    </div>

                    <div class="d-flex pro-folow">

                        <button type="button"><img src="<?php echo base_url() . FRONT_IMG; ?>heart.svg" width="15">&nbsp;&nbsp;Follow</button>

                        <button type="button"><img src="<?php echo base_url() . FRONT_IMG; ?>snap.png" width="15">&nbsp;&nbsp;Snapchat</button>

                    </div>

                </div>

            </div>

            </div> -->
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-lg-3">
                <div class="loader" id="loader" style="">
                    <img src="<?php echo base_url('themes') . '/loading.gif'; ?>">
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        //$(this).scrollTop(0);
        // $(document).ready(function() {
        window.scrollTo(0, 0);
        // });
        /* for load modal first times */
        var orderBy = $("#order").val();
        var final_orderby = 'DESC';
        if (orderBy == 'random') {
            final_orderby = 'RANDOM';
        }
        var limit = 8;
        var offset = 0;
        var target = $("#model_list");
        var target_live_models = $("#live_model");
        var usedIds = '0';
        $.ajax({
            type: 'post',
            data: {
                'limit': limit,
                'offset': offset,
                'order_by': final_orderby,
                'used_ids': usedIds
            },
            dataType: "json",
            url: "<?php echo base_url('home/load_models');  ?>",
            success: function(r) {
                var html = '';
                $.each(r.data, function(key, value) {
                    html += make_model(value);
                    usedIds += ',' + value.id;
                });
                $('#models').attr('data-total_rec', r.total_rec);
                $(target).html(html);
                $('#offset').val(limit + offset);
                $("#loader").hide();
                $('#usedIds').val(usedIds);
            },
            error: function() {
                console.log('ajax error');
            }
        });
        /* when scroll than load more modals */
        $(window).scroll(function() {
            var browser = '';
            if (navigator.userAgent.search("MSIE") >= 0) {
                //code goes here
            } else if (navigator.userAgent.search("Chrome") >= 0) {
                browser = 'chrome';
                //code goes here
            } else if (navigator.userAgent.search("Firefox") >= 0) {
                browser = 'firefox';
                //code goes here
            } else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
                //code goes here
            } else if (navigator.userAgent.search("Opera") >= 0) {
                //code goes here
            }


            var orderBy = $("#order").val();
            var searchValue = $('#search').val();
            var offset = parseInt($('#offset').val());
            var usedIds1 = $('#usedIds').val();
            // if ($(window).scrollTop() + 1 >= $(document).height() - $(window).height()) {
            var position = $(window).scrollTop();
            var bottom = $(document).height() - $(window).height();
            //  if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            position = Math.round(position);
            bottom = Math.round(bottom);
            //alert('position is' + position + " bottom is" + bottom);
            let topPosition = position + 1;
            //alert('position is' + topPosition + " bottom is" + bottom);
            if ((position == bottom) || (topPosition == bottom)) {
                var final_orderby = 'DESC';
                var usedIds = '0';
                var usedIds1 = $('#usedIds').val();
                var limit = 8;
                var target = $("#model_list");
                var loader = $("#loader");
                $(loader).show();
                if (orderBy == 'random') {
                    final_orderby = 'RANDOM';
                    var final_data = {
                        'limit': limit,
                        'offset': offset,
                        'order_by': final_orderby,
                        'used_ids': usedIds1,
                        'search': searchValue
                    };
                }
                if (orderBy == 'featured') {
                    var final_data = {
                        'limit': limit,
                        'used_ids': usedIds1,
                        'featured': 1
                    };
                }

                if (orderBy == 'newest') {
                    var final_data = {
                        'limit': limit,
                        'order_by': final_orderby,
                        'used_ids': usedIds1,
                        'newest': 1
                    };
                }

                if (orderBy == 'live_now_models') {
                    var final_data = {
                        'limit': limit,
                        'order_by': final_orderby,
                        'used_ids': usedIds1,
                        'live_status': 1
                    };
                }

                if (orderBy == 'recently_active') {
                    var final_data = {
                        'limit': limit,
                        'order_by': final_orderby,
                        'used_ids': usedIds1,
                        'recently_active': 1
                    }

                }

                if (orderBy == 'most_followed') {
                    var final_data = {
                        'limit': limit,
                        'order_by': final_orderby,
                        'used_ids': usedIds1,
                        'most_followed': 1
                    };
                }

                $.ajax({
                    type: 'post',
                    data: final_data,
                    dataType: "json",
                    url: "<?php echo base_url('home/load_models');  ?>",
                    beforeSend: function() {
                        $(loader).show();
                    },
                    success: function(r) {
                        var html = '';
                        $.each(r.data, function(key, value) {
                            html += make_model(value);
                            usedIds += ',' + value.id;
                        });


                        $(target).append(html);
                        $('#usedIds').val(usedIds1 + ',' + usedIds);
                        $('#offset').val(parseInt($('#offset').val()) + parseInt(limit));

                        $(loader).hide();
                    },
                    error: function() {
                        console.log('ajax error');
                    }
                });
            }
        });


        /* live models starts */
        $.ajax({
            type: 'post',
            data: {
                'live_status': 1
            },
            dataType: "json",
            url: "<?php echo base_url('home/load_models');  ?>",
            success: function(r) {
                var html = '';
                $.each(r.data, function(key, value) {
                    html += make_live_model(value);
                });

                $(target_live_models).html(html);

            },

            error: function() {

                //console.log('ajax error');

            }


        });
        /* live models ends */


        /* featured model starts*/
        $(document).on('click', '#feature_models', function() {

            $("#order").val('featured');

            $('.site-feature').find('a').removeClass('active');

            $('#offset').val(0);

            $('#usedIds').val('0');

            $('#models').attr('data-total_rec', '0');

            var usedIds = '0';

            var limit = 4;

            var offset = parseInt($('#offset').val());

            $.ajax({

                type: 'post',

                data: {

                    'featured': 1,

                    'limit': limit,

                    'offset': offset,

                },

                dataType: "json",

                url: "<?php echo base_url('home/load_models');  ?>",

                success: function(r) {

                    var html = '';

                    usedIds = 0;

                    $.each(r.data, function(key, value) {

                        html += make_model(value);

                        usedIds += ',' + value.id;

                    });

                    $(target).html(html);

                    $('#usedIds').val(usedIds);

                    $('#feature_models').addClass('active');

                    $('#offset').val(parseInt($('#offset').val()) + parseInt(limit));

                    $('#models').attr('data-total_rec', r.total_rec);


                },

                error: function() {

                    //console.log('ajax error');

                }


            });

        });
        /* featured model ends*/


        /* newest model starts*/
        $(document).on('click', '#newest_models', function() {

            $("#order").val('newest');

            $('.site-feature').find('a').removeClass('active');

            $('#usedIds').val('0');

            $("#loader").show();

            //var final_orderby = 'DESC';

            $('#offset').val(0);

            $('#models').attr('data-total_rec', '0');

            var limit = 4;

            var offset = parseInt($('#offset').val());

            var target = $("#model_list");

            var usedIds = '0';

            $.ajax({

                type: 'post',

                data: {

                    'limit': limit,

                    'offset': offset,

                    // 'order_by': final_orderby,

                    'used_ids': usedIds,

                    'newest': 1

                },

                dataType: "json",

                url: "<?php echo base_url('home/load_models');  ?>",

                success: function(r) {

                    var html = '';

                    $.each(r.data, function(key, value) {

                        html += make_model(value);

                        usedIds += ',' + value.id;

                    });

                    $(target).html(html);

                    $('#offset').val(limit + offset);

                    $("#loader").hide();

                    $('#usedIds').val(usedIds);

                    $('#models').attr('data-total_rec', r.total_rec);


                },

                error: function() {

                    console.log('ajax error');

                }

            });


        });
        /* newest model ends*/


        /* live_now model starts*/
        $(document).on('click', '#live_now_models', function() {

            $("#order").val('live_now_models');

            $('.site-feature').find('a').removeClass('active');

            $("#loader").show();

            var final_orderby = 'DESC';

            $('#offset').val(0);

            $('#models').attr('data-total_rec', '0');

            var limit = 4;

            var offset = parseInt($('#offset').val());

            var target = $("#model_list");

            var usedIds = '0';

            $.ajax({

                type: 'post',

                data: {

                    'limit': limit,

                    'offset': offset,

                    'order_by': final_orderby,

                    'used_ids': usedIds,

                    'live_status': 1

                },

                dataType: "json",

                url: "<?php echo base_url('home/load_models');  ?>",

                success: function(r) {

                    var html = '';

                    $.each(r.data, function(key, value) {

                        html += make_model(value);

                        usedIds += ',' + value.id;

                    });

                    $(target).html(html);

                    $('#offset').val(limit + offset);

                    $("#loader").hide();

                    $('#usedIds').val(usedIds);

                    $('#models').attr('data-total_rec', r.total_rec);


                },

                error: function() {

                    console.log('ajax error');

                }

            });


        });
        /* live_now model ends*/


        /* recently active starts*/
        $(document).on('click', '#recently_active_models', function() {

            $("#order").val('recently_active');

            $('.site-feature').find('a').removeClass('active');

            $("#loader").show();

            var final_orderby = 'DESC';

            $('#offset').val(0);

            $('#models').attr('data-total_rec', '0');

            var limit = 4;

            var offset = parseInt($('#offset').val());

            var target = $("#model_list");

            $(target).html();

            var usedIds = '0';

            $.ajax({

                type: 'post',

                data: {

                    'limit': limit,

                    'offset': offset,

                    'order_by': final_orderby,

                    'used_ids': usedIds,

                    'recently_active': 1

                },

                dataType: "json",

                url: "<?php echo base_url('home/load_models');  ?>",

                success: function(r) {

                    var html = '';

                    $.each(r.data, function(key, value) {

                        html += make_model(value);

                        usedIds += ',' + value.id;

                    });

                    $(target).html(html);

                    //$('#offset').val(limit + offset);

                    $("#loader").hide();

                    $('#usedIds').val(usedIds);

                    $('#models').attr('data-total_rec', r.total_rec);


                },

                error: function() {

                    console.log('ajax error');

                }

            });


        });
        /* recently active ends*/

        /* most followed model starts*/
        $(document).on('click', '#most_followed_models', function() {

            $("#order").val('most_followed');

            $('.site-feature').find('a').removeClass('active');

            $('#usedIds').val('0');

            $("#loader").show();

            //var final_orderby = 'DESC';

            $('#offset').val(0);

            $('#models').attr('data-total_rec', '0');

            var limit = 4;

            var offset = parseInt($('#offset').val());

            var target = $("#model_list");

            var usedIds = '0';

            $.ajax({

                type: 'post',

                data: {

                    'limit': limit,

                    'offset': offset,

                    'used_ids': usedIds,

                    'most_followed': 1

                },

                dataType: "json",

                url: "<?php echo base_url('home/load_models');  ?>",

                success: function(r) {

                    var html = '';

                    $.each(r.data, function(key, value) {

                        html += make_model(value);

                        usedIds += ',' + value.id;

                    });

                    $(target).html(html);

                    $('#offset').val(limit + offset);

                    $("#loader").hide();

                    $('#usedIds').val(usedIds);

                    $('#models').attr('data-total_rec', r.total_rec);


                },

                error: function() {

                    console.log('ajax error');

                }

            });


        });
        /* newest model ends*/


    });


    /* use for making html of modal */
    function make_model(value) {

        var html = '';

        html += "<div class='col-md-6 col-sm-12 col-lg-3'>";

        html += "<div class='profile-box'>";

        html += "<a href='<?php echo base_url('model'); ?>/" + value.user_slug + "'>";

        html += "<div class='box1'>";

        html += "<img src=" + value.cover_image + ">";

        html += "</div>";

        html += "<div class='box2'>"

        html += "<div class='d-flex mt-2 ml-2 mybdr'>";

        html += "<img src=" + value.profile_image + ">";

        html += "<p>" + value.name + "</p>";

        html += "</div>";

        html += "</a>";

        html += "<div class='d-flex icon-main'>";

        html += "<div class='col-md-4 pro-border'>";

        html += "<img src='<?php echo base_url() . FRONT_IMG; ?>icon1.png' class='pull-left'>";

        html += "<h4>" + value.total_images + "</h4>";

        html += "</div>";

        html += "<div class='col-md-4 pro-border'>";

        html += "<img src='<?php echo base_url() . FRONT_IMG; ?>icon2.png' class='pull-left'>";

        html += "<h4>" + value.total_videos + "</h4>";

        html += "</div>";

        html += "<div class = 'col-md-4 pro-border'>";

        html += "<img src = '<?php echo base_url() . FRONT_IMG; ?>icon3.png' class = 'pull-left'>";

        html += "<h4>" + value.diamonds + "</h4>";

        html += "</div>";

        html += "</div>";

        html += "<div class = 'd-flex pro-folow'>";

        html += "<button type = 'button' class='follow' data-toggle='modal' data-target='#smallModal' data-model-id='" + value.id + "'> <img src ='<?php echo base_url() . FRONT_IMG . 'heart.svg'; ?>' width = '15' > &nbsp; &nbsp; <?php echo $this->lang->line('lbl_follow'); ?></button>";

        html += "<button type = 'button' class='snapchat'> <img src = '<?php echo base_url() . FRONT_IMG; ?>snap.png' width = '15' >&nbsp; &nbsp; <?php echo $this->lang->line('lbl_snapchat'); ?></button>";

        html += "</div>";

        html += "</div>";

        html += "</div>";

        html += "</div>";

        return html;


    }


    /* live models getting starts */
    function make_live_model(value) {

        var html = '';

        html += "<div class='col-md-2 col-sm-4 fea-top'>";

        html += "<div class='online-list'>";

        html += "<img src=" + value.profile_image + ">";

        html += "<h3>" + value.name + "</h3>";

        html += "<h3>Online</h3>";

        html += "</div>";

        html += "</div>";

        return html;

    }

    /* live models getting ends */

    /* for search starts */
    $(document).on('click', '#search_models', function() {
        var searchValue = $('#search').val();
        create_search(searchValue);
    });

    $(document).ready(function() {
        $('body').addClass('site-feed,site-premium');
    });


    $(document).on('keypress', '#search', function(event) {
        var searchValue = $('#search').val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            create_search(searchValue);
        }
    });

    function create_search(searchValue) {
        var target = $("#model_list");
        var loader = $("#loader");
        $(loader).show();
        $.ajax({
            type: 'post',
            data: {
                'search': searchValue
            },
            dataType: "json",
            url: "<?php echo base_url('home/load_models');  ?>",
            beforeSend: function() {
                $(loader).show();
            },
            success: function(r) {
                $('html, body').animate({
                    scrollTop: $("#model_list").offset().top
                }, 2000);
                var html = '';
                $.each(r.data, function(key, value) {
                    html += make_model(value);
                });
                $(target).html(html);
                $(loader).hide();

            },
            error: function() {
                console.log('ajax error');
            }
        });
    }
</script>