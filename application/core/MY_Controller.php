<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MY_Controller extends CI_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->data['errors'] = array();
        if (isset($_POST['wsLang']) && $_POST['wsLang'] != '') {
            switch ($_POST['wsLang']) {
                case 'en':
                    $this->data['language'] = 'english';
                    break;
                case 'fr':
                    $this->data['language'] = 'french';

                    break;
                default:
                    $this->data['language'] = 'english';
                    break;
            }
        } else {
            $this->data['language'] = 'english';
        }
        $this->load->model('Admin');
        $this->Admin->siteSettings();
        $this->lang->load('wsmessage', $this->data['language']);
    }

    public function saveFile($tag_name = 'userfile', $path = SITE_UPD, $config = array()) {
        $content = array('file_name' => "", 'errors' => "");
        if (isset($_FILES[$tag_name]['name']) && $_FILES[$tag_name]['name'] != "") {
            $this->load->library('upload');
            $this->load->library('image_lib');
            $path = FCPATH . SITE_UPD . $path;
            if (!file_exists($path)) {
                @mkdir($path, 0777, true);
            }
            $file_config = array();
            $file_config['upload_path'] = $path;
            $file_config['allowed_types'] = 'jpg|png|jpeg';
            $file_config['overwrite'] = TRUE;
            $file_config['max_size'] = '5120';
            $file_config['file_name'] = md5(time() . rand());
            $file_config = array_replace($file_config, $config);
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload($tag_name)) {
                $upload_data = $this->upload->data();
                $postArray = array();
                $content['file_name'] = $upload_data['file_name'];
            } else {
                $content['errors'] = strip_tags($this->upload->display_errors());
            }
        }
        return $content;
    }

}
