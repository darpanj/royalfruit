<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkAdminLogin($PostData = array()) {
        $content = array();
        $content['status'] = 404;
        $_utype = 'admin';
        if ($this->ADM_URL == 'vendor/')
            $_utype = 'vendor';
        $query = $this->db->get_where('tbl_users', array("username" => $PostData['vEmail'], 'password' => md5($PostData['vPassword']), 'type' => $_utype));
        if ($query->num_rows() === 1) {
            $r = $query->row();
            //pre($r->type);

            if ($r->status == 'active') {
                if ($r->type == 'admin') {
                    $userdata = array(
                        'ADMINID' => $r->id,
                        'ADMINUSERTYPE' => $r->type,
                    );
                } else {
                    $userdata = array(
                        'BUSINESSID' => $r->id,
                        'BUSINESSTYPE' => $r->type,
                    );
                }
                $this->session->set_userdata($userdata);
                if ($PostData['isremember'] == 1) {
                    $this->input->set_cookie(array('name' => 'user_email',
                        'value' => $PostData['vEmail'],
                        'expire' => 2592000,
                        'path' => '/'));
                    $this->input->set_cookie(array('name' => 'user_password',
                        'value' => base64_encode($PostData['vPassword']),
                        'expire' => 2592000,
                        'path' => '/',
                    ));
                    $this->input->set_cookie(array('name' => 'user_isremember',
                        'value' => 1,
                        'expire' => 2592000,
                        'path' => '/'));
                } else {
                    $this->input->set_cookie(array('name' => 'user_email',
                        'value' => "",
                        'expire' => 0,
                        'path' => '/',
                        'secure' => TRUE));
                    $this->input->set_cookie(array('name' => 'user_password',
                        'value' => "",
                        'expire' => 0,
                        'path' => '/',
                        'secure' => TRUE));
                    $this->input->set_cookie(array('name' => 'user_isremember',
                        'value' => 0,
                        'expire' => 0,
                        'path' => '/',
                        'secure' => TRUE));
                }
                $content['status'] = 200;
                $content['message'] = 'success';
            } else {
                $content['message'] = $this->data['language']['err_inactive'];
            }
        } else {
            $content['message'] = $this->data['language']['err_login'];
        }

        return $content;
    }

    public function changeStatus($post = array(), $table = "") {

        $content = array();
        $table = isset($post['table']) ? $post['table'] : "";
        $content['status'] = 404;

        if ($this->db->table_exists($table)) {
            $this->db->update($table, array('status' => $post['value']), array('id' => $post['id']));
            if ($this->db->affected_rows() > 0) {
                $content['status'] = 200;
                $content['message'] = $this->data['language']['succ_status_changed'];
            }
        }
        return $content;
    }

    public function generate_checkbox($params = array()) {
        if (!isset($params['selected'])) {
            $params['selected'] = '';
        }
        if (!isset($params['disabled'])) {
            $params['disabled'] = '';
        }
        $checkbox = '<input type="checkbox" class="small-chk" ' . $params['disabled'] . ' value="' . $params['id'] . '" ' . $params['selected'] . '>';
        return $checkbox;
    }

    public function generate_label_status($params = array(), $class = 'info') {
        if (!isset($params['booking_status'])) {
            $params['booking_status'] = '';
        }
        $label = '<span class="kt-badge kt-badge--' . $class . ' kt-badge--inline kt-badge--pill kt-badge--rounded">' . $params['booking_status'] . '</span>';
        return $label;
    }

    public function generate_switch($params = array(), $on_text = 'active', $off_text = "inactive") {
//        $switch = '<div class="switch-small"><input data-switch="true" type="checkbox"  data-on-text="' . ucfirst($on_text) . '"
//                               data-off-text="' . ucfirst($off_text) . '" data-on-color="success" data-off-color="danger" data-id="' . $params['id'] . '" data-table="' . $params['table'] . '" data-getaction="' . (!empty($params['getaction']) ? $params['getaction'] : '') . '" data-url="' . $params['url'] . '" ' . $params['checked'] . ' class="make-switch ' . (!empty($params['class']) ? $params['class'] : 'status-switch') . '"></div>';
        $switch = '<div class="switch-small"><input type="checkbox" data-id="' . $params['id'] . '" data-table="' . $params['table'] . '" data-getaction="' . (!empty($params['getaction']) ? $params['getaction'] : '') . '" data-url="' . $params['url'] . '/change_status' . '" ' . $params['checked'] . ' class="make-switch ' . (!empty($params['class']) ? $params['class'] : 'status-switch') . '" data-on-label="<i class=\'fa fa-check\'></i>" data-off-label="<i class=\'fa fa-times\' ></i>" ></div>';

        return $switch;
    }

    public function generate_actions($params = array(), $isEdit = true, $isView = true, $isDelete = true, $extra = array(), $mvc = false) {
        $operation = '';

        if ($isView) {
            $operation .= '<button title="View" data-url="' . $params['url'] . '/view/' . $params['id'] . '" data-type="view" data-id="' . $params['id'] . '" class="btn_space btn btn-success btn-xs btn-sm btnView mr-2">' . $this->lang->line("lbl_view") . '</button>';
        }
        if ($isEdit) {
            $operation .= '<button title="Edit" data-type="form" data-url="' . $params['url'] . '/add_edit_view/' . $params['id'] . '" data-id="' . $params['id'] . '" class="btn_space btn btn-warning btn-xs btn-sm btnEdit mr-2">' . $this->lang->line("lbl_edit") . '</button>';
        }
        if ($isDelete) {
            $operation .= '<button title="Delete" data-url="' . $params['url'] . '/delete' . '" data-id="' . $params['id'] . '" class="btn_space btn btn-danger btn-xs btn-sm btnDelete mr-2">' . $this->lang->line("lbl_delete") . '</button>';
        }
        if (!empty($extra)) {
            foreach ($extra as $key => $value) {
                $btn_id = "";
                $onclick = "";
                $data_type = "";
                if (!empty($value['btn_id'])) {
                    $btn_id = "id='" . $value['btn_id'] . "'";
                }
                if (!empty($value['onclick'])) {
                    $onclick = "onclick='" . $value['onclick'] . "'";
                }
                if (array_key_exists('data_type', $value) && !empty($value['data_type'])) {
                    $data_type = "data-type='" . $value['data_type'] . "'";
                }
                if (empty($value['btn_modal']))
                    $operation .= '<button  data-url="' . $value['url'] . '" ' . $data_type . ' data-id="' . $value['data_id'] . '" ' . $btn_id . ' ' . $onclick . ' class="btn_space ' . $value['btn_class'] . ' btn-sm">' . $value['btn_name'] . '</button>';
                else
                    $operation .= '<button type="button" class="btn_space ' . $value['btn_class'] . ' btn-sm" data-toggle="modal" ' . $btn_id . ' ' . $onclick . ' data-target="#' . $value['modal_id'] . '">' . $value['btn_name'] . '</button>';
            }
        }
        return $operation;
    }

    public function getSingleRecordById($id = 0, $table = '', $key = '') {
        $query = $this->db->select('*')->from($table)->where($key, $id)->get();
        //pre($query);
        if ($query->num_rows() === 1) {
            return $query->row_array();
        } else {
            return get_column($table);
        }
    }

    public function getClassBatchData() {
        $data = $this->db->select('tc.id as classid,tc.name as classname,tb.id as batchid,tb.name as batchname')
                        ->from('tbl_class as tc')
                        ->join('tbl_batch tb', 'tc.id = tb.class_id')
                        ->where('tc.school_id', $this->data['school_detail']['id'])
                        //->where('u.status', 'active')
                        ->get()->result_array();
        return $data;
    }

    public function getRecordByQuestionId($id = 0, $table = '', $key = '') {
        $query = $this->db->select('*')->from($table)->where($key, $id)->get();

        return $query->result();
    }

    public function deleteSingle($value = 0, $table = '', $key = '', $isHard = true) {
        $content = array();
        $content['status'] = 400;
        if ($isHard) {
            $this->db->delete($table, array($key => $value));
        } else {
            $this->db->update($table, array('status' => 'inactive'), array($key => $value));
        }
        if ($this->db->affected_rows() > 0) {
            $content['status'] = 200;
            $content['message'] = $this->data['language']['succ_rec_deleted'];
        }
        return $content;
    }

    public function deleteMultiple($ids, $table = '', $key = '', $isHard = true) {
        $content = array();
        $content['status'] = 400;
        $condition = $this->db->where("FIND_IN_SET($key,'$ids') !=", 0);
        if ($isHard) {
            $this->db->delete($table);
        } else {
            $this->db->update($table, array('status' => 'delete'));
        }
        if ($this->db->affected_rows() > 0) {
            $content['status'] = 200;
            $content['message'] = $this->data['language']['succ_rec_deleted'];
        }
        return $content;
    }

    public function get_data($table_name = '', $data = '', $condition_array = array(), $order_by = '', $sort_by = 'ASC', $limit = '', $result = 'array') {
        $this->db->select($data);
        $this->db->from($table_name);
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }
        if ($order_by != '' && $sort_by != '') {
            $this->db->order_by($order_by, $sort_by);
        }
        if (!empty($limit))
            $this->db->limit($limit);
        if ($result == 'row')
            return $this->db->get()->row_array();
        else
            return $this->db->get()->result_array();
    }

}

class MY_Wsmodel extends CI_Model {

    protected $_table_name = '';
    protected $_primary_key = '';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    public $rules = array();

    function __construct() {
        parent::__construct();
    }

    public function get($id = NULL, $single = FALSE, $fields = '*') {
        if ($id != NULL) {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        } elseif ($single == TRUE) {
            $method = 'row';
        } else {
            $method = 'result';
        }

        $this->db->order_by($this->_order_by);

        return $this->db->get($this->_table_name)->$method();
    }

    function get_order_by($array = NULL) {
        if ($array != NULL) {
            $this->db->select()->from($this->_table_name)->where($array)->order_by($this->_order_by);
            $query = $this->db->get();
            return $query->result();
        } else {
            $this->db->select()->from($this->_table_name)->order_by($this->_order_by);
            $query = $this->db->get();
            return $query->result();
        }
    }

    function get_single($array = NULL, $fields = '*') {

        if ($array != NULL) {
            $this->db->select($fields)->from($this->_table_name)->where($array);
            $query = $this->db->get();
            return $query->row();
        } else {
            $this->db->select($fields)->from($this->_table_name)->order_by($this->_order_by);
            $query = $this->db->get();
            return $query->result();
        }
    }

    function insert($array) {
        $array = $this->getDatabseFields($array, $this->_table_name);
        $this->db->insert($this->_table_name, $array);
        $id = $this->db->insert_id();
        return $id;
    }

    function insert_batch($array) {
        $this->db->insert_batch($this->_table_name, $array);
    }

    function update($data, $id = NULL) {
        $filter = $this->_primary_filter;
        $id = $filter($id);
        $data = $this->getDatabseFields($data, $this->_table_name);
        $this->db->set($data);
        $this->db->where($this->_primary_key, $id);
        $this->db->update($this->_table_name);
    }

    function updatePassword($data, $token = NULL) {
        $data = $this->getDatabseFields($data, $this->_table_name);
        $this->db->set($data);
        $this->db->where('vActivationToken', $token);
        $this->db->update($this->_table_name);
    }

    public function getDatabseFields($postData, $tableName = '') {
        if (empty($tableName)) {
            $tableName = $this->_table;
        }

        $table_fields = $this->getFields($tableName);

        $final = array_intersect_key($postData, $table_fields);

        return $final;
    }

    public function getFields($tableName = '') {

        $query = $this->db->query("SHOW COLUMNS FROM " . $tableName);

        foreach ($query->result() as $row) {
            $table_fields[$row->Field] = $row->Field;
        }

        return $table_fields;
    }

    public function delete($id) {
        $filter = $this->_primary_filter;
        $id = $filter($id);

        if (!$id) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->delete($this->_table_name);
    }

    public function delete_by_where($array, $limit = 1) {
        $this->db->where($array);
        $this->db->limit($limit);
        $this->db->delete($this->_table_name);
    }

    public function hash($string) {
        return hash("sha512", $string . config_item("encryption_key"));
    }

    public function get_by_table_name($tableName = '', $order = '', $array = NULL, $fields = '*') {
        if ($array != NULL) {
            $this->db->where($array);
        }
        $method = 'result';
        $this->db->order_by($order);
        return $this->db->get($tableName)->$method();
    }

    public function check_active($fields, $array = NULL) {
        $this->db->select($fields)->from($this->_table_name)->where($array);
        $query = $this->db->get();
        $this->db->last_query();
        return $query->row();
    }

}
