<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Documents_model', 'Main');
    }

    public function index() {
        if ($this->data['action'] == 'submit_data') {
            $_FILES = isset($_FILES) && !empty($_FILES) ? $_FILES : array();
            $status = $this->Main->submitSettings($this->input->post(), $_FILES);
            if ($status == 1) {
                $this->session->set_flashdata("success", sprintf($this->data['language']['succ_rec_updated'], $this->lang->line('make_money_header_title')));
                redirect(base_url() . ADM_URL . "documents");
            }
        }
        $this->data['headTitle'] = $this->lang->line("make_money_header_title");
        $this->data['module'] = "documents";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("general_set_header_title") => "", $this->data['headTitle'] => ""));
        $this->data['documents'] = $this->common->get_data_by_id('tbl_agreements', 'id', 1, '*', array(), '', 'ASC', '', 'row');
        $this->load->view('admin/mainpage', $this->data);
    }

}
