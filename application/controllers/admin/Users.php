<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
        $this->load->model('admin/User_model', 'Main');
    }

    function index() {
        $this->data['headTitle'] = $this->lang->line("lbl_user_manage");
        $this->data['module'] = "list_users";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_user_manage") => "", $this->data['headTitle'] => ""));
        $this->load->view('admin/mainpage', $this->data);
    }

    public function lists() {
        if ($this->input->is_ajax_request()) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Main->getUsersAll($filters);
            echo json_encode($result);
        } else {
            base_url(ADM_URL);
        }
    }

    public function add_edit_view($id = 0) {
        $content = array('status' => 200, 'message' => $this->data['language']['err_something_went_wrong']);
        $this->data['user'] = $this->Main->getSingleRecordById($id, 'tbl_users', 'id');
        $this->data['user']['profile_image'] = checkImage(1, $this->data['user']['profile_image']);
        $this->data['headTitle'] = ($id > 0 ? $this->lang->line("lbl_edit") : $this->lang->line("lbl_add") . ' ' . $this->lang->line("lbl_new")) . " " . $this->lang->line("lbl_user");
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_user_manage") => "javascript:back_portlet();", $this->data['headTitle'] => ""));
        $content['html'] = $this->load->view('admin/form_user', $this->data, true);
        echo json_encode($content);
    }

    public function add_edit_data() {
        $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
        $postData['id'] = $this->input->post('id');
        $this->form_validation->set_rules('name', $this->lang->line('lbl_name'), 'trim|required');
        $this->form_validation->set_rules('email', $this->lang->line('lbl_email'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $content['message'] = get_error($this->form_validation->error_array());
        } else {
            $_POST['image_path'] = "";
            if (!empty($this->input->post('image'))) {
                $_POST['image_path'] = $this->upload_image('users');
            }
            $check = $this->Main->submit_user($this->input->post());
            if ($check['status'] == 200) {
                $content = $check;
            }
        }
        echo json_encode($content);
    }

    public function view($id = 0) {

        if ($this->input->is_ajax_request()) {
            $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
            if (!empty($id)) {
                $content['status'] = 200;
                $content['message'] = $this->data['language']['err_something_went_wrong'];
                $this->data['headTitle'] = $this->lang->line('lbl_view_user');
                $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_user_manage") => "javascript:back_portlet();", $this->data['headTitle'] => ""));
                $this->data['user'] = $this->Main->getUserDetailsById($id);
                $content['html'] = $this->load->view('admin/view_user', $this->data, true);
                echo json_encode($content);
            }
        } else {
            $this->load->view('admin/mainpage', $this->data);
        }
    }

    public function change_status() {
        $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
        $post = $this->input->post();
        $post['value'] = ($post['value'] == 'y') ? 'active' : 'inactive';
        $check = $this->Main->changeStatus($post, 'tbl_users');
        if ($check['status'] == 200) {
            $content = $check;
            $this->db->where('iUserId', $post['id'])->delete('tbl_user_devices');
        }
        echo json_encode($content);
    }

    public function removeImage() {
        $content['status'] = 401;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            if (!empty($id)) {
                $dataArray = array(
                    'profile_image' => ''
                );
                $this->db->update('tbl_users', $dataArray, array('id' => $id));
                if ($this->db->affected_rows() > 0) {
                    $content['status'] = 200;
                    $content['message'] = $this->lang->line("succ_rm_user_pic");
                }
            }
        }
        echo json_encode($content);
        die();
    }

    public function _checkUsernameExist($username = "") {
        $id = $this->input->post('id');
        $check = $this->db->get_where('tbl_users', array('username' => $username, 'id !=' => $id))->num_rows();
        if ($check > 0) {
            $this->form_validation->set_message("_checkUsernameExist", "%s already registered.");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function delete() {

        $content = array();
        $id = $this->input->post('id');
        $content['status'] = 404;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        if ($this->data['action'] == "delete") {
            $check = $this->Main->deleteSingle($id, 'tbl_users', 'id');
            $this->db->where('iUserId', $id);
            $this->db->delete('tbl_user_devices');
        } else {
            $check = $this->Main->deleteMultiple($this->input->post('ids'), 'tbl_users', 'id');
            $this->db->where_in('iUserId', $this->input->post('ids'));
            $this->db->delete('tbl_user_devices');
        }
        if ($check['status'] == 200) {
            $content = $check;
        }
        echo json_encode($content);
    }

    public function get_booking_data_by_user_id($userId) {


        if ($userId) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Admin->get_booking_data($filters, 0, $userId);
            echo json_encode($result);
        } else {
            redirect($this->ADM_URL . '/dashboard');
        }
    }

}
