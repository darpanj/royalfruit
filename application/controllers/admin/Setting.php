<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Settings');
    }

    public function index() {
        if ($this->data['action'] == 'submit_settings') {
            $_FILES = isset($_FILES) && !empty($_FILES) ? $_FILES : array();
            $status = $this->Settings->submitSettings($this->input->post(), $_FILES);
            if ($status == 1) {
                $this->session->set_flashdata("success", sprintf($this->data['language']['succ_rec_updated'], $this->lang->line('site_set_header_title')));
                redirect(base_url() . ADM_URL . "setting");
            }
        }
        $this->data['headTitle'] = $this->lang->line("site_set_header_title");
        $this->data['module'] = "setting";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("general_set_header_title") => "", $this->data['headTitle'] => ""));

        $this->data['getFields'] = $this->Settings->getFields();
        $this->load->view('admin/mainpage', $this->data);
    }

}
