<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_images extends Admin_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Post_images_model', 'Main');
    }

    function index() {
        $this->data['headTitle'] = $this->lang->line("lbl_post_images_manage");
        $this->data['module'] = "list_post_images";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_post_images_manage") => "", $this->data['headTitle'] => ""));
        $this->load->view('admin/mainpage', $this->data);
    }

    public function lists() {
        if ($this->input->is_ajax_request()) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Main->getGeoBlockingAll($filters);
            echo json_encode($result);
        } else {
            base_url(ADM_URL);
        }
    }

    /*public function change_status() {
        $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
        $post = $this->input->post();
        $post['value'] = ($post['value'] == 'y') ? 'active' : 'inactive';
        $check = $this->Main->changeStatus($post, 'tbl_geo_blocking');
        if ($check['status'] == 200) {
            $content = $check;
        }
        echo json_encode($content);
    }*/


}
