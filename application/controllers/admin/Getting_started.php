<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Getting_started extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Getting_started_model', 'Main');
    }

    public function index() {
        if ($this->data['action'] == 'submit_data') {
            $_FILES = isset($_FILES) && !empty($_FILES) ? $_FILES : array();
            $status = $this->Main->submitSettings($this->input->post(), $_FILES);
            if ($status == 1) {
                $this->session->set_flashdata("success", sprintf($this->data['language']['succ_rec_updated'], $this->lang->line('getting_started_header_title')));
                redirect(base_url() . ADM_URL . "getting_started");
            }
        }
        $this->data['headTitle'] = $this->lang->line("getting_started_header_title");
        $this->data['module'] = "getting_started";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("general_set_header_title") => "", $this->data['headTitle'] => ""));
        $this->data['getting_started'] = $this->common->get_data_by_id('tbl_getting_started', 'id', 1, '*', array(), '', 'ASC', '', 'row');
        $this->load->view('admin/mainpage', $this->data);
    }

}
