<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends Admin_Controller
{

    public $data = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Content_model', 'Main');
    }

    function index()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_list_content");
        $this->data['module'] = "list_content";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_list_content") => "", $this->data['headTitle'] => ""));
        $this->load->view('admin/mainpage', $this->data);
    }

    public function add_edit_view($id = 0)
    {
        $content = array('status' => 200, 'message' => $this->data['language']['err_something_went_wrong']);
        $this->data['data'] = $this->Main->getSingleRecordById($id, 'mst_content', 'id');
        $this->data['headTitle'] = ($id > 0 ? $this->lang->line("lbl_edit") : $this->lang->line("lbl_add") . ' ' . $this->lang->line("lbl_new")) . " " . $this->lang->line("lbl_Content");
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_list_content") => "javascript:back_portlet();", $this->data['headTitle'] => ""));
        $content['html'] = $this->load->view('admin/form_content', $this->data, true);
        echo json_encode($content);
    }

    public function add_edit_data()
    {
        $return_data = array('status' => 200, 'message' => $this->lang->line("succ_updated_Content"));
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules($this->add_edit_data_rules());
            if ($this->form_validation->run() == false) {
                $return_data['message'] = get_error($this->form_validation->error_array());
            } else {
                $return_data['status'] = 200;
                $id = $this->input->post('id');
                $data = array(
                    'title' => $this->input->post('title'),
                    'content' => $this->input->post('conetnt'),
                    'created' => get_date(),
                );
                $this->common->update_data($data, 'mst_content', 'id', $id);
                echo json_encode($return_data);
            }
        } else {
            redirect($this->ADM_URL . '/Content');
        }
    }

    public
    function add_edit_data_rules()
    {

        $rules = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'trim',
            ),
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'conetnt',
                'label' => 'conetnt',
                'rules' => 'trim',
            ),
        );
        return $rules;
    }

    public
    function lists()
    {
        if ($this->input->is_ajax_request()) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Main->Listing($filters);
            echo json_encode($result);
        } else {
            base_url($this->ADM_URL);
        }
    }

}
