<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
    }

    public function index() {

        //pre($this->data['user_detail']);
        if ($this->data['user_detail']['type'] == 'admin') {    //for admin
            $this->data['headTitle'] = $this->lang->line("dashboard_header_title");
            //$this->data['userCount'] = $this->common->get_table_count('tbl_users', array('type' => 'user'));
            // pre($this->data);
            $this->data['module'] = "dashboard";
            $this->load->view('admin/mainpage', $this->data);
        } else if ($this->data['user_detail']['type'] == 'vendor') { //for vendor
            $this->data['headTitle'] = $this->lang->line("vendor_dashboard_header_title");
            $this->data['module'] = "vendor_dashboard";
            $this->load->view('admin/mainpage', $this->data);
        }
    }

}
