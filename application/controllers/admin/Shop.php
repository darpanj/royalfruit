<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends Admin_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Shop_model', 'Main');
    }

    function index() {
        $this->data['headTitle'] = "Manage Shop";
        $this->data['module'] = "list_shop";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, "Manage Shop" => "", $this->data['headTitle'] => ""));
        $this->load->view('admin/mainpage', $this->data);
    }

    public function lists() {
        if ($this->input->is_ajax_request()) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Main->getShopAll($filters);
            echo json_encode($result);
        } else {
            base_url(ADM_URL);
        }
    }

    public function add_edit_view($id = 0) {
        $content = array('status' => 200, 'message' => $this->data['language']['err_something_went_wrong']);
        $this->data['shop'] = $this->Main->getSingleRecordById($id, 'tbl_shop', 'id');
        $this->data['shop']['image'] = checkImage(8, $this->data['shop']['image']);
        $this->data['headTitle'] = ($id > 0 ? $this->lang->line("lbl_edit") : $this->lang->line("lbl_add") . ' ' . $this->lang->line("lbl_new")) . " Shop";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, "Manage Shop" => "javascript:back_portlet();", $this->data['headTitle'] => ""));
        $content['html'] = $this->load->view('admin/form_shop', $this->data, true);
        echo json_encode($content);
    }

    public function add_edit_data() {
        $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
        $postData['id'] = $this->input->post('id');
        $this->form_validation->set_rules('name', $this->lang->line('lbl_name'), 'trim|required');
        $this->form_validation->set_rules('price', 'Price', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $content['message'] = get_error($this->form_validation->error_array());
        } else {
            $_POST['image_path'] = "";
            if (!empty($this->input->post('image'))) {
                $_POST['image_path'] = $this->upload_image('shop');
            }
            $check = $this->Main->submit_shop($this->input->post());
            if ($check['status'] == 200) {
                $content = $check;
            }
        }
        echo json_encode($content);
    }



    public function change_status() {
        $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
        $post = $this->input->post();
        $post['value'] = ($post['value'] == 'y') ? 'active' : 'inactive';
        $check = $this->Main->changeStatus($post, 'tbl_shop');
        if ($check['status'] == 200) {
            $content = $check;
        }
        echo json_encode($content);
    }

    public function removeImage() {
        $content['status'] = 401;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            if (!empty($id)) {
                $dataArray = array(
                    'profile_image' => ''
                );
                $this->db->update('tbl_shop', $dataArray, array('id' => $id));
                if ($this->db->affected_rows() > 0) {
                    $content['status'] = 200;
                    $content['message'] = $this->lang->line("succ_rm_user_pic");
                }
            }
        }
        echo json_encode($content);
        die();
    }

    public function delete() {

        $content = array();
        $id = $this->input->post('id');
        $content['status'] = 404;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        if ($this->data['action'] == "delete") {
            $check = $this->Main->deleteSingle($id, 'tbl_shop', 'id');
        } else {
            $check = $this->Main->deleteMultiple($this->input->post('ids'), 'tbl_shop', 'id');
        }
        if ($check['status'] == 200) {
            $content = $check;
        }
        echo json_encode($content);
    }

    public function get_booking_data_by_user_id($userId) {


        if ($userId) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Admin->get_booking_data($filters, 0, $userId);
            echo json_encode($result);
        } else {
            redirect($this->ADM_URL . '/dashboard');
        }
    }

}
