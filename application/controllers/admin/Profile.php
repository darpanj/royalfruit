<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Admin_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Common_model', 'common');
    }

    function index() {
        $type = $this->input->post('type');
        $id = (int) $this->input->post('id');
        if ($this->data['action'] == 'submit_profile') {
            $content = array();
            $postData['id'] = $this->input->post('id');
            $this->form_validation->set_rules('name', $this->lang->line('lbl_name'), 'trim|required');
            $this->form_validation->set_rules('email', $this->lang->line('email'), 'required|callback_unique_email');
            $this->form_validation->set_rules('mobile', $this->lang->line('mobile_no'), 'required');
            $this->form_validation->set_rules('username', $this->lang->line('username'), 'required|callback__checkUsernameExist');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_userdata('errors', $this->form_validation->error_array());
                $content['status'] = 401;
                $i = 0;
                foreach ($this->form_validation->error_array() as $key => $value) {
                    if ($i != 0) {
                        continue;
                    }
                    $content['message'] = $value;
                    $i++;
                }
            } else {
                if (!empty($this->input->post('image'))) {
                    $_POST['image_path'] = $this->upload_image('users');
                }
                $check = $this->Admin->submit_admin_profile($this->input->post());
                if ($check['status'] == 200) {
                    $content['status'] = 200;
                    $content['message'] = sprintf($this->data['language']['succ_rec_updated'], $this->lang->line('lbl_profile'));
                } else {
                    $content['status'] = 401;
                    $content['message'] = $this->data['language']['err_something_went_wrong'];
                }
            }
            echo json_encode($content);
            die();
        } elseif ($this->data['action'] == 'check_email') {
            $isExist = $this->db->select('id')->get_where('tbl_users', array('email' => $this->input->post('email'), 'id !=' => $this->input->post('id')))->num_rows();
            if ($isExist) {
                echo 'false';
            } else {
                echo 'true';
            }
            exit;
        } else {
            $content = array();
            $content['status'] = 200;
            $content['message'] = $this->data['language']['err_something_went_wrong'];
            $this->data['user'] = $this->common->get_data_by_id('tbl_users', 'id', $this->user_id, $data = '*', $condition_array = array(), $order_by = 'id', $sort_by = 'ASC', $limit = '', $result = 'row');
            if (count($this->data['user']) === 0) {
                $this->data['user'] = get_column('tbl_user');
            } else {
                if ($this->data['user']['type'] == 'admin') {
                    $this->data['user']['profile_image'] = checkImage(1, $this->data['user']['profile_image']);
                } else {
                    $this->data['user']['profile_image'] = checkImage(1, $this->data['user']['logo']);
                }
            }

            $this->data['headTitle'] = $this->lang->line('profile_header_title');
            $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->data['headTitle'] => ""));
            $this->data['module'] = "form_profile";

            $this->load->view('admin/mainpage', $this->data);
        }
    }

    public function removeImage() {
        $content['status'] = 401;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            if (!empty($id)) {
                $dataArray = array(
                    'profile_image' => ''
                );
                $this->db->update('tbl_users', $dataArray, array('id' => $id));
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ_profile_rm_pro_pic');
            }
        }
        echo json_encode($content);
        die();
    }

    public function _checkUsernameExist($username = "") {
        $id = $this->input->post('id');
        $check = $this->db->get_where('tbl_users', array('username' => $username, 'id !=' => $id))->num_rows();
        if ($check > 0) {
            $this->form_validation->set_message("_checkUsernameExist", "Username already registered.");
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
