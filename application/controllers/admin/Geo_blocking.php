<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Geo_blocking extends Admin_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Geo_blocking_model', 'Main');
    }

    function index() {
        $this->data['headTitle'] = $this->lang->line("lbl_geo_blocking_manage");
        $this->data['module'] = "list_geo_blocking";
        $this->data['bradcrumb'] = breadcrumb(array($this->lang->line("lbl_admin_home") => base_url() . ADM_URL, $this->lang->line("lbl_geo_blocking_manage") => "", $this->data['headTitle'] => ""));
        $this->load->view('admin/mainpage', $this->data);
    }

    public function lists() {
        if ($this->input->is_ajax_request()) {
            $filters = $this->getDTFilters($this->input->get());
            $result = $this->Main->getGeoBlockingAll($filters);
            echo json_encode($result);
        } else {
            base_url(ADM_URL);
        }
    }

    public function delete() {
        $content = array();
        $id = $this->input->post('id');
        $content['status'] = 404;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        if ($this->data['action'] == "delete") {
            $check = $this->Main->deleteSingle($id, 'tbl_geo_blocking', 'id');
        }
        if ($check['status'] == 200) {
            $content = $check;
        }
        echo json_encode($content);
    }

    public function change_status() {
        $content = array('status' => 404, 'message' => $this->data['language']['err_something_went_wrong']);
        $post = $this->input->post();
        $post['value'] = ($post['value'] == 'y') ? 'active' : 'inactive';
        $check = $this->Main->changeStatus($post, 'tbl_geo_blocking');
        if ($check['status'] == 200) {
            $content = $check;
        }
        echo json_encode($content);
    }


}
