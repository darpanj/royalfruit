<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Admin', 'admin');
    }

    public function index() {

        //pre($this->data['action']);
        if ($this->data['action'] == 'submit_login') {
            // pre($this->input->post());
            $content = array();
            $content['status'] = 404;
            $content['message'] = $this->data['language']['err_something_went_wrong'];
            $this->form_validation->set_rules('vEmail', $this->lang->line('email'), 'trim|required');
            $this->form_validation->set_rules('vPassword', $this->lang->line('password'), 'required');

            if ($this->form_validation->run() == FALSE) {
                //pre('false');
                $content['message'] = validation_errors();
            } else {
                //  pre('true');
                $postData['vEmail'] = $this->input->post('vEmail', TRUE);
                $postData['vPassword'] = $this->input->post('vPassword', TRUE);
                $postData['isremember'] = $this->input->post('isremember') == 1 ? 1 : 0;
                $check = $this->admin->checkAdminLogin($postData);
                // pre($check);
                if ($check['status'] == 200) {
                    $content = $check;
                } else {
                    $content['message'] = $check['message'];
                }
            }

            echo json_encode($content);
            exit;
        } else if ($this->data['action'] == 'submit_forgot') {
            $content = array();
            $content['status'] = 404;
            $content['message'] = $this->data['language']['err_something_went_wrong'];

            $this->form_validation->set_rules('vEmail', $this->lang->line('email'), 'trim|required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $content['message'] = get_error($this->form_validation->error_array());
//                $content['message'] = validation_errors();
            } else {
                $postData['vEmail'] = $this->input->post('vEmail', TRUE);
                $check = $this->Admin->forgotPassword($postData);
                if ($check['status'] == 200) {
                    $content = $check;
                } else {
                    $content['message'] = $check['message'];
                }
            }
            echo json_encode($content);
            exit;
        } else if ($this->data['action'] == 'submit_register') {
            $content = array();
            $content['status'] = 404;
            $content['message'] = $this->data['language']['err_something_went_wrong'];
            $_POST['email'] = $this->input->post('rEmail');
            $_POST['mobile'] = $this->input->post('rMobile');
            $this->form_validation->set_rules('rName', $this->lang->line('lbl_business_name'), 'trim|required');
            $this->form_validation->set_rules('rEmail', $this->lang->line('lbl_business_email'), 'trim|required|valid_email|callback_unique_email');
            $this->form_validation->set_rules('rUsername', $this->lang->line('lbl_business_username'), 'trim|required|callback__checkUsernameExist');
            $this->form_validation->set_rules('rMobile', $this->lang->line('lbl_business_mobile'), 'trim|required|callback_unique_mobile');
            if ($this->form_validation->run() == FALSE) {
                //$content['message'] = $this->form_validation->error_array();
                $content['message'] = validation_errors();
            } else {
                $postData = $this->input->post();
                $check = $this->Admin->businessRegister($postData);
                if ($check['status'] == 200) {
                    $content = $check;
                } else {
                    $content['message'] = $check['message'];
                }
            }
            echo json_encode($content);
            exit;
        } else {
            $this->data['email'] = $this->input->cookie('foodapp_user_email', true);
            $this->data['password'] = $this->input->cookie('foodapp_user_password', true) != "" ? base64_decode($this->input->cookie('foodapp_user_password', true)) : "";
            $this->data['isremember'] = $this->input->cookie('foodapp_user_isremember', true) == 1 ? 'checked' : '';
        }

        $this->data['headTitle'] = $this->lang->line('login_header_title');
        $this->data['module'] = "login";
        $this->data['header_panel'] = false;
        $this->data['footer_panel'] = false;
        $this->data['left_panel'] = false;
        $this->load->view('admin/mainpage', $this->data);
    }

    public function reset($token = "") {
        if ($this->data['action'] == 'submit_reset') {
            $content = array();
            $content['status'] = 404;
            $content['message'] = $this->data['language']['err_something_went_wrong'];

            $this->form_validation->set_rules('npassword', $this->lang->line('new_password'), 'required');
            $this->form_validation->set_rules('cpassword', $this->lang->line('comfirm_password'), 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['mstType'] = $this->form_validation->error_array();
            } else {
                $postData['vToken'] = $this->input->post('vToken');
                $check = $this->Admin->resetPass($this->input->post());
                if ($check['status'] == 200) {
                    $msgType = disMessage(array("type" => "Success", "var" => $check['message']));
                    $this->session->set_userdata("msgType", $msgType);
                    redirect(base_url() . ADM_URL . "login");
                } else {
                    $this->data['msgType'] = disMessage(array("type" => "Error", "var" => $check['message']));
                }
            }
        } else {
            $token = isset($token) ? $token : "";
            $check = $this->Admin->checkResetToken($token);
            if ($check['status'] != 200) {
                $msgType = disMessage(array("type" => "Error", "var" => $this->data['language']['err_invalid_token']));
                $this->session->set_userdata("msgType", $msgType);
                redirect(base_url() . ADM_URL . "login");
            }
        }
        $this->data['token'] = $token;
        $this->data['headTitle'] = $this->lang->line('reset_pass_header_title');
        $this->data['module'] = "login";
        $this->data['header_panel'] = false;
        $this->data['footer_panel'] = false;
        $this->data['left_panel'] = false;
        $this->load->view('admin/mainpage', $this->data);
    }

    public function logout() {
        if ($this->user_type == 'admin') {
            $this->session->unset_userdata('ADMINID');
            $this->session->unset_userdata('ADMINUSERTYPE');
            redirect(base_url($this->ADM_URL) . '/login');
        } else {
            $this->session->unset_userdata('BUSINESSID');
            $this->session->unset_userdata('BUSINESSTYPE');
            redirect(base_url($this->ADM_URL) . '/login');
        }
    }

    public function _checkUsernameExist($username = "") {
        $id = $this->input->post('id');
        $check = $this->db->get_where('tbl_users', array('username' => $username, 'id !=' => $id))->num_rows();
        if ($check > 0) {
            $this->form_validation->set_message("_checkUsernameExist", "%s already registered.");
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
