<?php
class My404 extends CI_Controller {
	public $data = array();
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('Admin');
		$this->Admin->siteSettings();
	}

	public function index() {
		$this->data['headTitle'] = '404';
		$this->load->view('404', $this->data); //loading in my template
	}
	public function noscript() {
		$this->data['headTitle'] = 'Javascript Disabbled';
		$this->load->view('noscript', $this->data); //loading in my template
	}
}
?>