<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends Web_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('front/User_model', 'user_model');
    }

    function index() {
        $this->data['search'] = $this->input->post('search');
        $this->data['headTitle'] = $this->lang->line("lbl_title_shop");
        $this->data['module'] = "shop";
        if (!empty($this->input->post('search'))) {
            $this->db->group_start()
                    ->like('name', $this->input->post('search'))
                    ->group_end();
        }
        $this->data['shop'] = $this->common->get_data_by_id('tbl_shop', 'status', 'active');
        $this->load->view('front/mainpage', $this->data);
    }

    public function shop_details() {
        $this->data['headTitle'] = $this->lang->line("lbl_title_shop_details");
        $this->data['module'] = "shop_details";
        $this->load->view('front/mainpage', $this->data);
    }

}
