<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends Web_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('front/User_model', 'user_model');
    }

    function index() {
        $this->data['search'] = $this->input->post('search');
        $this->data['headTitle'] = $this->lang->line("lbl_title_shop");
        $this->data['module'] = "shop";
        if (!empty($this->input->post('search'))) {
            $this->db->group_start()
                    ->like('name', $this->input->post('search'))
                    ->group_end();
        }
        $this->data['shop'] = $this->common->get_data_by_id('tbl_shop', 'status', 'active');
        $this->load->view('front/mainpage', $this->data);
    }

    public function shop_details() {
        $this->data['headTitle'] = $this->lang->line("lbl_title_shop_details");
        $this->data['module'] = "shop_details";
        $this->load->view('front/mainpage', $this->data);
    }

    public function checkout() {
        if (!empty($this->input->post('stripeToken'))) {
            $this->load->library('payment');
            $strip = $this->payment;
            $strip->init();
            $transaction = $strip->chargeCustomer(1, $this->input->post('stripeToken'));
            if (!empty($transaction) && $transaction['status'] == '200') {
                $_data = array(
                    'user_id' => $this->session->userdata('USERID'),
                    'shop_id' => $this->input->post('shop_id'),
                    'amount' => $this->input->post('amount'),
                    'transaction_id' => $transaction['id'],
                    'payment_status' => 'success',
                    'insert_date' => get_date(),
                    'update_date' => get_date(),
                );
                $this->common->insert_data('tbl_order', $_data);
                $this->session->set_flashdata('success', "Success");
            } else {
                $this->session->set_flashdata('error', $transaction['message']);
            }
        }
        redirect('shop');
    }

}
