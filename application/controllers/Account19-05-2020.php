<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Account extends Web_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('image_lib');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/User_model', 'user_model');
    }


    // when user login success than this function will be call
    function dashboard()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_dashboard");
        $this->data['module'] = "your_feed";
        $this->load->view('front/account/mainpage', $this->data);
    }

    // when model login success than this function will be call
    function application()
    {
        echo "<h1>Welcome Model " . $this->data['user_detail']['name'] . "</h1><br>";
        echo "<a href='" . base_url('home/logout') . "'>Logout</a>";
        pre('application');
        $this->data['headTitle'] = $this->lang->line("lbl_title_dashboard");
        $this->data['module'] = "model/dashboard";
        $this->load->view('front/mainpage', $this->data);
    }

    public function buy_tokens()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "buy_tokens";
        $this->data['tokens'] = $this->common->get_data('tbl_token', '*', array('status' => 'active'));
        //pre($this->data['tokens']);
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function custom_videos($status = 'listing')
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        if ($status == 'listing') {
            $this->data['module'] = "completed_custom_videos";
            $this->data['video_list'] = $this->user_model->get_all_custom_videos();
        }
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function message()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "message";
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function password()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_password");
        $this->data['module'] = "password";
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function premium_snap()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "premium_snap";
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function premium_videos()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_premium_content");
        $this->data['module'] = "premium_videos";
        $this->data['premium_videos'] = $this->user_model->get_premium_videos($this->user_id);
        //pre($this->data['premium_videos']);
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function settings()
    {
        $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
        $this->data['headTitle'] = $this->lang->line("lbl_settings");
        $this->data['module'] = "settings";
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function load_suggestion_models()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $models = $this->user_model->get_suggestion_models($limit, $this->user_id);
            foreach ($models as $key => $value) {
                $models[$key]['cover_image'] = checkImage(3, $value['cover_image']);
                $models[$key]['profile_image'] = checkImage(2, $value['profile_image']);
                $models[$key]['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 0));
                $models[$key]['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1));
            }
            $response['data'] = $models;
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function get_model_by_id()
    {
        $modelId = check_variable_value($this->input->post('userId'));
        $models = array();
        if ($modelId > 0) {
            $result['models'] = $this->common->get_models_with_price($modelId);
            //pre($result['models']);
            //$result['models']['price'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
        }
        echo json_encode($result);
    }

    public function load_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_feed_models($this->user_id, $limit, $filters);
            //pre('fsdfsdaf');
            foreach ($response['feed_models'] as $key => $value) {
                if ($value['post_type'] == '0') {
                    // $config['image_library']   = 'gd2';

                    // $config['source_image'] =  FCPATH . SITE_UPD . '/models' . '/' . $value['post_image'];
                    // $config['wm_text'] = 'RoyalFruit';
                    // $config['wm_type'] = 'text';

                    // $config['wm_font_size'] = '16';
                    // $config['wm_font_color'] = '#0000FF';

                    // $this->load->library('image_lib', $config);
                    // $this->image_lib->initialize($config);
                    // $this->image_lib->watermark();
                    // if (!$this->image_lib->watermark()) {
                    // }
                    $this->overlayWatermark($value['post_image']);
                } else {

                    // $inputvideo = FCPATH . SITE_UPD . '/models' . '/' . $value['post_image'];
                    // $logo = FCPATH . ADM_MEDIA  . 'logo.png';
                    // // pre($logo);
                    // //exec("ffmpeg -i " . $inputvideo . " " . $logo . ".flv");
                    // $mark = "ffmpeg -i " . $inputvideo . " -i " . $logo . " -filter_complex " . '"overlay=x=(main_w-overlay_w):y=(main_h-overlay_h)/(main_h-overlay_h)"' . " " .  FCPATH . SITE_UPD . 'models' . '/' . uniqid() . "topright.mp4";
                    // // $mark = "start notepad";
                    // pre(exec($mark));
                }
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function get_post_by_id()
    {
        $postId = check_variable_value($this->input->post('postId'));
        // pre($postId);
        $result = array();
        if ($postId > 0) {
            $result['models'] = $this->common->get_post_by_id($postId);
            //pre($result['models']);
            //$result['models']['price'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
        }
        echo json_encode($result);
    }

    // public function textWatermark($source_image)
    // {
    //     $config['source_image'] = $source_image;
    //     //The image path,which you would like to watermarking
    //     $config['wm_text'] = 'http://getsourcecodes.com';
    //     $config['wm_type'] = 'text';
    //     // $config['wm_font_path'] = './fonts/atlassol.ttf';
    //     $config['wm_font_size'] = 16;
    //     $config['wm_font_color'] = 'ffffff';
    //     $config['wm_vrt_alignment'] = 'middle';
    //     $config['wm_hor_alignment'] = 'right';
    //     $config['wm_padding'] = '20';
    //     $this->image_lib->initialize($config);
    //     if (!$this->image_lib->watermark()) {
    //         return $this->image_lib->display_errors();
    //     }
    // }

    public function change_password()
    {
        $this->form_validation->set_rules(
            'old_password',
            'Old Password',
            'required|callback_oldPassword',
            array('required' => 'You must provide a %s.', 'oldPassword' => $this->lang->line('err_old_password'))
        );
        $this->form_validation->set_rules(
            'vPassword',
            'Password',
            'required',
            array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('cPassword', 'Password Confirmation', 'required|matches[vPassword]', array(
            'matches' => 'Password Does not match.',
        ));
        if ($this->form_validation->run()) {
            $pass = md5($this->input->post('vPassword'));
            $check = $this->common->update_data($data = array('password' => $pass), 'tbl_users', 'id', $this->user_id);
            if ($check) {
                $this->session->set_flashdata('success', 'Password Changed successfully.');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong.');
            }
        } else {
            $i = 0;
            foreach ($this->form_validation->error_array() as $key => $value) {
                if ($i != 0) {
                    continue;
                }
                $this->session->set_flashdata("error", $value);
                $i++;
            }
            $this->session->set_flashdata('error', get_error($this->form_validation->error_array()));
        }
        redirect(base_url('account/password'));
    }

    public function update_settings()
    {
        $this->form_validation->set_rules($this->update_settings_check_form());
        if ($this->form_validation->run() == FALSE) {
            $i = 0;
            foreach ($this->form_validation->error_array() as $key => $value) {
                if ($i != 0) {
                    continue;
                }
                $this->session->set_flashdata("error", $value);
                $i++;
            }
            redirect(base_url('account/settings'));
        } else {
            $postData = $this->input->post();
            $iUserId = $this->user_model->update_user($postData, $this->user_id);
            if ($iUserId > 0) {
                $this->common->upload_user_image($_FILES, $this->user_id, 'users');
                $userData = $this->common->get_data_by_id('tbl_users', 'id', $iUserId, '*', array(), '', '', '', 'row');
                $this->session->set_flashdata('success', $this->lang->language['succ_profile_updated']);
                if ($userData['type'] == 'user') {
                    redirect(base_url('account/dashboard'));  // redirecting user dashboard
                } elseif ($userData['type'] == 'model') {
                    redirect(base_url('account/application')); //redirecting model dashboard
                } else {
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('error', "Something went worng");
            }
        }
    }

    protected function update_settings_check_form()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line('lbl_name'),
                'rules' => 'trim|required',
            )
        );
        return $rules;
    }

    public function purchase_tokens()
    {
        $tokenId = $this->input->post('token_id');
        $token_qty = $this->input->post('token_qty');
        if ($tokenId > 0) {
            $userId = $this->user_id;
            $tokensuserData = $this->common->get_data_by_id('tbl_token_users', 'user_id', $userId);
            $token_user = isset($tokensuserData[0]['tokens']) ? $tokensuserData[0]['tokens'] : 0;
            //pre($token_user);

            $type = 0; //0=token
            $price = $this->input->post('token_price');

            $purchase = $this->common->purchase($type, $userId, $price);
            if ($purchase['payment_flag'] == 1) {
                $tokentData = array(
                    'user_id' => $userId,
                    'tokens' => $token_user + $token_qty,
                );
                if ($token_user == 0) {
                    //insert firt time tokens

                    $id = $this->common->insert_data('tbl_token_users', $tokentData);
                } else {
                    //update tokens
                    $id = $this->common->new_update_data($tokentData, 'tbl_token_users', array('user_id' => $userId));
                }
                $message = 'You have now ' . $tokentData['tokens'] . ' tokens available';
                $this->session->set_flashdata('success', $message);
            } else {
                $this->session->set_flashdata('error', 'Payment Failed');
            }
            redirect(base_url('account/buy_tokens'));
        } else {
            $this->session->set_flashdata('error', 'Please Select Token');
            redirect(base_url('account/buy_tokens'));
        }
    }

    public function follow_payment()
    {
        $modelId = $this->input->post('modelId');
        $userId = $this->input->post('userId');
        $type = $this->input->post('type');
        $price = $this->input->post('price');

        $postId = $this->input->post('post_id');
        // pre($modelId);
        if ($userId > 0 && $modelId > 0) {


            $purchase = $this->common->purchase($type, $userId, $price);

            if ($purchase['payment_flag'] == 1) {


                if ($type == 1) {
                    $followersData = array(
                        'model_id' => $modelId,
                        'user_id' => $userId,
                        'payment_id' => $purchase['payment_id'],
                        'created' => get_date()
                    );
                    $id = $this->common->insert_data('tbl_model_followers', $followersData);
                    $purchase['message'] = "Payment Success, Now you can follow this model";
                }

                if ($type == 3) {
                    $followersData = array(
                        'post_id' => $postId,
                        'user_id' => $userId,
                        'payment_id' => $purchase['payment_id'],
                        'category' => '0'            // 0 for private video, 1 for custom video
                    );
                    $followersData['post_id'] = $this->input->post('post_id');
                    $id = $this->common->insert_data('tbl_videos', $followersData);
                    $purchase['message'] = "Successfully purchased this video, go to premium video section to view this video";
                }
                $purchase['type'] = "success";
            } else {
                $this->session->set_flashdata('error', 'Payment Failed');
                $purchase['message'] = "Payment Failed, Please Try again";
                $purchase['type'] = "error";
            }

            echo json_encode($purchase);
            // redirect(base_url('account/buy_tokens'));
        } else {
            $this->session->set_flashdata('error', 'Please Login to Continue');
            redirect(base_url('account/buy_tokens'));
        }
    }

    //for add fav post
    function post_add_to_fav()
    {

        if ($this->input->is_ajax_request()) {
            $response['message'] = "Something went wrong";
            $response['type'] = "error";
            $postId = $this->input->post('postId');
            $userId = $this->user_id;
            $insertData = array(
                'post_id' => $postId,
                'user_id' => $userId,
                'created    ' => get_date(),
                'updated' => get_date()
            );
            $insert = $this->common->insert_data('tbl_model_posts_fav', $insertData);
            if ($insert > 0) {
                $response['message'] = "Added to favourites";
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //for remvoe fav post
    function post_remove_fav()
    {

        if ($this->input->is_ajax_request()) {
            $response['message'] = "Something went wrong";
            $response['type'] = "error";
            $postId = $this->input->post('postId');
            $userId = $this->user_id;
            $insertData = array(
                'post_id' => $postId,
                'user_id' => $userId
            );
            $insert = $this->common->delete_data('tbl_model_posts_fav', $insertData);
            if ($insert > 0) {
                $response['message'] = "Remove from favourites";
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //favourites page
    function fav()
    {
        $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
        $this->data['headTitle'] = $this->lang->line("lbl_fav");
        $this->data['module'] = "favourites";
        $this->load->view('front/account/mainpage', $this->data);
    }

    //for load favourite posts
    public function fav_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_fav_post($this->user_id, $limit, $filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }


    //follows page
    function follows()
    {
        $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
        $this->data['headTitle'] = $this->lang->line("lbl_follows");
        $this->data['module'] = "follows";
        $this->load->view('front/account/mainpage', $this->data);
    }

    //for load followed post
    public function load_follows_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_followed_post($this->user_id, $limit, $filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //notification
    public function notification()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_notification");
        $this->data['notifications'] = $this->user_model->get_notification_list($this->user_id);
        $this->data['module'] = "notification";
        $this->load->view('front/account/mainpage', $this->data);
    }

    //change notification action
    public function change_notification_action()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $filters['notification_ids'] = check_variable_value($this->input->post('notification_ids'));
            $notification = explode(",", $filters['notification_ids']);
            //pre($notification);
            $filters['action'] = check_variable_value($this->input->post('action'));


            //1 for read notification
            //2 for delete notification
            //3 for mark all read
            //4 for all delete
            if ($filters['action'] == 1) {
                $res = 0;
                foreach ($notification as $key => $value) {
                    // pre($value);
                    if ($value != 0) {

                        $condition = array('id' => $value);
                        $updateData = array('read_flag' => 1);
                        $res = $this->common->new_update_data($updateData, 'tbl_notification', $condition);
                    }
                }
                //pre($res);
                //pre($this->db->last_query());
                if ($res == true) {

                    $response['message'] = $this->lang->line('notification_read_succ');
                    $response['type'] = "success";
                } else {
                    $response['message'] = $this->lang->line('notification_select_err');
                    $response['type'] = "error";
                }
            } else if ($filters['action'] == 2) {
                $res = 0;
                foreach ($notification as $key => $value) {
                    if ($value != 0) {
                        $condition = array('id' => $value);
                        $res = $this->common->delete_data('tbl_notification', $condition);
                    }
                }
                if ($res == true) {

                    $response['message'] = $this->lang->line('notification_dele_succ');
                    $response['type'] = "success";
                } else {
                    $response['message'] = $this->lang->line('notification_select_err');
                    $response['type'] = "error";
                }
            } else if ($filters['action'] == 3) {
                $res = 0;
                $condition = array('user_id' => $this->user_id);
                $updateData = array('read_flag' => 1);
                $this->common->new_update_data($updateData, 'tbl_notification', $condition);

                $response['message'] = $this->lang->line('notification_read_succ');
                $response['type'] = "success";
            } else if ($filters['action'] == 4) {
                $res = 0;
                $condition = array('user_id' => $this->user_id);
                $res = $this->common->delete_data('tbl_notification', $condition);

                $response['message'] = $this->lang->line('notification_dele_succ');
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function detail_request_video($postId)
    {
        if ($postId > 0) {
            $this->data['modelData'] = $this->common->get_modelsData_byPostId($postId);
            //pre($this->data['modelData']);
            $this->data['headTitle'] = $this->data['modelData']['name'];
            $this->data['module'] = "custom_video_request_page";
            $this->load->view('front/mainpage', $this->data);
        } else {
            redirect(base_url());
        }
    }
    public function demo()
    {
        $this->data['module'] = "demo";
        $this->load->view('front/account/mainpage', $this->data);
    }
}
