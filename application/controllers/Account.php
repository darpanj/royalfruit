<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Account extends Web_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('image_lib');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/User_model', 'user_model');
    }


    // when user login success than this function will be call
    function dashboard()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_dashboard");
        $this->data['module'] = "your_feed";
        $this->load->view('front/account/mainpage', $this->data);
    }

    // when model login success than this function will be call
    function application()
    {
        // echo "<h1>Welcome Model " . $this->data['user_detail']['name'] . "</h1><br>";
        // echo "<a href='" . base_url('home/logout') . "'>Logout</a>";
        // pre('application');
        $this->data['headTitle'] = $this->lang->line("lbl_title_dashboard");
        $this->data['module'] = "dashboard";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function buy_tokens()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "buy_tokens";
        $this->data['tokens'] = $this->common->get_data('tbl_token', '*', array('status' => 'active'));
        //pre($this->data['tokens']);
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function custom_videos($status = 'listing')
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        if ($status == 'listing') {
            $this->data['module'] = "completed_custom_videos";
            $this->data['video_list'] = $this->user_model->get_all_custom_videos();
        }
        $this->load->view('front/account/mainpage', $this->data);
    }

<<<<<<< HEAD
    /*public function message()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $userId = !empty($this->user_id) ? $this->user_id : 0;
        if ($this->data['user_detail']['type'] == 'user') {
            $this->data['chatUsers'] = $this->common->get_chat_users($userId);
            $this->data['model_follower_id'] = "";
            $this->data['chatMessages'] = "";
            if (!empty($this->data['chatUsers'])) {
                $this->data['chatMessages'] = $this->common->get_messages($this->data['chatUsers'][0]['mf_id']);
                $this->data['model_follower_id'] = $this->data['chatUsers'][0]['mf_id'];
            }
            $this->data['module'] = "message";
            $this->load->view('front/account/mainpage', $this->data);
        }
        if ($this->data['user_detail']['type'] == 'model') {
            $this->data['chatUsers'] = $this->common->get_chat_users_for_models($userId);
            $this->data['model_follower_id'] = "";
            $this->data['chatMessages'] = "";
            //pre($this->data['chatUsers']);
            //if(!empty($this->data['chatUsers'])) {
                //$this->data['chatMessages'] = $this->common->get_messages_for_models($this->data['chatUsers'][0]['mf_id']);
                //$this->data['model_follower_id'] = $this->data['chatUsers'][0]['mf_id'];
            //}
            $this->data['module'] = "message";
            $this->load->view('front/account/model/mainpage', $this->data);
        }
    }*/

    public function message()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $userId = !empty($this->user_id) ? $this->user_id : 0;
        if ($this->data['user_detail']['type'] == 'user') {
            $this->data['chatUsers'] = $this->common->get_chat_users_for_models($userId);
            $this->data['model_follower_id'] = "";
            $this->data['chatMessages'] = "";
            $this->data['module'] = "message";
            $this->load->view('front/account/mainpage', $this->data);
        }
        if ($this->data['user_detail']['type'] == 'model') {
            $this->data['chatUsers'] = $this->common->get_chat_users_for_models($userId);
            $this->data['model_follower_id'] = "";
            $this->data['chatMessages'] = "";
            $this->data['module'] = "message";
            $this->load->view('front/account/model/mainpage', $this->data);
        }
    }

    /*public function get_chat_user()
    {
        $html = '';
        $content['status'] = 412;
        $content['message'] = $this->lang->line('err_something_went_wrong');
        $content['html'] = $html;
        $search_keyword = ($this->input->get('search_keyword')) ? $this->input->get('search_keyword') : '';
        if ($search_keyword != '') {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $chatUsers = $this->common->get_chat_users($userId, $search_keyword);
            if (!empty($chatUsers)) {
                foreach ($chatUsers as $kCU => $vCU) {
                    $date = date("d M", strtotime($vCU['created_date']));
                    $image = checkImage(2, $vCU['profile_image']);
                    $html .= '<div class="chat_list active_chat" onclick="getChat(' . $vCU['mf_id'] . ')" id="' . $vCU['mf_id'] . '">
                            <div class="chat_people">
                                <div class="chat_img"> <img src="' . $image . '" alt="' . $vCU['model_name'] . '"> </div>
                                <div class="chat_ib">
                                    <h5>' . $vCU['model_name'] . ' <span class="chat_date">' . $date . '</span></h5>
                                </div>
                            </div>
                        </div>';
                    $content['status'] = 200;
                    $content['message'] = $this->lang->line('succ');
                    $content['html'] = $html;
                }
            }
        } else {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $chatUsers = $this->common->get_chat_users($userId);
            if (!empty($chatUsers)) {
                foreach ($chatUsers as $kCU => $vCU) {
                    $date = date("d M", strtotime($vCU['created_date']));
                    $image = checkImage(2, $vCU['profile_image']);
                    $html .= '<div class="chat_list active_chat" onclick="getChat(' . $vCU['mf_id'] . ')" id="' . $vCU['mf_id'] . '">
                            <div class="chat_people">
                                <div class="chat_img"> <img src="' . $image . '" alt="' . $vCU['model_name'] . '"> </div>
                                <div class="chat_ib">
                                    <h5>' . $vCU['model_name'] . ' <span class="chat_date">' . $date . '</span></h5>
                                </div>
                            </div>
                        </div>';
                    $content['status'] = 200;
                    $content['message'] = $this->lang->line('succ');
                    $content['html'] = $html;
                }
            }
        }
        echo json_encode($content);
    }*/

    public function get_chat_user()
    {
        $html = '';
        $content['status'] = 412;
        $content['message'] = $this->lang->line('err_something_went_wrong');
        $content['html'] = $html;
        $search_keyword = ($this->input->get('search_keyword')) ? $this->input->get('search_keyword') : '';
        if ($search_keyword != '') {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $chatUsers = $this->common->get_chat_users_for_models($userId, $search_keyword);
            if (!empty($chatUsers)) {
                foreach ($chatUsers as $kCU => $vCU) {
                    $date = date("d M", strtotime($vCU['created_date']));
                    $image = checkImage(5, $vCU['profile_image']);
                    $html .= '<div class="chat_list active_chat" onclick="getChat(' . $vCU['thread_id'] . ')" id="' . $vCU['thread_id'] . '">
                            <div class="chat_people">
                                <div class="chat_img"> <img src="' . $image . '" alt="' . $vCU['user_name'] . '"> </div>
                                <div class="chat_ib">
                                    <h5>' . $vCU['user_name'] . ' <span class="chat_date">' . $date . '</span></h5>
                                </div>
                            </div>
                        </div>';
                    $content['status'] = 200;
                    $content['message'] = $this->lang->line('succ');
                    $content['html'] = $html;
                }
            }
        } else {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $chatUsers = $this->common->get_chat_users_for_models($userId);
            if (!empty($chatUsers)) {
                foreach ($chatUsers as $kCU => $vCU) {
                    $date = date("d M", strtotime($vCU['created_date']));
                    $image = checkImage(5, $vCU['profile_image']);
                    $html .= '<div class="chat_list active_chat" onclick="getChat(' . $vCU['thread_id'] . ')" id="' . $vCU['thread_id'] . '">
                            <div class="chat_people">
                                <div class="chat_img"> <img src="' . $image . '" alt="' . $vCU['user_name'] . '"> </div>
                                <div class="chat_ib">
                                    <h5>' . $vCU['user_name'] . ' <span class="chat_date">' . $date . '</span></h5>
                                </div>
                            </div>
                        </div>';
                    $content['status'] = 200;
                    $content['message'] = $this->lang->line('succ');
                    $content['html'] = $html;
                }
            }
        }
        echo json_encode($content);
    }


    public function get_chat_user_by_type()
    {
        $html = '';
        $content['status'] = 412;
        $content['message'] = $this->lang->line('err_something_went_wrong');
        $content['html'] = $html;
        (int) $type = ($this->input->get('type')) ? $this->input->get('type') : '';
        /*if($type != '') {*/
        $userId = !empty($this->user_id) ? $this->user_id : 0;
        if ($type != '') {
            $chatUsers = $this->common->get_chat_users_for_models($userId, '', $type);
        } else {
            $chatUsers = $this->common->get_chat_users_for_models($userId, '', '');
        }
        if (!empty($chatUsers)) {
            foreach ($chatUsers as $kCU => $vCU) {
                $date = date("d M", strtotime($vCU['created_date']));
                if ($vCU['type'] == 'model') {
                    $uImage = checkImage(5, $vCU['profile_image']);
                } else {
                    $uImage = checkImage(1, $vCU['profile_image']);
                }
                $html .= '<div class="chat_list active_chat" onclick="getChat(' . $vCU['thread_id'] . ')" id="' . $vCU['thread_id'] . '">
                            <div class="chat_people">
                                <div class="chat_img"> <img src="' . $uImage . '" alt="' . $vCU['user_name'] . '"> </div>
                                <div class="chat_ib">
                                    <h5>' . $vCU['user_name'] . ' <span class="chat_date">' . $date . '</span></h5>
                                </div>
                            </div>
                        </div>';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            }
        }
        echo json_encode($content);
    }

    public function get_chat()
    {
        $html = '';
        $content['status'] = 412;
        $content['message'] = $this->lang->line('err_something_went_wrong');
        $content['html'] = $html;
        $model_follower_id = ($this->input->post('model_follower_id')) ? (int) $this->input->post('model_follower_id') : '';
        if ($model_follower_id != '') {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            //$chatMessages = $this->common->get_data_by_id('tbl_messages', 'model_follower_id', $model_follower_id, '', '', 'created_at', 'ASC', '', 'array');
            $chatMessages = $this->common->get_messages($model_follower_id);
            if (!empty($chatMessages)) {
                $html .= '<input type="hidden" name="model_follower_id" id="model_follower_id" value="' . $model_follower_id . '">';
                foreach ($chatMessages as $kCM => $vCM) {
                    //$image = checkImage(2, $vCM['profile_image']);
                    //$userData = $this->common->get_data_by_id('tbl_users', 'id', $vCM['sender_id'], 'profile_image', '', '', '', '', 'row');
                    $image = checkImage(2, $vCM['profile_image']);
                    $date = date("M d", strtotime($vCM['created_at']));
                    $time = date("h:i A", strtotime($vCM['created_at']));
                    if ($vCM['sender_id'] == $userId) {
                        $html .= '<div class="outgoing_msg">
                                    <div class="sent_msg">
                                        <p>' . $vCM['message_text'] . '</p>
                                        <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                    </div>
                                  </div>';
                    } else {
                        $html .= '<div class="incoming_msg">
                                    <div class="incoming_msg_img"> <img src="' . $image . '" alt="sunil"> </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>' . $vCM['message_text'] . '</p>
                                            <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                        </div>
                                    </div>
                                </div>';
                    }
                }
                //$html .= '</div>';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            } else {
                $html .= '<h3>No chat history found.</h3>
                            <input type="hidden" name="model_follower_id" id="model_follower_id" value="' . $model_follower_id . '">';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            }
        }
        echo json_encode($content);
    }

    public function get_chat_of_models()
    {
        $html = '';
        $content['status'] = 412;
        $content['message'] = $this->lang->line('err_something_went_wrong');
        $content['html'] = $html;
        $user_id = ($this->input->post('user_id')) ? (int) $this->input->post('user_id') : '';
        if ($user_id != '') {
            $model_id = !empty($this->user_id) ? $this->user_id : 0;
            //$chatMessages = $this->common->get_data_by_id('tbl_messages', 'model_follower_id', $model_follower_id, '', '', 'created_at', 'ASC', '', 'array');
            $chatMessages = $this->common->get_messages_of_model_chat($user_id, $model_id);
            if (!empty($chatMessages)) {
                $html .= '<input type="hidden" name="user_id" id="user_id" value="' . $user_id . '">';
                foreach ($chatMessages as $kCM => $vCM) {
                    //$image = checkImage(2, $vCM['profile_image']);
                    //$userData = $this->common->get_data_by_id('tbl_users', 'id', $vCM['sender_id'], 'profile_image', '', '', '', '', 'row');
                    $image = checkImage(2, $vCM['profile_image']);
                    $date = date("M d", strtotime($vCM['created_at']));
                    $time = date("h:i A", strtotime($vCM['created_at']));
                    if ($vCM['sender_id'] == $model_id) {
                        $html .= '<div class="outgoing_msg">
                                    <div class="sent_msg">
                                        <p>' . $vCM['message_text'] . '</p>
                                        <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                    </div>
                                  </div>';
                    } else {
                        $html .= '<div class="incoming_msg">
                                    <div class="incoming_msg_img"> <img src="' . $image . '" alt="sunil"> </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>' . $vCM['message_text'] . '</p>
                                            <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                        </div>
                                    </div>
                                </div>';
                    }
                }
                //$html .= '</div>';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            } else {
                $html .= '<h3>No chat history found.</h3>
                            <input type="hidden" name="user_id" id="user_id" value="' . $user_id . '">';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            }
        }
        echo json_encode($content);
    }

    public function get_chat_by_thread_id()
    {
        $html = '';
        $content['status'] = 412;
        $content['message'] = $this->lang->line('err_something_went_wrong');
        $content['html'] = $html;
        $thread_id = ($this->input->post('thread_id')) ? (int) $this->input->post('thread_id') : '';
        if ($thread_id != '') {
            $model_id = !empty($this->user_id) ? $this->user_id : 0;
            //$chatMessages = $this->common->get_data_by_id('tbl_messages', 'model_follower_id', $model_follower_id, '', '', 'created_at', 'ASC', '', 'array');
            $chatMessages = $this->common->get_messages_of_model_chat($thread_id, $model_id);
            if (!empty($chatMessages)) {
                $html .= '<input type="hidden" name="thread_id" id="thread_id" value="' . $thread_id . '">';
                foreach ($chatMessages as $kCM => $vCM) {
                    //$image = checkImage(2, $vCM['profile_image']);
                    //$userData = $this->common->get_data_by_id('tbl_users', 'id', $vCM['sender_id'], 'profile_image', '', '', '', '', 'row');
                    $image = checkImage(2, $vCM['profile_image']);
                    $date = date("M d", strtotime($vCM['created_at']));
                    $time = date("h:i A", strtotime($vCM['created_at']));
                    if ($vCM['sender_id'] == $model_id) {
                        $html .= '<div class="outgoing_msg">
                                    <div class="sent_msg">
                                        <p>' . $vCM['message_text'] . '</p>
                                        <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                    </div>
                                  </div>';
                    } else {
                        $html .= '<div class="incoming_msg">
                                    <div class="incoming_msg_img"> <img src="' . $image . '" alt="sunil"> </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>' . $vCM['message_text'] . '</p>
                                            <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                        </div>
                                    </div>
                                </div>';
                    }
                }
                //$html .= '</div>';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            } else {
                $html .= '<h3>No chat history found.</h3>
                            <input type="hidden" name="thread_id" id="thread_id" value="' . $thread_id . '">';
                $content['status'] = 200;
                $content['message'] = $this->lang->line('succ');
                $content['html'] = $html;
            }
        }
        echo json_encode($content);
    }

    public function send_message()
    {
        $html = '';
        $data = array('status' => 412, 'message' => $this->lang->line('err_something_went_wrong'), 'html' => $html);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules($this->send_message_rules());
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
            } else {
                $sender_id = !empty($this->user_id) ? $this->user_id : 0;
                $message_text = ($this->input->post('message_text')) ? $this->input->post('message_text') : '';
                $model_follower_id = ($this->input->post('model_follower_id')) ? $this->input->post('model_follower_id') : 0;
                if ($model_follower_id > 0) {
                    $data_insert = array(
                        'model_follower_id' => $model_follower_id,
                        'sender_id' => $sender_id,
                        'message_text' => $message_text,
                        'created_at' => get_date(),
                    );
                    $insertedId = $this->common->insert_data('tbl_messages', $data_insert);
                    if ($insertedId > 0) {
                        $data['status'] = 200;
                        $data['message'] = $this->lang->line('succ');

                        $userId = !empty($this->user_id) ? $this->user_id : 0;
                        $chatMessages = $this->common->get_messages($model_follower_id);
                        if (!empty($chatMessages)) {
                            $html .= '<input type="hidden" name="model_follower_id" id="model_follower_id" value="' . $model_follower_id . '">';
                            foreach ($chatMessages as $kCM => $vCM) {
                                $image = checkImage(2, $vCM['profile_image']);
                                $date = date("M d", strtotime($vCM['created_at']));
                                $time = date("h:i A", strtotime($vCM['created_at']));
                                if ($vCM['sender_id'] == $userId) {
                                    $html .= '<div class="outgoing_msg">
                                                <div class="sent_msg">
                                                    <p>' . $vCM['message_text'] . '</p>
                                                    <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                                </div>
                                              </div>';
                                } else {
                                    $html .= '<div class="incoming_msg">
                                                <div class="incoming_msg_img"> <img src="' . $image . '" alt="sunil"> </div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p>' . $vCM['message_text'] . '</p>
                                                        <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                                    </div>
                                                </div>
                                            </div>';
                                }
                            }
                            //$html .= '</div>';
                            $data['html'] = $html;
                        } else {
                            $html .= '<h3>No chat history found.</h3>
                                        <input type="hidden" name="model_follower_id" id="model_follower_id" value="' . $model_follower_id . '">';
                            $data['html'] = $html;
                        }
                    }
                }
            }
            echo json_encode($data);
        } else {
            base_url(base_url());
        }
    }

    public function send_message_rules()
    {
        $rules = array(
            array(
                'field' => 'message_text',
                'label' => 'Message Text',
                'rules' => 'trim|required',
            )
        );
        return $rules;
    }

    /*public function send_message_from_model() {
        $html = '';
        $data = array('status' => 412, 'message' => $this->lang->line('err_something_went_wrong'), 'html' => $html);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules($this->send_message_rules());
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
            } else {
                $sender_id = !empty($this->user_id) ? $this->user_id : 0;
                $message_text = ($this->input->post('message_text')) ? $this->input->post('message_text') : '';
                $thread_id = ($this->input->post('thread_id')) ? $this->input->post('thread_id') : 0;
                $thredData = $this->common->get_data_by_id('tbl_message_threads', 'id', $thread_id, '', '', '', '', '', 'row');
                if(!empty($thredData)) {
                    $chatUserOne = $thredData['chat_user_one'];
                    $chatUserTwo = $thredData['chat_user_two'];
                    if($chatUserOne == $this->user_id) {
                        $receiver_id = $chatUserTwo;
                    } else {
                        $receiver_id = $chatUserOne;
                    }
                }
                if ($thread_id > 0) {
                    $data_insert = array(
                        'message_thread_id' => $thread_id,
                        'sender_id' => $sender_id,
                        'receiver_id' => $receiver_id,
                        'message_text' => $message_text,
                        'created_at' => get_date(),
                    );
                    $insertedId = $this->common->insert_data('tbl_messages', $data_insert);
                    if($insertedId > 0) {
                        $data['status'] = 200;
                        $data['message'] = $this->lang->line('succ');

                        $userId = !empty($this->user_id) ? $this->user_id : 0;
                        $chatMessages = $this->common->get_messages($model_follower_id);
                        if(!empty($chatMessages)) {
                            $html .= '<input type="hidden" name="model_follower_id" id="model_follower_id" value="'.$model_follower_id.'">';
                            foreach($chatMessages as $kCM => $vCM) {
                                $image = checkImage(2, $vCM['profile_image']);
                                $date = date("M d", strtotime($vCM['created_at']));
                                $time = date("h:i A", strtotime($vCM['created_at']));
                                if($vCM['sender_id'] == $userId) {
                                    $html .= '<div class="outgoing_msg">
                                                <div class="sent_msg">
                                                    <p>'.$vCM['message_text'].'</p>
                                                    <span class="time_date"> '.$time.'    |    '.$date.'</span>
                                                </div>
                                              </div>';
                                } else {
                                    $html .= '<div class="incoming_msg">
                                                <div class="incoming_msg_img"> <img src="'.$image.'" alt="sunil"> </div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p>'.$vCM['message_text'].'</p>
                                                        <span class="time_date"> '.$time.'    |    '.$date.'</span>
                                                    </div>
                                                </div>
                                            </div>';
                                }
                            }
                            //$html .= '</div>';
                            $data['html'] = $html;
                        } else {
                            $html .= '<h3>No chat history found.</h3>
                                        <input type="hidden" name="model_follower_id" id="model_follower_id" value="'.$model_follower_id.'">';
                            $data['html'] = $html;
                        }


                    }
                }
            }
            echo json_encode($data);
        } else {
            base_url(base_url());
        }
    }*/

    public function send_message_from_model()
    {
        $html = '';
        $data = array('status' => 412, 'message' => $this->lang->line('err_something_went_wrong'), 'html' => $html);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules($this->send_message_rules());
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
            } else {
                $sender_id = !empty($this->user_id) ? $this->user_id : 0;
                $message_text = ($this->input->post('message_text')) ? $this->input->post('message_text') : '';
                $thread_id = ($this->input->post('thread_id')) ? $this->input->post('thread_id') : 0;
                $thredData = $this->common->get_data_by_id('tbl_message_threads', 'id', $thread_id, '', '', '', '', '', 'row');
                if (!empty($thredData)) {
                    $chatUserOne = $thredData['chat_user_one'];
                    $chatUserTwo = $thredData['chat_user_two'];
                    if ($chatUserOne == $this->user_id) {
                        $receiver_id = $chatUserTwo;
                    } else {
                        $receiver_id = $chatUserOne;
                    }
                }
                if ($thread_id > 0) {
                    $data_insert = array(
                        'message_thread_id' => $thread_id,
                        'sender_id' => $sender_id,
                        'receiver_id' => $receiver_id,
                        'message_text' => $message_text,
                        'created_at' => get_date(),
                    );
                    $insertedId = $this->common->insert_data('tbl_messages', $data_insert);
                    if ($insertedId > 0) {
                        $data['status'] = 200;
                        $data['message'] = $this->lang->line('succ');

                        $userId = !empty($this->user_id) ? $this->user_id : 0;
                        $chatMessages = $this->common->get_messages_of_model_chat($thread_id);
                        if (!empty($chatMessages)) {
                            $html .= '<input type="hidden" name="thread_id" id="thread_id" value="' . $thread_id . '">';
                            foreach ($chatMessages as $kCM => $vCM) {
                                $image = checkImage(5, $vCM['profile_image']);
                                $date = date("M d", strtotime($vCM['created_at']));
                                $time = date("h:i A", strtotime($vCM['created_at']));
                                if ($vCM['sender_id'] == $userId) {
                                    $html .= '<div class="outgoing_msg">
                                                <div class="sent_msg">
                                                    <p>' . $vCM['message_text'] . '</p>
                                                    <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                                </div>
                                              </div>';
                                } else {
                                    $html .= '<div class="incoming_msg">
                                                <div class="incoming_msg_img"> <img src="' . $image . '" alt="sunil"> </div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p>' . $vCM['message_text'] . '</p>
                                                        <span class="time_date"> ' . $time . '    |    ' . $date . '</span>
                                                    </div>
                                                </div>
                                            </div>';
                                }
                            }
                            //$html .= '</div>';
                            $data['html'] = $html;
                        } else {
                            $html .= '<h3>No chat history found.</h3>
                                        <input type="hidden" name="thread_id" id="thread_id" value="' . $thread_id . '">';
                            $data['html'] = $html;
                        }
                    }
                }
            }
            echo json_encode($data);
        } else {
            base_url(base_url());
        }
    }


    public function password()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_password");
        if ($this->data['user_detail']['type'] == 'model') {
            $this->data['module'] = "password";
            $this->load->view('front/account/model/mainpage', $this->data);
        }
        if ($this->data['user_detail']['type'] == 'user') {
            $this->data['module'] = "password";
            $this->load->view('front/account/mainpage', $this->data);
        }
=======
    public function message()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "message";
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function password()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_password");
        $this->data['module'] = "password";
        $this->load->view('front/account/mainpage', $this->data);
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    }

    public function premium_snap()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "premium_snap";
        $this->load->view('front/account/mainpage', $this->data);
    }

    public function premium_videos()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_premium_content");
        $this->data['module'] = "premium_videos";
        $this->data['premium_videos'] = $this->user_model->get_premium_videos($this->user_id);
        //pre($this->data['premium_videos']);
        $this->load->view('front/account/mainpage', $this->data);
    }

<<<<<<< HEAD
    public function add_comment()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $post_id = ($this->input->post('post_id') != '') ? (int) $this->input->post('post_id') : 0;
            $comment = ($this->input->post('comment') != '') ? $this->input->post('comment') : '';
            if ($post_id > 0) {
                $insertData['post_id'] = $post_id;
                $insertData['commenter_id'] = $this->user_id;
                $insertData['comment'] = $comment;
                $insertData['created_at'] = get_date();
                $insert = $this->common->insert_data('tbl_model_post_comments', $insertData);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function delete_comment()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $comment_id = ($this->input->post('comment_id') != '') ? (int) $this->input->post('comment_id') : 0;
            if ($comment_id > 0) {
                $where['id'] = $comment_id;
                $deleteComment = $this->common->delete_data('tbl_model_post_comments', $where);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function settings()
    {
        if ($this->data['user_detail']['type'] == 'model') {
            $this->data['user'] = $this->common->get_data_by_id('tbl_users', 'id', $this->user_id, '*', array(), '', '', '', 'row');
            $this->data['headTitle'] = $this->lang->line("lbl_settings");
            $this->data['module'] = "settings";
            $this->load->view('front/account/model/mainpage', $this->data);
        }
        if ($this->data['user_detail']['type'] == 'user') {
            $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
            $this->data['headTitle'] = $this->lang->line("lbl_settings");
            $this->data['module'] = "settings";
            $this->load->view('front/account/mainpage', $this->data);
        }
=======
    public function settings()
    {
        $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
        $this->data['headTitle'] = $this->lang->line("lbl_settings");
        $this->data['module'] = "settings";
        $this->load->view('front/account/mainpage', $this->data);
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    }

    public function load_suggestion_models()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $models = $this->user_model->get_suggestion_models($limit, $this->user_id);
            foreach ($models as $key => $value) {
                $models[$key]['cover_image'] = checkImage(3, $value['cover_image']);
                $models[$key]['profile_image'] = checkImage(2, $value['profile_image']);
                $models[$key]['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 0));
                $models[$key]['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1));
            }
            $response['data'] = $models;
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function get_model_by_id()
    {
        $modelId = check_variable_value($this->input->post('userId'));
        $models = array();
        if ($modelId > 0) {
            $result['models'] = $this->common->get_models_with_price($modelId);
            //pre($result['models']);
            //$result['models']['price'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
        }
        echo json_encode($result);
    }

    public function load_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_feed_models($this->user_id, $limit, $filters);
            //pre('fsdfsdaf');
            foreach ($response['feed_models'] as $key => $value) {
                if ($value['post_type'] == '0') {
                    // $config['image_library']   = 'gd2';

                    // $config['source_image'] =  FCPATH . SITE_UPD . '/models' . '/' . $value['post_image'];
                    // $config['wm_text'] = 'RoyalFruit';
                    // $config['wm_type'] = 'text';

                    // $config['wm_font_size'] = '16';
                    // $config['wm_font_color'] = '#0000FF';

                    // $this->load->library('image_lib', $config);
                    // $this->image_lib->initialize($config);
                    // $this->image_lib->watermark();
                    // if (!$this->image_lib->watermark()) {
                    // }
                    $this->overlayWatermark($value['post_image']);
                } else {

                    // $inputvideo = FCPATH . SITE_UPD . '/models' . '/' . $value['post_image'];
                    // $logo = FCPATH . ADM_MEDIA  . 'logo.png';
                    // // pre($logo);
                    // //exec("ffmpeg -i " . $inputvideo . " " . $logo . ".flv");
                    // $mark = "ffmpeg -i " . $inputvideo . " -i " . $logo . " -filter_complex " . '"overlay=x=(main_w-overlay_w):y=(main_h-overlay_h)/(main_h-overlay_h)"' . " " .  FCPATH . SITE_UPD . 'models' . '/' . uniqid() . "topright.mp4";
                    // // $mark = "start notepad";
                    // pre(exec($mark));
                }
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function get_post_by_id()
    {
        $postId = check_variable_value($this->input->post('postId'));
        // pre($postId);
        $result = array();
        if ($postId > 0) {
            $result['models'] = $this->common->get_post_by_id($postId);
            //pre($result['models']);
            //$result['models']['price'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
        }
        echo json_encode($result);
    }

    // public function textWatermark($source_image)
    // {
    //     $config['source_image'] = $source_image;
    //     //The image path,which you would like to watermarking
    //     $config['wm_text'] = 'http://getsourcecodes.com';
    //     $config['wm_type'] = 'text';
    //     // $config['wm_font_path'] = './fonts/atlassol.ttf';
    //     $config['wm_font_size'] = 16;
    //     $config['wm_font_color'] = 'ffffff';
    //     $config['wm_vrt_alignment'] = 'middle';
    //     $config['wm_hor_alignment'] = 'right';
    //     $config['wm_padding'] = '20';
    //     $this->image_lib->initialize($config);
    //     if (!$this->image_lib->watermark()) {
    //         return $this->image_lib->display_errors();
    //     }
    // }

    public function change_password()
    {
        $this->form_validation->set_rules(
            'old_password',
            'Old Password',
            'required|callback_oldPassword',
            array('required' => 'You must provide a %s.', 'oldPassword' => $this->lang->line('err_old_password'))
        );
        $this->form_validation->set_rules(
            'vPassword',
            'Password',
            'required',
            array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('cPassword', 'Password Confirmation', 'required|matches[vPassword]', array(
            'matches' => 'Password Does not match.',
        ));
        if ($this->form_validation->run()) {
            $pass = md5($this->input->post('vPassword'));
            $check = $this->common->update_data($data = array('password' => $pass), 'tbl_users', 'id', $this->user_id);
            if ($check) {
                $this->session->set_flashdata('success', 'Password Changed successfully.');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong.');
            }
        } else {
            $i = 0;
            foreach ($this->form_validation->error_array() as $key => $value) {
                if ($i != 0) {
                    continue;
                }
                $this->session->set_flashdata("error", $value);
                $i++;
            }
            $this->session->set_flashdata('error', get_error($this->form_validation->error_array()));
        }
        redirect(base_url('account/password'));
    }

    public function update_settings()
    {
        $this->form_validation->set_rules($this->update_settings_check_form());
        if ($this->form_validation->run() == FALSE) {
            $i = 0;
            foreach ($this->form_validation->error_array() as $key => $value) {
                if ($i != 0) {
                    continue;
                }
                $this->session->set_flashdata("error", $value);
                $i++;
            }
            redirect(base_url('account/settings'));
        } else {
            $postData = $this->input->post();
            $iUserId = $this->user_model->update_user($postData, $this->user_id);
            if ($iUserId > 0) {
                $this->common->upload_user_image($_FILES, $this->user_id, 'users');
                $userData = $this->common->get_data_by_id('tbl_users', 'id', $iUserId, '*', array(), '', '', '', 'row');
                $this->session->set_flashdata('success', $this->lang->language['succ_profile_updated']);
                if ($userData['type'] == 'user') {
                    redirect(base_url('account/dashboard'));  // redirecting user dashboard
                } elseif ($userData['type'] == 'model') {
                    redirect(base_url('account/application')); //redirecting model dashboard
                } else {
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('error', "Something went worng");
            }
        }
    }

    protected function update_settings_check_form()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line('lbl_name'),
                'rules' => 'trim|required',
            )
        );
        return $rules;
    }

    public function purchase_tokens()
    {
        $tokenId = $this->input->post('token_id');
        $token_qty = $this->input->post('token_qty');
        if ($tokenId > 0) {
            $userId = $this->user_id;
            $tokensuserData = $this->common->get_data_by_id('tbl_token_users', 'user_id', $userId);
            $token_user = isset($tokensuserData[0]['tokens']) ? $tokensuserData[0]['tokens'] : 0;
            //pre($token_user);

            $type = 0; //0=token
            $price = $this->input->post('token_price');

            $purchase = $this->common->purchase($type, $userId, $price);
            if ($purchase['payment_flag'] == 1) {
                $tokentData = array(
                    'user_id' => $userId,
<<<<<<< HEAD
                    //'tokens' => $token_user + $token_qty,
                    'tokens' => $token_qty,
                    'payment_id' => $purchase['payment_id'],
                    'token_id' => $tokenId,
                    'price' => $price
                );
                /*if ($token_user == 0) {*/
                //insert firt time tokens
                $id = $this->common->insert_data('tbl_token_users', $tokentData);
                /*} else {
                    //update tokens
                    $id = $this->common->new_update_data($tokentData, 'tbl_token_users', array('user_id' => $userId));
                }*/
=======
                    'tokens' => $token_user + $token_qty,
                );
                if ($token_user == 0) {
                    //insert firt time tokens
                    $id = $this->common->insert_data('tbl_token_users', $tokentData);
                } else {
                    //update tokens
                    $id = $this->common->new_update_data($tokentData, 'tbl_token_users', array('user_id' => $userId));
                }
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
                $message = 'You have now ' . $tokentData['tokens'] . ' tokens available';
                $this->session->set_flashdata('success', $message);
            } else {
                $this->session->set_flashdata('error', 'Payment Failed');
            }
            redirect(base_url('account/buy_tokens'));
        } else {
            $this->session->set_flashdata('error', 'Please Select Token');
            redirect(base_url('account/buy_tokens'));
        }
    }

    public function follow_payment()
    {
        $modelId = $this->input->post('modelId');
        $userId = $this->input->post('userId');
        $type = $this->input->post('type');
        $price = $this->input->post('price');

        $postId = $this->input->post('post_id');
        // pre($modelId);
        if ($userId > 0 && $modelId > 0) {


            $purchase = $this->common->purchase($type, $userId, $price);

            if ($purchase['payment_flag'] == 1) {


                if ($type == 1) {
                    $followersData = array(
                        'model_id' => $modelId,
                        'user_id' => $userId,
                        'payment_id' => $purchase['payment_id'],
                        'created' => get_date()
                    );
                    $id = $this->common->insert_data('tbl_model_followers', $followersData);
                    $purchase['message'] = "Payment Success, Now you can follow this model";
                    $notificationType = 1;
                    $userData = $this->common->get_data_by_id('tbl_users', 'id', $userId, '*', array());
                    $message = $userData[0]['name'] . ' ' . MESSEGE_FOR_MODEL_USER_FOLLOWED;
                    $this->common->create_notification($modelId, $message);
                }

                if ($type == 3) {
                    $followersData = array(
                        'post_id' => $postId,
                        'user_id' => $userId,
                        'payment_id' => $purchase['payment_id'],
                        'category' => '0'            // 0 for private video, 1 for custom video
                    );
                    $followersData['post_id'] = $this->input->post('post_id');
                    $id = $this->common->insert_data('tbl_videos', $followersData);
                    $purchase['message'] = "Successfully purchased this video, go to premium video section to view this video";
                }
                $purchase['type'] = "success";
            } else {
                $this->session->set_flashdata('error', 'Payment Failed');
                $purchase['message'] = "Payment Failed, Please Try again";
                $purchase['type'] = "error";
            }

            echo json_encode($purchase);
            // redirect(base_url('account/buy_tokens'));
        } else {
            $this->session->set_flashdata('error', 'Please Login to Continue');
            redirect(base_url('account/buy_tokens'));
        }
    }

    //for add fav post
    function post_add_to_fav()
    {

        if ($this->input->is_ajax_request()) {
            $response['message'] = "Something went wrong";
            $response['type'] = "error";
            $postId = $this->input->post('postId');
            $userId = $this->user_id;
            $insertData = array(
                'post_id' => $postId,
                'user_id' => $userId,
                'created    ' => get_date(),
                'updated' => get_date()
            );
            $insert = $this->common->insert_data('tbl_model_posts_fav', $insertData);
            if ($insert > 0) {
                $response['message'] = "Added to favourites";
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //for remvoe fav post
    function post_remove_fav()
    {

        if ($this->input->is_ajax_request()) {
            $response['message'] = "Something went wrong";
            $response['type'] = "error";
            $postId = $this->input->post('postId');
            $userId = $this->user_id;
            $insertData = array(
                'post_id' => $postId,
                'user_id' => $userId
            );
            $insert = $this->common->delete_data('tbl_model_posts_fav', $insertData);
            if ($insert > 0) {
                $response['message'] = "Remove from favourites";
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //favourites page
    function fav()
    {
        $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
        $this->data['headTitle'] = $this->lang->line("lbl_fav");
        $this->data['module'] = "favourites";
        $this->load->view('front/account/mainpage', $this->data);
    }

    //for load favourite posts
    public function fav_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_fav_post($this->user_id, $limit, $filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }


    //follows page
    function follows()
    {
        $this->data['user'] = $this->user_model->get_single_user(array('id' => $this->user_id));
        $this->data['headTitle'] = $this->lang->line("lbl_follows");
        $this->data['module'] = "follows";
        $this->load->view('front/account/mainpage', $this->data);
    }

    //for load followed post
    public function load_follows_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_followed_post($this->user_id, $limit, $filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //notification
    public function notification()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_notification");
        $this->data['notifications'] = $this->user_model->get_notification_list($this->user_id);
        $this->data['module'] = "notification";
        $this->load->view('front/account/mainpage', $this->data);
    }

    //change notification action
    public function change_notification_action()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $filters['notification_ids'] = check_variable_value($this->input->post('notification_ids'));
            $notification = explode(",", $filters['notification_ids']);
            //pre($notification);
            $filters['action'] = check_variable_value($this->input->post('action'));


            //1 for read notification
            //2 for delete notification
            //3 for mark all read
            //4 for all delete
            if ($filters['action'] == 1) {
                $res = 0;
                foreach ($notification as $key => $value) {
                    // pre($value);
                    if ($value != 0) {

                        $condition = array('id' => $value);
                        $updateData = array('read_flag' => 1);
                        $res = $this->common->new_update_data($updateData, 'tbl_notification', $condition);
                    }
                }
                //pre($res);
                //pre($this->db->last_query());
                if ($res == true) {

                    $response['message'] = $this->lang->line('notification_read_succ');
                    $response['type'] = "success";
                } else {
                    $response['message'] = $this->lang->line('notification_select_err');
                    $response['type'] = "error";
                }
            } else if ($filters['action'] == 2) {
                $res = 0;
                foreach ($notification as $key => $value) {
                    if ($value != 0) {
                        $condition = array('id' => $value);
                        $res = $this->common->delete_data('tbl_notification', $condition);
                    }
                }
                if ($res == true) {

                    $response['message'] = $this->lang->line('notification_dele_succ');
                    $response['type'] = "success";
                } else {
                    $response['message'] = $this->lang->line('notification_select_err');
                    $response['type'] = "error";
                }
            } else if ($filters['action'] == 3) {
                $res = 0;
                $condition = array('user_id' => $this->user_id);
                $updateData = array('read_flag' => 1);
                $this->common->new_update_data($updateData, 'tbl_notification', $condition);

                $response['message'] = $this->lang->line('notification_read_succ');
                $response['type'] = "success";
            } else if ($filters['action'] == 4) {
                $res = 0;
                $condition = array('user_id' => $this->user_id);
                $res = $this->common->delete_data('tbl_notification', $condition);

                $response['message'] = $this->lang->line('notification_dele_succ');
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function detail_request_video($postId)
    {
        if ($postId > 0) {
            $category = 1;
            $post_type = '';
            $this->data['modelData'] = $this->common->get_modelsData_byPostId($postId);
            //pre($this->data['modelData']);
            $this->data['model_post_images'] = $this->common->get_post_by_model_id($this->data['modelData']['id'], $post_type, $category);
            foreach ($this->data['model_post_images'] as $key => $value) {
                $fav_posts = $this->common->get_data_by_id('tbl_model_posts_fav', 'post_id', $value['id'], '*', array('user_id' => $this->user_id));
                if (count($fav_posts) > 0) {
                    $this->data['model_post_images'][$key]['is_fav_post'] = 1;
                } else {
                    $this->data['model_post_images'][$key]['is_fav_post'] = 0;
                }
            }
            $modelId = $this->data['modelData']['id'];
            $userId = $this->user_id;
            $this->data['is_follow'] = $this->common->check_follow_or_not($modelId, $userId);

            $this->data['rules'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $this->data['modelData']['id'], 'rules', '', 'id', 'DESC', '', 'row');
            $this->data['headTitle'] = $this->data['modelData']['name'];
            $this->data['module'] = "custom_video_request_page";
            $this->load->view('front/mainpage', $this->data);
        } else {
            redirect(base_url());
        }
    }
<<<<<<< HEAD
    /* model starts */

    public function freindfeed()
=======

    /* model starts */

    public function friendfeed()
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['module'] = "freindfeed";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

<<<<<<< HEAD
    public function freinds()
    {
        $exceptModels = array();
        $this->data['getSuggestions'] = '';
        //$modelRequestSent = $this->common->get_data_by_id('tbl_friend_requests', 'status', 'active', 'model_id', array('user_id' => $this->user_id));
        $modelRequestSent = $this->common->sent_request_to_models($this->user_id);
        if (!empty($modelRequestSent)) {
            foreach ($modelRequestSent as $kMRS => $vMRS) {
                $exceptModels[] = $vMRS['model_id'];
            }
            $this->db->limit(30);
            $this->data['getSuggestions'] = $this->common->getSuggestedModelsForFriendRequest($exceptModels);
        } else {
            $this->db->limit(30);
            $this->data['getSuggestions'] = $this->common->getSuggestedModelsForFriendRequest($exceptModels);
        }
        $this->data['getFriends'] = $this->common->getFriendsForModels($this->user_id);
=======
    public function friends()
    {
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['module'] = "freinds";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function friend_request()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['module'] = "friend_request";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function pending_request()
    {
<<<<<<< HEAD
        $this->data['sendRequests'] = '';
        $sentPendingRequest = $this->common->get_data_by_id('tbl_friend_requests', 'status', 'active', '*', array('user_id' => $this->user_id, 'request_status' => 0), 'created_at', 'DESC');
        if (!empty($sentPendingRequest)) {
            foreach ($sentPendingRequest as $kMRS => $vMRS) {
                $sentModels[] = $vMRS['model_id'];
            }
            $this->data['sendRequests'] = $this->common->getSentPendingFriendRequest($sentModels);
        }
        //pre($this->data['sendRequests']);
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['module'] = "pending_request";
        $this->load->view('front/account/model/mainpage', $this->data);
    }


<<<<<<< HEAD
    public function add_friend()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $model_id = $this->input->post('model_id');
            if ($model_id > 0) {
                $insertData['model_id'] = $model_id;
                $insertData['user_id'] = $this->user_id;
                $insertData['request_status'] = 0;
                $insertData['created_at'] = get_date();
                $insert = $this->common->insert_data('tbl_friend_requests', $insertData);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
=======
    public function payment()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['module'] = "payment";
        $this->load->view('front/account/model/mainpage', $this->data);
    }
    public function geo_block()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['model_geo_blocking_address'] = $this->common->get_data_by_id('tbl_geo_blocking', 'model_id', $this->user_id, '*');
        // pre($this->data['model_geo_blocking_address']);
        $this->data['module'] = "geo_block";
        $this->load->view('front/account/model/mainpage', $this->data);
    }
    function add_geo_blocking_data()
    {

        //  pre($this->user_id);
        if ($this->user_type == 'model' && $this->user_id > 0) {

            $post = $this->input->post();
            $country = isset($post['country']) ? $post['country'] : '';
            $state = isset($post['state']) ? $post['state'] : '';
            $city = isset($post['city']) ? $post['city'] : '';
            $modelId = $this->user_id;
            $count = $this->common->get_table_count('tbl_geo_blocking', array('model_id' => $modelId, 'country' => $country, 'state' => $state, 'city' => $city));
            if ($count == 0) {
                $insertData = array(
                    'country' => $country,
                    'state' => $state,
                    'city' => $city,
                    'model_id' => $modelId
                );

                // pre($insertData);
                $geoBlockId = $this->common->insert_data('tbl_geo_blocking', $insertData);
                $modelData = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
                //  pre($modelData['geo_block_id']);
                $geoBlockId = $modelData['geo_block_id'] . ',' . $geoBlockId;
                //pre($geoBlockId);
                $update_data = array('geo_block_id' => $geoBlockId);
                //  pre($update_data);
                //  pre($modelId);
                $date = $this->common->update_data($update_data, 'tbl_model_extra', 'model_id', $modelId);
                //  pre($this->db->last_query());
                $this->session->set_flashdata("success", "Location for blocking addedd successfully");
            } else {
                $this->session->set_flashdata("error", "This location already added");
            }
            redirect(base_url() . "/account/geo_block");
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        } else {
            redirect(base_url());
        }
    }

<<<<<<< HEAD
    public function cancel_friend_request()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $request_id = $this->input->post('request_id');
            if ($request_id > 0) {
                /*$updateData['request_status'] = 3;
                $updateData['updated_at'] = get_date();
                $cancelRequest = $this->common->update_data($updateData, 'tbl_friend_requests', 'id', $request_id);*/
                $condition_array = array('id' => $request_id);
                $cancelRequest = $this->common->delete_data('tbl_friend_requests', $condition_array);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function payment()
    {
        $getLastPayment = $this->common->get_last_payment_of_model($this->user_id);
        if (!empty($getLastPayment)) {
            $this->data['last_payment'] = $getLastPayment['price'];
        } else {
            $this->data['last_payment'] = 0;
        }
        $this->data['get_life_time_payment'] = $this->common->get_lifetime_payment_of_model($this->user_id);
        $payment_details_id = $this->common->get_data_by_id('tbl_model_payment_details', 'status', 'active', '*', array('model_id' => $this->user_id), '', '', '', 'row');
        $this->data['w9Form'] = $this->common->get_data_by_id('tbl_model_w9form', 'status', 'active', '*', array('model_id' => $this->user_id), '', 'id', 'DESC', 'row');
        if (!empty($payment_details_id)) {
            $this->data['payment_details'] = $payment_details_id;
            $this->data['pd_id'] = $payment_details_id['id'];
        } else {
            $this->data['pd_id'] = 0;
            $this->data['payment_details'] = '';
        }
        $this->data['headTitle'] = $this->lang->line("lbl_title_payments");
        $this->data['module'] = "payment";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function add_payment_details()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $id = ($this->input->post('id')) ? (int) $this->input->post('id') : 0;
            $payment_via = $this->input->post('payment_via');
            $this->form_validation->set_rules($this->add_payment_rules($payment_via));
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
            } else {
                $insertData['payment_via'] = ($payment_via) ? $payment_via : '1';
                $insertData['model_id'] = $this->user_id;
                if ($payment_via == 1) {
                    $insertData['account_holder'] = ($this->input->post('account_holder')) ? $this->input->post('account_holder') : '';
                    $insertData['email_address'] = ($this->input->post('email_address')) ? $this->input->post('email_address') : '';
                    $insertData['bank_name'] = ($this->input->post('bank_name')) ? $this->input->post('bank_name') : '';
                    $insertData['account_type'] = ($this->input->post('account_type')) ? $this->input->post('account_type') : '';
                    $insertData['routing_number'] = ($this->input->post('routing_number')) ? $this->input->post('routing_number') : '';
                    $insertData['account_number'] = ($this->input->post('account_number')) ? $this->input->post('account_number') : '';
                    $insertData['pay_to'] = '';
                    $insertData['mailing_address'] = '';
                    $insertData['paxum_email_address'] = '';
                }
                if ($payment_via == 2) {
                    $insertData['account_holder'] = '';
                    $insertData['email_address'] = '';
                    $insertData['bank_name'] = '';
                    $insertData['account_type'] = '';
                    $insertData['routing_number'] = '';
                    $insertData['account_number'] = '';
                    $insertData['pay_to'] = ($this->input->post('pay_to')) ? $this->input->post('pay_to') : '';
                    $insertData['mailing_address'] = ($this->input->post('mailing_address')) ? $this->input->post('mailing_address') : '';
                    $insertData['paxum_email_address'] = '';
                }
                if ($payment_via == 3) {
                    $insertData['account_holder'] = '';
                    $insertData['email_address'] = '';
                    $insertData['bank_name'] = '';
                    $insertData['account_type'] = '';
                    $insertData['routing_number'] = '';
                    $insertData['account_number'] = '';
                    $insertData['pay_to'] = '';
                    $insertData['mailing_address'] = '';
                    $insertData['paxum_email_address'] = ($this->input->post('paxum_email_address')) ? $this->input->post('paxum_email_address') : '';
                }
                if ($id > 0) {
                    $insertData['updated_at'] = get_date();
                    $update = $this->common->update_data($insertData, 'tbl_model_payment_details', 'id', $id);
                    $response['status'] = 200;
                    $response['message'] = $this->lang->line('succ');
                } else {
                    $insertData['created_at'] = get_date();
                    $insert = $this->common->insert_data('tbl_model_payment_details', $insertData);
                    $extraDetails['model_payment_details_id'] = $insert;
                    $update = $this->common->update_data($extraDetails, 'tbl_model_extra', 'model_id', $this->user_id);
                    $response['status'] = 200;
                    $response['message'] = $this->lang->line('succ');
                }
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function add_payment_rules($payment_via = 1)
    {
        $rules = array(
            array(
                'field' => 'payment_via',
                'label' => 'Payment via',
                'rules' => 'trim|required',
            )
        );
        if ($payment_via == 1) {
            $rules[] = array(
                array(
                    'field' => 'account_holder',
                    'label' => 'Account Holder',
                    'rules' => 'trim|required',
                )
            );
            $rules[] = array(
                array(
                    'field' => 'email_address',
                    'label' => 'Email Address',
                    'rules' => 'trim|required',
                )
            );
            $rules[] = array(
                array(
                    'field' => 'bank_name',
                    'label' => 'Bank Name',
                    'rules' => 'trim|required',
                )
            );
            $rules[] = array(
                array(
                    'field' => 'account_type',
                    'label' => 'Account Type',
                    'rules' => 'trim|required',
                )
            );
            $rules[] = array(
                array(
                    'field' => 'routing_number',
                    'label' => 'Rounting Number',
                    'rules' => 'trim|required',
                )
            );
            $rules[] = array(
                array(
                    'field' => 'account_number',
                    'label' => 'Account Number',
                    'rules' => 'trim|required',
                )
            );
        }
        if ($payment_via == 2) {
            $rules[] = array(
                array(
                    'field' => 'pay_to',
                    'label' => 'Pay to',
                    'rules' => 'trim|required',
                )
            );
            $rules[] = array(
                array(
                    'field' => 'mailing_address',
                    'label' => 'Mailing Address',
                    'rules' => 'trim|required',
                )
            );
        }
        if ($payment_via == 3) {
            $rules[] = array(
                array(
                    'field' => 'paxum_email_address',
                    'label' => 'Email Address',
                    'rules' => 'trim|required',
                )
            );
        }
        return $rules;
    }


    public function upload_w9_form()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->add_w9form_rules());
            if (empty($_FILES['form_file']['name'])) {
                $this->form_validation->set_rules('form_file', 'PDF File', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $response['message'] = get_error($this->form_validation->error_array());
                }
            } else {
                $oldFile = $this->common->get_data_by_id('tbl_model_w9form', 'model_id', $this->user_id, 'file_name', '', '', '', '', 'row');
                if (!empty($oldFile)) {
                    if ($oldFile['file_name'] != '') {
                        $path = FCPATH . SITE_UPD . 'w9form/' . $oldFile['file_name'];
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                }
                $this->common->delete_data('tbl_model_w9form', array('model_id' => $this->user_id));
                $insertData['file_name'] = $this->upload_form_file('w9form');
                $insertData['model_id'] = $this->user_id;
                $insertData['created_at'] = get_date();
                $insert = $this->common->insert_data('tbl_model_w9form', $insertData);
                if ($insert > 0) {
                    $response['status'] = 200;
                    $response['message'] = $this->lang->line('succ');
                }
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }


    public function add_w9form_rules()
    {
        $rules = array(
            array(
                'field' => 'form_file',
                'label' => 'PDF file',
                'rules' => 'required',
            )
        );
        return $rules;
    }

    public function upload_form_file($folder = 'w9form')
    {
        $failed = '';
        $config['upload_path'] = FCPATH . SITE_UPD . $folder;
        $config['allowed_types'] = 'pdf';
        $new_name = md5(time() . rand());
        $config['file_name'] = $new_name;
        $this->load->library('upload', $config);
        if (!empty($_FILES['form_file'])) {
            $isUpload = $this->upload->do_upload('form_file');
            $uploadedFileName = $this->upload->data('file_name');
            return $uploadedFileName;
        } else {
            return $failed;
        }
    }

    public function geo_block()
    {
        // $ip = $_SERVER['REMOTE_ADDR'];
        // echo $ip;

        // $ip = '49.34.88.170'; // your ip address here
        // $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        // if ($query && $query['status'] == 'success') {
        //     echo 'Your City is ' . $query['city'];
        //     echo '<br />';
        //     echo 'Your State is ' . $query['region'];
        //     echo '<br />';
        //     echo 'Your Zipcode is ' . $query['zip'];
        //     echo '<br />';
        //     echo 'Your Coordinates are ' . $query['lat'] . ', ' . $query['lon'];
        // }
        // pre('fsdf');

        $this->data['headTitle'] = $this->lang->line("lbl_title_freindfeed");
        $this->data['model_geo_blocking_address'] = $this->common->get_data_by_id('tbl_geo_blocking', 'model_id', $this->user_id, '*');
        // pre($this->data['model_geo_blocking_address']);
        $this->data['module'] = "geo_block";
        $this->load->view('front/account/model/mainpage', $this->data);
    }
    function add_geo_blocking_data()
    {

        //  pre($this->user_id);
        if ($this->user_type == 'model' && $this->user_id > 0) {

            $post = $this->input->post();
            $country = isset($post['country']) ? $post['country'] : '';
            $state = isset($post['state']) ? $post['state'] : '';
            $city = isset($post['city']) ? $post['city'] : '';
            $modelId = $this->user_id;
            $count = $this->common->get_table_count('tbl_geo_blocking', array('model_id' => $modelId, 'country' => $country, 'state' => $state, 'city' => $city));
            if ($count == 0) {
                $insertData = array(
                    'country' => $country,
                    'state' => $state,
                    'city' => $city,
                    'model_id' => $modelId
                );

                // pre($insertData);
                $geoBlockId = $this->common->insert_data('tbl_geo_blocking', $insertData);
                $modelData = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
                //  pre($modelData['geo_block_id']);
                $geoBlockId = $modelData['geo_block_id'] . ',' . $geoBlockId;
                //pre($geoBlockId);
                $update_data = array('geo_block_id' => $geoBlockId);
                //  pre($update_data);
                //  pre($modelId);
                $date = $this->common->update_data($update_data, 'tbl_model_extra', 'model_id', $modelId);
                //  pre($this->db->last_query());
                $this->session->set_flashdata("success", "Location for blocking addedd successfully");
            } else {
                $this->session->set_flashdata("error", "This location already added");
            }
            redirect(base_url() . "/account/geo_block");
        } else {
            redirect(base_url());
        }
    }

=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    function remove_from_geo_block()
    {
        $post = $this->input->post();
        $this->user_model->remove_from_geo_block($post);
        echo json_encode($post);
    }

<<<<<<< HEAD
    public function update_model_settings()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->form_validation->set_rules($this->update_model_settings_form());
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
            } else {
                $postData = $this->input->post();
                $postData['display_location'] = (isset($postData['display_location']) && $postData['display_location'] == 'on') ? '1' : '0';
                $postData['allow_fancam_request'] = (isset($postData['allow_fancam_request']) && $postData['allow_fancam_request'] == 'on') ? '1' : '0';
                $postData['allow_stream_request'] = (isset($postData['allow_stream_request']) && $postData['allow_stream_request'] == 'on') ? '1' : '0';
                $postData['new_subscriber'] = (isset($postData['new_subscriber']) && $postData['new_subscriber'] == 'on') ? '1' : '0';
                $postData['new_content_purchase'] = (isset($postData['new_content_purchase']) && $postData['new_content_purchase'] == 'on') ? '1' : '0';
                $postData['new_private_message'] = (isset($postData['new_private_message']) && $postData['new_private_message'] == 'on') ? '1' : '0';
                $postData['receive_latest_news_updates'] = (isset($postData['receive_latest_news_updates']) && $postData['receive_latest_news_updates'] == 'on') ? '1' : '0';
                $postData['new_model_friend_request'] = (isset($postData['new_model_friend_request']) && $postData['new_model_friend_request'] == 'on') ? '1' : '0';
                $postData['available_in_premium_video_store'] = (isset($postData['available_in_premium_video_store']) && $postData['available_in_premium_video_store'] == 'on') ? '1' : '0';

                $postData['allow_my_friends'] = (isset($postData['allow_my_friends']) && $postData['allow_my_friends'] == 'on') ? '1' : '0';
                //pre($postData);
                $iUserId = $this->user_model->update_user($postData, $this->user_id);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    protected function update_model_settings_form()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line('lbl_name'),
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'mobile',
                'label' => 'Phone',
                'rules' => 'trim|required',
            )
        );
        return $rules;
    }


    public function get_started()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_getting_started");
        $this->data['get_started'] = $this->common->get_data_by_id('tbl_getting_started', 'id', 1, '', '', 'created_at', 'ASC', '', 'row');
        $this->data['module'] = "get_started";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function make_money()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_make_money");
        $this->data['make_money'] = $this->common->get_data_by_id('tbl_make_money', 'id', 1, '', '', 'created_at', 'ASC', '', 'row');
        $this->data['module'] = "make_money";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function download_getting_started($filename)
    {
        if (!empty($filename)) {
            //load download helper
            $this->load->helper('download');

            //file path
            $file = 'themes/uploads/getting_started/' . $filename;
            //download file from directory
            force_download($file, NULL);
        }
    }

    public function download_make_money($filename)
    {
        if (!empty($filename)) {
            //load download helper
            $this->load->helper('download');

            //file path
            $file = 'themes/uploads/make_money/' . $filename;
            //download file from directory
            force_download($file, NULL);
        }
    }

    public function profile()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_edit_profile");
        $this->data['user'] = $this->common->get_data_by_id('tbl_users', 'id', $this->user_id, '', '', '', '', '', 'row');
        $this->data['module'] = "profile";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function update_avatar()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $updateProfileImage = $this->common->upload_profile_image_for_model($_FILES, $this->user_id, 'models');
            if ($updateProfileImage) {
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function update_bio()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $updateData['bio'] = (isset($_POST)) ? $_POST['bio'] : '';
            $updateBio = $this->common->update_data($updateData, 'tbl_users', 'id', $this->user_id);
            if ($updateBio) {
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function update_header()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            if (!empty($this->input->post('header'))) {
                $updateHeader = $this->common->upload_header_image_for_model($this->input->post('header'), $this->user_id, 'models');
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function update_stream()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            if (!empty($this->input->post('stream'))) {
                $updateHeader = $this->common->upload_stream_image_for_model($this->input->post('stream'), $this->user_id, 'models');
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    function freinds_feed()
    {
        $this->data['headTitle'] = "Freinds Feed";

        $this->data['module'] = "freindfeed";
        $this->load->view('front/account/model/mainpage', $this->data);
    }
    public function load_freinds_feed()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['feed_models'] = $this->user_model->get_freinds_feed($this->user_id, $limit, $filters);
            //pre('fsdfsdaf');
            foreach ($response['feed_models'] as $key => $value) {
                if ($value['post_type'] == '0') {
<<<<<<< HEAD
                    $this->overlayWatermark($value['post_image']);
                } else {
=======
                    // $config['image_library']   = 'gd2';

                    // $config['source_image'] =  FCPATH . SITE_UPD . '/models' . '/' . $value['post_image'];
                    // $config['wm_text'] = 'RoyalFruit';
                    // $config['wm_type'] = 'text';

                    // $config['wm_font_size'] = '16';
                    // $config['wm_font_color'] = '#0000FF';

                    // $this->load->library('image_lib', $config);
                    // $this->image_lib->initialize($config);
                    // $this->image_lib->watermark();
                    // if (!$this->image_lib->watermark()) {
                    // }
                    $this->overlayWatermark($value['post_image']);
                } else {

                    // $inputvideo = FCPATH . SITE_UPD . '/models' . '/' . $value['post_image'];
                    // $logo = FCPATH . ADM_MEDIA  . 'logo.png';
                    // // pre($logo);
                    // //exec("ffmpeg -i " . $inputvideo . " " . $logo . ".flv");
                    // $mark = "ffmpeg -i " . $inputvideo . " -i " . $logo . " -filter_complex " . '"overlay=x=(main_w-overlay_w):y=(main_h-overlay_h)/(main_h-overlay_h)"' . " " .  FCPATH . SITE_UPD . 'models' . '/' . uniqid() . "topright.mp4";
                    // // $mark = "start notepad";
                    // pre(exec($mark));
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
                }
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function followers()
    {
        $this->data['headTitle'] = "Followers";
        $this->data['module'] = "followers";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function load_followers()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $response['users'] = $this->user_model->get_followers_for_model($this->user_id, $limit, $filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //notification
    public function model_notification()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_notification");
        $this->data['notifications'] = $this->user_model->get_notification_list($this->user_id);
        $this->data['module'] = "notification";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    //change notification action
    public function change_notification_action2()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $filters['notification_ids'] = check_variable_value($this->input->post('notification_ids'));
            $notification = explode(",", $filters['notification_ids']);
            //pre($notification);
            $filters['action'] = check_variable_value($this->input->post('action'));


            //1 for read notification
            //2 for delete notification
            //3 for mark all read
            //4 for all delete
            if ($filters['action'] == 1) {
                $res = 0;
                foreach ($notification as $key => $value) {
                    // pre($value);
                    if ($value != 0) {

                        $condition = array('id' => $value);
                        $updateData = array('read_flag' => 1);
                        $res = $this->common->new_update_data($updateData, 'tbl_notification', $condition);
                    }
                }
                //pre($res);
                //pre($this->db->last_query());
                if ($res == true) {

                    $response['message'] = $this->lang->line('notification_read_succ');
                    $response['type'] = "success";
                } else {
                    $response['message'] = $this->lang->line('notification_select_err');
                    $response['type'] = "error";
                }
            } else if ($filters['action'] == 2) {
                $res = 0;
                foreach ($notification as $key => $value) {
                    if ($value != 0) {
                        $condition = array('id' => $value);
                        $res = $this->common->delete_data('tbl_notification', $condition);
                    }
                }
                if ($res == true) {

                    $response['message'] = $this->lang->line('notification_dele_succ');
                    $response['type'] = "success";
                } else {
                    $response['message'] = $this->lang->line('notification_select_err');
                    $response['type'] = "error";
                }
            } else if ($filters['action'] == 3) {
                $res = 0;
                $condition = array('user_id' => $this->user_id);
                $updateData = array('read_flag' => 1);
                $this->common->new_update_data($updateData, 'tbl_notification', $condition);

                $response['message'] = $this->lang->line('notification_read_succ');
                $response['type'] = "success";
            } else if ($filters['action'] == 4) {
                $res = 0;
                $condition = array('user_id' => $this->user_id);
                $res = $this->common->delete_data('tbl_notification', $condition);

                $response['message'] = $this->lang->line('notification_dele_succ');
                $response['type'] = "success";
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function premium_videos_model()
    {
        if ($this->user_type == 'model') {
<<<<<<< HEAD
=======

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
            $this->data['headTitle'] = "Premium Videos";
            $this->data['module'] = "premium_videos";
            $this->load->view('front/account/model/mainpage', $this->data);
        } else {
            redirect(base_url());
        }
    }
    public function load_premium_videos_model()
    {
        if ($this->input->is_ajax_request()) {
            $post_type = 1;
            $category = 3;
            $this->data['model_post_images'] = $this->user_model->get_premium_video_by_model_id($this->user_id, $post_type, $category);
            // pre($this->data['model_post_images']);
            echo json_encode($this->data);
        } else {
            redirect(base_url());
        }
    }
<<<<<<< HEAD

=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    public function referals()
    {
        if ($this->user_type == 'model') {

            $this->data['headTitle'] = "Referals";
            $this->data['module'] = "referals";
            $res = $this->common->get_data_by_id('tbl_users', 'id', $this->user_id, '*', array());
            // pre($res);
            $this->data['modelData'] = $res[0];

            $this->load->view('front/account/model/mainpage', $this->data);
        } else {
            redirect(base_url());
        }
    }
    public function your_content()
    {
        if ($this->user_type == 'model') {
            $this->data['headTitle'] = "Your Content";
            $this->data['module'] = "your_content";
            $this->load->view('front/account/model/mainpage', $this->data);
        } else {
            redirect(base_url());
        }
    }
<<<<<<< HEAD

    public function documents()
    {
        $userId = !empty($this->user_id) ? $this->user_id : 0;
        $this->data['userData'] = $this->common->get_data_by_id('tbl_users', 'id', $userId, '', '', '', '', '', 'row');
        $this->data['agreements'] = $this->common->get_data_by_id('tbl_agreements', 'id', 1, '', '', '', '', '', 'row');
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "documents";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function upload_photo_id()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->add_w9form_rules());
            if (empty($_FILES['document_photo_id']['name'])) {
                $this->form_validation->set_rules('document_photo_id', 'Document', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $response['message'] = get_error($this->form_validation->error_array());
                }
            } else {
                $updatePhotoID = $this->common->upload_photo_id_for_model($_FILES, $this->user_id, 'models');
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function upload_id_verification_selfie()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->add_w9form_rules());
            if (empty($_FILES['document_id_verification_selfie']['name'])) {
                $this->form_validation->set_rules('document_id_verification_selfie', 'Document', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $response['message'] = get_error($this->form_validation->error_array());
                }
            } else {
                $updatePhotoID = $this->common->upload_id_verification_selfie_for_model($_FILES, $this->user_id, 'models');
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function blocked_fan_settings()
    {
        $model_id = !empty($this->user_id) ? $this->user_id : 0;
        $this->data['blocked_fans'] = $this->common->get_blocked_fans($model_id);
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->data['module'] = "blocked_fan_settings";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function unblock_fan()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $block_id = ($this->input->post('block_id') != '') ? (int) $this->input->post('block_id') : 0;
            if ($block_id > 0) {
                $where['id'] = $block_id;
                $unblockFan = $this->common->delete_data('tbl_blocked_fans', $where);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
                $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
                exit;
            }
            $this->output
                ->set_status_header(412)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
            exit;
        } else {
            redirect(base_url());
        }
    }



    public function document_verification_step_account()
    {
        if (!empty($_POST)) {
            $this->form_validation->set_rules($this->step_account_form_validation());
            if ($this->form_validation->run() == FALSE) {
                $i = 0;
                foreach ($this->form_validation->error_array() as $key => $value) {
                    if ($i != 0) {
                        continue;
                    }
                    $this->session->set_flashdata("error", $value);
                    $i++;
                }
                redirect(base_url('account/document_verification_step_account'));
            } else {
                $postData = $this->input->post();
                $postData['current_step'] = 2;
                $iUserId = $this->user_model->update_user($postData, $this->user_id);
                if ($iUserId > 0) {
                    $userData = $this->common->get_data_by_id('tbl_users', 'id', $iUserId, '*', array(), '', '', '', 'row');
                    $this->session->set_flashdata('success', 'Account Details Submitted Successfully.');
                    redirect(base_url('account/document_verification_step_verify')); //redirecting model dashboard

                } else {
                    $this->session->set_flashdata('error', "Something went worng");
                }
            }
        } else {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $this->data['userData'] = $this->common->get_data_by_id('tbl_users', 'id', $userId, '', '', '', '', '', 'row');
            $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
            $this->load->view('front/account/model/document_verification_step_account', $this->data);
        }
    }

    protected function step_account_form_validation()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line('lbl_name'),
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'country_name',
                'label' => 'Country',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'subscription_price',
                'label' => 'Subscription Price',
                'rules' => 'trim|required',
            ),
        );
        return $rules;
    }

    public function document_verification_step_verify()
    {
        if (!empty($_POST)) {
            $this->form_validation->set_rules($this->verify_account_form_validation());
            if ($this->form_validation->run() == FALSE) {
                $i = 0;
                foreach ($this->form_validation->error_array() as $key => $value) {
                    if ($i != 0) {
                        continue;
                    }
                    $this->session->set_flashdata("error", $value);
                    $i++;
                }
                redirect(base_url('account/document_verification_step_verify'));
            } else {
                $postData = $this->input->post();
                $postData['current_step'] = 3;
                $postData['no_expiration'] = (isset($postData['no_expiration']) && $postData['no_expiration'] == 'on') ? '1' : '0';
                $iUserId = $this->user_model->update_user($postData, $this->user_id);
                if ($iUserId > 0) {
                    $userData = $this->common->get_data_by_id('tbl_users', 'id', $iUserId, '*', array(), '', '', '', 'row');
                    $this->session->set_flashdata('success', 'Verify Details Submitted Successfully.');
                    redirect(base_url('account/document_verification_step_review'));
                } else {
                    $this->session->set_flashdata('error', "Something went worng");
                }
            }
        } else {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $this->data['userData'] = $this->common->get_data_by_id('tbl_users', 'id', $userId, '', '', '', '', '', 'row');
            $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
            $this->load->view('front/account/model/document_verification_step_verify', $this->data);
        }
    }

    protected function verify_account_form_validation()
    {
        $rules = array(
            array(
                'field' => 'government_issued_id_type',
                'label' => 'Government Issued ID Type',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'government_id_issued_by_state',
                'label' => 'Issued By (State/Province)',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'government_id_number',
                'label' => 'ID Number',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'legal_name',
                'label' => 'Full Legal Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'dob_month',
                'label' => 'Month',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'dob_day',
                'label' => 'Day',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'dob_year',
                'label' => 'Year',
                'rules' => 'trim|required',
            ),
        );
        return $rules;
    }

    public function document_verification_step_review()
    {
        if (!empty($_POST)) {
            $this->form_validation->set_rules($this->review_account_form_validation());
            if ($this->form_validation->run() == FALSE) {
                $i = 0;
                foreach ($this->form_validation->error_array() as $key => $value) {
                    if ($i != 0) {
                        continue;
                    }
                    $this->session->set_flashdata("error", $value);
                    $i++;
                }
                redirect(base_url('account/document_verification_step_review'));
            } else {
                $postData = $this->input->post();
                $postData['current_step'] = 4;
                $postData['agreement_model'] = (isset($postData['agreement_model']) && $postData['agreement_model'] == 'on') ? 1 : 0;
                $postData['agreement_2257'] = (isset($postData['agreement_2257']) && $postData['agreement_2257'] == 'on') ? 1 : 0;
                $iUserId = $this->user_model->update_user($postData, $this->user_id);
                if ($iUserId > 0) {
                    $userData = $this->common->get_data_by_id('tbl_users', 'id', $iUserId, '*', array(), '', '', '', 'row');
                    $this->session->set_flashdata('success', 'Details have been reviewed successfully.');
                    redirect(base_url('account/document_verification_step_upload'));
                } else {
                    $this->session->set_flashdata('error', "Something went worng");
                }
            }
        } else {
            $userId = !empty($this->user_id) ? $this->user_id : 0;
            $this->data['userData'] = $this->common->get_data_by_id('tbl_users', 'id', $userId, '', '', '', '', '', 'row');
            $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
            $this->load->view('front/account/model/document_verification_step_review', $this->data);
        }
    }

    protected function review_account_form_validation()
    {
        $rules = array(
            array(
                'field' => 'agreement_model',
                'label' => 'Model agreement',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'agreement_2257',
                'label' => 'Model 2257 agreement',
                'rules' => 'trim|required',
            )
        );
        return $rules;
    }

    public function document_verification_step_upload()
    {
        $userId = !empty($this->user_id) ? $this->user_id : 0;
        $this->data['userData'] = $this->common->get_data_by_id('tbl_users', 'id', $userId, '', '', '', '', '', 'row');
        $condition_array = array('model_id' => $userId);
        $this->data['totalPosts'] = $this->common->get_table_count('tbl_model_posts', $condition_array);
        $this->data['getPosts'] = $this->common->get_data_by_id('tbl_model_posts', 'model_id', $userId, '', array('category' => 1), 'id', 'DESC', '10', 'array');
        $this->data['headTitle'] = $this->lang->line("lbl_buy_tokens");
        $this->load->view('front/account/model/document_verification_step_upload', $this->data);
    }

    public function upload_teaser_image()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->add_w9form_rules());
            if (empty($_FILES['teaser_image']['name'])) {
                $this->form_validation->set_rules('teaser_image', 'Document', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $response['message'] = get_error($this->form_validation->error_array());
                }
            } else {
                $updatePhotoID = $this->common->upload_teaser_image($_FILES, $this->user_id, 'models');
                $response['user_details'] = $this->common->get_data_by_id('tbl_users', 'id', $this->user_id, '', '', '', '', '', 'row');
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function upload_teaser_video()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->add_w9form_rules());
            if (empty($_FILES['teaser_video']['name'])) {
                $this->form_validation->set_rules('teaser_video', 'Document', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $response['message'] = get_error($this->form_validation->error_array());
                }
            } else {
                $updatePhotoID = $this->common->upload_teaser_video($_FILES, $this->user_id, 'models');
                $response['user_details'] = $this->common->get_data_by_id('tbl_users', 'id', $this->user_id, '', '', '', '', '', 'row');
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    public function load_your_content()
    {
        if ($this->input->is_ajax_request()) {
            $post_type = 1;
            $category = 3;
            $this->data['feed_models'] = $this->user_model->get_all_post_by_model_id($this->user_id);
            echo json_encode($this->data);
        } else {
            redirect(base_url());
        }
    }
    public function load_models_followers()
    {
        if ($this->input->is_ajax_request()) {
            $filter = $this->input->post('filter');
            $this->data['modelsData'] = $this->user_model->get_followers_of_model($this->user_id, $filter);
            $this->data['customvideo'] = $this->user_model->get_customVideos_of_model($this->user_id, $filter);
            $this->data['privatevideo'] = $this->user_model->get_privateVideos_of_model($this->user_id, $filter);
            // pre($this->data['modelsData']);
            echo json_encode($this->data);
        } else {
            redirect(base_url());
        }
    }
<<<<<<< HEAD

    public function custom_video_settings()
    {
        $modelId = !empty($this->user_id) ? $this->user_id : 0;
        $this->data['model_extra_data'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '', '', 'id', 'DESC', '', 'row');
        $this->data['headTitle'] = 'Custom Video Settings';
        $this->data['module'] = "custom_video_settings";
        $this->load->view('front/account/model/mainpage', $this->data);
    }

    public function update_custom_video_settings()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $this->load->library('form_validation');
            if (isset($_POST, $_POST['is_allow_coustom_video_request'])) {
                $this->form_validation->set_rules($this->custom_video_settings_rules());
                if ($this->form_validation->run() == FALSE) {
                    $response['message'] = get_error($this->form_validation->error_array());
                } else {
                    $id = $this->input->post('id');
                    $is_allow_coustom_video_request = $this->input->post('is_allow_coustom_video_request');
                    $custom_video_price = $this->input->post('custom_video_price');
                    $rules = $this->input->post('rules');
                    $custom_video_delivery_time = $this->input->post('custom_video_delivery_time');
                    if ($id > 0) {
                        $updateData['is_allow_coustom_video_request'] = ($is_allow_coustom_video_request == 'on') ? 1 : 0;
                        $updateData['custom_video_price'] = $custom_video_price;
                        $updateData['rules'] = $rules;
                        $updateData['custom_video_delivery_time'] = $custom_video_delivery_time;
                        $updateData['updated_at'] = get_date();
                        $update = $this->common->update_data($updateData, 'tbl_model_extra', 'id', $id);
                        if ($update) {
                            $response['status'] = 200;
                            $response['message'] = $this->lang->line('succ');
                        }
                    } else {
                        $insertData['model_id'] = !empty($this->user_id) ? $this->user_id : 0;
                        $insertData['is_allow_coustom_video_request'] = ($is_allow_coustom_video_request == 'on') ? 1 : 0;
                        $insertData['custom_video_price'] = $custom_video_price;
                        $insertData['rules'] = $rules;
                        $insertData['custom_video_delivery_time'] = $custom_video_delivery_time;
                        $insertData['created_at'] = get_date();
                        $insert = $this->common->insert_data('tbl_model_extra', $insertData);
                        if ($insert > 0) {
                            $response['status'] = 200;
                            $response['message'] = $this->lang->line('succ');
                        }
                    }
                }
            } else {
                $id = $this->input->post('id');
                if ($id > 0) {
                    $updateData['is_allow_coustom_video_request'] = 0;
                    $updateData['custom_video_price'] = 0;
                    $updateData['rules'] = '';
                    $updateData['custom_video_delivery_time'] = '';
                    $updateData['updated_at'] = get_date();
                    $update = $this->common->update_data($updateData, 'tbl_model_extra', 'id', $id);
                    if ($update) {
                        $response['status'] = 200;
                        $response['message'] = $this->lang->line('succ');
                    }
                }
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function custom_video_settings_rules()
    {
        $rules = array(
            array(
                'field' => 'custom_video_price',
                'label' => 'Price',
                'rules' => 'required',
            ),
            array(
                'field' => 'custom_video_delivery_time',
                'label' => 'Delivery Time',
                'rules' => 'required',
            ),
            array(
                'field' => 'rules',
                'label' => 'Rules',
                'rules' => 'required',
            ),
        );
        return $rules;
    }

    public function block_friend()
    {
        $response = array();
        if ($this->input->is_ajax_request()) {
            $response['status'] = 412;
            $response['message'] = $this->lang->line('err_something_went_wrong');
            $request_id = $this->input->post('request_id');
            if ($request_id > 0) {
                $request_data = $this->common->get_data_by_id('tbl_friend_requests', 'id', $request_id, '', '', 'id', 'DESC', '', 'row');
                $insertData['model_id'] = $request_data['user_id'];
                $insertData['blocked_fan_id'] = $request_data['model_id'];
                $insertData['blocked_fan_type'] = 'model';
                $insertData['created_at'] = get_date();
                $insertBlockData = $this->common->insert_data('tbl_blocked_fans', $insertData);
                $condition_array = array('id' => $request_id);
                $cancelRequest = $this->common->delete_data('tbl_friend_requests', $condition_array);
                $response['status'] = 200;
                $response['message'] = $this->lang->line('succ');
            }
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    public function total_earninngs()
    {
        //pre('fasdfasf');
        $this->data['headTitle'] = "Total Earnings";
        $this->data['module'] = "dashboard";
        $this->data['today_earning'] = $this->user_model->get_model_earning($this->user_id, 'today');
        $this->data['yesterday_earning'] = $this->user_model->get_model_earning($this->user_id, 'yesterday');
        $this->data['last_sevenDays_earning'] = $this->user_model->get_model_earning($this->user_id, 'last_sevenDays_earning');
        $this->data['month_earning'] = $this->user_model->get_model_earning($this->user_id, 'month_earning');
        $this->data['last_payment'] = $this->user_model->get_model_earning($this->user_id, 'last_payment');
        $this->data['lifetime_payment'] = $this->user_model->get_model_earning($this->user_id, 'lifetime_payment');

        $this->data['fourperiod'] = get_periods_dates();
        $this->data['fourth_peroid_value'] = $this->user_model->get_earning_date_wise($this->user_id, $this->data['fourperiod']['fourthStartDate'], $this->data['fourperiod']['fourthEndDate']);
        $this->data['third_peroid_value'] = $this->user_model->get_earning_date_wise($this->user_id, $this->data['fourperiod']['thirdStartDate'], $this->data['fourperiod']['thirdEndDate']);
        $this->data['second_peroid_value'] = $this->user_model->get_earning_date_wise($this->user_id, $this->data['fourperiod']['secondStartDate'], $this->data['fourperiod']['secondEndDate']);
        $this->data['first_peroid_value'] = $this->user_model->get_earning_date_wise($this->user_id, $this->data['fourperiod']['firstStartDate'], $this->data['fourperiod']['firstEndDate']);

        // pre($fourperiod);
        //$this->data['lifetime_payment'] = $this->user_model->get_model_earning($this->user_id, 'lifetime_payment');
        // pre($this->data['lifetime_payment']);

        $this->load->view('front/account/model/mainpage', $this->data);
    }
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
}
