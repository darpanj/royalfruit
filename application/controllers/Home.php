<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Web_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->siteSettings();
        $this->data['fb_login_url'] = get_facebook_url();
        $this->data['google_login_url'] = get_google_url();
        $this->load->model('front/User_model', 'user_model');
    }

    function index()
    {


        $this->data['modelsData'] = array();
        $this->data['headTitle'] = $this->lang->line("lbl_title_home");
        $this->data['module'] = "home";
        $this->data['a'] = true;
        $this->load->view('front/mainpage', $this->data);
    }

    public function faq()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_faq");
        $this->data['module'] = "faq";
        $this->load->view('front/mainpage', $this->data);
    }

    public function become_model()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_become_model");
        $this->data['module'] = "become_model";
        $this->load->view('front/mainpage', $this->data);
    }

    public function model_details($slug = '')
    {
        $post_type = '';
        $category = 1;
        $this->data['model'] = $this->user_model->get_model_detail($slug);
        $this->data['model']['cover_image'] = checkImage(3, $this->data['model']['cover_image']);
        $this->data['model']['profile_image'] = checkImage(2, $this->data['model']['profile_image']);
        $this->data['model']['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $this->data['model']['id'], 'post_type' => 0));
        $this->data['model']['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $this->data['model']['id'], 'post_type' => 1));
        $this->data['model_post_images'] = $this->common->get_post_by_model_id($this->data['model']['id'], $post_type, $category);
        foreach ($this->data['model_post_images'] as $key => $value) {
            $fav_posts = $this->common->get_data_by_id('tbl_model_posts_fav', 'post_id', $value['id'], '*', array('user_id' => $this->user_id));
            if (count($fav_posts) > 0) {
                $this->data['model_post_images'][$key]['is_fav_post'] = 1;
            } else {
                $this->data['model_post_images'][$key]['is_fav_post'] = 0;
            }
        }
        //  pre($this->data['model_post_images']);
        //pre($this->data['model']);
        $modelId = $this->data['model']['id'];
        $userId = $this->user_id;
        $this->data['is_follow'] = $this->common->check_follow_or_not($modelId, $userId);
        $this->data['headTitle'] = $this->lang->line("lbl_title_model_details");
        $this->data['module'] = "model_details";
        $this->load->view('front/mainpage', $this->data);
    }

    public function explore()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_model_explore");
        $this->data['module'] = "explore";
        $this->load->view('front/mainpage', $this->data);
    }

    public function login()
    {
        $this->data['headTitle'] = $this->lang->line("login_header_title");
        $this->data['module'] = "login";
        $this->load->view('front/mainpage', $this->data);
    }

    public function login_check()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            redirect(base_url('home'));
        }
        $number = 0;
        $this->form_validation->set_rules($this->login_check_form());
        if ($this->form_validation->run() == FALSE) {
            $i = 0;
            foreach ($this->form_validation->error_array() as $key => $value) {
                if ($i != 0) {
                    continue;
                }
                $this->session->set_flashdata("error", $value);
                $i++;
                $number++;
            }
        } else {
            $pass = $this->input->post('password', true);
            $email = $this->input->post('email', true);
            $this->db->where('email', $email);
            $this->db->where('password', md5($pass));
            $this->db->where('name!=', 'admin');
            $query = $this->db->get('tbl_users');

            // pre($query->num_rows());
            if ($query->num_rows() == 1) {
                $user_data_h['userData'] = $query->row_array();
                //pre($user_data_h['userData']);
                if ($user_data_h['userData']['type'] == 'model') {
                    $this->session->set_flashdata("success", $this->lang->language['succ_login']);
                    $this->session->set_userdata('USERID', $user_data_h['userData']['id']);
                    $this->session->set_userdata('USERTYPE', $user_data_h['userData']['type']);
<<<<<<< HEAD
                    $this->session->set_userdata('USERSLUG', $user_data_h['userData']['user_slug']);
                    if ($user_data_h['userData']['is_approved'] == 1) {
                        redirect(base_url('account/application'));
                    } else {
                        if ($user_data_h['userData']['current_step'] == 1) {
                            redirect(base_url('account/document_verification_step_account'));
                        } else if ($user_data_h['userData']['current_step'] == 2) {
                            redirect(base_url('account/document_verification_step_verify'));
                        } else if ($user_data_h['userData']['current_step'] == 3) {
                            redirect(base_url('account/document_verification_step_review'));
                        } else if ($user_data_h['userData']['current_step'] == 4) {
                            redirect(base_url('account/document_verification_step_upload'));
                        } else {
                            redirect(base_url('account/application'));
                        }
                    }
=======
                    redirect(base_url('account/application'));
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
                } else {
                    if ($user_data_h['userData']['status'] == "active") {
                        $this->session->set_flashdata("success", $this->lang->language['succ_login']);
                        $this->session->set_userdata('USERID', $user_data_h['userData']['id']);
                        $this->session->set_userdata('USERTYPE', $user_data_h['userData']['type']);
                    } else {
                        $this->session->set_flashdata("error", $this->lang->language['err_inactive']);
                        $number++;
                    }
                    redirect(base_url('account/dashboard'));
                }
            } else {
                $this->session->set_flashdata("error", $this->lang->language['err_login']);
                $number++;
                redirect(base_url('home/login'));
            }
        }
    }

    protected function login_check_form()
    {
        $rules = array(
            array(
                'field' => 'email',
                'label' => $this->lang->language['lbl_email'],
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'password',
                'label' => $this->lang->language['lbl_password'],
                'rules' => 'trim|required|min_length[6]|max_length[16]',
            )
        );
        return $rules;
    }

    public function register()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_register");
        $this->data['module'] = "register";
        $this->load->view('front/mainpage', $this->data);
    }

    public function siteSettings()
    {
        $this->db->select(array('vConstant', 'vValue'));
        $qry = $this->db->get("mst_sitesettings");
        foreach ($qry->result_array() as $k => $v) {
            if (!defined($v["vConstant"])) {
                define($v["vConstant"], $v["vValue"]);
            }
        }
    }

    function reset($token = "")
    {
        $this->data['token'] = $token;
        $this->load->view('resetform', $this->data);
    }

    public function reset_password($token = "")
    {
        $this->form_validation->set_rules(
            'vPassword',
            'Password',
            'required',
            array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('cPassword', 'Password Confirmation', 'required|matches[vPassword]', array(
            'matches' => 'Password Does not match.',
        ));
        if ($this->form_validation->run()) {
            $check_token = $this->common->get_data_by_id('tbl_users', 'token', $token, '*', array('status' => 'active'), '', 'ASC', '', 'row');
            if (!empty($check_token)) {
                $pass = md5($this->input->post('vPassword'));
                $check = $this->common->update_data($data = array('token' => '', 'password' => $pass), 'tbl_users', 'id', $check_token['id']);
                if ($check) {
                    $this->session->set_flashdata('success', 'Password Reset successfully.');
                } else {
                    $this->session->set_flashdata('error', 'Invalid token.');
                }
            } else {
                $this->session->set_flashdata('error', 'Invalid token.');
            }
        } else {
            $this->session->set_flashdata('error', get_error($this->form_validation->error_array()));
        }
        redirect('home/reset/' . $token);
    }

    public function logout()
    {

        $this->session->unset_userdata('USERID');
        $this->session->unset_userdata('USERTYPE');
        $this->session->set_flashdata("success", $this->lang->language['succ_logout']);
        redirect(base_url('home'));
    }

    public function signup_user()
    {
        $this->form_validation->set_rules($this->signup_check_form());
        if ($this->form_validation->run() == FALSE) {
            $i = 0;
            foreach ($this->form_validation->error_array() as $key => $value) {
                if ($i != 0) {
                    continue;
                }
                $this->session->set_flashdata("error", $value);
                $i++;
            }
            redirect(base_url('home/register'));
        } else {
            $postData = $this->input->post();
            $iUserId = $this->user_model->register_user($postData);
            if ($iUserId > 0) {
                $userData = $this->common->get_data_by_id('tbl_users', 'id', $iUserId, '*', array(), '', '', '', 'row');
                $this->session->set_flashdata('success', $this->lang->language['succ_signup']);
                $this->session->set_userdata('USERID', $userData['id']);
                $this->session->set_userdata('USERTYPE', $userData['type']);
                if ($userData['type'] == 'user') {
                    redirect(base_url('account/dashboard'));  // redirecting user dashboard
                } elseif ($userData['type'] == 'model') {
                    $uniqueString = genUniqueStr('', 5, 'tbl_users', true);
<<<<<<< HEAD
                    $referralCode = genUniqueStr('REFERRAL-', 20, 'tbl_users', true);
                    $this->user_model->update_user(array('user_slug' => $uniqueString, 'refreal_code' => $referralCode), $iUserId);
                    $this->session->set_userdata('USERSLUG', $uniqueString);
                    //redirect(base_url('account/application')); //redirecting model dashboard
                    redirect(base_url('account/document_verification_step_account')); //redirecting model dashboard
=======
                    $this->user_model->update_user(array('user_slug' => $uniqueString), $iUserId);
                    redirect(base_url('account/application')); //redirecting model dashboard
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
                } else {
                    redirect(base_url());
                }
            }
        }
    }

    protected function signup_check_form()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|callback_unique_email',
                'errors' => array(
                    'unique_email' => $this->lang->line("email_in_use"),
                )
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[6]|max_length[16]',
            )
        );
        return $rules;
    }

    public function switch_lang()
    {
        if ($this->input->is_ajax_request()) {
            $lang = $this->input->post('language');
            $this->change_lang($lang);
        } else {
            redirect(base_url());
        }
    }

    //for loading models when ajax call from home page and premium viedeos page
    public function load_models()
    {
        $userId = !empty($this->user_id) ? $this->user_id : 0;

        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $offset = check_variable_value($this->input->post('offset'));
            $order_by = check_variable_value($this->input->post('order_by'));
            $usedIds = check_variable_value($this->input->post('used_ids'));
            $filters['search'] = check_variable_value($this->input->post('search'));
            $filters['live_status'] = check_variable_value($this->input->post('live_status'));
            $filters['featured'] = check_variable_value($this->input->post('featured'));
            $filters['newest'] = check_variable_value($this->input->post('newest'));
            $filters['recently_active'] = check_variable_value($this->input->post('recently_active'));
            $filters['most_followed'] = check_variable_value($this->input->post('most_followed'));
            //pre($usedIds);
            $usedIds = explode(",", $usedIds);
            $models = $this->user_model->get_models($limit, $offset, $order_by, $usedIds, $filters);
            foreach ($models as $key => $value) {
                $models[$key]['cover_image'] = checkImage(3, $value['cover_image']);
                $models[$key]['profile_image'] = checkImage(2, $value['profile_image']);
                $models[$key]['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 0));
                $models[$key]['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1));
                $models[$key]['total_premium_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1, 'category' => 3));
<<<<<<< HEAD
            }
            $response['data'] = $models;
            $response['total_rec'] = $this->common->get_table_count('tbl_users', array('type' => 'model'));
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    //load post for explore
    public function load_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $filters['limit'] = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $filters['order_by'] = check_variable_value($this->input->post('order_by'));
            $response['data'] = $this->user_model->get_allModels_all_teaserPosts($filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    // for follow button click gets model detail
    public function get_model_by_id()
    {
        $modelId = check_variable_value($this->input->post('userId'));
        $models = array();
        if ($modelId > 0) {
            // $result['models'] = $this->common->get_data_by_id('tbl_users', 'id', $modelId, '*', array(), '', 'ASC', '', 'row');
            $result['models'] = $this->common->get_models_with_price($modelId);
        }
        echo json_encode($result);
    }

    //live models list page
    public function live()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_model_explore");
        $this->data['module'] = "live_models";
        $this->data['live_models'] = $this->common->get_live_models();
        $this->data['available_tokens'] = $this->common->get_data_by_id('tbl_token_users', 'user_id', $this->user_id, 'tokens');
        //pre($this->data['live_models']);
        $this->load->view('front/mainpage', $this->data);
    }


    //premium videos from model detail page
    function premium_video($slug)
    {
        $post_type = 1;
        $category = 3;
        $this->data['model'] = $this->user_model->get_model_detail($slug);
        $this->data['model']['cover_image'] = checkImage(3, $this->data['model']['cover_image']);
        $this->data['model']['profile_image'] = checkImage(2, $this->data['model']['profile_image']);
        $this->data['model']['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $this->data['model']['id'], 'post_type' => 0));
        $this->data['model']['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $this->data['model']['id'], 'post_type' => 1));
        $this->data['model_post_images'] = $this->user_model->get_premium_video_by_model_id($this->data['model']['id'], $post_type, $category);
        foreach ($this->data['model_post_images'] as $key => $value) {
            $fav_posts = $this->common->get_data_by_id('tbl_model_posts_fav', 'post_id', $value['id'], '*', array('user_id' => $this->user_id));
            if (count($fav_posts) > 0) {
                $this->data['model_post_images'][$key]['is_fav_post'] = 1;
            } else {
                $this->data['model_post_images'][$key]['is_fav_post'] = 0;
            }
        }

        $modelId = $this->data['model']['id'];
        $userId = $this->user_id;
        $this->data['is_follow'] = $this->common->check_follow_or_not($modelId, $userId);
        $this->data['headTitle'] = $this->lang->line("lbl_title_premium_video");
        $this->data['module'] = "model_detail_premium_videos";
        $this->load->view('front/mainpage', $this->data);
    }

    public function get_post_by_id()
    {
        $postId = check_variable_value($this->input->post('postId'));
        $models = array();
        if ($postId > 0) {
            $result['models'] = $this->common->get_post_by_id($postId);
            //pre($result['models']);
            //$result['models']['price'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
        }
        echo json_encode($result);
    }

    function snap_model()
    {

        $this->data['headTitle'] = $this->lang->line("lbl_title_snap_models");
        $this->data['module'] = "snap_models";
        $this->load->view('front/mainpage', $this->data);
    }

    //get header premium video page
    function premium_video_models()
    {

        $this->data['headTitle'] = $this->lang->line("lbl_title_premium_video");
        $this->data['module'] = "premium_videos";
        $this->load->view('front/mainpage', $this->data);
    }

    function premium_video_models_list()
    {
        // pre($this->input->post());
        $userId = !empty($this->user_id) ? $this->user_id : 0;

        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $offset = check_variable_value($this->input->post('offset'));
            $order_by = check_variable_value($this->input->post('order_by'));
            $usedIds = check_variable_value($this->input->post('used_ids'));
            $usedIds2 = check_variable_value($this->input->post('used_ids2'));
            $filters['search'] = check_variable_value($this->input->post('search'));
            $filters['live_status'] = check_variable_value($this->input->post('live_status'));
            $filters['featured'] = check_variable_value($this->input->post('featured'));
            $filters['newest'] = check_variable_value($this->input->post('newest'));
            $filters['recently_active'] = check_variable_value($this->input->post('recently_active'));
            $filters['most_followed'] = check_variable_value($this->input->post('most_followed'));
            //pre($usedIds);
            // pre('fasfsafasf');
            //pre($filters);
            // pre($this->input->post('search'));
            $usedIds = explode(",", $usedIds);
            $usedIds2 = explode(",", $usedIds2);
            // pre($order_by);
            $models = $this->user_model->get_models_with_premium_videos($limit, $offset, $order_by, $usedIds, $filters);
            // pre($usedIds2);

            $premiumVideos = $this->user_model->get_premium_videos_page($limit, $offset, $order_by, $usedIds2, $filters);
            foreach ($models as $key => $value) {
                $models[$key]['cover_image'] = checkImage(3, $value['cover_image']);
                $models[$key]['profile_image'] = checkImage(2, $value['profile_image']);
                $models[$key]['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 0));
                $models[$key]['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1));
                $models[$key]['total_premium_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1, 'category' => 3));
            }
            foreach ($premiumVideos as $key => $value) {
                $premiumVideos[$key]['profile_image'] = checkImage(2, $value['profile_image']);
            }
            $response['data'] = $models;
            $response['premium_videos'] = $premiumVideos;
            $response['total_rec'] = $this->common->get_table_count('tbl_users', array('type' => 'model'));
=======
            }
            $response['data'] = $models;
            $response['total_rec'] = $this->common->get_table_count('tbl_users', array('type' => 'model'));

            //pre($response);
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

<<<<<<< HEAD
=======
    //load post for explore
    public function load_posts()
    {
        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $filters['limit'] = check_variable_value($this->input->post('limit'));
            $filters['used_post_ids'] = check_variable_value($this->input->post('post_ids'));
            $filters['order_by'] = check_variable_value($this->input->post('order_by'));
            $response['data'] = $this->user_model->get_allModels_all_teaserPosts($filters);
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

    // for follow button click gets model detail
    public function get_model_by_id()
    {
        $modelId = check_variable_value($this->input->post('userId'));
        $models = array();
        if ($modelId > 0) {
            // $result['models'] = $this->common->get_data_by_id('tbl_users', 'id', $modelId, '*', array(), '', 'ASC', '', 'row');
            $result['models'] = $this->common->get_models_with_price($modelId);
        }
        echo json_encode($result);
    }

    //live models list page
    public function live()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_title_model_explore");
        $this->data['module'] = "live_models";
        $this->data['live_models'] = $this->common->get_live_models();
        $this->data['available_tokens'] = $this->common->get_data_by_id('tbl_token_users', 'user_id', $this->user_id, 'tokens');
        //pre($this->data['live_models']);
        $this->load->view('front/mainpage', $this->data);
    }


    //premium videos from model detail page
    function premium_video($slug)
    {
        $post_type = 1;
        $category = 3;
        $this->data['model'] = $this->user_model->get_model_detail($slug);
        $this->data['model']['cover_image'] = checkImage(3, $this->data['model']['cover_image']);
        $this->data['model']['profile_image'] = checkImage(2, $this->data['model']['profile_image']);
        $this->data['model']['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $this->data['model']['id'], 'post_type' => 0));
        $this->data['model']['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $this->data['model']['id'], 'post_type' => 1));
        $this->data['model_post_images'] = $this->user_model->get_premium_video_by_model_id($this->data['model']['id'], $post_type, $category);
        foreach ($this->data['model_post_images'] as $key => $value) {
            $fav_posts = $this->common->get_data_by_id('tbl_model_posts_fav', 'post_id', $value['id'], '*', array('user_id' => $this->user_id));
            if (count($fav_posts) > 0) {
                $this->data['model_post_images'][$key]['is_fav_post'] = 1;
            } else {
                $this->data['model_post_images'][$key]['is_fav_post'] = 0;
            }
        }

        $modelId = $this->data['model']['id'];
        $userId = $this->user_id;
        $this->data['is_follow'] = $this->common->check_follow_or_not($modelId, $userId);
        $this->data['headTitle'] = $this->lang->line("lbl_title_premium_video");
        $this->data['module'] = "model_detail_premium_videos";
        $this->load->view('front/mainpage', $this->data);
    }

    public function get_post_by_id()
    {
        $postId = check_variable_value($this->input->post('postId'));
        $models = array();
        if ($postId > 0) {
            $result['models'] = $this->common->get_post_by_id($postId);
            //pre($result['models']);
            //$result['models']['price'] = $this->common->get_data_by_id('tbl_model_extra', 'model_id', $modelId, '*', array(), '', 'ASC', '', 'row');
        }
        echo json_encode($result);
    }

    function snap_model()
    {

        $this->data['headTitle'] = $this->lang->line("lbl_title_snap_models");
        $this->data['module'] = "snap_models";
        $this->load->view('front/mainpage', $this->data);
    }

    //get header premium video page
    function premium_video_models()
    {

        $this->data['headTitle'] = $this->lang->line("lbl_title_premium_video");
        $this->data['module'] = "premium_videos";
        $this->load->view('front/mainpage', $this->data);
    }

    function premium_video_models_list()
    {
        // pre($this->input->post());
        $userId = !empty($this->user_id) ? $this->user_id : 0;

        $response = array();
        $filters = array();
        if ($this->input->is_ajax_request()) {
            $limit = check_variable_value($this->input->post('limit'));
            $offset = check_variable_value($this->input->post('offset'));
            $order_by = check_variable_value($this->input->post('order_by'));
            $usedIds = check_variable_value($this->input->post('used_ids'));
            $usedIds2 = check_variable_value($this->input->post('used_ids2'));
            $filters['search'] = check_variable_value($this->input->post('search'));
            $filters['live_status'] = check_variable_value($this->input->post('live_status'));
            $filters['featured'] = check_variable_value($this->input->post('featured'));
            $filters['newest'] = check_variable_value($this->input->post('newest'));
            $filters['recently_active'] = check_variable_value($this->input->post('recently_active'));
            $filters['most_followed'] = check_variable_value($this->input->post('most_followed'));
            //pre($usedIds);
            // pre('fasfsafasf');
            //pre($filters);
            // pre($this->input->post('search'));
            $usedIds = explode(",", $usedIds);
            $usedIds2 = explode(",", $usedIds2);
            // pre($order_by);
            $models = $this->user_model->get_models_with_premium_videos($limit, $offset, $order_by, $usedIds, $filters);
            // pre($usedIds2);

            $premiumVideos = $this->user_model->get_premium_videos_page($limit, $offset, $order_by, $usedIds2, $filters);
            foreach ($models as $key => $value) {
                $models[$key]['cover_image'] = checkImage(3, $value['cover_image']);
                $models[$key]['profile_image'] = checkImage(2, $value['profile_image']);
                $models[$key]['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 0));
                $models[$key]['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1));
                $models[$key]['total_premium_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $value['id'], 'post_type' => 1, 'category' => 3));
            }
            $response['data'] = $models;
            $response['premium_videos'] = $premiumVideos;
            $response['total_rec'] = $this->common->get_table_count('tbl_users', array('type' => 'model'));
            echo json_encode($response);
        } else {
            redirect(base_url());
        }
    }

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
    public function submit_request_for_custom_video()
    {
        $data = array('status' => 412, 'message' => $this->lang->line('err_something_went_wrong'));
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules($this->request_form_rules());
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
            } else {
                $userId = !empty($this->user_id) ? $this->user_id : 0;
                $price = ($this->input->post('price')) ? $this->input->post('price') : 0;
                $instruction = ($this->input->post('instruction')) ? $this->input->post('instruction') : '';
                $model_id = ($this->input->post('model_id')) ? $this->input->post('model_id') : 0;
                $type = '4';
                $purchase = $this->common->purchase($type, $userId, $price);
                if ($purchase['payment_flag'] == 1) {
                    $data['status'] = 200;
                    $data_insert = array(
                        'price' => $price,
                        'instruction' => $instruction,
                        'model_id' => $model_id,
                        'request_sender_id' => $userId,
                        'payment_id' => $purchase['payment_id'],
                        'created_at' => get_date(),
                    );
                    $insertedId = $this->common->insert_data('tbl_custom_video_requests', $data_insert);
                    $data['message'] = $this->lang->line('succ');
                } else {
                    $data['message'] = "Payment Failed, Please Try again";
                }
            }
            echo json_encode($data);
        } else {
            base_url(base_url());
        }
    }

    public function request_form_rules()
    {
        $rules = array(
            array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'instruction',
                'label' => 'Instruction',
                'rules' => 'trim|required',
            ),
        );
        return $rules;
    }
<<<<<<< HEAD

    public function usc()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_usc");
        $this->data['module'] = "usc";
        /*$this->data['extra_class'] = "site-listing";*/
        $this->data['usc_content'] = $this->common->get_data_by_id('mst_content', 'sys_flag', 'usc', '*', array(), '', '', '', 'row');
        $this->load->view('front/mainpage', $this->data);
    }

    public function copyright()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_copyright");
        $this->data['module'] = "copyright";
        /*$this->data['extra_class'] = "site-listing";*/
        $this->data['copyright_content'] = $this->common->get_data_by_id('mst_content', 'sys_flag', 'copyright', '*', array(), '', '', '', 'row');
        $this->load->view('front/mainpage', $this->data);
    }

    public function privacypolicy()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_privacypolicy");
        $this->data['module'] = "privacypolicy";
        /*$this->data['extra_class'] = "site-listing";*/
        $this->data['privacypolicy_content'] = $this->common->get_data_by_id('mst_content', 'sys_flag', 'privacypolicy', '*', array(), '', '', '', 'row');
        $this->load->view('front/mainpage', $this->data);
    }

    public function california_privacy_policy()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_california_privacypolicy");
        $this->data['module'] = "california_privacy_policy";
        /*$this->data['extra_class'] = "site-listing";*/
        $this->data['cpp_content'] = $this->common->get_data_by_id('mst_content', 'sys_flag', 'californiaprivacypolicy', '*', array(), '', '', '', 'row');
        $this->load->view('front/mainpage', $this->data);
    }

    public function terms_and_conditions()
    {
        $this->data['headTitle'] = $this->lang->line("lbl_terms_and_conditions");
        $this->data['module'] = "terms_and_conditions";
        /*$this->data['extra_class'] = "site-listing";*/
        $this->data['tc_content'] = $this->common->get_data_by_id('mst_content', 'sys_flag', 'termsandconditions', '*', array(), '', '', '', 'row');
        $this->load->view('front/mainpage', $this->data);
    }
    public function subscribe_now()
    {
        // pre($this->input->post());
        $data = array('status' => 412, 'message' => $this->lang->line('err_something_went_wrong'));
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules($this->subscribe_now_rules());
            if ($this->form_validation->run() == false) {
                $data['message'] = get_error($this->form_validation->error_array());
                $data['type'] = "error";
            } else {
                $modelId = $this->input->post('modelId');
                $email = $this->input->post('email');
                $check = $this->common->get_table_count('tbl_model_subscribers', array('model_id' => $modelId, 'email' => $email));
                // pre($check);
                if ($check > 0) {
                    $data['message'] = "This email is already subscribed this model";
                    $data['type'] = "error";
                } else {
                    $insert_data = array(
                        'email' => $email,
                        'model_id' => $modelId,
                        'created' => get_date()
                    );
                    $insert_id = $this->common->insert_data('tbl_model_subscribers', $insert_data);
                    $data['message'] = "Subscribed Successfully";
                    $data['type'] = "success";
                }
            }
            echo json_encode($data);
        } else {
            base_url(base_url());
        }
    }

    public function subscribe_now_rules()
    {
        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
            ),
        );
        return $rules;
    }
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
}
