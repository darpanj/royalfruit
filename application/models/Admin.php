<?php

defined('BASEPATH') or exit('No direct script access allowed');

class admin extends MY_Model
{

    private $userID = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function isAdminLogin()
    {
        if ($this->session->userdata('ADMINID') > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function siteCount()
    {
        //pre($this->data);
        $content = array();
        $this->db->where('type', 'user')->from('tbl_users');
        $content['users'] = $this->db->count_all_results();

        $this->db->where(array('type' => 'model', 'status' => 'active'))->from('tbl_users');
        $content['active_models'] = $this->db->count_all_results();


        return $content;
    }
    public function siteCount_web($userId)
    {
        //pre($this->data);
        $content = array();
        $this->db->where('user_id', $userId)->from('tbl_model_posts_fav');
        $content['total_favorites'] = $this->db->count_all_results();

        $this->db->where(array('user_id' => $userId, 'read_flag' => 0))->from('tbl_notification');
        $content['total_notification'] = $this->db->count_all_results();

        $this->db->where(array('user_id' => $userId))->from('tbl_videos');
        $content['total_premium_videos'] = $this->db->count_all_results();

        $this->db->where(array('model_id' => $userId, 'category' => 3))->from('tbl_model_posts');
        $content['total_premium_videos_model'] = $this->db->count_all_results();

<<<<<<< HEAD
=======

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        // pre($content['total_favorites']);
        return $content;
    }

    public function siteSettings()
    {
        $this->db->select(array('vConstant', 'vValue'));
        $qry = $this->db->get("mst_sitesettings");
        foreach ($qry->result_array() as $k => $v) {
            if (!defined($v["vConstant"])) {
                define($v["vConstant"], $v["vValue"]);
            }
        }
    }

    function forgotPassword($PostData = array())
    {
        $content = array();
        $content['status'] = 404;
        $query = $this->db->query("SELECT id,name,status FROM tbl_users WHERE email = ?", array($PostData['vEmail']));
        if ($query->num_rows() === 1) {
            $result = $query->row_array();
            if ($result['status'] == 'active') {
                $to = $PostData['vEmail'];
                $token = md5($result['id'] . time());
                $this->db->query("UPDATE tbl_users SET token = '" . $token . "' WHERE id = ?", array($result['id']));
                if ($this->db->affected_rows() > 0) {
                    $link = base_url("home/reset/" . $token);
                    $email_data = generateEmailTemplate('forgot_password', array("{{GREETINGS}}", "{{RESET_PASS_LINK}}"), array($result['name'], $link));
                    $check = sendEmailAddress($to, $email_data['subject'], $email_data['message'], $email_data['fromemail'], $email_data['fromname']);
                    if ($check) {
                        $content['status'] = 200;
                        $content['message'] = $this->data['language']['succ_forgot_link_sent'];
                    } else {
                        $content['message'] = $this->data['language']['err_email_not_sent'];
                    }
                }
            } else {
                $content['message'] = $this->data['language']['err_inactive'];
            }
        } else {
            $content['message'] = $this->data['language']['err_email_not_registered'];
        }
        return $content;
    }

    function checkResetToken($token = "")
    {
        $content = array();
        $content['status'] = 404;
        $query = $this->db->get_where('tbl_users', array('token' => $token));
        if ($query->num_rows() > 0) {
            $content['status'] = 200;
        }
        return $content;
    }

    function resetPass($post = array())
    {
        $content = array();
        $content['status'] = 404;

        $query = $this->db->query("SELECT id,status FROM tbl_users WHERE token = ? ", array($post['vToken']));

        if ($query->num_rows() === 1) {
            $result = $query->row_array();
            if ($result['status'] == 'active') {
                $this->db->query("UPDATE tbl_users SET password = '" . md5($post['npassword']) . "',token = '" . md5(rand()) . "' WHERE id = ?", array($result['id']));
                $content['status'] = 200;
                $content['message'] = $this->data['language']['succ_pass_changed'];
            } else {
                $content['message'] = $this->data['language']['err_inactive'];
            }
        } else {
            $content['message'] = $this->data['language']['err_invalid_token'];
        }

        return $content;
    }

    public function submit_admin_profile($post = array())
    {
        //pre($_FILES);
        $content = array();
        $content['status'] = 404;
        $content['message'] = $this->data['language']['err_something_went_wrong'];

        $iUserId = isset($post['id']) ? (int) $post['id'] : 0;
        $dataArray = array(
            'name' => $post['name'],
            'username' => $post['username'],
            'email' => $post['email'],
            'mobile' => $post['mobile'],
        );
        if (!empty($post['password'])) {
            $dataArray['password'] = md5($post['password']);
        }
        if (!empty($post['image_path'])) {
            $dataArray['profile_image'] = $post['image_path'];
        }
        if ($this->user_type != 'admin') {
            $dataArray['location'] = $post['location'];
            $dataArray['latitude'] = $post['latitude'];
            $dataArray['longitude'] = $post['longitude'];
            //$dataArray['category'] = implode(',', $post['category']);
        }

        if ($iUserId > 0) {
            $this->db->update('tbl_users', $dataArray, array('id' => $iUserId));
            $content['status'] = 200;
            $content['message'] = $this->data['language']['succ_rec_updated'];
        }
        if (isset($_FILES['image_input']['name']) && $_FILES['image_input']['name'] != "") {
            $this->load->library('upload');
            $this->load->library('image_lib');
            if (isset($_FILES['image_input']['name']) && $_FILES['image_input']['name'] != '') {
                $file_config = array();
                $path = FCPATH . SITE_UPD . 'users/' . $iUserId . '/';
                if (!file_exists($path)) {
                    @mkdir($path);
                }
                $file_config['upload_path'] = $path;
                $file_config['allowed_types'] = 'jpg|png|jpeg';
                $file_config['overwrite'] = TRUE;
                $file_config['max_size'] = '204800';
                $file_config['file_name'] = md5(time() . rand());

                $this->upload->initialize($file_config);
                if ($this->upload->do_upload('image_input')) {
                    $content['status'] = 200;
                    $upload_data = $this->upload->data();

                    if ($this->data['user_detail']['type'] == 'vendor') {
                        $edit_data['logo'] = $iUserId . '/' . $upload_data['file_name'];
                        $query = $this->db->query("SELECT logo FROM tbl_users WHERE id = ? LIMIT 1", array($iUserId));
                        if ($query->num_rows() === 1) {
                            $result = $query->row_array();
                            $file = FCPATH . SITE_UPD . 'users/' . $result['logo'];
                            unlink_file($file);
                        }
                    } else {
                        $edit_data['profile_image'] = $iUserId . '/' . $upload_data['file_name'];
                        $query = $this->db->query("SELECT profile_image FROM tbl_users WHERE id = ? LIMIT 1", array($iUserId));
                        if ($query->num_rows() === 1) {
                            $result = $query->row_array();
                            $file = FCPATH . SITE_UPD . 'users/' . $result['profile_image'];
                            unlink_file($file);
                        }
                    }

                    $this->db->update('tbl_users', $edit_data, array('id' => $iUserId));
                }
            }
        }
        return $content;
    }

    public function getCommonModule()
    {
        $this->db->select('mod_modulegroupcode, mod_modulegroupname');
        $this->db->from('mst_module');
        $this->db->group_by('mod_modulegroupcode');
        $this->db->order_by('mod_modulegrouporder', 'ASC');
        $this->db->order_by('mod_moduleorder', 'ASC');
        return $this->db->get()->result_array();
    }

    public function getAllModule()
    {
        $this->db->select('mod_modulegroupcode, mod_modulegroupname, mod_modulepagename,  mod_modulecode, mod_modulename,vImage,mod_moduleorder,mod_modulegroupname_lang,mod_modulename_lang');
        $this->db->from('mst_module');
        $this->db->order_by('mod_modulegrouporder', 'ASC');
        $this->db->order_by('mod_moduleorder', 'ASC');
        return $this->db->get()->result_array();
    }

    public function getUserRights($role_code = 'admin')
    {
        $this->db->select('rr_modulecode, rr_create,  rr_edit, rr_delete, rr_view,rr_status,extra');
        $this->db->from('mst_role_rights');
        $this->db->where('rr_rolecode', $role_code);
        $this->db->order_by('rr_modulecode', 'ASC');
        return $this->db->get()->result_array();
    }

    function register_user($array)
    {
        $userData = array(
            'name' => $array['fname'] . ' ' . $array['lname'],
            'first_name' => $array['fname'],
            'last_name' => $array['lname'],
            'email' => $array['email'],
            'mobile' => $array['mobile_no'],
            'password' => md5($array['password']),
            'status' => 'active',
            'created' => get_date(),
            'updated' => get_date(),
        );
        $this->db->insert('tbl_users', $userData);
        return $this->db->insert_id();
    }

    function get_booking_data($filters = array(), $vendorId = 0, $userId = 0, $carId = 0)
    {
        $result = array();
        extract($filters);
        $whrArr = array(
            'u.status' => 'active'
        );
        if ($vendorId > 0) {
            $whrArr['b.vendor_id'] = $vendorId;
        } else if ($userId > 0) {
            $whrArr['b.user_id'] = $userId;
        } else if ($carId > 0) {
            $whrArr['b.car_id'] = $carId;
        }

        $likeArr = array();
        if (!empty($search)) {
            $likeArr['u.name'] = $search;
        }
        $this->db->select('b.id,b.car_id,b.status as booking_status,b.amount,b.pickup_date,b.drop_date,b.deposit,b.created,u.country_code,u.mobile,u.email,u.location,u.name as user_name,u.logo')
            ->from('tbl_booking b')->join('tbl_users u', 'b.user_id=u.id')->where($whrArr);

        if (!empty($likeArr)) {
            $this->db->group_start()
                ->like('b.amount', $search)
                ->or_like('u.name', $search)
                ->or_like('b.status', $search)
                ->group_end();
        }
        $selUsers = $this->db->limit($limit, $offset)
            ->order_by($sort, $order)
            ->get()->result_array();


        $this->db->select('COUNT(b.id) AS total')
            ->from('tbl_booking b')->join('tbl_users u', 'b.user_id=u.id')->where($whrArr);
        if (!empty($likeArr)) {
            $this->db->group_start()
                ->like('u.name', $search)
                ->or_like('b.amount', $search)
                ->or_like('b.status', $search)
                ->group_end();
        }
        $totalRec = $this->db->get()->row_array();

        $result['data'] = array();
        foreach ($selUsers as $i => $user) {
            $result['data'][] = array(
                'id' => ($offset + $i + 1),
                'user_name' => $user['user_name'],
                'amount' => $user['amount'],
                'pickup_date' => $user['pickup_date'],
                'country_code' => $user['country_code'],
                'mobile' => $user['mobile'],
                'email' => $user['email'],
                'drop_date' => $user['drop_date'],
                'deposit' => $user['deposit'],
                'created' => $user['created']
            );
        }
        $result["recordsTotal"] = (int) $totalRec['total'];
        $result["recordsFiltered"] = (int) $totalRec['total'];

        //pre($result);
        return $result;
    }
}
