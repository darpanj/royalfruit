<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends MY_WsModel
{

    protected $_table_name = 'tbl_users';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = "id DESC";

    function __construct()
    {
        parent::__construct();
    }

    function get_single_user($array)
    {
        $this->_table_name = 'tbl_users';
        $fields = 'id,name,bio,country_code,mobile,profile_image,notification,twitter_id,facebook_id,google_id,status,language,email';
        $query = parent::get_single($array, $fields);
        return $query;
    }

    function generate_auth($iUserId)
    {
        $data = array();
        $data['key'] = md5($iUserId . time() . rand());
        $data['auth'] = parent::hash($data['key']);
        return $data;
    }

    function insert_device($array)
    {
        $this->_table_name = 'tbl_user_devices';
        $array['dCreatedDate'] = get_date();
        return parent::insert($array);
    }

    function insert_user($array)
    {
        $array['status'] = 'active';
        $array['created'] = get_date();
        $array['updated'] = get_date();
        $this->_table_name = 'tbl_users';
        return parent::insert($array);
    }

    function update_user($data, $id = NULL)
    {
        $data['updated'] = get_date();
        $this->_table_name = 'tbl_users';
        parent::update($data, $id);
        return $id;
    }

    function resetPassword($postArray, $token)
    {
        $this->_table_name = 'tbl_users';
        return $this->db->update($this->_table_name, $postArray, array("token" => $token));
    }

    function check_login($vAuthToken = "")
    {
        $this->_table_name = 'tbl_user_devices';
        $vAuthToken = parent::hash($vAuthToken);
        $query = parent::get_single(array('vAuthToken' => $vAuthToken, 'eStatus' => 'y'));
        return $query;
    }

    function logout($vAuthToken)
    {
        $vAuthToken = parent::hash($vAuthToken);
        $this->_table_name = 'tbl_user_devices';
        $this->db->delete('tbl_user_devices', array('vAuthToken' => $vAuthToken));
    }

    function register_user($array)
    {
        $userData = array(
            'name' => (isset($array['name'])) ? ucfirst($array['name']) : "",
            'password' => (isset($array['password'])) ? md5($array['password']) : "",
            'facebook_id' => (isset($array['facebook_id'])) ? $array['facebook_id'] : "",
            'google_id' => (isset($array['google_id'])) ? $array['google_id'] : "",
            'twitter_id' => (isset($array['twitter_id'])) ? $array['twitter_id'] : "",
            'mobile' => (isset($array['mobile'])) ? $array['mobile'] : "",
            'email' => (isset($array['email'])) ? $array['email'] : "",
            'type' => (!empty($array['type'])) ? $array['type'] : "user",
            'country_code' => (isset($array['country_code'])) ? '+' . $array['country_code'] : "",
            'created' => get_date(),
            'updated' => get_date(),
        );
        $userData['status'] = ($userData['type'] == 'model') ? 'inactive' : 'active';
        $this->db->insert('tbl_users', $userData);
        $iUserId = $this->db->insert_id();
        return $iUserId;
    }

    public function social_login_check($country_code, $mobile, $email)
    {
        $query = $this->db->group_start()
            ->where('country_code', $country_code)->where('mobile', $mobile)
            ->group_end();
        if (!empty($email)) {
            $this->db->or_group_start()->where('email', $email)
                ->group_end();
        }
        $data = $this->db->get('tbl_users')->row();
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

    public function create_update_stripe_user($user_id)
    {
        $data = array('status' => 412, 'message' => $this->lang->language['err_something_went_wrong'], 'id' => 0);
        $strip_token = $this->input->post('stripe_card_token');
        $user_data = $this->common->get_data_by_id('tbl_users', 'id', $user_id, 'id,email,customer_id', array(), '', 'ASC', '', 'row');
        if (!empty($user_data['customer_id'])) {
            $data = $this->stripe->update_user($user_data['customer_id'], $strip_token);
        } else {
            $data = $this->stripe->create_user($user_data['email'], $strip_token);
            if ($data['status'] == 200) {
                $this->common->update_data(array('customer_id' => $data['id']), 'tbl_users', 'id', $user_id);
            }
        }
        return $data;
    }

    public function get_models($limit, $offset, $order, $usedIds = array(), $filters = array())
    {
        $followed_models_ids = array(0);
        $userId = ($this->user_id > 0) ? $this->user_id : 0;
        $whrArr_followed_models = array('mf.user_id' => $userId);
        $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_model_followers mf')->where($whrArr_followed_models)->get()->result_array();
        foreach ($tmp_followed_models_ids as $key => $value) {
            $followed_models_ids[] = $value['model_id'];
        }
        extract($filters);
        $whrArr = array('m.type' => 'model', 'm.status' => 'active');
        if (!empty($search)) {
            $whrArr['m.name'] = $search;
        }
        if (!empty($featured)) {
            $whrArr['m.is_featured'] = 1;
        }
        if (!empty($live_status)) {
            $whrArr['m.live_status'] = 1;
        }

        if (empty($most_followed)) {
            //  $this->db->select('m.*')->from('tbl_users m')->where($whrArr)->where_not_in('m.id', $usedIds);
            $this->db->select('m.*')->from('tbl_users m')
                ->join('tbl_model_followers mfm', 'mfm.model_id=m.id', 'left')
                ->where($whrArr)->where_not_in('m.id', $usedIds);
        }


        if (!empty($newest)) {  //get new model date wise created
            $this->db->group_by('m.id');
            $this->db->order_by('m.created', 'desc');
        } else if (!empty($recently_active)) {  //get  models recently logged in
            $this->db->group_by('m.id');
            $this->db->order_by('m.updated', 'desc');
        } else if (!empty($most_followed)) {  //get most followed models
            $this->db->select('m.*,COUNT(mf.id) as followers')->from('tbl_users m')->where($whrArr)->where_not_in('m.id', $usedIds);
            $this->db->join('tbl_model_followers mf', 'mf.model_id=m.id', 'left');
            $this->db->group_by('m.id');
            $this->db->order_by('followers', 'desc');
        }


        if ($order != 'RANDOM') {
            $this->db->limit($limit, $offset);
        } else {
            $this->db->limit($limit);
        }
        //   $this->db->group_by('m.id');
        $result = $this->db->where_not_in('m.id', $followed_models_ids)->get()->result_array();
        //  $result = $this->db->where_not_in('m.id', $followed_models_ids)->get()->result_array();
        //  pre($result);
        // pre($this->user_city)
        foreach ($result as $key => $value) {
            $blockedCities = array();
            $model_id = $value['id'];
            $geoBlockedResult = $this->common->get_data_by_id('tbl_geo_blocking', 'model_id', $model_id, '*', array());
            if (count($geoBlockedResult) > 0) {
                foreach ($geoBlockedResult as $key1 => $value1) {
                    $blockedCities[] = $value1['city'];
                }
                // pre($model_id);
            }
            if (in_array($this->user_city, $blockedCities)) {
                unset($result[$key]);
            }
        }
        // pre($result);
        return $result;
    }

    public function get_models_with_premium_videos($limit, $offset, $order, $usedIds = array(), $filters = array())
    {
        $followed_models_ids = array(0);
        $userId = ($this->user_id > 0) ? $this->user_id : 0;
        $whrArr_followed_models = array('mf.user_id' => $userId);
        $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_model_followers mf')->where($whrArr_followed_models)->get()->result_array();
        foreach ($tmp_followed_models_ids as $key => $value) {
            // $followed_models_ids[] = $value['model_id'];
        }
        extract($filters);
        $whrArr = array('m.type' => 'model', 'm.status' => 'active');
        $likeArr = array();
        // pre($search);
        if (!empty($search)) {
            $likeArr['m.name'] = $search;
        }
        if (!empty($featured)) {
            $whrArr['m.is_featured'] = 1;
        }
        if (!empty($live_status)) {
            $whrArr['m.live_status'] = 1;
        }
        //pre($most_followed);
        if (empty($most_followed)) {
            //  $this->db->select('m.*')->from('tbl_users m')->where($whrArr)->where_not_in('m.id', $usedIds);
            $this->db->select('m.*')->from('tbl_users m')
                ->join('tbl_model_followers mfm', 'mfm.model_id=m.id', 'left')
                ->where($whrArr)->where_not_in('m.id', $usedIds);
        } else if (!empty($most_followed)) {  //get most followed models
            $this->db->select('m.*,COUNT(mf.id) as followers')->from('tbl_users m')->where($whrArr)->where_not_in('m.id', $usedIds);
            $this->db->join('tbl_model_followers mf', 'mf.model_id=m.id', 'left');
            $this->db->order_by('followers', 'desc');
        }
        $this->db->group_by('m.id');

        //pre($newest);
        if ($newest) {  //get new model date wise created
            $this->db->order_by('m.created', 'desc');
        } else if (!$newest) {
            $this->db->order_by('m.created', 'asc');
        } else if (!empty($recently_active)) {  //get  models recently logged in
            $this->db->order_by('m.updated', 'desc');
        }

        //  $this->db->order_by('m.id', $order);
        // if ($order != 'RANDOM') {
        //     $this->db->limit($limit, $offset);
        // } else {
        //     $this->db->limit($limit);
        // }

        if (!empty($likeArr)) {
            $this->db->like($likeArr);
        }
        $this->db->distinct();
        $result = $this->db->limit($limit, $offset)->get()->result_array();
        // pre($this->db->last_query());
        //pre($result);
        return $result;
    }


    public function get_feed_models($userId = 0, $limit = 0, $filter = array())
    {
        $followed_models_ids = array(0);
        extract($filter);
        $whrArr = array('mf.user_id' => $userId);
        $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_model_followers mf')->where($whrArr)->get()->result_array();
        foreach ($tmp_followed_models_ids as $key => $value) {
            $followed_models_ids[] = $value['model_id'];
        }
        $whrArr2 = array('p.category' => 1);
        $used_post_ids = explode(",", $used_post_ids);
        $res = $this->db->select('p.id as post_id,p.image as teaser_image,p.created as teaser_created_date,p.post_type,p.image as post_image,u.*')->from('tbl_model_posts p')->join('tbl_users u', 'u.id=p.model_id', 'left')->where($whrArr2)->where_not_in('p.model_id', $followed_models_ids)->where_not_in('p.id', $used_post_ids)->group_by('p.id')->order_by('p.created', 'desc')->limit($limit)->get()->result_array();
        foreach ($res as $key => $value) {
            $res[$key]['is_fav_post'] = 0;
            $fav_posts = $this->common->get_data_by_id('tbl_model_posts_fav', 'post_id', $value['post_id'], '*', array('user_id' => $userId));
            if (count($fav_posts) > 0) {
                $res[$key]['is_fav_post'] = 1;
            }
        }
        //pre($res);
        return $res;
    }

    //get favourite post
    public function get_fav_post($userId = 0, $limit = 0, $filter = array())
    {
        extract($filter);
        // $followed_models_ids = array(0);
        // $whrArr = array('mf.user_id' => $userId);
        // $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_model_posts_fav mf')->where($whrArr)->get()->result_array();
        // foreach ($tmp_followed_models_ids as $key => $value) {
        //     $followed_models_ids[] = $value['model_id'];
        // }
        $whrArr2 = array('fav.user_id' => $userId);
        $used_post_ids = explode(",", $used_post_ids);
        $res = $this->db->select('p.id as post_id,p.image as teaser_image,p.created as teaser_created_date,p.post_type,p.image as post_image,u.*')
            ->from('tbl_model_posts_fav fav')
            ->join('tbl_model_posts p', 'p.id=fav.post_id', 'left')
            ->join('tbl_users u', 'u.id=p.model_id', 'left')
            ->where($whrArr2)
            //->where_not_in('p.model_id', $followed_models_ids)
            ->where_not_in('p.id', $used_post_ids)->group_by('p.id')
            ->order_by('p.created', 'desc')
            ->limit($limit)
            ->get()->result_array();

        return $res;
    }

    //get followed models's post
    public function get_followed_post($userId = 0, $limit = 0, $filter = array())
    {
        $followed_models_ids = array(0);
        extract($filter);
        $whrArr = array('mf.user_id' => $userId);
        $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_model_followers mf')->where($whrArr)->get()->result_array();
        foreach ($tmp_followed_models_ids as $key => $value) {
            $followed_models_ids[] = $value['model_id'];
        }
        //pre($followed_models_ids);
        // $whrArr2 = array('p.category' => 1);
        $whrArr2 = array('p.category' => 0);
        $used_post_ids = explode(",", $used_post_ids);
        $res = $this->db->select('p.id as post_id,p.image as teaser_image,p.created as teaser_created_date,p.post_type,p.image as post_image,u.*')
            ->from('tbl_model_posts p')
            ->join('tbl_users u', 'u.id=p.model_id', 'left')
            ->where($whrArr2)
            ->where_in('p.model_id', $followed_models_ids)
            ->where_not_in('p.id', $used_post_ids)
            ->group_by('p.id')
            ->order_by('p.created', 'desc')
            ->limit($limit)
            ->get()->result_array();
        //pre($res);
        foreach ($res as $key => $value) {
            $res[$key]['is_fav_post'] = 0;
            $fav_posts = $this->common->get_data_by_id('tbl_model_posts_fav', 'post_id', $value['post_id'], '*', array('user_id' => $userId));
            if (count($fav_posts) > 0) {
                $res[$key]['is_fav_post'] = 1;
            }
        }

        // pre($res);
        return $res;
    }

    public function get_allModels_all_teaserPosts($filter = array())
    {
        extract($filter);
        $whrArr2 = array('p.category' => 1);

        $used_post_ids = explode(",", $used_post_ids);
        return $this->db->select('p.id as post_id,p.image as teaser_image,p.created as teaser_created_date,p.post_type,p.image as post_image,u.*')->from('tbl_model_posts p')->join('tbl_users u', 'u.id=p.model_id', 'left')->where($whrArr2)->where_not_in('p.id', $used_post_ids)->group_by('p.id')->order_by('p.created', $order_by)->limit($limit)->get()->result_array();
    }

    public function get_suggestion_models($limit, $userId)
    {
        $followed_models_ids = array(0);
        $whrArr = array('mf.user_id' => $userId);
        $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_model_followers mf')->where($whrArr)->get()->result_array();
        foreach ($tmp_followed_models_ids as $key => $value) {
            $followed_models_ids[] = $value['model_id'];
        }
        $whrArr2 = array('type' => 'model', 'diamonds>=' => 5);
        $result = $this->db->select('*')->from('tbl_users')->where($whrArr2)->where_not_in('id', $followed_models_ids)->limit($limit)->order_by('diamonds', 'desc')->get()->result_array();
        return $result;
    }

    public function get_model_detail($slug = '')
    {

        $whrArr = array('m.user_slug' => $slug);
        $modelData = $this->db->select('m.*,me.rules as bio,me.follow_price,me.snapchat_price')->from('tbl_users m')->join('tbl_model_extra me', 'me.model_id=m.id', 'left')->where($whrArr)->get()->row_array();
        return $modelData;
    }

    public function get_notification_list($user_id)
    {
        $whrArr = array('user_id' => $user_id);
        $res = $this->db->select('n.*')
            ->from('tbl_notification n')
            ->where($whrArr)
            ->get()
            ->result_array();
        return $res;
    }

    //get premium videos for user that he purchased
    public function get_premium_videos($user_id)
    {
        $whrArr = array('v.user_id' => $user_id, 'v.category ' => '0');
        $res = $this->db->select('p.id as post_id, p.image,u.profile_image,u.name')
            ->from('tbl_videos v')
            ->join('tbl_model_posts p', 'p.id=v.post_id', 'left')
            ->join('tbl_users u', 'u.id=p.model_id', 'left')
            ->where($whrArr)
            ->get()->result_array();
        if (!empty($res)) {
            foreach ($res as $kR => $vR) {
                $commentCount = $this->db->select('')
                    ->from('tbl_model_post_comments')
                    ->where('post_id', $vR['post_id'])
                    ->where('status', 'active')
                    ->get()->num_rows();
                $res[$kR]['comment_count'] = $commentCount;

                $commentsArray = $this->db->select('pc.id as comment_id, pc.commenter_id as commenter_id, u.name as commenter_name, u.type as commenter_type, u.profile_image as commenter_image, pc.comment as comment, pc.created_at as created_at')
                    ->from('tbl_model_post_comments as pc')
                    ->join('tbl_users as u', 'u.id=pc.commenter_id', 'left')
                    ->where('pc.post_id', $vR['post_id'])
                    ->where('pc.status', 'active')
                    ->order_by('pc.created_at', 'DESC')
                    ->get()->result_array();
                $res[$kR]['comments'] = $commentsArray;
            }
        }
        //pre($res);
        return $res;
    }

    // //get premium video from model detail page that user not purchased
    // public function get_premium_video_for_model_detail($slug = '')
    // {
    //     $purchased_video_post_id = array();
    //     $premium_videos = $this->db->select('v.*')->from('tbl_videos v')
    //     ->join('tbl_users m','m.id=p.model')
    //     ->where('user_id', $this->user_id)->get()->result_array();
    //     foreach ($premium_videos as $key => $value) {
    //         $purchased_video_post_id[] = $value['post_id'];
    //     }
    //     $whrArr = array('m.user_slug' => $slug);
    //     $modelData = $this->db->select('m.*,me.rules as bio,me.follow_price,me.snapchat_price')
    //         ->from('tbl_users m')
    //         ->join('tbl_model_extra me', 'me.model_id=m.id', 'left')
    //         ->where($whrArr)
    //         ->where_not_in('')
    //         ->get()->row_array();
    //     return $modelData;
    // }


    //get premium video from model detail page that user not purchased
    public function get_premium_video_by_model_id($modelID = 0, $postType = 0, $category = 1)
    {

        $purchased_video_post_id = array();
        $premium_videos = $this->db->select('v.*')->from('tbl_videos v')
            ->where('user_id', $this->user_id)->get()->result_array();
        foreach ($premium_videos as $key => $value) {
            $purchased_video_post_id[] = $value['post_id'];
        }

        $result = array();
        if ($modelID) {
            $whrArr = array('p.model_id' => $modelID, 'category' => $category);
            if ($postType != '') {
                $whrArr['p.post_type'] = $postType;
            }
            $this->db->select('p.*,u.profile_image,u.name')
                ->from('tbl_model_posts p')
                ->join('tbl_users u', 'u.id=p.model_id', 'left')
                ->where($whrArr);
            if (count($purchased_video_post_id) > 0) {

                $this->db->where_not_in('p.id', $purchased_video_post_id);
            }
            $result = $this->db->get()->result_array();
        }
        //pre($this->db->last_query());
        return $result;
    }

    //get premmium videos for main premium videos header page
    public function get_premium_videos_page($limit, $offset, $order, $usedIds = array(), $filters = array())
    {

        $purchased_video_post_id = array();
        $premium_videos = $this->db->select('v.*')->from('tbl_videos v')
            ->where('user_id', $this->user_id)->get()->result_array();
        foreach ($premium_videos as $key => $value) {
            $purchased_video_post_id[] = $value['post_id'];
        }

        $result = array();

        // $whrArr = array('p.category' => 3);
        extract($filters);
        $whrArr = array('m.type' => 'model', 'm.status' => 'active', 'p.category' => '3');
        $likeArr = array();
        if (!empty($search)) {
            $likeArr['p.video_hint_text'] = $search;
        }
        if (!empty($featured)) {
            $whrArr['m.is_featured'] = 1;
        }
        if (!empty($live_status)) {
            $whrArr['m.live_status'] = 1;
        }

        $this->db->select('p.*,m.name,m.id as model_id')
            ->from('tbl_model_posts p')
            ->join('tbl_users m', 'm.id=p.model_id', 'left')
            ->where($whrArr)
            ->where_not_in('p.id', $usedIds);
        if (count($purchased_video_post_id) > 0) {
            $this->db->where_not_in('p.id', $purchased_video_post_id);
        }
        if (!empty($likeArr)) {
            $this->db->like($likeArr);
        }

        if (!empty($newest)) {  //get new model date wise created
            $this->db->order_by('p.created', 'desc');
        }
        if ($order != 'RANDOM') {
            $this->db->limit($limit, $offset);
        } else {
            $this->db->limit($limit);
        }
        $this->db->group_by('p.id');
        $result = $this->db->get()->result_array();

        foreach ($result as $key => $value) {
            $result[$key]['thumb_image'] = checkImage(3, $value['video_thumb_image']);
        }

        //pre($result);
        return $result;
    }

    function get_all_custom_videos()
    {
        $whrArr = array('p.post_type ' => '1', 'p.category' => '2');
        $res = $this->db->select('p.id,p.image,p.post_price,u.profile_image,u.name,u.id as model_id')
            ->from('tbl_model_posts p')
            ->join('tbl_users u', 'u.id=p.model_id', 'left')
            ->where($whrArr)
            ->get()->result_array();
        //   pre($res);
        return $res;
    }

    function remove_from_geo_block($post = array())
    {
        $whrArr = array('id' => $post['block_id']);
        $this->db->where($whrArr)->delete('tbl_geo_blocking');
    }
    public function get_freinds_feed($userId = 0, $limit = 0, $filter = array())
    {
        $followed_models_ids = array(0);
        extract($filter);
        $whrArr = array('mf.user_id' => $userId, 'mf.request_status' => 1);
        $tmp_followed_models_ids = $this->db->select('mf.model_id')->from('tbl_friend_requests mf')->where($whrArr)->get()->result_array();
        // pre($tmp_followed_models_ids);
        foreach ($tmp_followed_models_ids as $key => $value) {
            $followed_models_ids[] = $value['model_id'];
        }
        $whrArr2 = array('p.category' => 0);
        $used_post_ids = explode(",", $used_post_ids);
        $res = $this->db->select('p.id as post_id,p.image as teaser_image,p.created as teaser_created_date,p.post_type,p.image as post_image,u.*')->from('tbl_model_posts p')->join('tbl_users u', 'u.id=p.model_id', 'left')->where($whrArr2)->where_in('p.model_id', $followed_models_ids)->where_not_in('p.id', $used_post_ids)->group_by('p.id')->order_by('p.created', 'desc')->limit($limit)->get()->result_array();

        //pre($res);
        return $res;
    }
    public function get_followers_for_model($userId = 0, $limit = 0, $filter = array())
    {
        //pre($filter);
        $followed_models_ids = array(0);
        extract($filter);
        $whrArr = array('mf.model_id' => $userId);
        $res = $this->db->select('u.*')
            ->from('tbl_model_followers mf')
            ->join('tbl_users u', 'u.id=mf.user_id', 'left')
            ->where($whrArr)
            ->where_not_in('u.id', $used_post_ids)
            ->get()->result_array();
        return $res;
    }
    public function get_all_post_by_model_id($modelID = 0)
    {

        $result = array();
        if ($modelID) {
            $whrArr = array('p.model_id' => $modelID);
            $this->db->select('p.*,p.created as teaser_created_date,u.profile_image,u.name')
                ->from('tbl_model_posts p')
                ->join('tbl_users u', 'u.id=p.model_id', 'left')
                ->where($whrArr);
            $result = $this->db->get()->result_array();
        }
        return $result;
    }

    public function get_followers_of_model($model_id, $filters)
    {
        $res2 = array();
        if ($filters == "last_seven_days") {
            $whrArr = array('mf.model_id' => $model_id);
            $res = $this->db->select('mf.*,u.name')
                ->from('tbl_model_followers mf')
                ->join('tbl_users u', 'u.id=mf.user_id', 'left')
                ->where($whrArr)
                ->get()->result_array();

            foreach ($res as $key => $value) {
                for ($i = 1; $i <= 7; $i++) {
                    $datee = date('Y-m-d', strtotime('-' . $i . ' days'));
                    // pre($datee);
                    if (date('Y-m-d', strtotime($value['created'])) == $datee) {
                        $value['day'] = $i;
                        $res2[] = $value;
                    }
                }
            }
        }
        //pre($res2);
        for ($i = 1; $i <= 7; $i++) {
            $counter = 0;
            foreach ($res2 as $key => $value) {
                if ($value['day'] == $i) {
                    $counter = $counter + 1;
                    $res3[$i] = $counter;
                }
            }
        }

        // for ($i = 1; $i <= 7; $i++) {
        //     $counter = 0;
        //     foreach ($res2 as $key => $value) {
        //         if ($value['day'] == $i) {
        //             $counter = $counter + 1;
        //             $res3[$i] = $counter;
        //         }
        //     }
        // }
        // pre($res3);
        // pre($res4);
        return $res3;
    }

    public function get_customVideos_of_model($model_id, $filters)
    {
        $res2 = array();
        $res3 = array();
        if ($filters == "last_seven_days") {
            $whrArr = array('mp.model_id' => $model_id, 'v.category' => '0');
            $res = $this->db->select('p.*')
                ->from('tbl_model_posts mp')
                ->join('tbl_videos v', 'v.post_id=mp.id', 'left')
                ->join('tbl_payment p', 'p.payment_id=v.payment_id', 'left')
                ->where($whrArr)
                ->get()->result_array();
            // pre($this->db->last_query());
            foreach ($res as $key => $value) {
                for ($i = 1; $i <= 7; $i++) {
                    $datee = date('Y-m-d', strtotime('-' . $i . ' days'));

                    if (date('Y-m-d', strtotime($value['created'])) == $datee) {
                        $value['day'] = $i;
                        $res2[] = $value;
                    }
                }
            }
        }
        //pre($res2);
        for ($i = 1; $i <= 7; $i++) {
            $counter = 0;
            foreach ($res2 as $key => $value) {
                if ($value['day'] == $i) {
                    $counter = $counter + 1;
                    $res3[$i] = $counter;
                }
            }
        }
        //pre($res3);
        return $res3;
    }
    public function get_privateVideos_of_model($model_id, $filters)
    {
        $res2 = array();
        $res3 = array();
        if ($filters == "last_seven_days") {
            $whrArr = array('mp.model_id' => $model_id, 'v.category' => '1');
            $res = $this->db->select('p.*')
                ->from('tbl_model_posts mp')
                ->join('tbl_videos v', 'v.post_id=mp.id', 'left')
                ->join('tbl_payment p', 'p.payment_id=v.payment_id', 'left')
                ->where($whrArr)
                ->get()->result_array();
            // pre($this->db->last_query());
            foreach ($res as $key => $value) {
                for ($i = 1; $i <= 7; $i++) {
                    $datee = date('Y-m-d', strtotime('-' . $i . ' days'));

                    if (date('Y-m-d', strtotime($value['created'])) == $datee) {
                        $value['day'] = $i;
                        $res2[] = $value;
                    }
                }
            }
        }
        //pre($res2);
        for ($i = 1; $i <= 7; $i++) {
            $counter = 0;
            foreach ($res2 as $key => $value) {
                if ($value['day'] == $i) {
                    $counter = $counter + 1;
                    $res3[$i] = $counter;
                }
            }
        }
        //pre($res3);
        return $res3;
    }
}
