<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_videos_model extends MY_Model
{

    public $_table = 'tbl_model_posts';
    public $_fields = "*";
    public $_where = array();
    public $_whereField = "";
    public $_whereFieldVal = "";
    public $_except_fields = array();

    public function __construct()
    {
        parent::__construct();
    }

    //User management starts
    function getGeoBlockingAll($filters = array())
    {
        $result = array();
        extract($filters);
        $whrArr = array('mp.post_type' => 1);

        $likeArr = array();
        if (!empty($search)) {
            $likeArr['m.name'] = $search;
        }
        $this->db->select('mp.id, mp.category, mp.image, m.name, IF(mp.status="active","checked","") as status, mp.is_approved')
            ->from('tbl_model_posts as mp')
            ->join('tbl_users as m', 'm.id = mp.model_id')
            ->where($whrArr);

        if (!empty($likeArr)) {
            $this->db->group_start()
                ->like('m.name', $search)
                ->or_like($likeArr)
                ->group_end();
        }
        $sort = 'mp.id';
        $selUsers = $this->db->limit($limit, $offset)
            ->order_by($sort, $order)
            ->group_by('mp.id')
            ->get()->result_array();
        $this->db->select('COUNT(mp.id) AS total')
            ->from('tbl_model_posts as mp')
            ->join('tbl_users as m', 'm.id = mp.model_id')
            ->where($whrArr);
        if (!empty($likeArr)) {
            $this->db->group_start()
                ->like('m.name', $search)
                ->or_like($likeArr)
                ->group_end();
        }
        $totalRec = $this->db->get()->row_array();

        $params = array('url' => base_url($this->ADM_URL . 'post_images'));
        $result['data'] = array();
        foreach ($selUsers as $i => $user) {
            $params['id'] = $user['id'];
            $params['checked'] = $user['status'];
            $params['table'] = 'tbl_geo_blocking';
            $checkbox = $this->generate_checkbox($params);
            $switch = '-';
            $extra = array();
            $is_edit = FALSE;
            $is_view = FALSE;
            $is_delete = FALSE;
            if (array_key_exists('MODEL_POST', $this->access)) {
                $change_status = TRUE;
                if ($this->access['MODEL_POST']['IMAGES']['status'] == 'no')
                    $change_status = FALSE;
                //if ($change_status == TRUE)
                    //$switch = $this->generate_switch($params);
                if ($this->access['MODEL_POST']['IMAGES']['edit'] == 'yes')
                    $is_edit = TRUE;
                if ($this->access['MODEL_POST']['IMAGES']['delete'] == 'yes')
                    $is_delete = TRUE;
                if ($this->access['MODEL_POST']['IMAGES']['view'] == 'yes')
                    $is_view = TRUE;
                if ($this->access['MODEL_POST']['IMAGES']['extra'] == 'yes')
                    if($user['is_approved'] == 0) {
                        $extra['approve_button']['btn_id'] = $user['id'];
                        $extra['approve_button']['onclick'] = '';
                        $extra['approve_button']['data_type'] = 'is_approved';
                        $extra['approve_button']['btn_class'] = 'btn btn-primary btn-xs btnApprove mr-2';
                        $extra['approve_button']['btn_name'] = 'Approve';
                        $extra['approve_button']['btn_modal'] = '';
                        $extra['approve_button']['url'] = base_url($this->ADM_URL . 'post_images/change_is_approved');
                        $extra['approve_button']['data_id'] = $user['id'];
                    }
            }
            $action = $this->generate_actions($params, $is_edit, $is_view, $is_delete, $extra);
            if ($is_edit == FALSE && $is_view == FALSE && $is_delete == FALSE && empty($extra))
                $action = '-';
            if($user['category'] == 0) {
                $categoryName = 'Followers';
            } else if ($user['category'] == 1) {
                $categoryName = 'Teaser';
            } else if ($user['category'] == 2) {
                $categoryName = 'Private';
            } else if ($user['category'] == 3) {
                $categoryName = 'Premium';
            } else {
                $categoryName = 'N/A';    
            }
            if($user['image'] != '') {
                $imageSrc = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal-'.$user['id'].'">
                              Watch Video
                            </button>

                            <div class="modal fade" id="exampleModal-'.$user['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Watch Video</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <video width="320" height="240" controls>
                                      <source src="'.base_url().'themes/uploads/models/'.$user['image'].'" type="video/mp4">
                                    </video>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>';
                $video_code = '<video width="320" height="240" controls>
                              <source src="'.base_url().'themes/uploads/models/'.$user['image'].'" type="video/mp4">
                            </video>';
            } else {
                $imageSrc = 'N/A';
            }
            $result['data'][] = array(
                'id' => ($offset + $i + 1),
                'name' => (!empty($user['name'])) ? $user['name'] : 'N/A',
                'category' => $categoryName,
                'image' => $imageSrc,
                'action' => $action,
                'video_code' => $video_code
            );
        }
        $result["recordsTotal"] = (int)$totalRec['total'];
        $result["recordsFiltered"] = (int)$totalRec['total'];
        return $result;
    }

    function getUserDetailsById($iUserId = 0)
    {
        $query = $this->db->select("*")->from('tbl_users as u')
            ->where('u.id', $iUserId)->get();
        $_result = $query->row_array();
        if (!empty($_result)) {
            $_result['profile_image'] = checkImage(2, $_result['profile_image'], 0, 0, false);
        }
        return $_result;
    }

}
