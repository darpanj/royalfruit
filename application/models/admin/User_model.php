<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model
{

    public $_table = 'tbl_users';
    public $_fields = "*";
    public $_where = array();
    public $_whereField = "";
    public $_whereFieldVal = "";
    public $_except_fields = array();

    public function __construct()
    {
        parent::__construct();
    }

    //User management starts
    function getUsersAll($filters = array())
    {
        $result = array();
        extract($filters);
        $whrArr = array('type' => 'user');

        $likeArr = array();
        if (!empty($search)) {
            $likeArr['u.name'] = $search;
        }
        $this->db->select('u.id,u.type, u.name, u.email ,u.profile_image ,u.mobile, IF(u.status="active","checked","") as status')
            ->from('tbl_users u')
            ->where($whrArr);

        if (!empty($likeArr)) {
            $this->db->group_start()
                ->like('name', $search)
                ->or_like('email', $search)
                ->or_like('mobile', $search)
                ->or_like($likeArr)
                ->group_end();
        }
        $selUsers = $this->db->limit($limit, $offset)
            ->order_by($sort, $order)
            ->group_by('u.id')
            ->get()->result_array();
        $this->db->select('COUNT(u.id) AS total')
            ->from('tbl_users u')
            ->where($whrArr);
        if (!empty($likeArr)) {
            $this->db->group_start()
                ->like('name', $search)
                ->or_like('email', $search)
                ->or_like('mobile', $search)
                ->or_like($likeArr)
                ->group_end();
        }
        $totalRec = $this->db->get()->row_array();

        $params = array('url' => base_url($this->ADM_URL . strtolower($this->data['class'])));
        $result['data'] = array();
        foreach ($selUsers as $i => $user) {
            $params['id'] = $user['id'];
            $params['checked'] = $user['status'];
            $params['table'] = 'tbl_users';
            $checkbox = $this->generate_checkbox($params);
            $switch = '-';
            $extra = array(//                ''
            );
            $is_edit = FALSE;
            $is_view = FALSE;
            $is_delete = FALSE;
            if (array_key_exists('USERS', $this->access)) {
                $change_status = TRUE;
                if ($this->access['USERS']['USERS']['status'] == 'no')
                    $change_status = FALSE;
                if ($change_status == TRUE)
                    $switch = $this->generate_switch($params);
                if ($this->access['USERS']['USERS']['edit'] == 'yes')
                    $is_edit = TRUE;
                if ($this->access['USERS']['USERS']['delete'] == 'yes')
                    $is_delete = TRUE;
                if ($this->access['USERS']['USERS']['view'] == 'yes')
                    $is_view = TRUE;
            }
            $action = $this->generate_actions($params, $is_edit, $is_view, $is_delete, $extra);
            if ($is_edit == FALSE && $is_view == FALSE && $is_delete == FALSE && empty($extra))
                $action = '-';
            $icon = checkImage(1, $user['profile_image']);
            $img = "<a href='{$icon}' data-fancybox data-caption='{$user['name']}'><img width='50px' height='50px' src='{$icon}'></a>";
            $result['data'][] = array(
                'id' => ($offset + $i + 1),
                'image' => $img,
                'name' => (!empty($user['name'])) ? $user['name'] : 'N/A',
                'mobile' => ($user['mobile'] != '' ? $user['mobile'] : 'NA'),
                'status' => $switch,
                'action' => $action
            );
        }
        $result["recordsTotal"] = (int)$totalRec['total'];
        $result["recordsFiltered"] = (int)$totalRec['total'];
        return $result;
    }

    function submit_user($post = array())
    {
        $content = array();
        $content['status'] = 404;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        $iUserId = isset($post['id']) ? (int)$post['id'] : 0;
        $dataArray = array(
            'name' => $post['name'],
            'email' => $post['email'],
            'country_code' => '+' . $post['country_code'],
            //'mobile' => $post['mobile'],
            'updated' => get_date(),
        );

        if ($this->input->post('password') != '') {
            $dataArray['password'] = md5($this->input->post('password'));
        }
        if (!empty($this->input->post('image_path'))) {
            $dataArray['profile_image'] = $post['image_path'];
        }
        if ($iUserId > 0) {
            $this->db->update('tbl_users', $dataArray, array('id' => $iUserId));
            $content['status'] = 200;
            $content['message'] = sprintf($this->data['language']['succ_rec_updated'], $this->lang->line('lbl_user'));
        } else {
            $dataArray['country_code'] = '+257';
            $dataArray['type'] = 'user';
            $dataArray['created'] = get_date();
            $this->db->insert('tbl_users', $dataArray);
            $iUserId = $this->db->insert_id();
            if ($iUserId > 0) {
                $content['status'] = 200;
                $content['message'] = sprintf($this->data['language']['succ_rec_added'], $this->lang->line('lbl_user'));
            }
        }
        if ($iUserId > 0) {
            $this->load->library('upload');
            $this->load->library('image_lib');
            if (isset($_FILES['profile_image']['name']) && $_FILES['profile_image']['name'] != '') {
                $file_config = array();
                $path = FCPATH . SITE_UPD . 'users/';
                if (!file_exists($path)) {
                    @mkdir($path);
                }
                $file_config['upload_path'] = $path;
                $file_config['allowed_types'] = 'jpg|png|jpeg';
                $file_config['overwrite'] = TRUE;
                $file_config['max_size'] = '204800';
                $file_config['file_name'] = md5(time() . rand());

                $this->upload->initialize($file_config);
                if ($this->upload->do_upload('profile_image')) {
                    $content['status'] = 200;
                    $upload_data = $this->upload->data();
                    $edit_data['profile_image'] = $upload_data['file_name'];

                    $query = $this->db->query("SELECT profile_image FROM tbl_users WHERE id = ? LIMIT 1", array($iUserId));
                    if ($query->num_rows() === 1) {
                        $result = $query->row_array();
                        $file = FCPATH . SITE_UPD . 'users/' . $result['profile_image'];
                        unlink_file($file);
                    }
                    $this->db->update('tbl_users', $edit_data, array('id' => $iUserId));
                }
            }
        }
        return $content;
    }

    function getUserDetailsById($iUserId = 0)
    {
        $query = $this->db->select("*")->from('tbl_users as u')->where('u.id', $iUserId)->get();
        $_result = $query->row_array();
        if (!empty($_result)) {
            $_result['profile_image'] = checkImage(1, $_result['profile_image'], 0, 0, false);
        }
        return $_result;
    }


    public function get_addition_item($user_id = 0)
    {
        $q = $this->db->from('tbl_booking_additional ba')->join('tbl_booking b', 'ba.booking_id=b.id')->where('b.user_id', $user_id)->get()->num_rows();
        return $q;
    }


}
