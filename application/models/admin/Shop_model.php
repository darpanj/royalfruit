<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends MY_Model {

    public $_table = 'tbl_shop';
    public $_fields = "*";
    public $_where = array();
    public $_whereField = "";
    public $_whereFieldVal = "";
    public $_except_fields = array();

    public function __construct() {
        parent::__construct();
    }

    //Shop management starts
    function getShopAll($filters = array()) {
        $result = array();
        extract($filters);
        $whrArr = array();

        $likeArr = array();
        if (!empty($search)) {
            $likeArr['name'] = $search;
        }
        $this->db->select('s.*, IF(s.status="active","checked","") as status')
                ->from('tbl_shop s')
                ->where($whrArr);

        if (!empty($likeArr)) {
            $this->db->group_start()
                    ->like('name', $search)
                    ->or_like('price', $search)
                    ->or_like($likeArr)
                    ->group_end();
        }
        $selShop = $this->db->limit($limit, $offset)
                        ->order_by($sort, $order)
                        ->get()->result_array();
        $this->db->select('COUNT(s.id) AS total')
                ->from('tbl_shop s')
                ->where($whrArr);
        if (!empty($likeArr)) {
            $this->db->group_start()
                    ->like('name', $search)
                    ->or_like('price', $search)
                    ->or_like($likeArr)
                    ->group_end();
        }
        $totalRec = $this->db->get()->row_array();

        $params = array('url' => base_url($this->ADM_URL . strtolower($this->data['class'])));
        $result['data'] = array();
        foreach ($selShop as $i => $shop) {
            $params['id'] = $shop['id'];
            $params['checked'] = $shop['status'];
            $params['table'] = 'tbl_shop';
            $checkbox = $this->generate_checkbox($params);
            $switch = '-';
            $extra = array();
            $is_edit = FALSE;
            $is_view = FALSE;
            $is_delete = FALSE;
            if (array_key_exists('SHOP', $this->access)) {
                $change_status = TRUE;
                if ($this->access['SHOP']['SHOP']['status'] == 'no')
                    $change_status = FALSE;
                if ($change_status == TRUE)
                    $switch = $this->generate_switch($params);
                if ($this->access['SHOP']['SHOP']['edit'] == 'yes')
                    $is_edit = TRUE;
                if ($this->access['SHOP']['SHOP']['delete'] == 'yes')
                    $is_delete = TRUE;
                if ($this->access['SHOP']['SHOP']['view'] == 'yes')
                    $is_view = FALSE;
            }
            $action = $this->generate_actions($params, $is_edit, $is_view, $is_delete, $extra);
            if ($is_edit == FALSE && $is_view == FALSE && $is_delete == FALSE && empty($extra))
                $action = '-';
            $icon = checkImage(8, $shop['image']);
            $img = "<a href='{$icon}' data-fancybox data-caption='{$shop['name']}'><img width='50px' height='50px' src='{$icon}'></a>";
            $result['data'][] = array(
                'id' => ($offset + $i + 1),
                'image' => $img,
                'name' => (!empty($shop['name'])) ? $shop['name'] : 'N/A',
                'price' => ($shop['price'] != '' ? $shop['price'] : 'NA'),
                'status' => $switch,
                'action' => $action
            );
        }
        $result["recordsTotal"] = (int) $totalRec['total'];
        $result["recordsFiltered"] = (int) $totalRec['total'];
        return $result;
    }

    function submit_shop($post = array()) {
        $content = array();
        $content['status'] = 404;
        $content['message'] = $this->data['language']['err_something_went_wrong'];
        $iShopId = isset($post['id']) ? (int) $post['id'] : 0;
        $dataArray = array(
            'name' => $post['name'],
            'price' => $post['price'],
            'update_date' => get_date(),
        );

        if (!empty($this->input->post('image_path'))) {
            $dataArray['image'] = $post['image_path'];
        }
        if ($iShopId > 0) {
            $this->db->update('tbl_shop', $dataArray, array('id' => $iShopId));
            $content['status'] = 200;
            $content['message'] = sprintf($this->data['language']['succ_rec_updated'], 'Shop');
        } else {
            $dataArray['insert_date'] = get_date();
            $this->db->insert('tbl_shop', $dataArray);
            $iShopId = $this->db->insert_id();
            if ($iShopId > 0) {
                $content['status'] = 200;
                $content['message'] = sprintf($this->data['language']['succ_rec_added'], 'Shop');
            }
        }
        return $content;
    }

}
