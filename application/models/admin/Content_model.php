<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Content_model extends MY_Model
{
    function Listing($filters = array())
    {
        $result = array();
        extract($filters);

        $likeArr = array();
        if (!empty($search)) {
            $likeArr['u.name'] = $search;
        }
        $this->db->select('c.id,c.title')->from('mst_content c');


        //         $this->db->limit($limit, $offset)
        //            ->order_by($sort, $order)
        $selUsers = $this->db->get()->result_array();


        $this->db->select('COUNT(c.id) AS total')
            ->from('mst_content c');

        $totalRec = $this->db->get()->row_array();

        $params = array('url' => base_url($this->ADM_URL . strtolower($this->data['class'])));
        $result['data'] = array();
        foreach ($selUsers as $i => $user) {
            $params['id'] = $user['id'];
            $params['checked'] = 'active';
            $params['table'] = 'mst_content';
            $extra = array();
            $is_edit = FALSE;
            $is_view = FALSE;
            $is_delete = FALSE;
            // pre($this->access);
            if (array_key_exists('GEN_SET', $this->access)) {
                $change_status = TRUE;
                if ($this->access['GEN_SET']['Content']['status'] == 'no')
                    $change_status = FALSE;
                if ($change_status == TRUE)
                    $switch = $this->generate_switch($params);
                if ($this->access['GEN_SET']['Content']['edit'] == 'yes')
                    $is_edit = TRUE;
                // pre($this->access['GEN_SET']['Content']);
                if ($this->access['GEN_SET']['Content']['delete'] == 'yes')
                    $is_delete = TRUE;
                if ($this->access['GEN_SET']['Content']['view'] == 'yes')
                    $is_view = FALSE;
            }
            $action = $this->generate_actions($params, $is_edit, $is_view, $is_delete, $extra);
            if ($is_edit == FALSE && $is_view == FALSE && $is_delete == FALSE && empty($extra))
                $action = '-';
            $result['data'][] = array(
                'id' => ($offset + $i + 1),
                'title' => $user['title'],
                'action' => $action
            );
        }
        $result["recordsTotal"] = (int) $totalRec['total'];
        $result["recordsFiltered"] = (int) $totalRec['total'];
        return $result;
    }
}
