<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Getting_started_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function submitSettings($post = array(), $files = array()) {
        $status = 0;
        $oldFile = $this->common->get_data_by_id('tbl_getting_started', 'id', 1, 'image, pdf', '', '', '', '', 'row');
        $this->load->library('upload');
        $this->load->library('image_lib');
        $insertData['file_name'] = $this->upload_form_file('getting_started', $files);
        $data['image'] = $insertData['file_name'][0];
        $data['pdf'] = $insertData['file_name'][1];
        $data['updated_at'] = get_date();
        $updateData = $this->common->update_data($data, 'tbl_getting_started', 'id', 1);
        if($updateData) {
            if(!empty($oldFile)) {
                if($oldFile['image'] != '') {
                    $path = FCPATH . SITE_UPD . 'getting_started/'.$oldFile['image'];
                        if (file_exists($path)){
                            unlink($path);
                        }
                }
                if($oldFile['pdf'] != '') {
                    $path = FCPATH . SITE_UPD . 'getting_started/'.$oldFile['pdf'];
                        if (file_exists($path)){
                            unlink($path);
                        }
                }
            }
            $status = 1;
        }
        return $status;
    }

    public function upload_form_file($folder = 'getting_started', $files)
    {
        $failed = '';
        $config['upload_path'] = FCPATH . SITE_UPD . $folder;
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);
        $images = array();
        foreach ($files as $key => $image) {
            $_FILES['images[]']['name']= $image['name'];
            $_FILES['images[]']['type']= $image['type'];
            $_FILES['images[]']['tmp_name']= $image['tmp_name'];
            $_FILES['images[]']['error']= $image['error'];
            $_FILES['images[]']['size']= $image['size'];

            $new_name = md5(time() . rand());
            $config['file_name'] = $new_name;

            $this->upload->initialize($config);
            if ($this->upload->do_upload('images[]')) {
                $images[] = $this->upload->data('file_name');
            } else {
                /*$error = array('error' => $this->upload->display_errors());
                pre($error);*/
                return false;
            }
        }
        return $images;
    }


}
