<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Common extends CI_Model
{

    public function get_data_by_id($table_name = '', $column_name = '', $column_value = '', $data = '', $condition_array = array(), $order_by = '', $sort_by = 'ASC', $limit = '', $result = 'array')
    {
        $this->db->select($data);
        $this->db->from($table_name);
        $this->db->where($column_name, $column_value);
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }
        if ($order_by != '' && $sort_by != '') {
            $this->db->order_by($order_by, $sort_by);
        }
        if (!empty($limit))
            $this->db->limit($limit);
        if ($result == 'row')
            return $this->db->get()->row_array();
        else
            return $this->db->get()->result_array();
    }

    public function get_data($table_name = '', $data = '', $condition_array = array(), $order_by = '', $sort_by = 'ASC', $limit = '', $result = 'array')
    {
        $this->db->select($data);
        $this->db->from($table_name);
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }
        if ($order_by != '' && $sort_by != '') {
            $this->db->order_by($order_by, $sort_by);
        }
        if (!empty($limit))
            $this->db->limit($limit);
        if ($result == 'row')
            return $this->db->get()->row_array();
        else
            return $this->db->get()->result_array();
    }
    public function get_data_by_join($from = '', $data = '', $join = array(), $condition_array = array(), $order_by = '', $sort_by = 'ASC', $limit = '', $group_by = '', $result = 'array')
    {
        $this->db->select($data);
        $this->db->from($from);
        if (!empty($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, 'left');
            }
        }
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }
        if ($order_by != '' && $sort_by != '') {
            $this->db->order_by($order_by, $sort_by);
        }
        if (!empty($limit))
            $this->db->limit($limit);
        if (!empty($order_by))
            $this->db->order_by($order_by);
        if ($result == 'row')
            return $this->db->get()->row_array();
        else
            return $this->db->get()->result_array();
    }

    public function insert_data($tabel_name = '', $data = array())
    {
        $id = 0;
        if (!empty($tabel_name)) {
            $this->db->insert($tabel_name, $data);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    public function new_update_data($data = array(), $tabel_name = '', $wh = array())
    {
        $this->db->set($data);
        $this->db->where($wh);
        $this->db->update($tabel_name);
        return TRUE;
    }

    public function delete_data($tabel_name = '', $condition_array = array())
    {
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
            $this->db->delete($tabel_name);
            return TRUE;
        }
        return FALSE;
    }

    public function get_table_count($tabel_name = 'tbl_users', $condition_array = array())
    {
        $this->db->from($tabel_name);
        if (!empty($condition_array))
            $this->db->where($condition_array);
        return (string) $this->db->count_all_results();
    }

    public function delete_old_image($table_name = "", $condation = array(), $path = "", $name = "")
    {
        $check = $this->get_data($condation, $table_name, 'result_array');
        if ($check !== 0) {
            foreach ($check as $key => $value) {
                $file = FCPATH . SITE_UPD . $path . '/' . $value[$name];
                if (file_exists($file)) {
                    unlink_file($file);
                }
            }
        }
    }

    public function upload_image($file = array(), $user_id = 0, $path = "users")
    {

        $update_data = array();
        $path = FCPATH . SITE_UPD . $path . '/' . $user_id . '/';

        $this->load->library('upload');
        $this->load->library('image_lib');
        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png|jpeg',
            'overwrite' => TRUE,
            'max_size' => '204800',
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }

        if (isset($file['profile_image']['name']) && !empty($file['profile_image']['name'])) {
            $file_config['file_name'] = md5(time() . rand());
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('profile_image')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/' . $upload_data['file_name'];
                $update_data['profile_image'] = $file_path;
            }
        }
        if (count($update_data)) {
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
        }
    }

    public function update_data($data = array(), $tabel_name = '', $column_name = '', $column_value = '')
    {
<<<<<<< HEAD
=======

>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
        $this->db->set($data);
        $this->db->where($column_name, $column_value);
        $this->db->update($tabel_name);
        return TRUE;
    }

    public function upload_user_image($file = array(), $user_id = 0, $path = "users")
    {
        $update_data = array();
        $this->load->library('upload');
        $this->load->library('image_lib');
        $path = FCPATH . SITE_UPD . $path . '/' . $user_id . '/' . 'profile_pic/';
        //        pre($path);
        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png|jpeg',
            'overwrite' => TRUE,
            'max_size' => '204800',
            'file_name' => md5(time() . rand())
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }
        if (isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name'])) {
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('profile_image')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/profile_pic/' . $upload_data['file_name'];
                $update_data['profile_image'] = $file_path;
            }
        }

        if (count($update_data)) {
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
        }
    }

    public function get_post_by_model_id($modelID = 0, $postType = 0, $category = 1)
    {
        $result = array();
        if ($modelID) {
            $whrArr = array('p.model_id' => $modelID, 'category' => $category);
            if ($postType != '') {
                $whrArr['p.post_type'] = $postType;
            }
            $result = $this->db->select('p.*')->from('tbl_model_posts p')->where($whrArr)->get()->result_array();
        }
        //pre($this->db->last_query());
        return $result;
    }
<<<<<<< HEAD

    function get_live_models()
    {
        $whrArr = array('status' => 'active', 'type' => 'model', 'live_status' => 1);
        return $this->db->select('u.*')
            ->from('tbl_users u')
            ->where($whrArr)
            ->get()->result_array();
    }

    function purchase($type = 0, $user_id, $price)
    {
        //type 0 for token purchase
        //type 1 for follow purchase
        // type 2 for snapchat purchase
        // type 3 for private video purchase
        // type 4 for custom video purchase
        $paymentId = genUniqueStr('Payment_', 13, 'tbl_payment', 'payment_id');
        $insertdata = array(
            'payment_id' => $paymentId,
            'status' => 'success',
            'user_id' => $user_id,
            'payment_for' => $type,
            'price' => $price,
            'created' => get_date()
        );
        //pre($insertdata);
        $this->db->insert('tbl_payment', $insertdata);
        //pre('dfadfas');
        $returndata = array();
        $returndata['payment_flag'] = 1;
        $returndata['status'] = $insertdata['status'];
        $returndata['payment_id'] = $insertdata['payment_id'];
        return $returndata;
    }

    function get_models_with_price($modelID)
    {
        $whrArr = array('m.id' => $modelID);
        $res = $this->db->select('m.*,me.rules,me.follow_price,me.snapchat_price')
            ->from('tbl_users m')
            ->join('tbl_model_extra me', 'me.model_id=m.id', 'left')
            ->where($whrArr)
            ->get()->row_array();

        return $res;
    }

    function check_follow_or_not($modelID, $user_id)
    {
        $whrArr = array('model_id' => $modelID, 'user_id' => $user_id);
        $res = $this->db->select('*')->from('tbl_model_followers')->where($whrArr)->get()->num_rows();
        // pre($res);
        return $res;
    }

    public function get_post_by_id($postID = 0)
    {
        $result = array();
        if ($postID) {
            $whrArr = array('p.id' => $postID);
            $result = $this->db->select('p.*,m.name,m.profile_image')->from('tbl_model_posts p')
                ->join('tbl_users m', 'm.id=p.model_id', 'left')
                ->where($whrArr)->get()->row_array();
        }
        //pre($this->db->last_query());
        return $result;
    }

    public function get_modelsData_byPostId($postId)
    {
        $whrArr = array('p.id' => $postId);
        $res = $this->db->select('m.*')
            ->from('tbl_model_posts p')
            ->join('tbl_users m', 'm.id=p.model_id', 'left')
            ->where($whrArr)
            ->get()->row_array();
        $res['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $res['id'], 'post_type' => 0));
        $res['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $res['id'], 'post_type' => 1));

        $check = $this->db->select('*')->from('tbl_model_extra')->where('model_id', $res['id'])->get()->num_rows();
        if ($check > 0) {
            $res2 = $this->db->select('*')->from('tbl_model_extra')->where('model_id', $res['id'])->get()->row_array();
            $res['follow_price'] = $res2['follow_price'];
            $res['snapchat_price'] = $res2['snapchat_price'];
        } else {
            $res['follow_price'] = 0;
            $res['snapchat_price'] = 0;
        }
        return $res;
    }

    public function get_chat_users($userId = 0, $search_keyword = '')
    {
        $msgNotAvailable = array();
        if ($search_keyword != '') {
            $this->db->like('m.name', $search_keyword);
        }
        $res = $this->db->select('mf.id as mf_id, mf.model_id as model_id, m.name as model_name, mf.created as created_date, m.profile_image as profile_image')
            ->from('tbl_model_followers as mf')
            ->join('tbl_users as m', 'm.id = mf.model_id')
            ->join('tbl_messages as msg', 'msg.model_follower_id = mf.id')
            ->where('mf.user_id', $userId)
            ->group_by('mf.id')
            ->order_by('msg.created_at', 'DESC')
            ->get()->result_array();
        $mfIds = $this->db->select('id')->from('tbl_model_followers')->where('user_id', $userId)->get()->result_array();
        $msgNotAvailable = array();
        if (!empty($mfIds)) {
            foreach ($mfIds as $kMFID => $vMFID) {
                $messages = $this->db->select()->from('tbl_messages')->where('model_follower_id', $vMFID['id'])->get()->result_array();
                if (empty($messages)) {
                    $msgNotAvailable[] = $vMFID['id'];
                }
            }
            if(!empty($msgNotAvailable)) {
                if ($search_keyword != '') {
                    $this->db->like('m.name', $search_keyword);
                }
                $noChat = $this->db->select('mf.id as mf_id, mf.model_id as model_id, m.name as model_name, mf.created as created_date, m.profile_image as profile_image')
                    ->from('tbl_model_followers as mf')
                    ->join('tbl_users as m', 'm.id = mf.model_id')
                    ->where('mf.user_id', $userId)
                    ->where_in('mf.id', $msgNotAvailable)
                    ->group_by('mf.id')
                    ->order_by('mf.created', 'DESC')
                    ->get()->result_array();
                $res = array_merge($res, $noChat);
            }
        }
        return $res;
    }
    
    /*public function get_chat_users_for_models($userId = 0, $search_keyword = '')
    {
        if($search_keyword != '') {
            $this->db->like('u.name', $search_keyword);
        }

        $cUsers = array();
        $SQL = 'SELECT * FROM `tbl_messages` WHERE `sender_id` = '.$userId.' OR `receiver_id` = '.$userId.' GROUP BY case when sender_id = '.$userId.' then receiver_id else sender_id end ORDER BY id desc';
        $query = $this->db->query($SQL);
        $chatUsers = $query->result_array();
        if(!empty($chatUsers)) {
            foreach($chatUsers as $kCU => $vCU) {
                if($vCU['sender_id'] == $userId) {
                    $cUsers[$kCU]['id'] = $vCU['receiver_id'];
                    $userData = $this->db->select('name, profile_image')->from('tbl_users')->where('id', $vCU['receiver_id'])->get()->row_array();
                    $cUsers[$kCU]['user_name'] = (!empty($userData) && $userData['name'] != '') ? $userData['name'] : 'N/A';
                    $cUsers[$kCU]['profile_image'] = $userData['profile_image'];
                    $cUsers[$kCU]['created_date'] = $vCU['created_at'];
                } else {
                    $cUsers[$kCU]['id'] = $vCU['sender_id'];
                    $userData = $this->db->select('name, profile_image')->from('tbl_users')->where('id', $vCU['sender_id'])->get()->row_array();
                    $cUsers[$kCU]['user_name'] = (!empty($userData) && $userData['name'] != '') ? $userData['name'] : 'N/A';
                    $cUsers[$kCU]['profile_image'] = $userData['profile_image'];
                    $cUsers[$kCU]['created_date'] = $vCU['created_at'];
                }
            }
        }
        return $cUsers;
    }*/
    

    public function get_chat_users_for_models($userId = 0, $search_keyword = '', $type = '')
    {
        $res = array();
        $cUsers = array();
        if($search_keyword != '') {
            $this->db->group_start();
            $this->db->like('o.name', $search_keyword);
            $this->db->or_like('t.name', $search_keyword);
            $this->db->group_end();
        }
        if($type != '') {
            $this->db->where('mt.type', $type);
            $this->db->group_start();
            $this->db->where('mt.chat_user_one', $userId);
            $this->db->or_where('mt.chat_user_two', $userId);
            $this->db->group_end();
        } else {
            $this->db->group_start();
            $this->db->where('mt.chat_user_one', $userId);
            $this->db->or_where('mt.chat_user_two', $userId);
            $this->db->group_end();
        }
        $res = $this->db->select('mt.id as thread_id, mt.chat_user_one as chat_user_one, mt.chat_user_two as chat_user_two, mt.type as type, mt.created_at as created_at, o.name as user_one_name, t.name as user_two_name, o.profile_image as user_one_image, t.profile_image as user_two_image')
                        ->from('tbl_message_threads as mt')
                        ->join('tbl_users as o', 'o.id = mt.chat_user_one')
                        ->join('tbl_users as t', 't.id = mt.chat_user_two')
                        ->order_by('mt.created_at', 'DESC')
                        ->get()->result_array();
        if(!empty($res)) {
            foreach($res as $kR => $vR) {
                if($vR['chat_user_one'] != $userId) {
                    $cUsers[$kR]['id'] = $vR['chat_user_one'];
                    $userData = $this->db->select('name, profile_image,type')->from('tbl_users')->where('id', $vR['chat_user_one'])->get()->row_array();
                    $cUsers[$kR]['user_name'] = (!empty($userData) && $userData['name'] != '') ? $userData['name'] : 'N/A';
                    $cUsers[$kR]['type'] = $userData['type'];
                    $cUsers[$kR]['profile_image'] = $userData['profile_image'];
                    $cUsers[$kR]['created_date'] = $vR['created_at'];
                    $cUsers[$kR]['thread_id'] = $vR['thread_id'];
                } else {
                    $cUsers[$kR]['id'] = $vR['chat_user_two'];
                    $userData = $this->db->select('name, profile_image,type')->from('tbl_users')->where('id', $vR['chat_user_two'])->get()->row_array();
                    $cUsers[$kR]['user_name'] = (!empty($userData) && $userData['name'] != '') ? $userData['name'] : 'N/A';
                    $cUsers[$kR]['type'] = $userData['type'];
                    $cUsers[$kR]['profile_image'] = $userData['profile_image'];
                    $cUsers[$kR]['created_date'] = $vR['created_at'];
                    $cUsers[$kR]['thread_id'] = $vR['thread_id'];
                }
            }
        }
        return $cUsers;
    }


    public function get_messages($model_follower_id = 0)
    {
        $res = $this->db->select('m.*, s.profile_image as profile_image')
            ->from('tbl_messages as m')
            ->join('tbl_users as s', 's.id = m.sender_id')
            ->where('m.model_follower_id', $model_follower_id)
            ->order_by('m.created_at', 'ASC')
            ->get()->result_array();
        return $res;
    }

    /*public function get_messages_of_model_chat($user_id = 0, $model_id = 0)
    {
        $res = $this->db->select('m.*, s.profile_image as profile_image')
                        ->from('tbl_messages as m')
                        ->join('tbl_users as s', 's.id = m.sender_id')
                        ->where('m.sender_id', $user_id)
                        ->where('m.receiver_id', $model_id)
                        ->or_group_start()
                                ->where('m.sender_id', $model_id)
                                ->where('m.receiver_id', $user_id)
                        ->group_end()
                        ->order_by('m.created_at', 'ASC')
                        ->get()->result_array();
        return $res;
    }*/
    
    public function get_messages_of_model_chat($thread_id = 0, $model_id = 0)
    {
        $res = $this->db->select('m.*, s.profile_image as profile_image')
                        ->from('tbl_messages as m')
                        ->join('tbl_users as s', 's.id = m.sender_id')
                        ->where('m.message_thread_id', $thread_id)
                        ->order_by('m.created_at', 'ASC')
                        ->get()->result_array();
        return $res;
    }

    public function getSuggestedModelsForFriendRequest($exceptModels = array())
    {
        $res = array();
        if (!empty($exceptModels)) {
            $res = $this->db->select('*')
                ->from('tbl_users')
                ->where('type', 'model')
                ->where('status', 'active')
                ->where_not_in('id', $exceptModels)
                ->order_by('id', 'DESC')
                ->get()->result_array();
        } else {
            $res = $this->db->select('*')
                ->from('tbl_users')
                ->where('type', 'model')
                ->where('status', 'active')
                ->order_by('id', 'DESC')
                ->get()->result_array();
        }
        return $res;
    }

    public function getSentPendingFriendRequest($sentModels = array())
    {
        $res = array();
        if (!empty($sentModels)) {
            $res = $this->db->select('m.*, fr.id as request_id, fr.created_at as sent_request_date')
                ->from('tbl_users as m')
                ->join('tbl_friend_requests as fr', 'fr.model_id = m.id')
                ->where('m.type', 'model')
                ->where('m.status', 'active')
                ->where_in('m.id', $sentModels)
                ->order_by('fr.created_at', 'DESC')
                ->get()->result_array();
        }
        return $res;
    }

    public function sent_request_to_models($userId = 0)
    {
        $res = array();
        if ($userId > 0) {
            $res = $this->db->select('model_id')
                ->from('tbl_friend_requests')
                ->where('status', 'active')
                ->where('user_id', $userId)
                ->group_start() // Open bracket
                ->where('request_status', 0)
                ->or_where('request_status', 1)
                ->group_end() // Close bracket
                ->get()->result_array();
        }
        return $res;
    }

    public function get_last_payment_of_model($user_id = 0)
    {
        $lastPayment = array();
        $pIDs = array();
        if ($user_id != 0) {
            $pIDs[] = $this->db->select('payment_id')
                ->from('tbl_model_followers')
                ->where('status', 'active')
                ->where('model_id', (int) $user_id)
                ->order_by('id', 'DESC')
                ->get()
                ->row_array();
            $pIDs[] = $this->db->select('payment_id')
                ->from('tbl_custom_video_requests')
                ->where('status', 'active')
                ->where('request_status', 1)
                ->where('model_id', (int) $user_id)
                ->order_by('id', 'DESC')
                ->get()
                ->row_array();
            $pIDs[] = $this->db->select('payment_id')
                ->from('tbl_videos as v')
                ->join('tbl_model_posts as mp', 'mp.id = v.post_id')
                ->where('v.status', 'active')
                ->where('v.category', '0')
                ->where('mp.model_id', (int) $user_id)
                ->order_by('v.id', 'DESC')
                ->get()
                ->row_array();
            $idArray = array();
            if (!empty($pIDs)) {
                foreach ($pIDs as $kPID => $vPID) {
                    $getID = $this->db->select('id')
                        ->from('tbl_payment')
                        ->where('payment_id', $vPID['payment_id'])
                        ->get()
                        ->row_array();
                    $idArray[] = $getID['id'];
                }
                $lastPaymentID = max($idArray);
                $lastPayment = $this->db->select('price')
                    ->from('tbl_payment')
                    ->where('id', $lastPaymentID)
                    ->get()
                    ->row_array();
            }
        }
        return $lastPayment;
    }

    public function get_lifetime_payment_of_model($user_id = 0)
    {
        $getTotalPayment = 0;
        if ($user_id != 0) {
            $modelFollwer = $this->db->select('payment_id')
                ->from('tbl_model_followers')
                //->where('status', 'active')
                ->where('model_id', (int) $user_id)
                ->order_by('id', 'DESC')
                ->get()
                ->result_array();
            $customVideo = $this->db->select('payment_id')
                ->from('tbl_custom_video_requests')
                ->where('status', 'active')
                ->where('request_status', 1)
                ->where('model_id', (int) $user_id)
                ->order_by('id', 'DESC')
                ->get()
                ->result_array();
            $privateVideo = $this->db->select('payment_id')
                ->from('tbl_videos as v')
                ->join('tbl_model_posts as mp', 'mp.id = v.post_id')
                ->where('v.status', 'active')
                ->where('v.category', '0')
                ->where('mp.model_id', (int) $user_id)
                ->order_by('v.id', 'DESC')
                ->get()
                ->result_array();
            $finalArray = array_merge($modelFollwer, $customVideo, $privateVideo);
            $getTotalPayment = $this->get_payment_amount($finalArray);
        }
        return $getTotalPayment;
    }

    public function get_payment_amount($paymentIds = array())
    {
        $totalPayment = 0;
        if (!empty($paymentIds)) {
            foreach ($paymentIds as $kPID => $vPID) {
                $price = $this->db->select('price')
                    ->from('tbl_payment')
                    ->where('payment_id', $vPID['payment_id'])
                    ->get()
                    ->row_array();
                $totalPayment += $price['price'];
            }
        }
        return $totalPayment;
    }

    public function upload_profile_image_for_model($file = array(), $user_id = 0, $folder = "models")
    {
        $update_data = array();
        $this->load->library('upload');
        $this->load->library('image_lib');
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/profile_pic/';

        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png|jpeg',
            'overwrite' => TRUE,
            'max_size' => '204800',
            'file_name' => md5(time() . rand())
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }
        if (isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name'])) {
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('profile_image')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/profile_pic/' . $upload_data['file_name'];
                $update_data['profile_image'] = $file_path;
                $update_data['updated'] = get_date();
            }
        }

        if (count($update_data)) {
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function upload_header_image_for_model($data = '', $user_id = 0, $folder = "models")
    {
        $update_data = array();
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/cover_pic/';
        if (!file_exists($path)) {
            @mkdir($path);
        }

        if (!empty($data)) {
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $imageName = md5(time() . rand()) . '.jpeg';
            //file_put_contents(FCPATH . SITE_UPD . $folder . '/' . $imageName, $data);
            file_put_contents($path . $imageName, $data);
            $update_data['cover_image'] = $user_id . '/cover_pic/' . $imageName;
            $update_data['updated'] = get_date();
            //return $imageName;
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
            return TRUE;
        } else {
            //return $data;
            return FALSE;
        }
    }

    public function upload_stream_image_for_model($data = '', $user_id = 0, $folder = "models")
    {
        $update_data = array();
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/stremer_pic/';
        if (!file_exists($path)) {
            @mkdir($path);
        }

        if (!empty($data)) {
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $imageName = md5(time() . rand()) . '.jpeg';
            //file_put_contents(FCPATH . SITE_UPD . $folder . '/' . $imageName, $data);
            file_put_contents($path . $imageName, $data);
            $update_data['streamer_image'] = $user_id . '/stremer_pic/' . $imageName;
            $update_data['updated'] = get_date();
            //return $imageName;
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
            return TRUE;
        } else {
            //return $data;
            return FALSE;
        }
    }

    public function create_notification($user_id, $message)
    {
        $insertdata = array('user_id' => $user_id, 'notification_message' => $message);
        $this->insert_data('tbl_notification', $insertdata);
    }
    
    public function upload_photo_id_for_model($file = array(), $user_id = 0, $folder = "models")
    {
        $update_data = array();
        $this->load->library('upload');
        $this->load->library('image_lib');
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/photo_id/';
        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png|jpeg|gif',
            'overwrite' => TRUE,
            'max_size' => '204800',
            'file_name' => md5(time() . rand())
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }
        if (isset($_FILES['document_photo_id']['name']) && !empty($_FILES['document_photo_id']['name'])) {
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('document_photo_id')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/photo_id/' . $upload_data['file_name'];
                $update_data['document_photo_id'] = $file_path;
                $update_data['updated'] = get_date();
            }
        }

        if (count($update_data)) {
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function upload_id_verification_selfie_for_model($file = array(), $user_id = 0, $folder = "models")
    {
        $update_data = array();
        $this->load->library('upload');
        $this->load->library('image_lib');
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/id_verification_selfie/';
        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png|jpeg|gif',
            'overwrite' => TRUE,
            'max_size' => '204800',
            'file_name' => md5(time() . rand())
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }
        if (isset($_FILES['document_id_verification_selfie']['name']) && !empty($_FILES['document_id_verification_selfie']['name'])) {
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('document_id_verification_selfie')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/id_verification_selfie/' . $upload_data['file_name'];
                $update_data['document_id_verification_selfie'] = $file_path;
                $update_data['updated'] = get_date();
            }
        }

        if (count($update_data)) {
            $this->update_data($update_data, 'tbl_users', 'id', $user_id);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_blocked_fans($model_id = 0) {
        $blocked_fans = array();
        if($model_id > 0) {
                $blocked_fans = $this->db->select('bf.id as block_id, bf.created_at as created_at, f.name as fan_name, f.profile_image as fan_image')
                                    ->from('tbl_blocked_fans as bf')
                                    ->join('tbl_users as f', 'f.id = bf.blocked_fan_id')
                                    ->where('bf.model_id', $model_id)
                                    ->order_by('bf.created_at', 'DESC')
                                    ->get()
                                    ->result_array();
        }
        return $blocked_fans;
    }

    public function upload_teaser_image($file = array(), $user_id = 0, $folder = "models")
    {
        $insertData = array();
        $this->load->library('upload');
        $this->load->library('image_lib');
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/teaser';
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/teaser/images/';
        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png|jpeg|gif',
            'overwrite' => TRUE,
            //'max_size' => '204800',
            'file_name' => md5(time() . rand())
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }
        if (isset($_FILES['teaser_image']['name']) && !empty($_FILES['teaser_image']['name'])) {
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('teaser_image')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/teaser/images/' . $upload_data['file_name'];
                $insertData['model_id'] = $user_id;
                $insertData['category'] = 1;
                $insertData['post_type'] = 0;
                $insertData['image'] = $file_path;
                $insertData['created'] = get_date();
            } else {
                $error = array('error' => $this->upload->display_errors());
                pre($error);
            }
        }

        if (count($insertData)) {
            $this->insert_data('tbl_model_posts', $insertData);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function upload_teaser_video($file = array(), $user_id = 0, $folder = "models")
    {
        $insertData = array();
        $this->load->library('upload');
        $this->load->library('image_lib');
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id;
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/teaser';
        if (!file_exists($path)) {
            @mkdir($path);
        }
        $path = FCPATH . SITE_UPD . $folder . '/' . $user_id . '/teaser/videos/';
        $file_config = array(
            'upload_path' => $path,
            'allowed_types' => 'mp4|mov|3gp',
            'overwrite' => TRUE,
            //'max_size' => '204800',
            'file_name' => md5(time() . rand())
        );
        if (!file_exists($path)) {
            @mkdir($path);
        }
        if (isset($_FILES['teaser_video']['name']) && !empty($_FILES['teaser_video']['name'])) {
            $this->upload->initialize($file_config);
            if ($this->upload->do_upload('teaser_video')) {
                $content['status'] = 200;
                $upload_data = $this->upload->data();
                $file_path = $user_id . '/teaser/videos/' . $upload_data['file_name'];
                $insertData['model_id'] = $user_id;
                $insertData['category'] = 1;
                $insertData['post_type'] = 1;
                $insertData['image'] = $file_path;
                $insertData['created'] = get_date();
            } else {
                $error = array('error' => $this->upload->display_errors());
                pre($error);
            }
        }

        if (count($insertData)) {
            $this->insert_data('tbl_model_posts', $insertData);
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function getFriendsForModels($model_id = 0) {
        $friends = array();
        if($model_id > 0) {
                $friends = $this->db->select('fr.id as friend_request_id, fr.updated_at as updated_at, m.name as friend_name, m.profile_image as friend_image')
                                    ->from('tbl_friend_requests as fr')
                                    ->join('tbl_users as m', 'm.id = fr.model_id')
                                    ->where('fr.user_id', $model_id)
                                    ->where('fr.request_status', 1)
                                    ->order_by('fr.updated_at', 'DESC')
                                    ->get()
                                    ->result_array();
        }
        return $friends;
    }
=======
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6

    function get_live_models()
    {
        $whrArr = array('status' => 'active', 'type' => 'model', 'live_status' => 1);
        return $this->db->select('u.*')
            ->from('tbl_users u')
            ->where($whrArr)
            ->get()->result_array();
    }

    function purchase($type = 0, $user_id, $price)
    {
        //pre($type);
        //type 0 for token purchase
        //type 1 for follow purchase
        // type 2 for snapchat purchase
        // type 3 for private video purchase
        // type 4 for custom video purchase
        $paymentId = genUniqueStr('Payment_', 13, 'tbl_payment', 'payment_id');
        $insertdata = array(
            'payment_id' => $paymentId,
            'status' => 'success',
            'user_id' => $user_id,
            'payment_for' => $type,
            'price' => $price,
            'created' => get_date()
        );
        //pre($insertdata);
        $this->db->insert('tbl_payment', $insertdata);
        //pre('dfadfas');
        $returndata = array();
        $returndata['payment_flag'] = 1;
        $returndata['status'] = $insertdata['status'];
        $returndata['payment_id'] = $insertdata['payment_id'];
        return $returndata;
    }

    function get_models_with_price($modelID)
    {
        $whrArr = array('m.id' => $modelID);
        $res = $this->db->select('m.*,me.rules,me.follow_price,me.snapchat_price')
            ->from('tbl_users m')
            ->join('tbl_model_extra me', 'me.model_id=m.id', 'left')
            ->where($whrArr)
            ->get()->row_array();

        return $res;
    }

    function check_follow_or_not($modelID, $user_id)
    {
        $whrArr = array('model_id' => $modelID, 'user_id' => $user_id);
        $res = $this->db->select('*')->from('tbl_model_followers')->where($whrArr)->get()->num_rows();
        // pre($res);
        return $res;
    }

    public function get_post_by_id($postID = 0)
    {
        $result = array();
        if ($postID) {
            $whrArr = array('p.id' => $postID);
            $result = $this->db->select('p.*,m.name,m.profile_image')->from('tbl_model_posts p')
                ->join('tbl_users m', 'm.id=p.model_id', 'left')
                ->where($whrArr)->get()->row_array();
        }
        //pre($this->db->last_query());
        return $result;
    }

    public function get_modelsData_byPostId($postId)
    {
        $whrArr = array('p.id' => $postId);
        $res = $this->db->select('m.*')
            ->from('tbl_model_posts p')
            ->join('tbl_users m', 'm.id=p.model_id', 'left')
            ->where($whrArr)
            ->get()->row_array();
        $res['total_images'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $res['id'], 'post_type' => 0));
        $res['total_videos'] = $this->common->get_table_count('tbl_model_posts', array('model_id' => $res['id'], 'post_type' => 1));

        $check = $this->db->select('*')->from('tbl_model_extra')->where('model_id', $res['id'])->get()->num_rows();
        if ($check > 0) {
            $res2 = $this->db->select('*')->from('tbl_model_extra')->where('model_id', $res['id'])->get()->row_array();
            $res['follow_price'] = $res2['follow_price'];
            $res['snapchat_price'] = $res2['snapchat_price'];
        } else {
            $res['follow_price'] = 0;
            $res['snapchat_price'] = 0;
        }
        return $res;
    }

    public function create_notification($user_id, $message)
    {
        $insertdata = array('user_id' => $user_id, 'notification_message' => $message);
        $this->insert_data('tbl_notification', $insertdata);
    }
}
