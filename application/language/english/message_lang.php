<<<<<<< HEAD
<?php

$lang["succ"] = "Success";
$lang["error"] = "Error";
$lang["err_not_auth_user"] = "You are not authenticate user, please contact admin";
$lang["err_something_went_wrong"] = "Something went wrong";
$lang["err_inactive"] = "Your account is not active.";
$lang["err_login"] = "Invalid username or password.";
$lang["err_email_not_registered"] = "Email not registered yet please check.";
$lang["err_email_not_sent"] = "Email could not send please try again";
$lang["err_invalid_token"] = "Invalid token";
$lang["err_email_already_exists"] = "Email already exists";
$lang["err_wrong_old_pwd"] = "Wrong old password";
$lang["err_pswd_req"] = "Please enter password";
$lang["err_con_pswd_req"] = "Conform Password required";
$lang["err_pswd_not_match"] = "Password not matched";
$lang["err_login_req"] = "Please login to continue.";
$lang["err_not_auth"] = "You are not authorised to access this page.";
$lang["err_rem_country"] = "Can't deactivate until campaign exists";
$lang["err_rem_campaign"] = "Can't delete until store or user assigned";
$lang["err_rem_campaign_user"] = "Can't delete until user created record exists";

$lang["err_rem_state"] = "Can't delete until store record exists";

$lang["err_rem_format"] = "Can't delete until store record exists";
$lang["err_rem_format_status"] = "Can't delete until format is active";

$lang["err_rem_chain"] = "Can't delete until store record exists";
$lang["err_rem_chain_status"] = "Can't delete until chain is active";

$lang["err_rem_banner"] = "Can't delete until store record exists";
$lang["err_rem_banner_status"] = "Can't delete until banner is active";

$lang["err_invalid_date"] = "Invalid date range";
$lang["err_no_data"] = "No data found";
$lang["err_no_language"] = "Language not found";
$lang["err_invalid_data"] = "Invalid csv data found";

$lang["succ_forgot_link_sent"] = "Reset password link sent successfully";
$lang["succ_pass_changed"] = "Password has been changed successfully";
$lang["succ_rec_added"] = "%s has been added successfully";
$lang["succ_rec_updated"] = "%s has been updated successfully";
$lang["succ_rec_deleted"] = "Record has been deleted successfully";
$lang["succ_status_changed"] = "Status has been changed successfully";
$lang["succ_campaign_archive"] = "Campaign successfully archived";
$lang["succ_campaign_unarchive"] = "Campaign successfully unarchived";
$lang["succ_data_import"] = "Data imported successfully";

$lang["succ_register"] = "Successfully registered, please check your email to verify account";
$lang["succ_register_business"] = "You are registered successfully, after admin verify your account you will be able for login.";
$lang["succ_acc_varified"] = "Account Successfully verified";

$lang["succ_profile_updated"] = "Profile updated successfully";
$lang["succ_message"] = "Message has been submited Successfully";

$lang['make_change_to_save'] = "Please make any change in form to save.";
$lang['suc_profile_upd'] = "Profile updated successfully.";
$lang['err_rmv_promo_used'] = "Can not remove promo because it is used some user.";
/*
 * Common Label
 */
$lang['lbl_lng_eng'] = 'English';
$lang['lbl_language'] = 'Language';
$lang['lbl_admin_home'] = 'Home';
$lang['lbl_edit'] = 'Edit';
$lang['lbl_add'] = 'Add';
$lang['lbl_export'] = 'Export';
$lang['lbl_new'] = 'New';
$lang['lbl_view'] = 'View';
$lang['lbl_delete'] = 'Delete';
$lang['lbl_new'] = 'New';
$lang['email'] = 'Email';
$lang['lbl_csv'] = 'CSV';
$lang['lbl_pdf'] = 'PDF';
$lang['lbl_excel'] = 'Excel';
$lang['lbl_accept'] = 'Accept';
$lang['lbl_reject'] = 'Reject';

$lang['lbl_save'] = 'Save';
$lang['lbl_payment'] = 'Payment';
$lang['password'] = 'Password';
$lang['con_password'] = 'Confirm Password';

$lang['old_password'] = 'Old Password';
$lang['new_password'] = 'New Password';
$lang['comfirm_password'] = 'Confirm Password';
$lang['first_name'] = 'First Name';
$lang['lbl_name'] = 'Name';
$lang['lbl_username'] = 'Username';
$lang['last_name'] = 'Last Name';

$lang['lbl_orders'] = 'Orders';
$lang['lbl_submit'] = 'Submit';
$lang['lbl_cancel'] = 'Cancel';
$lang['lbl_profile'] = 'Profile';
$lang['lbl_logout'] = 'Logout';
$lang['lbl_add'] = 'Add';
$lang['lbl_delete_sel'] = 'Delete Selected';
$lang['lbl_sr_no'] = 'Sr. No.';
$lang['lbl_operation'] = 'Operation';
$lang['lbl_login'] = 'Login';
$lang['lbl_send'] = 'Send';
$lang['lbl_back'] = 'Back';
$lang['lbl_close'] = 'Close';
$lang['lbl_information'] = 'Information';
$lang['lbl_warning_change_status'] = 'Are you sure you want to change status?';
$lang['lbl_warning_delete'] = 'Are you sure you want to delete this?';
$lang['lbl_warning_plz_sel_at_one'] = 'please select at-least one record';
$lang['lbl_warning_delete_all'] = 'Are you sure you want to delete selected records?';
$lang['lbl_help_block_image'] = 'Please upload at least 400 * 640 size and type jpeg,jpg,png images.';
$lang['lbl_help_block_image1'] = 'Please upload at least 640 * 400 size and type jpeg,jpg,png,gif images.';

/*
 * Login Module
 */

$lang['login_header_title'] = 'Login';
$lang['lbl_register'] = 'Register';
$lang['reset_pass_header_title'] = 'Reset Password';
$lang['login_email'] = 'Login';
$lang['lbl_header_reset_pass'] = "Enter new password and confirm password to reset";
$lang['lbl_header_login_form'] = "Login";
$lang['lbl_header_forgot_pass'] = "Forget Password";
$lang['lbl_par_forgot_pass'] = "Enter your e-mail address below to reset your password";
$lang['lbl_forgot_place_email'] = "Enter Email";
$lang['lbl_login_place_email'] = "Enter Email";
$lang['lbl_login_place_username'] = "Enter Username";
$lang['lbl_login_place_Password'] = "Enter Password";
$lang['lbl_register_place_password'] = "Enter Password";
$lang['lbl_register_place_name'] = "Enter Name";
$lang['lbl_register_place_email'] = "Enter Email";
$lang['lbl_register_place_mobile'] = "Enter Mobile";
$lang['lbl_register_place_username'] = "Enter Username";
$lang['lbl_register_place_address'] = "Enter Address";
$lang['lbl_reset_pass_place_new_pass'] = "Enter New Password";
$lang['lbl_reset_pass_place_confirm_pass'] = "Enter Confirm Password";
$lang['err_reset_pass_new_pass_req'] = "Password is required";
$lang['err_reset_pass_confirm_pass_req'] = "Confirm password is required";
$lang['err_reset_pass_confirm_not_match'] = "Confirm password must match";
$lang['lbl_forgot_pass_link'] = "Forgot Password?";
$lang['lbl_remember_me'] = "Remember me";

/*
 * Profile Module
 */

$lang['profile_header_title'] = 'Edit Profile';
$lang['succ_profile_rm_pro_pic'] = 'remove image successfully';
$lang['labl_frm_pro_pic'] = 'Profile Picture';
$lang['placeholder_ent_name'] = 'Enter Name';
$lang['placeholder_ent_fname'] = 'Enter First Name';
$lang['placeholder_ent_lname'] = 'Enter Last Name';
$lang['placeholder_ent_mobile'] = 'Enter Mobile';
$lang['placeholder_ent_email'] = 'Enter Email';
$lang['placeholder_ent_username'] = 'Enter Username';
$lang['bootbox_rm_img_confirm'] = 'Are you sure you want to remove image?';
$lang['err_profile_enter_name'] = 'Please enter name';
$lang['err_profile_enter_fname'] = 'Please enter first name';
$lang['err_profile_enter_lname'] = 'Please enter last name';
$lang['err_profile_enter_mobile'] = 'Please enter mobile';
$lang['err_profile_enter_email'] = 'Please enter email address';
$lang['err_profile_enter_username'] = 'Please enter username';
$lang['err_profile_enter_valid_email'] = 'Please enter valid email address';

/*
 * general settings module
 */

$lang['general_set_header_title'] = 'General settings';
$lang['change_pass_header_title'] = 'Change Password';
$lang['site_set_header_title'] = 'Site Settings';
$lang['err_change_pass_old_pass_req'] = 'Old password is required';
$lang['err_change_pass_old_pass_not_correct'] = 'Old password is not correct';
$lang['err_change_pass_new_pass_req'] = 'New password is required';
$lang['err_change_pass_at_least_6_req'] = 'At least 6 characters';
$lang['err_change_pass_used_cur_plz_change'] = 'This password is used currently please provide different';
$lang['err_change_pass_not_valid'] = 'Password must contain atleast 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character';
$lang['err_change_pass_should_same'] = 'Password should be same';
$lang['err_change_pass_conf_req'] = 'Confirm password is required';
$lang['getting_started_header_title'] = 'Getting Started';
$lang['make_money_header_title'] = 'How to Make Money';

/*
 * Dashboard module
 */
$lang['dashboard_header_title'] = 'Admin Panel';
$lang['vendor_dashboard_header_title'] = 'Vendor Panel';
$lang['dashboard_total_user'] = 'Total Users';
$lang['dashboard_users'] = 'Users';
$lang['dashboard_subcategory'] = 'Sub Categories';
$lang['dashboard_greengrocers'] = 'Greengrocers';

/*
 * User Module
 */
$lang["lbl_user_list"] = "User List";
$lang["lbl_user_details"] = "User Details";
$lang["lbl_user_manage"] = "Manage Users";
$lang['succ_rm_user_pic'] = 'remove image successfully';
$lang['err_user_email_already_reg'] = '%s already registered.';
$lang['lbl_mobile_no'] = 'Mobile';
$lang['user_type'] = 'User Type';
$lang['user_gender'] = 'Gender';
$lang['user_birthdate'] = 'Birthdate';
$lang['user_lbl_img'] = 'Image';
$lang['user_place_enter_fname'] = 'Enter First Name';
$lang['user_place_enter_lname'] = 'Enter Last Name';
$lang['user_place_enter_email'] = 'Enter Email';
$lang['user_place_enter_pass'] = 'Enter Password';
$lang['user_place_enter_mobile_num'] = 'Enter Mobile Number';
$lang['user_place_enter_username'] = 'Enter Username';
$lang['user_bootbox_rm_img_alert'] = 'Are you sure you want to remove image?';

$lang['user_err_first_name_req'] = 'Please enter first name';
$lang['user_err_last_name_req'] = 'Please enter last name';
$lang['user_err_email_req'] = 'Please enter email address';
$lang['user_err_username_req'] = 'Please enter username';
$lang['user_err_mobile_req'] = 'Please enter mobile';
$lang['user_err_email_valid'] = 'Please enter a valid email address.';
$lang['user_err_email_already_exist'] = 'Email is already exist';
$lang['user_err_mobile_req'] = 'Please enter mobile number';
$lang['user_err_username_req'] = 'Please enter username';
$lang['user_err_mobile_already_exist'] = 'Mobile number is already exist';
$lang['err_profile_nospace_username'] = 'No space please allowed';
$lang['err_username_exist'] = 'Username already exist';
$lang['lbl_total_orders'] = 'Total Orders';
$lang['lbl_address'] = 'Address';


$lang['err_name_req'] = 'Please enter name';

/*
* Models module
*/
$lang["lbl_models_list"] = "Model List";
$lang["lbl_models_details"] = "Model Details";
$lang["lbl_models_manage"] = "Manage Models";
$lang["lbl_model"] = "Model";



//new here
$lang['mobile_no'] = 'Mobile';
$lang['lbl_user'] = 'User';
$lang['plc_pass'] = 'Please Enter Password';
$lang['plc_mobile'] = 'Please Enter Mobile';
$lang['error_user_pass'] = 'Please Enter Password';
$lang['lbl_view_user'] = 'View Users';
$lang['err_number_taken'] = 'This Mobile Number is Already Taken';






//Content Module
$lang['lbl_list_content'] = 'Manage Content';
$lang['lbl_Content'] = 'Content';
$lang['succ_updated_Content'] = 'Content Updated Successfully';


//Major lang here
$lang['lbl_id'] = 'Id';
$lang['lbl_vendor_name'] = 'Vendor Name';
$lang['lbl_logo'] = 'Logo';
$lang['lbl_image'] = 'Image';


$lang['placeholder_ent_car_name'] = 'Please Enter Car Name';

$lang['lbl_status'] = 'Status';
$lang['lbl_action'] = 'Actions';

$lang['lbl_yes'] = 'Yes';
$lang['lbl_no'] = 'No';
$lang['lbl_limited'] = 'Limited';
$lang['lbl_unlimited'] = 'Unlimited';
$lang['lbl_title'] = 'Title';
$lang['placeholder_title'] = 'Please Enter Title';


// for front

$lang['lbl_title_become_model'] = "Become a model";
$lang['lbl_title_model_details'] = "Model details";
$lang['lbl_title_shop_details'] = "Shop details";
$lang['lbl_title_shop'] = "Shop";
$lang['lbl_title_home'] = "Home";
$lang['lbl_title_faq'] = "Faq";
$lang['lbl_register'] = "Register";
$lang['lbl_title_dashboard'] = "Dashboard";


/* Login Module */
$lang['err_email_required'] = "Email is required";
$lang['err_pwd_required'] = "Password is required";
$lang['err_confirm_pwd_required'] = "Confirm password is required";
$lang['err_pswrd_same_cnfrm_pswrd'] = "Password and confirm password does not match";
$lang['cnfrm_pswrd_same_err_pswrd'] = "Confirm password and password does not match";
$lang['err_pwd_maxlength'] = "Password is not valid more than 16 characters";
$lang['err_pwd_minlength'] = "Password is not valid minimum than 6 characters";
$lang['err_email_valid_type'] = "Email is not in valid format";
$lang['lbl_email'] = "Email";
$lang['lbl_password'] = "Password";
$lang['err_login'] = "Please enter a valid login";
$lang['err_inactive'] = "Your account is inactive";
$lang['succ_login'] = "Login successfully";
$lang['succ_logout'] = "Logout successfully";
$lang['lbl_login'] = "Login";
$lang['lbl_forgot_password'] = "Forgot Password";
$lang['lbl_new_user'] = "New user";
$lang['lbl_join_now'] = "Join Now";


/* Register Module */
$lang['lbl_confirm_email'] = "Confirm Email";
$lang['err_confirm_email_required'] = "Confirm Email is required";
$lang['err_email_confirmEmail_same'] = "Email and Confirm-Email must be same";
$lang['succ_signup'] = "Signup successfully";
$lang['email_in_use'] = "An account with that email already exists";
$lang['err_name_required'] = "Username is required";
$lang['err_model_name_required'] = "Modelname is required";
$lang['lbl_user_name'] = "User Name";
$lang['lbl_model_name'] = "Your Model Name";
$lang['err_age_confirmation'] = "Age confirmation is required";


/* Home Module */
$lang['lbl_follow'] = "Follow";
$lang['lbl_snapchat'] = "Snapchat";

/* Buy Tokens */
$lang['lbl_buy_tokens'] = "Buy Tokens";

/* Password module */
$lang['placeholder_new_password'] = "New Password";
$lang['placeholder_confirm_password'] = "Confirm Password";
$lang['err_old_password'] = "Old password is worng. Please try again";

/* Settings module */
$lang['placeholder_name'] = "Name";
$lang['placeholder_bio'] = "Bio";
$lang['lbl_settings'] = "Settings";

/* Explore module */
$lang['lbl_title_model_explore'] = "Explore";

/* Follows Module*/
$lang['lbl_follows'] = "Follows";

/* Favourties Module*/
$lang['lbl_fav'] = "Favourites";

/* Notification Module*/
$lang['lbl_notification'] = "Notification";
$lang['notification_read_succ'] = "Notification read successfully";
$lang['notification_dele_succ'] = "Notification delete successfully";
$lang['notification_select_err'] = "Please select notification";
$lang['lbl_title_premium_video'] = "Premium video";
$lang['lbl_premium_content'] = "Premium-Content";
$lang['lbl_title_snap_models'] = "Snap-Models";
$lang['lbl_title_freindfeed'] = "FreindFeed";

/* Payment Module */
$lang['lbl_title_payments'] = "Payments";

/* CMS Pages */
$lang['lbl_usc'] = "18 U.S.C 2257";
$lang['lbl_copyright'] = "Copyright";
$lang['lbl_privacypolicy'] = "Privacy Policy";
$lang['lbl_california_privacypolicy'] = "California Privacy Policy";
$lang['lbl_terms_and_conditions'] = "Terms and Conditions";

/* Admin Payment Module */
$lang['lbl_follower_purchase_manage'] = "Manage Follower Purchase";
$lang['lbl_view_follower_purchase'] = "View Follower Purchase";

$lang['lbl_custom_video_purchase_manage'] = "Manage Custom Video Purchase";
$lang['lbl_view_custom_video_purchase'] = "View Custom Video Purchase";

$lang['lbl_premium_video_purchase_manage'] = "Manage Premium Video Purchase";
$lang['lbl_view_premium_video_purchase'] = "View Premium Video Purchase";

$lang['lbl_token_purchase_manage'] = "Manage Token Purchase";
$lang['lbl_view_token_purchase'] = "View Token Purchase";

$lang['lbl_getting_started'] = 'How to Get Started';
$lang['lbl_make_money'] = 'How to Make Money';
$lang['lbl_edit_profile'] = 'Edit Profile';


/*
* Geo Blocking module
*/
$lang["lbl_geo_blocking_list"] = "Geo Blocking List";
$lang["lbl_geo_blocking_details"] = "Geo Blocking Details";
$lang["lbl_geo_blocking_manage"] = "Manage Geo Blocking";
$lang["lbl_geo_blocking"] = "Geo Blocking";

/*
* Post Images module
*/
$lang["lbl_post_images_list"] = "Post Images List";
$lang["lbl_post_images_details"] = "Post Images Details";
$lang["lbl_post_images_manage"] = "Manage Post Images";
$lang["lbl_post_images"] = "Post Images";

$lang["succ_rec_approved"] = "Record has been approved successfully";

/*
* Post Videos module
*/
$lang["lbl_post_videos_list"] = "Post Videos List";
$lang["lbl_post_videos_details"] = "Post Videos Details";
$lang["lbl_post_videos_manage"] = "Manage Post Videos";
$lang["lbl_post_videos"] = "Post Videos";

/*
* Pending Approval module
*/
$lang["lbl_pa_list"] = "Pending Approval List";
$lang["lbl_pa_details"] = "Pending Approval Details";
$lang["lbl_pa_manage"] = "Manage Pending Approvals";
$lang["lbl_pa"] = "Pending Approvals";
=======
<?php

$lang["succ"] = "Success";
$lang["error"] = "Error";
$lang["err_not_auth_user"] = "You are not authenticate user, please contact admin";
$lang["err_something_went_wrong"] = "Something went wrong";
$lang["err_inactive"] = "Your account is not active.";
$lang["err_login"] = "Invalid username or password.";
$lang["err_email_not_registered"] = "Email not registered yet please check.";
$lang["err_email_not_sent"] = "Email could not send please try again";
$lang["err_invalid_token"] = "Invalid token";
$lang["err_email_already_exists"] = "Email already exists";
$lang["err_wrong_old_pwd"] = "Wrong old password";
$lang["err_pswd_req"] = "Please enter password";
$lang["err_con_pswd_req"] = "Conform Password required";
$lang["err_pswd_not_match"] = "Password not matched";
$lang["err_login_req"] = "Please login to continue.";
$lang["err_not_auth"] = "You are not authorised to access this page.";
$lang["err_rem_country"] = "Can't deactivate until campaign exists";
$lang["err_rem_campaign"] = "Can't delete until store or user assigned";
$lang["err_rem_campaign_user"] = "Can't delete until user created record exists";

$lang["err_rem_state"] = "Can't delete until store record exists";

$lang["err_rem_format"] = "Can't delete until store record exists";
$lang["err_rem_format_status"] = "Can't delete until format is active";

$lang["err_rem_chain"] = "Can't delete until store record exists";
$lang["err_rem_chain_status"] = "Can't delete until chain is active";

$lang["err_rem_banner"] = "Can't delete until store record exists";
$lang["err_rem_banner_status"] = "Can't delete until banner is active";

$lang["err_invalid_date"] = "Invalid date range";
$lang["err_no_data"] = "No data found";
$lang["err_no_language"] = "Language not found";
$lang["err_invalid_data"] = "Invalid csv data found";

$lang["succ_forgot_link_sent"] = "Reset password link sent successfully";
$lang["succ_pass_changed"] = "Password has been changed successfully";
$lang["succ_rec_added"] = "%s has been added successfully";
$lang["succ_rec_updated"] = "%s has been updated successfully";
$lang["succ_rec_deleted"] = "Record has been deleted successfully";
$lang["succ_status_changed"] = "Status has been changed successfully";
$lang["succ_campaign_archive"] = "Campaign successfully archived";
$lang["succ_campaign_unarchive"] = "Campaign successfully unarchived";
$lang["succ_data_import"] = "Data imported successfully";

$lang["succ_register"] = "Successfully registered, please check your email to verify account";
$lang["succ_register_business"] = "You are registered successfully, after admin verify your account you will be able for login.";
$lang["succ_acc_varified"] = "Account Successfully verified";

$lang["succ_profile_updated"] = "Profile updated successfully";
$lang["succ_message"] = "Message has been submited Successfully";

$lang['make_change_to_save'] = "Please make any change in form to save.";
$lang['suc_profile_upd'] = "Profile updated successfully.";
$lang['err_rmv_promo_used'] = "Can not remove promo because it is used some user.";
/*
 * Common Label
 */
$lang['lbl_lng_eng'] = 'English';
$lang['lbl_language'] = 'Language';
$lang['lbl_admin_home'] = 'Home';
$lang['lbl_edit'] = 'Edit';
$lang['lbl_add'] = 'Add';
$lang['lbl_export'] = 'Export';
$lang['lbl_new'] = 'New';
$lang['lbl_view'] = 'View';
$lang['lbl_delete'] = 'Delete';
$lang['lbl_new'] = 'New';
$lang['email'] = 'Email';
$lang['lbl_csv'] = 'CSV';
$lang['lbl_pdf'] = 'PDF';
$lang['lbl_excel'] = 'Excel';
$lang['lbl_accept'] = 'Accept';
$lang['lbl_reject'] = 'Reject';

$lang['lbl_save'] = 'Save';
$lang['lbl_payment'] = 'Payment';
$lang['password'] = 'Password';
$lang['con_password'] = 'Confirm Password';

$lang['old_password'] = 'Old Password';
$lang['new_password'] = 'New Password';
$lang['comfirm_password'] = 'Confirm Password';
$lang['first_name'] = 'First Name';
$lang['lbl_name'] = 'Name';
$lang['lbl_username'] = 'Username';
$lang['last_name'] = 'Last Name';

$lang['lbl_orders'] = 'Orders';
$lang['lbl_submit'] = 'Submit';
$lang['lbl_cancel'] = 'Cancel';
$lang['lbl_profile'] = 'Profile';
$lang['lbl_logout'] = 'Logout';
$lang['lbl_add'] = 'Add';
$lang['lbl_delete_sel'] = 'Delete Selected';
$lang['lbl_sr_no'] = 'Sr. No.';
$lang['lbl_operation'] = 'Operation';
$lang['lbl_login'] = 'Login';
$lang['lbl_send'] = 'Send';
$lang['lbl_back'] = 'Back';
$lang['lbl_close'] = 'Close';
$lang['lbl_information'] = 'Information';
$lang['lbl_warning_change_status'] = 'Are you sure you want to change status?';
$lang['lbl_warning_delete'] = 'Are you sure you want to delete this?';
$lang['lbl_warning_plz_sel_at_one'] = 'please select at-least one record';
$lang['lbl_warning_delete_all'] = 'Are you sure you want to delete selected records?';
$lang['lbl_help_block_image'] = 'Please upload at least 400 * 640 size and type jpeg,jpg,png images.';
$lang['lbl_help_block_image1'] = 'Please upload at least 640 * 400 size and type jpeg,jpg,png,gif images.';

/*
 * Login Module
 */

$lang['login_header_title'] = 'Login';
$lang['lbl_register'] = 'Register';
$lang['reset_pass_header_title'] = 'Reset Password';
$lang['login_email'] = 'Login';
$lang['lbl_header_reset_pass'] = "Enter new password and confirm password to reset";
$lang['lbl_header_login_form'] = "Login";
$lang['lbl_header_forgot_pass'] = "Forget Password";
$lang['lbl_par_forgot_pass'] = "Enter your e-mail address below to reset your password";
$lang['lbl_forgot_place_email'] = "Enter Email";
$lang['lbl_login_place_email'] = "Enter Email";
$lang['lbl_login_place_username'] = "Enter Username";
$lang['lbl_login_place_Password'] = "Enter Password";
$lang['lbl_register_place_password'] = "Enter Password";
$lang['lbl_register_place_name'] = "Enter Name";
$lang['lbl_register_place_email'] = "Enter Email";
$lang['lbl_register_place_mobile'] = "Enter Mobile";
$lang['lbl_register_place_username'] = "Enter Username";
$lang['lbl_register_place_address'] = "Enter Address";
$lang['lbl_reset_pass_place_new_pass'] = "Enter New Password";
$lang['lbl_reset_pass_place_confirm_pass'] = "Enter Confirm Password";
$lang['err_reset_pass_new_pass_req'] = "Password is required";
$lang['err_reset_pass_confirm_pass_req'] = "Confirm password is required";
$lang['err_reset_pass_confirm_not_match'] = "Confirm password must match";
$lang['lbl_forgot_pass_link'] = "Forgot Password?";
$lang['lbl_remember_me'] = "Remember me";

/*
 * Profile Module
 */

$lang['profile_header_title'] = 'Edit Profile';
$lang['succ_profile_rm_pro_pic'] = 'remove image successfully';
$lang['labl_frm_pro_pic'] = 'Profile Picture';
$lang['placeholder_ent_name'] = 'Enter Name';
$lang['placeholder_ent_fname'] = 'Enter First Name';
$lang['placeholder_ent_lname'] = 'Enter Last Name';
$lang['placeholder_ent_mobile'] = 'Enter Mobile';
$lang['placeholder_ent_email'] = 'Enter Email';
$lang['placeholder_ent_username'] = 'Enter Username';
$lang['bootbox_rm_img_confirm'] = 'Are you sure you want to remove image?';
$lang['err_profile_enter_name'] = 'Please enter name';
$lang['err_profile_enter_fname'] = 'Please enter first name';
$lang['err_profile_enter_lname'] = 'Please enter last name';
$lang['err_profile_enter_mobile'] = 'Please enter mobile';
$lang['err_profile_enter_email'] = 'Please enter email address';
$lang['err_profile_enter_username'] = 'Please enter username';
$lang['err_profile_enter_valid_email'] = 'Please enter valid email address';

/*
 * general settings module
 */

$lang['general_set_header_title'] = 'General settings';
$lang['change_pass_header_title'] = 'Change Password';
$lang['site_set_header_title'] = 'Site Settings';
$lang['err_change_pass_old_pass_req'] = 'Old password is required';
$lang['err_change_pass_old_pass_not_correct'] = 'Old password is not correct';
$lang['err_change_pass_new_pass_req'] = 'New password is required';
$lang['err_change_pass_at_least_6_req'] = 'At least 6 characters';
$lang['err_change_pass_used_cur_plz_change'] = 'This password is used currently please provide different';
$lang['err_change_pass_not_valid'] = 'Password must contain atleast 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character';
$lang['err_change_pass_should_same'] = 'Password should be same';
$lang['err_change_pass_conf_req'] = 'Confirm password is required';

/*
 * Dashboard module
 */
$lang['dashboard_header_title'] = 'Admin Panel';
$lang['vendor_dashboard_header_title'] = 'Vendor Panel';
$lang['dashboard_total_user'] = 'Total Users';
$lang['dashboard_users'] = 'Users';
$lang['dashboard_subcategory'] = 'Sub Categories';
$lang['dashboard_greengrocers'] = 'Greengrocers';

/*
 * User Module
 */
$lang["lbl_user_list"] = "User List";
$lang["lbl_user_details"] = "User Details";
$lang["lbl_user_manage"] = "Manage Users";
$lang['succ_rm_user_pic'] = 'remove image successfully';
$lang['err_user_email_already_reg'] = '%s already registered.';
$lang['lbl_mobile_no'] = 'Mobile';
$lang['user_type'] = 'User Type';
$lang['user_gender'] = 'Gender';
$lang['user_birthdate'] = 'Birthdate';
$lang['user_lbl_img'] = 'Image';
$lang['user_place_enter_fname'] = 'Enter First Name';
$lang['user_place_enter_lname'] = 'Enter Last Name';
$lang['user_place_enter_email'] = 'Enter Email';
$lang['user_place_enter_pass'] = 'Enter Password';
$lang['user_place_enter_mobile_num'] = 'Enter Mobile Number';
$lang['user_place_enter_username'] = 'Enter Username';
$lang['user_bootbox_rm_img_alert'] = 'Are you sure you want to remove image?';

$lang['user_err_first_name_req'] = 'Please enter first name';
$lang['user_err_last_name_req'] = 'Please enter last name';
$lang['user_err_email_req'] = 'Please enter email address';
$lang['user_err_username_req'] = 'Please enter username';
$lang['user_err_mobile_req'] = 'Please enter mobile';
$lang['user_err_email_valid'] = 'Please enter a valid email address.';
$lang['user_err_email_already_exist'] = 'Email is already exist';
$lang['user_err_mobile_req'] = 'Please enter mobile number';
$lang['user_err_username_req'] = 'Please enter username';
$lang['user_err_mobile_already_exist'] = 'Mobile number is already exist';
$lang['err_profile_nospace_username'] = 'No space please allowed';
$lang['err_username_exist'] = 'Username already exist';
$lang['lbl_total_orders'] = 'Total Orders';
$lang['lbl_address'] = 'Address';


$lang['err_name_req'] = 'Please enter name';

/*
* Models module
*/
$lang["lbl_models_list"] = "Model List";
$lang["lbl_models_details"] = "Model Details";
$lang["lbl_models_manage"] = "Manage Models";
$lang["lbl_model"] = "Model";



//new here
$lang['mobile_no'] = 'Mobile';
$lang['lbl_user'] = 'User';
$lang['plc_pass'] = 'Please Enter Password';
$lang['plc_mobile'] = 'Please Enter Mobile';
$lang['error_user_pass'] = 'Please Enter Password';
$lang['lbl_view_user'] = 'View Users';
$lang['err_number_taken'] = 'This Mobile Number is Already Taken';






//Content Module
$lang['lbl_list_content'] = 'Manage Content';
$lang['lbl_Content'] = 'Content';
$lang['succ_updated_Content'] = 'Content Updated Successfully';


//Major lang here
$lang['lbl_id'] = 'Id';
$lang['lbl_vendor_name'] = 'Vendor Name';
$lang['lbl_logo'] = 'Logo';
$lang['lbl_image'] = 'Image';


$lang['placeholder_ent_car_name'] = 'Please Enter Car Name';

$lang['lbl_status'] = 'Status';
$lang['lbl_action'] = 'Actions';

$lang['lbl_yes'] = 'Yes';
$lang['lbl_no'] = 'No';
$lang['lbl_limited'] = 'Limited';
$lang['lbl_unlimited'] = 'Unlimited';
$lang['lbl_title'] = 'Title';
$lang['placeholder_title'] = 'Please Enter Title';


// for front

$lang['lbl_title_become_model'] = "Become a model";
$lang['lbl_title_model_details'] = "Model details";
$lang['lbl_title_shop_details'] = "Shop details";
$lang['lbl_title_shop'] = "Shop";
$lang['lbl_title_home'] = "Home";
$lang['lbl_title_faq'] = "Faq";
$lang['lbl_register'] = "Register";
$lang['lbl_title_dashboard'] = "Dashboard";


/* Login Module */
$lang['err_email_required'] = "Email is required";
$lang['err_pwd_required'] = "Password is required";
$lang['err_confirm_pwd_required'] = "Confirm password is required";
$lang['err_pswrd_same_cnfrm_pswrd'] = "Password and confirm password does not match";
$lang['cnfrm_pswrd_same_err_pswrd'] = "Confirm password and password does not match";
$lang['err_pwd_maxlength'] = "Password is not valid more than 16 characters";
$lang['err_pwd_minlength'] = "Password is not valid minimum than 6 characters";
$lang['err_email_valid_type'] = "Email is not in valid format";
$lang['lbl_email'] = "Email";
$lang['lbl_password'] = "Password";
$lang['err_login'] = "Please enter a valid login";
$lang['err_inactive'] = "Your account is inactive";
$lang['succ_login'] = "Login successfully";
$lang['succ_logout'] = "Logout successfully";
$lang['lbl_login'] = "Login";
$lang['lbl_forgot_password'] = "Forgot Password";
$lang['lbl_new_user'] = "New user";
$lang['lbl_join_now'] = "Join Now";


/* Register Module */
$lang['lbl_confirm_email'] = "Confirm Email";
$lang['err_confirm_email_required'] = "Confirm Email is required";
$lang['err_email_confirmEmail_same'] = "Email and Confirm-Email must be same";
$lang['succ_signup'] = "Signup successfully";
$lang['email_in_use'] = "An account with that email already exists";
$lang['err_name_required'] = "Username is required";
$lang['err_model_name_required'] = "Modelname is required";
$lang['lbl_user_name'] = "User Name";
$lang['lbl_model_name'] = "Your Model Name";
$lang['err_age_confirmation'] = "Age confirmation is required";


/* Home Module */
$lang['lbl_follow'] = "Follow";
$lang['lbl_snapchat'] = "Snapchat";

/* Buy Tokens */
$lang['lbl_buy_tokens'] = "Buy Tokens";

/* Password module */
$lang['placeholder_new_password'] = "New Password";
$lang['placeholder_confirm_password'] = "Confirm Password";
$lang['err_old_password'] = "Old password is worng. Please try again";

/* Settings module */
$lang['placeholder_name'] = "Name";
$lang['placeholder_bio'] = "Bio";
$lang['lbl_settings'] = "Settings";

/* Explore module */
$lang['lbl_title_model_explore'] = "Explore";

/* Follows Module*/
$lang['lbl_follows'] = "Follows";

/* Favourties Module*/
$lang['lbl_fav'] = "Favourites";

/* Notification Module*/
$lang['lbl_notification'] = "Notification";
$lang['notification_read_succ'] = "Notification read successfully";
$lang['notification_dele_succ'] = "Notification delete successfully";
$lang['notification_select_err'] = "Please select notification";
$lang['lbl_title_premium_video'] = "Premium video";
$lang['lbl_premium_content'] = "Premium-Content";
$lang['lbl_title_snap_models'] = "Snap-Models";
$lang['lbl_title_freindfeed'] = "FreindFeed";
>>>>>>> 64b1a2a35410f885ddcdf3d04575c7be101847d6
