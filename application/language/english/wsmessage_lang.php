<?php

//success
$lang['suc_number_change'] = "Mobile Number Updated successfully";


//error
$lang['err_number_change'] = "Mobile Number is already taken, please try another one.";
$lang['err_user_exists_new'] = "User Name is already taken, please try another one.";
$lang['err_user_not_active_new'] = "Your Account has been Deleted";
$lang['err_admin_user_not_active_new'] = "Your Account has been Deactivated By Administrator";
$lang['err_password'] = "Your Account has been Deactivated By Administrator";


//post
$lang['suc_post_created'] = "Post Created successfully";
$lang['suc_post_updated'] = "Post Updated successfully";
$lang['del_post'] = "Post Deleted successfully";
$lang['inv_post'] = "This Post Is Unavailable To View";


//old codes
$lang['err_something_went_wrong'] = "Something went wrong";
$lang['err_invalid_token'] = "Invalid token";
$lang['err_invalid_device'] = "This device is not allowed";
$lang['err_method_not_allowed'] = "Requested http method not allowed";
$lang['err_user_exists'] = "User already exists";
$lang['err_user_not_exist'] = "User not exist";
$lang['err_driver_not_exist'] = "Driver not exist";
$lang['err_user_not_exists'] = "Invalid Mobile Number or password";
$lang['err_driver_not_exists'] = "Invalid Driver ID or password";
$lang['err_user_not_active'] = "User not active";
$lang['err_driver_not_active'] = "Driver not active";
$lang['err_invalid_password'] = "Current Password Doesnt Match";
$lang['err_please_login'] = "Please login to continue";
$lang['err_req_values'] = "Fill all values properly";
$lang['err_email_not_registered'] = "Email not registered yet";
$lang['err_old_password'] = "Old password is wrong";
$lang['err_code_sent'] = "Code not send successfully, please try again!";
$lang['err_no_record'] = "No record found";
$lang['err_invalid_mobile'] = "Please enter valid mobile no";
$lang['err_mobile_exists'] = "This mobile number already exist.";
$lang['err_email_exists'] = "This email address already exist.";
$lang['err_invalid_user'] = "You are not valid user";
$lang['succ'] = "success";
$lang['succ_register'] = "Register successfully";
$lang['succ_login'] = "Login successfully";
$lang['succ_reset_link_sent'] = "Reset password link sent successfully";
$lang['succ_password_change'] = "Password reset successfully";
$lang['succ_profile_change'] = "Profile updated successfully";
$lang['succ_logout'] = "Logout successfully";
$lang['succ_record_insert'] = "Data inserted successfully";
$lang['succ_record_update'] = "Data updated successfully";
$lang['succ_rec_deleted'] = "Data deleted successfully";
$lang['succ_verified'] = "You are verified successfully";
$lang['succ_send_opt'] = "OTP Sent Successfully.";
$lang['err_send_opt'] = "Problem in send OTP , Please Re-generate OTP to activate your account.";
$lang['succ_update_profile'] = "Update profile successfully";
$lang['succ_reset_pass'] = "Password reset successfully";
$lang['err_you_need_to_reg'] = "You need to register";
$lang['err_video_req'] = "Please select video";
$lang['succ_like_post'] = "Like post successfully";
$lang['succ_dislike_post'] = "Dislike post successfully";
$lang['succ_comment_post'] = "Comment post successfully";
$lang['succ_event_create'] = "Event created successfully";
$lang['succ_event_update'] = "Event updated successfully";
$lang['succ_email_change'] = "Email change successfully";
$lang['succ_mobile_change'] = "Mobile change successfully";
$lang['succ_follow'] = "Scoped user successfully";
$lang['succ_unfollow'] = "Unscoped user successfully";
$lang['succ_fav'] = "Favorite post successfully";
$lang['succ_unfavorite'] = "Unfavorite post successfully";
$lang['succ_delete_post'] = "Post deleted successfully";
$lang['succ_event_delete'] = "Event deleted successfully";


//new code here
$lang['lbl_out_off_stock'] = "{{NAME}} is currently out of stock.";
$lang['lbl_item_stock'] = "{{NAME}} is {{QTY}} available only";
$lang['lbl_car_not_available'] = "Car is not available";
$lang['lbl_car_not_found'] = "Cars not available at location";
$lang['lbl_success_car_book'] = "Car is Booked Successfully";
$lang['err_book_detail_not_found'] = "Car Booking Not found";
$lang['err_booking_completed'] = "Car Booking Is Completed";
$lang['err_booking_al_cancel'] = "Car Booking Is Already Canceled";
$lang['err_booking_cancel'] = "Car Booking Is Canceled";
$lang['err_booking_last_day'] = "booking Can not cancel on last day";
$lang['err_booking_on_running_cancel'] = 'booking Can not cancel';
$lang['suc_cancel_booking'] = 'Car Booking Cancel Successfully';
$lang['suc_extend_booking'] = 'Car Booking Extended Successfully';

$lang['err_booking_pending'] = 'Booking is not Activated';
$lang['err_extend_booking'] = 'Car Booking Not Extended';

$lang['err_booking_not_found'] = 'Car Booking Not Found';
$lang['err_car_already_booked'] = 'Car Is Already Booked By someone else';

$lang['suc_booking_ratting_added'] = 'Booking Rating Added Successfully';
$lang['suc_booking_ratting_updated'] = 'Booking Rating Updated Successfully';
$lang['err_booking_user_not'] = 'You Are not authorize to give rating.';

$lang['succ_currency_changed'] = 'Currency Changed Successfully';
