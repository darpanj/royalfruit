<?php

//login page
$lang['login_header_title'] = 'Login';
$lang['lbl_sign_in'] = 'Sign In';
$lang['lbl_forget_pass'] = 'Forget Password';
$lang['lbl_remember_checkbox'] = 'Remember Me';

$lang['btn_sign_in'] = 'Sign In ?';
$lang['btn_forget_pass'] = 'Forget password ?';

$lang['input_place_user'] = 'Please Enter Username';
$lang['input_place_password'] = 'Please Enter Password';
$lang['input_place_forget_password'] = 'Please Enter Email';


$lang['err_email_req'] = 'Please Enter Username';
$lang['err_password_req'] = 'Please Enter Password';

//$lang['err_not_auth_user'] = '';


$lang['err_something_went_wrong'] = 'Something Went Wrongs';
$lang['err_login'] = 'Invalid username or password.';
//$lang['']='';


$lang['lbl_heder_sign_in'] = 'تسجيل الدخول';

