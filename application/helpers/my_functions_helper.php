<?php
defined('BASEPATH') or exit('No direct script access allowed');


if (!function_exists('setempty')) {

    function setempty($name = '', $method = 'post')
    {
        $CI = &get_instance();
        $name = ($CI->input->$method($name)) ? $CI->input->$method($name) : '';
        return $name;
    }
}


if (!function_exists('disMessage')) {

    function disMessage($msgArray = array(), $script = true)
    {
        $CI = &get_instance();
        $message = '';
        $content = '';
        $type = isset($msgArray["type"]) ? $msgArray["type"] : NULL;
        $var = isset($msgArray["var"]) ? $msgArray["var"] : NULL;
        $CI->session->unset_userdata('msgType');
        if (!is_null($var)) {
            if (defined($var)) {
                $message = constant($var);
            } else {
                $message = $var;
            }
        }

        if ($script) {
            $content = '<script type="text/javascript" language="javascript">$(document).ready(function(){Custom.myNotification("' . ucwords($type) . '","' . $message . '");})</script>';
        } else {
            $content = $message;
        }

        return $content;
    }
}



if (!function_exists('sendEmailAddress')) {

    function sendEmailAddress($to, $subject, $message, $fromEmail = ADMIN_EMAIL, $fromName = SITENAME)
    {
        $list = is_array($to) ? $to : (array) $to;
        $CI = &get_instance();
        $CI->load->library('email');
        $CI->email->initialize(array(
            'protocol' => 'SMTP',
            'smtp_host' => 'mail.deluxcoder.com',
            'smtp_user' => 'zestbrains@deluxcoder.com',
            'smtp_pass' => 'wZpP8-o6a{G1',
            'smtp_port' => 587,
            'smtp_crypto' => 'ssl',
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => "html",
            'charset' => "utf-8",
        ));
        $CI->email->from($fromEmail, $fromName);
        $CI->email->to($list);
        $CI->email->reply_to($fromEmail, $fromName);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if ($CI->email->send()) {
            return true;
        } else {
            show_error($CI->email->print_debugger());
            return false;
        }
    }
}

if (!function_exists('breadcrumb')) {

    function breadcrumb($aBradcrumb = array())
    {
        $content = '';
        ob_start();
        if (count($aBradcrumb) > 0) {
?>

            <?php
            $i = 1;
            foreach ($aBradcrumb as $key => $link) {
            ?>
                <a href="<?php echo $link != '' ? $link : 'javascript:void(0)'; ?>" class="kt-subheader__breadcrumbs-link"><?php echo ucfirst($key); ?></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
            <?php
                $i++;
            }
            ?>
<?php
        }
        $content = ob_get_clean();
        return $content;
    }
}

if (!function_exists('generateEmailTemplate')) {

    function generateEmailTemplate($sys_flag, $key = array(), $val = array())
    {
        $content = array();
        $content['fromemail'] = ADMIN_EMAIL;
        $content['fromname'] = SITENAME;
        $content['message'] = "";
        $content['subject'] = "";
        $CI = &get_instance();
        $q = $CI->db->query("SELECT et.vTemplate,et.vSubject,et.vFromEmail,et.vFromName FROM mst_email_templates as et WHERE et.vSysFlag = ? AND et.eIsactive = ?", array($sys_flag, 'y'));

        if ($q->num_rows() === 1) {
            $r = $q->row_array();
            $footertext = str_replace('{{YEAR}}', date('Y'), FOOTER_TEXT);
            $sitelogo = base_url('themes/admin/media/logo.png');
            $key = array_merge(array('{{SITENAME}}', '{{SITE_URL}}', '{{FOOTER_TEXT}}', '{{SITE_LOGO}}'), $key);
            $val = array_merge(array(SITENAME, base_url(), $footertext, $sitelogo), $val);

            $message = trim(stripslashes($r["vTemplate"]));
            $subject = trim(stripslashes($r["vSubject"]));
            $subject = str_replace('{{SITENAME}}', SITENAME, $subject);
            $message = str_replace($key, $val, $message);
            $content['message'] = $message;
            $content['subject'] = $subject;
            $content['fromemail'] = $r["vFromEmail"] != "" ? $r["vFromEmail"] : ADMIN_EMAIL;
            $content['fromname'] = $r["vFromName"] != "" ? $r["vFromName"] : SITENAME;
        }
        return $content;
    }
}
if (!function_exists('get_date')) {

    function get_date()
    {
        return gmdate('Y-m-d H:i:s +0000');
    }
}

if (!function_exists('unlink_file')) {

    function unlink_file($files = "")
    {
        if (is_array($files)) {
            foreach ($files as $file) {
                unlink_file($file);
            }
        } else {
            if (is_file($files)) {
                @unlink($files);
            }
        }
    }
}

if (!function_exists('replaceEmptyString')) {

    function replaceEmptyString($str = '', $replaceStr = 'N/A')
    {
        return ($str != '' ? $str : $replaceStr);
    }
}

if (!function_exists('je_mobile')) {

    function je_mobile($Status, $Message = "", $ResponseData = NULL, $extraData = NULL)
    {
        if (is_array($ResponseData)) {
            $data["status"] = $Status;
            $data["message"] = $Message;
            $data["data"] = $ResponseData;
            if (!empty($extraData)) {
                foreach ($extraData as $key => $val) {
                    $data[$key] = $val;
                }
            }
        } else if (!empty($ResponseData)) {
            $data["status"] = $Status;
            $data["message"] = $Message;
            $data["data"] = $ResponseData;
        } else {
            $data["status"] = $Status;
            $data["message"] = $Message;
            $data["data"] = new stdClass();
        }
        header('Content-Type: application/json');
        http_response_code($data["status"]);
        echo json_encode($data);
        die;
    }
}

if (!function_exists('pre')) {

    function pre($arg, $exit = TRUE)
    {
        echo "<pre>";
        print_r($arg);
        echo "</pre>";
        if ($exit)
            die();
    }
}

if (!function_exists('random_number')) {

    function random_number($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $random = substr(str_shuffle($chars), 0, $length);
        return $random;
    }
}

function mb_truncate($str, $limit)
{
    return mb_substr(strip_tags($str), 0, $limit, 'UTF-8') . (mb_strlen($str) > $limit ? '...' : '');
}

function get_column($table)
{
    $r = array();
    $CI = &get_instance();
    $q = $CI->db->query('SHOW COLUMNS FROM ' . $table)->result_array();
    foreach ($q as $key => $value) {
        $r[$value['Field']] = "";
    }
    return $r;
}

function checkImage($id = 0, $filepath = "", $zc = 0, $ql = 100, $crop = TRUE)
{

    $CI = &get_instance();
    $src = base_url() . SITE_UPD . "no_image_available.jpg";
    $params = array();

    $q = $CI->db->query("SELECT * FROM mst_imagethumb WHERE iThumbId = ?", array($id));

    if ($q->num_rows() > 0) {
        $r = $q->row_array();
        $filepath = $r['vFolder'] . '/' . $filepath;

        if (!is_file(FCPATH . SITE_UPD . $filepath)) {
            if (!is_file(FCPATH . SITE_UPD . $r['vFolder'] . '/' . $r['vDefaultImage'])) {
                $filepath = "no_image_available.jpg";
            } else {
                $filepath = $r['vFolder'] . '/' . $r['vDefaultImage'];
            }
        }
        $src = SITE_UPD . $filepath;
        if ($crop)
            $src = base_url() . "image-thumb.php?w=" . $r['iWidth'] . "&h=" . $r['iHeight'] . "&zc=" . $zc . "&q=" . $ql . "&src=" . base_url() . $src;
        else
            $src = base_url() . $src;
    }
    return $src;
}

function isTokenExist($token, $table, $field)
{
    $CI = &get_instance();
    if ($token != '') {
        $q = $CI->db->select('*')->from($table)->where($field, $token)->get();
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

if (!function_exists('space_remove')) {

    function space_remove($input = "")
    {
        return preg_replace('/\s+/', '', $input);
    }
}

function genUniqueStr($prefix = '', $length = 10, $table, $field, $isAlphaNum = false)
{
    $arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    if ($isAlphaNum) {
        $arr = array_merge($arr, array(
            'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z'
        ));
    }


    $token = $prefix;
    $maxLen = max(($length - strlen($prefix)), 0);
    for ($i = 0; $i < $maxLen; $i++) {
        $index = rand(0, count($arr) - 1);
        $token .= $arr[$index];
    }

    if (isTokenExist($token, $table, $field)) {
        genUniqueStr($prefix, $length, $table, $field, $isAlphaNum);
    } else {
        return $token;
    }
}

if (!function_exists('get_error')) {

    function get_error($data = array())
    {
        $i = 0;
        $return_data = "";
        foreach ($data as $key => $value) {
            if ($i != 0) {
                continue;
            }
            $return_data = $value;
            $i++;
        }
        return $return_data;
    }
}

function sendPushNotification($fields = '')
{
    $result = '';
    if ($fields != '') {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Authorization: key=" . FCM_KEY;
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }
    return $result;
}

function get_price($start_date, $end_date, $price = 10, $weekly_price = 20, $monthly_price = 30, $yearly_price = 50, $increment = 1)
{

    if ($price < 1) {
        $price = 1;
    }
    if ($weekly_price < 1) {
        $weekly_price = 1;
    }
    if ($monthly_price < 1) {
        $monthly_price = 1;
    }
    if ($yearly_price < 1) {
        $yearly_price = 1;
    }


    $earlier = new DateTime(date('Y-m-d', strtotime($start_date)));
    $later = new DateTime(date('Y-m-d', strtotime($end_date)));
    $total_days = $later->diff($earlier)->format("%a") + $increment;
    $total_price = 0;
    if ($total_days < 7) {
        $total_price = $total_days * $price;
    } elseif ($total_days >= 7 && $total_days < 30) {
        $total_weeks = intval((int) $total_days / 7);
        $week_days = $total_weeks * 7;
        $left_days = $total_days - $week_days;
        $total_price = ($week_days * $weekly_price) + ($left_days * $price);
    } elseif ($total_days >= 30 && $total_days < 365) {
        $total_month = intval((int) $total_days / 30);
        $month_days = $total_month * 30;
        $left_month_days = $total_days - $month_days;
        $inner_price = $month_days * $monthly_price;
        if ($left_month_days > 0) {
            $total_weeks = intval((int) $left_month_days / 7);
            $week_days = $total_weeks * 7;
            $left_days = $left_month_days - $week_days;
            $total_price = ($week_days * $weekly_price) + ($left_days * $price);
            $inner_price = $inner_price + $total_price;
        }
        $total_price = $inner_price;
    } else {
        $total_year = intval((int) $total_days / 365);
        $total_year_days = $total_year * 365;
        $inner_price = $total_year_days * $yearly_price;
        $year_left_days = $total_days - $total_year_days;
        if ($year_left_days > 0) {
            $total_month = intval((int) $year_left_days / 30);
            $month_days = $total_month * 30;
            $left_month_days = $year_left_days - $month_days;
            $inner_price += $month_days * $monthly_price;
            if ($left_month_days > 0) {
                $total_weeks = intval((int) $left_month_days / 7);
                $week_days = $total_weeks * 7;
                $left_days = $left_month_days - $week_days;
                $total_price = ($week_days * $weekly_price) + ($left_days * $price);
                $inner_price = $inner_price + $total_price;
            }
            $total_price = $inner_price;
        }
    }
    return $total_price;
}

function formatInterval($start_date, $end_date)
{
    $earlier = new DateTime(date('Y-m-d H:i:s', strtotime($start_date)));
    $later = new DateTime(date('Y-m-d H:i:s', strtotime($end_date)));
    $interval = $later->diff($earlier);
    $result = "";
    if ($interval->y) {
        $result = $interval->format("%yyr");
    }
    if (empty($result) && $interval->m) {
        $result = $interval->format("%mmonth");
    }
    if (empty($result) && $interval->d) {
        $result = $interval->format("%dd");
    }
    if (empty($result) && $interval->h) {
        $result = $interval->format("%hhr");
    }
    if (empty($result) && $interval->i) {
        $result = $interval->format("%imin");
    }
    if (empty($result) && $interval->s) {
        $result = $interval->format("%ssec");
    }
    if (empty($result))
        $result = 'now';

    return $result;
}

function get_days($start_date, $end_date, $increment = 1)
{
    $earlier = new DateTime(date('Y-m-d', strtotime($start_date)));
    $later = new DateTime(date('Y-m-d', strtotime($end_date)));
    return $later->diff($earlier)->format("%a") + $increment;
}

if (!function_exists('get_price_code')) {

    function get_price_code($currency = "usd")
    {
        //        'usd','afghani','inr','riyal','dirham','euro'
        if ($currency == "afghani") {
            return 'AFN';
        } elseif ($currency == "inr") {
            return 'INR';
        } elseif ($currency == "riyal") {
            return 'SAR';
        } elseif ($currency == "dirham") {
            return 'AED';
        } elseif ($currency == "euro") {
            return 'EUR';
        } else {
            return 'USD';
        }
        //{currency: 'Afghan Afghani', code: 'AFN'},
        //{currency: 'Indian Rupee', code: 'INR'},
        //{currency: 'Saudi Riyal', code: 'SAR'},
        //{currency: 'United Arab Emirates Dirham', code: 'AED'},
        //{currency: 'United States Dollar', code: 'USD'},
        //{currency: 'Euro', code: 'EUR'},
    }
}

function calculation_percentage($amount = 100)
{
    $cal = ($amount * Cancellation_charge) / 100;
    $total = $amount - $cal;
    return $total;
}

function get_facebook_url()
{
    require_once FCPATH . 'application/libraries/Facebook/autoload.php';
    require_once FCPATH . 'application/libraries/GoogleAPI/vendor/autoload.php';
    $fb = new Facebook\Facebook([
        'app_id' => '477264979633405', // Replace {app-id} with your app id
        'app_secret' => 'b4d6751c3444ad4416638b9e60803b28',
        'default_graph_version' => 'v3.2',
    ]);
    $helper = $fb->getRedirectLoginHelper()->getLoginUrl(base_url('home/social_login/facebook/'), ['email']);

    $g_client = new Google_Client();
    $g_client->setClientId("1077711542210-kbeg700csvnqrh899qrbvhi4j58pdo22.apps.googleusercontent.com");
    $g_client->setClientSecret("-9UGY6M-BO-zmft2G1gnm2QV");
    $g_client->setApplicationName("stintr");
    $g_client->setRedirectUri(base_url('home/social_login/google/'));
    $g_client->addScope("https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email");
    $google_login_url = $g_client->createAuthUrl();

    $fb_login_url = $helper;

    return $fb_login_url;
}

function get_google_url()
{


    $g_client = new Google_Client();
    $g_client->setClientId("104727049902-eqbs5ec0aas4195ldmk49vfpesi77ikn.apps.googleusercontent.com");
    $g_client->setClientSecret("g8r5IP9Etni8D0uqR8M1zyJD");
    $g_client->setApplicationName("ajar");
    $g_client->setRedirectUri(base_url('home/social_login/google/'));
    $g_client->addScope("https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email");
    $google_login_url = $g_client->createAuthUrl();


    return $google_login_url;
}

function get_currency_symbol($currency)
{
    if ($currency == "afghani") {
        return '؋';
    } elseif ($currency == "inr") {
        return '₹';
    } elseif ($currency == "riyal") {
        return '﷼‎';
    } elseif ($currency == "dirham") {
        return 'د.إ';
    } elseif ($currency == "euro") {
        return '€';
    } else {
        return '$';
    }
}

function multisort(&$array, $key, $order = 'asc')
{
    $valsort = array();
    $ret = array();
    reset($array);
    foreach ($array as $ii => $va) {
        $valsort[$ii] = $va[$key];
    }
    if ($order == 'asc') {
        asort($valsort);
    } else {
        arsort($valsort);
    }
    foreach ($valsort as $ii => $va) {
        $ret[$ii] = $array[$ii];
    }
    $array = $ret;
    return $array;
}

function check_variable_value($var, $type = 'var')
{
    $variable = '';
    if ($type != 'var') {
        $variable = array();
        if (count($var)) {
            $variable = $var;
        }
    } else {
        if (isset($var)) {
            $variable = $var;
        }
    }
    return $variable;
}
function get_date_in_format($convertedDate = '')
{
    $dateDisplay = '0 Years, 0 Months, 0 Weeks';
    if ($convertedDate != '') {
        $dataDate = strtotime($convertedDate);
        $todayDate = strtotime(date('Y-m-d h:i:s'));

        $diff = abs($todayDate - $dataDate);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = get_days($convertedDate, date('Y-m-d h:i:s'));
        $weeks = floor($days / 7);

        if ($years > 0) {
            $dateDisplay = $years . ' year ago';
        } else  if ($months > 0 && $months <= 12 && $weeks > 4) {
            $dateDisplay = $months . ' month ago';
        } else if ($weeks > 0 && $weeks <= 4 && $months <= 0) {
            $dateDisplay = $weeks . ' week ago';
        }
    }
    return $dateDisplay;
}


if (!function_exists('time_Ago')) {
    function time_Ago($time)
    {
        // Calculate difference between current 
        // time and given timestamp in seconds 
        $diff = time() - $time;
        // Time difference in seconds 
        $sec = $diff;
        // Convert time difference in minutes 
        $min = round($diff / 60);
        // Convert time difference in hours 
        $hrs = round($diff / 3600);
        // Convert time difference in days 
        $days = round($diff / 86400);
        // Convert time difference in weeks 
        $weeks = round($diff / 604800);
        // Convert time difference in months 
        $mnths = round($diff / 2600640);
        // Convert time difference in years 
        $yrs = round($diff / 31207680);
        // Check for seconds 
        if ($sec <= 60) {
            echo "$sec seconds ago";
        }
        // Check for minutes 
        else if ($min <= 60) {
            if ($min == 1) {
                echo "one minute ago";
            } else {
                echo "$min minutes ago";
            }
        }
        // Check for hours 
        else if ($hrs <= 24) {
            if ($hrs == 1) {
                echo "an hour ago";
            } else {
                echo "$hrs hours ago";
            }
        }
        // Check for days 
        else if ($days <= 7) {
            if ($days == 1) {
                echo "Yesterday";
            } else {
                echo "$days days ago";
            }
        }
        // Check for weeks 
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                echo "a week ago";
            } else {
                echo "$weeks weeks ago";
            }
        }
        // Check for months 
        else if ($mnths <= 12) {
            if ($mnths == 1) {
                echo "a month ago";
            } else {
                echo "$mnths months ago";
            }
        }
        // Check for years 
        else {
            if ($yrs == 1) {
                echo "one year ago";
            } else {
                echo "$yrs years ago";
            }
        }
    }
}

function get_periods_dates()
{
    $arr = array();
    $date = date('d');
    if ($date >= 1 && $date <= 15) {
        $fourthmonth = date('M'); //JULY 1-13
        $fourthStartDate = 1;
        $fourthEndDate = date('d', strtotime('-1 days'));
        $thirdmonth = date('M', strtotime('-1 month'));
        $thirdStartDate = 16;
        $thirdEndDate = date('j', strtotime('last day of previous month'));
        $secondMonth = date('M', strtotime('-1 month'));
        $secondStartDate = 1;
        $secondEndDate = 15;
        $firstmonth = date('M', strtotime('-2 month'));
        $firstStartDate = 16;
        $firstEndDate = date('t', strtotime('-2 month'));

        $year = date('Y');
        $arr['fourthStartDate'] = date('Y-m-d', strtotime($year . '-' . $fourthmonth . '-' . $fourthStartDate));
        $arr['fourthEndDate'] = date('Y-m-d', strtotime($year . '-' . $fourthmonth . '-' . $fourthEndDate));
        $arr['thirdStartDate'] = date('Y-m-d', strtotime($year . '-' . $thirdmonth . '-' . $thirdStartDate));
        $arr['thirdEndDate'] = date('Y-m-d', strtotime($year . '-' . $thirdmonth . '-' . $thirdEndDate));
        $arr['secondStartDate'] = date('Y-m-d', strtotime($year . '-' . $secondMonth . '-' . $secondStartDate));
        $arr['secondEndDate'] = date('Y-m-d', strtotime($year . '-' . $secondMonth . '-' . $secondEndDate));
        $arr['firstStartDate'] = date('Y-m-d', strtotime($year . '-' . $firstmonth . '-' . $firstStartDate));
        $arr['firstEndDate'] = date('Y-m-d', strtotime($year . '-' . $firstmonth . '-' . $firstEndDate));

        $arr['fourth_ele'] = $fourthmonth . " " . $fourthStartDate . "-" . $fourthEndDate;
        $arr['third_ele'] = $thirdmonth . " " . $thirdStartDate . "-" . $thirdEndDate;
        $arr['second_ele'] = $secondMonth . " " . $secondStartDate . "-" . $secondEndDate;
        $arr['first_ele'] = $firstmonth . " " . $firstStartDate . "-" . $firstEndDate;
    } else {
        $fourthmonth = date('M'); //JULY 16-19
        $fourthStartDate = 16;
        $fourthEndDate = date('d', strtotime('-1 days'));
        $thirdmonth = date('M');
        $thirdStartDate = 1;
        $thirdEndDate = 15;
        $secondMonth = date('M', strtotime('-1 month'));
        $secondStartDate = 16;
        $secondEndDate = date('t', strtotime('-1 month'));
        $firstmonth = date('M', strtotime('-2 month'));
        $firstStartDate = 1;
        $firstEndDate = 15;
        $arr['fourth_ele'] = $fourthmonth . " " . $fourthStartDate . "-" . $fourthEndDate;
        $arr['third_ele'] = $thirdmonth . " " . $thirdStartDate . "-" . $thirdEndDate;
        $arr['second_ele'] = $secondMonth . " " . $secondStartDate . "-" . $secondEndDate;
        $arr['first_ele'] = $firstmonth . " " . $firstStartDate . "-" . $firstEndDate;

        $year = date('Y');
        $arr['fourthStartDate'] = date('Y-m-d', strtotime($year . '-' . $fourthmonth . '-' . $fourthStartDate));
        $arr['fourthEndDate'] = date('Y-m-d', strtotime($year . '-' . $fourthmonth . '-' . $fourthEndDate));
        $arr['thirdStartDate'] = date('Y-m-d', strtotime($year . '-' . $thirdmonth . '-' . $thirdStartDate));
        $arr['thirdEndDate'] = date('Y-m-d', strtotime($year . '-' . $thirdmonth . '-' . $thirdEndDate));
        $arr['secondStartDate'] = date('Y-m-d', strtotime($year . '-' . $secondMonth . '-' . $secondStartDate));
        $arr['secondEndDate'] = date('Y-m-d', strtotime($year . '-' . $secondMonth . '-' . $secondEndDate));
        $arr['firstStartDate'] = date('Y-m-d', strtotime($year . '-' . $firstmonth . '-' . $firstStartDate));
        $arr['firstEndDate'] = date('Y-m-d', strtotime($year . '-' . $firstmonth . '-' . $firstEndDate));
    }

    return $arr;
}
